package com.ascamm.ascammnav.map;

/**
 * 
 * Defines the types of map
 * @author Junzi Sun [CTAE]
 *
 */
public enum MapType {
	MAP_CALIBRATION,
	MAP_NAVIGATION,
	MAP_ANALYZER
}
