package com.ascamm.ascammnav.map;

import java.util.ArrayList;
import java.util.LinkedList;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.ascamm.ascammnav.ui.Preferences;
import com.ascamm.motion.algorithm.ParticleState;
import com.ascamm.motion.algorithm.PointWeight;
import com.ascamm.motion.database.APWithPosition;
import com.ascamm.motion.database.CalibrationPoint;
import com.ascamm.motion.database.DBHelper;
import com.ascamm.motion.database.PointWithCalibrationItems;
import com.ascamm.motion.utils.Point3DWithFloor;
import com.ascamm.pois.utils.Fence;
import com.ascamm.pois.spatialite.GeoFence;
import com.ascamm.pois.spatialite.Geometry;
import com.ascamm.pois.utils.POI;
import com.ascamm.motion.utils.Edge;
import com.ascamm.motion.utils.Marker;
import com.ascamm.utils.Point3D;

import com.ascamm.motion.LocationData;
import com.ascamm.ascammnav.R;

public class Map extends SurfaceView implements SurfaceHolder.Callback {

	private String TAG = "Map";
	private boolean debug = false;
	/*+++++++++++++++++++++++++ Map related variables +++++++++++++++++++++++++*/
	/** Map style, defines offline or online map */
	private MapType mMapType;

	/** thread triggers onDraw() method */
	private MapThread mMapThread;

	/** Bitmap format of the map */
	private Bitmap mBmpMap;

	/** map size in pixels */
	private float mMapWidthInPx = 0, mMapHeightInPx = 0;
	/** Surface View size in pixels */
	private float surfaceWidth, surfaceHeight;
	
	private int mMapFloor = 0;
	private int mMapBackgroundColor;
	private boolean mMapInitialized;

	/** current selected pixel default (0,0) */
	private Point3D mCurrentCalibPosition = new Point3D(0, 0, 0);
	private Point3D mOldCalibPosition = new Point3D(0, 0, 0);
	private Point3D mCurrentOnlinePosition = new Point3D(-1, -1, -1);
	private Point3D mCurrentOnlinePosition2 = new Point3D(-1, -1, -1);
	private ArrayList<Point3D> positionHistoric = new ArrayList<Point3D>();
	private ArrayList<Point3D> position2Historic = new ArrayList<Point3D>();
	private ArrayList<Point3D> detectionsPos = new ArrayList<Point3D>();
	private ArrayList<Fence> fences = new ArrayList<Fence>();
	private ArrayList<Geometry> geometries = new ArrayList<Geometry>();
	private int selectedFenceId = -1;
	private Point3D destinationPoint;
	private float mOrientation = 0;
	private float mUserOrientation = -1;
	private float mUserOrientationReliability = 0;
	private float mOrientationCorrected = 0;

	//LQ: -----------------[POIs]-----------------
	private ArrayList<POI> staticPois = new ArrayList<POI>();
	private ArrayList<POI> mobilePois = new ArrayList<POI>();
	private int selectedPoiId = -1;
	private POI destinationMobilePOI = null;
	private static boolean mSettingShowPOIs = true;
	private static boolean mSettingShowMobilePOIs = false;
	private static boolean mSettingShowStaticPOIs = true;
	private static boolean mSettingShowMobileDestination = true;
	/*-----------------[END OF POIs]-----------------*/
	
	private Point3D mCurrentParticleFilterPosition = new Point3D(-1, -1, -1);
	private Point3D mCurrentMapFusionPosition = new Point3D(-1, -1, -1);
	private ArrayList<ParticleState> mCurrentParticles;
	private int mFilterIsResampled;

	private int mCurrentError;
	private ArrayList<Point3D> mCurrentNeighbors = new ArrayList<Point3D>();
	private ArrayList<PointWeight> mListPositions = new ArrayList<PointWeight>();
	
	private int mCurrentError2;
	private ArrayList<Point3D> mCurrentNeighbors2 = new ArrayList<Point3D>();
	private ArrayList<PointWeight> mListPositions2 = new ArrayList<PointWeight>();
	
	private Point3D mLastOnlinePosition;
	private float mPriorDistance = 0;
	
	private ArrayList<CalibrationPoint> mCalibratedPoints;
	private ArrayList<PointWithCalibrationItems> mListOfPointsWithCalibrations;
	private ArrayList<APWithPosition> mListOfAPs;
	private ArrayList<APWithPosition> mListOfAPsOfFloor;
	private ArrayList<Point> mListOfHighLightPoints;
	private ArrayList<Point3D> mNodes;
	private ArrayList<Edge> mEdges;
	private ArrayList<Edge> mCurrentEdges;
	private ArrayList<Marker> mMarkers;
	private ArrayList<Point3D> mCalibNodes;
	private ArrayList<Edge> mCalibEdges;
	private Edge mCurrentCalibEdge;
	
	private Paint mPaint = new Paint();
	
	private boolean refresh = false, variableError = false, anchored = false;
	private static double lineTolerance = 10;
	private static float angleTolerance = 30;

	private boolean demoMode = false;
	/* ------------------- [END OF] Map related variables -------------------------*/

	/* +++++++++++++++++++++++++ Map touch and zoom variables +++++++++++++++++++++++++*/
	/* canvas transformation matrix */
	private Matrix mMapMatrix = new Matrix();
	private Matrix mMapMatrixHold = new Matrix();
	
	/* multi-touch helpers */
	private PointF mPointStartTouch = new PointF();
	private PointF mSecondPointStartTouch = new PointF();
	private PointF mPointFinalTouch = new PointF();
	private PointF mSecondPointFinalTouch = new PointF();
	private PointF mPointMidOfTwoFingers = new PointF();
	private float mDistOfTwoFingers;
	private float mDistOfTwoFingersHold;
	private int mTouchMode;

	/** Zoom scale applied when zoom button is clicked **/
	private static final float mZoomScale = 1.2f;
	private float maxScale = 8;
	private float minScale = 0.5f;
	private float mScale = 1;
	private float rotAngle = 0;
	private float lastRotAngle = 0;
	private double totalRotAngle = 0;
	private float totalScale = 1;
	private float MIN_ROT_ANGLE = 15;
	private float OFFSET_ROT_ANGLE = 0;
	private boolean rotEnabled = false;
		
	private static final int TOUCH_MODE_NONE = 0;
	private static final int TOUCH_MODE_DRAG = 1;
	private static final int TOUCH_MODE_ZOOM = 2;
	private static final int TOUCH_MODE_HALT = 3;
	private static final int TOUCH_MODE_ROTATE = 4;
	/* ---------------- [ END OF ] Map touch and zoom variables ------------------*/
	
	/* ++++++++++++++++++++++++++ Temporary Settings +++++++++++++++++++++++++++++++++++++++*/
	private static final int mSettingShowOutput = 1;
	private static int mSettingShowNeighbors = 0;
	private static int mSettingShowFilterOutput = 0;
	private static int mSettingShowFilterParticles = 0;
	private static int mSettingShowMapFusionOutput = 0;
	private static int mSettingShowListPositions = 0;
	private static final int mSettingShowOutput2 = 1;
	private static int mSettingShowNeighbors2 = 1;
	private static int mSettingShowHistoric1 = 0;
	private static int mSettingShowHistoric2 = 0;
	private static int mSettingShowRoute = 1;
	private static boolean mSettingShowPaths = false;
	private static boolean mSettingShowFences = true;
	private static int mSettingShowPriorDistance = 0;
	private static boolean mSettingShowOrientation = true;
	private static boolean mSettingShowOrientationCorrected = false;
	/* ---------------- [ END OF ] Temporary Settings --------------------------------------*/
	
	/* +++++++++++++++++++ List of colors used to represent positions ++++++++++++++++++++++*/
	private static final String colorPos = "#7FAF1B"; // green
	private static final String colorPosBorder = "#FFFFFF"; // white
	private static final String colorPosShadow = "#888888"; // grey
	private static final String colorNeighbor = "#FF9900"; // orange
	private static final String colorNeighborAbove = "#DF0000"; // red
	private static final String colorNeighborBelow = "#0040FF"; // blue
	private static final String colorPos2 = "#843179"; // violet
	private static final String colorNeighbor2 = "#888888"; // grey
	private static final String colorNeighborAbove2 = "#620000"; // dark red
	private static final String colorNeighborBelow2 = "#011D72"; // dark blue
	private static final String colorFilter = "#2848B2"; // blue
	//private static final String colorParticle = "#909B3D"; // khaki green
	//private static final String colorParticleResampled = "#6C444B"; // violet
	private static final String colorParticle = "#FF9900"; // orange
	private static final String colorParticleResampled = "#C67700"; // dark orange
	private static final String colorParticleAbove = "#DF0000"; // red
	private static final String colorParticleBelow = "#0040FF"; // blue
	private static final String colorParticleFree = "#01DFA5"; // light green
	private static final String colorParticleFreeResampled = "#007F5D"; // dark green
	private static final String colorParticleMaxWeight = "#909B3D"; // khaki green
	private static final String colorParticleFreeMaxWeight = "#6C444B"; // violet
	private static final String colorMapFusion = "#FF0000"; // red
	private static final String colorEdges = "#0000FF"; // blue
	private static final String colorCalibEdges = "#00FF00"; // green
	private static final String colorCalibEdgesSent = "#888888"; // grey
	private static final String colorCurrentCalibEdge = "#DF0000"; // red
	private static final String colorPointWifi = "#0000FF"; // blue
	private static final String colorPointBle = "#FF0000"; // red
	private static final String colorHighlightPoint = "#00FF00"; // green
	private static final String colorText = "#FF0000"; // red
	private static final String colorPointRoute = "#00FF00"; // green
	private static final String colorPointRoutePassed = "#FF0000"; // red
	private static final String colorDetection = "#FF0000"; // red
	private static final String colorFence = "#0000FF"; // blue
	private static final String colorFenceselected = "#FF0000"; // red
	private static final String colorGeoFence = "#00FF00"; // green
	private static final String colorGeoFenceselected = "#FF0000"; // red
	private static final String colorPriorDistance = "#DA00FF"; // violet
	private static final String colorOrientationCorrected = "#FF0000"; // red
	private static final String colorOrientationNonReliable = "#FF0000"; // red
	private static final String colorPOI = "#0000FF"; // blue
	private static final String colorPOIselected = "#FF0000"; // blue
	/*----------------- [ END OF ] List of colors used to represent positions --------------*/
	
	/*----------------- Routes to test the performance of the Positioning System -----------*/
	private static final ArrayList<Point3D> mTestingRoute = new ArrayList<Point3D>();
	/*----------------- [ END OF ] Routes -----------------------------------*/
	
	/*---------------- Routes ----------------------- */
	private LinkedList<Point3D> mRoute;
	private boolean showRoute = true;
	public ArrayList<Edge> tmpEdges;
	private boolean navigationMode = false;
	private static final float navigationModeZoom = 2;
	/*---------------- [ END OF ] Routes ---------------*/
	
	Resources res;
	Bitmap bitmap;
	
	/** list of event listener subscriber */
	private ArrayList<MapEventListener> mOfflineCurrentPositionListener;
	private ArrayList<PointSelectedEventListener> mPointSelectedListener;

	private DBHelper mDBHelper;
	
	public void setTestingRoute(ArrayList<Point3D> route){
		mTestingRoute.clear();
		for(Point3D p : route){
			mTestingRoute.add(p);
		}
	}
	
	public Map(Context context, MapType mapType, Bitmap bmpMap, int initialFloor, int idSite, boolean showListPositions, boolean showPaths) {
		super(context);
		
		if(showListPositions){
			mSettingShowListPositions = 1;
		}
		else{
			mSettingShowListPositions = 0;
		}
		mSettingShowPaths = showPaths;
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

		demoMode = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_DEMO_MODE, false);
		if(demoMode){
			mSettingShowFilterOutput = 0;
			mSettingShowFilterParticles = 0; 
			mSettingShowMapFusionOutput = 0;
			mSettingShowPriorDistance = 0;
		} else{
			mSettingShowFilterOutput = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_PARTICLE_FILTER, false) ? 1 : 0;;
			mSettingShowFilterParticles = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_SHOW_FILTER_PARTICLES, false) ? 1 : 0;;
			mSettingShowMapFusionOutput = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_MAP_FUSION, false) ? 1 : 0;;
		}
		mMapBackgroundColor = context.getResources().getColor(R.color.ascamm_map_background);
		/* initialize variables */
		mMapType = mapType;
		mMapFloor = initialFloor;
		mBmpMap = bmpMap;
		if(mBmpMap!=null){
			mMapWidthInPx = mBmpMap.getWidth();
			mMapHeightInPx = mBmpMap.getHeight();
		}
		mMapInitialized = false;
				
		/* register the view to the surface holder */
		getHolder().addCallback(this);
		
		/* set the thread, NOT started yet */
		mMapThread = new MapThread(this);
				
		/* secure the view is focusable */
		setFocusable(true);

		/* get the DB helper */
		mDBHelper = new DBHelper(getContext(), idSite, null, false);
		
		if(mMapType == MapType.MAP_CALIBRATION || mMapType == MapType.MAP_ANALYZER){
			/*
			mListOfAPs = mDBHelper.getAllAPs();
			getNodesAndEdges(initialFloor);
			getCalibNodesAndEdges(initialFloor);
			if(mMapType == MapType.MAP_CALIBRATION){
				mMarkers = new ArrayList<Marker>();
			}
			else if(mMapType == MapType.MAP_ANALYZER){
				getAPsOfFloor(initialFloor);
				getCalibratedPoints(initialFloor);
			}
			*/
		} else if (mMapType == MapType.MAP_NAVIGATION){
			getNodesAndEdges(initialFloor);
		}
		
		/* Listeners */
		mOfflineCurrentPositionListener = new ArrayList<MapEventListener>();
		mPointSelectedListener = new ArrayList<PointSelectedEventListener>();

		//location icon
		res = getResources();
		bitmap = BitmapFactory.decodeResource(res, R.drawable.location_on_red);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		if(debug)	Log.i(TAG,"Surface Changed. width: " + width + " , height: " + height);
		surfaceWidth = width;
		surfaceHeight = height;
		if(anchored){
			moveCurrentCalibPositionToCenter();
		}
		setRefresh(true);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if(!mMapThread.isAlive()) {
			mMapThread = new MapThread(this);
		}
		mMapThread.mIsRun = true;
		mMapThread.start();
		
		if(!mMapInitialized){
			/* Obtain the Width and Height of the Surface View */
			surfaceWidth = holder.getSurfaceFrame().width();
			surfaceHeight = holder.getSurfaceFrame().height();
			if(debug)	Log.i(TAG,"Surface Created. width: " + surfaceWidth + " , height: " + surfaceHeight);
			
			/* Set the initial zoom of the map */
			float initialScale = 1;
			if(mMapWidthInPx!=0){
				initialScale = Math.min(surfaceWidth/mMapWidthInPx, surfaceHeight/mMapHeightInPx);
			}else{
				mMapWidthInPx = surfaceWidth;
				mMapHeightInPx = surfaceHeight;
			}
			float extraScale = 1f;
			mScale = initialScale*extraScale;
			//System.out.println("initialScale: " + mScale);
			mMapMatrix.postTranslate(-(mMapWidthInPx - surfaceWidth)/2, -(mMapHeightInPx - surfaceHeight)/2);
			mMapMatrix.postScale(mScale, mScale, surfaceWidth/2, surfaceHeight/2);
				
			// Set the MAX and MIN Zoom
			maxScale = maxScale*mScale;
			minScale = minScale*mScale;
			
			// totalScale is used to control that the scale is in the range {minScale,maxScale}
			totalScale = mScale;
			
			mMapInitialized = true;
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
		mMapThread.mIsRun = false;
		while (retry) {
			try {
				mMapThread.join();
				retry = false;
			} catch (InterruptedException e) {
				// it will try it again and again
			}
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event){
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
			/* First finger tap down */
			case MotionEvent.ACTION_DOWN:
				mTouchMode = TOUCH_MODE_NONE;
				mMapMatrixHold.set(mMapMatrix);
				mPointStartTouch.set(event.getX(), event.getY());
				if(anchored){
					mOldCalibPosition.x = mCurrentCalibPosition.x;
					mOldCalibPosition.y = mCurrentCalibPosition.y;
				}
				//System.out.println("ACTION_DOWN. x: " + mPointStartTouch.x + " , y: " + mPointStartTouch.y);
				break;
			/* Second finger tap down*/
			case MotionEvent.ACTION_POINTER_DOWN:
				//System.out.println("ACTION_POINTER_DOWN. numPointers: " + event.getPointerCount());
				if(event.getPointerCount()==2){
					//mPointStartTouch.set(event.getX(0), event.getY(0));
					mSecondPointStartTouch.set(event.getX(1), event.getY(1));
					mDistOfTwoFingersHold = spaceBetweenTwoFingers(event);
					//System.out.println("ACTION_POINTER_DOWN. distOfFingers: " + mDistOfTwoFingersHold + " , pointers: " + event.getPointerCount());
					if (mDistOfTwoFingersHold > 10f) {
						mMapMatrixHold.set(mMapMatrix);
						setMidPointOfTwoFingers(mPointMidOfTwoFingers, event);
						mTouchMode = TOUCH_MODE_ZOOM;
					}
				}
				break;
			case MotionEvent.ACTION_POINTER_UP:
			case MotionEvent.ACTION_UP:
				int numPointers = event.getPointerCount();
				//System.out.println("ACTION_UP. num_pointers: " + numPointers);
				if(numPointers<=2){
					if(numPointers==2){
						if (mTouchMode == TOUCH_MODE_ZOOM){
							//update totalScale
							totalScale = mScale;
							// update total rotation angle
							Log.i(TAG,"lastRotAngle: " + lastRotAngle);
							if(rotAngle!=0 && rotAngle!=lastRotAngle){
								totalRotAngle += rotAngle;
								totalRotAngle = totalRotAngle%360;
								lastRotAngle = rotAngle;
								rotEnabled = false;
							}
							else if(rotAngle!=0){
								Log.e(TAG,"Angle is the same as the last one");
							}
							Log.i(TAG,"rotAngle: " + rotAngle + " , totalRotAngle: " + totalRotAngle + " , totalScale: " + totalScale);
						}
					}
					else if (numPointers==1){
						if (mTouchMode == TOUCH_MODE_DRAG) {
							//do nothing
						}else if (mTouchMode == TOUCH_MODE_NONE && !anchored) {
							// when not anchored, considered as a click
							if(mMapType == MapType.MAP_CALIBRATION){
								Edge edge = calcClickedEdge(event);
								mCurrentCalibEdge = edge;
							}
							else if(mMapType == MapType.MAP_ANALYZER){
								mListOfHighLightPoints = null;
								Point3D point = calcClickPixel(event);
								if (point!=null) {
									mCurrentCalibPosition.x = point.x;
									mCurrentCalibPosition.y = point.y;
									mCurrentCalibPosition.setId(point.getId());
									fireOfflineCurrentPosition(mCurrentCalibPosition);
								}
							}
							else if(mMapType == MapType.MAP_NAVIGATION){
								int poiId = -1;
								Point3D mapPoint = calcClickedMapPoint(event);
								fireSelectedPoint(mapPoint);
							}
						}
					}
					rotAngle = 0;
					mTouchMode = TOUCH_MODE_HALT;
				}
				break;
			case MotionEvent.ACTION_MOVE:
				//System.out.println("ACTION_MOVE");
				
				float angle = 0;
				rotAngle = 0;
				if(event.getPointerCount()==2){
					
					mPointFinalTouch.set(event.getX(0), event.getY(0));
	                mSecondPointFinalTouch.set(event.getX(1), event.getY(1));
	                mDistOfTwoFingers = spaceBetweenTwoFingers(event);
					angle = angleBetweenLines (mPointStartTouch, mSecondPointStartTouch, mPointFinalTouch, mSecondPointFinalTouch);
					PointF centerZoomPoint = new PointF();
					if(anchored){
						centerZoomPoint.x = surfaceWidth/2;
						centerZoomPoint.y = surfaceHeight/2;
					}
					else{
						centerZoomPoint.x = mPointMidOfTwoFingers.x;
						centerZoomPoint.y = mPointMidOfTwoFingers.y;
					}
					//Log.i(TAG,"centerZoomPoint: " + centerZoomPoint.toString());
					if (mTouchMode == TOUCH_MODE_ZOOM) {
						if (mDistOfTwoFingers > 10f) {
							mMapMatrix.set(mMapMatrixHold);
							float scale = (float) Math.sqrt((mDistOfTwoFingers / mDistOfTwoFingersHold)); // sqrt here to smooth the finger movements
													
							float futureScale = totalScale*scale;
				            
				            if(totalScale<=maxScale && totalScale>=minScale){
					            if(futureScale<maxScale && futureScale>minScale) 
					            {
					            	mMapMatrix.postScale(scale, scale, centerZoomPoint.x, centerZoomPoint.y);
					            	mScale = futureScale;
					            }
					            else {
					            	//System.out.println("Only rotate");
					            	if (futureScale > maxScale){
					            		scale = maxScale/totalScale;
					            		//System.out.println("setting scale to max. Scale: " + scale);
					            		mMapMatrix.postScale(scale, scale, centerZoomPoint.x, centerZoomPoint.y);
						            	mScale = maxScale;
						            }else if (futureScale < minScale){
					            		scale = minScale/totalScale;
					            		//System.out.println("setting scale to min. Scale: " + scale);
					            		mMapMatrix.postScale(scale, scale, centerZoomPoint.x, centerZoomPoint.y);
						            	mScale = minScale;
						            }
					            }
				            }
				            else{
				            	//totalScale has gone out of range {minScale,maxScale}
				            	Log.e(TAG,"totalScale out of range");
				            	if(futureScale<maxScale && futureScale>minScale) 
					            {
				            		if((totalScale > maxScale && scale < 1) || (totalScale < minScale && scale > 1)){
				            			mMapMatrix.postScale(scale, scale, centerZoomPoint.x, centerZoomPoint.y);
				            			mScale = futureScale;
				            		}
					            }
				            }
				            
				            //Rotate the map
				            if(!rotEnabled){
				            	if(Math.abs(angle)>=MIN_ROT_ANGLE){
				            		if(angle>0){
				            			OFFSET_ROT_ANGLE = -MIN_ROT_ANGLE;
				            		}
				            		else{
				            			OFFSET_ROT_ANGLE = MIN_ROT_ANGLE;
				            		}
				            		rotEnabled = true;
					            	rotAngle = angle + OFFSET_ROT_ANGLE; 
									mMapMatrix.postRotate(-rotAngle, centerZoomPoint.x, centerZoomPoint.y);
					            }else{
					            	rotAngle = 0;
					            }
				            }
				            else{
				            	rotAngle = angle + OFFSET_ROT_ANGLE; 
				            	mMapMatrix.postRotate(-rotAngle, centerZoomPoint.x, centerZoomPoint.y);
				            }
							
				            /*
							System.out.println("totalScale: " + totalScale);
				            System.out.println("mScale: " + mScale);
				            System.out.println("futureScale: " + futureScale);
		            		System.out.println("scale: " + scale);
							*/
						}
					}
				}else if (event.getPointerCount()==1){
					
					if((Math.abs(event.getX(0)-mPointStartTouch.x)>8 || Math.abs(event.getY(0)-mPointStartTouch.y)>8)
							&& mTouchMode == TOUCH_MODE_NONE) {
						mTouchMode = TOUCH_MODE_DRAG;
					}
					
					if (mTouchMode == TOUCH_MODE_DRAG) {
						if(anchored){
							Point3D pStart = new Point3D(mPointStartTouch.x,mPointStartTouch.y,0);
							Point3D pStartT = transformScreenToMapPoint(pStart);
							Point3D pEnd = new Point3D(event.getX(0),event.getY(0),0);
							Point3D pEndT = transformScreenToMapPoint(pEnd);
							float angleLine = angleLine(pStartT, pEndT);
							//Log.i(TAG,"angleLineDegMap: " + angleLine);
							boolean moveMap = false;
							Point3D nextPointMap = new Point3D();
							for(Edge e : mCurrentEdges){
								float edgeAngle1 = e.lineAngle1;
								float edgeAngle2 = e.lineAngle2;
								//Log.i(TAG,"edge angles: " + edgeAngle1 + " , " + edgeAngle2);
								if(((angleLine<(edgeAngle1 + angleTolerance/2)) && (angleLine>(edgeAngle1 - angleTolerance/2))) || ((angleLine<(edgeAngle2 + angleTolerance/2)) && (angleLine>(edgeAngle2 - angleTolerance/2)))){
									//Log.i(TAG,"Edge found: " + e.startNode.toString() + " , " + e.endNode.toString());
									double movX = pEndT.x - pStartT.x;
									double movY = pEndT.y - pStartT.y;
									nextPointMap = calcNextPoint(e,movX,movY);
									//Log.i(TAG,"nextPointMap: " + nextPointMap.toString());
									//Log.i(TAG,"mCurrentCalibPosition: " + mCurrentCalibPosition.toString() + " , belongs: " + pointBelongsToEdge(mCurrentCalibPosition,e));
									if(!nextPointMap.equals(mCurrentCalibPosition)){
										moveMap = true;
										break;
									}
									else{
										//Log.i(TAG,"nextPoint is equal to currentCalibPosition");
									}
								}
							}
							//update current edges
							if(moveMap){
								updateCurrentEdges(nextPointMap);
								mMapMatrix.set(mMapMatrixHold);
								Point3D nextPointScreen = transformMapToScreenPoint(nextPointMap);
								mMapMatrix.postTranslate(-(float)(nextPointScreen.x - (surfaceWidth/2)), -(float)(nextPointScreen.y - (surfaceHeight/2)));
								mCurrentCalibPosition.x = nextPointMap.x;
								mCurrentCalibPosition.y = nextPointMap.y;
								fireOfflineCurrentPosition(mCurrentCalibPosition);
							}
						}
						else{
							mMapMatrix.set(mMapMatrixHold);
							mMapMatrix.postTranslate(event.getX(0) - mPointStartTouch.x, event.getY(0) - mPointStartTouch.y);
						}
					}
				}
				break;
		}
		//System.out.println("ACTION. mTouchMode: " + mTouchMode);
		/*
		System.out.println("ACTION. distFingers: " + mDistOfTwoFingers + " , distFingersHold: " + mDistOfTwoFingersHold);
		System.out.println("ACTION. angle: " + angle);
		*/
		setRefresh(true);
		return true;
	}
	
	@Override
	public void onDraw(Canvas canvas){
		
		setRefresh(false);
		
		//System.out.println("Inside onDraw method");
		canvas.setMatrix(mMapMatrix);
		
		/* reset background */
		canvas.drawColor(mMapBackgroundColor);
		
		/* draw map */
		mPaint.setAlpha(255);
		if(mBmpMap!=null){
			canvas.drawBitmap(mBmpMap, 0, 0, mPaint);
		}else{
			drawText(canvas, mPaint, "Unable to represent the map", colorText);
			Log.e(TAG,"no map detected");
		}
		
		if(mMapType == MapType.MAP_CALIBRATION){
			/* draw edges */
			for (Edge e : mEdges) {
				drawEdge(canvas, mPaint, e, 192, colorEdges, true);
			}
			/* draw calibrated edges */
			for (Edge e : mCalibEdges) {
				String color = colorCalibEdges;
				if(e.getStatus()!=0){
					color = colorCalibEdgesSent;
				}
				drawEdge(canvas, mPaint, e, 80, color, false); 
			}
			
			if(anchored){
				/* draw current point */
				drawCurrentPosition(canvas, mPaint, mCurrentCalibPosition.getPoint2D());
				/* draw current scanning path */
				int numMarkers = mMarkers.size();
				for(int i=0; i<numMarkers-1;i++){
					drawCalibrationLine(canvas, mPaint, (float)mMarkers.get(i).getPos().x, (float)mMarkers.get(i).getPos().y, (float)mMarkers.get(i+1).getPos().x, (float)mMarkers.get(i+1).getPos().y, false);
				}
				drawCalibrationLine(canvas, mPaint, (float)mMarkers.get(numMarkers-1).getPos().x, (float)mMarkers.get(numMarkers-1).getPos().y, (float)mCurrentCalibPosition.x, (float)mCurrentCalibPosition.y, true);
			}
			else{
				if(mCurrentCalibEdge != null){
					/* draw current selected edge */
					drawEdge(canvas, mPaint, mCurrentCalibEdge, 255, colorCurrentCalibEdge, false);
				}
			}
		} else if (mMapType == MapType.MAP_ANALYZER){
			/* draw edges */
			for (Edge e : mEdges) {
				drawEdge(canvas, mPaint, e, 192, colorEdges, true);
			}
			/* draw calibrated edges */
			for (Edge e : mCalibEdges) {
				String color = colorCalibEdges;
				if(e.getStatus()!=0){
					color = colorCalibEdgesSent;
				}
				drawEdge(canvas, mPaint, e, 80, color, false); 
			}
			/* draw calibrated points */
			for (CalibrationPoint cp : mCalibratedPoints) {
				int transparency = 128;
				drawCalibratedPositionWithColor(canvas, mPaint, cp.getPoint().getPoint2D(), transparency, cp.getTech());
			}
			/* draw APs */
			for (APWithPosition ap : mListOfAPsOfFloor){
				drawAP(canvas, mPaint, ap);
			}
			
			if(mListOfHighLightPoints!=null){
				/* draw highlighted points */
				for (Point p : mListOfHighLightPoints){
					drawHighlightPoints(canvas, mPaint, p);
				}
			}

			/* draw current point */
			drawCurrentPosition(canvas, mPaint, mCurrentCalibPosition.getPoint2D());
		} else if (mMapType == MapType.MAP_NAVIGATION){
			
			if(mSettingShowPaths){
				for (Edge e : mEdges) {
					drawEdge(canvas, mPaint, e, 128, colorEdges, false);
				}
				for (Point3D n : mNodes){
					drawNode(canvas, mPaint, n, -1, colorEdges);
				}
			}
			if(mSettingShowRoute>0){
				for(Point3D p : mTestingRoute){
					String color = colorPointRoute;
					if(p.getId()!=0){
						color = colorPointRoutePassed;
					}
					drawSmallPoint(canvas, mPaint, p.getPoint2D(), -2, color); // draw neighbors
				}
			}
			//LQ: draw fences
			if(mSettingShowFences && fences !=null){
				for(Fence fence : fences){
					drawFence(canvas, mPaint, fence, colorFence); // draw Fence
				}
			}
			if(mSettingShowFences && geometries!=null){
				for(Geometry geom : geometries){
					drawGeometry(canvas, mPaint, geom, colorGeoFence); // draw GeoFence
				}
			}
			//LQ: draw POIs
			if(mSettingShowPOIs) {

				if(mSettingShowStaticPOIs && !this.navigationMode) {


					if(staticPois != null) {
						for(POI poi : staticPois){
							if(poi.getPoint().z == this.mMapFloor) {
								drawPOI(canvas, mPaint, poi, colorPOI); // draw POI
							}
						}
					}
				}

				if(mSettingShowMobilePOIs && !this.navigationMode) {

					if(mobilePois != null) {
						for(POI poi: mobilePois) {
							if(poi.getPoint().z == this.mMapFloor) {
								drawPOI(canvas, mPaint, poi, colorParticle); //draw mobile POI with a different color
							}
						}
					}
				}
			}

			if(mSettingShowMobileDestination) {
				if(this.destinationMobilePOI != null && this.destinationMobilePOI.getPoint().z == mMapFloor) {
					drawPOI(canvas, mPaint, this.destinationMobilePOI, colorParticle);
				}
			}

			//show detections
			for(Point3D detPos : detectionsPos){
				drawDetection(canvas, mPaint, detPos.getPoint2D());
			}
			
			//draw route if it has been requested
			if(this.getShowRoute() && this.mRoute != null) {
				this.drawRoute(canvas, mPaint, mRoute);
			}
			
			if(destinationPoint != null && destinationPoint.z == mMapFloor) {
				mPaint.setAlpha(255);
				//canvas.drawBitmap(bitmap, (float) destinationPoint.x - bitmap.getWidth()/2, (float) destinationPoint.y - bitmap.getHeight(), mPaint);
				//drawSmallPoint(canvas, mPaint, new Point((int) destinationPoint.x, (int) destinationPoint.y), 10, this.colorPos);
				canvas.save();
				float rotation = (float)totalRotAngle + rotAngle;
				canvas.rotate(rotation, (float) destinationPoint.x, (float) destinationPoint.y);
				canvas.scale(1/mScale, 1/mScale, (float) destinationPoint.x, (float) destinationPoint.y);
				canvas.drawBitmap(bitmap, (float) destinationPoint.x - bitmap.getWidth() / 2, (float) destinationPoint.y - bitmap.getHeight(), mPaint);
				canvas.restore();
			}
			
			//----------------------------------------------------
			
			if(mSettingShowHistoric1>0){
				drawHistoric(canvas, mPaint, positionHistoric, 192, 80, colorPos);
			}
			
			if(mSettingShowListPositions>0){
				String color = "#000000";
				for (PointWeight pw : mListPositions){
					if(pw.getPoint().z!=mCurrentOnlinePosition.z){
						//Draw the list point with special color
						if(pw.getPoint().z > mCurrentOnlinePosition.z){
							// red color
							color = colorNeighborAbove;
						}else{
							// blue color
							color = colorNeighborBelow;
						}
					}
					else{
						//Draw the list point with orange color
						color = colorPos;
					}
					//Log.d(TAG,"weight: " + pw.getWeight());
					drawWeightedPoint(canvas, mPaint, pw.getPoint().getPoint2D(), pw.getWeight(), color); // draw point
				}
			}
			
			if(mSettingShowFilterParticles>0){
				//if (mCurrentParticles != null && mCurrentParticleFilterPosition != null && mCurrentParticleFilterPosition.z == mMapFloor)
				if (mCurrentParticles != null && mCurrentParticleFilterPosition != null)
				{
					for (ParticleState particle : mCurrentParticles){
						drawParticle(canvas, mPaint, particle);
					}
				}
			}
			
			if(mSettingShowNeighbors>0){
				String color = "#000000";
				for (Point3D neighbor : mCurrentNeighbors){
					if(neighbor.z!=mCurrentOnlinePosition.z){
						//Draw the neighbor point with special color
						if(neighbor.z > mCurrentOnlinePosition.z){
							// red color
							color = colorNeighborAbove;
						}else{
							// blue color
							color = colorNeighborBelow;
						}
					}
					else{
						//Draw the neighbor point with orange color
						color = colorNeighbor;
					}
					drawSmallPoint(canvas, mPaint, neighbor.getPoint2D(), -1, color); // draw neighbors
				}
			}
			
			if(mSettingShowPriorDistance>0){
				if(mLastOnlinePosition!=null){
					drawPriorDistance(canvas, mPaint, mLastOnlinePosition.getPoint2D(), mPriorDistance, colorPriorDistance); // draw neighbors
				}
			}
			
			if(mSettingShowOutput>0){
				if (mCurrentOnlinePosition.x != -1 && mCurrentOnlinePosition.y != -1 && mCurrentOnlinePosition.z == mMapFloor){
					drawBigPoint(canvas, mPaint, mCurrentOnlinePosition.getPoint2D(), mCurrentError, colorPos); // draw point
				}
			}

			if(mSettingShowOrientation){
				float orientationToShow = mOrientation;
				if(demoMode){
					orientationToShow = mUserOrientation; // show user orientation from Particle Filter if demo mode is enabled
				}
				drawOrientationArrow(canvas, mPaint, mCurrentOnlinePosition.getPoint2D(), orientationToShow, colorPos);
			}

			if(mSettingShowOrientationCorrected){
				drawOrientationArrow(canvas, mPaint, mCurrentOnlinePosition.getPoint2D(), mOrientationCorrected, colorOrientationCorrected);
			}
			
			if(mSettingShowFilterOutput>0){
				if (mCurrentParticleFilterPosition != null  && mCurrentParticleFilterPosition.z == mMapFloor)
				{
					//drawFilterPosition(canvas, mPaint, mCurrentOnlinePosition.getPoint2D(), mCurrentParticleFilterPosition.getPoint2D(), (mSettingShowOutput>0));
					//drawFilterPosition(canvas, mPaint, mCurrentOnlinePosition.getPoint2D(), mCurrentParticleFilterPosition.getPoint2D(), false); //'false' for no line drawn
					drawBigPoint(canvas, mPaint, mCurrentParticleFilterPosition.getPoint2D(), -1, colorFilter); // draw point
					if(mSettingShowOrientation) {
						String colorArrow = colorOrientationNonReliable;
						if(mUserOrientationReliability>0.8){
							colorArrow = colorFilter;
						}
						drawOrientationArrow(canvas, mPaint, mCurrentParticleFilterPosition.getPoint2D(), mUserOrientation, colorArrow);
					}
				}
			}
			
			if(mSettingShowMapFusionOutput>0){
				if (mCurrentMapFusionPosition != null && mCurrentMapFusionPosition.z == mMapFloor)
				{
					drawMapFusionPosition(canvas, mPaint, mCurrentMapFusionPosition.getPoint2D());
				}
			}
			
			/** Lines added to test RSS transformation **/
			if(mSettingShowHistoric2>0){
				drawHistoric(canvas, mPaint, position2Historic, 192, 80, colorPos2);
			}
			
			if(mSettingShowListPositions>0){
				String color = "#000000";
				for (PointWeight pw : mListPositions2){
					if(pw.getPoint().z!=mCurrentOnlinePosition2.z){
						//Draw the list point with special color
						if(pw.getPoint().z > mCurrentOnlinePosition2.z){
							// red color
							color = colorNeighborAbove2;
						}else{
							// blue color
							color = colorNeighborBelow2;
						}
					}
					else{
						//Draw the list point with orange color
						color = colorPos2;
					}
					drawWeightedPoint(canvas, mPaint, pw.getPoint().getPoint2D(), pw.getWeight(), color); // draw point
				}
			}
						
			if(mSettingShowNeighbors2>0){
				String color = "#000000";
				for (Point3D neighbor : mCurrentNeighbors2){
					if(neighbor.z!=mCurrentOnlinePosition2.z){
						//Draw the neighbor point with special color
						if(neighbor.z > mCurrentOnlinePosition2.z){
							// red color (granate)
							color = colorNeighborAbove2;
						}else{
							// blue color (dark blue)
							color = colorNeighborBelow2;
						}
					}
					else{
						//Draw the neighbor point with grey color
						color = colorNeighbor2;
					}
					drawSmallPoint(canvas, mPaint, neighbor.getPoint2D(), -1, color); // draw neighbors
				}
			}
			
			if(mSettingShowOutput2>0){
				if (mCurrentOnlinePosition2.x != -1 && mCurrentOnlinePosition2.y != -1 && mCurrentOnlinePosition2.z == mMapFloor){
					drawBigPoint(canvas, mPaint, mCurrentOnlinePosition2.getPoint2D(), mCurrentError2, colorPos2); // draw point
				}
			}
			
			
			/********************************************/
			
		}
	}

	protected void getNodesAndEdges(int floor){
		mNodes = mDBHelper.getNodesOfFloor(floor);
		mEdges = mDBHelper.getEdgesOfFloor(floor);
		
		if(debug){
			Log.i(TAG,"numNodes: " + mNodes.size());
			for(Point3D n : mNodes){
				Log.i(TAG,"node: " + n.getId() + " , " + n.toString());
			}
			Log.i(TAG,"numEdges: " + mEdges.size());
		}
		for(Edge e : mEdges){
			float angleLineDeg1 = angleLine(e.startNode, e.endNode);
			float angleLineDeg2 = angleLine(e.endNode, e.startNode);
			e.setLineAngle1(angleLineDeg1);
			e.setLineAngle2(angleLineDeg2);
			float angleAbs = Math.abs(angleLineDeg1);
			if(angleAbs != 0 && angleAbs !=90 && angleAbs!=180 && angleAbs != 270){
				double a = (e.endNode.y - e.startNode.y)/(e.endNode.x - e.startNode.x);
				double b = e.startNode.y - a*e.startNode.x;
				e.setA(a);
				e.setB(b);
			}
			if(debug){
				Log.i(TAG,"startNode: (" + e.startNode.x + "," + e.startNode.y + "," + e.startNode.z + ")");
				Log.i(TAG,"endNode: (" + e.endNode.x + "," + e.endNode.y + "," + e.endNode.z + ")");
				Log.i(TAG,"(dx,dy): (" + e.dX + "," + e.dY + ")");
				Log.i(TAG,"angleLineDeg1: " + angleLineDeg1);
				Log.i(TAG,"angleLineDeg2: " + angleLineDeg2);
			}
		}
	}
	
	/*
	public void getCalibNodesAndEdges(int floor){
		mCalibNodes = mDBHelper.getCalibratedNodesOfFloor(floor);
		mCalibEdges = mDBHelper.getCalibratedEdgesOfFloor(floor);
		
		if(debug){
			Log.i(TAG,"numCalibNodes: " + mCalibNodes.size());
			for(Point3D n : mCalibNodes){
				Log.i(TAG,"node: (" + n.toString() + ")");
			}
			Log.i(TAG,"numCalibEdges: " + mCalibEdges.size());
		}
		
		for(Edge e : mCalibEdges){
			float angleLineDeg1 = angleLine(e.startNode, e.endNode);
			float angleLineDeg2 = angleLine(e.endNode, e.startNode);
			e.setLineAngle1(angleLineDeg1);
			e.setLineAngle2(angleLineDeg2);
			float angleAbs = Math.abs(angleLineDeg1);
			if(angleAbs != 0 && angleAbs !=90 && angleAbs!=180 && angleAbs != 270){
				double a = (e.endNode.y - e.startNode.y)/(e.endNode.x - e.startNode.x);
				double b = e.startNode.y - a*e.startNode.x;
				e.setA(a);
				e.setB(b);
			}
			if(debug){
				Log.i(TAG,"startNode: (" + e.startNode.x + "," + e.startNode.y + "," + e.startNode.z + ")");
				Log.i(TAG,"endNode: (" + e.endNode.x + "," + e.endNode.y + "," + e.endNode.z + ")");
				Log.i(TAG,"(dx,dy): (" + e.dX + "," + e.dY + ")");
				Log.i(TAG,"angleLineDeg1: " + angleLineDeg1);
				Log.i(TAG,"angleLineDeg2: " + angleLineDeg2);
			}
		}
	}
	*/
	
	public synchronized void setRoute(LinkedList<Point3D> route) {
		this.mRoute = route;
	}
	
	public synchronized LinkedList<Point3D> getRoute() {
		return this.mRoute;
	}
	
	public synchronized boolean getShowRoute() {
		return this.showRoute;
	}
	
	public synchronized void setShowRoute(boolean draw) {
		this.showRoute = draw;
	}
	
	public void setDestinationPoint(Point3D point) {
		this.destinationPoint = point;
	}
	
	protected void getAPsOfFloor(int floor){
		mListOfAPsOfFloor = mDBHelper.getAPsOfFloor(floor);
	}
	
	/*
	public void getCalibratedPoints(int floor){
		mCalibratedPoints = mDBHelper.getCalibratedPoints(floor);
	}
    */
	
	protected void updateCurrentEdges(Point3D p){
		mCurrentEdges = new ArrayList<Edge>();
		for(Edge edge : mEdges){
			boolean belongs = pointBelongsToEdge(p,edge);
			if(belongs){
				mCurrentEdges.add(edge);
			}
		}
		
		if(mCurrentEdges.size()>1){
			//Calculate the intersection
			Point3D intersection = calcIntersection(mCurrentEdges.get(0), mCurrentEdges.get(1));
			p.x = intersection.x;
			p.y = intersection.y;
			// recalculate the current edges with the new point
			mCurrentEdges = new ArrayList<Edge>();
			for(Edge edge : mEdges){
				boolean belongs = pointBelongsToEdge(p,edge);
				if(belongs){
					mCurrentEdges.add(edge);
				}
			}
		}
		/*
		Log.i(TAG,"Current edges:");
		int i = 1;
		for(Edge edge : mCurrentEdges){
			Log.i(TAG,"Edge " + i +": " + edge.startNode.toString() + " , " + edge.endNode.toString());
			i++;
		}
		*/
	}
	
	private ArrayList<Integer> getCurrentEdgesId(){
		ArrayList<Integer> currentEdgesId = new ArrayList<Integer>();
		for(Edge edge : mCurrentEdges){
			currentEdgesId.add(edge.getEdgeId());
		}
		return currentEdgesId;
	}
	
	public PointWithCalibrationItems getCalibrationsOfPoint(Point p){
		for (PointWithCalibrationItems cop : mListOfPointsWithCalibrations){
			if (cop.getPoint().x==p.x && cop.getPoint().y==p.y){
				return cop;
			}
		}
		return null;
	}
	
	public boolean isAPLocation(Point3D p){
		for (APWithPosition ap : mListOfAPsOfFloor){
			if (ap.getPoint().x==p.x && ap.getPoint().y==p.y && ap.getPoint().z==p.z){
				return true;
			}
		}
		
		return false;
	}
	
	/*
	public void highlightAPReachables(Point3D pAP, int floor){
		String bssid = mDBHelper.getAPBssidFromLocation(pAP);
		
		if(bssid!=null){
			mListOfHighLightPoints = mDBHelper.getAPReachablePoints(bssid, floor, -90f);
			/*
			for(Point p : mListOfHighLightPoints){
				Log.i(TAG,"highlight point: (" + p.x + "," + p.y + ")");
			}
			*//*
		}
		else{
			Log.i(TAG,"bssid is null. pointAP: " + pAP.toString());
		}
		
	}
	*/
	
	public synchronized boolean needsRefresh() {
		return refresh;
	}

	public synchronized void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}
	
	public void setShowKnnNeighbors(boolean showNeighbors) {
		if(showNeighbors){
			mSettingShowNeighbors = 1;
		}
		else{
			mSettingShowNeighbors = 0;
		}
	}

	public void setShowOrientation(boolean showOrientation) {
		mSettingShowOrientation = showOrientation;
	}
	
	public boolean isAnchored(){
		return anchored;
	}

	public boolean isCurrentCalibEdgeSelected(){
		return (mCurrentCalibEdge != null);
	}
	
	public Edge getCurrentCalibEdgeSelected(){
		return mCurrentCalibEdge;
	}
	
	public void clearCurrentCalibEdgeSelected(){
		mCurrentCalibEdge = null;
	}
	
	public void increaseMarker(int floor){
		// store time and position of the marker
		long ts = System.currentTimeMillis();
		Point3D currentPoint = new Point3D(mCurrentCalibPosition.x,mCurrentCalibPosition.y,floor);
		ArrayList<Integer> currentEdgesId = getCurrentEdgesId();
		mMarkers.add(new Marker(ts,currentPoint,currentEdgesId));
	}
	
	public void resetMarker(){
		mMarkers.clear();
	}
	
	public ArrayList<Marker> getMarkers(){
		return mMarkers;
	}
	
	public Point3D getCurrentPosition(){
		return mCurrentCalibPosition;
	}

	/*
	public void updateOnlineLocation(Point3D pointKnn, int errorRadius){
		mCurrentOnlinePosition = pointKnn;
		//mCurrentFilteredPosition = pointFiltered;
		//mCurrentNeighbors = neighbors;
		mCurrentError = errorRadius;
		//mCurrentParticles = particles;
		//System.out.println("inside updateOnlineLocation");
	}
	*/
	
	/*
	public void updateOnlineLocation(Point pointKnn, Point pointFiltered, int errorRadius, ArrayList<Point> neighbors, ArrayList<ParticleState> particles){
		mCurrentOnlinePosition = pointKnn;
		mCurrentFilteredPosition = pointFiltered;
		mCurrentNeighbors = neighbors;
		mCurrentError = errorRadius;
		mCurrentParticles = particles;
	}
	*/
	
	public void changeFloor(int floor){
		getNodesAndEdges(floor);
		mMapFloor = floor;
		/*
		getCalibNodesAndEdges(floor);
		if(mMapType == MapType.MAP_CALIBRATION){
			// do nothing
		}
		else if(mMapType == MapType.MAP_ANALYZER){
			getCalibratedPoints(floor);
			getAPsOfFloor(floor);
		}
		*/
	}
	
	public void changeFloorMap(Bitmap bmp){
		if(mBmpMap!=null){
			mBmpMap.recycle();
		}
		mBmpMap = bmp;
	}
		
	public synchronized void addOfflineCurrentPositionListener(MapEventListener listener){
		mOfflineCurrentPositionListener.add(listener);
	}

	private synchronized void fireOfflineCurrentPosition(Point3D point3D){
		for (MapEventListener listener : mOfflineCurrentPositionListener ) {
			listener.updateOfflineCurrentPosition(point3D);
		}
	}
	
	public synchronized void addSelectedPointListener(PointSelectedEventListener listener){
		mPointSelectedListener.add(listener);
	}

	private synchronized void fireSelectedPoint(Point3D point){
		for (PointSelectedEventListener listener : mPointSelectedListener ) {
			listener.newPointSelected(point);
		}
	}
	
	public void setSelectedFenceId(int id){
		selectedFenceId = id;
	}

	/** calculate space between two fingers **/
	private float spaceBetweenTwoFingers(MotionEvent event){
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return (float) Math.sqrt(x * x + y * y);
	}

	/** set the mid point of two fingers **/
	private void setMidPointOfTwoFingers(PointF point, MotionEvent event) {
		float x = event.getX(0) + event.getX(1);
		float y = event.getY(0) + event.getY(1);
		point.set(x / 2, y / 2);
	}
	
	/** Calculate the rotation angle **/
	private float angleBetweenLines (PointF firstStart, PointF secondStart, PointF firstFinal, PointF secondFinal) {
        float angle1 = (float) Math.atan2( (secondStart.y - firstStart.y), (secondStart.x - firstStart.x) );
        float angle2 = (float) Math.atan2( (secondFinal.y - firstFinal.y), (secondFinal.x - firstFinal.x) );

        float angle = ((float)Math.toDegrees(angle1 - angle2)) % 360;
        if (angle < -180.f) angle += 360.0f;
        if (angle > 180.f) angle -= 360.0f;
        return angle;
    }
	
	/** Calculate the angle of a line **/
	public float angleLine (Point3D start, Point3D end) {
        float angle = (float) Math.atan2( (end.y - start.y), (end.x - start.x) );
        
        float angleDeg = ((float)Math.toDegrees(angle)) % 360;
        return angleDeg;
    }
	
	/** calculate distance between two points **/
	public float distanceBetweenPoints(Point3D a, Point3D b){
		float x = (float) (a.x - b.x);
		float y = (float) (a.y - b.y);
		return (float) Math.sqrt(x*x + y*y);
	}
	
	/** calculate distance between clicked point and an edge **/
	private double distanceBetweenClickedPointAndEdge(Point3D p, Edge e){
		double dist = -1;
		float angleLineDeg = e.getLineAngle1();
		float angleAbs = Math.abs(angleLineDeg);
		double minX = Math.min(e.startNode.x, e.endNode.x);
		double maxX = Math.max(e.startNode.x, e.endNode.x);
		double minY = Math.min(e.startNode.y, e.endNode.y);
		double maxY = Math.max(e.startNode.y, e.endNode.y);
		//Log.i(TAG,"angleLineDeg: " + angleLineDeg);
		if(angleAbs != 0 && angleAbs !=90 && angleAbs!=180 && angleAbs != 270){
			double a = e.getA();
			double b = e.getB();
			//Log.i(TAG,"a: " + a + " , b: " + b);
			//Log.i(TAG,"equation_res: " + (p.y -a*p.x - b));
			if(p.y<=maxY && p.y>=minY && p.x<=maxX && p.x>=minX){
				dist = Math.abs(p.y -a*p.x - b)/Math.sqrt(1+a*a);
			}
		}
		else if(angleAbs == 90 || angleAbs == 270){
			if(p.y<=maxY && p.y>=minY){
				dist = Math.abs(p.x-e.startNode.x);
			}
		}
		else if(angleAbs == 0 || angleAbs == 180){
			if(p.x<=maxX && p.x>=minX){
				dist = Math.abs(p.y-e.startNode.y);
			}
		}
		return dist;
	}
	
	/** Convert screen point to map point **/
	private Point3D transformScreenToMapPoint(Point3D p){
		Point3D pt = new Point3D();
		float[] matrixValue = new float[9];
		mMapMatrix.getValues(matrixValue);
		
		double trans_x = p.x - matrixValue[Matrix.MTRANS_X];
		double trans_y = p.y - matrixValue[Matrix.MTRANS_Y];
		double rot_x = trans_x*Math.cos(-totalRotAngle*Math.PI/180) + trans_y*Math.sin(-totalRotAngle*Math.PI/180);
		double rot_y = trans_y*Math.cos(-totalRotAngle*Math.PI/180) - trans_x*Math.sin(-totalRotAngle*Math.PI/180);
		double scaled_x = rot_x/totalScale;
		double scaled_y = rot_y/totalScale;
				
		pt.x = scaled_x;
		pt.y = scaled_y;
		
		return pt;
	}
	
	/** Convert map point to screen point **/
	private Point3D transformMapToScreenPoint(Point3D p){
		Point3D put = new Point3D();
		float[] matrixValue = new float[9];
		mMapMatrix.getValues(matrixValue);
		
		double scaled_x = p.x*totalScale;
		double scaled_y = p.y*totalScale;
		double rot_x = scaled_x*Math.cos(-totalRotAngle*Math.PI/180) - scaled_y*Math.sin(-totalRotAngle*Math.PI/180);
		double rot_y = scaled_y*Math.cos(-totalRotAngle*Math.PI/180) + scaled_x*Math.sin(-totalRotAngle*Math.PI/180);
		double trans_x = rot_x + matrixValue[Matrix.MTRANS_X];
		double trans_y = rot_y + matrixValue[Matrix.MTRANS_Y];
						
		put.x = trans_x;
		put.y = trans_y;
		
		return put;
	}
	
	/**
	 * Function convert clicked pixel to map coordination
	 * @param event - MotionEvent
	 * @return Point
	 */
	private Point3D calcClickPixel(MotionEvent event){
		Point3D clickedPoint = new Point3D();
		Point3D p = new Point3D(event.getX(),event.getY(),0);
		Point3D pt = transformScreenToMapPoint(p);
		
		/* snap the point to a calibrated point, if there is one very close */
		for (CalibrationPoint cp : mCalibratedPoints){
			if (Math.sqrt(Math.pow((pt.x-cp.getPoint().x), 2)+Math.pow((pt.y-cp.getPoint().y), 2))<10){
				clickedPoint.x = (int) cp.getPoint().x;
				clickedPoint.y = (int) cp.getPoint().y;
				clickedPoint.setId(cp.getId());
				return clickedPoint;
			}
		}
		
		/* snap the point to an AP point, if there is one very close */
		for (APWithPosition ap : mListOfAPs){
			if (Math.sqrt(Math.pow((pt.x-ap.getPoint().x), 2)+Math.pow((pt.y-ap.getPoint().y), 2))<10){
				clickedPoint.x = ap.getPoint().x;
				clickedPoint.y = ap.getPoint().y;
				return clickedPoint;
			}
		}
		
		clickedPoint.x = (int) pt.x;
		clickedPoint.y = (int) pt.y;
		return clickedPoint;
	}
	
	/**
	 * Function convert clicked pixel to closest edge
	 * @param event - MotionEvent
	 * @return Edge
	 */
	private Edge calcClickedEdge(MotionEvent event){
		Point3D p = new Point3D(event.getX(),event.getY(),0);
		Point3D pt = transformScreenToMapPoint(p);
		double minDist = 10000;
		Edge selectedEdge = null;
		/* snap the point to a calibrated edge, if there is one very close */
		for (Edge e : mCalibEdges){
			if(e.getStatus()==0){
				double dist = distanceBetweenClickedPointAndEdge(pt,e);
				if (dist!=-1){
					if(dist<minDist){
						minDist = dist;
						selectedEdge = e;
					}
				}
			}
		}
		return selectedEdge;
	}
	
	/**
	 * Function convert clicked pixel to map coordination
	 * @param event - MotionEvent
	 * @return Point3D
	 */
	private Point3D calcClickedMapPoint(MotionEvent event){
		Point3D p = new Point3D(event.getX(),event.getY(),0);
		Point3D pt = transformScreenToMapPoint(p);
		pt.z = mMapFloor;				
		return pt;
	}
	
	/**
	 * Function that calculates the next point when the marker is dragged
	 * @param e - Edge
	 * @param movX - Movement in x direction
	 * @param movY - Movement in y direction
	 * @return Point3D - The calculated point
	 */
	private Point3D calcNextPoint(Edge e, double movX, double movY){
		Point3D nextPoint = new Point3D();
		double minX = Math.min(e.startNode.x, e.endNode.x);
		double maxX = Math.max(e.startNode.x, e.endNode.x);
		double minY = Math.min(e.startNode.y, e.endNode.y);
		double maxY = Math.max(e.startNode.y, e.endNode.y);
		boolean xAdjusted = false;
		boolean yAdjusted = false;
		double nextX = 0;
		double nextY = 0;
		float edgeAngle1 = e.lineAngle1;
		if(((edgeAngle1<(90 + angleTolerance/4)) && (edgeAngle1>(90 - angleTolerance/4))) || ((edgeAngle1<(270 + angleTolerance/4)) && (edgeAngle1>(270 - angleTolerance/4)))){
			// vertical line
			nextY = mOldCalibPosition.y + movY;
			// adjust y coordinate
			if(nextY>maxY){
				nextY = maxY;
				yAdjusted = true;
			}else if(nextY<minY){
				nextY = minY;
				yAdjusted = true;
			}
			if(yAdjusted){
				if(nextY==e.startNode.y){
					nextX = e.startNode.x;
				}else if(nextY==e.endNode.y){
					nextX = e.endNode.x;
				}
			}else{
				if(e.dX==0){
					nextX = e.startNode.x;
				}
				else{
					nextX = (nextY - e.b)/e.a;
				}
			}
		}
		else if(((edgeAngle1<(0 + angleTolerance/4)) && (edgeAngle1>(0 - angleTolerance/4))) || ((edgeAngle1<(180 + angleTolerance/4)) && (edgeAngle1>(180 - angleTolerance/4)))){
			// horizontal line
			nextX = mOldCalibPosition.x + movX;
			// adjust x coordinate
			if(nextX>maxX){
				nextX = maxX;
				xAdjusted = true;
			}else if(nextX<minX){
				nextX = minX;
				xAdjusted = true;
			}
			if(xAdjusted){
				if(nextX==e.startNode.x){
					nextY = e.startNode.y;
				}else if(nextX==e.endNode.x){
					nextY = e.endNode.y;
				}
			}else{
				if(e.dY==0){
					nextY = e.startNode.y;
				}
				else{
					nextY = e.a*nextX + e.b;
				}
			}
		}
		else{
			// not vertical nor horizontal line
			if(Math.abs(movX)>=Math.abs(movY)){
				nextX = mOldCalibPosition.x + movX;
				// adjust x coordinate
				if(nextX>maxX){
					nextX = maxX;
					xAdjusted = true;
				}else if(nextX<minX){
					nextX = minX;
					xAdjusted = true;
				}
				if(xAdjusted){
					if(nextX==e.startNode.x){
						nextY = e.startNode.y;
					}else if(nextX==e.endNode.x){
						nextY = e.endNode.y;
					}
				}else{
					nextY = e.a*nextX + e.b;
				}
			}
			else{
				nextY = mOldCalibPosition.y + movY;
				// adjust y coordinate
				if(nextY>maxY){
					nextY = maxY;
					yAdjusted = true;
				}else if(nextY<minY){
					nextY = minY;
					yAdjusted = true;
				}
				if(yAdjusted){
					if(nextY==e.startNode.y){
						nextX = e.startNode.x;
					}else if(nextY==e.endNode.y){
						nextX = e.endNode.x;
					}
				}else{
					nextX = (nextY - e.b)/e.a;
				}
			}
		}
		nextPoint.x = nextX;
		nextPoint.y = nextY;
		return nextPoint;
	}

	/** Function to Zoom-in the map and cells */
	public void zoomIn(){
		mMapMatrix.postScale(mZoomScale, mZoomScale);
	}
	/** Function to Zoom-out the map and cells */
	public void zoomOut(){
		mMapMatrix.postScale(1/mZoomScale, 1/mZoomScale);
	}

	public void centerPositionToNode(){
		if(anchored){
			mCurrentCalibPosition.x = 0;
			mCurrentCalibPosition.y = 0;
			fireOfflineCurrentPosition(mCurrentCalibPosition);
			anchored = false;
		}
		else{
			Point3D p = new Point3D(surfaceWidth/2,surfaceHeight/2,0);
			Point3D pt = transformScreenToMapPoint(p);
			// Obtain the nearest node
			float minDist = 10000;
			Point3D selectedNode = new Point3D(0,0,0);
			for(Point3D node : mNodes){
				float dist = distanceBetweenPoints(node,pt);
				if(dist<minDist){
					minDist = dist;
					selectedNode = node;
				}
			}
			if(minDist!=10000){
				Point3D selectedNodeScreen = transformMapToScreenPoint(selectedNode);
				updateCurrentEdges(selectedNode);
				
				// Center the selected node in the middle of the surface
				mMapMatrix.postTranslate((float)(p.x - selectedNodeScreen.x), (float)(p.y - selectedNodeScreen.y));
				
				mCurrentCalibPosition.x = selectedNode.x;
				mCurrentCalibPosition.y = selectedNode.y;
				fireOfflineCurrentPosition(mCurrentCalibPosition);
				anchored = true;
			}
		}
	}
	
	protected void moveCurrentCalibPositionToCenter(){
		Point3D currentCalibScreen = transformMapToScreenPoint(mCurrentCalibPosition);
		mMapMatrix.postTranslate(-(float) (currentCalibScreen.x - (surfaceWidth / 2)), -(float) (currentCalibScreen.y - (surfaceHeight / 2)));
	}
	
	public boolean pointBelongsToEdge(Point3D p, Edge e){
		boolean belongs = false;
		float angleLineDeg = e.getLineAngle1();
		float angleAbs = Math.abs(angleLineDeg);
		double minX = Math.min(e.startNode.x, e.endNode.x);
		double maxX = Math.max(e.startNode.x, e.endNode.x);
		double minY = Math.min(e.startNode.y, e.endNode.y);
		double maxY = Math.max(e.startNode.y, e.endNode.y);
		//Log.i(TAG,"angleLineDeg: " + angleLineDeg);
		if(angleAbs != 0 && angleAbs !=90 && angleAbs!=180 && angleAbs != 270){
			double a = e.getA();
			double b = e.getB();
			//Log.i(TAG,"a: " + a + " , b: " + b);
			//Log.i(TAG,"equation_res: " + (p.y -a*p.x - b));
			if(Math.abs(p.y -a*p.x - b)<=lineTolerance && p.y<=maxY && p.y>=minY && p.x<=maxX && p.x>=minX){
				belongs = true;
			}
		}
		else if(angleAbs == 90 || angleAbs == 270){
			//Log.i(TAG,"equation_res: " + (p.x - e.startNode.x));
			if(Math.abs(p.x - e.startNode.x)<=lineTolerance && p.y<=maxY && p.y>=minY){
				belongs = true;
			}
		}
		else if(angleAbs == 0 || angleAbs == 180){
			//Log.i(TAG,"equation_res: " + (p.y - e.startNode.y));
			if(Math.abs(p.y - e.startNode.y)<=lineTolerance && p.x<=maxX && p.x>=minX){
				belongs = true;
			}
		}
		return belongs;
	}
	
	public boolean pointBelongsToPath(Point3D p){
		boolean belongs = false;
		for(Edge e : mEdges){
			boolean belongsToEdge = pointBelongsToEdge(p,e);
			if(belongsToEdge){
				belongs = true;
				break;
			}
		}
		return belongs;
	}
	
	private Point3D calcIntersection(Edge e1, Edge e2){
		Point3D p = new Point3D(-1,-1,-1);
		double x = -1;
		double y = -1;
		float angleLineDeg1 = e1.getLineAngle1();
		float angleAbs1 = Math.abs(angleLineDeg1);
		float angleLineDeg2 = e2.getLineAngle1();
		float angleAbs2 = Math.abs(angleLineDeg2);
		if(angleAbs1 != 0 && angleAbs1 !=90 && angleAbs1!=180 && angleAbs1 != 270){
			double a1 = e1.getA();
			double b1 = e1.getB();
			if(angleAbs2 != 0 && angleAbs2 !=90 && angleAbs2!=180 && angleAbs2 != 270){
				double a2 = e2.getA();
				double b2 = e2.getB();
				x = (b2 - b1)/(a1 - a2);
				y = a2*x + b2;
			}
			else if(angleAbs2 == 90 || angleAbs2 == 270){
				x = e2.startNode.x;
				y = a1*x + b1;
			}
			else if(angleAbs2 == 0 || angleAbs2 == 180){
				y = e2.startNode.y;
				x = (y - b1)/a1;
			}
		}
		else if(angleAbs1 == 90 || angleAbs1 == 270){
			x = e1.startNode.x;
			if(angleAbs2 != 0 && angleAbs2 !=90 && angleAbs2!=180 && angleAbs2 != 270){
				double a2 = e2.getA();
				double b2 = e2.getB();
				y = a2*x + b2;
			}
			else if(angleAbs2 == 0 || angleAbs2 == 180){
				y = e2.startNode.y;
			}
		}
		else if(angleAbs1 == 0 || angleAbs1 == 180){
			y = e1.startNode.y;
			if(angleAbs2 != 0 && angleAbs2 !=90 && angleAbs2!=180 && angleAbs2 != 270){
				double a2 = e2.getA();
				double b2 = e2.getB();
				x = (y - b2)/a2;
			}
			else if(angleAbs2 == 90 || angleAbs2 == 270){
				x = e2.startNode.x;
			}
		}
		p.x = x;
		p.y = y;
		return p;
	}
	
	/*
	public synchronized void drawUserPosition(MyLocationData location){
		//System.out.println("Inside drawUserPosition");
		float error = 30;
		if (location.candidatesLocal.size()>0){
			float sum_dist = 0;
			int i = 0;
			Point3D cand0 = new Point3D((int) location.candidatesLocal.get(0)[0],(int) location.candidatesLocal.get(0)[1], (int) location.candidatesLocal.get(0)[2]);
			if(mSettingShowKnnNeighbors>0){
				mCurrentNeighbors.clear();
			}
			for(double[] cand : location.candidatesLocal){
				Point3D candPoint = new Point3D((int) cand[0], (int) cand[1], (int) cand[2]);
				if(mSettingShowKnnNeighbors>0){
					mCurrentNeighbors.add(candPoint);
				}
				if(variableError){
					if(i==0){
						i=1;
					}else{
						sum_dist = sum_dist + distanceBetweenPoints(candPoint, cand0);
					}
				}
			}
			if(variableError){
				error = sum_dist/(location.candidatesLocal.size()-1);
				error = error/2;
				//System.out.println("ERROR: " + error);
			}
		}
		Point3D point = new Point3D((int) location.positionLocal[0], (int) location.positionLocal[1], (int) location.positionLocal[2]);
		updateOnlineLocation(point, (int) error);
		setRefresh(true);
	}
	*/
	
	private void drawHistoric(Canvas canvas, Paint paint, ArrayList<Point3D> points, int pointTransparency, int lineTransparency, String color) {
		float lineWidth = Math.min(mMapWidthInPx/300,mMapHeightInPx/300);
		int dotSize = (int) Math.min(mMapWidthInPx/100,mMapHeightInPx/100);
		
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.parseColor(color));				/* a blue color for the edge*/
		paint.setStrokeWidth(lineWidth);
		paint.setPathEffect(new DashPathEffect(new float[]{20, 5}, 0));
		for(int i=0; i<points.size()-1;i++){
			if(points.get(i).z == mMapFloor && points.get(i+1).z == mMapFloor){
				paint.setAlpha(pointTransparency);										/* set to no transparency while drawing points */
				canvas.drawCircle((float)points.get(i).x, (float)points.get(i).y, dotSize/2f, paint);
				paint.setAlpha(lineTransparency);										/* set to no transparency while drawing points */
				canvas.drawLine((float)points.get(i).x, (float)points.get(i).y, (float)points.get(i+1).x, (float)points.get(i+1).y, paint);
			}
		}
		
	}
	
	public synchronized void drawUserPosition(ArrayList<LocationData> locations){
		//System.out.println("Inside drawUserPosition");
		float error = 30;
		if (locations.get(0).candidatesLocal.size()>0){
			float sum_dist = 0;
			int i = 0;
			Point3D cand0 = new Point3D((int) locations.get(0).candidatesLocal.get(0)[0],(int) locations.get(0).candidatesLocal.get(0)[1], (int) locations.get(0).candidatesLocal.get(0)[2]);
			if(mSettingShowNeighbors>0){
				mCurrentNeighbors.clear();
			}
			for(double[] cand : locations.get(0).candidatesLocal){
				Point3D candPoint = new Point3D((int) cand[0], (int) cand[1], (int) cand[2]);
				if(mSettingShowNeighbors>0){
					mCurrentNeighbors.add(candPoint);
				}
				if(variableError){
					if(i==0){
						i=1;
					}else{
						sum_dist = sum_dist + distanceBetweenPoints(candPoint, cand0);
					}
				}
			}
			if(variableError){
				error = sum_dist/(locations.get(0).candidatesLocal.size()-1);
				error = error/2;
				//System.out.println("ERROR: " + error);
			}
		}
		if(mSettingShowListPositions>0){
			if (locations.get(0).listPositions!=null && locations.get(0).listPositions.size()>0){
				mListPositions.clear();
				for(double[] pos : locations.get(0).listPositions){
					PointWeight listPoint = new PointWeight(new Point3DWithFloor(pos[0], pos[1], pos[2], pos[2]), pos[3]);
					mListPositions.add(listPoint);
				}
			}
		}
		Point3D point = new Point3D((int) locations.get(0).positionLocal[0], (int) locations.get(0).positionLocal[1], (int) Math.round(locations.get(0).positionLocal[2]));

		if(locations.get(0).accuracy != -1){
			error = (float) locations.get(0).accuracy;
		}
	
		//updateOnlineLocation(point, point_particle_filter, point_map_fusion, (int) error);
		mCurrentOnlinePosition = point;		
		mCurrentError = (int) error;
		mOrientation = (float) locations.get(0).relativeOrientation;
		mUserOrientation = (float) locations.get(0).userOrientation;
		mUserOrientationReliability = (float) locations.get(0).userOrientationReliability;
		//mOrientationCorrected = mOrientation + (float)locations.get(0).absoluteOrientation;
		mOrientationCorrected = (float)locations.get(0).absoluteOrientation;

		if(mSettingShowHistoric1>0){
			positionHistoric.add(point);
		}
		
		if(locations.size() > 2){
			if(locations.get(2) != null)
			{
				Point3D point_particle_filter = new Point3D((int) locations.get(2).positionLocal[0], (int) locations.get(2).positionLocal[1], (int) locations.get(2).positionLocal[2]);
				mCurrentParticleFilterPosition = point_particle_filter;
				mCurrentParticles = locations.get(2).filterParticles;
				mFilterIsResampled = locations.get(2).filterIsResampled;
			}
			
			if(locations.get(3) != null)
			{
				Point3D point_map_fusion = new Point3D((int) locations.get(3).positionLocal[0], (int) locations.get(3).positionLocal[1], (int) locations.get(3).positionLocal[2]);
				mCurrentMapFusionPosition = point_map_fusion;
			}
		}
		
		if(locations.get(0).lastPositionLocal != null){
			//Log.i(TAG,"lastPositionLocal: ("+locations.get(0).lastPositionLocal[0] + ")");
			mLastOnlinePosition = new Point3D((int) locations.get(0).lastPositionLocal[0], (int) locations.get(0).lastPositionLocal[1], (int) locations.get(0).lastPositionLocal[2]);
			mPriorDistance = locations.get(0).priorDistance;
		}
		else{
			//Log.i(TAG,"lastPositionLocal is null");
		}
		
		setRefresh(true);
	}
	
	public synchronized void drawTwoUserPosition(ArrayList<LocationData> locations){
		//System.out.println("Inside drawUserPosition");
		float error = 30;
		if (locations.get(0).candidatesLocal.size()>0){
			float sum_dist = 0;
			int i = 0;
			Point3D cand0 = new Point3D((int) locations.get(0).candidatesLocal.get(0)[0],(int) locations.get(0).candidatesLocal.get(0)[1], (int) locations.get(0).candidatesLocal.get(0)[2]);
			if(mSettingShowNeighbors>0){
				mCurrentNeighbors.clear();
			}
			for(double[] cand : locations.get(0).candidatesLocal){
				Point3D candPoint = new Point3D((int) cand[0], (int) cand[1], (int) cand[2]);
				if(mSettingShowNeighbors>0){
					mCurrentNeighbors.add(candPoint);
				}
				if(variableError){
					if(i==0){
						i=1;
					}else{
						sum_dist = sum_dist + distanceBetweenPoints(candPoint, cand0);
					}
				}
			}
			if(variableError){
				error = sum_dist/(locations.get(0).candidatesLocal.size()-1);
				error = error/2;
				//System.out.println("ERROR: " + error);
			}
		}
		if(mSettingShowListPositions>0){
			if (locations.get(0).listPositions!=null && locations.get(0).listPositions.size()>0){
				mListPositions.clear();
				for(double[] pos : locations.get(0).listPositions){
					PointWeight listPoint = new PointWeight(new Point3DWithFloor(pos[0], pos[1], pos[2], pos[2]), pos[3]);
					mListPositions.add(listPoint);
				}
			}
		}
		Point3D point = new Point3D((int) locations.get(0).positionLocal[0], (int) locations.get(0).positionLocal[1], (int) locations.get(0).positionLocal[2]);
		if(mSettingShowHistoric1>0){
			positionHistoric.add(point);
		}
		
		float error2 = 30;
		if (locations.get(1).candidatesLocal.size()>0){
			float sum_dist = 0;
			int i = 0;
			Point3D cand0 = new Point3D((int) locations.get(1).candidatesLocal.get(0)[0],(int) locations.get(1).candidatesLocal.get(0)[1], (int) locations.get(1).candidatesLocal.get(0)[2]);
			if(mSettingShowNeighbors2>0){
				mCurrentNeighbors2.clear();
			}
			for(double[] cand : locations.get(1).candidatesLocal){
				Point3D candPoint = new Point3D((int) cand[0], (int) cand[1], (int) cand[2]);
				if(mSettingShowNeighbors2>0){
					mCurrentNeighbors2.add(candPoint);
				}
				if(variableError){
					if(i==0){
						i=1;
					}else{
						sum_dist = sum_dist + distanceBetweenPoints(candPoint, cand0);
					}
				}
			}
			if(variableError){
				error2 = sum_dist/(locations.get(1).candidatesLocal.size()-1);
				error2 = error2/2;
				//System.out.println("ERROR 2: " + error2);
			}
		}
		if(mSettingShowListPositions>0){
			if (locations.get(1).listPositions!=null && locations.get(1).listPositions.size()>0){
				mListPositions2.clear();
				for(double[] pos : locations.get(1).listPositions){
					PointWeight listPoint = new PointWeight(new Point3DWithFloor(pos[0], pos[1], pos[2], pos[2]), pos[3]);
					mListPositions2.add(listPoint);
				}
			}
		}
		Point3D point2 = new Point3D((int) locations.get(1).positionLocal[0], (int) locations.get(1).positionLocal[1], (int) locations.get(1).positionLocal[2]);
		if(mSettingShowHistoric2>0){
			position2Historic.add(point2);
		}
		
		//updateOnlineLocation(point, (int) error);
		mCurrentOnlinePosition = point;
		mCurrentError = (int) error;
		mCurrentOnlinePosition2 = point2;
		mCurrentError2 = (int) error2;
		
		setRefresh(true);
	}
	
	public void drawDetections(ArrayList<Point3D> detections, int floor){
		detectionsPos.clear();
		for(Point3D det : detections){
			detectionsPos.add(det);
		}
		setRefresh(true);
	}
	
	public void drawFences(ArrayList<Fence> fences, int floor){
		this.fences.clear();
		if(fences !=null){
			for(Fence fence : fences){
				if(fence.getPoint().z==floor){
					this.fences.add(fence);
				}
			}
		}
		setRefresh(true);
	}
	
	public void drawGeoFences(ArrayList<GeoFence> geoFences, int floor){
		if(debug)	Log.d(TAG,"inside drawGeoFences. geoFences: " + geoFences);
		this.geometries.clear();
		if(geoFences!=null){
			for(GeoFence fence : geoFences){
				ArrayList<Geometry> geoms = fence.getGeometries();
				if(geoms!=null){
					for (Geometry geom : geoms){
						if(geom.getFloor()==floor){
							if(fence.isMultiple()){
								geom.setPointA(fence.getPointA());
								geom.setPointB(fence.getPointB());
							}
							geom.setRepresentativePoint(fence.getRepresentativePoint());
							this.geometries.add(geom);
						}
					}
				}
			}
		}
		setRefresh(true);
	}

	private void drawNode(Canvas canvas, Paint paint, Point3D p, float radius, String color){
		float lineWidth = Math.min(mMapWidthInPx/100,mMapHeightInPx/100);
		String id = String.valueOf(p.getId());
		FontMetrics fm = new FontMetrics();
		paint.setColor(Color.parseColor(color));
		paint.setAlpha(80);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(14);
        paint.getFontMetrics(fm);
        float margin = 4;
        float x = (float) p.x;
        float y = (float) p.y;
        canvas.drawOval(new RectF(x - paint.measureText(id) / 2 - margin, y + lineWidth / 2 - paint.getTextSize(), x + paint.measureText(id) / 2 + margin, y + lineWidth / 2 + margin), paint);
        //canvas.drawRect(x-paint.measureText(id)/2 - margin, y + lineWidth/2 - paint.getTextSize(), x + paint.measureText(id)/2 + margin, y + lineWidth/2 + margin, paint);
        paint.setColor(Color.WHITE);
        canvas.drawText(id, x, y + lineWidth / 2, paint);
	}
	
	private void drawEdge(Canvas canvas, Paint paint, Edge edge, int transparency, String color, boolean wide) {
		float lineWidth = Math.min(mMapWidthInPx/100,mMapHeightInPx/100);
		if(wide){
			lineWidth = (float) (lineWidth*1.4);
		}
		
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.parseColor(color));				/* a blue color for the edge*/
		paint.setAlpha(transparency);										/* set to no transparency while drawing points */
		paint.setStrokeWidth(lineWidth);
		paint.setPathEffect(null);
		canvas.drawLine((float) edge.startNode.x, (float) edge.startNode.y, (float) edge.endNode.x, (float) edge.endNode.y, paint);
	}
	
	private void drawCalibrationLine(Canvas canvas, Paint paint, float startX, float startY, float endX, float endY, boolean dashed) {
		float lineWidth = Math.min(mMapWidthInPx/150,mMapHeightInPx/150);

		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.STROKE);
		paint.setAlpha(255);										/* set to no transparency while drawing points */
		paint.setStrokeWidth(lineWidth);
		if(dashed){
			paint.setPathEffect(new DashPathEffect(new float[] {10,20}, 0));
			paint.setColor(Color.parseColor("#DA00FF"));				/* a pink color for the markers*/
		}
		else{
			paint.setPathEffect(null);
			paint.setColor(Color.parseColor("#843179"));				/* a violet color for the markers*/
		}
		canvas.drawLine(startX, startY, endX, endY, paint);
	}
	
	private void drawCalibratedPosition(Canvas canvas, Paint paint, Point p) {
		int dotSize = (int) Math.min(mMapWidthInPx/100,mMapHeightInPx/100);

		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.parseColor("#FF9900"));				/* a orange color for the point*/
		paint.setAlpha(255);										/* set to no transparency while drawing points */
		canvas.drawCircle(p.x, p.y, dotSize / 1.5f, paint); 			/* make point radius 1/8 of width*/
	}
	
	private void drawCurrentPosition(Canvas canvas, Paint paint, Point p){
		int dotSize = (int) Math.min(mMapWidthInPx/20,mMapHeightInPx/20);
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(dotSize/3);
		paint.setColor(Color.parseColor("#843179"));				/* a violet color for the circle*/
		paint.setAlpha(255);										/* set to no transparency while drawing points */
		paint.setPathEffect(null);
		canvas.drawCircle(p.x, p.y, dotSize, paint);
		canvas.drawLine(p.x - dotSize / 1.5f, p.y, p.x + dotSize / 1.5f, p.y, paint);
		canvas.drawLine(p.x, p.y - dotSize / 1.5f, p.x, p.y + dotSize / 1.5f, paint);
	}
	
	private void drawFilterPosition(Canvas canvas, Paint paint, Point pKnn, Point pFiltered, boolean drawLine){
		int dotSize = (int) Math.min(mMapWidthInPx/100,mMapHeightInPx/100);
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		
		// draw filter output location
		paint.setColor(Color.parseColor(colorFilter));
		paint.setAlpha(255);
		paint.setPathEffect(null);
		canvas.drawCircle(pFiltered.x, pFiltered.y, dotSize / 1f, paint);
		
		if(drawLine){
			// draw a line between two points
			paint.setColor(Color.parseColor(colorFilter));
			canvas.drawLine(pKnn.x, pKnn.y, pFiltered.x, pFiltered.y, paint);
		}
	}
	
	private void drawParticle(Canvas canvas, Paint paint, ParticleState particle){
		int dotSize = (int) Math.min(mMapWidthInPx/200,mMapHeightInPx/200);
		float radius = dotSize/5f;
		float weightRadius = (float) (5*particle.getWeight()*radius) + 5;
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		String color = colorParticle; // default color
		if((mFilterIsResampled % 2)!=0){
			color = colorParticleResampled;
		}
		
		if(particle.getFloor()!=mMapFloor){
			//Draw the particle with different color
			if(particle.getFloor() > mMapFloor){
				// red color
				color = colorParticleAbove;
			}else{
				// blue color
				color = colorParticleBelow;
			}
		}
		
		if(particle.exceedsLength()){
			//color = colorMapFusion;
			color = "#FFFF00";
		}
		/*
		if(particle.isMaxWeight()){
			radius = dotSize;
			weightRadius = (float) (5*particle.getWeight()*radius) + 5;
			color = colorParticleMaxWeight;
		}
		*/

		paint.setColor(Color.parseColor(color));
		paint.setAlpha(255);
		canvas.drawCircle(particle.getX(), particle.getY(), radius, paint);
		paint.setAlpha(150);
		canvas.drawCircle(particle.getX(), particle.getY(), weightRadius, paint);

		if(particle.getParticleFree()!=null){
			if((mFilterIsResampled % 2)==0){
				color = colorParticleFree;
			} else {
				color = colorParticleFreeResampled;
			}
			/*
			if(particle.isMaxWeight()){
				color = colorParticleFreeMaxWeight;
			}
			*/

			paint.setColor(Color.parseColor(color));
			paint.setAlpha(255);
			canvas.drawCircle((float)particle.getParticleFree().x, (float)particle.getParticleFree().y, radius, paint);
			paint.setAlpha(150);
			canvas.drawCircle((float)particle.getParticleFree().x, (float)particle.getParticleFree().y, weightRadius, paint);
		}
	}
	
	private void drawMapFusionPosition(Canvas canvas, Paint paint, Point pMapFusion){
		int dotSize = (int) Math.min(mMapWidthInPx/200,mMapHeightInPx/200);
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		
		// draw filter output location
		paint.setColor(Color.parseColor(colorMapFusion));
		paint.setAlpha(255);
		paint.setPathEffect(null);
		canvas.drawCircle(pMapFusion.x, pMapFusion.y, dotSize/1f, paint);	
	}
	
	private void drawAP(Canvas canvas, Paint paint, APWithPosition ap){
		int dotSize = (int) Math.min(mMapWidthInPx/100,mMapHeightInPx/100);

		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.parseColor("#0000FF"));				/* a blue color for the point*/
		paint.setAlpha(255);										/* set to no transparency while drawing points */
		canvas.drawCircle((float)ap.getPoint().x, (float)ap.getPoint().y, dotSize, paint); 			/* make point radius 1/8 of width*/
		
	}
	
	private void drawCalibratedPositionWithColor(Canvas canvas, Paint paint, Point p, int transperancy, String tech){
		int dotSize = (int) Math.min(mMapWidthInPx/100,mMapHeightInPx/100);

		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.parseColor("#FF9900"));
		paint.setAlpha(255);
		canvas.drawCircle(p.x, p.y, dotSize / 2f, paint);
			
		if(tech.equalsIgnoreCase("wifi")){
			paint.setColor(Color.parseColor(colorPointWifi));
		}else if(tech.equalsIgnoreCase("ble")){
			paint.setColor(Color.parseColor(colorPointBle));
		}
		paint.setAlpha(transperancy);		
		canvas.drawCircle(p.x, p.y, dotSize*2f, paint); 
	}
	
	private void drawHighlightPoints(Canvas canvas, Paint paint, Point p){
		int dotSize = (int) Math.min(mMapWidthInPx/100,mMapHeightInPx/100);

		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.parseColor(colorHighlightPoint));
		paint.setAlpha(255);										/* set to no transparency while drawing points */
		canvas.drawCircle(p.x, p.y, dotSize, paint); 			/* make point radius 1/8 of width*/
		
	}
	
	private void drawBigPoint(Canvas canvas, Paint paint, Point p, float radius, String color){
		//int dotSize = (int) Math.min(mMapWidthInPx/100,mMapHeightInPx/100);
		float dotSize = Math.min(surfaceWidth/30,surfaceHeight/30);
		dotSize /= mScale;

		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		// draw the radius
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.parseColor(color));
		paint.setAlpha(80);
		canvas.drawCircle(p.x, p.y, radius, paint);
		// draw the point
		paint.setColor(Color.parseColor(color));
		paint.setAlpha(255);
		canvas.drawCircle(p.x, p.y, dotSize / 1.5f, paint);
		// draw border
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(dotSize / 8);
		paint.setPathEffect(null);
		paint.setShadowLayer(dotSize / 32, 0, 0, Color.parseColor(colorPosShadow));
		paint.setColor(Color.parseColor(colorPosBorder));
		paint.setAlpha(255);
		canvas.drawCircle(p.x, p.y, dotSize / 1.5f, paint);
		paint.clearShadowLayer();
	}
	
	private void drawSmallPoint(Canvas canvas, Paint paint, Point p, float radius, String color){
		int dotSize = (int) Math.min(mMapWidthInPx/100,mMapHeightInPx/100);

		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		// draw the point
		paint.setColor(Color.parseColor(color));
		paint.setAlpha(192);
		canvas.drawCircle(p.x, p.y, dotSize/2f, paint); 
		// draw the radius
		if(radius==-1){
			radius = dotSize*2f;
		}
		if(radius==-2){
			radius = dotSize*1.2f;
		}
		paint.setColor(Color.parseColor(color));
		paint.setAlpha(80);		
		canvas.drawCircle(p.x, p.y, radius, paint); 
	}
	
	private void drawWeightedPoint(Canvas canvas, Paint paint, Point p, double weight, String color){
		int dotSize = (int) Math.min(mMapWidthInPx/100,mMapHeightInPx/100);
		float innerRadius = dotSize/1.5f;
		float weightRadius = (float) (5*weight*innerRadius);
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		// draw the point
		paint.setColor(Color.parseColor(color));
		paint.setAlpha(255);
		canvas.drawCircle(p.x, p.y, innerRadius, paint); 
		// draw the radius
		paint.setColor(Color.parseColor(color));
		paint.setAlpha(180);		
		canvas.drawCircle(p.x, p.y, weightRadius, paint);
	}
	
	private void drawFence(Canvas canvas, Paint paint, Fence fence, String color){
		int dotSize = (int) Math.min(mMapWidthInPx/100,mMapHeightInPx/100);
		Point p = fence.getPoint().getPoint2D();
		float radius = fence.getRadius();
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		// draw the point
		paint.setColor(Color.parseColor(color));
		if(fence.getId()== selectedFenceId){
			paint.setColor(Color.parseColor(colorFenceselected));
		}
		paint.setAlpha(255);
		canvas.drawCircle(p.x, p.y, dotSize / 1.5f, paint);
		// draw the radius
		paint.setAlpha(80);		
		canvas.drawCircle(p.x, p.y, radius, paint);
		if(fence.isMultiple()){
			/*
			paint.setTextSize(20);
			paint.setAlpha(255);
			canvas.drawText("A", (float) fence.getPointA().x, (float) fence.getPointA().y, paint);
			canvas.drawText("B", (float) fence.getPointB().x, (float) fence.getPointB().y, paint);
			*/
			
			FontMetrics fm = new FontMetrics();
			paint.setAlpha(192);
	        paint.setTextAlign(Paint.Align.CENTER);
	        paint.setTextSize(14);
	        paint.getFontMetrics(fm);
	        float margin = 4;
	        
	        String id = "A";
	        paint.setColor(Color.parseColor(color));
			float x = (float) fence.getPointA().x;
	        float y = (float) fence.getPointA().y;
	        canvas.drawOval(new RectF(x-paint.measureText(id)/2 - margin, y - paint.getTextSize()/2 , x + paint.measureText(id)/2 + margin, y + paint.getTextSize()/2 + margin), paint);
	        //canvas.drawRect(x-paint.measureText(id)/2 - margin, y + lineWidth/2 - paint.getTextSize(), x + paint.measureText(id)/2 + margin, y + lineWidth/2 + margin, paint);
	        paint.setColor(Color.WHITE);
	        canvas.drawText(id, x, y + paint.getTextSize()/2, paint);
	        
	        id = "B";
	        paint.setColor(Color.parseColor(color));
			x = (float) fence.getPointB().x;
	        y = (float) fence.getPointB().y;
	        canvas.drawOval(new RectF(x-paint.measureText(id)/2 - margin, y - paint.getTextSize()/2 , x + paint.measureText(id)/2 + margin, y + paint.getTextSize()/2 + margin), paint);
	        //canvas.drawRect(x-paint.measureText(id)/2 - margin, y + lineWidth/2 - paint.getTextSize(), x + paint.measureText(id)/2 + margin, y + lineWidth/2 + margin, paint);
	        paint.setColor(Color.WHITE);
	        canvas.drawText(id, x, y + paint.getTextSize()/2, paint);
		}
	}

	private void drawGeometry(Canvas canvas, Paint paint, Geometry geom, String color){
		int dotSize = (int) Math.min(mMapWidthInPx/100,mMapHeightInPx/100);
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.parseColor(color));
		if(geom.getPoiId()== selectedFenceId){
			paint.setColor(Color.parseColor(colorGeoFenceselected));
		}
		paint.setAlpha(80);
		ArrayList<Point3D> points = geom.getPoints();
		/*
		for (int i=1; i<points.size();i++){
				canvas.drawLine(points.get(i-1).x, points.get(i-1).y, points.get(i).x, points.get(i).y, paint);
		}
		*/
		Path geoPath = new Path();
		//geoPath.reset(); // only needed when reusing this path for a new build
		geoPath.moveTo((float) points.get(0).x, (float) points.get(0).y); // used for first point
		for (int i=1; i<points.size();i++){
			geoPath.lineTo((float)points.get(i).x, (float)points.get(i).y);
		}
		canvas.drawPath(geoPath, paint);
		if(geom.getPointA()!=null && geom.getPointB()!=null){
			/*
			paint.setTextSize(20);
			paint.setAlpha(255);
			canvas.drawText("A", (float) poi.getPointA().x, (float) poi.getPointA().y, paint);
			canvas.drawText("B", (float) poi.getPointB().x, (float) poi.getPointB().y, paint);
			*/

			FontMetrics fm = new FontMetrics();
			paint.setAlpha(192);
			paint.setTextAlign(Paint.Align.CENTER);
			paint.setTextSize(14);
			paint.getFontMetrics(fm);
			float margin = 4;

			String id = "A";
			paint.setColor(Color.parseColor(color));
			float x = (float) geom.getPointA().x;
			float y = (float) geom.getPointA().y;
			canvas.drawOval(new RectF(x-paint.measureText(id)/2 - margin, y - paint.getTextSize()/2 , x + paint.measureText(id)/2 + margin, y + paint.getTextSize()/2 + margin), paint);
			//canvas.drawRect(x-paint.measureText(id)/2 - margin, y + lineWidth/2 - paint.getTextSize(), x + paint.measureText(id)/2 + margin, y + lineWidth/2 + margin, paint);
			paint.setColor(Color.WHITE);
			canvas.drawText(id, x, y + paint.getTextSize()/2, paint);

			id = "B";
			paint.setColor(Color.parseColor(color));
			x = (float) geom.getPointB().x;
			y = (float) geom.getPointB().y;
			canvas.drawOval(new RectF(x-paint.measureText(id)/2 - margin, y - paint.getTextSize()/2 , x + paint.measureText(id)/2 + margin, y + paint.getTextSize()/2 + margin), paint);
			//canvas.drawRect(x-paint.measureText(id)/2 - margin, y + lineWidth/2 - paint.getTextSize(), x + paint.measureText(id)/2 + margin, y + lineWidth/2 + margin, paint);
			paint.setColor(Color.WHITE);
			canvas.drawText(id, x, y + paint.getTextSize()/2, paint);
		}
		Point3D representativePoint = geom.getRepresentativePoint();
		if(representativePoint!=null) {
			float x = (float) representativePoint.x;
			float y = (float) representativePoint.y;
			paint.setAlpha(192);
			paint.setColor(Color.WHITE);
			canvas.drawCircle(x, y, dotSize, paint);
		}
	}
	
	private void drawText(Canvas canvas, Paint paint, String text, String color) {
		paint.setTextSize(30);
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.parseColor(color));
		paint.setAlpha(255);										/* set to no transparency while drawing text */
		paint.setPathEffect(null);
		float x = mMapWidthInPx/3;
		float y = mMapHeightInPx/2;
		Log.i(TAG, "x: " + x + " , y: " + y);
		canvas.drawText(text, x, y, paint);
	}

	private void drawDetection(Canvas canvas, Paint paint, Point p){
		int dotSize = (int) Math.min(mMapWidthInPx/70,mMapHeightInPx/70);
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(dotSize / 4);
		paint.setColor(Color.parseColor(colorDetection));				/* a violet color for the circle*/
		paint.setAlpha(255);										/* set to no transparency while drawing points */
		paint.setPathEffect(null);
		canvas.drawCircle(p.x, p.y, dotSize, paint);
		canvas.drawLine(p.x - dotSize / 1.5f, p.y, p.x + dotSize / 1.5f, p.y, paint);
		canvas.drawLine(p.x, p.y - dotSize / 1.5f, p.x, p.y + dotSize / 1.5f, paint);
	}
	
	private void drawRoute(Canvas canvas, Paint paint, LinkedList<Point3D> route) {
		
		int dotSize = (int) Math.min(mMapWidthInPx/100,mMapHeightInPx/100);
		
		float lineWidth = Math.min(mMapWidthInPx/150,mMapHeightInPx/150);
		
		Point3D lastPoint = null;
		paint.setAntiAlias(true);
		paint.setColor(Color.MAGENTA);
		paint.setAlpha(255);
		paint.setPathEffect(null);
		paint.setStrokeWidth(lineWidth);
		
		for(Point3D point: route) {
			
			if(point.z == this.mMapFloor) {
				paint.setStyle(Paint.Style.FILL);
				paint.setPathEffect(null);
				canvas.drawCircle((float) point.x, (float) point.y, dotSize, paint);
			}
			if(lastPoint != null) {
				if(lastPoint.z == this.mMapFloor || point.z == this.mMapFloor) {
					paint.setStyle(Paint.Style.STROKE);
					
					if(lastPoint.z != point.z) {
						paint.setPathEffect(new DashPathEffect(new float[] {1,2}, 0));
					} else {
						paint.setPathEffect(null);
					}
					
					drawArrow(paint, canvas, (float) lastPoint.x,(float) lastPoint.y, (float) point.x, (float) point.y, lineWidth);
				}
			}
			lastPoint = point;
		}
		
		//draw end and first node bigger and with a different color
		paint.setStyle(Paint.Style.FILL);
		Point3D p = route.getFirst();
		if(p.z == this.mMapFloor) {
			paint.setColor(Color.BLUE);
			canvas.drawCircle((float) p.x, (float) p.y, dotSize, paint);
		}
		
		p = route.getLast();
		if(p.z == this.mMapFloor) {
			paint.setColor(Color.DKGRAY);
			canvas.drawCircle((float) p.x, (float) p.y, (int) (dotSize*1.2), paint);
		}
	}
	
	private void drawArrow(Paint paint, Canvas canvas, float x0, float y0, float x1, float y1, float lineWidth) {
	    paint.setStyle(Paint.Style.STROKE);
		paint.setAlpha(255);

		canvas.drawLine(x0, y0, x1, y1, paint);

	    //int arrowHeadLength = 30;
		float arrowHeadLength = 4*lineWidth;
		int arrowHeadAngle = 45;
	    float[] linePts = new float[] {x1 - arrowHeadLength, y1, x1, y1};
	    float[] linePts2 = new float[] {x1, y1, x1, y1 + arrowHeadLength};

	    //get the center of the line
	    float centerX = (x1 + x0)/2;
	    float centerY = (y1 + y0)/2;

	    //set the angle
	    double angle = Math.atan2(y1 - y0, x1 - x0) * 180 / Math.PI + arrowHeadAngle;

		/*
	    //rotate the matrix around the center
	    Matrix rotateMat = new Matrix();
		rotateMat.setRotate((float) angle, centerX, centerY);
	    rotateMat.mapPoints(linePts);
	    rotateMat.mapPoints(linePts2);

	    canvas.drawLine(linePts [0], linePts [1], linePts [2], linePts [3], paint);
	    canvas.drawLine(linePts2 [0], linePts2 [1], linePts2 [2], linePts2 [3], paint);
	    */

		// option 2 (Dani)
		Path mPath = new Path();
		mPath.moveTo(centerX - arrowHeadLength, centerY); // used for first point
		mPath.lineTo(centerX, centerY);
		mPath.lineTo(centerX, centerY + arrowHeadLength);
		//draw Arrow with correct orientation
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.STROKE);
		canvas.save();
		canvas.rotate((float)angle, centerX, centerY);
		canvas.drawPath(mPath, paint);
		canvas.restore();
	}
	
	private void drawPriorDistance(Canvas canvas, Paint paint, Point p, float radius, String color){
		int dotSize = (int) Math.min(mMapWidthInPx/100,mMapHeightInPx/100);

		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		// draw the point
		paint.setColor(Color.parseColor(color));
		paint.setAlpha(192);
		canvas.drawCircle(p.x, p.y, dotSize/2f, paint); 
		// draw the radius
		if(radius==-1){
			radius = dotSize*2f;
		}
		if(radius==-2){
			radius = dotSize*1.2f;
		}
		//paint.setAlpha(80);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(dotSize / 1.5f);
		paint.setPathEffect(new DashPathEffect(new float[]{10, 10}, 0));
		canvas.drawCircle(p.x, p.y, radius, paint); 
	}

	private void drawOrientationArrow(Canvas canvas, Paint paint, Point p, float degrees, String color){
		//float dotSize = Math.min(mMapWidthInPx/100,mMapHeightInPx/100);
		float dotSize = Math.min(surfaceWidth/30,surfaceHeight/30);
		dotSize /= mScale;
		// option 1: Arrow above point
		/*
		float d_s = dotSize*2; // distance small
		float d_l = d_s*1.5f; // distance large
		Path mPath = new Path();
		mPath.moveTo(p.x, p.y - d_l); // used for first point
		mPath.lineTo(p.x + d_s, p.y + d_l);
		mPath.lineTo(p.x, p.y + d_s);
		mPath.lineTo(p.x - d_s, p.y + d_l);
		mPath.lineTo(p.x, p.y - d_l);
		mPath.close();
		*/
		// option 2: Arrow behind point
		float d_t = dotSize*0.8f;	// triangle dimension
		float d_to_point = dotSize*0.9f; // distance to point
		Path mPath = new Path();
		RectF rf = new RectF(p.x - d_to_point, p.y - d_to_point, p.x + d_to_point, p.y + d_to_point);
		mPath.moveTo(p.x, p.y - d_to_point - d_t * 0.6f); // used for first point
		mPath.arcTo(rf, 270 - 30, 60, true);
		mPath.lineTo(p.x, p.y - d_to_point - d_t * 0.6f);
		mPath.close();
		//draw Arrow with correct orientation
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.parseColor(color));
		canvas.save();
		paint.setAlpha(255);
		canvas.rotate(degrees, p.x, p.y);
		canvas.drawPath(mPath, paint);
		canvas.restore();
	}

	//LQ: POIs and Routes

	public void setSelectedPoiId(int id){
		selectedPoiId = id;
	}

	public void drawPOIs(ArrayList<POI> pois, String type){

		float minRadius = Math.max(mMapHeightInPx/20, mMapWidthInPx/20);
		ArrayList<POI> localPois;

		if(type.equals("mobile")) {
			localPois = this.mobilePois;
		} else {
			localPois = this.staticPois;
		}

		localPois.clear();
		for(POI poi : pois){
			//if radius is -1 it means it is undefined, so we have to set a minimum radius to allow its selections
			if(poi.getRadius() == -1){
				poi.setRadius(minRadius);
			}
			localPois.add(poi);
		}
		setRefresh(true);
	}

	private void drawPOI(Canvas canvas, Paint paint, POI poi, String color){
		int dotSize = (int) Math.min(mMapWidthInPx/100,mMapHeightInPx/100);
		Point p = poi.getPoint().getPoint2D();
		float radius = poi.getRadius();
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL);
		// draw the point
		paint.setColor(Color.parseColor(color));
		if(poi.getId()==selectedPoiId){
			paint.setColor(Color.parseColor(colorPOIselected));
		}
		paint.setAlpha(255);
		canvas.drawCircle(p.x, p.y, dotSize/1.5f, paint);
		// draw the radius
		paint.setAlpha(80);
		canvas.drawCircle(p.x, p.y, radius, paint);
		this.drawTextTag(canvas, paint, 5, poi.getName(), p.x, p.y);
		if(poi.isMultiple()){
                        /*
                        paint.setTextSize(20);
                        paint.setAlpha(255);
                        canvas.drawText("A", (float) poi.getPointA().x, (float) poi.getPointA().y, paint);
                        canvas.drawText("B", (float) poi.getPointB().x, (float) poi.getPointB().y, paint);
                        */

			FontMetrics fm = new FontMetrics();
			paint.setAlpha(192);
			paint.setTextAlign(Paint.Align.CENTER);
			paint.setTextSize(14);
			paint.getFontMetrics(fm);
			float margin = 4;

			String id = "A";
			paint.setColor(Color.parseColor(color));
			float x = (float) poi.getPointA().x;
			float y = (float) poi.getPointA().y;
			canvas.drawOval(new RectF(x-paint.measureText(id)/2 - margin, y - paint.getTextSize()/2 , x + paint.measureText(id)/2 + margin, y + paint.getTextSize()/2 + margin), paint);
			//canvas.drawRect(x-paint.measureText(id)/2 - margin, y + lineWidth/2 - paint.getTextSize(), x + paint.measureText(id)/2 + margin, y + lineWidth/2 + margin, paint);
			paint.setColor(Color.WHITE);
			canvas.drawText(id, x, y + paint.getTextSize()/2, paint);

			id = "B";
			paint.setColor(Color.parseColor(color));
			x = (float) poi.getPointB().x;
			y = (float) poi.getPointB().y;
			canvas.drawOval(new RectF(x-paint.measureText(id)/2 - margin, y - paint.getTextSize()/2 , x + paint.measureText(id)/2 + margin, y + paint.getTextSize()/2 + margin), paint);
			//canvas.drawRect(x-paint.measureText(id)/2 - margin, y + lineWidth/2 - paint.getTextSize(), x + paint.measureText(id)/2 + margin, y + lineWidth/2 + margin, paint);
			paint.setColor(Color.WHITE);
			canvas.drawText(id, x, y + paint.getTextSize()/2, paint);
		}
	}

	public void setMobileDestination(POI poi) {
		this.destinationMobilePOI = poi;
	}

	public void setNavigationMode(boolean enabled) {

		float scale;

		this.navigationMode = enabled;

		//apply zoom to current position the first time navigation mode is enabled
		if(enabled) {
			scale = (maxScale/Map.navigationModeZoom)/totalScale;
			centerPosition();
			mMapMatrix.postScale(scale, scale, surfaceWidth/2, surfaceHeight/2);
			totalScale *= scale;

		} else {
			scale = Math.min(surfaceWidth/mMapWidthInPx, surfaceHeight/mMapHeightInPx)/totalScale;
			mMapMatrix.postScale(scale, scale, -(float) surfaceWidth/2, -(float) surfaceHeight/2);
			totalScale *= scale;
			centerMap();
			this.destinationMobilePOI = null;
		}
	}

	private void centerPosition() {
		Point3D nextPointScreen = transformMapToScreenPoint(mCurrentOnlinePosition);
		mMapMatrix.postTranslate(-(float)(nextPointScreen.x - (surfaceWidth/2)), -(float)(nextPointScreen.y - (surfaceHeight/2)));

	}

	private void centerMap() {
		Point3D point = transformMapToScreenPoint(new Point3D(mMapWidthInPx/2, mMapHeightInPx/2, 0));
		mMapMatrix.postTranslate(-(float)(point.x - (surfaceWidth/2)), -(float)(point.y - (surfaceHeight/2)));
		//mMapMatrix.postTranslate(-(float)(mMapWidthInPx - (surfaceWidth/2)), -(float)(mMapHeightInPx - (surfaceHeight/2)));
	}

	private void drawTextTag(Canvas canvas, Paint paint, int margin, String text, float x, float y) {
		float lineWidth = Math.min(mMapWidthInPx/100,mMapHeightInPx/100);
		FontMetrics fm = new FontMetrics();
		paint.setColor(Color.WHITE);
		paint.setStyle(Paint.Style.FILL);
		paint.setAlpha(150);
		paint.setTextAlign(Paint.Align.CENTER);

		//final float densityMultiplier = getContext().getResources().getDisplayMetrics().density;
		paint.setTextSize(14.0f);
		float height = paint.getTextSize();
		paint.getFontMetrics(fm);

		canvas.drawRoundRect(new RectF(x - margin - paint.measureText(text) / 2, y - margin - height / 2 - lineWidth / 2, x + margin + paint.measureText(text) / 2, y + margin + lineWidth / 2 + height / 2), 5, 5, paint);

		paint.setColor(Color.BLACK);
		paint.setAlpha(255);
		canvas.drawText(text, x, y + lineWidth / 2, paint);
	}

}
