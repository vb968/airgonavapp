package com.ascamm.ascammnav.map;

import android.content.Context;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Air-Fi.
 */

public class MicelloMapView extends WebView {

    private boolean map_initiated = false;

    private String API_KEY = "aHSN8BCLh07mFtt1OKxXYYLpxMyfTr";
    private int COMMUNITY_ID_DEMO = 78;
    private int COMMUNITY_ID_ASCAMM = 24504;
    private int COMMUNITY_ID_ILLA = 1704;
    private int COMMUNITY_ID_MWC = 25251;

    int community;

    public MicelloMapView (Context context) {
        super (context);

        community = COMMUNITY_ID_DEMO;

        // MUY IMPORTANTE habilitar la ejecucion de javascript
        this.getSettings().setJavaScriptEnabled(true);

        this.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                //Log.d("webclient", "on page finished!");
                executeJavascript("initMicelloMap('"+API_KEY+"',"+community+");");
            }
        });

        //Load HTML content
        this.loadUrl("file:///android_asset/micelloMap.html");
    }

    public void initMap(int site_id) {
        if (site_id == 28)
            community = COMMUNITY_ID_ASCAMM;
        else if (site_id == 35)
            community = COMMUNITY_ID_ILLA;
        else if (site_id == 36)
            community = COMMUNITY_ID_MWC;

        Log.d("MicelloMapView", "loading map community " + community + " from site "+ site_id);

        map_initiated = true;
    }

    public boolean isMapInitiated() {
        return map_initiated;
    }

    public void setLocation(double lat, double lon, double level) {
//        Log.d("MicelloMapView", "set location: "+"setNavCoordinates("+lat+","+lon+","+level+");");
        executeJavascript("setNavCoordinates("+lat+","+lon+","+level+");");
    }

    public void clearScansets() {
//        Log.d("MicelloMapView", "scansets cleared");
        executeJavascript("clearScansets();");
    }

    public void setScanset(String mac, float rss, int bat, int perc) {
//        Log.d("MicelloMapView", "set scanset: "+"setScanset("+mac+","+rss+","+bat+","+perc+");");
        executeJavascript("setScanset("+"\""+mac+"\""+","+rss+","+bat+","+perc+");");
    }

    public void setSource(String source) {
        executeJavascript("setSource("+"\""+source+"\");");
    }

    private void executeJavascript(String command) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            this.evaluateJavascript(command, null);
        } else {
            this.loadUrl("javascript:"+command);
        }
    }
}
