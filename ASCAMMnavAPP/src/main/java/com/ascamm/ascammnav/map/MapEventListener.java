package com.ascamm.ascammnav.map;

import com.ascamm.utils.Point3D;

/**
 * A interface defines the methods related with map events. 
 * @author Junzi Sun [CTAE]
 *
 */
public interface MapEventListener {
	/**
	 * update offline map current coordination
	 * @param point3D - current cell coordination
	 */
	public void updateOfflineCurrentPosition(Point3D point3D);
	
	/**
	 * upate online map current location 
	 * @param pointF - location in meters
	 * @param errorRadius - error radius in meters
	 */

}
