package com.ascamm.ascammnav.map;

import com.ascamm.utils.Point3D;


/**
 * A interface that defines the methods related with map Fence events.
 * @author Dani Fernandez [ASCAMM]
 *
 */
public interface PointSelectedEventListener {
	/**
	 * update Fence selected
	 * @param point - new selected point
	 */
	public void newPointSelected(Point3D point);

}
