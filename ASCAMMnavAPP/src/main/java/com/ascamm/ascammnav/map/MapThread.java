package com.ascamm.ascammnav.map;

import android.graphics.Canvas;


/**
 * Thread class to Thread which will trigger the onDraw() method of our view.
 * @author Junzi Sun [CTAE]
 */
public class MapThread extends Thread{

	private Map mFreeMap;
	public boolean mIsRun = false;

	public MapThread(Map map){
		mFreeMap = map;
	}

	/* perform the loop */
	@Override
	public void run(){
		Canvas canvas;
		while (mIsRun) {
			canvas = null;
			try{
				if(mFreeMap.needsRefresh()){
					canvas = mFreeMap.getHolder().lockCanvas();
					synchronized (mFreeMap.getHolder()) {
						mFreeMap.onDraw(canvas);
					}
				}
				else{
					Thread.sleep(10);		// sleep a little bit for the sake of CPU
				}
			}  catch (Exception e) {
				// exception
			}  finally {
				// if an exception is thrown, we don't leave the surface in an inconsistent state
				if(canvas != null){
					mFreeMap.getHolder().unlockCanvasAndPost(canvas);
				}
			}
		}
	}

}
