package com.ascamm.ascammnav.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

public class Media {

	private String TAG = getClass().getSimpleName();
	private Context mContext;
	
	public Media(Context context){
		mContext = context;
	}
	
	public Bitmap getBitmap(Uri fileUri, boolean isFromGallery) {
		ContentResolver cr = mContext.getContentResolver();
		InputStream inStream = null;
		Bitmap transformedBitmap = null;
		// Open Input Stream
		try {
			inStream = cr.openInputStream(fileUri);
			Log.i("URI ===> ", fileUri.getPath());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (inStream != null) {

			// Decode image size
	        BitmapFactory.Options opt = new BitmapFactory.Options();
	        opt.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(inStream, null, opt);

	        // The new size we want to scale to
	        final int REQUIRED_SIZE = 480;

	        // Find the correct scale value. It should be power of 2.
	        int width_tmp = opt.outWidth, height_tmp = opt.outHeight;
	        int scale = 1;
	        System.out.println("Initial size: ( " + width_tmp + " , " + height_tmp + " )");
			while (true) {
	            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
	                break;
	            }
	            width_tmp /= 2;
	            height_tmp /= 2;
	            scale *= 2;
	        }
			System.out.println("Final size: ( " + width_tmp + " , " + height_tmp + " )");
			
			// Close Input Stream
			try {
				inStream.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			// Open New Input Stream
			try {
				inStream = cr.openInputStream(fileUri);
				Log.i("URI ===> ", fileUri.getPath());
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(inStream != null){
		        // Decode with inSampleSize to reduce the size of the image
			    BitmapFactory.Options opt2 = new BitmapFactory.Options();
				opt2.inSampleSize = scale;
				Bitmap bit = BitmapFactory.decodeStream(inStream, null, opt2);
				
				try {
					inStream.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				
				// Change the orientation of the picture
				int rotate = 0;
				int orientation = 0;
				if(isFromGallery){
					String[] orientationColumn = {MediaStore.Images.Media.ORIENTATION};
		            //Cursor cur = managedQuery(outputImageUri, orientationColumn, null, null, null);
		            Cursor cur = mContext.getContentResolver().query(fileUri, orientationColumn, null, null, null);
		            if (cur != null && cur.moveToFirst()) {
		                orientation = cur.getInt(cur.getColumnIndex(orientationColumn[0]));
		                rotate = orientation;
		            }  
	            }else{
					ExifInterface exif = null;
					try {
						exif = new ExifInterface(fileUri.getPath());
					} catch (IOException e) {
						e.printStackTrace();
					}
					if (exif != null) {
						orientation = exif.getAttributeInt(
								ExifInterface.TAG_ORIENTATION,
								ExifInterface.ORIENTATION_NORMAL);
						if (orientation == 5 || orientation == 6) //ORIENTATION_ROTATE_90
							rotate = 90;
						else if (orientation == 7 || orientation == 8) //ORIENTATION_ROTATE_270
							rotate = 270;
						else if (orientation == 3 || orientation == 4) //ORIENTATION_ROTATE_180
							rotate = 180;
					}	
				}
				System.out.println("orientation: " + orientation
						+ " , rotate: " + rotate);
				Matrix bitmapMatrix = new Matrix();
				bitmapMatrix.postRotate(rotate);
				transformedBitmap = Bitmap.createBitmap(bit, 0, 0,
						bit.getWidth(), bit.getHeight(), bitmapMatrix, false);
			}
		}
		return transformedBitmap;
	}
	
	public void galleryAddFile(Uri fileUri) {
		Intent mediaScanIntent = new Intent(
				Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		mediaScanIntent.setData(fileUri);
		mContext.sendBroadcast(mediaScanIntent);
	}
	
	public Bitmap getVideoThumbnail(String fileUri) {

		Bitmap video_thumbnail = ThumbnailUtils.createVideoThumbnail(
				fileUri,
				MediaStore.Images.Thumbnails.MINI_KIND);
		return video_thumbnail;
	}
	
	public String encodeBase64(String type, Uri fileUri){
		String encodedFile = null;
		if (fileUri != null) {
			if(type.equalsIgnoreCase("image")){
				// Encode image to Base64
				System.out.println("image URI: " + fileUri);
				//BitmapFactory.Options options = new BitmapFactory.Options();
				//Bitmap bm = BitmapFactory.decodeFile(filePath,options);
				Bitmap bm = getBitmap(fileUri, true);
				try{
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					if (bm != null) {// Image found
						bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
						byte[] b = baos.toByteArray();
						try {
							encodedFile = Base64.encodeToString(b, Base64.DEFAULT);
						} catch (Exception ex) {
							Log.d("Base64 Encoding", "Error codificacion en Base64");
						}
					} else {// Image not found
						Log.d("Base64 Encoding", "bm is NULL");
					}
					baos.close();
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
			else if(type.equalsIgnoreCase("video")){
				// Encode video to Base64
				File tmpFile = new File(fileUri.getPath());
				FileInputStream inStream = null;
				int bytesRead, bytesAvailable, bufferSize;
				byte[] buffer;
				int maxBufferSize = 1 * 1024 * 1024;
				try
				{
					System.out.println("video URI: " + fileUri);
				    inStream = new FileInputStream(tmpFile);
				
					bytesAvailable = inStream.available(); // create a buffer of maximum size
					int bytesFile = bytesAvailable;
					Log.d("Base64 Encoding", "video size : " + bytesFile);
					
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					buffer = new byte[bufferSize];
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					
					// read file and write it into form...
					bytesRead = inStream.read(buffer, 0, bufferSize);
					
					while (bytesRead > 0) {
						baos.write(buffer, 0, bufferSize);
						bytesAvailable = inStream.available();
						bufferSize = Math.min(bytesAvailable, maxBufferSize);
						System.out.println("bytesAvailable " + bytesAvailable);
						System.out.println("bytesRead: " + bytesRead);
						bytesRead = inStream.read(buffer, 0, bufferSize);
					}
					
					encodedFile = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
					// close streams
					inStream.close();
					baos.flush();
					baos.close();
				} 
				catch (FileNotFoundException e) {
				    e.printStackTrace();
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
				catch (Exception e) {
					Log.d("Base64 Encoding", "Error codificacion en Base64");
				}
				
			}
			else{
				Log.d("Base64 Encoding", "Bad File type");
			}
		}
		Log.d("Base64 Encoding", "encodedFile: " + encodedFile);
		return encodedFile;
	}
	
	public Bitmap shrinkBitmap(String file, int width, int height) {

		BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
		bmpFactoryOptions.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

		int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
				/ (float) height);
		int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
				/ (float) width);
		Log.d(TAG,"widthRatio: " + widthRatio + " , heightRatio: " + heightRatio);
		
		if (heightRatio > 1 || widthRatio > 1) {
			if (heightRatio > widthRatio) {
				bmpFactoryOptions.inSampleSize = heightRatio;
			} else {
				bmpFactoryOptions.inSampleSize = widthRatio;
			}
		}

		bmpFactoryOptions.inJustDecodeBounds = false;
		bmpFactoryOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
		bmpFactoryOptions.inDither = false;
		bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
		return bitmap;
	}
	
	public Bitmap shrinkBitmap2(String file, int maxWidth, int maxHeight) {

		BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
		bmpFactoryOptions.inJustDecodeBounds = false;
		Bitmap bm = BitmapFactory.decodeFile(file, bmpFactoryOptions);
		
		int width = bm.getWidth();
	    int height = bm.getHeight();
	    float widthRatio = (float) maxWidth / (float) width;
	    float heightRatio = (float) maxHeight / (float) height;
		Log.d(TAG,"widthRatio: " + widthRatio + " , heightRatio: " + heightRatio);
		
		float scale = 1;
	    if (heightRatio < 1 || widthRatio < 1) {
			if (heightRatio < widthRatio) {
				scale = heightRatio;
			    width *= scale; 
			    height *= scale;
			} else {
				scale = widthRatio;
				width *= scale;
				height *= scale;
			}
			// "RECREATE" THE NEW BITMAP
		    Bitmap resizedBitmap = Bitmap.createScaledBitmap(bm, width, height, false);
		    bm.recycle();
		    return resizedBitmap;
		}
	    else{
	    	return bm;
	    }
	    
	}
	
}
