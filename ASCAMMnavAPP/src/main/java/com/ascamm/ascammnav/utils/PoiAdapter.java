package com.ascamm.ascammnav.utils;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ascamm.ascammnav.R;
import com.ascamm.utils.Point3D;
import com.ascamm.pois.utils.POI;

public class PoiAdapter extends ArrayAdapter<POI>{

    //Context context;
    private int layoutResourceId;

    final int POI_ID_SELECT_FROM_MAP = -1;
    int selectedPoiId = -2;

    public PoiAdapter(Context context, int layoutResourceId, ArrayList<POI> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        addSelectFromMap();
        /*this.context = context;
        setData(data);*/
    }

    private void addSelectFromMap() {
        POI selectFromMap = new POI(POI_ID_SELECT_FROM_MAP, new Point3D(-1,-1,-1), 0);
        selectFromMap.setName("Select from map");
        super.add(selectFromMap);
    }

    public void setData(ArrayList<POI> data) {

        super.clear();

        addSelectFromMap();

        if(data != null) {
            for(POI poi: data) {
                super.add(poi);
            }
        }

    }

    public void setSelectedPoiId(int id){
        selectedPoiId = id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        PoiHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)getContext()).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new PoiHolder();
            holder.layPoi = (LinearLayout)row.findViewById(R.id.lay_poi);
            //holder.layPoiPos = (LinearLayout)row.findViewById(R.id.lay_poi_pos);
            holder.txtName = (TextView)row.findViewById(R.id.poi_name);
            //holder.txtName = (CheckedTextView)row.findViewById(R.id.poi_name);
            //holder.txtPos = (TextView)row.findViewById(R.id.poi_pos);

            row.setTag(holder);
        }
        else
        {
            holder = (PoiHolder)row.getTag();
        }

        POI poi = getItem(position);
        holder.txtName.setText(poi.getName());
        if(poi.getId()==POI_ID_SELECT_FROM_MAP){
            //holder.layPoiPos.setVisibility(View.INVISIBLE);
        }
        else{
            //holder.layPoiPos.setVisibility(View.VISIBLE);
            //holder.txtPos.setText("{" + poi.getPoint().x + "," + poi.getPoint().y + "," + poi.getPoint().z + "}");

        }
        if(poi.getId()==selectedPoiId){
            holder.txtName.setTextColor(getContext().getResources().getColor(R.color.ascamm_dialog_clicked_text));
            //holder.txtPos.setTextColor(getContext().getResources().getColor(R.color.ascamm_dialog_clicked_text));
            holder.layPoi.setBackgroundColor(getContext().getResources().getColor(R.color.ascamm_dialog_clicked_background));
        }
        else{
            holder.txtName.setTextColor(Color.BLACK);
            //holder.txtPos.setTextColor(Color.GRAY);
            holder.layPoi.setBackgroundDrawable(null);
        }

        return row;
    }

    static class PoiHolder
    {
        LinearLayout layPoi;
        //LinearLayout layPoiPos;
        TextView txtName;
        //CheckedTextView txtName;
        //TextView txtPos;
    }
}