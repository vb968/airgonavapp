package com.ascamm.ascammnav.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ascamm.motion.PosActions;
import com.ascamm.motion.PositioningService;

/**
 * Created by indoorpositioning on 04/05/16.
 */

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context ctxt, Intent i) {
        if(AppData.START_POS_SERVICE_ON_BOOT) {
            Intent intent = new Intent(ctxt, PositioningService.class);
            intent.setAction(PosActions.ACTION_START_ON_BOOT);
            ctxt.startService(intent); // call the service
        }
    }
}