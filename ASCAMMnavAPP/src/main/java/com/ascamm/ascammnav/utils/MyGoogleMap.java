package com.ascamm.ascammnav.utils;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

import com.ascamm.ascammnav.R;
import com.ascamm.motion.LocationData;
import com.ascamm.motion.utils.Edge;
import com.ascamm.utils.Point3D;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class MyGoogleMap {

	private String TAG = getClass().getSimpleName();
	private boolean debug = false;
	
	private GoogleMap map;
	private GroundOverlay mGroundOverlay = null;
	private GroundOverlayOptions mOptions;
	
	private Marker userMarker;
	private Circle userMarkerCircle;
	private ArrayList<Marker> candidateMarkers = new ArrayList<Marker>();
	private boolean userMarkerExists = false, candidateMarkersExist = false;
	private int markerColorStrokeGreen = 0xFF7FAF1B;
	private int markerColorFillGreen = 0x557FAF1B;
	private ArrayList<Marker> detectionMarkers = new ArrayList<Marker>();
	private ArrayList<Polyline> edgeLines = new ArrayList<Polyline>();
	
	
	private Media mMedia;
	private int MAX_IMAGE_PX = 1000;
	
	public MyGoogleMap(GoogleMap googleMap, float zoom, LatLng centerPos, Media media){
		map = googleMap;
		if(map!=null){
			map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			//map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
			map.moveCamera(CameraUpdateFactory.newLatLngZoom(centerPos, zoom));
			/*
			CameraPosition cameraPosition = new CameraPosition.Builder()
					.target(center_position) // Sets the center of the map
					.zoom(zoom) // Sets the zoom
					//.bearing(Float.valueOf("-4.5")) // Sets the orientation of the camera
					// .tilt(30) // Sets the tilt of the camera to 30 degrees
					.build(); // Creates a CameraPosition from the builder
			map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
			*/
			map.setMyLocationEnabled(false);
			map.getUiSettings().setZoomControlsEnabled(false);
			map.getUiSettings().setCompassEnabled(true);
		}else{
			Log.e(TAG,"map received is null");
		}
		mMedia = media;
	}
	
	public boolean insertIndoorMap(String mapPath, double lat, double lng, float width, float height, float bearing){
		boolean success = false;
		try{
			LatLng anchorLatLng = new LatLng(lat, lng);
			Bitmap mapBmp = mMedia.shrinkBitmap2(mapPath, MAX_IMAGE_PX, MAX_IMAGE_PX); 
			BitmapDescriptor bmpDescriptor = BitmapDescriptorFactory.fromBitmap(mapBmp);
			//BitmapDescriptor bmpDescriptor = BitmapDescriptorFactory.fromPath(mapPath); //can throw out of memory error if image is too large
			
			if(mGroundOverlay == null){
				// create groundOverlay
				mOptions = new GroundOverlayOptions();
				if(mapPath!=null){
					mOptions.image(bmpDescriptor);
				}
				mOptions.anchor(0, 0);
				mOptions.bearing(bearing);
				mOptions.position(anchorLatLng, width, height);
				try{
					mGroundOverlay = map.addGroundOverlay(mOptions);
				}catch(IllegalArgumentException e){
					e.printStackTrace();
				}
			}
			else {
				// update groundOverlay
				//map.clear();
				mGroundOverlay.setBearing(bearing);
				mGroundOverlay.setPosition(anchorLatLng);
				mGroundOverlay.setDimensions(width, height);
				mGroundOverlay.setImage(bmpDescriptor);
			}
			
			success = true;
		} catch(IllegalArgumentException e){
			e.printStackTrace();
		} catch(IllegalStateException e){
			e.printStackTrace();
		} catch(Exception e){
			e.printStackTrace();
		}
		return success;
	}
	
	public boolean updateCameraPosition(float zoom, double lat, double lng){
		boolean success = false;
		LatLng center_position = new LatLng(lat, lng);
		if(map!=null){
			CameraPosition cameraPosition = new CameraPosition.Builder()
					.target(center_position) // Sets the center of the map
					.zoom(zoom) // Sets the zoom
					.build(); // Creates a CameraPosition from the builder
			map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
			success = true;
		}else{
			Log.e(TAG,"map received is null");
		}
		return success;
	}
	
	public float getCurrentZoom(){
		float zoom = -1;
		if(map!=null){
			zoom = map.getCameraPosition().zoom;
		}
		return zoom;
	}
	
	public void changeFloorImage(String mapPath){
		Bitmap mapBmp = mMedia.shrinkBitmap2(mapPath, MAX_IMAGE_PX, MAX_IMAGE_PX); 
		BitmapDescriptor bmpDescriptor = BitmapDescriptorFactory.fromBitmap(mapBmp);
		mOptions.image(bmpDescriptor);
		if(mGroundOverlay != null){
			mGroundOverlay.remove();
			mGroundOverlay = null;
		}
		mGroundOverlay = map.addGroundOverlay(mOptions);
	}
	
	public void drawPosition(LocationData userPosition){
		LatLng user_pos = new LatLng(userPosition.positionGlobal[0],
				userPosition.positionGlobal[1]);
		if (userMarkerExists == false) {
			userMarker = map.addMarker(new MarkerOptions()
					.position(user_pos)
					.icon(BitmapDescriptorFactory.fromResource(R.drawable.dot_green))
					.anchor(0.5f, 0.5f));
			userMarkerCircle = map.addCircle(new CircleOptions().center(user_pos)
					.radius(userPosition.accuracy_meters).strokeWidth(1).strokeColor(markerColorStrokeGreen)
					.fillColor(markerColorFillGreen).zIndex(10));
			userMarkerExists = true;
		} else {
			userMarker.setPosition(user_pos);
			userMarkerCircle.setRadius(userPosition.accuracy_meters);
			userMarkerCircle.setCenter(user_pos);
		}
	}
	
	public void drawCandidates(LocationData userPosition){
		if(!candidateMarkersExist){
			if(userPosition.candidatesGlobal!=null && userPosition.candidatesGlobal.size()>0){
				// Draw candidates
				if(debug)	Log.i(TAG,"Drawing candidates");
				for(double[] candidate : userPosition.candidatesGlobal){
					LatLng candidatePos = new LatLng(candidate[0],
							candidate[1]);
					Marker candidateMarker = map.addMarker(new MarkerOptions()
					.position(candidatePos)
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.dot_orange))
					.anchor(0.5f, 0.5f));
					candidateMarkers.add(candidateMarker);
				}
				candidateMarkersExist = true;
			}
		} else {
			if(userPosition.candidatesGlobal!=null && userPosition.candidatesGlobal.size()>0){
				// Update position of candidates
				if(debug)	Log.i(TAG,"Updating candidates");
				for(int i=0; i < userPosition.candidatesGlobal.size(); i++){
					LatLng candidatePos = new LatLng(userPosition.candidatesGlobal.get(i)[0],
							userPosition.candidatesGlobal.get(i)[1]);
					candidateMarkers.get(i).setPosition(candidatePos);
					double candidateFloor = userPosition.candidatesLocal.get(i)[2];
					double userFloor = userPosition.positionLocal[2];
					int id = R.drawable.dot_orange;
					if(candidateFloor != userFloor){
						if(candidateFloor > userFloor){
							id = R.drawable.dot_red;
						} else{
							id = R.drawable.dot_blue;
						}
					}
					candidateMarkers.get(i).setIcon(BitmapDescriptorFactory.fromResource(id));
				}
			}
			else{
				// Remove markers
				if(debug)	Log.i(TAG,"Removing candidates");
				for(Marker cMarker:candidateMarkers){
					cMarker.remove();
				}
				candidateMarkers.clear();
				candidateMarkersExist = false;
			}
		}
	}
	
	public void drawDetections(ArrayList<Point3D> posDetections, int floor){
		// Remove detection markers if they exist
		if(debug)	Log.i(TAG,"Removing detections");
		for(Marker dMarker : detectionMarkers){
			dMarker.remove();
		}
		detectionMarkers.clear();
		// Draw detections
		if(debug)	Log.i(TAG,"Drawing detections");
		for(Point3D posDet : posDetections){
			//if(posDet.z==floor){
				LatLng detectionPos = new LatLng(posDet.x, posDet.y);
				Marker detectionMarker = map.addMarker(new MarkerOptions()
				.position(detectionPos)
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.dot_red))
				.anchor(0.5f, 0.5f));
				detectionMarkers.add(detectionMarker);
			//}
		}
	}
	
	public void drawEdges(ArrayList<Edge> edges){
		// Remove edge lines if they exist
		if(debug)	Log.i(TAG,"Removing edges");
		for(Polyline edgeLine : edgeLines){
			edgeLine.remove();
		}
		edgeLines.clear();
		// Draw edges
		if(debug)	Log.i(TAG,"Drawing edges");
		if(edges!=null){
			PolylineOptions polyLineOptions = null;
			for (Edge e : edges) {
				LatLng start = new LatLng(e.getStartNode().x, e.getStartNode().y);
				LatLng end = new LatLng(e.getEndNode().x, e.getEndNode().y);
				polyLineOptions = new PolylineOptions();
				polyLineOptions.add(start);
				polyLineOptions.add(end);
				polyLineOptions.width(5);
				polyLineOptions.color(Color.BLUE);
				Polyline polyline = map.addPolyline(polyLineOptions);
				edgeLines.add(polyline);
			}
		}
	}
	
}
