package com.ascamm.ascammnav.utils;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ascamm.ascammnav.R;
import com.ascamm.pois.spatialite.GeoFence;

public class FenceAdapter extends ArrayAdapter<GeoFence>{

    Context context; 
    int layoutResourceId;    
    ArrayList<GeoFence> data = new ArrayList<GeoFence>();
    final int FENCE_ID_SELECT_FROM_MAP = -1;
    int selectedFenceId = -2;
    
    public FenceAdapter(Context context, int layoutResourceId, ArrayList<GeoFence> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        setData(data);
    }
    
    public void setData(ArrayList<GeoFence> data){
    	this.data = data;
    	GeoFence selectFromMap = new GeoFence(FENCE_ID_SELECT_FROM_MAP, FENCE_ID_SELECT_FROM_MAP, "Select from map");
    	data.add(selectFromMap);
    }
    
    public void setSelectedFenceId(int id){
    	selectedFenceId = id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        fenceHolder holder = null;
        
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            
            holder = new fenceHolder();
            holder.layPoi = (LinearLayout)row.findViewById(R.id.lay_poi);
            holder.layPoiPos = (LinearLayout)row.findViewById(R.id.lay_poi_pos);
            holder.txtName = (TextView)row.findViewById(R.id.poi_name);
            //holder.txtName = (CheckedTextView)row.findViewById(R.id.poi_name);
            holder.txtPos = (TextView)row.findViewById(R.id.poi_pos);
            
            row.setTag(holder);
        }
        else
        {
            holder = (fenceHolder)row.getTag();
        }
        
        GeoFence geoFence = data.get(position);
        holder.txtName.setText(geoFence.getName());
        if(geoFence.getId()== FENCE_ID_SELECT_FROM_MAP){
        	holder.layPoiPos.setVisibility(View.INVISIBLE);
        }
        else{
        	holder.layPoiPos.setVisibility(View.VISIBLE);
        	String text = "";
        	if(geoFence.isMultiple()){
        		text = "MULTIPLE";
        	}
        	holder.txtPos.setText(text);
            
        }
        if(geoFence.getId()== selectedFenceId){
        	holder.txtName.setTextColor(context.getResources().getColor(R.color.ascamm_dialog_clicked_text));
        	holder.txtPos.setTextColor(context.getResources().getColor(R.color.ascamm_dialog_clicked_text));
        	holder.layPoi.setBackgroundColor(context.getResources().getColor(R.color.ascamm_dialog_clicked_background));
        }
        else{
        	holder.txtName.setTextColor(Color.BLACK);
        	holder.txtPos.setTextColor(Color.GRAY);
        	holder.layPoi.setBackgroundDrawable(null);
        }
        
        return row;
    }
    
    static class fenceHolder
    {
    	LinearLayout layPoi;
    	LinearLayout layPoiPos;
    	TextView txtName;
    	//CheckedTextView txtName;
        TextView txtPos;
    }
}