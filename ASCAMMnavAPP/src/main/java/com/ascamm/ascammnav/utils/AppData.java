package com.ascamm.ascammnav.utils;

import java.io.File;

import android.os.Environment;

/**
 * This class contains constants used by the program.
 */

public final class AppData {
	
	public static final boolean DEBUGMODE = true;
	
	public static final int NOT_SET = Integer.MIN_VALUE;
	
	public static final String APP_PREFERENCES_NAME = "ASCAMMNavPreferences";
	public static final String APP_DATA_FOLDER_NAME = "ASCAMMnav";
	
	public static final File APP_DATA_FOLDER = new File (Environment.getExternalStorageDirectory() 
			+ "/" + APP_DATA_FOLDER_NAME);
	
	public static String PREFERENCES_FILENAME = "com.ascamm.ascammnav_preferences";
    //public static String PREFERENCES_FILENAME = "es.casabatllo_preferences";

	public static final boolean START_POS_SERVICE_ON_BOOT = false;
}
