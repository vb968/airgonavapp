package com.ascamm.ascammnav.ui;

import android.os.Bundle;

/**
 * Created by Dani Fernandez on 10/05/16.
 */
public class PreferencesActivity extends BaseActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction().replace(android.R.id.content, new Preferences()).commit();
    }
}