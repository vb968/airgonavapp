package com.ascamm.ascammnav.ui;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;

import com.ascamm.ascammnav.R;
import com.ascamm.motion.utils.Settings;

public class Preferences extends PreferenceFragment implements OnSharedPreferenceChangeListener {
	/** General settings **/
	public static final String KEY_CHECKBOX_USE_WIFI = "use_wifi";
    public static final String KEY_CHECKBOX_USE_BLE = "use_ble";
    public static final String KEY_CHECKBOX_USE_IBEACONS = "use_ibeacons";
    public static final String KEY_CHECKBOX_USE_GPS = "use_gps";
    public static final String KEY_CHECKBOX_USE_KALMAN_RSSI = "use_kalman_rssi";
    public static final String KEY_CHECKBOX_USE_WEIGHTED_RSSI = "use_weighted_rssi";
    public static final String KEY_CHECKBOX_USE_DETECTIONS = "use_detections";
    public static final String KEY_EDIT_TEXT_BLE_RSS_DETECTION = "ble_rss_detection";
    public static final String KEY_CHECKBOX_USE_SENSORS = "use_sensors";
    public static final String KEY_CHECKBOX_SITE_INFO_MANUAL = "site_info_manual";
    public static final String KEY_EDIT_TEXT_SITE_ID = "site_id";
    public static final String KEY_CHECKBOX_DEMO_MODE = "demo_mode";
    public static final String KEY_CHECKBOX_STORE_LOGS = "store_logs";
    public static final String KEY_EDIT_TEXT_LOGS_FILENAME = "logs_filename";
    public static final String KEY_CHECKBOX_SEND_POSITIONS = "send_positions";
    public static final String KEY_EDIT_TEXT_SEND_POSITIONS_TIME = "send_positions_time";
    public static final String KEY_EDIT_TEXT_SEND_POSITIONS_SERVER = "send_positions_server";
    /** Algorithm settings **/
    public static final String KEY_LIST_POSITION_CALCULATION = "position_calculation";
	public static final String KEY_EDIT_TEXT_SERVER_OBTAIN = "server_to_obtain";
	public static final String KEY_EDIT_TEXT_TIME_WINDOW = "time_window";
	public static final String KEY_LIST_ESTIMATION_TYPE = "estimation_type";
    public static final String KEY_CHECKBOX_FLOOR_HYSTERESIS = "floor_hysteresis";
    public static final String KEY_EDIT_TEXT_FLOOR_HYSTERESIS = "num_floor_hysteresis";
    public static final String KEY_CHECKBOX_RSS_TRANSFORMATION = "rss_transf";
    public static final String KEY_CHECKBOX_USE_PARTICLE_FILTER = "use_particle_filter";
    public static final String KEY_CHECKBOX_USE_PARTICLE_FILTER_SENSORS = "use_particle_filter_sensors";
    public static final String KEY_CHECKBOX_USE_MAP_FUSION = "use_map_fusion";
    /** UI settings **/
    public static final String KEY_CHECKBOX_CANDIDATES = "show_candidates";
    public static final String KEY_CHECKBOX_SHOW_SCANSET = "show_scanset";
    public static final String KEY_CHECKBOX_SHOW_SUBSET = "show_subset";
    public static final String KEY_CHECKBOX_SHOW_PATHS = "show_paths";
    public static final String KEY_CHECKBOX_SHOW_ORIENTATION = "show_orientation";
    public static final String KEY_CHECKBOX_SHOW_STEPS = "show_steps";
    public static final String KEY_CHECKBOX_SHOW_FILTER_PARTICLES = "show_filter_particles";
    public static final String KEY_CHECKBOX_USE_GOOGLE_MAPS = "use_google_maps";



    /** General settings **/
    private CheckBoxPreference mCheckBoxUseWifi;
    private CheckBoxPreference mCheckBoxUseBle;
    private CheckBoxPreference mCheckBoxUseIBeacons;
    private CheckBoxPreference mCheckBoxUseGps;
    private CheckBoxPreference mCheckBoxUseKalmanRSSI;
    private CheckBoxPreference mCheckBoxUseWeightedRSSI;
    private CheckBoxPreference mCheckBoxUseDetections;
    private EditTextPreference mEditTextBleRssDetection;
    private CheckBoxPreference mCheckBoxUseSensors;
    private CheckBoxPreference mCheckBoxSiteInfoManual;
    private EditTextPreference mEditTextSiteId;
    private CheckBoxPreference mCheckBoxDemoMode;
    private CheckBoxPreference mCheckBoxStoreLogs;
    private EditTextPreference mEditTextLogsFilename;
    private CheckBoxPreference mCheckBoxSendPositions;
    private EditTextPreference mEditTextSendPositionsTime;
    private EditTextPreference mEditTextSendPositionsServer;
    /** Algorithm settings **/
    private ListPreference mListPositionCalculation;
    private EditTextPreference mEditTextServerObtain;
    private EditTextPreference mEditTextTimeWindow;
    private ListPreference mListEstimationType;
    private CheckBoxPreference mCheckBoxFloorHysteresis;
    private EditTextPreference mEditTextNumFloorHysteresis;
    private CheckBoxPreference mCheckBoxRssTransformation;
    private CheckBoxPreference mCheckBoxUseParticleFilter;
    private CheckBoxPreference mCheckBoxUseParticleFilterSensors;
    private CheckBoxPreference mCheckBoxUseMapFusion;
    /** UI settings **/
    private CheckBoxPreference mCheckBoxCandidates;
    private CheckBoxPreference mCheckBoxShowScanset;
    private CheckBoxPreference mCheckBoxShowSubset;
    private CheckBoxPreference mCheckBoxShowPaths;
    private CheckBoxPreference mCheckBoxShowOrientation;
    private CheckBoxPreference mCheckBoxShowSteps;
    private CheckBoxPreference mCheckBoxShowFilterParticles;
    private CheckBoxPreference mCheckBoxUseGoogleMaps;



    /** General settings **/
    public static final boolean KEY_USE_WIFI_DEFAULT = Settings.useWifi;
    public static final boolean KEY_USE_BLE_DEFAULT = Settings.useBle;
    public static final boolean KEY_USE_IBEACONS_DEFAULT = Settings.useIBeacons;
    public static final boolean KEY_USE_GPS_DEFAULT = Settings.useGps;
    public static final boolean KEY_USE_KALMAN_RSSI_DEFAULT = Settings.useKalmanRSSI;
    public static final boolean KEY_USE_WEIGHTED_RSSI_DEFAULT = Settings.useWeightedRSSI;
    public static final boolean KEY_USE_DETECTIONS_DEFAULT = Settings.useDetections;
    public static final String KEY_BLE_RSS_DETECTION_DEFAULT = String.valueOf(Settings.bleRssDetection);
    public static final boolean KEY_USE_SENSORS_DEFAULT = Settings.useSensors;
    public static final boolean KEY_SITE_INFO_MANUAL_DEFAULT = Settings.siteInfoManual;
    public static final String KEY_SITE_ID_DEFAULT = String.valueOf(Settings.siteFromSettings);
    public static final boolean KEY_DEMO_MODE_DEFAULT = Settings.demoMode;
    public static final boolean KEY_STORE_LOGS_DEFAULT = Settings.storeLogs;
    public static final String KEY_LOGS_FILENAME_DEFAULT = Settings.logsFilename;
    public static final boolean KEY_SEND_POSITIONS_DEFAULT = false;
    public static final String KEY_SEND_POSITIONS_TIME_DEFAULT = String.valueOf(Settings.sendPositionsTime);
    public static final String KEY_SEND_POSITIONS_SERVER_DEFAULT = Settings.sendPositionsServer;
    /** Algorithm settings **/
    public static final String KEY_POSITION_CALCULATION_DEFAULT = "local";
    public static final String KEY_SERVER_OBTAIN_DEFAULT = Settings.lnsUrl;
    public static final String KEY_TIME_WINDOW_DEFAULT = String.valueOf(Settings.timeWindow);
    public static final String KEY_ESTIMATION_TYPE_DEFAULT = Settings.estimationType;
    public static final boolean KEY_USE_FLOOR_HYSTERESIS_DEFAULT = false;
    public static final String KEY_FLOOR_HYSTERESIS_DEFAULT = String.valueOf(Settings.numPointsHyst);
    public static final boolean KEY_USE_RSS_TRANSFORMATION_DEFAULT = Settings.rssTransf;
    public static final boolean KEY_USE_PARTICLE_FILTER_DEFAULT = Settings.useParticleFilter;
    public static final boolean KEY_USE_PARTICLE_FILTER_SENSORS_DEFAULT = Settings.useParticleFilterSensors;
    public static final boolean KEY_USE_MAP_FUSION_DEFAULT = Settings.useMapFusion;
    /** UI settings **/
    public static final boolean KEY_SHOW_CANDIDATES_DEFAULT = false;
    public static final boolean KEY_SHOW_SCANSET_DEFAULT = false;
    public static final boolean KEY_SHOW_SUBSET_DEFAULT = false;
    public static final boolean KEY_SHOW_PATHS_DEFAULT = false;
    public static final boolean KEY_SHOW_ORIENTATION_DEFAULT = false;
    public static final boolean KEY_SHOW_STEPS_DEFAULT = false;
    public static final boolean KEY_SHOW_FILTER_PARTICLES_DEFAULT = false;
    public static final boolean KEY_USE_GOOGLE_MAPS_DEFAULT = false;


	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Load the XML preferences file
        addPreferencesFromResource(R.xml.preferences);
        // Get a reference to the preferences
        /** General settings **/
        mCheckBoxUseWifi = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_USE_WIFI);
        mCheckBoxUseBle = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_USE_BLE);
        mCheckBoxUseIBeacons = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_USE_IBEACONS);
        mCheckBoxUseGps = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_USE_GPS);
        mCheckBoxUseKalmanRSSI = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_USE_KALMAN_RSSI);
        mCheckBoxUseWeightedRSSI = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_USE_WEIGHTED_RSSI);
        mCheckBoxUseDetections = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_USE_DETECTIONS);
        mEditTextBleRssDetection = (EditTextPreference) getPreferenceScreen()
                .findPreference(KEY_EDIT_TEXT_BLE_RSS_DETECTION);
        mCheckBoxUseSensors = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_USE_SENSORS);
        mCheckBoxSiteInfoManual = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_SITE_INFO_MANUAL);
        mEditTextSiteId = (EditTextPreference) getPreferenceScreen()
                .findPreference(KEY_EDIT_TEXT_SITE_ID);
        mCheckBoxDemoMode = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_DEMO_MODE);
        mCheckBoxStoreLogs = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_STORE_LOGS);
        mEditTextLogsFilename = (EditTextPreference) getPreferenceScreen()
                .findPreference(KEY_EDIT_TEXT_LOGS_FILENAME);
        mCheckBoxSendPositions = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_SEND_POSITIONS);
        mEditTextSendPositionsTime = (EditTextPreference) getPreferenceScreen()
                .findPreference(KEY_EDIT_TEXT_SEND_POSITIONS_TIME);
        mEditTextSendPositionsServer = (EditTextPreference) getPreferenceScreen()
                .findPreference(KEY_EDIT_TEXT_SEND_POSITIONS_SERVER);
        /** Algorithm settings **/
        mListPositionCalculation = (ListPreference) getPreferenceScreen()
                .findPreference(KEY_LIST_POSITION_CALCULATION);
        mEditTextServerObtain = (EditTextPreference) getPreferenceScreen()
                .findPreference(KEY_EDIT_TEXT_SERVER_OBTAIN);
        mEditTextTimeWindow = (EditTextPreference) getPreferenceScreen()
                .findPreference(KEY_EDIT_TEXT_TIME_WINDOW);
        mListEstimationType = (ListPreference) getPreferenceScreen()
                .findPreference(KEY_LIST_ESTIMATION_TYPE);
        mCheckBoxFloorHysteresis = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_FLOOR_HYSTERESIS);
        mEditTextNumFloorHysteresis = (EditTextPreference) getPreferenceScreen()
                .findPreference(KEY_EDIT_TEXT_FLOOR_HYSTERESIS);
        mCheckBoxRssTransformation = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_RSS_TRANSFORMATION);
        mCheckBoxUseParticleFilter = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_USE_PARTICLE_FILTER);
        mCheckBoxUseParticleFilterSensors = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_USE_PARTICLE_FILTER_SENSORS);
        mCheckBoxUseMapFusion = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_USE_MAP_FUSION);
        /** UI settings **/
        mCheckBoxCandidates = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_CANDIDATES);
        mCheckBoxShowScanset = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_SHOW_SCANSET);
        mCheckBoxShowSubset = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_SHOW_SUBSET);
        mCheckBoxShowPaths = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_SHOW_PATHS);
        mCheckBoxShowOrientation = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_SHOW_ORIENTATION);
        mCheckBoxShowSteps = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_SHOW_STEPS);
        mCheckBoxShowFilterParticles = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_SHOW_FILTER_PARTICLES);
        mCheckBoxUseGoogleMaps = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(KEY_CHECKBOX_USE_GOOGLE_MAPS);
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();
        // Setup the initial values
        /** General settings **/
        mCheckBoxUseWifi
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_USE_WIFI, false) ? 
                    "Enabled" : "Disabled");
        mCheckBoxUseBle
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_USE_BLE, false) ? 
                    "Enabled" : "Disabled");
        mCheckBoxUseIBeacons
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_USE_IBEACONS, false) ? 
                    "Enabled" : "Disabled");
        mCheckBoxUseGps
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_USE_GPS, false) ? 
                    "Enabled" : "Disabled");
        mCheckBoxUseKalmanRSSI
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_USE_KALMAN_RSSI, false) ? 
                    "Enabled" : "Disabled");
        mCheckBoxUseWeightedRSSI
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_USE_WEIGHTED_RSSI, false) ? 
                    "Enabled" : "Disabled");
        mCheckBoxUseDetections
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_USE_DETECTIONS, false) ?
                "Enabled" : "Disabled");
        mEditTextBleRssDetection.setSummary(sharedPreferences.getString(KEY_EDIT_TEXT_BLE_RSS_DETECTION, ""));
        mCheckBoxUseSensors
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_USE_SENSORS, false) ?
                "Enabled" : "Disabled");
        mCheckBoxSiteInfoManual
                .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_SITE_INFO_MANUAL, false) ?
                        "Enabled" : "Disabled");
        mEditTextSiteId.setSummary(sharedPreferences.getString(KEY_EDIT_TEXT_SITE_ID, ""));
        mCheckBoxDemoMode
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_DEMO_MODE, false) ?
                "Enabled" : "Disabled");
        mCheckBoxStoreLogs
                .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_STORE_LOGS, false) ?
                    "Enabled" : "Disabled");
        mEditTextLogsFilename.setSummary(sharedPreferences.getString(KEY_EDIT_TEXT_LOGS_FILENAME, ""));
        mCheckBoxSendPositions
                .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_SEND_POSITIONS, false) ?
                        "Enabled" : "Disabled");
        mEditTextSendPositionsTime.setSummary(sharedPreferences.getString(KEY_EDIT_TEXT_SEND_POSITIONS_TIME, "") + "s");
        mEditTextSendPositionsServer.setSummary(sharedPreferences.getString(KEY_EDIT_TEXT_SEND_POSITIONS_SERVER, ""));
        /** Algorithm settings **/
        mListPositionCalculation.setSummary(sharedPreferences.getString(KEY_LIST_POSITION_CALCULATION, ""));
        mEditTextServerObtain.setSummary(sharedPreferences.getString(KEY_EDIT_TEXT_SERVER_OBTAIN, ""));
        mEditTextTimeWindow.setSummary(sharedPreferences.getString(KEY_EDIT_TEXT_TIME_WINDOW, "") + "s");
        mListEstimationType.setSummary(sharedPreferences.getString(KEY_LIST_ESTIMATION_TYPE, ""));
        mCheckBoxFloorHysteresis
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_FLOOR_HYSTERESIS, false) ? 
                    "Enabled" : "Disabled");
        mEditTextNumFloorHysteresis.setSummary(sharedPreferences.getString(KEY_EDIT_TEXT_FLOOR_HYSTERESIS, ""));
        mCheckBoxRssTransformation
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_RSS_TRANSFORMATION, false) ? 
                    "Enabled" : "Disabled");
        mCheckBoxUseParticleFilter
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_USE_PARTICLE_FILTER, false) ? 
                    "Enabled" : "Disabled");
        mCheckBoxUseParticleFilterSensors
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_USE_PARTICLE_FILTER_SENSORS, false) ? 
                    "Enabled" : "Disabled");
        mCheckBoxUseMapFusion
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_USE_MAP_FUSION, false) ? 
                    "Enabled" : "Disabled");
        /** UI settings **/
        mCheckBoxCandidates
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_CANDIDATES, false) ? 
                    "Enabled" : "Disabled");
        mCheckBoxShowScanset
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_SHOW_SCANSET, false) ? 
                    "Enabled" : "Disabled");
        mCheckBoxShowSubset
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_SHOW_SUBSET, false) ? 
                    "Enabled" : "Disabled");
        mCheckBoxShowPaths
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_SHOW_PATHS, false) ? 
                    "Enabled" : "Disabled");
        mCheckBoxShowOrientation
                .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_SHOW_ORIENTATION, false) ?
                        "Enabled" : "Disabled");
        mCheckBoxShowSteps
                .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_SHOW_STEPS, false) ?
                        "Enabled" : "Disabled");
        mCheckBoxShowFilterParticles
                .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_SHOW_FILTER_PARTICLES, false) ?
                        "Enabled" : "Disabled");
        mCheckBoxUseGoogleMaps
        .setSummary(sharedPreferences.getBoolean(KEY_CHECKBOX_USE_GOOGLE_MAPS, false) ? 
                    "Enabled" : "Disabled");
        
        // Set up a listener whenever a key changes
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Unregister the listener whenever a key changes
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
            String key) {
        // Let's do something a preference value changes
    	/** General settings **/
    	if (key.equals(KEY_CHECKBOX_USE_WIFI)) {
            mCheckBoxUseWifi.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_USE_BLE)) {
            mCheckBoxUseBle.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_USE_IBEACONS)) {
            mCheckBoxUseIBeacons.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_USE_GPS)) {
            mCheckBoxUseGps.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_USE_KALMAN_RSSI)) {
            mCheckBoxUseKalmanRSSI.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_USE_WEIGHTED_RSSI)) {
            mCheckBoxUseWeightedRSSI.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_USE_DETECTIONS)) {
            mCheckBoxUseDetections.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_EDIT_TEXT_BLE_RSS_DETECTION)) {
            mEditTextBleRssDetection.setSummary(sharedPreferences.getString(key, ""));
        }
        else if (key.equals(KEY_CHECKBOX_USE_SENSORS)) {
            mCheckBoxUseSensors.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_SITE_INFO_MANUAL)) {
            mCheckBoxSiteInfoManual.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_EDIT_TEXT_SITE_ID)) {
        	mEditTextSiteId.setSummary(sharedPreferences.getString(key, ""));
        }
        else if (key.equals(KEY_CHECKBOX_DEMO_MODE)) {
            mCheckBoxDemoMode.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_STORE_LOGS)) {
            mCheckBoxStoreLogs.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_EDIT_TEXT_LOGS_FILENAME)) {
        	mEditTextLogsFilename.setSummary(sharedPreferences.getString(key, ""));
        }
        else if (key.equals(KEY_CHECKBOX_SEND_POSITIONS)) {
            mCheckBoxSendPositions.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_EDIT_TEXT_SEND_POSITIONS_TIME)) {
            mEditTextSendPositionsTime.setSummary(sharedPreferences.getString(key, "") + "s");
        }
        else if (key.equals(KEY_EDIT_TEXT_SEND_POSITIONS_SERVER)) {
            mEditTextSendPositionsServer.setSummary(sharedPreferences.getString(key, ""));
        }
        /** Algorithm settings **/
        else if (key.equals(KEY_LIST_POSITION_CALCULATION)) {
    		mListPositionCalculation.setSummary(sharedPreferences.getString(key, ""));
        }
    	else if (key.equals(KEY_EDIT_TEXT_SERVER_OBTAIN)) {
        	mEditTextServerObtain.setSummary(sharedPreferences.getString(key, ""));
        }
    	else if (key.equals(KEY_EDIT_TEXT_TIME_WINDOW)) {
        	mEditTextTimeWindow.setSummary(sharedPreferences.getString(key, "") + "s");
        }
        else if (key.equals(KEY_LIST_ESTIMATION_TYPE)) {
    		mListEstimationType.setSummary(sharedPreferences.getString(key, ""));
        }
    	else if (key.equals(KEY_CHECKBOX_FLOOR_HYSTERESIS)) {
            mCheckBoxFloorHysteresis.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_EDIT_TEXT_FLOOR_HYSTERESIS)) {
        	mEditTextNumFloorHysteresis.setSummary(sharedPreferences.getString(key, ""));
        }
        else if (key.equals(KEY_CHECKBOX_RSS_TRANSFORMATION)) {
            mCheckBoxRssTransformation.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_USE_PARTICLE_FILTER)) {
            mCheckBoxUseParticleFilter.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_USE_PARTICLE_FILTER_SENSORS)) {
            mCheckBoxUseParticleFilterSensors.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_USE_MAP_FUSION)) {
            mCheckBoxUseMapFusion.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
    	/** UI settings **/
        else if (key.equals(KEY_CHECKBOX_CANDIDATES)) {
            mCheckBoxCandidates.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_SHOW_SCANSET)) {
            mCheckBoxShowScanset.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_SHOW_SUBSET)) {
            mCheckBoxShowSubset.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_SHOW_PATHS)) {
            mCheckBoxShowPaths.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_SHOW_ORIENTATION)) {
            mCheckBoxShowOrientation.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_SHOW_STEPS)) {
            mCheckBoxShowSteps.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_SHOW_FILTER_PARTICLES)) {
            mCheckBoxShowFilterParticles.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
        else if (key.equals(KEY_CHECKBOX_USE_GOOGLE_MAPS)) {
            mCheckBoxUseGoogleMaps.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "Enabled" : "Disabled");
        }
    }

}
