package com.ascamm.ascammnav.ui;

import android.os.Bundle;
import android.widget.TextView;

import com.ascamm.ascammnav.R;

public class Message extends BaseActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.message);
		
		String message = getIntent().getStringExtra("message");
		TextView message_text = (TextView) findViewById(R.id.message);
		message_text.setText(message);
	}
	
}
