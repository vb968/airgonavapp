package com.ascamm.ascammnav.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.ascamm.ascammnav.R;

public class MainMenu extends BaseActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);
	}
	
	public void locateIndoorMaps(View view){
		Intent i = new Intent(this, LocationIndoor.class);
		startActivity(i);
	}

	public void locateIndoorMicello(View view){
		Intent i = new Intent(this, LocationIndoorMicello.class);
		startActivity(i);
	}
	
	public void monitor(View view){
		Toast.makeText(this, "option not available yet", Toast.LENGTH_SHORT).show();
	}
	
	public void settings(View view){
		Intent i = new Intent(this, PreferencesActivity.class);
		startActivity(i);
	}
}
