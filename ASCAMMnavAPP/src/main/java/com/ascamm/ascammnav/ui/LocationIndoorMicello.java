package com.ascamm.ascammnav.ui;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.ascamm.ascammnav.R;
import com.ascamm.ascammnav.database.LocalDBHelper;
import com.ascamm.ascammnav.map.Map;
import com.ascamm.ascammnav.map.MicelloMapView;
import com.ascamm.ascammnav.utils.AppData;
import com.ascamm.ascammnav.utils.MapInfo;
import com.ascamm.ascammnav.utils.Media;
import com.ascamm.ascammnav.utils.VersionInfo;
import com.ascamm.ascammnav.ws.DownloadImageAT;
import com.ascamm.ascammnav.ws.DownloadImageCompleteListener;
import com.ascamm.ascammnav.ws.DownloadImageResult;
import com.ascamm.motion.LocationData;
import com.ascamm.motion.PosActions;
import com.ascamm.motion.PositioningService;
import com.ascamm.motion.PositioningServiceBinder;
import com.ascamm.motion.algorithm.AutocalibrationParameters;
import com.ascamm.motion.algorithm.CoordConversion;
import com.ascamm.motion.database.DBHelper;
import com.ascamm.motion.detections.Detection;
import com.ascamm.motion.detections.DetectionListener;
import com.ascamm.motion.mems.MotionMode;
import com.ascamm.motion.routing.Route;
import com.ascamm.motion.routing.RouteListener;
import com.ascamm.motion.utils.Constants;
import com.ascamm.motion.utils.DbListener;
import com.ascamm.motion.utils.Edge;
import com.ascamm.motion.utils.LogWriter;
import com.ascamm.motion.utils.MotionListener;
import com.ascamm.motion.utils.MotionStepsInfoListener;
import com.ascamm.motion.utils.OnlineItem;
import com.ascamm.motion.utils.Point3DWithFloor;
import com.ascamm.motion.utils.PositionsListener;
import com.ascamm.motion.utils.ScansetListener;
import com.ascamm.motion.utils.Settings;
import com.ascamm.motion.utils.UpdateAutocalibrationParametersListener;
import com.ascamm.motion.ws.WebService;
import com.ascamm.motion.ws.WebServiceAT;
import com.ascamm.motion.ws.WebServiceCompleteListener;
import com.ascamm.motion.ws.WebServiceResult;
import com.ascamm.utils.LatLng3D;
import com.ascamm.utils.Point3D;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

public class LocationIndoorMicello extends BaseActivity {

	private final String TAG = this.getClass().getSimpleName();
	private boolean debug = true;

	private PositioningService mPositioningService;
	private Intent posServiceIntent;
	private boolean posServiceStarted = false;
	private boolean posServiceBound = true;
	private boolean posServiceIsLocating = false;
	private boolean useBoundPosService = true;
	private boolean runForegroundService = true;
	private int NOTIFICATION_ID = 100;

	private boolean isFirstPosition = true;
	private LatLng firstPosition = null;
	private boolean showCandidates = false, useFloorHyst = false, sendLocations = false, rssTransf = false, useWifi = false, useBle = false, useIBeacons = false, useGps = false, useParticleFilter = false, useParticleFilterSensors = false, useMapFusion = false, useKalmanRSSI = false, useWeightedRSSI = false, useDetections = false, showScanset = false, showSubset = false, showPaths = false, showOrientation = false, showSteps = false, showFilterParticles = false, demoMode = false, useSensors = false;
	private boolean dialogDetectionsActive = false;
	private int mCurrentFloor = 0;
	private String sendLocationsServer, LNSserver, positionCalculation, estimationType, logsFilename;
	private int timeWindow = 0, numPointsHyst = 0, sendLocationsTime = 0;
	private int POSITION_REFRESH_TIME = 1000; // Milliseconds
	private float bleRssDetection = 0;

	private MenuItem playMenu;
	private ProgressDialog dialog;

	private boolean siteInfoManual = false;
	private int siteId = 0;
	private boolean hasLocalVersionData = true;
	private boolean mapsOk = false;
	private boolean mapsDownloadOk = false;
	private boolean mapsNeedUpdate = false;
	private int apsRemoteVersion, mapsRemoteVersion, nodesEdgesRemoteVersion;
	private ArrayList<MapInfo> mapsList;
	private ArrayList<Boolean> mapsImagesDownload = new ArrayList<Boolean>();
	private LocalDBHelper mLocalDBHelper;
	private boolean mMapVisible = false;

	private DecimalFormat decFor = new DecimalFormat("#.00");

	/*-------------------- Map --------------------*/
	private ArrayList<MapInfo> mMapsInfo;
	private Bitmap mBmpMap;
	private MicelloMapView mMicelloMap;
	private String mMapBasePath;
	private FrameLayout mFlMapWrapper;

	private Media mMedia;
	//Obtain these values from the Server (we can simulate them adding these values to the WS response)
	/*
	// ASCAMM Lab
	double lat = 41.489573582267;
	double lng = 2.1253579068142;
	float width = 44;
	float height = 72;
	float bearing = -18.7f;
	*/
	/*
	// ASCAMM Lab (Marc Compte)
	double lat = 41.4895735930717;
	double lng = 2.12535791799289;
	float width = 46.66f;
	float height = 72;
	float bearing = -18.7f;
	*/
	/*
	//CAR Sant Cugat
	double lat = 41.483322016051;
	double lng = 2.0758443037591;
	float width = 72;
	float height = 127;
	float bearing = 175.778773259f;
	*/

	// Mercat Rubi
	/*
	double lat = 41.48972017;
	double lng = 2.03434322;
	float width = 82.83f;
	float height = 83.11f;
	float bearing = 0;
	*/
	/*
	//CAR Sant Cugat (Marc Compte)
	double lat = 41.483306432;
	double lng = 2.075868909;
	float width = 73.53f;
	float height = 134.06f;
	float bearing = 176.2f;
	*/
	/*
	// Casa Batllo (Marc Compte)
	double mapAnchorLat = 41.39143712186111;
	double mapAnchorLng = 2.1644167136759083;
	float mapWidth = 56.3f;
	float mapHeight = 20.8f;
	float mapBearing = 316.3f;
	*/
	/*
	// Renault Valladolid (Marc Compte)
	double lat = 41.59799845040325;
	double lng = -4.727854671297326;
	float width = 76.3f;
	float height = 79.6f;
	float bearing = 263.7f;
	*/
	/*
	// Eurecat Proves (David Badosa)
	double mapAnchorLat = 41.488882486292;
	double mapAnchorLng = 2.1253324371713;
	float mapWidth = 37.3f;
	float mapHeight = 17.8f;
	float mapBearing = -108.7f;
	*/
	/*
	// Smart Retail (Dani Fernandez)
	double mapAnchorLat = 41.48915962;
	double mapAnchorLng = 2.125727929;
	float mapWidth = 8.27f;
	float mapHeight = 12.18f;
	float mapBearing = -19f;
	*/
	// Illa Diagonal (Dani Fernandez)
	double mapAnchorLat = 41.388951;
	double mapAnchorLng = 2.138567;
	float mapWidth = 8.27f;
	float mapHeight = 12.18f;
	float mapBearing = -161f;

	/*------------------- Route Testing -------------*/
	private boolean testRoute = false;
	private int idRoute = 14;
	private ArrayList<Point3D> route = new ArrayList<Point3D>();
	private int pos = 0;
	private int numPointsRoute = 0;
	private DBHelper dBHelper;
	private boolean dialogShown = false;
	/*-----------------------------------------------*/

	private int totalSteps = 0;
	private int totalMotionModeSteps = 0;
	private int totalNonStepsWalking = 0;

	/*-------------------- Particular APP settings -------------------*/
	private static boolean pdbFromAssets = false;

	/*-------------------- PERMISSIONS (API 23) ----------------------*/
	private static int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
	private static int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2;
	private static int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 3;
	private static int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 4;
	private boolean permissionAccessFineLocation = false;
	private boolean permissionWriteExternalStorage = false;
	private boolean permissionReadPhoneState = true;

	/** Code added for logs **/
	private LogWriter mLogWriter;
	private boolean logsRunning = false, storeLogs = true;
	//private String logsFilename = Constants.logsFilename + "_pos" + Constants.logsFilenameExt;

	/** Broadcast Receivers **/
	private PositionReceiver mPositionReceiver;

	private void updateScansetTable(ArrayList<OnlineItem> scanset){
//		Log.d(TAG, "inside updateScansetTable. numScans: " + scanset.size());
//		LayoutInflater inflater = LocationIndoorMicello.this.getLayoutInflater();

		mMicelloMap.clearScansets();
		for(OnlineItem oi : scanset){
			mMicelloMap.setScanset(oi.getBSSID(), oi.getRSS(), oi.getCount(), (int)oi.getCountPercentage());
		}
	}

	/** Broadcast Receivers **/
	private class PositionReceiver extends BroadcastReceiver {
		private String TAG = getClass().getSimpleName();

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			Log.d(TAG,"action: " + action);
			if(action.equalsIgnoreCase(PosActions.SERVICE_OK)){
				sendIsLocatingToPosService();
				sendSetSiteIdToPosService(siteId);
				sendLocationsConfigToPosService();
			}
			else if(action.equalsIgnoreCase(PosActions.IS_LOCATING_RESP)){
				boolean isLocating = intent.getExtras().getBoolean("isLocating");
				Log.d(TAG, "isLocating: " + isLocating);
				if(isLocating){
					posServiceIsLocating = true;
					changeMenuIcon();
				}
			}
			else if(action.equalsIgnoreCase(PosActions.SET_SITE_ID_RESP)){
				boolean success = intent.getExtras().getBoolean("success");
				Log.d(TAG, "setSiteIdResp: " + success);
			}
			else if(action.equalsIgnoreCase(PosActions.SEND_LOCATIONS_CONFIG_RESP)){
				boolean success = intent.getExtras().getBoolean("success");
				Log.d(TAG, "sendLocationsConfigResp: " + success);
			}
			else if(action.equalsIgnoreCase(PosActions.START_LOCATING_RESP)){
				boolean success = intent.getExtras().getBoolean("success");
				Log.d(TAG, "startLocatingResp: " + success);
			}
			else if(action.equalsIgnoreCase(PosActions.STOP_LOCATING_RESP)){
				boolean success = intent.getExtras().getBoolean("success");
				Log.d(TAG, "stopLocatingResp: " + success);
			}
			else if(action.equalsIgnoreCase(PosActions.NEW_POSITIONS)){
				/*
				LocationData location = (LocationData) intent.getExtras().getSerializable("location");
				LocationData location2 = (LocationData) intent.getExtras().getSerializable("location2");
				ArrayList<LocationData> locationsData = new ArrayList<LocationData>();
				locationsData.add(location);
				locationsData.add(location2);
				*/
				ArrayList<LocationData> locationsData = (ArrayList<LocationData>) intent.getExtras().getSerializable("locations");
				processNewPositions(locationsData);
			}
			else if(action.equalsIgnoreCase(PosActions.NEW_SCANSET)){
				ArrayList<OnlineItem> scanset = (ArrayList<OnlineItem>) intent.getExtras().getSerializable("scanset");
				processNewScanset(scanset);
			}
			else if(action.equalsIgnoreCase(PosActions.NEW_MOTION)){
				int sensorState = intent.getExtras().getInt("sensorState");
				processNewMotion(sensorState);
			}
			else if(action.equalsIgnoreCase(PosActions.NEW_AUTO_PARAMS)){
				double slope = intent.getExtras().getDouble("slope");
				double intersection = intent.getExtras().getDouble("intersection");
				double residuals = intent.getExtras().getDouble("residuals");
				processNewAutoParams(slope, intersection, residuals);
			}
		}
	};

	private void sendLocationsConfigToPosService(){
		Intent intent = new Intent();
		intent.setAction(PosActions.SEND_LOCATIONS_CONFIG);
		intent.putExtra("seconds", sendLocationsTime);
		intent.putExtra("server_url", sendLocationsServer);
		sendBroadcast(intent);
	}

	private void sendSetSiteIdToPosService(int siteId){
		Intent intent = new Intent();
		intent.setAction(PosActions.SET_SITE_ID);
		intent.putExtra("siteId", siteId);
		sendBroadcast(intent);
	}

	private void sendIsLocatingToPosService(){
		Intent intent = new Intent();
		intent.setAction(PosActions.IS_LOCATING);
		sendBroadcast(intent);
	}

	private void sendCalcRouteToPosService(Point3D currentPosition, Point3D destinationPosition){
		Intent intent = new Intent();
		intent.setAction(PosActions.CALC_ROUTE);
		intent.putExtra("current_position", currentPosition);
		intent.putExtra("destination_position", destinationPosition);
		sendBroadcast(intent);
	}

	/* Event listeners */
	private UpdateAutocalibrationParametersListener mAutocalibrationListener = new UpdateAutocalibrationParametersListener() {
		@Override
		public void newUpdate(AutocalibrationParameters autoParams){
			double slope = autoParams.getSlope();
			double intersection = autoParams.getIntersection();
			double residuals = autoParams.getResiduals();
			processNewAutoParams(slope, intersection, residuals);
		}
	};

	private PositionsListener mPositionListener = new PositionsListener() {
		@Override
		public void newPositionAvailable(ArrayList<LocationData> locationsData){
			processNewPositions(locationsData);
		}
	};

	private ScansetListener mScansetListener = new ScansetListener() {
		@Override
		public void newScansetAvailable(ArrayList<OnlineItem> scanset){
			processNewScanset(scanset);
		}
	};

	private MotionListener mMotionListener = new MotionListener() {
		@Override
		public void newMotionAvailable(int sensorState){
			processNewMotion(sensorState);
		}
	};

	private MotionStepsInfoListener mMotionStepsInfoListener = new MotionStepsInfoListener() {
		@Override
		public void newMotionStepsInfoAvailable(int motionMode, ArrayList<Double> stepsList, double orientation, double diffOrientation){
			processNewMotionStepsInfo(motionMode, stepsList, orientation, diffOrientation);
		}
	};

	private void saveFirstPosition(LocationData pos){
		firstPosition = new LatLng(pos.positionGlobal[0], pos.positionGlobal[1]);
	}

	private void processNewPositions(ArrayList<LocationData> locationsData){
		//Log.i(TAG,"relativeOrientation: " + locationsData.get(0).relativeOrientation);
		int numSteps = (int) locationsData.get(0).numSteps;
		if(numSteps != -1) {
			totalSteps += numSteps;
			//Log.i(TAG,"numSteps: " + numSteps + " , totalSteps: " + totalSteps);
		}
		double userOrientation = locationsData.get(0).userOrientation;
		double userOrientationReliability = locationsData.get(0).userOrientationReliability;
		double accuracy = locationsData.get(0).accuracy;
		Log.d(TAG, "userOrientation: " + userOrientation);
		Log.d(TAG, "userOrientationReliability: " + userOrientationReliability);
		Log.d(TAG, "accuracy: " + accuracy);
		if(debug){
			Log.d(TAG,"locationsData(0): (" + locationsData.get(0).positionLocal[0] + "," + locationsData.get(0).positionLocal[1] + "," + locationsData.get(0).positionLocal[2] + "), siteId: " + locationsData.get(0).siteId + " , tech: " + locationsData.get(0).sourceType);
			//Log.d(TAG,"locationsData(1): (" + locationsData.get(1).positionLocal[0] + "," + locationsData.get(1).positionLocal[1] + "," + locationsData.get(1).positionLocal[2] + "), siteId: " + locationsData.get(1).siteId + " , tech: " + locationsData.get(1).sourceType);
			Log.d(TAG,"global(0): (" + locationsData.get(0).positionGlobal[0] + "," + locationsData.get(0).positionGlobal[1] + "," + locationsData.get(0).positionGlobal[2] + ")");
		}
		if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
		if(isFirstPosition){
			//Save first position as the center when using GoogleMaps
			saveFirstPosition(locationsData.get(0));
			isFirstPosition = false;
		}

		drawPosition(locationsData);
	}

	private void processNewScanset(ArrayList<OnlineItem> scanset){
		if(debug){
			Log.d(TAG, "newScanset received");
			/*
			for(OnlineItem oi : scanset){
				Log.i(TAG,"ssid: " + oi.getSSID() + " , mac: " + oi.getBSSID() + " , rss: " + oi.getRSS() + " , stdev: " + oi.getRSSstdev() + " , tech: " + oi.getTech() + " , count: " + oi.getCount() + " , count(%): " + oi.getCountPercentage());
			}
			*/
		}
		updateScansetTable(scanset);
	}

	private void processNewMotion(int sensorState){
		if(debug){
			Log.d(TAG, "newMotion received. sensorState: " + sensorState);
		}
	}

	private void processNewAutoParams(double slope, double intersection, double residuals){
		if(debug)	Log.d(TAG, "newUpdate received. a: " + slope + " , b: " + intersection + " , y: " + residuals + " , mapVisible: " + mMapVisible);
	}

	private void processNewMotionStepsInfo(int motionMode, ArrayList<Double> stepsList, double orientation, double diffOrientation){
		int steps = stepsList.size();
		totalMotionModeSteps += steps;
		boolean nonStepsDetected = false;
		if(motionMode == MotionMode.WALKING.ordinal() && steps==0){
			totalNonStepsWalking += 1;
			nonStepsDetected = true;
		}
		if(debug){
			Log.d(TAG, "newMotionStepsInfo received. motionMode: " + motionMode + " , steps: " + steps + " , totalMotionModeSteps: " + totalMotionModeSteps);
		}
	}

	/** Defines callback for service binding, passed to bindService() */
	private ServiceConnection mPosServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			// We've bound to PositioningService, cast the IBinder and get
			// PositioningService instance
			PositioningServiceBinder binder = (PositioningServiceBinder) service;
			mPositioningService = binder.getService();
			if(mPositioningService.isLocating()){
				posServiceIsLocating = true;
			}
			mPositioningService.setUIcontext(LocationIndoorMicello.this);
			mPositioningService.setSendLocationsConfig(sendLocationsTime, sendLocationsServer);
			mPositioningService.setSiteId(siteId);
			mPositioningService.addPositionsListener(mPositionListener);
			mPositioningService.addAutocalibrationListener(mAutocalibrationListener);
			if(showScanset){
				mPositioningService.addScansetListener(mScansetListener);
			}
			mPositioningService.addMotionListener(mMotionListener);
			mPositioningService.addMotionStepsInfoListener(mMotionStepsInfoListener);
			posServiceBound = true;
			//Toast.makeText(LocationIndoor.this, "PosService connected",Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			posServiceBound = false;
			//Toast.makeText(LocationIndoor.this, "PosService disconnected",Toast.LENGTH_SHORT).show();
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//Log.i(TAG,"inside onCreateOptionsMenu");
		getMenuInflater().inflate(R.menu.menu, menu);
		playMenu = menu.findItem(R.id.start_pos);
		if(posServiceIsLocating){
			changeMenuIcon();
		}
		return true;
	}

	private void changeMenuIcon(){
		if(playMenu!=null){
			//change the icon
			playMenu.setIcon(R.drawable.stop_pos);
			playMenu.setTitle("stop");
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == R.id.start_pos) {
			// app icon in action bar clicked; start/stop PositioningService
			if(!posServiceIsLocating){
				// Validate that GPS, WiFi and BLE are enabled
				LocationManager mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
				boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
				WifiManager wifi = (WifiManager)getApplicationContext().getSystemService(WIFI_SERVICE);
				boolean isWifiEnabled = wifi.isWifiEnabled();
				boolean isBleEnabled = false;
				BluetoothAdapter mBluetoothAdapter = null;
				if(useBle){
					if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
					    Toast.makeText(this, "This device does not support BLE", Toast.LENGTH_SHORT).show();
					}
					else{
						final BluetoothManager bluetoothManager =
						        (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
						mBluetoothAdapter = bluetoothManager.getAdapter();
						isBleEnabled = mBluetoothAdapter.isEnabled();
					}
				}
				if(debug)	Log.d(TAG,"isGPSEnabled: " + isGPSEnabled + " , isWifiEnabled: " + isWifiEnabled + " , isBleEnabled: " + isBleEnabled);

				if(useWifi && !isWifiEnabled){
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage("Change settings before start positioning")
					       .setTitle("WiFi not enabled");
					builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
					    public void onClick(DialogInterface dialog, int which) {
					        // Do my action here
					    	Intent i = new Intent(android.provider.Settings.ACTION_SETTINGS);
							startActivity(i);
					    	dialog.dismiss();
					    }
					});
					builder.setNegativeButton("Enable", new DialogInterface.OnClickListener() {
					    public void onClick(DialogInterface dialog, int which) {
					        // Do my action here
					    	WifiManager wifi = (WifiManager)getApplicationContext().getSystemService(WIFI_SERVICE);
							boolean success = wifi.setWifiEnabled(true);
					    	if(!success){
					    		Toast.makeText(LocationIndoorMicello.this, "WiFi could not be enabled", Toast.LENGTH_SHORT).show();
					    	}
					    	else{
					    		dialog.dismiss();
					    	}
					    }
					});
					AlertDialog alert = builder.create();
					alert.show();
				}
				else if (useBle && (mBluetoothAdapter == null || !isBleEnabled)) {
					// Ensures Bluetooth is available on the device and it is enabled. If not,
					// displays a dialog requesting user permission to enable Bluetooth.

				    AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage("Change settings before start positioning")
					       .setTitle("Bluetooth not enabled");
					builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
					    public void onClick(DialogInterface dialog, int which) {
					        // Do my action here
					    	Intent i = new Intent(android.provider.Settings.ACTION_SETTINGS);
							startActivity(i);
					    	dialog.dismiss();
					    }
					});
					builder.setNegativeButton("Enable", new DialogInterface.OnClickListener() {
					    public void onClick(DialogInterface dialog, int which) {
					        // Do my action here
					    	Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
						    startActivity(enableBtIntent);
						    dialog.dismiss();
					    }
					});
					AlertDialog alert = builder.create();
					alert.show();
				}
				else if(useGps && !isGPSEnabled){
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage("Change settings before start positioning")
					       .setTitle("GPS not enabled");
					builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
					    public void onClick(DialogInterface dialog, int which) {
					        // Do my action here
					    	Intent i = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							startActivity(i);
					    	dialog.dismiss();
					    }
					});
					AlertDialog alert = builder.create();
					alert.show();
				}
				else{
					startLocation();
					//change the icon
					playMenu.setIcon(R.drawable.stop_pos);
					playMenu.setTitle("stop");
				}
			}else{
				stopLocation();
				//change the icon
		    	playMenu.setIcon(R.drawable.start_pos);
		    	playMenu.setTitle("start");
			}
			return true;
		}  else {
			return super.onOptionsItemSelected(item);
		}

	}

	/*
	private void refreshTestingRoute(){
		Log.d(TAG,"inside refreshTestingRoute");
		for(Point3D point : route){
			point.setId(0);
		}
		pos = 0;
	}
	*/

	private String getWifiMacAddress() {
		String wifiMac = null;
		WifiManager manager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		if (manager != null) {
			WifiInfo info = manager.getConnectionInfo();
			if (info != null) {
				String wifiAddress = info.getMacAddress();
				if (wifiAddress != null) {
					wifiMac = wifiAddress.toLowerCase();
				}
			}
		}
		//Log.d(TAG,"wifiMAC: " + wifiMac);
		return wifiMac;
	}

	private String getBleMacAddress() {
		String bleMac = null;
		if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2){
			final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
			BluetoothAdapter mBluetoothAdapter = bluetoothManager.getAdapter();
			bleMac = mBluetoothAdapter.getAddress().toLowerCase();
		}
		//Log.d(TAG,"bleMac: " + bleMac);
		return bleMac;
	}

	private String getIMEI(Context context){
		String imei = null;
		TelephonyManager tManager = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
		if(tManager!=null){
			imei = tManager.getDeviceId();
		}
		//Log.d(TAG, "IMEI: " + imei);
		return imei;
	}

	private String getAndroidId(Context context){
		String androidId = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
		//Log.d(TAG, "androidId: " + androidId);
		return androidId;
	}

	private String getSerialNumber() {
		String serialNumber = Build.SERIAL;
		//Log.d(TAG, "serialNumber: " + serialNumber);
		return serialNumber;
	}

	private String getDeviceId(Context context){
		//String uid = getWifiMacAddress();
		//String uid = getBleMacAddress();
		//String uid = getIMEI(context);
		//String uid = getAndroidId(context);
		String uid = getSerialNumber();
		return uid;
	}

	private boolean checkPermissions(){
		// Here, this is the current activity
		List<String> permissionsNeeded = new ArrayList<>();
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
			permissionAccessFineLocation = true;
		}
		else{
			permissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
			/*
			// Should we show an explanation?
			if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {

				// Show an explanation to the user *asynchronously* -- don't block
				// this thread waiting for the user's response! After the user
				// sees the explanation, try again to request the permission.
				Toast.makeText(this,"Explanation of the permission",Toast.LENGTH_SHORT).show();

			} else {
				// No explanation needed, we can request the permission.
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
			}
			*/
			// No explanation needed, we can request the permission.
			//ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
		}
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
			permissionWriteExternalStorage = true;
		}
		else{
			permissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
			/*
			// Should we show an explanation?
			if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {

				// Show an explanation to the user *asynchronously* -- don't block
				// this thread waiting for the user's response! After the user
				// sees the explanation, try again to request the permission.
				Toast.makeText(this,"Explanation of the permission",Toast.LENGTH_SHORT).show();

			} else {
				// No explanation needed, we can request the permission.
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
			}
			*/
			// No explanation needed, we can request the permission.
			//ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
		}
		if (!permissionsNeeded.isEmpty()) {
			ActivityCompat.requestPermissions(this, permissionsNeeded.toArray(new String[permissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
			return false;
		}
		return true;
	}

	private void setAllPermissionsOk(){
		permissionAccessFineLocation = true;
		permissionWriteExternalStorage = true;
		permissionReadPhoneState = true;
	}

	private boolean getAllPermissionsOk(){
		boolean ok = false;
		if(permissionAccessFineLocation && permissionWriteExternalStorage && permissionReadPhoneState){
			ok = true;
		}
		return ok;
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		if (requestCode == REQUEST_ID_MULTIPLE_PERMISSIONS) {
			java.util.Map<String, Integer> perms = new HashMap<>();
			// Initialize the map with both permissions
			perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
			perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
			// Fill with actual results from user
			if (grantResults.length > 0) {
				for (int i = 0; i < permissions.length; i++)
					perms.put(permissions[i], grantResults[i]);
				// Check for both permissions
				if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
						&& perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
					// all permissions needed granted
					Log.d(TAG, "location and write permissions granted");
					permissionAccessFineLocation = true;
					permissionWriteExternalStorage = true;
					if(getAllPermissionsOk()){
						startServices();
					}
				} else {
					Log.d(TAG, "Some permissions are not granted ask again ");
					showDialogPermissions();
				}
			}
			return;
		}
		else if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				// permission was granted
				permissionAccessFineLocation = true;
				if(getAllPermissionsOk()){
					startServices();
				}
			} else {
				// permission denied
				Toast.makeText(this, "You must accept permissions", Toast.LENGTH_SHORT).show();
				permissionAccessFineLocation = false;
			}
			return;
		}
		else if (requestCode == MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				// permission was granted
				permissionWriteExternalStorage = true;
				if(getAllPermissionsOk()){
					startServices();
				}
			} else {
				// permission denied
				Toast.makeText(this, "You must accept permissions", Toast.LENGTH_SHORT).show();
				permissionWriteExternalStorage = false;
			}
			return;
		}
		else{
			// other 'case' lines to check for other
			// permissions this app might request
			return;
		}
	}

	private void showDialogPermissions() {
		String message = "Location Services and Write Permissions required for this app";
		DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						checkPermissions();
						break;
					case DialogInterface.BUTTON_NEGATIVE:
						// proceed with logic by disabling the related features or quit the app.
						break;
				}
			}
		};
		new AlertDialog.Builder(this)
				.setMessage(message)
				.setPositiveButton("OK", clickListener)
				.setNegativeButton("Cancel", clickListener)
				.create()
				.show();
	}

	private void startServices(){
		if (startService(posServiceIntent) != null) {
			posServiceStarted = true;
		}

		if (useBoundPosService) {
			doBindPosService();
		}
	}

	private void stopServices(){
		if(useBoundPosService){
			try {
				doUnbindPosService();
			} catch (Throwable t) {
				Log.e(TAG, "Failed to unbind from the service", t);
			}
		}
		if(posServiceIsLocating == false){
			if(stopService(posServiceIntent)){
				posServiceStarted = false;
			}
		}
	}

	public void setSettings(){
		Settings.useWifi = useWifi;
		Settings.useBle = useBle;
		Settings.useIBeacons = useIBeacons;
		Settings.useGps = useGps;
		Settings.useKalmanRSSI = useKalmanRSSI;
		Settings.useWeightedRSSI = useWeightedRSSI;
		Settings.useDetections = useDetections;
		Settings.bleRssDetection = bleRssDetection;
		Settings.useSensors = useSensors;
		Settings.siteInfoManual = siteInfoManual;
		Settings.demoMode = demoMode;
		Settings.storeLogs = storeLogs;
		Settings.logsFilename = logsFilename;
		Settings.pdbFromAssets = pdbFromAssets;
		if(positionCalculation.equals("local"))
			Settings.posLocal = true;
		else
			Settings.posLocal = false;
		Settings.lnsUrl = LNSserver;
		Settings.timeWindow = timeWindow;
		Settings.estimationType = estimationType;
		Settings.numPointsHyst = numPointsHyst;
		Settings.rssTransf = rssTransf;
		Settings.useParticleFilter = useParticleFilter;
		Settings.useParticleFilterSensors = useParticleFilterSensors;
		Settings.useMapFusion = useMapFusion;
		Settings.scansetNeeded = showScanset;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTextView("Map is loading...");

		// String API_KEY = "aHSN8BCLh07mFtt1OKxXYYLpxMyfTr";
		// int COMMUNITY_ID = 78; //24373;

		mMicelloMap = new MicelloMapView(this);
		setContentView(mMicelloMap);

		if(!useBoundPosService){
			IntentFilter posFilter = new IntentFilter();
			posFilter.addAction(PosActions.SERVICE_OK);
			posFilter.addAction(PosActions.SEND_LOCATIONS_CONFIG_RESP);
			posFilter.addAction(PosActions.IS_LOCATING_RESP);
			posFilter.addAction(PosActions.START_LOCATING_RESP);
			posFilter.addAction(PosActions.STOP_LOCATING_RESP);
			posFilter.addAction(PosActions.NEW_POSITIONS);
			posFilter.addAction(PosActions.NEW_DETECTIONS);
			posFilter.addAction(PosActions.CALC_ROUTE_RESP);
			posFilter.addAction(PosActions.ERROR);
			posFilter.addAction(PosActions.NEW_SCANSET);
			posFilter.addAction(PosActions.NEW_MOTION);
			posFilter.addAction(PosActions.NEW_AUTO_PARAMS);
			posFilter.addAction(PosActions.NEW_DATABASE_DATA);
			mPositionReceiver = new PositionReceiver();
			registerReceiver(mPositionReceiver, posFilter);
		}

		int apiVersion = Build.VERSION.SDK_INT;
		if(debug)	Log.d(TAG,"apiVersion: " + apiVersion);
		if(apiVersion >= Build.VERSION_CODES.M) {
			checkPermissions();
		}
		else{
			setAllPermissionsOk();
		}
		
		String deviceId = getDeviceId(this);
		if(debug)	Log.d(TAG,"deviceId: " + deviceId);
		
		// Keep screen on
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		/*
		Hasher mHasher = new Hasher();
		String hash = mHasher.toSHA1("11:22:33:44:55:66AsCm@m45123456789");
		Log.d(TAG,"hash: " + hash);
		*/

		posServiceIntent = new Intent(this, PositioningService.class);
	}

	@Override
	protected void onResume() {
		super.onResume();

		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		useWifi = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_WIFI, Preferences.KEY_USE_WIFI_DEFAULT);
		useBle = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_BLE, Preferences.KEY_USE_BLE_DEFAULT);
		useIBeacons = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_IBEACONS, Preferences.KEY_USE_IBEACONS_DEFAULT);
		useGps = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_GPS, Preferences.KEY_USE_GPS_DEFAULT);
		useKalmanRSSI = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_KALMAN_RSSI, Preferences.KEY_USE_KALMAN_RSSI_DEFAULT);
		useWeightedRSSI = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_WEIGHTED_RSSI, Preferences.KEY_USE_WEIGHTED_RSSI_DEFAULT);
		useDetections = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_DETECTIONS, Preferences.KEY_USE_DETECTIONS_DEFAULT);
		if(useDetections){
			String bleRssString = sharedPref.getString(Preferences.KEY_EDIT_TEXT_BLE_RSS_DETECTION, Preferences.KEY_BLE_RSS_DETECTION_DEFAULT);
			if(bleRssString != null && bleRssString != ""){
				bleRssDetection = Float.valueOf(bleRssString);
			}
		}
		useSensors = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_SENSORS, Preferences.KEY_USE_SENSORS_DEFAULT);
		// Get site id from preferences
		siteInfoManual = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_SITE_INFO_MANUAL, Preferences.KEY_SITE_INFO_MANUAL_DEFAULT);
		String siteIdStr = sharedPref.getString(Preferences.KEY_EDIT_TEXT_SITE_ID, Preferences.KEY_SITE_ID_DEFAULT);
		if(siteIdStr != null && siteIdStr != ""){
			siteId = Integer.valueOf(siteIdStr);
		}
		demoMode = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_DEMO_MODE, Preferences.KEY_DEMO_MODE_DEFAULT);
		storeLogs = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_STORE_LOGS, Preferences.KEY_STORE_LOGS_DEFAULT);
		logsFilename = sharedPref.getString(Preferences.KEY_EDIT_TEXT_LOGS_FILENAME, Preferences.KEY_LOGS_FILENAME_DEFAULT);
		if(storeLogs && mLogWriter==null){
			String storeLogsFilename = logsFilename + "_pos" + Constants.logsFilenameExt;
			mLogWriter = new LogWriter(Constants.APP_DATA_FOLDER_NAME, storeLogsFilename);
		}
		//Obtain value of Send Locations
		sendLocations = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_SEND_POSITIONS, Preferences.KEY_SEND_POSITIONS_DEFAULT);
		if(sendLocations){
			String sendLocationsTimeStr = sharedPref.getString(Preferences.KEY_EDIT_TEXT_SEND_POSITIONS_TIME, Preferences.KEY_SEND_POSITIONS_TIME_DEFAULT);
			if(sendLocationsTimeStr != null && sendLocationsTimeStr != ""){
				sendLocationsTime = Integer.valueOf(sendLocationsTimeStr);
			}
			//Obtain the address of the server to store positions
			sendLocationsServer = sharedPref.getString(Preferences.KEY_EDIT_TEXT_SEND_POSITIONS_SERVER, Preferences.KEY_SEND_POSITIONS_SERVER_DEFAULT);
		}

		//Obtain the type of Position Calculation (local or remote)
		positionCalculation = sharedPref.getString(Preferences.KEY_LIST_POSITION_CALCULATION, Preferences.KEY_POSITION_CALCULATION_DEFAULT);
		LNSserver = sharedPref.getString(Preferences.KEY_EDIT_TEXT_SERVER_OBTAIN, Preferences.KEY_SERVER_OBTAIN_DEFAULT);
		//Obtain the time of the window to use
		String time = sharedPref.getString(Preferences.KEY_EDIT_TEXT_TIME_WINDOW, Preferences.KEY_TIME_WINDOW_DEFAULT);
		if(time != null && time != ""){
			timeWindow = Integer.valueOf(time);
		}
		//Obtain the type of estimation
		estimationType = sharedPref.getString(Preferences.KEY_LIST_ESTIMATION_TYPE, Preferences.KEY_ESTIMATION_TYPE_DEFAULT);
		//Obtain the number of points to use in the Floor Hysteresis (if enabled)
		useFloorHyst = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_FLOOR_HYSTERESIS, Preferences.KEY_USE_FLOOR_HYSTERESIS_DEFAULT);
		if(useFloorHyst){
			String numPoints = sharedPref.getString(Preferences.KEY_EDIT_TEXT_FLOOR_HYSTERESIS, Preferences.KEY_FLOOR_HYSTERESIS_DEFAULT);
			if(numPoints != null && numPoints != ""){
				numPointsHyst = Integer.valueOf(numPoints);
			}
		}
		//Obtain value of RSS Transformation
		rssTransf = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_RSS_TRANSFORMATION, Preferences.KEY_USE_RSS_TRANSFORMATION_DEFAULT);
		useParticleFilter = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_PARTICLE_FILTER, Preferences.KEY_USE_PARTICLE_FILTER_DEFAULT);
		useParticleFilterSensors = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_PARTICLE_FILTER_SENSORS, Preferences.KEY_USE_PARTICLE_FILTER_SENSORS_DEFAULT);
		useMapFusion = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_MAP_FUSION, Preferences.KEY_USE_MAP_FUSION_DEFAULT);


		//Obtain value of Show Candidates
		showCandidates = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_CANDIDATES, Preferences.KEY_SHOW_CANDIDATES_DEFAULT);
		showScanset = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_SHOW_SCANSET, Preferences.KEY_SHOW_SCANSET_DEFAULT);
		showSubset = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_SHOW_SUBSET, Preferences.KEY_SHOW_SUBSET_DEFAULT);
		showPaths = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_SHOW_PATHS, Preferences.KEY_SHOW_PATHS_DEFAULT);
		showOrientation = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_SHOW_ORIENTATION, Preferences.KEY_SHOW_ORIENTATION_DEFAULT);
		showSteps = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_SHOW_STEPS, Preferences.KEY_SHOW_STEPS_DEFAULT);
		showFilterParticles = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_SHOW_FILTER_PARTICLES, Preferences.KEY_SHOW_FILTER_PARTICLES_DEFAULT);

		setSettings();
		
		if(debug){
			Log.i(TAG,"--- GENERAL ---");
			Log.i(TAG,"useWifi: " + useWifi);
			Log.i(TAG,"useBle: " + useBle);
			Log.i(TAG,"useIBeacons: " + useIBeacons);
			Log.i(TAG,"useGps: " + useGps);
			Log.i(TAG,"useKalmanRSSI: " + useKalmanRSSI);
			Log.i(TAG,"useWeightedRSSI: " + useWeightedRSSI);
			Log.i(TAG,"useDetections: " + useDetections);
			Log.i(TAG,"detectionLevel: " + bleRssDetection);
			Log.i(TAG,"useSensors: " + useSensors);
			Log.i(TAG,"siteInfoManual: " + siteInfoManual);
			Log.i(TAG,"siteId: " + siteId);
			Log.i(TAG,"demoMode: " + demoMode);
			Log.i(TAG,"storeLogs: " + storeLogs);
			Log.i(TAG,"storeLogsFilename: " + logsFilename);
			Log.i(TAG,"sendLocations: " + sendLocations);
			Log.i(TAG,"sendLocationsTime: " + sendLocationsTime);
			Log.i(TAG,"sendLocationsServer: " + sendLocationsServer);
			Log.i(TAG,"--- ALGORITHM ---");
			Log.i(TAG,"positionCalculation: " + positionCalculation);
			Log.i(TAG,"LNSserver: " + LNSserver);
			Log.i(TAG,"timeWindow: " + timeWindow);
			Log.i(TAG,"estimationType: " + estimationType);
			Log.i(TAG,"numPointsHyst: " + numPointsHyst);
			Log.i(TAG,"rssTransf: " + rssTransf);
			Log.i(TAG,"useParticleFilter: " + useParticleFilter);
			Log.i(TAG,"useParticleFilterSensors: " + useParticleFilterSensors);
			Log.i(TAG,"useMapFusion: " + useMapFusion);
			Log.i(TAG,"--- UI ---");
			Log.i(TAG,"showCandidates: " + showCandidates);
			Log.i(TAG,"showScanset: " + showScanset);
			Log.i(TAG,"showSubset: " + showSubset);
			Log.i(TAG,"showPaths: " + showPaths);
			Log.i(TAG,"showOrientation: " + showOrientation);
			Log.i(TAG,"showSteps: " + showSteps);
			Log.i(TAG,"showFilterParticles: " + showFilterParticles);
		}
		if((LNSserver.equals("") || LNSserver.equals("0.0.0.0")) && positionCalculation.equals("remote")){
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Incorrect Server Address")
					.setMessage("Change server settings before start positioning")
					.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// Do my action here
							Intent i = new Intent(LocationIndoorMicello.this, Preferences.class);
							startActivity(i);
							dialog.dismiss();
						}
					});
			AlertDialog alert = builder.create();
			alert.show();
		}
		if(demoMode && testRoute){
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Alert")
					.setMessage("demoMode and testRoute activated. What do you want to do?")
					.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
			    		public void onClick(DialogInterface dialog, int which) {
			        		// Do my action here
			    			dialog.dismiss();
			    		}
					})
					.setNegativeButton("Settings", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// Do my action here
							Intent i = new Intent(LocationIndoorMicello.this, Preferences.class);
							startActivity(i);
							dialog.dismiss();
						}
					});
			AlertDialog alert = builder.create();
			alert.show();
		}
		if(getAllPermissionsOk()){
			startServices();
		}
		//Toast.makeText(this, "onResume LocationIndoor", Toast.LENGTH_SHORT).show();
		if (!mMicelloMap.isMapInitiated()) {
			mMicelloMap.initMap(siteId);
		}
	}
	
	@Override
	protected void onPause(){
		if(getAllPermissionsOk()){
			stopServices();
		}
		//Toast.makeText(this, "onPause LocationIndoor", Toast.LENGTH_SHORT).show();
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		if(!useBoundPosService){
			unregisterReceiver(mPositionReceiver);
		}
		/*
		if(mBound){
			stopLocation();
		}
		*/
		if(mBmpMap!=null){
			mBmpMap.recycle();
		}
		//Toast.makeText(this, "onDestroy LocationIndoor", Toast.LENGTH_SHORT).show();
		super.onDestroy();	
	}

	private void doBindPosService() {
		// Establish a connection with the service. We use an explicit
		// class name because we want a specific service implementation that
		// we know will be running in our own process (and thus won't be
		// supporting component replacement by other applications).
		bindService(posServiceIntent, mPosServiceConnection, BIND_AUTO_CREATE);
	}

	private void doUnbindPosService() {
		if (posServiceBound) {
			// Detach our existing connection.
			unbindService(mPosServiceConnection);
			posServiceBound = false;
		}
	}
	
	private void startLocation(){
		totalSteps = 0;
		totalMotionModeSteps = 0;
		//doBindService();
		if(useBoundPosService){
			if(posServiceBound && posServiceStarted){
				mPositioningService.Start(POSITION_REFRESH_TIME);
				if(runForegroundService){
					Intent notificationIntent = new Intent(this, LocationIndoorMicello.class);
					//Intent notificationIntent = new Intent();
					PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

					Notification notification = new NotificationCompat.Builder(getApplicationContext())
							.setContentTitle(getString(R.string.app_name))
							.setContentText("location running")
							.setSmallIcon(R.mipmap.ic_launcher)
							.setWhen(System.currentTimeMillis())
							.setContentIntent(pendingIntent)
							.build();
					mPositioningService.startForeground(NOTIFICATION_ID, notification);
				}
				posServiceIsLocating = true;
			}
		}
		else{
			if(posServiceStarted){
				Intent intent = new Intent();
				intent.setAction(PosActions.START_LOCATING);
				intent.putExtra("period", POSITION_REFRESH_TIME);
				sendBroadcast(intent);
				posServiceIsLocating = true;
			}
		}
//		LQ: Commented message Requesting location
		dialog = new ProgressDialog(this);
		dialog.setMessage("Requesting Location...");
//		dialog.setTitle("Title");
		dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
    	dialog.show();
    	/** Code added to store data info into logs.txt **/
		if(storeLogs && mLogWriter.startLogs()>0){
			logsRunning = true;
		}
	}
	
	private void stopLocation(){
		if(useBoundPosService){
			if(posServiceBound && posServiceStarted){
				// Comment next line in simulation mode
				mPositioningService.Stop();
				if(runForegroundService){
					mPositioningService.stopForeground(true);
				}
				posServiceIsLocating = false;
			}
		}
		else{
			if(posServiceStarted){
				Intent intent = new Intent();
				intent.setAction(PosActions.STOP_LOCATING);
				sendBroadcast(intent);
				posServiceIsLocating = false;
			}
		}
		/*
		try {
			doUnbindService();
		} catch (Throwable t) {
			Log.e(TAG, "Failed to unbind from the service", t);
		}
		*/
		/** Code added to store data info into logs.txt **/
		if(storeLogs && logsRunning){
			mLogWriter.stopLogs();
			logsRunning = false;
		}
	}
	
	/*
	private void drawPosition(LocationData userPosition) {
		//Change the floor image if user has changed the floor
		if(!userPosition.sourceType.equalsIgnoreCase("gps") && mCurrentFloor != userPosition.positionLocal[2]){
			changeFloor((int) userPosition.positionLocal[2]);
		}
		//Update user's position
		mMap.drawUserPosition(userPosition);
		}
	}
	*/

	private void drawPosition(ArrayList<LocationData> userPositions) {
		//Change the floor image if user has changed the floor
		/*if(!userPositions.get(0).sourceType.equalsIgnoreCase("gps") && mCurrentFloor != userPositions.get(0).positionLocal[2]){
			changeFloor((int) userPositions.get(0).positionLocal[2]);
		}*/
		mMicelloMap.setSource(userPositions.get(0).sourceType);
		int site_ID = userPositions.get(0).siteId;
		double lat = userPositions.get(0).positionGlobal[0];// - 0.0005;
		double lon = userPositions.get(0).positionGlobal[1];// + 0.000519;
		double level = Math.round(userPositions.get(0).positionLocal[2]);
		Log.d("Micello Draw", "before: " + lat + " " + lon + " " + level);
		if (site_ID == 28) {
			if (level < 0) level = 1;
			level -= 1;
		}
		else if (site_ID == 35) {
			//if (level < 0) level = 0;
			level += 1;
			mCurrentFloor = (int) level;
		} // LQ: this works for site 35 outdoors
		else if (site_ID == -1 && level != -100){
			level += 1;
			mCurrentFloor = (int) level;
		}
		else if (site_ID == 36) {
			//LQ: site ID MWC=36
			level += 1;
			mCurrentFloor = (int) level;
		}

		/*if ((int) level != mCurrentFloor) {
            mCurrentFloor = (int) level;
        }*/
		Log.d("Micello Draw", "after: " + lat + " " + lon + " " + level + " " + mCurrentFloor);
		mMicelloMap.setLocation(lat, lon, mCurrentFloor);
	}
	
	private VersionInfo obtainLocalVersions() {
		return mLocalDBHelper.getVersions();
	}

	private void compareVersions() {
		mapsNeedUpdate = true;
		mapsOk = false;
		VersionInfo vInfo = obtainLocalVersions();
		if (vInfo == null) {
			// It is the first time that we use the site. Download all the data
			hasLocalVersionData = false;
		} else {
			if(debug){
				Log.i(TAG,"Local Versions: " + vInfo.getAps() + " , " + vInfo.getMaps() + " , " + vInfo.getNodes_edges());
				Log.i(TAG, "Remote Versions: " + apsRemoteVersion + " , " + mapsRemoteVersion + " , " + nodesEdgesRemoteVersion);
			}
			if (mapsRemoteVersion == vInfo.getMaps()) {
				mapsOk = true;
				mapsNeedUpdate = false;
			}
		}
		if (!mapsOk) {
			downloadData();
		}
	}
	
	private void downloadData() {
		if (mapsNeedUpdate) {
			WebServiceAT wsAT = new WebServiceAT(this,
					new WebServiceOnCompleteListener(), getResources()
							.getString(R.string.ws_URL_BASE)
							+ getResources().getString(R.string.ws_maps),
					WebService.RequestMethod.POST);
			try {
				JSONObject json = new JSONObject();
				json.put("site_id", siteId);
				wsAT.addHeader("TOKEN", "ascamm");
//				LQ: Commented message obtaning maps data
				wsAT.AddDialog();
				wsAT.setDialogMessage("Obtaining Maps Data...");
				wsAT.AddName("maps");
				wsAT.addJSON(json);
				wsAT.execute();
			} catch (Exception e) {
				Log.e("WebService AT", "Error!", e);
			}
		}
	}
	
	private void downloadMapImage(int id, String url) {
		DownloadImageAT imageAT = new DownloadImageAT(this,
				new DownloadImageOnCompleteListener());
		try {
			imageAT.setId(id);
			imageAT.AddDialog();
			imageAT.setDialogMessage("Downloading maps...");
			imageAT.setSiteId(siteId);
			imageAT.execute(url);
		} catch (Exception e) {
			Log.e("DownloadImage AT", "Error!", e);
		}
	}
	
	private void saveMapsInfo(ArrayList<MapInfo> mapsList) {
		// Delete old Maps data
		int numMapsDeleted = mLocalDBHelper.deleteMapsAll();
		// Insert new Maps data
		boolean mapsInsertedOk = mLocalDBHelper.insertMaps(mapsList);
		if(debug){
			Log.i(TAG, "numMapsDeleted: " + numMapsDeleted);
			Log.i(TAG, "mapsInsertedOk: " + mapsInsertedOk);
		}
	}
	
	private void checkDownloadFinished() {
		boolean mapsImages = true;
		if(debug)	Log.i(TAG, "checkDownloadFinished. mapsOk: " + mapsOk);
		for (boolean map : mapsImagesDownload) {
			if(debug)	Log.i(TAG, "" + map);
			if (!map) {
				mapsImages = false;
			}
		}
		
		if (mapsOk && mapsImages) {
			if(debug){
				if(mapsList!=null){
					for (MapInfo map : mapsList) {
						Log.i(TAG, "(" + map.getId() + " , " + map.getFloor() + " , " + map.getUrl() + " , " + map.getFilename() + ")");
					}
				}
			}
			if(mapsDownloadOk){
				// Update DB with new data and versions
				SaveDataAT sdAT = new SaveDataAT(this);
				sdAT.AddDialog();
				sdAT.execute();
			}
			else{
//				LQ: Commented message site data could not be downloaded try again later
				showDialogMessage("Site Data could not be downloaded. Please try again later");
			}
		}
	}
	
	private void setTextView(String text){
    	setContentView(R.layout.textmessage);
		TextView txtView = (TextView) findViewById(R.id.TextMessage);
		txtView.setText(text);
    }
	
	private void showDialogMessage(String msgTxt) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(msgTxt);
		builder.setPositiveButton("Close",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// YES button clicked
						dialog.dismiss();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	public class WebServiceOnCompleteListener implements WebServiceCompleteListener<String> {

		private String ws_res = null;
		
		@Override
		public void onTaskComplete(WebServiceResult result) {
			// do something with the response of the Web Service
			ws_res = result.getResponse();
			try {
				int statusCode = result.getStatus();
				if (statusCode == 0 && ws_res == null) {
					VersionInfo vInfo = obtainLocalVersions();
					if (vInfo == null) {
						// It is the first time that we use the site. Download all the data
						setTextView(getResources().getString(R.string.no_connection_title) + "\n" + getResources().getString(R.string.no_connection_message));
					} else {
						// It has previous versions. Continue with last local version
						if(debug){
							Log.i(TAG,"Local Versions: " + vInfo.getAps() + " , " + vInfo.getMaps() + " , " + vInfo.getNodes_edges());
						}
//						LQ: commmented message site data could not be downloaded working offline
//						String txtMsg = "Site data could not be obtained. Working in offline mode";
//						showDialogMessage(txtMsg);
						//Toast.makeText(LocationIndoor.this, txtMsg, Toast.LENGTH_LONG).show();
					}
				} else if (statusCode == 200) {
					if (result.getName().equalsIgnoreCase("versions")) {
						JSONObject jsonObject = new JSONObject(ws_res);
						apsRemoteVersion = jsonObject.getInt("access_point_version");
						mapsRemoteVersion = jsonObject.getInt("site_map_version");
						nodesEdgesRemoteVersion = jsonObject
								.getInt("node_edge_version");
						// Compare local versions with remote versions
						compareVersions();
					} else if (result.getName().equalsIgnoreCase("maps")) {
						// Obtain Maps Data
						mapsList = new ArrayList<MapInfo>();
						JSONArray maps = new JSONArray(ws_res);
						for (int i = 0; i < maps.length(); i++) {
							JSONObject map = maps.getJSONObject(i);
							int floor = Integer.valueOf(map.getString("floor"));
							String path = new String(map.getString("filename"));
							mapsList.add(new MapInfo(i, floor, path, null));
						}
						if (mapsList.isEmpty()) {
							mapsImagesDownload.clear();
							mapsDownloadOk = false;
							Toast.makeText(LocationIndoorMicello.this, "0 Maps received", Toast.LENGTH_SHORT).show();
						} else {
							if(debug)	Log.i(TAG, mapsList.size() + " Maps received");
							for (MapInfo map : mapsList) {
								if(debug)	Log.i(TAG,"(" + map.getId() + " , " + map.getFloor() + " , " + map.getUrl() + ")");
								mapsImagesDownload.add(false);
							}
							mapsDownloadOk = true;
							for (MapInfo map : mapsList) {
								downloadMapImage(map.getId(), map.getUrl());
							}
						}
						mapsOk = true;
						checkDownloadFinished();
					} else {
						Log.e(TAG, "Unknown WS name");
					}
				} else {
					setTextView("Error. Status: " + statusCode);
				}
			} catch (Exception e) {
				Log.e("WebService AT", "Parsing JSON Error!", e);
			}
		}
	}
	
	public class DownloadImageOnCompleteListener implements DownloadImageCompleteListener<String> {

		@Override
		public void onTaskComplete(DownloadImageResult result) {
			for (int i = 0; i < mapsImagesDownload.size(); i++) {
				if (result.getId() == i) {
					String filename = result.getFilename();
					if (filename == null) {
						mapsDownloadOk = false;
						String msgTxt = "Map " + result.getId() + " could not be downloaded";
						Log.d(TAG,msgTxt);
						Toast.makeText(LocationIndoorMicello.this, msgTxt, Toast.LENGTH_LONG).show();
					}
					// update boolean's array value
					mapsList.get(i).setFilename(filename);
					mapsImagesDownload.set(i, true);
					checkDownloadFinished();
				}
			}
		}
	}
	
	private class SaveDataAT extends AsyncTask<Void, Void, Integer> {

		private ProgressDialog dialog;
		private Context context;
		private Boolean use_dialog;
		private String message;

		public SaveDataAT(Context context) {
			this.context = context;
			this.use_dialog = false;
		}

		public void AddDialog() {
			use_dialog = true;
		}

		public void setDialogMessage(String message) {
			this.message = message;
		}

		@Override
		protected void onPreExecute() {
			if (use_dialog) {
				super.onPreExecute();
				dialog = new ProgressDialog(context);
				if (message != null) {
					dialog.setMessage(message);
				} else {
					dialog.setMessage("Saving Data...");
				}
				// dialog.setTitle("Title");
				dialog.setIndeterminate(true);
				dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				dialog.setCancelable(true);
				dialog.show();
			}
		}

		@Override
		protected Integer doInBackground(Void... values) {

			int problemsSaving = 0;
			
			if (mapsNeedUpdate) {
				// Save maps info
				saveMapsInfo(mapsList);
			}
			if (hasLocalVersionData) {
				// update version data in versions table
				int updateOk = mLocalDBHelper.updateVersions(apsRemoteVersion, mapsRemoteVersion, nodesEdgesRemoteVersion);
				if (updateOk > 0) {
					if(debug)	Log.i(TAG, "Versions info updated");
				} else {
					if(debug)	Log.i(TAG, "Problems updating versions info");
					problemsSaving = 1;
				}
			} else {
				// create a new register in versions table
				long id = mLocalDBHelper.insertVersions(apsRemoteVersion, mapsRemoteVersion, nodesEdgesRemoteVersion);
				if (id != -1) {
					if(debug)	Log.i(TAG, "Versions info created");
				} else {
					if(debug)	Log.i(TAG, "Problems creating versions info");
					problemsSaving = 2;
				}
			}

			return problemsSaving;
		}

		@Override
		protected void onPostExecute(Integer problemsSaving) {
			if (problemsSaving == 2) {
				setContentView(R.layout.textmessage);
				TextView txtView = (TextView) findViewById(R.id.TextMessage);
				txtView.setText("Error. Version data could not be created. Please try again");
			} else if (problemsSaving == 1) {
//				LQ: Commented message site data could not be updated working offline
				String txtMsg = "Site data could not be updated. Working in offline mode";
				showDialogMessage(txtMsg);
				Toast.makeText(context, txtMsg, Toast.LENGTH_LONG).show();
			}
			if (use_dialog) {
				if (dialog.isShowing()) {
					dialog.dismiss();
				}
			}
		}

	}
	
}
