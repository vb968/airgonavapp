package com.ascamm.ascammnav.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class BaseActivity extends AppCompatActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);
		 enableActionBarHome();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == android.R.id.home) {
			// app icon in action bar clicked; go to previous activity
			finish();
			return true;
		} 
		else {
			return super.onOptionsItemSelected(item);
		}

	}

	protected void enableActionBarHome(){
		if (getSupportActionBar() != null) {
			getSupportActionBar().setDisplayHomeAsUpEnabled(true); // Enable home button
		}
	}

}
