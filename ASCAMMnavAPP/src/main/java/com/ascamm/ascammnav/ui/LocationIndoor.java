package com.ascamm.ascammnav.ui;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.ascamm.ascammnav.R;
import com.ascamm.ascammnav.database.LocalDBHelper;
import com.ascamm.ascammnav.map.Map;
import com.ascamm.ascammnav.map.MapType;
import com.ascamm.ascammnav.map.PointSelectedEventListener;
import com.ascamm.ascammnav.utils.AppData;
import com.ascamm.ascammnav.utils.FenceAdapter;
import com.ascamm.ascammnav.utils.MapInfo;
import com.ascamm.ascammnav.utils.Media;
import com.ascamm.ascammnav.utils.MyGoogleMap;
import com.ascamm.ascammnav.utils.PoiAdapter;
import com.ascamm.ascammnav.utils.VersionInfo;
import com.ascamm.ascammnav.ws.DownloadImageAT;
import com.ascamm.ascammnav.ws.DownloadImageCompleteListener;
import com.ascamm.ascammnav.ws.DownloadImageResult;
import com.ascamm.motion.LocationData;
import com.ascamm.motion.PosActions;
import com.ascamm.motion.PositioningService;
import com.ascamm.motion.PositioningServiceBinder;
import com.ascamm.motion.algorithm.AutocalibrationParameters;
import com.ascamm.motion.algorithm.CoordConversion;
import com.ascamm.motion.database.DBHelper;
import com.ascamm.motion.detections.Detection;
import com.ascamm.motion.detections.DetectionListener;
import com.ascamm.motion.mems.MotionMode;
import com.ascamm.motion.routing.Route;
import com.ascamm.motion.routing.RouteListener;
import com.ascamm.motion.utils.Constants;
import com.ascamm.motion.utils.DbListener;
import com.ascamm.motion.utils.Edge;
import com.ascamm.motion.utils.LogWriter;
import com.ascamm.motion.utils.MotionListener;
import com.ascamm.motion.utils.MotionStepsInfoListener;
import com.ascamm.motion.utils.OnlineItem;
import com.ascamm.motion.utils.Point3DWithFloor;
import com.ascamm.motion.utils.PositionsListener;
import com.ascamm.motion.utils.ScansetListener;
import com.ascamm.motion.utils.Settings;
import com.ascamm.motion.utils.UpdateAutocalibrationParametersListener;
import com.ascamm.motion.ws.WebService;
import com.ascamm.motion.ws.WebServiceAT;
import com.ascamm.motion.ws.WebServiceCompleteListener;
import com.ascamm.motion.ws.WebServiceResult;
import com.ascamm.pois.FenceActions;
import com.ascamm.pois.FenceService;
import com.ascamm.pois.spatialite.GeoFence;
import com.ascamm.pois.utils.Fence;
import com.ascamm.pois.utils.FenceInfo;
import com.ascamm.pois.utils.FenceInfoListener;
import com.ascamm.pois.utils.POI;
import com.ascamm.pois.utils.ReachedFenceListener;
import com.ascamm.utils.LatLng3D;
import com.ascamm.utils.Point3D;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class LocationIndoor extends BaseActivity implements OnMapReadyCallback {

	private final String TAG = this.getClass().getSimpleName();
	private boolean debug = true;
	
	private PositioningService mPositioningService;
	private Intent posServiceIntent;
	private boolean posServiceStarted = false;
	private boolean posServiceBound = true;
	private boolean posServiceIsLocating = false;
	private boolean useBoundPosService = true;
	private boolean runForegroundService = true;
	private int NOTIFICATION_ID = 100;

	private Intent fenceServiceIntent;
	private boolean fenceServiceStarted = false;

	private boolean isFirstPosition = true;
	private LatLng firstPosition = null;
	private boolean showCandidates = false, useFloorHyst = false, sendLocations = false, rssTransf = false, useWifi = false, useBle = false, useIBeacons = false, useGps = false, useParticleFilter = false, useParticleFilterSensors = false, useMapFusion = false, useKalmanRSSI = false, useWeightedRSSI = false, useDetections = false, showScanset = false, showSubset = false, showPaths = false, showOrientation = false, showSteps = false, showFilterParticles = false, demoMode = false, useSensors = false;
	private boolean dialogDetectionsActive = false;
	private int mCurrentFloor = 0;
	private String sendLocationsServer, LNSserver, positionCalculation, estimationType, logsFilename;
	private int timeWindow = 0, numPointsHyst = 0, sendLocationsTime = 0;
	private int POSITION_REFRESH_TIME = 1000; // Milliseconds
	private Point3D currentPosition;
	private Point3D destinationPosition;
	private boolean fenceSelected = false;
	private float bleRssDetection = 0;

	private MenuItem playMenu;
	private TextView slopeView, intersectionView, residualsView, motionView, nameView, distView, angleView, posView, stepsView, motionModeView, nonStepsWalkingView;
	private LinearLayout rssTransfLayout, fenceInfoLayout, fenceDetails, fenceBar;
	private LinearLayout poiInfoLayout, poiBar, recalculateLL, routeLL;
	private TableLayout tableDetections, tableScanset, tableOrientation;
	private RadioGroup radioGroup;
	private ImageButton fenceSelect;
	private Button fenceUnselect;
	private Button incident;
	private ProgressDialog dialog;

	private boolean siteInfoManual = false;
	private int siteId = 0;
	private boolean siteInfoNeeded = true, siteInfoPending = false;
	private boolean hasLocalVersionData = true;
	private boolean mapsOk = false;
	private boolean mapsDownloadOk = false;
	private boolean mapsNeedUpdate = false;
	private int apsRemoteVersion, mapsRemoteVersion, nodesEdgesRemoteVersion;
	private ArrayList<MapInfo> mapsList;
	private ArrayList<Boolean> mapsImagesDownload = new ArrayList<Boolean>();
	private LocalDBHelper mLocalDBHelper;
	private boolean mMapVisible = false;
	private boolean useLocalMaps = false;
		
	private DecimalFormat decFor = new DecimalFormat("#.00");
	
	private boolean useGoogleMaps = false;
	/*-------------------- Map --------------------*/
	private ArrayList<MapInfo> mMapsInfo;
	private Bitmap mBmpMap;
	private Map mMap;
	private String mMapBasePath;
	private FrameLayout mFlMapWrapper;
	
	/*---------------- Google Map -----------------*/
	private MyGoogleMap mGoogleMap;
	private Media mMedia;
	private String mMapPath;
	//Obtain these values from the Server (we can simulate them adding these values to the WS response)
	/*
	// ASCAMM Lab
	double lat = 41.489573582267;
	double lng = 2.1253579068142;
	float width = 44;
	float height = 72;
	float bearing = -18.7f;
	*/
	/*
	// ASCAMM Lab (Marc Compte)
	double lat = 41.4895735930717;
	double lng = 2.12535791799289;
	float width = 46.66f;
	float height = 72;
	float bearing = -18.7f;
	*/
	/*
	//CAR Sant Cugat
	double lat = 41.483322016051;
	double lng = 2.0758443037591;
	float width = 72;
	float height = 127;
	float bearing = 175.778773259f;
	*/

	// Mercat Rubi
	/*
	double lat = 41.48972017;
	double lng = 2.03434322;
	float width = 82.83f;
	float height = 83.11f;
	float bearing = 0;
	*/
	/*
	//CAR Sant Cugat (Marc Compte)
	double lat = 41.483306432;
	double lng = 2.075868909;
	float width = 73.53f;
	float height = 134.06f;
	float bearing = 176.2f;
	*/
	/*
	// Casa Batllo (Marc Compte)
	double mapAnchorLat = 41.39143712186111;
	double mapAnchorLng = 2.1644167136759083;
	float mapWidth = 56.3f;
	float mapHeight = 20.8f;
	float mapBearing = 316.3f;
	*/
	/*
	// Renault Valladolid (Marc Compte)
	double lat = 41.59799845040325;
	double lng = -4.727854671297326;
	float width = 76.3f;
	float height = 79.6f;
	float bearing = 263.7f;
	*/

	// Eurecat Proves (David Badosa)
	double mapAnchorLat = 41.488882486292;
	double mapAnchorLng = 2.1253324371713;
	float mapWidth = 37.3f;
	float mapHeight = 17.8f;
	float mapBearing = -108.7f;

	/*
	// Smart Retail (Dani Fernandez)
	double mapAnchorLat = 41.48915962;
	double mapAnchorLng = 2.125727929;
	float mapWidth = 8.27f;
	float mapHeight = 12.18f;
	float mapBearing = -19f;
	*/
	/*
	// Illa Diagonal (Dani Fernandez)
	double mapAnchorLat = 41.388951;
	double mapAnchorLng = 2.138567;
	float mapWidth = 8.27f;
	float mapHeight = 12.18f;
	float mapBearing = -161f;
	*/
		
	/*------------------- Route Testing -------------*/
	private boolean testRoute = false;
	private int idRoute = 14;
	private ArrayList<Point3D> route = new ArrayList<Point3D>();
	private int pos = 0;
	private int numPointsRoute = 0;
	private DBHelper dBHelper;
	private boolean dialogShown = false;
	/*-----------------------------------------------*/
	
	private boolean useFences = false;
	private boolean useFencesDBapp = true;
	private String fencesDBappName = "pois_ascamm.sqlite";
	private String fencesDbPath = null;
	private ArrayList<GeoFence> geoFences = null;
	private FenceAdapter mFencesAdapter;
	private boolean waitingPointSelection = false;

	/*----------------------------------------------------*/
	//LQ: POIs
	private boolean usePOIs = false;
	private ArrayList<POI> staticPois = null;
	private ArrayList<POI> mobilePois = null;
	private PoiAdapter mPOIsAdapter;
	private POI currentSelectedPoi = null;

	//LQ: Routes
	/** route modes **/
	private final static int POSITION = 0;
	private final static int CHOOSING_POINT = 1;
	private final static int SHOWING_ROUTE = 2;
	private final static int ROUTE_NAVIGATION = 3;
	private final static int POINT_CHOSED = 4;
	private final static int STOPPED = 5;

	private int currentRouteMode = 0;
	private TextView routeTxt;

	/*------------------[ Realtime POIs from AirFi Webservice ]--------------*/
	private static long RTPollingTime = 1000;
	private Button poiUnselect, quieroIr;
	private Timer RTTimer;
	private TimerTask RTTask;
	/*----------------------------------------------------*/

	private int totalSteps = 0;
	private int totalMotionModeSteps = 0;
	private int totalNonStepsWalking = 0;

	/*-------------------- Particular APP settings -------------------*/
	private static boolean pdbFromAssets = false;

	/*-------------------- PERMISSIONS (API 23) ----------------------*/
	private static int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
	private static int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2;
	private static int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 3;
	private static int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 4;
	private boolean permissionAccessFineLocation = false;
	private boolean permissionWriteExternalStorage = false;
	private boolean permissionReadPhoneState = true;

	// LQ: INCIDENTS
	private LatLng3D currentPositionGlobal;
	private String deviceId;

	/** Code added for logs **/
	private LogWriter mLogWriter;
	private boolean logsRunning = false, storeLogs = true;
	//private String logsFilename = Constants.logsFilename + "_pos" + Constants.logsFilenameExt;
	
	/** Broadcast Receivers **/
	private PositionReceiver mPositionReceiver;
	private FenceReceiver mFenceReceiver;
	
	private void showDialogRouteFinished(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Route execution finished");
		builder.setMessage("What do you want to do?");
		builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int which) {
		        dialog.dismiss();
		    }
		});
		builder.setNegativeButton("Stop", new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int which) {
		    	stopLocation();
				//change the icon
		    	playMenu.setIcon(R.drawable.start_pos);
		    	playMenu.setTitle("start");
		    	dialog.dismiss();   	
		    }
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	private void showDialogDetections(ArrayList<Detection> detections){
		if(!dialogDetectionsActive){
			String title = detections.size() + " detections found";
			String msg = "";
			for(Detection det : detections){
				msg += "- " + det.toString() + "\n";
			}
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(title);
			builder.setMessage(msg);
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog, int which) {
			        dialog.dismiss();
			        dialogDetectionsActive = false;
			    }
			});
			builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
				
				@Override
				public void onCancel(DialogInterface dialog) {
					dialogDetectionsActive = false;
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
			dialogDetectionsActive = true;
		}
	}
	
	private void updateDetectionsTable(ArrayList<Detection> detections){
		//Log.d(TAG,"inside updateDetectionsTable");
		String title = detections.size() + " detections found:";
		LayoutInflater inflater = LocationIndoor.this.getLayoutInflater();
		
		tableDetections.removeAllViews();
		TableRow firstRow = (TableRow) inflater.inflate(R.layout.detection_item_title, tableDetections, false);
		TextView tvTitle = (TextView) firstRow.findViewById(R.id.tv_title);
		tvTitle.setText(title);
		tableDetections.addView(firstRow);
		for(Detection det : detections){
			TableRow row = (TableRow) inflater.inflate(R.layout.detection_item, tableDetections, false);
			TextView rowTvBSSID = (TextView) row.findViewById(R.id.tv_bssid);
			rowTvBSSID.setText(det.getMac());
			TextView rowTvRSS = (TextView) row.findViewById(R.id.tv_rss);
			rowTvRSS.setText(decFor.format(det.getRss()));
			if(det.getPosLocal()!=null){
				rowTvBSSID.setTextColor(getResources().getColor(R.color.ascamm_red));
				rowTvRSS.setTextColor(getResources().getColor(R.color.ascamm_red));
			}
			tableDetections.addView(row);
		}
	}
	
	private void updateScansetTable(ArrayList<OnlineItem> scanset){
		//Log.d(TAG, "inside updateScansetTable. numScans: " + scanset.size());
		LayoutInflater inflater = LocationIndoor.this.getLayoutInflater();
		
		tableScanset.removeAllViews();
		for(OnlineItem oi : scanset){
			TableRow row = (TableRow) inflater.inflate(R.layout.scanset_item, tableScanset, false);
			TextView rowTvBSSID = (TextView) row.findViewById(R.id.tv_bssid);
			rowTvBSSID.setText(oi.getBSSID());
			TextView rowTvRSS = (TextView) row.findViewById(R.id.tv_rss);
			rowTvRSS.setText(decFor.format(oi.getRSS()));
			TextView rowTvCOUNT = (TextView) row.findViewById(R.id.tv_count);
			rowTvCOUNT.setText(oi.getCount() + "(" + (int)oi.getCountPercentage() + "%)");
			String tech = oi.getTech();
			if(tech!=null){
				if(tech.equalsIgnoreCase("ble")){
					rowTvBSSID.setTextColor(getResources().getColor(R.color.ascamm_red));
					rowTvRSS.setTextColor(getResources().getColor(R.color.ascamm_red));
					rowTvCOUNT.setTextColor(getResources().getColor(R.color.ascamm_red));
				}
				else if(tech.equalsIgnoreCase("wifi")){
					rowTvBSSID.setTextColor(getResources().getColor(R.color.ascamm_blue));
					rowTvRSS.setTextColor(getResources().getColor(R.color.ascamm_blue));
					rowTvCOUNT.setTextColor(getResources().getColor(R.color.ascamm_blue));
				}
			}
			tableScanset.addView(row);
		}
	}

	private void updateOrientationTable(ArrayList<Double> stepsList, double orientation, double diffOrientation){
		//Log.d(TAG,"inside updateOrientationTable");
		LayoutInflater inflater = LocationIndoor.this.getLayoutInflater();
		tableOrientation.removeAllViews();
		if(stepsList.size()!=0) {
			for (double step : stepsList) {
				//Log.d(TAG,"orientation: " + sp.mHeading + " , diffOrient: " + sp.mHeadingDiff);
				TableRow row = (TableRow) inflater.inflate(R.layout.orientation_item, tableOrientation, false);
				TextView rowTvStepLength = (TextView) row.findViewById(R.id.tv_step_length);
				rowTvStepLength.setText(decFor.format(step));
				TextView rowTvOrientation = (TextView) row.findViewById(R.id.tv_orientation);
				//rowTvOrientation.setText("" + (int)sp.mHeading);
				rowTvOrientation.setText(decFor.format(orientation));
				TextView rowTvDiffOrientation = (TextView) row.findViewById(R.id.tv_diff_orientation);
				//rowTvDiffOrientation.setText("" + (int)sp.mHeadingDiff);
				rowTvDiffOrientation.setText(decFor.format(diffOrientation));
				if (Math.abs(diffOrientation) > 30) {
					rowTvOrientation.setTextColor(getResources().getColor(R.color.ascamm_red));
					rowTvDiffOrientation.setTextColor(getResources().getColor(R.color.ascamm_red));
				}
				tableOrientation.addView(row);
			}
		}
		else{
			//Log.d(TAG,"orientation: " + orientation + " , diffOrient: " + diffOrientation);
			TableRow row = (TableRow) inflater.inflate(R.layout.orientation_item, tableOrientation, false);
			TextView rowTvStepLength = (TextView) row.findViewById(R.id.tv_step_length);
			rowTvStepLength.setText(decFor.format(0));
			TextView rowTvOrientation = (TextView) row.findViewById(R.id.tv_orientation);
			rowTvOrientation.setText("" + decFor.format(orientation));
			TextView rowTvDiffOrientation = (TextView) row.findViewById(R.id.tv_diff_orientation);
			rowTvDiffOrientation.setText("" + decFor.format(diffOrientation));
			tableOrientation.addView(row);
		}
	}
	
	private void updateMotionUI(int sensorState){
		Log.d(TAG,"updateMotionUI. sensorState: " + sensorState);
		if(sensorState==Constants.STATIC){
			motionView.setText("S");
			motionView.setBackgroundColor(getResources().getColor(R.color.ascamm_red));
		}
		else if(sensorState==Constants.WALKING){
			motionView.setText("W");
			motionView.setBackgroundColor(getResources().getColor(R.color.ascamm_green));
		}
		else if(sensorState==Constants.GOING_TO_STATIC){
			motionView.setText("G");
			motionView.setBackgroundColor(getResources().getColor(R.color.ascamm_orange));
		}
	}

	private void updateStepsUI(int numSteps){
		//Log.d(TAG,"updateStepsUI. numSteps: " + numSteps);
		if(mMapVisible) {
			stepsView.setText(numSteps + " / " + totalSteps);
		}
	}

	private void updateMotionModeUI(int motionMode, int steps, boolean nonStepsDetected){
		Log.d(TAG,"updateMotionModeUI. motionMode: " + motionMode + " , steps: " + steps);
		if(motionMode == MotionMode.STATIC.ordinal()){
			motionModeView.setText("S (" + steps + " / " + totalMotionModeSteps + ")");
			motionModeView.setBackgroundColor(getResources().getColor(R.color.ascamm_red));
		}
		else if(motionMode == MotionMode.IRREGULAR_MOTION.ordinal()){
			motionModeView.setText("I (" + steps + " / " + totalMotionModeSteps + ")");
			motionModeView.setBackgroundColor(getResources().getColor(R.color.ascamm_orange));
		}
		else if(motionMode == MotionMode.WALKING.ordinal()){
			motionModeView.setText("W (" + steps + " / " + totalMotionModeSteps + ")");
			motionModeView.setBackgroundColor(getResources().getColor(R.color.ascamm_green));
		}
		if(nonStepsDetected){
			nonStepsWalkingView.setText("" + totalNonStepsWalking);
			nonStepsWalkingView.setBackgroundColor(getResources().getColor(R.color.ascamm_red));
		}
		else{
			nonStepsWalkingView.setBackgroundColor(getResources().getColor(R.color.ascamm_green));
		}
	}

    /** Broadcast Receivers **/
	private class PositionReceiver extends BroadcastReceiver {
		private String TAG = getClass().getSimpleName();
		
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			Log.d(TAG,"action: " + action);
			if(action.equalsIgnoreCase(PosActions.SERVICE_OK)){
				sendIsLocatingToPosService();
				sendSetSiteIdToPosService(siteId);
				sendLocationsConfigToPosService();
			}
			else if(action.equalsIgnoreCase(PosActions.IS_LOCATING_RESP)){
				boolean isLocating = intent.getExtras().getBoolean("isLocating");
				Log.d(TAG, "isLocating: " + isLocating);
				if(isLocating){
					posServiceIsLocating = true;
					changeMenuIcon();
				}
			}
			else if(action.equalsIgnoreCase(PosActions.SET_SITE_ID_RESP)){
				boolean success = intent.getExtras().getBoolean("success");
				Log.d(TAG, "setSiteIdResp: " + success);
			}
			else if(action.equalsIgnoreCase(PosActions.SEND_LOCATIONS_CONFIG_RESP)){
				boolean success = intent.getExtras().getBoolean("success");
				Log.d(TAG, "sendLocationsConfigResp: " + success);
			}
			else if(action.equalsIgnoreCase(PosActions.START_LOCATING_RESP)){
				boolean success = intent.getExtras().getBoolean("success");
				Log.d(TAG, "startLocatingResp: " + success);
			}
			else if(action.equalsIgnoreCase(PosActions.STOP_LOCATING_RESP)){
				boolean success = intent.getExtras().getBoolean("success");
				Log.d(TAG, "stopLocatingResp: " + success);
			}
			else if(action.equalsIgnoreCase(PosActions.NEW_POSITIONS)){
				/*
				LocationData location = (LocationData) intent.getExtras().getSerializable("location");
				LocationData location2 = (LocationData) intent.getExtras().getSerializable("location2");
				ArrayList<LocationData> locationsData = new ArrayList<LocationData>();
				locationsData.add(location);
				locationsData.add(location2);
				*/
				ArrayList<LocationData> locationsData = (ArrayList<LocationData>) intent.getExtras().getSerializable("locations");
				processNewPositions(locationsData);
			}
			else if(action.equalsIgnoreCase(PosActions.NEW_DETECTIONS)){
				ArrayList<Detection> detections = (ArrayList<Detection>) intent.getExtras().getSerializable("detections");
				processNewDetections(detections);
			}
			else if(action.equalsIgnoreCase(PosActions.CALC_ROUTE_RESP)){
				Route route = (Route) intent.getExtras().getSerializable("route");
				processNewRoute(route);
			}
			else if(action.equalsIgnoreCase(PosActions.NEW_SCANSET)){
				ArrayList<OnlineItem> scanset = (ArrayList<OnlineItem>) intent.getExtras().getSerializable("scanset");
				processNewScanset(scanset);
			}
			else if(action.equalsIgnoreCase(PosActions.NEW_MOTION)){
				int sensorState = intent.getExtras().getInt("sensorState");
				processNewMotion(sensorState);
			}
			else if(action.equalsIgnoreCase(PosActions.NEW_AUTO_PARAMS)){
				double slope = intent.getExtras().getDouble("slope");
				double intersection = intent.getExtras().getDouble("intersection");
				double residuals = intent.getExtras().getDouble("residuals");
				processNewAutoParams(slope, intersection, residuals);
			}
			else if(action.equalsIgnoreCase(PosActions.NEW_DATABASE_DATA)){
				float pxMeter = intent.getExtras().getFloat("px_meter");
				processNewDatabaseData(pxMeter);
			}
		}
	};

	private class FenceReceiver extends BroadcastReceiver {
		private String TAG = getClass().getSimpleName();

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			Log.d(TAG,"action: " + action);
			if(action.equalsIgnoreCase(FenceActions.SERVICE_OK)){
				sendSetFencesDBToFenceService(fencesDbPath);
				sendAngleToNorthToFenceService(360 - mapBearing);
			}
			// LQ: is this action called?
			else if(action.equalsIgnoreCase(FenceActions.SET_FENCE_DB_RESP)){
				boolean success = intent.getExtras().getBoolean("success");
				Log.d(TAG,"fences DB init ok: " + success);
				if(success){
					sendSetSiteIdToFenceService(siteId);
					sendGetFencesToFenceService();
				}
			}
			else if(action.equalsIgnoreCase(FenceActions.GET_FENCES_RESP)){
				ArrayList<GeoFence> fences = (ArrayList<GeoFence>) intent.getExtras().getSerializable("fences");
				setGeoFences(fences);
			}
			else if(action.equalsIgnoreCase(FenceActions.CALC_CLICKED_FENCE_RESP)){
				int selectedFenceId = intent.getExtras().getInt("selected_fence_id");
				String selectedFenceName = intent.getExtras().getString("selected_fence_name");
				double centroid_x = intent.getExtras().getDouble("centroid_x");
				double centroid_y = intent.getExtras().getDouble("centroid_y");
				double centroid_z = intent.getExtras().getDouble("centroid_z");
				Point3D centroid = new Point3D(centroid_x,centroid_y,centroid_z);
				processFenceSelected(selectedFenceId, selectedFenceName, centroid);
			}
			else if(action.equalsIgnoreCase(FenceActions.GET_CENTROID_OF_SELECTED_FENCE_RESP)){
				int selectedFenceId = intent.getExtras().getInt("fence_id");
				String selectedFenceName = intent.getExtras().getString("fence_name");
				double centroid_x = intent.getExtras().getDouble("centroid_x");
				double centroid_y = intent.getExtras().getDouble("centroid_y");
				double centroid_z = intent.getExtras().getDouble("centroid_z");
				Point3D centroid = new Point3D(centroid_x,centroid_y,centroid_z);
				processFenceSelected(selectedFenceId, selectedFenceName, centroid);
			}
			else if(action.equalsIgnoreCase(FenceActions.FENCE_REACHED)){
				int fenceId = intent.getExtras().getInt("fence");
				int fenceLocalId = intent.getExtras().getInt("fence_local");
				processFenceReached(fenceId, fenceLocalId);
			}
			else if(action.equalsIgnoreCase(FenceActions.FENCE_INFO)) {
				String distance = intent.getExtras().getString("distance");
				String angle = intent.getExtras().getString("angle");
				String pos = intent.getExtras().getString("pos");
				processFenceInfo(distance, angle, pos);
			}
			else if(action.equalsIgnoreCase(FenceActions.ERROR)) {
				String type = intent.getExtras().getString("type");
				String code = intent.getExtras().getString("code");
				String msg = intent.getExtras().getString("msg");
				processFenceError(type, code, msg);
			}
		}
	};

	private void sendLocationsConfigToPosService(){
		Intent intent = new Intent();
		intent.setAction(PosActions.SEND_LOCATIONS_CONFIG);
		intent.putExtra("seconds", sendLocationsTime);
		intent.putExtra("server_url", sendLocationsServer);
		sendBroadcast(intent);
	}

	private void sendSetSiteIdToPosService(int siteId){
		Intent intent = new Intent();
		intent.setAction(PosActions.SET_SITE_ID);
		intent.putExtra("siteId", siteId);
		sendBroadcast(intent);
	}
	
	private void sendIsLocatingToPosService(){
		Intent intent = new Intent();
		intent.setAction(PosActions.IS_LOCATING);
		sendBroadcast(intent);
	}
	
	private void sendCalcRouteToPosService(Point3D currentPosition, Point3D destinationPosition){
		Intent intent = new Intent();
		intent.setAction(PosActions.CALC_ROUTE);
		intent.putExtra("current_position", currentPosition);
		intent.putExtra("destination_position", destinationPosition);
		sendBroadcast(intent);
	}
	
	public void newRouteButton(View view) {
		if(this.destinationPosition != null && this.currentPosition != null) {
			dialog = new ProgressDialog(this);
			dialog.setMessage("Calculating route...");
			dialog.setIndeterminate(true);
			dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			dialog.setCancelable(true);
			dialog.show();
			if(useBoundPosService){
				this.mPositioningService.calculateRoute(this.currentPosition, this.destinationPosition);
			}
			else{
				sendCalcRouteToPosService(this.currentPosition, this.destinationPosition);
			}
		}
	}

	private void sendSetFencesDBToFenceService(String dbPath){
		Intent intent = new Intent();
		intent.setAction(FenceActions.SET_FENCE_DB);
		intent.putExtra("fences_db_path", dbPath);
		sendBroadcast(intent);
	}

	private void sendSetSiteIdToFenceService(int siteId){
		Intent intent = new Intent();
		intent.setAction(FenceActions.SET_SITE_ID);
		intent.putExtra("site_id", siteId);
		sendBroadcast(intent);
	}

	private void sendGetFencesToFenceService(){
		Intent intent = new Intent();
		intent.setAction(FenceActions.GET_FENCES);
		sendBroadcast(intent);
	}

	private void sendSelectFenceToFenceService(int fenceId){
		Intent intent = new Intent();
		intent.setAction(FenceActions.SELECT_FENCE);
		intent.putExtra("fence", fenceId);
		sendBroadcast(intent);
	}

	private void sendCalcClickedFenceToFenceService(Point3D point){
		Intent intent = new Intent();
		intent.setAction(FenceActions.CALC_CLICKED_FENCE);
		intent.putExtra("site_id", siteId);
		intent.putExtra("point", point);
		sendBroadcast(intent);
	}

	private void sendGetCentroidOfSelectedFenceToFenceService(int fenceId, String fenceName, Point3D point){
		Intent intent = new Intent();
		intent.setAction(FenceActions.GET_CENTROID_OF_SELECTED_FENCE);
		intent.putExtra("fence_id", fenceId);
		intent.putExtra("fence_name", fenceName);
		intent.putExtra("point", point);
		sendBroadcast(intent);
	}

	private void sendDoFenceCalculationsToFenceService(Point3D point){
		Intent intent = new Intent();
		intent.setAction(FenceActions.DO_FENCE_CALCULATIONS);
		intent.putExtra("point", point);
		sendBroadcast(intent);
	}

	private void sendPxMeterToFenceService(float pxMeter){
		Intent intent = new Intent();
		intent.setAction(FenceActions.SET_PX_METER);
		intent.putExtra("px_meter", pxMeter);
		sendBroadcast(intent);
	}

	private void sendAngleToNorthToFenceService(float angle){
		Intent intent = new Intent();
		intent.setAction(FenceActions.SET_ANGLE_TO_NORTH);
		intent.putExtra("angle", angle);
		sendBroadcast(intent);
	}
	
	/* Event listeners */
	private UpdateAutocalibrationParametersListener mAutocalibrationListener = new UpdateAutocalibrationParametersListener() {
		@Override
		public void newUpdate(AutocalibrationParameters autoParams){
			double slope = autoParams.getSlope();
			double intersection = autoParams.getIntersection();
			double residuals = autoParams.getResiduals();
			processNewAutoParams(slope, intersection, residuals);
		}
	};
	
	private PositionsListener mPositionListener = new PositionsListener() {
		@Override
		public void newPositionAvailable(ArrayList<LocationData> locationsData){
			processNewPositions(locationsData);
		}
	};
	
	private DetectionListener mDetectionListener = new DetectionListener() {
		@Override
		public void newDetectionAvailable(ArrayList<Detection> detections, int siteId){
			processNewDetections(detections);
		}
	};
	
	private ScansetListener mScansetListener = new ScansetListener() {
		@Override
		public void newScansetAvailable(ArrayList<OnlineItem> scanset){
			processNewScanset(scanset);
		}
	};
	
	private MotionListener mMotionListener = new MotionListener() {
		@Override
		public void newMotionAvailable(int sensorState){
			processNewMotion(sensorState);
		}
	};
	
	private RouteListener mRouteListener = new RouteListener() {
		
		@Override
		public void newRouteCalculated(Route route) {
			processNewRoute(route);
		}
	};

	private DbListener mDbListener = new DbListener() {

		@Override
		public void dbLoaded(float pxMeter) {
			processNewDatabaseData(pxMeter);
		}
	};

	private MotionStepsInfoListener mMotionStepsInfoListener = new MotionStepsInfoListener() {
		@Override
		public void newMotionStepsInfoAvailable(int motionMode, ArrayList<Double> stepsList, double orientation, double diffOrientation){
			processNewMotionStepsInfo(motionMode, stepsList, orientation, diffOrientation);
		}
	};

	private ReachedFenceListener mReachedFenceListener = new ReachedFenceListener() {
		@Override
		public void newReachedFence(int fenceId, int fenceLocalId){
			processFenceReached(fenceId, fenceLocalId);
		}
	};
	
	private PointSelectedEventListener mPointSelectedListener = new PointSelectedEventListener() {
		@Override
		public void newPointSelected(Point3D point){
			if(waitingPointSelection){
				waitingPointSelection = false;
			}
			processPointSelected(point);
		}
	};
	
	private FenceInfoListener mFenceInfoListener = new FenceInfoListener() {
		@Override
		public void newFenceInfo(FenceInfo fenceInfo){
			String dist = fenceInfo.getDistance_px() + " (" + fenceInfo.getDistance_m() + "m)";
			String angle = fenceInfo.getAngle_to_relative_north() + " (" + fenceInfo.getAngle_to_absolute_north() + ")";
			String pos = fenceInfo.getPos();
			processFenceInfo(dist, angle, pos);
		}
	};
	
	private void saveFirstPosition(LocationData pos){
		firstPosition = new LatLng(pos.positionGlobal[0], pos.positionGlobal[1]);
	}
	
	private void processNewPositions(ArrayList<LocationData> locationsData){
		//Log.i(TAG,"relativeOrientation: " + locationsData.get(0).relativeOrientation);
		int numSteps = (int) locationsData.get(0).numSteps;
		if(numSteps != -1) {
			totalSteps += numSteps;
			//Log.i(TAG,"numSteps: " + numSteps + " , totalSteps: " + totalSteps);
			updateStepsUI(numSteps);
		}
		double userOrientation = locationsData.get(0).userOrientation;
		double userOrientationReliability = locationsData.get(0).userOrientationReliability;
		double accuracy = locationsData.get(0).accuracy;
		Log.d(TAG, "userOrientation: " + userOrientation);
		Log.d(TAG, "userOrientationReliability: " + userOrientationReliability);
		Log.d(TAG, "accuracy: " + accuracy);
		if(debug){
			Log.d(TAG,"locationsData(0): (" + locationsData.get(0).positionLocal[0] + "," + locationsData.get(0).positionLocal[1] + "," + locationsData.get(0).positionLocal[2] + "), siteId: " + locationsData.get(0).siteId + " , tech: " + locationsData.get(0).sourceType);
			Log.d(TAG,"locationsData(1): (" + locationsData.get(1).positionLocal[0] + "," + locationsData.get(1).positionLocal[1] + "," + locationsData.get(1).positionLocal[2] + "), siteId: " + locationsData.get(1).siteId + " , tech: " + locationsData.get(1).sourceType);
			//Log.d(TAG,"global(0): (" + locationsData.get(0).positionGlobal[0] + "," + locationsData.get(0).positionGlobal[1] + "," + locationsData.get(0).positionGlobal[2] + ")");
		}
		if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
		if(isFirstPosition){
			//Save first position as the center when using GoogleMaps
			saveFirstPosition(locationsData.get(0));
			isFirstPosition = false;
		}
		if(!waitingPointSelection){
			if(locationsData.get(0).siteId==-1){
				// It is a position from GPS
				siteId = locationsData.get(0).siteId;
				if(!mMapVisible){
					showMap();
				}
				if(rssTransf){
					drawTwoPositions(locationsData);
				}else{
					//drawPosition(locationsData.get(0));
					drawPosition(locationsData);
				}
				if(!siteInfoNeeded){
					siteInfoNeeded = true;
				}
			}
			else{
				// It is an indoor position
				if(siteInfoNeeded){
					siteId = locationsData.get(0).siteId;
					
					mLocalDBHelper = new LocalDBHelper(LocationIndoor.this, siteId);
					siteInfoNeeded = false;
					siteInfoPending = true;
					if(useLocalMaps){
						showMap();
					}
					else{
						//Execute WS to obtain site info
						obtainSiteVersions();
					}
				}
				else{
					if(!siteInfoPending){
						if(rssTransf){
							drawTwoPositions(locationsData);
						}else{
							//drawPosition(locationsData.get(0));
							drawPosition(locationsData);
						}
						this.currentPosition = new Point3D(locationsData.get(0).positionLocal[0], locationsData.get(0).positionLocal[1], Math.round(locationsData.get(0).positionLocal[2]));
						this.currentPositionGlobal = new LatLng3D(locationsData.get(0).positionGlobal[0], locationsData.get(0).positionGlobal[1], locationsData.get(0).positionGlobal[2]);
						if(testRoute){
							if(pos<numPointsRoute){
								route.get(pos).setId(1);
								/** Code added to store data info into logs.txt **/
								if(storeLogs && logsRunning){
									Point3D realPos = route.get(pos);
									Point3D estimatedPos1 = new Point3D(locationsData.get(0).positionLocal[0], locationsData.get(0).positionLocal[1],  Math.round(locationsData.get(0).positionLocal[2]));
									long ts = System.currentTimeMillis();
									float error1 = mMap.distanceBetweenPoints(realPos, estimatedPos1);
									if(rssTransf){
										Point3D estimatedPos2 = new Point3D(locationsData.get(1).positionLocal[0], locationsData.get(1).positionLocal[1],  Math.round(locationsData.get(1).positionLocal[2]));
										float error2 = mMap.distanceBetweenPoints(realPos, estimatedPos2);
										mLogWriter.addPositionErrorLog2(ts, realPos, estimatedPos1, error1, estimatedPos2, error2);
									}
									else{
										mLogWriter.addPositionErrorLog(ts, realPos, estimatedPos1, error1);
									}
								}
								pos++;
							}
							else{
								Log.i(TAG,"Route execution finished!");
								if(!dialogShown){
									showDialogRouteFinished();
									//Toast.makeText(LocationIndoor.this, "Route execution finished!", Toast.LENGTH_SHORT).show();
									dialogShown = true;
								}
							}
						}
					}
				}
			}
			if(useFences){
				Point3D currentPosition = new Point3D(locationsData.get(0).positionLocal[0], locationsData.get(0).positionLocal[1],  Math.round(locationsData.get(0).positionLocal[2]));
				sendDoFenceCalculationsToFenceService(currentPosition);
			}
		}
	}
	
	private void processNewDetections(ArrayList<Detection> detections){
		if(debug){
			Log.d(TAG, "newDetections received:");
			for(Detection det : detections){
				Log.d(TAG, det.toString());
			}
		}
		String msg = detections.size() + " detections found:\n";
		for(Detection det : detections){
			msg += "- " + det.toString() + "\n";
		}
		//show Alert
		//Toast.makeText(LocationIndoor.this, msg, Toast.LENGTH_SHORT).show();
		//showDialogDetections(detections);
		if(mMapVisible){
			drawDetections(detections);
			updateDetectionsTable(detections);
		}
	}
	
	private void processNewScanset(ArrayList<OnlineItem> scanset){
		if(debug){
			Log.d(TAG, "newScanset received");
			/*
			for(OnlineItem oi : scanset){
				Log.i(TAG,"ssid: " + oi.getSSID() + " , mac: " + oi.getBSSID() + " , rss: " + oi.getRSS() + " , stdev: " + oi.getRSSstdev() + " , tech: " + oi.getTech() + " , count: " + oi.getCount() + " , count(%): " + oi.getCountPercentage());
			}
			*/
		}
		if(mMapVisible){
			updateScansetTable(scanset);
		}
	}
	
	private void processNewMotion(int sensorState){
		if(debug){
			Log.d(TAG, "newMotion received. sensorState: " + sensorState);
		}
		if (mMapVisible){
			updateMotionUI(sensorState);
		}
	}
	
	private void processNewAutoParams(double slope, double intersection, double residuals){
		if(debug)	Log.d(TAG, "newUpdate received. a: " + slope + " , b: " + intersection + " , y: " + residuals + " , mapVisible: " + mMapVisible);
		if(mMapVisible){
			slopeView.setText(String.format("%.4f",slope));
			intersectionView.setText(String.format("%.4f", intersection));
			residualsView.setText(String.format("%.4f", residuals));
		}
	}

	private void processNewRoute(Route route){
		mMap.setRoute(route.getRoutePoints());
		mMap.setRefresh(true);
		dialog.dismiss();
	}

	private void processNewDatabaseData(float pxMeter){
		//Toast.makeText(LocationIndoor.this, "pxMeter: " + pxMeter, Toast.LENGTH_SHORT).show();
		if(useFences) {
			sendPxMeterToFenceService(pxMeter);
		}
	}

	private void processNewMotionStepsInfo(int motionMode, ArrayList<Double> stepsList, double orientation, double diffOrientation){
		int steps = stepsList.size();
		totalMotionModeSteps += steps;
		boolean nonStepsDetected = false;
		if(motionMode == MotionMode.WALKING.ordinal() && steps==0){
			totalNonStepsWalking += 1;
			nonStepsDetected = true;
		}
		if(debug){
			Log.d(TAG, "newMotionStepsInfo received. motionMode: " + motionMode + " , steps: " + steps + " , totalMotionModeSteps: " + totalMotionModeSteps);
		}
		if (mMapVisible){
			updateMotionModeUI(motionMode, steps, nonStepsDetected);
			updateOrientationTable(stepsList, orientation, diffOrientation);
		}
	}

	private void processFenceReached(int fenceId, int fenceLocalId){
		String txtMsg = "fenceReached: " + fenceId + " , localId: " + fenceLocalId;
		if(debug)	Log.d(TAG, txtMsg);
		Toast.makeText(LocationIndoor.this, txtMsg, Toast.LENGTH_SHORT).show();
		//LQ: TODO?
		if(mMapVisible){
			
		}
	}
	
	private void processFenceSelected(int fenceId, String fenceName, Point3D centroid){
		String txtMsg = "fenceSelected: " + fenceId;
		if(debug)	Log.d(TAG, txtMsg);
		//Toast.makeText(LocationIndoor.this, txtMsg, Toast.LENGTH_SHORT).show();
		if(mMapVisible){
			mMap.setSelectedFenceId(fenceId);
			if(fenceId==-1){
				if(fenceSelected){
					fenceInfoLayout.setVisibility(View.GONE);
					fenceUnselect.setVisibility(View.GONE);
					fenceSelected = false;
					this.mMap.setDestinationPoint(null);
				} else {
					fenceInfoLayout.setVisibility(View.VISIBLE);
					fenceUnselect.setVisibility(View.VISIBLE);
					fenceDetails.setVisibility(View.GONE);
					fenceBar.setVisibility(View.GONE);
					this.mMap.setDestinationPoint(this.destinationPosition);
				}
				
			}
			else{
				fenceSelected = true;
				this.destinationPosition = centroid;
				fenceInfoLayout.setVisibility(View.VISIBLE);
				fenceUnselect.setVisibility(View.VISIBLE);
				fenceDetails.setVisibility(View.VISIBLE);
				fenceBar.setVisibility(View.VISIBLE);
				nameView.setText(fenceName);
				// LQ: DESTINATION POINT IS NULL!!
				//this.mMap.setDestinationPoint(null);
				this.mMap.setDestinationPoint(destinationPosition);
			}
			mMap.setRefresh(true);
		}
		/*
		if(useBoundPosService){
			mPositioningService.selectFence(fenceId);
		}
		else{
			sendSelectFenceToService(fenceId);
		}
		*/
		sendSelectFenceToFenceService(fenceId);
	}

	private void processFenceInfo(String distance, String angle, String pos){
		String txtMsg = "fenceInfo: (" + distance + " , " + angle + " , " + pos + ")";
		if(debug)	Log.d(TAG, txtMsg);
		//Toast.makeText(LocationIndoor.this, txtMsg, Toast.LENGTH_SHORT).show();
		if(mMapVisible){
			if(fenceInfoLayout.getVisibility()==View.VISIBLE){
				distView.setText("d: " + distance);
				angleView.setText("a: " + angle);
				posView.setText("p: " + pos);
			}

		}
	}

	private void processFenceError(String type, String code, String msg){
		String txtMsg = "fenceError: (" + type + " , " + code + " , " + msg + ")";
		if(debug)	Log.d(TAG, txtMsg);
		Toast.makeText(LocationIndoor.this, txtMsg, Toast.LENGTH_SHORT).show();
	}
	
	private void processPointSelected(Point3D point){
		String txtMsg = "point: " + point;
		if(debug)	Log.d(TAG, txtMsg);
		//Toast.makeText(LocationIndoor.this, txtMsg, Toast.LENGTH_SHORT).show();
		this.destinationPosition = point;
		if(useFences) {
			sendCalcClickedFenceToFenceService(point);
		}
		else{
			fenceInfoLayout.setVisibility(View.VISIBLE);
			fenceUnselect.setVisibility(View.VISIBLE);
			fenceDetails.setVisibility(View.GONE);
			fenceBar.setVisibility(View.GONE);
			this.mMap.setDestinationPoint(this.destinationPosition);
		}

		if (usePOIs) {
			if(this.getRouteMode() == CHOOSING_POINT || this.getRouteMode() == POSITION || this.getRouteMode() == POINT_CHOSED) {
				POI selectedPOI = calcClickedPOI(point);
				if(selectedPOI == null) {
					this.destinationPosition = point;
					this.mMap.setDestinationPoint(point);
				} else {
					this.mMap.setDestinationPoint(null);
					this.destinationPosition = selectedPOI.getPoint();
				}
				processPoiSelected(selectedPOI);
				this.mMap.setRefresh(true);
				if(this.getRouteMode() == CHOOSING_POINT) {
					this.changeRouteMode(POINT_CHOSED);
				} else if(this.getRouteMode() == POSITION) {
					this.changeRouteMode(POINT_CHOSED);
				}
			}
		}
	}

	/** Defines callback for service binding, passed to bindService() */
	private ServiceConnection mPosServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			// We've bound to PositioningService, cast the IBinder and get
			// PositioningService instance
			PositioningServiceBinder binder = (PositioningServiceBinder) service;
			mPositioningService = binder.getService();
			if(mPositioningService.isLocating()){
				posServiceIsLocating = true;
			}
			mPositioningService.setUIcontext(LocationIndoor.this);
			mPositioningService.setSendLocationsConfig(sendLocationsTime, sendLocationsServer);
			mPositioningService.setSiteId(siteId);
			mPositioningService.addPositionsListener(mPositionListener);
			mPositioningService.addDetectionListener(mDetectionListener);
			mPositioningService.addRouteListener(mRouteListener);
			mPositioningService.addAutocalibrationListener(mAutocalibrationListener);
			mPositioningService.addDatabaseDataListener(mDbListener);
			if(showScanset){
				mPositioningService.addScansetListener(mScansetListener);
			}
			mPositioningService.addMotionListener(mMotionListener);
			mPositioningService.addMotionStepsInfoListener(mMotionStepsInfoListener);
			posServiceBound = true;
			//Toast.makeText(LocationIndoor.this, "PosService connected",Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			posServiceBound = false;
			//Toast.makeText(LocationIndoor.this, "PosService disconnected",Toast.LENGTH_SHORT).show();
		}
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//Log.i(TAG,"inside onCreateOptionsMenu");
		getMenuInflater().inflate(R.menu.menu, menu);
		playMenu = menu.findItem(R.id.start_pos);
		if(posServiceIsLocating){
			changeMenuIcon();
		}
		return true;
	}
	
	private void changeMenuIcon(){
		if(playMenu!=null){
			//change the icon
			playMenu.setIcon(R.drawable.stop_pos);
			playMenu.setTitle("stop");
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == R.id.start_pos) {
			// app icon in action bar clicked; start/stop PositioningService
			if(!posServiceIsLocating){
				// Validate that GPS, WiFi and BLE are enabled
				LocationManager mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
				boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
				WifiManager wifi = (WifiManager)getApplicationContext().getSystemService(WIFI_SERVICE);
				boolean isWifiEnabled = wifi.isWifiEnabled();
				boolean isBleEnabled = false;
				BluetoothAdapter mBluetoothAdapter = null;
				if(useBle){
					if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
					    Toast.makeText(this, "This device does not support BLE", Toast.LENGTH_SHORT).show();
					}
					else{
						final BluetoothManager bluetoothManager =
						        (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
						mBluetoothAdapter = bluetoothManager.getAdapter();
						isBleEnabled = mBluetoothAdapter.isEnabled();
					}
				}
				if(debug)	Log.d(TAG,"isGPSEnabled: " + isGPSEnabled + " , isWifiEnabled: " + isWifiEnabled + " , isBleEnabled: " + isBleEnabled);
				
				if(useWifi && !isWifiEnabled){
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage("Change settings before start positioning")
					       .setTitle("WiFi not enabled");
					builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
					    public void onClick(DialogInterface dialog, int which) {
					        // Do my action here
					    	Intent i = new Intent(android.provider.Settings.ACTION_SETTINGS);
							startActivity(i);
					    	dialog.dismiss();
					    }
					});
					builder.setNegativeButton("Enable", new DialogInterface.OnClickListener() {
					    public void onClick(DialogInterface dialog, int which) {
					        // Do my action here
					    	WifiManager wifi = (WifiManager)getApplicationContext().getSystemService(WIFI_SERVICE);
							boolean success = wifi.setWifiEnabled(true);
					    	if(!success){
					    		Toast.makeText(LocationIndoor.this, "WiFi could not be enabled", Toast.LENGTH_SHORT).show();
					    	}
					    	else{
					    		dialog.dismiss();
					    	}
					    }
					});
					AlertDialog alert = builder.create();
					alert.show();
				}
				else if (useBle && (mBluetoothAdapter == null || !isBleEnabled)) {
					// Ensures Bluetooth is available on the device and it is enabled. If not,
					// displays a dialog requesting user permission to enable Bluetooth.
					
				    AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage("Change settings before start positioning")
					       .setTitle("Bluetooth not enabled");
					builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
					    public void onClick(DialogInterface dialog, int which) {
					        // Do my action here
					    	Intent i = new Intent(android.provider.Settings.ACTION_SETTINGS);
							startActivity(i);
					    	dialog.dismiss();
					    }
					});
					builder.setNegativeButton("Enable", new DialogInterface.OnClickListener() {
					    public void onClick(DialogInterface dialog, int which) {
					        // Do my action here
					    	Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
						    startActivity(enableBtIntent);
						    dialog.dismiss();
					    }
					});
					AlertDialog alert = builder.create();
					alert.show();
				}
				else if(useGps && !isGPSEnabled){
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage("Change settings before start positioning")
					       .setTitle("GPS not enabled");
					builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
					    public void onClick(DialogInterface dialog, int which) {
					        // Do my action here
					    	Intent i = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							startActivity(i);
					    	dialog.dismiss();
					    }
					});
					AlertDialog alert = builder.create();
					alert.show();
				}
				else{
					startLocation();
					//change the icon
					playMenu.setIcon(R.drawable.stop_pos);
					playMenu.setTitle("stop");
				}
			}else{
				stopLocation();
				//change the icon
		    	playMenu.setIcon(R.drawable.start_pos);
		    	playMenu.setTitle("start");
			}
			return true;
		}  else {
			return super.onOptionsItemSelected(item);
		}

	}
	
	private void generateTestingRoute(int idRoute, int idSite){
		boolean staticRoute = false;
		int time = POSITION_REFRESH_TIME; // in milliseconds
		float v = 0.5f; // m/s
		//float v = 0.25f; // m/s (route 10)
		//float v = 0.15f; // m/s (route 11)
		//float pixelsMeter = 55.2f;  //ASCAMM Supermarket
		float pixelsMeter = 11.86f;  //Mercat Rubi
		float d = v*time/1000*pixelsMeter;
		float res_d = 0;
		Log.i(TAG,"time: " + time + "(ms) , v: " + v + "(m/s) , d: " + d + "(px)");
		ArrayList<Integer> nodesIdRoute = new ArrayList<Integer>();
		if(idRoute==0){
			staticRoute = true;
			int numPoints = 5000;
			Point3D point = new Point3D(296,30,0);
			for(int i=0; i<numPoints; i++){
				route.add(point);
			}
		}
		else if(idRoute==1){
			nodesIdRoute.add(4032);
			nodesIdRoute.add(4028);
			nodesIdRoute.add(4031);
			nodesIdRoute.add(4033);
			nodesIdRoute.add(4035);
			nodesIdRoute.add(4034);
			nodesIdRoute.add(4032);
			nodesIdRoute.add(4033);
		}
		else if(idRoute==2){
			nodesIdRoute.add(4033);
			nodesIdRoute.add(4031);
			nodesIdRoute.add(4028);
			nodesIdRoute.add(4032);
			nodesIdRoute.add(4034);
			nodesIdRoute.add(4035);
			nodesIdRoute.add(4033);
			nodesIdRoute.add(4032);
		}
		else if(idRoute==3){
			staticRoute = true;
			int numPoints = 300;
			Point3D point = new Point3D(48,319,0);
			for(int i=0; i<numPoints; i++){
				route.add(point);
			}
		}
		else if(idRoute==4){
			staticRoute = true;
			int numPoints = 300;
			Point3D point = new Point3D(215,581,0);
			for(int i=0; i<numPoints; i++){
				route.add(point);
			}
		}
		else if(idRoute==5){
			staticRoute = true;
			int numPoints = 300;
			Point3D point = new Point3D(220,36,0);
			for(int i=0; i<numPoints; i++){
				route.add(point);
			}
		}
		else if(idRoute==6){
			// Mercat Rubi (al mig)
			staticRoute = true;
			int numPoints = 180;
			Point3D point = new Point3D(746,518,0);
			for(int i=0; i<numPoints; i++){
				route.add(point);
			}
		}
		else if(idRoute==7){
			// Mercat Rubi (peixateria)
			staticRoute = true;
			int numPoints = 180;
			Point3D point = new Point3D(831,268,0);
			for(int i=0; i<numPoints; i++){
				route.add(point);
			}
		}
		else if(idRoute==8){
			// Mercat Rubi
			nodesIdRoute.add(74675);
			nodesIdRoute.add(74674);
			nodesIdRoute.add(74684);
			nodesIdRoute.add(74688);
			nodesIdRoute.add(74681);
			nodesIdRoute.add(74699);
			nodesIdRoute.add(74679);
			nodesIdRoute.add(74703);
			nodesIdRoute.add(74675);
		}
		else if(idRoute==9){
			// ASCAMM Supermarket (wifi + ble)
			nodesIdRoute.add(99938);
			nodesIdRoute.add(99939);
			nodesIdRoute.add(99941);
			nodesIdRoute.add(99937);
			nodesIdRoute.add(99935);
			nodesIdRoute.add(99936);
			nodesIdRoute.add(99938);
			nodesIdRoute.add(99937);
		}
		else if(idRoute==10){
			// CAR Sant Cugat (passadis + sortida)
			nodesIdRoute.add(147800);
			nodesIdRoute.add(147798);
			nodesIdRoute.add(147797);
			nodesIdRoute.add(147796);
			nodesIdRoute.add(147795);
			nodesIdRoute.add(156480);
		}
		else if(idRoute==11){
			// CAR Sant Cugat (passadis + piscina)
			nodesIdRoute.add(147800);
			nodesIdRoute.add(147798);
			nodesIdRoute.add(147797);
			nodesIdRoute.add(147809);
			nodesIdRoute.add(147808);
			nodesIdRoute.add(147804);
		}
		else if(idRoute==12){
			// CAR Sant Cugat (recepcio)
			staticRoute = true;
			int numPoints = 180;
			Point3D point = new Point3D(354,860,0);
			for(int i=0; i<numPoints; i++){
				route.add(point);
			}
		}
		else if(idRoute==13){
			// CAR Sant Cugat (davant vestuaris)
			staticRoute = true;
			int numPoints = 180;
			Point3D point = new Point3D(353,1017,0);
			for(int i=0; i<numPoints; i++){
				route.add(point);
			}
		}
		else if(idRoute==14){
			// CAR Sant Cugat (dins vestuaris)
			staticRoute = true;
			int numPoints = 18000;
			Point3D point = new Point3D(459,979,0);
			for(int i=0; i<numPoints; i++){
				route.add(point);
			}
		}
		else{
			Log.e(TAG,"Wrong route id");
		}
		if(!staticRoute){
			dBHelper = new DBHelper(this,idSite,null,false);
			if(dBHelper.isDatabaseOk()){
				Hashtable<Integer,Point3D> nodes = dBHelper.getNodes();
				// generate route points
				int numNodes = nodesIdRoute.size();
				for(int i=0;i<numNodes; i++){
					if(i!=(numNodes-1)){
						Point3D actualNode = nodes.get(nodesIdRoute.get(i));
						Point3D nextNode = nodes.get(nodesIdRoute.get(i+1));
						//Log.i(TAG,"actualNode: " + actualNode.toString() + " , nextNode: " + nextNode.toString());
						float angle = mMap.angleLine(actualNode, nextNode);
						double minX = Math.min(actualNode.x, nextNode.x);
						double maxX = Math.max(actualNode.x, nextNode.x);
						double minY = Math.min(actualNode.y, nextNode.y);
						double maxY = Math.max(actualNode.y, nextNode.y);
						Point3D p = new Point3D(actualNode.x, actualNode.y, actualNode.z);
						p.x = p.x + res_d*Math.cos(angle*Math.PI/180);
						p.y = p.y + res_d*Math.sin(angle*Math.PI/180);
						// Calculate all the points between these two nodes
						while(p.y<=maxY && p.y>=minY && p.x<=maxX && p.x>=minX){
							//Log.i(TAG,"routePoint: " + p.toString());
							route.add(new Point3D(p.x,p.y,p.z));
							p.x = p.x + d*Math.cos(angle*Math.PI/180);
							p.y = p.y + d*Math.sin(angle*Math.PI/180);
						}
						// Calculate residual distance
						res_d = mMap.distanceBetweenPoints(p, nextNode);
					}
				}
			}
		}
		
		numPointsRoute = route.size();
		//generate route in Map class
		mMap.setTestingRoute(route);
	}
	
	/*
	private void refreshTestingRoute(){
		Log.d(TAG,"inside refreshTestingRoute");
		for(Point3D point : route){
			point.setId(0);
		}
		pos = 0;
	}
	*/
	
	private void createRadioGroup(ArrayList<MapInfo> mapsInfo) {
		if(!mapsInfo.isEmpty()){
			int num_floors = mapsInfo.size();
			radioGroup.setOrientation(RadioGroup.VERTICAL);
			radioGroup.removeAllViews();
			int idCheck = -1;
			int floor = 0;
			for (int i = 0; i < num_floors; i++) {
				MapInfo map = mapsInfo.get(i);
				idCheck = map.getId();
				floor = map.getFloor();
				RadioButton rb = (RadioButton) this.getLayoutInflater().inflate(
						R.layout.indoor_radio_button, null);
				int heightInPx = (int) TypedValue.applyDimension(
						TypedValue.COMPLEX_UNIT_DIP, 40, getResources()
								.getDisplayMetrics());
				RadioGroup.LayoutParams rbParams = new RadioGroup.LayoutParams(
						RadioGroup.LayoutParams.MATCH_PARENT, heightInPx);
				rb.setLayoutParams(rbParams);
				rb.setText(String.valueOf(floor));
				rb.setId(idCheck);
				rb.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						RadioButton rb = (RadioButton) v;
						// Log.i(TAG,"id: " + rb.getId() + " , text: " +
						// rb.getText());
						changeFloor(Integer.parseInt(rb.getText().toString()));
					}
	
				});
				radioGroup.addView(rb); // the RadioButtons are added to the radioGroup
			}
			//Log.d(TAG,"inside createRadioGroup. idCheck: " + idCheck + " , idChecked: " + radioGroup.getCheckedRadioButtonId());
			if (idCheck != -1) {
				radioGroup.clearCheck();
				radioGroup.check(idCheck);
				mCurrentFloor = floor;
			}
		}else{
			if(debug)	Log.e(TAG,"mapsInfo is empty");
		}
	}

	private String getWifiMacAddress() {
		String wifiMac = null;
		WifiManager manager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		if (manager != null) {
			WifiInfo info = manager.getConnectionInfo();
			if (info != null) {
				String wifiAddress = info.getMacAddress();
				if (wifiAddress != null) {
					wifiMac = wifiAddress.toLowerCase();
				}
			}
		}
		//Log.d(TAG,"wifiMAC: " + wifiMac);
		return wifiMac;
	}

	private String getBleMacAddress() {
		String bleMac = null;
		if(android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.JELLY_BEAN_MR2){
			final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
			BluetoothAdapter mBluetoothAdapter = bluetoothManager.getAdapter();
			bleMac = mBluetoothAdapter.getAddress().toLowerCase();
		}
		//Log.d(TAG,"bleMac: " + bleMac);
		return bleMac;
	}
	
	private String getIMEI(Context context){
		String imei = null;
		TelephonyManager tManager = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
		if(tManager!=null){
			// LQ: TODO: check permissions to get DEVICE ID
			//if (checkPermission(Manifest.permission.READ_PHONE_STATE, 0, 0)) {
				imei = tManager.getDeviceId();
			//}
		}
		//Log.d(TAG, "IMEI: " + imei);
		return imei;
	}
	
	private String getAndroidId(Context context){
		String androidId = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
		//Log.d(TAG, "androidId: " + androidId);
		return androidId;
	}
	
	private String getSerialNumber() {
		String serialNumber = Build.SERIAL;
		//Log.d(TAG, "serialNumber: " + serialNumber);
		return serialNumber;
	}
	
	private String getDeviceId(Context context){
		//String uid = getWifiMacAddress();
		//String uid = getBleMacAddress();
		//String uid = getIMEI(context);
		//String uid = getAndroidId(context);
		String uid = getSerialNumber();
		return uid;
	}

	private boolean checkPermissions(){
		// Here, this is the current activity
		List<String> permissionsNeeded = new ArrayList<>();
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
			permissionAccessFineLocation = true;
		}
		else{
			permissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
			/*
			// Should we show an explanation?
			if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {

				// Show an explanation to the user *asynchronously* -- don't block
				// this thread waiting for the user's response! After the user
				// sees the explanation, try again to request the permission.
				Toast.makeText(this,"Explanation of the permission",Toast.LENGTH_SHORT).show();

			} else {
				// No explanation needed, we can request the permission.
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
			}
			*/
			// No explanation needed, we can request the permission.
			//ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
		}
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
			permissionWriteExternalStorage = true;
		}
		else{
			permissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
			/*
			// Should we show an explanation?
			if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {

				// Show an explanation to the user *asynchronously* -- don't block
				// this thread waiting for the user's response! After the user
				// sees the explanation, try again to request the permission.
				Toast.makeText(this,"Explanation of the permission",Toast.LENGTH_SHORT).show();

			} else {
				// No explanation needed, we can request the permission.
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
			}
			*/
			// No explanation needed, we can request the permission.
			//ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
		}
		if (!permissionsNeeded.isEmpty()) {
			ActivityCompat.requestPermissions(this, permissionsNeeded.toArray(new String[permissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
			return false;
		}
		return true;
	}

	private void setAllPermissionsOk(){
		permissionAccessFineLocation = true;
		permissionWriteExternalStorage = true;
		permissionReadPhoneState = true;
	}

	private boolean getAllPermissionsOk(){
		boolean ok = false;
		if(permissionAccessFineLocation && permissionWriteExternalStorage && permissionReadPhoneState){
			ok = true;
		}
		return ok;
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		if (requestCode == REQUEST_ID_MULTIPLE_PERMISSIONS) {
			java.util.Map<String, Integer> perms = new HashMap<>();
			// Initialize the map with both permissions
			perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
			perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
			// Fill with actual results from user
			if (grantResults.length > 0) {
				for (int i = 0; i < permissions.length; i++)
					perms.put(permissions[i], grantResults[i]);
				// Check for both permissions
				if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
						&& perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
					// all permissions needed granted
					Log.d(TAG, "location and write permissions granted");
					permissionAccessFineLocation = true;
					permissionWriteExternalStorage = true;
					if(getAllPermissionsOk()){
						startServices();
					}
				} else {
					Log.d(TAG, "Some permissions are not granted ask again ");
					showDialogPermissions();
				}
			}
			return;
		}
		else if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				// permission was granted
				permissionAccessFineLocation = true;
				if(getAllPermissionsOk()){
					startServices();
				}
			} else {
				// permission denied
				Toast.makeText(this, "You must accept permissions", Toast.LENGTH_SHORT).show();
				permissionAccessFineLocation = false;
			}
			return;
		}
		else if (requestCode == MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				// permission was granted
				permissionWriteExternalStorage = true;
				if(getAllPermissionsOk()){
					startServices();
				}
			} else {
				// permission denied
				Toast.makeText(this, "You must accept permissions", Toast.LENGTH_SHORT).show();
				permissionWriteExternalStorage = false;
			}
			return;
		}
		else{
			// other 'case' lines to check for other
			// permissions this app might request
			return;
		}
	}

	private void showDialogPermissions() {
		String message = "Location Services and Write Permissions required for this app";
		DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						checkPermissions();
						break;
					case DialogInterface.BUTTON_NEGATIVE:
						// proceed with logic by disabling the related features or quit the app.
						break;
				}
			}
		};
		new AlertDialog.Builder(this)
				.setMessage(message)
				.setPositiveButton("OK", clickListener)
				.setNegativeButton("Cancel", clickListener)
				.create()
				.show();
	}

	private void startServices(){
		if (startService(posServiceIntent) != null) {
			posServiceStarted = true;
		}

		if (useBoundPosService) {
			doBindPosService();
		}

		if (useFences) {
			if (startService(fenceServiceIntent) != null) {
				fenceServiceStarted = true;
			}
		}
	}

	private void stopServices(){
		if(useBoundPosService){
			try {
				doUnbindPosService();
			} catch (Throwable t) {
				Log.e(TAG, "Failed to unbind from the service", t);
			}
		}
		if(posServiceIsLocating == false){
			if(stopService(posServiceIntent)){
				posServiceStarted = false;
			}
		}
		if(fenceServiceStarted){
			if(stopService(fenceServiceIntent)){
				fenceServiceStarted = false;
			}
		}
	}

	public void setSettings(){
		Settings.useWifi = useWifi;
		Settings.useBle = useBle;
		Settings.useIBeacons = useIBeacons;
		Settings.useGps = useGps;
		Settings.useKalmanRSSI = useKalmanRSSI;
		Settings.useWeightedRSSI = useWeightedRSSI;
		Settings.useDetections = useDetections;
		Settings.bleRssDetection = bleRssDetection;
		Settings.useSensors = useSensors;
		Settings.siteInfoManual = siteInfoManual;
		Settings.demoMode = demoMode;
		Settings.storeLogs = storeLogs;
		Settings.logsFilename = logsFilename;
		Settings.pdbFromAssets = pdbFromAssets;
		if(positionCalculation.equals("local"))
			Settings.posLocal = true;
		else
			Settings.posLocal = false;
		Settings.lnsUrl = LNSserver;
		Settings.timeWindow = timeWindow;
		Settings.estimationType = estimationType;
		Settings.numPointsHyst = numPointsHyst;
		Settings.rssTransf = rssTransf;
		Settings.useParticleFilter = useParticleFilter;
		Settings.useParticleFilterSensors = useParticleFilterSensors;
		Settings.useMapFusion = useMapFusion;
		Settings.scansetNeeded = showScanset;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTextView("Click play button to start positioning");
		
		if(!useBoundPosService){
			IntentFilter posFilter = new IntentFilter();
			posFilter.addAction(PosActions.SERVICE_OK);
			posFilter.addAction(PosActions.SEND_LOCATIONS_CONFIG_RESP);
			posFilter.addAction(PosActions.IS_LOCATING_RESP);
			posFilter.addAction(PosActions.START_LOCATING_RESP);
			posFilter.addAction(PosActions.STOP_LOCATING_RESP);
			posFilter.addAction(PosActions.NEW_POSITIONS);
			posFilter.addAction(PosActions.NEW_DETECTIONS);
			posFilter.addAction(PosActions.CALC_ROUTE_RESP);
			posFilter.addAction(PosActions.ERROR);
			posFilter.addAction(PosActions.NEW_SCANSET);
			posFilter.addAction(PosActions.NEW_MOTION);
			posFilter.addAction(PosActions.NEW_AUTO_PARAMS);
			posFilter.addAction(PosActions.NEW_DATABASE_DATA);
			mPositionReceiver = new PositionReceiver();
			registerReceiver(mPositionReceiver, posFilter);
		}

		if(useFences){
			IntentFilter fenceFilter = new IntentFilter();
			fenceFilter.addAction(FenceActions.SERVICE_OK);
			fenceFilter.addAction(FenceActions.SET_FENCE_DB_RESP);
			fenceFilter.addAction(FenceActions.SET_SITE_ID_RESP);
			fenceFilter.addAction(FenceActions.GET_FENCES_RESP);
			fenceFilter.addAction(FenceActions.CALC_CLICKED_FENCE_RESP);
			fenceFilter.addAction(FenceActions.GET_CENTROID_OF_SELECTED_FENCE_RESP);
			fenceFilter.addAction(FenceActions.FENCE_REACHED);
			fenceFilter.addAction(FenceActions.FENCE_INFO);
			fenceFilter.addAction(FenceActions.ERROR);
			mFenceReceiver = new FenceReceiver();
			registerReceiver(mFenceReceiver, fenceFilter);

			if(useFencesDBapp){
				File fencesDBfile = new File(AppData.APP_DATA_FOLDER, fencesDBappName);
				// File fencesDBfile = new File("android.resource//",fencesDBappName);
				/*
				if(fencesDBfile.exists()) {
					fencesDbPath = fencesDBfile.getAbsolutePath();
				}
				else{
					Log.e(TAG,"FencesDB file could not be found");
				}
				*/
				fencesDbPath = fencesDBfile.getAbsolutePath();
			}
		}

		int apiVersion = android.os.Build.VERSION.SDK_INT;
		if(debug)	Log.d(TAG,"apiVersion: " + apiVersion);
		if(apiVersion >= Build.VERSION_CODES.M) {
			checkPermissions();
		}
		else{
			setAllPermissionsOk();
		}
		
		deviceId = getDeviceId(this);
		if(debug)	Log.d(TAG,"deviceId: " + deviceId);
		
		// Keep screen on
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		/*
		Hasher mHasher = new Hasher();
		String hash = mHasher.toSHA1("11:22:33:44:55:66AsCm@m45123456789");
		Log.d(TAG,"hash: " + hash);
		*/

		posServiceIntent = new Intent(this, PositioningService.class);
		fenceServiceIntent = new Intent(this, FenceService.class);
	}

	@Override
	protected void onResume() {
		super.onResume();

		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		useWifi = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_WIFI, Preferences.KEY_USE_WIFI_DEFAULT);
		useBle = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_BLE, Preferences.KEY_USE_BLE_DEFAULT);
		useIBeacons = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_IBEACONS, Preferences.KEY_USE_IBEACONS_DEFAULT);
		useGps = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_GPS, Preferences.KEY_USE_GPS_DEFAULT);
		useKalmanRSSI = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_KALMAN_RSSI, Preferences.KEY_USE_KALMAN_RSSI_DEFAULT);
		useWeightedRSSI = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_WEIGHTED_RSSI, Preferences.KEY_USE_WEIGHTED_RSSI_DEFAULT);
		useDetections = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_DETECTIONS, Preferences.KEY_USE_DETECTIONS_DEFAULT);
		if(useDetections){
			String bleRssString = sharedPref.getString(Preferences.KEY_EDIT_TEXT_BLE_RSS_DETECTION, Preferences.KEY_BLE_RSS_DETECTION_DEFAULT);
			if(bleRssString != null && bleRssString != ""){
				bleRssDetection = Float.valueOf(bleRssString);
			}
		}
		useSensors = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_SENSORS, Preferences.KEY_USE_SENSORS_DEFAULT);
		// Get site id from preferences
		siteInfoManual = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_SITE_INFO_MANUAL, Preferences.KEY_SITE_INFO_MANUAL_DEFAULT);
		String siteIdStr = sharedPref.getString(Preferences.KEY_EDIT_TEXT_SITE_ID, Preferences.KEY_SITE_ID_DEFAULT);
		if(siteIdStr != null && siteIdStr != ""){
			siteId = Integer.valueOf(siteIdStr);
		}
		demoMode = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_DEMO_MODE, Preferences.KEY_DEMO_MODE_DEFAULT);
		storeLogs = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_STORE_LOGS, Preferences.KEY_STORE_LOGS_DEFAULT);
		logsFilename = sharedPref.getString(Preferences.KEY_EDIT_TEXT_LOGS_FILENAME, Preferences.KEY_LOGS_FILENAME_DEFAULT);
		if(storeLogs && mLogWriter==null){
			String storeLogsFilename = logsFilename + "_pos" + Constants.logsFilenameExt;
			mLogWriter = new LogWriter(Constants.APP_DATA_FOLDER_NAME, storeLogsFilename);
		}
		//Obtain value of Send Locations
		sendLocations = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_SEND_POSITIONS, Preferences.KEY_SEND_POSITIONS_DEFAULT);
		if(sendLocations){
			String sendLocationsTimeStr = sharedPref.getString(Preferences.KEY_EDIT_TEXT_SEND_POSITIONS_TIME, Preferences.KEY_SEND_POSITIONS_TIME_DEFAULT);
			if(sendLocationsTimeStr != null && sendLocationsTimeStr != ""){
				sendLocationsTime = Integer.valueOf(sendLocationsTimeStr);
			}
			//Obtain the address of the server to store positions
			sendLocationsServer = sharedPref.getString(Preferences.KEY_EDIT_TEXT_SEND_POSITIONS_SERVER, Preferences.KEY_SEND_POSITIONS_SERVER_DEFAULT);
		}

		//Obtain the type of Position Calculation (local or remote)
		positionCalculation = sharedPref.getString(Preferences.KEY_LIST_POSITION_CALCULATION, Preferences.KEY_POSITION_CALCULATION_DEFAULT);
		LNSserver = sharedPref.getString(Preferences.KEY_EDIT_TEXT_SERVER_OBTAIN, Preferences.KEY_SERVER_OBTAIN_DEFAULT);
		//Obtain the time of the window to use
		String time = sharedPref.getString(Preferences.KEY_EDIT_TEXT_TIME_WINDOW, Preferences.KEY_TIME_WINDOW_DEFAULT);
		if(time != null && time != ""){
			timeWindow = Integer.valueOf(time);
		}
		//Obtain the type of estimation
		estimationType = sharedPref.getString(Preferences.KEY_LIST_ESTIMATION_TYPE, Preferences.KEY_ESTIMATION_TYPE_DEFAULT);
		//Obtain the number of points to use in the Floor Hysteresis (if enabled)
		useFloorHyst = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_FLOOR_HYSTERESIS, Preferences.KEY_USE_FLOOR_HYSTERESIS_DEFAULT);
		if(useFloorHyst){
			String numPoints = sharedPref.getString(Preferences.KEY_EDIT_TEXT_FLOOR_HYSTERESIS, Preferences.KEY_FLOOR_HYSTERESIS_DEFAULT);
			if(numPoints != null && numPoints != ""){
				numPointsHyst = Integer.valueOf(numPoints);
			}
		}
		//Obtain value of RSS Transformation
		rssTransf = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_RSS_TRANSFORMATION, Preferences.KEY_USE_RSS_TRANSFORMATION_DEFAULT);
		useParticleFilter = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_PARTICLE_FILTER, Preferences.KEY_USE_PARTICLE_FILTER_DEFAULT);
		useParticleFilterSensors = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_PARTICLE_FILTER_SENSORS, Preferences.KEY_USE_PARTICLE_FILTER_SENSORS_DEFAULT);
		useMapFusion = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_MAP_FUSION, Preferences.KEY_USE_MAP_FUSION_DEFAULT);


		//Obtain value of Show Candidates
		showCandidates = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_CANDIDATES, Preferences.KEY_SHOW_CANDIDATES_DEFAULT);
		showScanset = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_SHOW_SCANSET, Preferences.KEY_SHOW_SCANSET_DEFAULT);
		showSubset = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_SHOW_SUBSET, Preferences.KEY_SHOW_SUBSET_DEFAULT);
		showPaths = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_SHOW_PATHS, Preferences.KEY_SHOW_PATHS_DEFAULT);
		showOrientation = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_SHOW_ORIENTATION, Preferences.KEY_SHOW_ORIENTATION_DEFAULT);
		showSteps = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_SHOW_STEPS, Preferences.KEY_SHOW_STEPS_DEFAULT);
		showFilterParticles = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_SHOW_FILTER_PARTICLES, Preferences.KEY_SHOW_FILTER_PARTICLES_DEFAULT);
		useGoogleMaps = sharedPref.getBoolean(Preferences.KEY_CHECKBOX_USE_GOOGLE_MAPS, Preferences.KEY_USE_GOOGLE_MAPS_DEFAULT);
		if(useGoogleMaps){
			setTitle("Location Google Maps");
		}

		setSettings();
		
		if(debug){
			Log.i(TAG,"--- GENERAL ---");
			Log.i(TAG,"useWifi: " + useWifi);
			Log.i(TAG,"useBle: " + useBle);
			Log.i(TAG,"useIBeacons: " + useIBeacons);
			Log.i(TAG,"useGps: " + useGps);
			Log.i(TAG,"useKalmanRSSI: " + useKalmanRSSI);
			Log.i(TAG,"useWeightedRSSI: " + useWeightedRSSI);
			Log.i(TAG,"useDetections: " + useDetections);
			Log.i(TAG,"detectionLevel: " + bleRssDetection);
			Log.i(TAG,"useSensors: " + useSensors);
			Log.i(TAG,"siteInfoManual: " + siteInfoManual);
			Log.i(TAG,"siteId: " + siteId);
			Log.i(TAG,"demoMode: " + demoMode);
			Log.i(TAG,"storeLogs: " + storeLogs);
			Log.i(TAG,"storeLogsFilename: " + logsFilename);
			Log.i(TAG,"sendLocations: " + sendLocations);
			Log.i(TAG,"sendLocationsTime: " + sendLocationsTime);
			Log.i(TAG,"sendLocationsServer: " + sendLocationsServer);
			Log.i(TAG,"--- ALGORITHM ---");
			Log.i(TAG,"positionCalculation: " + positionCalculation);
			Log.i(TAG,"LNSserver: " + LNSserver);
			Log.i(TAG,"timeWindow: " + timeWindow);
			Log.i(TAG,"estimationType: " + estimationType);
			Log.i(TAG,"numPointsHyst: " + numPointsHyst);
			Log.i(TAG,"rssTransf: " + rssTransf);
			Log.i(TAG,"useParticleFilter: " + useParticleFilter);
			Log.i(TAG,"useParticleFilterSensors: " + useParticleFilterSensors);
			Log.i(TAG,"useMapFusion: " + useMapFusion);
			Log.i(TAG,"--- UI ---");
			Log.i(TAG,"showCandidates: " + showCandidates);
			Log.i(TAG,"showScanset: " + showScanset);
			Log.i(TAG,"showSubset: " + showSubset);
			Log.i(TAG,"showPaths: " + showPaths);
			Log.i(TAG,"showOrientation: " + showOrientation);
			Log.i(TAG,"showSteps: " + showSteps);
			Log.i(TAG,"showFilterParticles: " + showFilterParticles);
			Log.i(TAG,"useGoogleMaps: " + useGoogleMaps);
		}
		if((LNSserver.equals("") || LNSserver.equals("0.0.0.0")) && positionCalculation.equals("remote")){
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Incorrect Server Address")
					.setMessage("Change server settings before start positioning")
					.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// Do my action here
							Intent i = new Intent(LocationIndoor.this, Preferences.class);
							startActivity(i);
							dialog.dismiss();
						}
					});
			AlertDialog alert = builder.create();
			alert.show();
		}
		if(demoMode && testRoute){
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Alert")
					.setMessage("demoMode and testRoute activated. What do you want to do?")
					.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
			    		public void onClick(DialogInterface dialog, int which) {
			        		// Do my action here
			    			dialog.dismiss();
			    		}
					})
					.setNegativeButton("Settings", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// Do my action here
							Intent i = new Intent(LocationIndoor.this, Preferences.class);
							startActivity(i);
							dialog.dismiss();
						}
					});
			AlertDialog alert = builder.create();
			alert.show();
		}
		if(getAllPermissionsOk()){
			startServices();
		}
		//Toast.makeText(this, "onResume LocationIndoor", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	protected void onPause(){
		if(getAllPermissionsOk()){
			stopServices();
		}
		//Toast.makeText(this, "onPause LocationIndoor", Toast.LENGTH_SHORT).show();
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		if(!useBoundPosService){
			unregisterReceiver(mPositionReceiver);
		}
		if(useFences){
			unregisterReceiver(mFenceReceiver);
		}
		/*
		if(mBound){
			stopLocation();
		}
		*/
		if(mBmpMap!=null){
			mBmpMap.recycle();
		}
		//Toast.makeText(this, "onDestroy LocationIndoor", Toast.LENGTH_SHORT).show();
		super.onDestroy();	
	}

	private void doBindPosService() {
		// Establish a connection with the service. We use an explicit
		// class name because we want a specific service implementation that
		// we know will be running in our own process (and thus won't be
		// supporting component replacement by other applications).
		bindService(posServiceIntent, mPosServiceConnection, BIND_AUTO_CREATE);
	}

	private void doUnbindPosService() {
		if (posServiceBound) {
			// Detach our existing connection.
			unbindService(mPosServiceConnection);
			posServiceBound = false;
		}
	}
	
	private void startLocation(){
		totalSteps = 0;
		totalMotionModeSteps = 0;
		updateStepsUI(0);
		//doBindService();
		if(useBoundPosService){
			if(posServiceBound && posServiceStarted){
				mPositioningService.Start(POSITION_REFRESH_TIME);
				if(runForegroundService){
					Intent notificationIntent = new Intent(this, LocationIndoor.class);
					//Intent notificationIntent = new Intent();
					PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

					Notification notification = new NotificationCompat.Builder(getApplicationContext())
							.setContentTitle(getString(R.string.app_name))
							.setContentText("location running")
							.setSmallIcon(R.mipmap.ic_launcher)
							.setWhen(System.currentTimeMillis())
							.setContentIntent(pendingIntent)
							.build();
					mPositioningService.startForeground(NOTIFICATION_ID, notification);
				}
				posServiceIsLocating = true;
			}
		}
		else{
			if(posServiceStarted){
				Intent intent = new Intent();
				intent.setAction(PosActions.START_LOCATING);
				intent.putExtra("period", POSITION_REFRESH_TIME);
				sendBroadcast(intent);
				posServiceIsLocating = true;
			}
		}
		// LQ: commented message Requesting location
		dialog = new ProgressDialog(this);
		dialog.setMessage("Requesting Location...");
		//dialog.setTitle("Title");
		dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
    	dialog.show();
    	/** Code added to store data info into logs.txt **/
		if(storeLogs && mLogWriter.startLogs()>0){
			logsRunning = true;
		}
	}
	
	private void stopLocation(){
		if(useBoundPosService){
			if(posServiceBound && posServiceStarted){
				// Comment next line in simulation mode
				mPositioningService.Stop();
				if(runForegroundService){
					mPositioningService.stopForeground(true);
				}
				posServiceIsLocating = false;
			}
		}
		else{
			if(posServiceStarted){
				Intent intent = new Intent();
				intent.setAction(PosActions.STOP_LOCATING);
				sendBroadcast(intent);
				posServiceIsLocating = false;
			}
		}
		/*
		try {
			doUnbindService();
		} catch (Throwable t) {
			Log.e(TAG, "Failed to unbind from the service", t);
		}
		*/
		/** Code added to store data info into logs.txt **/
		if(storeLogs && logsRunning){
			mLogWriter.stopLogs();
			logsRunning = false;
		}
	}
	
	/*
	private void drawPosition(LocationData userPosition) {
		//Change the floor image if user has changed the floor
		if(!userPosition.sourceType.equalsIgnoreCase("gps") && mCurrentFloor != userPosition.positionLocal[2]){
			changeFloor((int) userPosition.positionLocal[2]);
		}
		//Update user's position
		if(useGoogleMaps){
			drawGoogleMapsPosition(userPosition);
		}
		else{
			mMap.drawUserPosition(userPosition);
		}
	}
	*/

	private void drawPosition(ArrayList<LocationData> userPositions) {
		// RP: LOGs. TODO: delete 'hem
		Double a = userPositions.get(0).positionLocal[2];
		Log.d(TAG,"Not a integer number (with float type)" + userPositions.get(0).positionLocal[2]);

		//Change the floor image if user has changed the floor
		if(!userPositions.get(0).sourceType.equalsIgnoreCase("gps") &&
				mCurrentFloor !=  Math.round(userPositions.get(0).positionLocal[2])){
			changeFloor((int)  Math.round(userPositions.get(0)
					.positionLocal[2]));
		}
		//Update user's position
		if(useGoogleMaps){
			drawGoogleMapsPosition(userPositions.get(0));
		} else {
			mMap.drawUserPosition(userPositions);
		}
	}
	
	private void drawTwoPositions(ArrayList<LocationData> userPositions) {
		//Change the floor image if user has changed the floor
		if(!userPositions.get(0).sourceType.equals("gps") && mCurrentFloor !=
				Math.round(userPositions.get(0).positionLocal[2])){
			changeFloor((int)  Math.round(userPositions.get(0)
					.positionLocal[2]));
		}
		//Update user's position
		if(useGoogleMaps){
			drawGoogleMapsPosition(userPositions.get(0));
		} else {
			mMap.drawTwoUserPosition(userPositions);
		}
	}
	
	private void drawGoogleMapsPosition(LocationData userPosition) {
		mGoogleMap.drawPosition(userPosition);
		if (showCandidates) {
			mGoogleMap.drawCandidates(userPosition);
		}
	}
	
	private void drawDetections(ArrayList<Detection> detections) {
		ArrayList<Point3D> posDetections = new ArrayList<Point3D>();
		if(useGoogleMaps){
			for(Detection det : detections){
				if(det.getPosGlobal()!=null){
					posDetections.add(det.getPosGlobal());
				}
			}
			mGoogleMap.drawDetections(posDetections, mCurrentFloor);
		}
		else{
			for(Detection det : detections){
				if(det.getPosLocal()!=null){
					posDetections.add(det.getPosLocal());
				}
			}
			mMap.drawDetections(posDetections, mCurrentFloor);
		}
	}
	
	private void drawFences(ArrayList<Fence> fences){
		if(useGoogleMaps){
			// TODO draw Fences on GoogleMaps
		} else {
			mMap.drawFences(fences, mCurrentFloor);
		}
	}
	
	private void drawGeoFences(ArrayList<GeoFence> fences){
		if(useGoogleMaps){
			// TODO draw Fences on GoogleMaps
		}
		else{
			mMap.drawGeoFences(fences, mCurrentFloor);
		}
	}
	
	private ArrayList<Edge> getNodesAndEdges(int floor){
		ArrayList<Edge> mEdges = null;
		if(dBHelper==null){
			dBHelper = new DBHelper(this,siteId,null,false);
		}
		if(dBHelper.isDatabaseOk()){
			ArrayList<Point3D> mNodes = dBHelper.getNodesOfFloor(floor);
			mEdges = dBHelper.getEdgesOfFloor(floor);
			//Matrix matrix = dBHelper.getConversionMatrix();
			CoordConversion coordConv = dBHelper.getConversionClass();

			if (debug) {
				Log.i(TAG, "numNodes: " + mNodes.size());
				for(Point3D n : mNodes){
					Log.i(TAG,"node: " + n.getId() + " , " + n.toString());
				}
				Log.i(TAG, "numEdges: " + mEdges.size());
			}
			for(Edge e : mEdges){
				Point3D startNodeGlobal = new Point3D(-1,-1,-1);
				Point3D endNodeGlobal = new Point3D(-1,-1,-1);
				if(coordConv!=null){
					//startNodeGlobal = matrix.multiplyMV(e.getStartNode());
					//endNodeGlobal = matrix.multiplyMV(e.getEndNode());
					Point3DWithFloor startNode = e.getStartNode();
					Point3D startNodeLocal = startNode.getPoint3DWithoutHeight();
					LatLng3D startNodeConv = coordConv.convertFromLocalToGlobal(startNodeLocal);
					startNodeGlobal = startNodeConv.getPoint3DfromLatLng3D();
					Point3DWithFloor endNode = e.getEndNode();
					Point3D endNodeLocal = endNode.getPoint3DWithoutHeight();
					LatLng3D endNodeConv = coordConv.convertFromLocalToGlobal(endNodeLocal);
					endNodeGlobal = endNodeConv.getPoint3DfromLatLng3D();
				}
				Point3DWithFloor finalStartNodeGlobal = new Point3DWithFloor(startNodeGlobal.x, startNodeGlobal.y, 0, startNodeGlobal.z);
				Point3DWithFloor finalEndNodeGlobal = new Point3DWithFloor(endNodeGlobal.x, endNodeGlobal.y, 0, endNodeGlobal.z);
				e.setStartNode(finalStartNodeGlobal);
				e.setEndNode(finalEndNodeGlobal);
				if(debug){
					Log.i(TAG,"startNode: (" + e.startNode.x + "," + e.startNode.y + "," + e.startNode.z + ")");
					Log.i(TAG,"endNode: (" + e.endNode.x + "," + e.endNode.y + "," + e.endNode.z + ")");
				}
			}
		}
		return mEdges;
	}
	
	private void changeFloor(int floor) {
		Log.i(TAG, "inside changeFloor. floor: " + floor + " , mCurrentFloor: " + mCurrentFloor);
		if(useGoogleMaps){
			boolean changes = false;
			String mapPath = null;
	    	int buttonId = -1;
			if (floor != mCurrentFloor) {
				for (MapInfo map : mMapsInfo) {
					if (floor == map.getFloor()) {
						if (mBmpMap != null) {
							mBmpMap.recycle();
						}
						String mapFilename = map.getFilename();
						mapPath = mMapBasePath + mapFilename;
						buttonId = map.getId();
						changes = true;
						mCurrentFloor = floor;
						break;
					}
				}
			}
	    	if(changes){
	    		mGoogleMap.changeFloorImage(mapPath);
	    		radioGroup.check(buttonId);
	    		if(showPaths){
					ArrayList<Edge> mEdges = getNodesAndEdges(mCurrentFloor);
					mGoogleMap.drawEdges(mEdges);
				}
	    	}
		}
		else{
			boolean changes = false;
			int buttonId = -1;
			if (floor != mCurrentFloor) {
				for (MapInfo map : mMapsInfo) {
					if (floor == map.getFloor()) {
						if (mBmpMap != null) {
							mBmpMap.recycle();
						}
						String mapFilename = map.getFilename();
						if(useLocalMaps){
							mBmpMap = BitmapFactory.decodeResource(getResources(), Integer.parseInt(mapFilename));
						}
						else{
							mBmpMap = BitmapFactory.decodeFile(mMapBasePath	+ mapFilename);
						}
						buttonId = map.getId();
						changes = true;
						mCurrentFloor = floor;
						break;
					}
				}
			}
			if (changes) {
				// change the image of the floor
				mMap.changeFloor(mCurrentFloor);
				mMap.changeFloorMap(mBmpMap);
				if(geoFences !=null){
					mMap.drawGeoFences(geoFences, mCurrentFloor);
				}
				mMap.setRefresh(true);
				radioGroup.check(buttonId);
			}
		}
	}

	private void setGeoFences(ArrayList<GeoFence> fences){
		this.geoFences = fences;
		if(fences!=null){
			mFencesAdapter = new FenceAdapter(this, R.layout.fence_item_row, fences);
		}
	}
	
	private void registerPointselectedListener(){
		if(useGoogleMaps){
		
		}
		else{
			mMap.addSelectedPointListener(mPointSelectedListener);
		}
	}
	
	public void showFences(View view){
		showFencesDialog();
	}
	
	private void showFencesDialog(){
		mFencesAdapter.setSelectedFenceId(-2);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Select a Fence");
		builder.setSingleChoiceItems(mFencesAdapter, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            // The 'which' argument contains the index position
            // of the selected item
            	Log.d(TAG,"item clicked: " + which);
            	GeoFence selectedFence = mFencesAdapter.getItem(which);
            	mFencesAdapter.setSelectedFenceId(selectedFence.getId());
            	mFencesAdapter.notifyDataSetChanged();
            }
		});
		builder.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// OK button clicked
						int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
						if(selectedPosition>=0){
							GeoFence selectedFence = mFencesAdapter.getItem(selectedPosition);
							int fenceId = selectedFence.getId();
							if(fenceId==-1){
								//Select from map
								waitingPointSelection = true;
							}
							else{
								getCentroidOfSelectedFence(fenceId, selectedFence.getName());
							}
							dialog.dismiss();
						}
						else{
							Toast.makeText(LocationIndoor.this, "No Fence has been selected", Toast.LENGTH_SHORT).show();
						}
						Log.d(TAG,"OK button clicked. Position: " + selectedPosition);
					}
				});
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Cancel button clicked
						dialog.dismiss();
						Log.d(TAG, "Cancel button clicked: " + which);
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	public void unselectFence(View view) {
		processFenceSelected(-1, null, null);
		if(mMapVisible){
			mMap.setDestinationPoint(null);
			mMap.setRoute(null);
			mMap.setRefresh(true);
			fenceInfoLayout.setVisibility(View.GONE);
			fenceUnselect.setVisibility(View.GONE);
		}
	}
	
	private void getCentroidOfSelectedFence(int fenceId, String fenceName){
		/*
		if(useBoundPosService){
			Point3D centroid = mPositioningService.getCentroidOfSelectedFence(fenceId, currentPosition);
			processFenceSelected(fenceId, fenceName, centroid);
		}
		else{
			sendGetCentroidOfSelectedfFenceToService(fenceId, fenceName, currentPosition);
		}
		*/
		sendGetCentroidOfSelectedFenceToFenceService(fenceId, fenceName, currentPosition);
	}

	/*----------------------------------------------------------------*/

	private class RTTimerTask extends TimerTask {

		@Override
		public void run() {
			if(currentSelectedPoi != null) {
				WebService ws = new WebService(getResources().getString(R.string.ws_openrtls_url_base) + getResources().getString(R.string.ws_get_pois) + "id/" + currentSelectedPoi.getId());
				try {
					ws.Execute(WebService.RequestMethod.GET);
					WebServiceResult result = new WebServiceResult(ws.getResponseCode(),ws.getResponse());
					android.os.Message msg = new android.os.Message();
					msg.obj = result;
					RTHandler.sendMessage(msg);
				} catch(Exception e) {
					//there was an error when making the petition
					Log.d(TAG, e.getMessage());
				}
			}
		}
	}

	private Handler RTHandler = new Handler() {

		public void handleMessage(android.os.Message msg) {
			WebServiceResult result = (WebServiceResult) msg.obj;
			try {
				JSONObject json = new JSONObject(result.getResponse());
				if(json.getString("status").equals("success")) {
					JSONArray data = json.getJSONArray("data");

					JSONObject element = data.getJSONObject(0);
					int id = element.getInt("id");

					//search for element in our list of pois
					POI poi = null;
					for(POI ipoi: mobilePois) {
						if(ipoi.getId() == element.getInt("id")) {
							poi = ipoi;
							break;
						}
					}

					if(poi == null) {
						//this should not happen
						Log.d(TAG, "Realtime path planner error on updating POI");
					} else {
						Point3D point = mPositioningService.convertFromGlobalToLocal(new LatLng3D(element.getDouble("lat"), element.getDouble("lon"), element.getDouble("alt")));

						//check if POI has changed position
						if(poi.getPoint().x != point.x || poi.getPoint().y != point.y || poi.getPoint().z != point.z) {
							poi.setPoint(point);
							mPositioningService.calculateRoute(currentPosition, point);
						}
					}
				}
			} catch (JSONException e) {
				Log.d(TAG, "Error parsing JSON response!");
			}
		}
	};

	/*----------------------------------------------------------------*/

	/**
	 * LQ: POIs
	 */

	/** AirFi POIs ***/

	private void queryForAirFiPois(String type, WebServiceCompleteListener<String> completeListener) {

		if(completeListener == null) {
			completeListener = new POIWebserviceOnComplete();
		}

		if(type == null) {
			type = "all";
		}

		WebServiceAT wsAT = new WebServiceAT(this, completeListener, this.getResources().getString(R.string.ws_openrtls_url_base) + getResources().getString(R.string.ws_get_pois) + type, WebService.RequestMethod.POST);
		wsAT.AddDialog();
		wsAT.setDialogMessage("Downloading points of interest...");
		//Log.d(TAG,"--- deviceId: " + deviceId + " , siteId: " + siteId);
		JSONObject data = new JSONObject();
		try {
			data.put("device_id", deviceId);
			data.put("site_id", siteId);
			wsAT.addJSON(data);
			wsAT.execute();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setPOIs(ArrayList<POI> pois){
		if(pois == null) {
			pois = new ArrayList<POI>();
		}
		this.staticPois = pois;
		mMap.drawPOIs(pois, "static");
		mPOIsAdapter = new PoiAdapter(this, R.layout.poi_item_row, pois);

	}

	private void registerPOIselectedListener(){
		if(useGoogleMaps){

		}
		else{
			mMap.addSelectedPointListener(mPointSelectedListener);
		}
	}

	public void showPOIs(View view){
		refreshAndShowPOIDialog();
	}

	public void refreshPOIDialogAdapter(ArrayList<POI> pois) {
		mPOIsAdapter = new PoiAdapter(this, R.layout.poi_item_row, pois);
	}

	private void refreshAndShowPOIDialog() {

		queryForAirFiPois("all", new WebServiceOnCompleteListener() {
			@Override
			public void onTaskComplete(WebServiceResult result) {

				if (result.getResponse() != null) {
					try {
						JSONObject json = new JSONObject(result.getResponse());
						if (json.getString("status").equals("success")) {
							JSONArray data = json.getJSONArray("data");
							ArrayList<POI> pois = parseWebservicePOIsResponse(data);
							savePOIs(pois);
						}
					} catch (JSONException e) {
						Log.d(TAG, "Error parsing JSON response!");
					}

					ArrayList<POI> pois = new ArrayList<POI>();
					pois.addAll(staticPois);
					pois.addAll(mobilePois);

					refreshPOIDialogAdapter(pois);
					showPOIsDialog();
				} else {
					errorObtainingPOIDialog();
				}
			}
		});

	}

	private void errorObtainingPOIDialog() {
		AlertDialog alertDialog = new AlertDialog.Builder(LocationIndoor.this).create();
		alertDialog.setTitle("Error");
		alertDialog.setMessage("Points of interest could not be obtained");
		alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		alertDialog.show();
	}

	private void showPOIsDialog(){
		//if mPOIsAdapter is null, it's because POIS haven't been already loaded

                /*if(mPOIsAdapter == null) {
                        mPOIsAdapter = new PoiAdapter(context, layoutResourceId, data)
                }*/

		mPOIsAdapter.setSelectedPoiId(-2);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Select a POI");
		builder.setSingleChoiceItems(mPOIsAdapter, -1, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				// The 'which' argument contains the index position
				// of the selected item
				Log.d(TAG,"item clicked: " + which);
				POI selectedPOI = mPOIsAdapter.getItem(which);
				mPOIsAdapter.setSelectedPoiId(selectedPOI.getId());
				mPOIsAdapter.notifyDataSetChanged();
			}
		});
		builder.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// OK button clicked
						int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
						if(selectedPosition>=0){
							POI selectedPOI = mPOIsAdapter.getItem(selectedPosition);
							int poiId = selectedPOI.getId();
							if(poiId==-1){
								//Select from map
								mMap.setDestinationPoint(null);
								changeRouteMode(CHOOSING_POINT);
							}
							else{
								if(selectedPOI.isAvailable()) {
									destinationPosition = selectedPOI.getPoint();
									processPoiSelected(selectedPOI);
									changeRouteMode(SHOWING_ROUTE);
									mMap.setDestinationPoint(null);
									mMap.setRefresh(true);
									dialog.dismiss();
								} else {
									showPOITooOldError();
									dialog.dismiss();
								}
							}
						}
						else{
							Toast.makeText(LocationIndoor.this, "No POI has been selected", Toast.LENGTH_SHORT).show();
						}
						Log.d(TAG,"OK button clicked. Position: " + selectedPosition);
					}
				});
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Cancel button clicked
						dialog.dismiss();
						Log.d(TAG,"Cancel button clicked: " + which);
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	//notify mobile POI is too old to be routed through
	private void showPOITooOldError() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Last position is too old")
				.setTitle("Error!");
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		}).show();
	}

	public void unselectPOI(View view) {
		if(this.getRouteMode() == ROUTE_NAVIGATION) {
			this.changeRouteMode(SHOWING_ROUTE);
		} else {
			this.changeRouteMode(POSITION);
		}
	}

	/**
	 * Function to check if a POI has been selected
	 * @param pt - Point3D
	 * @return POI
	 */
	private POI calcClickedPOI(Point3D pt){
		POI selectedPOI = null;

                /* check if the clicked point is in the area of a POI */
		if(staticPois != null){
			for (POI poi : staticPois){
				if (poi.getPoint().z == mCurrentFloor && distanceBetweenPoints(pt, poi.getPoint())<poi.getRadius()){
					selectedPOI = poi;
					return selectedPOI;
				}
			}
		}


		//mobile POIS are not drew, so they cannot be selected
                /*if(mobilePois != null) {
                        for (POI poi : mobilePois){
                                if (poi.getPoint().z == mCurrentFloor && distanceBetweenPoints(pt, poi.getPoint())<poi.getRadius()){
                                        selectedPOI = poi;
                                        return selectedPOI;
                                }
                        }
                }*/

		return selectedPOI;
	}

	private void startUpdatingMobilePOI() {
		//check if it is already started
		if(this.RTTimer != null) {
			this.RTTimer.cancel();
		}

		this.RTTimer = new Timer();
		this.RTTask = new RTTimerTask();
		this.RTTimer.schedule(RTTask, 0, LocationIndoor.RTPollingTime);

	}

	private void stopUpdatingMobilePOI() {

		if(this.RTTimer != null) {
			this.RTTimer.cancel();
		}

	}

	private void savePOIs(ArrayList<POI> pois) {

		if(pois != null) {

			staticPois = new ArrayList<POI>();
			mobilePois = new ArrayList<POI>();

			for(POI poi: pois) {
				if(poi.isMobile()) {
					this.mobilePois.add(poi);
				} else {
					this.staticPois.add(poi);
				}
			}

			this.mMap.drawPOIs(staticPois, "static");
			this.mMap.drawPOIs(mobilePois, "mobile");
		}
	}

	private ArrayList<POI> parseWebservicePOIsResponse(JSONArray data) throws JSONException {

		JSONObject element;
		ArrayList<POI> pois = new ArrayList<POI>();
		POI poi;
		Point3D point;

		//get floor list to save only pois which are located in one of the available floors
		List<Integer> floors = this.mPositioningService.getFloorList();

		for(int i=0; i < data.length(); i++) {
			element = data.getJSONObject(i);

			if(floors.contains((int) element.getDouble("alt"))) {

				point = mPositioningService.convertFromGlobalToLocal(new LatLng3D(element.getDouble("lat"), element.getDouble("lon"), element.getDouble("alt")));
				boolean isMobile;
				if(point == null) {
					Log.d(TAG, "Cannot convert coordinates from global to local! Aborting!");
					pois = null;
					break;
				} else {
					if(element.getInt("mobile") == 1) {
						isMobile = true;
					} else {
						isMobile = false;
					}
					poi = new POI(element.getInt("id"), point, element.getString("name"), element.getString("type"), isMobile);
					if(element.has("available")) {
						if(element.getInt("available") == 0) {
							poi.setAvailable(false);
						} else {
							poi.setAvailable(true);
						}
					}
				}

				pois.add(poi);
			}
		}
		return pois;
	}

	private class POIWebserviceOnComplete implements WebServiceCompleteListener<String> {

		@Override
		public void onTaskComplete(WebServiceResult result) {

			if(result.getResponse() != null) {

				try {
					JSONObject json = new JSONObject(result.getResponse());
					if(json.getString("status").equals("success")) {
						JSONArray data = json.getJSONArray("data");
						ArrayList<POI> pois = parseWebservicePOIsResponse(data);
						setPOIs(pois);
					}
				} catch (JSONException e) {
					Log.d(TAG, "Error parsing JSON response!");
				}

			} else {
				errorObtainingPOIDialog();
			}

		}

	}

	/** calculate distance between two points **/
	public double distanceBetweenPoints(Point3D a, Point3D b){
		float x = (float) (a.x - b.x);
		float y = (float) (a.y - b.y);
		return Math.sqrt(x*x + y*y);
	}

	private void processPoiSelected(POI poi){
		int poiId = -1;
		if (poi!=null) {
			poiId = poi.getId();
		}
		this.currentSelectedPoi = poi;
		String txtMsg = "poiSelected: " + poiId;
		if(debug)       Log.d(TAG, txtMsg);
		//Toast.makeText(LocationIndoor.this, txtMsg, Toast.LENGTH_SHORT).show();
		if(mMapVisible) {
			mMap.setSelectedPoiId(poiId);
		}
	}

	/**
	 * -------------- END OF POIs -----------------
	 */

	/**
	 * Routes: necessary for POIs
	 */

	private void changeRouteMode(int newMode) {

		switch(newMode) {
			case STOPPED:
				if(mMapVisible){
					poiInfoLayout.setVisibility(View.GONE);
					quieroIr.setVisibility(View.GONE);
					poiUnselect.setVisibility(View.GONE);
					incident.setVisibility(View.GONE);
					this.mMap.setRoute(null);
					this.mMap.setDestinationPoint(null);
					mMap.setMobileDestination(null);
				}
				processPoiSelected(null);
				this.currentSelectedPoi = null;
				break;

			case POSITION:
				//we don't show any button, except the one to choose a poi from list
				if (usePOIs) {
					poiInfoLayout.setVisibility(View.GONE);
					quieroIr.setVisibility(View.VISIBLE);
					processPoiSelected(null);
					poiUnselect.setVisibility(View.GONE);
					this.currentSelectedPoi = null;
				}
				this.mMap.setRoute(null);
				this.mMap.setDestinationPoint(null);
				mMap.setMobileDestination(null);
				incident.setVisibility(View.VISIBLE);
				break;

			case CHOOSING_POINT:
				poiInfoLayout.setVisibility(View.GONE);
				quieroIr.setVisibility(View.GONE);
				poiUnselect.setVisibility(View.GONE);
				quieroIr.setVisibility(View.GONE);
				poiUnselect.setText("Cancel");
				this.mMap.setRoute(null);
				mMap.setMobileDestination(null);
				incident.setVisibility(View.VISIBLE);
				break;

			case SHOWING_ROUTE:
				if (usePOIs) {
					poiInfoLayout.setVisibility(View.VISIBLE);
					quieroIr.setVisibility(View.GONE);
					routeLL.setVisibility(View.VISIBLE);
					routeTxt.setVisibility(View.VISIBLE);
					routeTxt.setText("Navigate");
					poiUnselect.setVisibility(View.VISIBLE);
					poiUnselect.setText("Delete");
					if(this.getRouteMode() == ROUTE_NAVIGATION) {
						mMap.setNavigationMode(false);
						stopUpdatingMobilePOI();
					}
					if(this.currentSelectedPoi == null) {
						this.mPositioningService.calculateRoute(currentPosition, destinationPosition);
						this.mMap.setDestinationPoint(this.destinationPosition);
					} else {
						//LQ: DEBUG currentPosition, currentSelectedPOI
						Log.d("LQ","current position: "+currentPosition+" selected POI: "+currentSelectedPoi);
						this.mPositioningService.calculateRoute(currentPosition, this.currentSelectedPoi.getPoint());
						this.mMap.setDestinationPoint(null);
						if(this.currentSelectedPoi.isMobile()) {
							mMap.setMobileDestination(currentSelectedPoi);
						}
					}
				}
				recalculateLL.setVisibility(View.GONE);
				poiBar.setVisibility(View.GONE);
				incident.setVisibility(View.VISIBLE);
				//route will be shown when it has been calculated
				//listener will populate the variable
				this.mMap.setRoute(null);

				break;

			case ROUTE_NAVIGATION:
				if (usePOIs) {
					poiInfoLayout.setVisibility(View.VISIBLE);
					quieroIr.setVisibility(View.GONE);
					recalculateLL.setVisibility(View.VISIBLE);
					poiBar.setVisibility(View.GONE);
					routeLL.setVisibility(View.GONE);
					poiUnselect.setText("Exit navigation");
					//if destination is a mobile poi, start following destination
					if(this.currentSelectedPoi != null && this.currentSelectedPoi.isMobile()) {
						startUpdatingMobilePOI();
						mMap.setMobileDestination(currentSelectedPoi);
					}
				}
				incident.setVisibility(View.VISIBLE);
				mMap.setNavigationMode(true);
				break;

			case POINT_CHOSED:
				if (usePOIs) {
					quieroIr.setVisibility(View.GONE);
					routeLL.setVisibility(View.VISIBLE);
					recalculateLL.setVisibility(View.GONE);
					poiBar.setVisibility(View.GONE);
					poiInfoLayout.setVisibility(View.VISIBLE);
					poiUnselect.setVisibility(View.VISIBLE);
					routeTxt.setText("Route");
					poiUnselect.setText("Delete");
					if(this.currentSelectedPoi == null) {
						this.mMap.setDestinationPoint(this.destinationPosition);
					} else {
						this.mMap.setDestinationPoint(null);
					}
				}
				this.mMap.setRoute(null);
				mMap.setMobileDestination(null);
				break;
		}
		this.currentRouteMode = newMode;
		if(mMap!=null){
			mMap.setRefresh(true);
		}
	}

	private int getRouteMode() {
		return this.currentRouteMode;
	}

	/**
	 * -------------- END OF Routes -----------------
	 */
	
	private void showMap(){
			
		mMapPath = null;
		if(!mMapVisible){
			if(useGoogleMaps){
				setContentView(R.layout.google_map);
			}else{
				setContentView(R.layout.indoor_map);
			}
			// Shared UI elements
			radioGroup = (RadioGroup) findViewById(R.id.radio_level);
			slopeView = (TextView) findViewById(R.id.rss_transf_slope);
			intersectionView = (TextView) findViewById(R.id.rss_transf_intersection);
			residualsView = (TextView) findViewById(R.id.rss_transf_residuals);
			rssTransfLayout = (LinearLayout) findViewById(R.id.rss_transf_param);
			tableDetections = (TableLayout) findViewById(R.id.table_detections);
			tableScanset = (TableLayout) findViewById(R.id.table_scanset);
			motionView = (TextView) findViewById(R.id.tv_motion);
			stepsView = (TextView) findViewById(R.id.tv_steps);
			motionModeView = (TextView) findViewById(R.id.tv_motion_mode);
			nonStepsWalkingView = (TextView) findViewById(R.id.tv_non_steps_walking);
			tableOrientation = (TableLayout) findViewById(R.id.table_orientation);
			routeTxt = (TextView) findViewById(R.id.route_txt);

			if(rssTransf){
				rssTransfLayout.setVisibility(View.VISIBLE);
			}
			if(useDetections){
				tableDetections.setVisibility(View.VISIBLE);
			}
			if(showScanset) {
				tableScanset.setVisibility(View.VISIBLE);
			}
			if(demoMode || !useSensors){
				motionView.setVisibility(View.GONE);
			}
			if(demoMode || !showSteps){
				stepsView.setVisibility(View.GONE);
				motionModeView.setVisibility(View.GONE);
				tableOrientation.setVisibility(View.GONE);
				nonStepsWalkingView.setVisibility(View.GONE);
			}
		}
		if(siteId!=-1){
			if(useLocalMaps) {
				mMapBasePath = "";
				mMapsInfo = new ArrayList<MapInfo>();
				mMapsInfo.add(new MapInfo(1, 0, "", String.valueOf(R.drawable.alabama3587)));
			}
			else {
				mMapBasePath = AppData.APP_DATA_FOLDER.toString() + "/" + siteId + "/";
				// get maps info
				mMapsInfo = mLocalDBHelper.getMaps();
			}
			
			if (!mMapsInfo.isEmpty()) {
				String mapFilename = mMapsInfo.get(mMapsInfo.size() - 1).getFilename();
				mMapPath = mMapBasePath + mapFilename;
				if(debug){
					for (MapInfo map : mMapsInfo) {
						Log.i(TAG,"(" + map.getId() + "," + map.getFloor() + "," + map.getUrl() + "," + map.getFilename() + ")");
					}
					Log.i(TAG, "mapPath: " + mMapBasePath + mapFilename);
				}
				
			}
			createRadioGroup(mMapsInfo);
		}
		
		if(useGoogleMaps){
			//Google maps
			if(!mMapVisible){
                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment);
                mapFragment.getMapAsync(this);
			}else {
                insertIndoor();
            }
		}else {
			//Indoor Maps
			if (!mMapVisible) {
				// LQ: incidents
				incident = (Button) findViewById(R.id.incident);
				// LQ: POIs
				poiInfoLayout = (LinearLayout) findViewById(R.id.poi_info);
				quieroIr = (Button) findViewById(R.id.quiero_ir);
				routeLL = (LinearLayout) findViewById(R.id.go_poi);
				recalculateLL = (LinearLayout) findViewById(R.id.recalculate);
				poiUnselect = (Button) findViewById(R.id.poi_unselect);
				poiBar = (LinearLayout) findViewById(R.id.poi_bar);
				if (!usePOIs) {
					poiInfoLayout.setVisibility(View.GONE);
					quieroIr.setVisibility(View.GONE);
					routeLL.setVisibility(View.GONE);
					recalculateLL.setVisibility(View.GONE);
					poiUnselect.setVisibility(View.GONE);
					poiBar.setVisibility(View.GONE);
				}
				// LQ: fences
				fenceSelect = (ImageButton) findViewById(R.id.fence_select);
				fenceUnselect = (Button) findViewById(R.id.fence_unselect);
				fenceInfoLayout = (LinearLayout) findViewById(R.id.fence_info);
				fenceDetails = (LinearLayout) findViewById(R.id.fence_details);
				fenceBar = (LinearLayout) findViewById(R.id.fence_bar);
				nameView = (TextView) findViewById(R.id.txtName);
				distView = (TextView) findViewById(R.id.txtDist);
				angleView = (TextView) findViewById(R.id.txtAngle);
				posView = (TextView) findViewById(R.id.txtPos);
				mFlMapWrapper = (FrameLayout) findViewById(R.id.online_map_wrapper);
				if (mMapPath != null) {
					if (useLocalMaps) {
						mBmpMap = BitmapFactory.decodeResource(getResources(), Integer.parseInt(mMapPath));
					} else {
						mBmpMap = BitmapFactory.decodeFile(mMapPath); //load map
					}
				}
				mMap = new Map(this, MapType.MAP_NAVIGATION, mBmpMap, mCurrentFloor, siteId, showSubset, showPaths);
				mFlMapWrapper.addView(mMap);

				if (testRoute) {
					generateTestingRoute(idRoute, siteId);    // pass the route id as the parameter
				}

				if (mMap != null) {
					mMap.setShowKnnNeighbors(showCandidates);
					mMap.setShowOrientation(showOrientation);
					mMap.setRefresh(true);
					mMapVisible = true;
				}
			} else {
				if (mMapPath != null) {
					mBmpMap = BitmapFactory.decodeFile(mMapPath); //load map
				}
				mMap.changeFloorMap(mBmpMap);
			}
		}
		if(useFences) {
			registerPointselectedListener();
			if (geoFences != null) {
				fenceSelect.setVisibility(View.VISIBLE);
				drawGeoFences(geoFences);
			}
		}
		if (usePOIs) {
			registerPOIselectedListener();
		}
		siteInfoPending = false;		
	}

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMedia = new Media(this);
        mGoogleMap = new MyGoogleMap(googleMap, 18, firstPosition, mMedia);
        insertIndoor();
    }

    private void insertIndoor(){
        if(siteId!=-1){
            mGoogleMap.insertIndoorMap(mMapPath, mapAnchorLat, mapAnchorLng, mapWidth, mapHeight, mapBearing);
            if(showPaths){
                ArrayList<Edge> mEdges = getNodesAndEdges(mCurrentFloor);
                mGoogleMap.drawEdges(mEdges);
            }
        }
        mMapVisible = true;
    }
    private void obtainSiteVersions() {
		WebServiceAT wsAT = new WebServiceAT(this,
				new WebServiceOnCompleteListener(), getResources().getString(
						R.string.ws_URL_BASE)
						+ getResources().getString(R.string.ws_versions),
				WebService.RequestMethod.POST);
		try {
			JSONObject json = new JSONObject();
			json.put("site_id", siteId);
			wsAT.addHeader("TOKEN", "ascamm");
//			LQ: Commented message Obtaining site data
			wsAT.AddDialog();
			wsAT.setDialogMessage("Obtaining Site Data...");
			wsAT.AddName("versions");
			wsAT.addJSON(json);
			wsAT.execute();
		} catch (Exception e) {
			Log.e("WebService AT", "Error!", e);
		}
	}
	
	private VersionInfo obtainLocalVersions() {
		return mLocalDBHelper.getVersions();
	}

	private void compareVersions() {
		mapsNeedUpdate = true;
		mapsOk = false;
		VersionInfo vInfo = obtainLocalVersions();
		if (vInfo == null) {
			// It is the first time that we use the site. Download all the data
			hasLocalVersionData = false;
		} else {
			if(debug){
				Log.i(TAG,"Local Versions: " + vInfo.getAps() + " , " + vInfo.getMaps() + " , " + vInfo.getNodes_edges());
				Log.i(TAG, "Remote Versions: " + apsRemoteVersion + " , " + mapsRemoteVersion + " , " + nodesEdgesRemoteVersion);
			}
			if (mapsRemoteVersion == vInfo.getMaps()) {
				mapsOk = true;
				mapsNeedUpdate = false;
			}
		}
		if (mapsOk) {
			showMap();
		} else {
			downloadData();
		}
	}
	
	private void downloadData() {
		if (mapsNeedUpdate) {
			WebServiceAT wsAT = new WebServiceAT(this,
					new WebServiceOnCompleteListener(), getResources()
							.getString(R.string.ws_URL_BASE)
							+ getResources().getString(R.string.ws_maps),
					WebService.RequestMethod.POST);
			try {
				JSONObject json = new JSONObject();
				json.put("site_id", siteId);
				wsAT.addHeader("TOKEN", "ascamm");
//				LQ: Commented message obtaning maps data
				wsAT.AddDialog();
				wsAT.setDialogMessage("Obtaining Maps Data...");
				wsAT.AddName("maps");
				wsAT.addJSON(json);
				wsAT.execute();
			} catch (Exception e) {
				Log.e("WebService AT", "Error!", e);
			}
		}
	}
	
	private void downloadMapImage(int id, String url) {
		DownloadImageAT imageAT = new DownloadImageAT(this,
				new DownloadImageOnCompleteListener());
		try {
			imageAT.setId(id);
			imageAT.AddDialog();
			imageAT.setDialogMessage("Downloading maps...");
			imageAT.setSiteId(siteId);
			imageAT.execute(url);
		} catch (Exception e) {
			Log.e("DownloadImage AT", "Error!", e);
		}
	}
	
	private void saveMapsInfo(ArrayList<MapInfo> mapsList) {
		// Delete old Maps data
		int numMapsDeleted = mLocalDBHelper.deleteMapsAll();
		// Insert new Maps data
		boolean mapsInsertedOk = mLocalDBHelper.insertMaps(mapsList);
		if(debug){
			Log.i(TAG, "numMapsDeleted: " + numMapsDeleted);
			Log.i(TAG, "mapsInsertedOk: " + mapsInsertedOk);
		}
	}
	
	private void checkDownloadFinished() {
		boolean mapsImages = true;
		if(debug)	Log.i(TAG, "checkDownloadFinished. mapsOk: " + mapsOk);
		for (boolean map : mapsImagesDownload) {
			if(debug)	Log.i(TAG, "" + map);
			if (!map) {
				mapsImages = false;
			}
		}
		
		if (mapsOk && mapsImages) {
			if(debug){
				if(mapsList!=null){
					for (MapInfo map : mapsList) {
						Log.i(TAG, "(" + map.getId() + " , " + map.getFloor() + " , " + map.getUrl() + " , " + map.getFilename() + ")");
					}
				}
			}
			if(mapsDownloadOk){
				// Update DB with new data and versions
				SaveDataAT sdAT = new SaveDataAT(this);
				sdAT.AddDialog();
				sdAT.execute();
			}
			else{
//				LQ: Commented message site data could not be downloaded try again later
				showDialogMessage("Site Data could not be downloaded. Please try again later");
			}
		}
	}
	
	private void setTextView(String text){
    	setContentView(R.layout.textmessage);
		TextView txtView = (TextView) findViewById(R.id.TextMessage);
		txtView.setText(text);
    }
	
	private void showDialogMessage(String msgTxt) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(msgTxt);
		builder.setPositiveButton("Close",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// YES button clicked
						dialog.dismiss();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	public class WebServiceOnCompleteListener implements WebServiceCompleteListener<String> {

		private String ws_res = null;
		
		@Override
		public void onTaskComplete(WebServiceResult result) {
			// do something with the response of the Web Service
			ws_res = result.getResponse();
			try {
				int statusCode = result.getStatus();
				if (statusCode == 0 && ws_res == null) {
					VersionInfo vInfo = obtainLocalVersions();
					if (vInfo == null) {
						// It is the first time that we use the site. Download all the data
						// LQ: commented message No connection available
						setTextView(getResources().getString(R.string.no_connection_title) + "\n" + getResources().getString(R.string.no_connection_message));
					} else {
						// It has previous versions. Continue with last local version
						if(debug){
							Log.i(TAG,"Local Versions: " + vInfo.getAps() + " , " + vInfo.getMaps() + " , " + vInfo.getNodes_edges());
						}
//						LQ: Commented message site data could not be obtained working offline
						String txtMsg = "Site data could not be obtained. Working in offline mode";
						showDialogMessage(txtMsg);
						//Toast.makeText(LocationIndoor.this, txtMsg, Toast.LENGTH_LONG).show();
						showMap();
					}
				} else if (statusCode == 200) {
					if (result.getName().equalsIgnoreCase("versions")) {
						JSONObject jsonObject = new JSONObject(ws_res);
						apsRemoteVersion = jsonObject.getInt("access_point_version");
						mapsRemoteVersion = jsonObject.getInt("site_map_version");
						nodesEdgesRemoteVersion = jsonObject
								.getInt("node_edge_version");
						// Compare local versions with remote versions
						compareVersions();
					} else if (result.getName().equalsIgnoreCase("maps")) {
						// Obtain Maps Data
						mapsList = new ArrayList<MapInfo>();
						JSONArray maps = new JSONArray(ws_res);
						for (int i = 0; i < maps.length(); i++) {
							JSONObject map = maps.getJSONObject(i);
							int floor = Integer.valueOf(map.getString("floor"));
							String path = new String(map.getString("filename"));
							mapsList.add(new MapInfo(i, floor, path, null));
						}
						if (mapsList.isEmpty()) {
							mapsImagesDownload.clear();
							mapsDownloadOk = false;
							Toast.makeText(LocationIndoor.this, "0 Maps received", Toast.LENGTH_SHORT).show();
						} else {
							if(debug)	Log.i(TAG, mapsList.size() + " Maps received");
							for (MapInfo map : mapsList) {
								if(debug)	Log.i(TAG,"(" + map.getId() + " , " + map.getFloor() + " , " + map.getUrl() + ")");
								mapsImagesDownload.add(false);
							}
							mapsDownloadOk = true;
							for (MapInfo map : mapsList) {
								downloadMapImage(map.getId(), map.getUrl());
							}
						}
						mapsOk = true;
						checkDownloadFinished();
					} else {
						Log.e(TAG, "Unknown WS name");
					}
				} else {
					setTextView("Error. Status: " + statusCode);
				}
			} catch (Exception e) {
				Log.e("WebService AT", "Parsing JSON Error!", e);
			}
		}
	}
	
	public class DownloadImageOnCompleteListener implements DownloadImageCompleteListener<String> {

		@Override
		public void onTaskComplete(DownloadImageResult result) {
			for (int i = 0; i < mapsImagesDownload.size(); i++) {
				if (result.getId() == i) {
					String filename = result.getFilename();
					if (filename == null) {
						mapsDownloadOk = false;
						String msgTxt = "Map " + result.getId() + " could not be downloaded";
						Log.d(TAG,msgTxt);
						Toast.makeText(LocationIndoor.this, msgTxt, Toast.LENGTH_LONG).show();
					}
					// update boolean's array value
					mapsList.get(i).setFilename(filename);
					mapsImagesDownload.set(i, true);
					checkDownloadFinished();
				}
			}
		}
	}

	// LQ: class SaveDataAT for saving downloaded PDB to device
	
	private class SaveDataAT extends AsyncTask<Void, Void, Integer> {

		private ProgressDialog dialog;
		private Context context;
		private Boolean use_dialog;
		private String message;

		public SaveDataAT(Context context) {
			this.context = context;
			this.use_dialog = false;
		}

		public void AddDialog() {
			use_dialog = true;
		}

		public void setDialogMessage(String message) {
			this.message = message;
		}

		@Override
		protected void onPreExecute() {
			if (use_dialog) {
				super.onPreExecute();
				dialog = new ProgressDialog(context);
				if (message != null) {
					dialog.setMessage(message);
				} else {
					dialog.setMessage("Saving Data...");
				}
				// dialog.setTitle("Title");
				dialog.setIndeterminate(true);
				dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				dialog.setCancelable(true);
				dialog.show();
			}
		}

		@Override
		protected Integer doInBackground(Void... values) {

			int problemsSaving = 0;
			
			if (mapsNeedUpdate) {
				// Save maps info
				saveMapsInfo(mapsList);
			}
			if (hasLocalVersionData) {
				// update version data in versions table
				int updateOk = mLocalDBHelper.updateVersions(apsRemoteVersion, mapsRemoteVersion, nodesEdgesRemoteVersion);
				if (updateOk > 0) {
					if(debug)	Log.i(TAG, "Versions info updated");
				} else {
					if(debug)	Log.i(TAG, "Problems updating versions info");
					problemsSaving = 1;
				}
			} else {
				// create a new register in versions table
				long id = mLocalDBHelper.insertVersions(apsRemoteVersion, mapsRemoteVersion, nodesEdgesRemoteVersion);
				if (id != -1) {
					if(debug)	Log.i(TAG, "Versions info created");
				} else {
					if(debug)	Log.i(TAG, "Problems creating versions info");
					problemsSaving = 2;
				}
			}

			return problemsSaving;
		}

		@Override
		protected void onPostExecute(Integer problemsSaving) {
			if (problemsSaving == 2) {
				setContentView(R.layout.textmessage);
				TextView txtView = (TextView) findViewById(R.id.TextMessage);
				txtView.setText("Error. Version data could not be created. Please try again");
			} else if (problemsSaving == 1) {
//				LQ: Commented message site data could not be updated working offline
				String txtMsg = "Site data could not be updated. Working in offline mode";
				showDialogMessage(txtMsg);
				//Toast.makeText(context, txtMsg, Toast.LENGTH_LONG).show();
				showMap();
			} else {
				showMap();
			}
			if (use_dialog) {
				if (dialog.isShowing()) {
					dialog.dismiss();
				}
			}
		}

	}

	// LQ: INCIDENTS from Renault version

	private class IncidentWebserviceOnComplete implements WebServiceCompleteListener<String> {

		@Override
		public void onTaskComplete(WebServiceResult result) {
			// TODO Auto-generated method stub
		}

	}

	private void sendIncidentWebservice(String description) {
		WebServiceAT wsAT = new WebServiceAT(this, new IncidentWebserviceOnComplete(), getResources().getString(R.string.ws_openrtls_url_base) + getResources().getString(R.string.ws_report_incident), WebService.RequestMethod.POST);
		wsAT.setDialogMessage("Sending incident...");
		JSONObject data = new JSONObject();
		try {
			data.put("mac", deviceId);
			data.put("timestamp", (int) (System.currentTimeMillis() / 1000L));
			data.put("lon", currentPositionGlobal.lng);
			data.put("lat", currentPositionGlobal.lat);
			data.put("alt", (int) currentPositionGlobal.z);
			data.put("description", description);
			data.put("site", siteId);
			wsAT.addJSON(data);
			wsAT.execute();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	//send an incident to viso
	public void newIncident(View view) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("New Incident");
		builder.setMessage("Description");

		// Set up the input
		final EditText input = new EditText(this);
		// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		builder.setView(input);

		// Set up the buttons
		builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String description = input.getText().toString();
				dialog.dismiss();

				sendIncidentWebservice(description);
			}
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();


			}
		});

		builder.show();

	}
	
}
