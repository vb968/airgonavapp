package com.ascamm.ascammnav.ws;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.ascamm.ascammnav.utils.AppData;

public class DownloadImageAT extends AsyncTask<String, Void, DownloadImageResult>{

	private ProgressDialog dialog;
	private Context context;
	private DownloadImageCompleteListener<String> listener;
	private DownloadImageResult res;
	private int id, idSite;
	private boolean use_dialog;
	private String message;
			
	public DownloadImageAT(Context context, DownloadImageCompleteListener<String> listener){
		this.context = context;
		this.listener = listener;
	}
	
	public void AddDialog(){
		use_dialog = true;
    }
	
	public void setDialogMessage(String message){
		this.message = message; 
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public void setSiteId(int id){
		this.idSite = id;
	}
	
	@Override
    protected void onPreExecute() {
		super.onPreExecute();
		if(use_dialog){
			super.onPreExecute();
	        dialog = new ProgressDialog(context);
			if(message!=null){
				dialog.setMessage(message);
			}
			else{
				dialog.setMessage("Downloading image...");
			}
	        //dialog.setTitle("Title");
			dialog.setIndeterminate(true);
	        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	        dialog.setCancelable(true);
	    	dialog.show();
		}
    }
		
	@Override
    protected DownloadImageResult doInBackground(String... urls)
    {
        //Log.d("DEBUG", "drawable");
		String filename = null;
        InputStream is = downloadImage(urls[0]);
        if(is != null){
	        String ext = urls[0].substring((urls[0].lastIndexOf(".")), urls[0].length());
	        //System.out.println("ext: " + ext);
	        checkFolderStructure();
	        filename = "map_" + id + ext;
	        boolean savedOk = saveImage(filename, is);
	        if(!savedOk){
	        	filename = null;
	        }
        }
        
        res = new DownloadImageResult(id, filename);
        return res;
    }
	
	@Override
    protected void onPostExecute(DownloadImageResult result) {
		listener.onTaskComplete(result);
		if(use_dialog){
			if (dialog.isShowing()) {
	            dialog.dismiss();
	        }
		}
    }
	
	private InputStream downloadImage(String imageUrl)
    {
        try
        {
            URL url = new URL(imageUrl);
            //InputStream is = (InputStream)url.getContent();
			InputStream is = url.openStream();
            /*
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);                   
            urlConnection.connect();
            InputStream is = urlConnection.getInputStream();
            */
            return is;
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
            return null;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }
	
	private void checkFolderStructure(){
		//create folders if it is the first time
		File appFolder = AppData.APP_DATA_FOLDER;
		boolean appFolderExists = appFolder.exists();
	    if (!appFolderExists) {
	    	appFolderExists = appFolder.mkdir();
		}
		// create .nomedia file to prevent files to appear when gallery loads
		if(appFolderExists){
			File noMediaFile = new File(appFolder, ".nomedia");
			if(!noMediaFile.exists()){
				try {
					noMediaFile.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private boolean saveImage(String filename, InputStream is) {
		boolean savedOk = false;
		try {
			//Log.i("Local filename:", "" + filename);
			File siteDirectory = new File(AppData.APP_DATA_FOLDER, String.valueOf(idSite));
			boolean folderExists = siteDirectory.exists();
		    if (!folderExists) {
		    	folderExists = siteDirectory.mkdir();
			}
		    File siteFile = new File(siteDirectory.toString(), filename);
			FileOutputStream fileOutput = new FileOutputStream(siteFile);
			byte[] buffer = new byte[1024*4];
			int bufferLength = 0;
			while ( (bufferLength = is.read(buffer)) > 0 ) 
			{                 
				fileOutput.write(buffer, 0, bufferLength);                  
			}             
			fileOutput.close();
			savedOk = true;
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return savedOk;
	}
    
}