package com.ascamm.ascammnav.ws;


public class DownloadImageResult{
	private int id;
	private String filename;
	
	public DownloadImageResult(int id, String filename){
		this.id = id;
		this.filename = filename;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public void setFilename(String filename){
		this.filename = filename;
	}
	
	public int getId(){
		return id;
	}
	
	public String getFilename(){
		return filename;
	}
}