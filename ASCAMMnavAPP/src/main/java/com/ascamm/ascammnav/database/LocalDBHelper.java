package com.ascamm.ascammnav.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ascamm.ascammnav.utils.MapInfo;
import com.ascamm.ascammnav.utils.VersionInfo;
import com.ascamm.motion.utils.Edge;
import com.ascamm.motion.utils.OnlineItem;
import com.ascamm.utils.Point3D;

/**
 * Database helper class, defines the SQLite database, tables, columns, and all the methods provided publicly.
 * @author Dani Fernandez [CTAE]
 */
public class LocalDBHelper extends SQLiteOpenHelper {
	
	private String TAG = getClass().getSimpleName();
	private int debug = 2;
	
	/* define Database, tables, and columns */
	public static final String 	DB_NAME 		=	"AscammNavDB";
	public static final int 	DB_VERSION			=	1;
	public static final String 	TB_APS		 	= 	"aps";
	public static final String 	TB_NODES	 	= 	"nodes";
	public static final String 	TB_EDGES	 	= 	"edges";
	public static final String 	TB_MAPS		 	= 	"maps";
	public static final String 	TB_VERSIONS	 	= 	"versions";
	public static final String 	COL_ID 			= 	"id" ; 
	public static final String 	COL_ID_SITE		= 	"idSite" ;
	public static final String 	COL_ID_POINT	= 	"idPoint" ;
	public static final String 	COL_ID_NODE		= 	"idNode" ;
	public static final String 	COL_ID_EDGE 	= 	"idEdge" ;
	public static final String 	COL_ID_CALIB_EDGE 	= 	"idCalibEdge" ;
	public static final String 	COL_ID_START_NODE	= 	"StartNodeId" ;
	public static final String 	COL_ID_END_NODE	= 	"EndNodeId" ;
	public static final String 	COL_SSID 		= 	"ssid" ; 
	public static final String 	COL_BSSID 		= 	"bssid" ; 
	public static final String 	COL_POSX	 	= 	"position_x" ;
	public static final String 	COL_POSY 		= 	"position_y" ;
	public static final String 	COL_POSZ 		= 	"position_z" ;
	public static final String 	COL_TECH		= 	"tech" ;
	public static final String 	COL_STATUS		= 	"status" ;
	public static final String 	COL_RSS 		= 	"rss" ;				/* Received Signal Strength */
	public static final String 	COL_LENGTH		= 	"length" ;
	public static final String 	COL_FLOOR		= 	"floor" ;
	public static final String 	COL_URL			= 	"url" ;
	public static final String 	COL_FILENAME	= 	"filename" ;
	public static final String 	COL_APS			= 	"aps" ;
	public static final String 	COL_MAPS		= 	"maps" ;
	public static final String 	COL_NODES_EDGES	= 	"nodes_edges" ;

	private static final String SQLITE_FILE_NAME = "ascamm_calib.sqlite";
	
	private int idSite = 0;

	public LocalDBHelper(Context context, int idSite) {
		super(context, DB_NAME, null, DB_VERSION);
		this.idSite = idSite;
	}

	/**
	 * Create one SQLite database named AscammNavDB. This database is divided in multiple tables: 
	 *  <ul>
	 *  <li>TB_APS: Contains information related to APs of the site</li>
	 *  <li>TB_NODES: Contains information of the path to calibrate</li>
	 *  <li>TB_EDGES: Contains information of the path to calibrate</li>
	 *  <li>TB_MAPS: Contains information related to the maps of the site</li>
	 *  <li>TB_VERSIONS: Contains information of the versions of the site (aps, maps and nodes_edges)</li>
	 *  <li>TB_CALIB_POINTS: Contains calibration information (points calibrated)</li>
	 *  <li>TB_CALIB_MEAS: Contains calibration information (RSS measurements)</li>
	 *  <li>TB_CALIB_NODES: Contains calibration information (calibrated nodes)</li>
	 *  <li>TB_CALIB_EDGES: Contains calibration information (calibrated edges)</li>
	 *  </ul>
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			
			db.execSQL("CREATE TABLE " + TB_APS + " (" 
					+ COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
					+ COL_SSID + " TEXT, " 
					+ COL_BSSID + " TEXT, " 
					+ COL_POSX + " FLOAT, " 
					+ COL_POSY + " FLOAT, " 
					+ COL_POSZ + " FLOAT, "
					+ COL_ID_SITE + " INTEGER "
					+ ");");
			
			db.execSQL("CREATE TABLE " + TB_NODES + " (" 
					+ COL_ID_NODE + " INTEGER PRIMARY KEY, " 
					+ COL_POSX + " FLOAT, " 
					+ COL_POSY + " FLOAT, " 
					+ COL_POSZ + " FLOAT, "
					+ COL_ID_SITE + " INTEGER "
					+ ");");
			
			db.execSQL("CREATE TABLE " + TB_EDGES + " (" 
					+ COL_ID_EDGE + " INTEGER PRIMARY KEY, " 
					+ COL_ID_START_NODE + " INTEGER, " 
					+ COL_ID_END_NODE + " INTEGER, " 
					+ COL_LENGTH + " FLOAT "
					+ ");");
			
			db.execSQL("CREATE TABLE " + TB_MAPS + " (" 
					+ COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
					+ COL_FLOOR + " TEXT, " 
					+ COL_URL + " TEXT, " 
					+ COL_FILENAME + " TEXT, " 
					+ COL_ID_SITE + " INTEGER "
					+ ");");
			
			db.execSQL("CREATE TABLE " + TB_VERSIONS + " (" 
					+ COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
					+ COL_APS + " INTEGER, " 
					+ COL_MAPS + " INTEGER, " 
					+ COL_NODES_EDGES + " INTEGER, " 
					+ COL_ID_SITE + " INTEGER "
					+ ");");

		} catch ( SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		/* 
		   Called when the database needs to be upgraded. The implementation 
		   should use this method to drop tables, add tables, or do anything 
		   else it needs to upgrade to the new schema version. 
		 */
	}

	public long insertAP_oldVersion(String ssid, String bssid, Point3D p){
		SQLiteDatabase db = getWritableDatabase();
		ContentValues cvs = new ContentValues();
		cvs.put(COL_SSID, ssid);
		cvs.put(COL_BSSID, bssid);
		cvs.put(COL_POSX, p.x);
		cvs.put(COL_POSY, p.y);
		cvs.put(COL_POSZ, p.z);
		cvs.put(COL_ID_SITE, idSite);
		long newRecordID = db.insert(TB_APS, null, cvs);
		db.close();
		return newRecordID;
	}
	
	public int updateAP(String oldBSSID, String newSSID, String newBSSID, Point3D p){
		SQLiteDatabase db = getWritableDatabase();
		ContentValues cvs = new ContentValues();
		cvs.put(COL_SSID, newSSID);
		cvs.put(COL_BSSID, newBSSID);
		cvs.put(COL_POSX, p.x);
		cvs.put(COL_POSY, p.y);
		cvs.put(COL_POSZ, p.z);
		int updatedRows = db.update(TB_APS, cvs, COL_BSSID+"='"+oldBSSID+"' AND "+ COL_ID_SITE+"="+idSite, null);

		db.close();
		return updatedRows;
	}
	
	/**
	 *  Insert a new access point to the database AP table
	 * @param ap - APWithPosition
	 * @return newRecordID
	 */
	public long insertAP(SQLiteDatabase db, APWithPosition ap){
		ContentValues cvs = new ContentValues();
		cvs.put(COL_SSID, ap.getSSID());
		cvs.put(COL_BSSID, ap.getBSSID());
		cvs.put(COL_POSX, ap.getPositionX());
		cvs.put(COL_POSY, ap.getPositionY());
		cvs.put(COL_POSZ, ap.getPositionZ());
		cvs.put(COL_ID_SITE, idSite);
		long newRecordID = db.insert(TB_APS, null, cvs);
		return newRecordID;
	}
	
	public boolean insertAPs(ArrayList<APWithPosition> aps){
		boolean insertOk = true;
		SQLiteDatabase db = getWritableDatabase();
		db.beginTransaction();	// Begin Transaction 
		for(APWithPosition ap : aps){
			long id = insertAP(db,ap);
			if(id==-1){
				insertOk = false;
				break;
			}
		}
		if(insertOk){
			db.setTransactionSuccessful();
		}
		db.endTransaction();	// End Transaction
		db.close();
		return insertOk;
	}

	/**
	 *  Insert a new node to the database TB_NODES table
	 * @param db - SQLiteDatabase writable database
	 * @param node - Point3D representing the node
	 * @return newRecordID - The ID of the newly created node or -1 if an error occurred
	 */
	public long insertNode(SQLiteDatabase db, Point3D node){
		ContentValues cvs = new ContentValues();
		cvs.put(COL_ID_NODE, node.getId());
		cvs.put(COL_POSX, node.x);
		cvs.put(COL_POSY, node.y);
		cvs.put(COL_POSZ, node.z);
		cvs.put(COL_ID_SITE, idSite);
		long newRecordID = db.insert(TB_NODES, null, cvs);
		return newRecordID;
	}
	
	public boolean insertNodes(ArrayList<Point3D> nodes){
		boolean insertOk = true;
		SQLiteDatabase db = getWritableDatabase();
		db.beginTransaction();	// Begin Transaction 
		for(Point3D n : nodes){
			long id = insertNode(db,n);
			if(id==-1){
				insertOk = false;
				break;
			}
		}
		if(insertOk){
			db.setTransactionSuccessful();
		}
		db.endTransaction();	// End Transaction
		db.close();
		return insertOk;
	}
	
	/**
	 *  Insert a new edge to the database TB_EDGES table
	 * @param db - SQLiteDatabase writable database
	 * @param idStart - the ID of the start node
	 * @param idEnd - the ID of the end node
	 * @return newRecordID - The ID of the newly created edge or -1 if an error occurred
	 */
	public long insertEdge(SQLiteDatabase db, int edgeId, long idStart, long idEnd){
		ContentValues cvs = new ContentValues();
		cvs.put(COL_ID_EDGE, edgeId);
		cvs.put(COL_ID_START_NODE, idStart);
		cvs.put(COL_ID_END_NODE, idEnd);
		long newRecordID = db.insert(TB_EDGES, null, cvs);
		return newRecordID;
	}
	
	public boolean insertEdges(ArrayList<Edge> edges){
		boolean insertOk = true;
		SQLiteDatabase db = getWritableDatabase();
		db.beginTransaction();	// Begin Transaction 
		for(Edge e : edges){
			long id = insertEdge(db,e.edgeId, e.startNodeId, e.endNodeId);
			if(id==-1){
				insertOk = false;
				break;
			}
		}
		if(insertOk){
			db.setTransactionSuccessful();
		}
		db.endTransaction();	// End Transaction
		db.close();
		return insertOk;
	}
	
	/**
	 *  Insert a new MapInfo to the database TB_MAPS table
	 * @param db - SQLiteDatabase writable database
	 * @param map - MapInfo with the info related to a map
	 * @return newRecordID - The ID of the newly created map record or -1 if an error occurred
	 */
	public long insertMap(SQLiteDatabase db, MapInfo map){
		ContentValues cvs = new ContentValues();
		cvs.put(COL_FLOOR, map.getFloor());
		cvs.put(COL_URL, map.getUrl());
		cvs.put(COL_FILENAME, map.getFilename());
		cvs.put(COL_ID_SITE, idSite);
		long newRecordID = db.insert(TB_MAPS, null, cvs);
		return newRecordID;
	}
	
	public boolean insertMaps(ArrayList<MapInfo> maps){
		boolean insertOk = true;
		SQLiteDatabase db = getWritableDatabase();
		db.beginTransaction();	// Begin Transaction 
		for(MapInfo m : maps){
			long id = insertMap(db,m);
			if(id==-1){
				insertOk = false;
				break;
			}
		}
		if(insertOk){
			db.setTransactionSuccessful();
		}
		db.endTransaction();	// End Transaction
		db.close();
		return insertOk;
	}
	
	/**
	 *  Insert a new record to the database TB_VERSIONS table
	 * @param aps - Version of the APs of the site
	 * @param maps - Version of the Maps of the site
	 * @param nodes_edges - Version of the Nodes&Edges of the site
	 * @return newRecordID - The ID of the newly created versions record or -1 if an error occurred
	 */
	public long insertVersions(int aps, int maps, int nodes_edges){
		SQLiteDatabase db = getWritableDatabase();
		ContentValues cvs = new ContentValues();
		cvs.put(COL_APS, aps);
		cvs.put(COL_MAPS, maps);
		cvs.put(COL_NODES_EDGES, nodes_edges);
		cvs.put(COL_ID_SITE, idSite);
		long newRecordID = db.insert(TB_VERSIONS, null, cvs);
		db.close();
		return newRecordID;
	}
	
	public int updateVersions(int aps, int maps, int nodes_edges){
		SQLiteDatabase db = getWritableDatabase();
		ContentValues cvs = new ContentValues();
		cvs.put(COL_APS, aps);
		cvs.put(COL_MAPS, maps);
		cvs.put(COL_NODES_EDGES, nodes_edges);
		int updatedRows = db.update(TB_VERSIONS, cvs, COL_ID_SITE+"="+idSite, null);
		db.close();
		return updatedRows;
	}
	
	/**
	 * Delete an access point from database AP table
	 * @param bssid - MAC address of the AP
	 * @return deleteRows - number of rows in table been deleted
	 */
	public int deleteAP(String bssid){
		SQLiteDatabase db = getWritableDatabase();
		int deleteRows = db.delete(TB_APS, 
				COL_BSSID+"=\""+bssid+"\"", 
				null);
		db.close();
		return deleteRows;
	}

	/**
	 * Delete all access points from the site
	 * @return deleteRows - number of rows in table been deleted
	 */
	public int deleteAPAll(){
		SQLiteDatabase db = getWritableDatabase();
		int deleteRows = db.delete(TB_APS, COL_ID_SITE+"="+idSite, null);
		db.close();
		return deleteRows;
	}
	
	public int deleteEdgesAll(SQLiteDatabase db,ArrayList<Integer> edgesId){
		int deleteRows = 0;
		int numEdges = edgesId.size();
		if(numEdges>0){
			String edges = "(";
			for(int i=0; i<numEdges; i++){
				edges += edgesId.get(i);
				if(i!=numEdges-1){
					edges += ",";
				}
			}
			edges += ")";
			//deleteRows = db.delete(TB_EDGES, COL_ID_SITE+"="+idSite+" AND "+COL_ID_EDGE+" IN "+edges, null);
			deleteRows = db.delete(TB_EDGES, COL_ID_EDGE+" IN "+edges, null);
		}
		return deleteRows;
	}
	
	public int deleteNodesAll(SQLiteDatabase db){
		int deleteRows = db.delete(TB_NODES, COL_ID_SITE+"="+idSite, null);
		return deleteRows;
	}
	
	public int deleteNodesAndEdgesAll(){
		ArrayList<Integer> edgesId = getEdgesIdAll();
		SQLiteDatabase db = getWritableDatabase();
		db.beginTransaction();
		int numNodes = 0;
		int numEdges = deleteEdgesAll(db,edgesId);
		if(numEdges>0){
			numNodes = deleteNodesAll(db);
			if(numNodes>0){
				db.setTransactionSuccessful();
			}
		}
		db.endTransaction();
		db.close();
		return numNodes;
	}
	
	public int deleteMapsAll(){
		SQLiteDatabase db = getWritableDatabase();
		int deleteRows = db.delete(TB_MAPS, COL_ID_SITE+"="+idSite, null);
		db.close();
		return deleteRows;
	}
	
	// delete versions???

	
	
	
	
	
	
	/**
	 * Export the SQLite database to SD card as a ".sqlite" file
	 */
	public boolean exportDB(String targetDir){
		SQLiteDatabase db = getReadableDatabase();
		String targetPath = targetDir + "/" + SQLITE_FILE_NAME;

		File targetFolder = new File(targetDir);
		if (!targetFolder.exists()) {
			targetFolder.mkdirs();
		}
		try{
			File sourceFile = new File(db.getPath());
			FileInputStream inStream = new FileInputStream(sourceFile);

			File targetFile = new File(targetPath);
			FileOutputStream outStream = new FileOutputStream(targetFile);

			int c;
			while( (c = inStream.read()) != -1 ){
				outStream.write(c);
			}
			inStream.close();
			outStream.close();

		} catch (Exception e) {
			Log.e("DBHelper", "Can not export database to file");
			return false;
		}

		db.close();
		return true;
	}

	/**
	 * Import the ".sqlite" file to SQLite database
	 */
	public boolean importDB(String sourceDirPath){
		String sourceFilePath = sourceDirPath + "/" + SQLITE_FILE_NAME;
		SQLiteDatabase db = getReadableDatabase();

		try{
			File sourceFile = new File(sourceFilePath);
			FileInputStream inStream = new FileInputStream(sourceFile);

			File targetFile = new File(db.getPath());
			FileOutputStream outStream = new FileOutputStream(targetFile);

			int c;
			while( (c = inStream.read()) != -1 ){
				outStream.write(c);
			}
			inStream.close();
			outStream.close();

		} catch (Exception e) {
			Log.e("DBHelper", "Can not import database file");
			return false;
		}

		db.close();
		return true;
	}

	public ArrayList<APWithPosition> getAllAPs(){
		ArrayList<APWithPosition> aps = new ArrayList<APWithPosition>();
		SQLiteDatabase db = getReadableDatabase();

		String sql = "SELECT * FROM "+TB_APS + " WHERE " + COL_ID_SITE + "=" + idSite;
		Cursor cur = db.rawQuery(sql, null);

		while (cur.moveToNext()){
			APWithPosition apWithPosition = new APWithPosition(
					cur.getString(1),		// bsid
					cur.getString(2),		// bssid
					cur.getInt(3),		// position x
					cur.getInt(4),		// position y
					cur.getInt(5) 		// position z
			);
			aps.add(apWithPosition);
		}

		db.close();
		return aps;
	}

	public ArrayList<String> getAllAPsMac(){
		ArrayList<String> aps = new ArrayList<String>();
		SQLiteDatabase db = getReadableDatabase();

		String sql = "SELECT * FROM "+TB_APS + " WHERE " + COL_ID_SITE + "=" + idSite;;
		Cursor cur = db.rawQuery(sql, null);

		while (cur.moveToNext()){
			aps.add(cur.getString(2).toLowerCase());		// mac address
		}

		db.close();
		return aps;
	}
	
	
	public ArrayList<Integer> getEdgesIdAll(){
		ArrayList<Integer> edges = new ArrayList<Integer>();
		SQLiteDatabase db = getReadableDatabase();

		String sql = "SELECT "+COL_ID_EDGE+" FROM " + TB_EDGES + " as e " +
				"JOIN " + TB_NODES + " as n1 ON n1." + COL_ID_NODE + "=e." + COL_ID_START_NODE + 
				" JOIN " + TB_NODES + " as n2 ON n2." + COL_ID_NODE + "=e." + COL_ID_END_NODE +  
				" WHERE n1."+COL_ID_SITE+"="+idSite;
		Cursor cur = db.rawQuery(sql, null);

		while (cur.moveToNext()){
			edges.add(cur.getInt(0));
		}

		db.close();
		return edges;
	}
	
	public ArrayList<MapInfo> getMaps(){
		ArrayList<MapInfo> maps = new ArrayList<MapInfo>();
		SQLiteDatabase db = getReadableDatabase();

		String sql = "SELECT * FROM "+TB_MAPS + " WHERE " + COL_ID_SITE + "=" + idSite;
		Cursor cur = db.rawQuery(sql, null);

		while (cur.moveToNext()){
			MapInfo map = new MapInfo(
					cur.getInt(0),		// id
					cur.getInt(1),		// floor
					cur.getString(2),	// url
					cur.getString(3)	// filename
			);
			maps.add(map);
		}

		db.close();
		// sort maps by floor
		Collections.sort(maps, new SortByFloor());
		return maps;
	}
	
	public VersionInfo getVersions(){
		SQLiteDatabase db = getReadableDatabase();

		String sql = "SELECT * FROM "+TB_VERSIONS + " WHERE " + COL_ID_SITE + "=" + idSite;
		Cursor cur = db.rawQuery(sql, null);

		VersionInfo version = null;
		if(cur.moveToNext()){
			version = new VersionInfo(
					cur.getInt(1),		// aps
					cur.getInt(2),		// maps
					cur.getInt(3)	// nodes_edges
			);		
		}
		db.close();
		return version;
	}
	
	
	/*nested class for sorting APData objects by RSS IN DESC ORDER (signs of the 'one' changed with respect to ASCENDENT order)*/
	public class SortByRSS implements Comparator<OnlineItem>{
		public int compare(OnlineItem arg0, OnlineItem arg1) {
			if ( arg0.getRSS() < arg1.getRSS()){
				return 1;
			} else if ( arg0.getRSS() == arg1.getRSS() ){
				return 0;	
			} else if ( arg0.getRSS() > arg1.getRSS() ){
				return -1;
			}
			return 0;
		}
	}
	
	/*nested class for sorting MapInfo objects by floor IN DESC ORDER (signs of the 'one' changed with respect to ASCENDENT order)*/
	public class SortByFloor implements Comparator<MapInfo>{
		public int compare(MapInfo arg0, MapInfo arg1) {
			if ( arg0.getFloor() < arg1.getFloor()){
				return 1;
			} else if ( arg0.getFloor() == arg1.getFloor() ){
				return 0;	
			} else if ( arg0.getFloor() > arg1.getFloor() ){
				return -1;
			}
			return 0;
		}
	}

}
