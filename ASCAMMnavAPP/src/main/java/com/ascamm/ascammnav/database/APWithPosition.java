package com.ascamm.ascammnav.database;

public class APWithPosition {
	
	private String mSSID;
	private String mBSSID;
	private float mPositionX;
	private float mPositionY;
	private float mPositionZ;
	
	public APWithPosition(String ssid, String bssid, float positionX, float positionY, float positionZ) {
		mSSID = ssid;
		mBSSID = bssid;
		mPositionX = positionX;
		mPositionY = positionY;
		mPositionZ = positionZ;
	}
	
	public String getSSID(){
		return mSSID;
	}
	
	public String getBSSID(){
		return mBSSID;
	}
	public float getPositionX(){
		return mPositionX;
	}

	public float getPositionY(){
		return mPositionY;
	}

	public float getPositionZ(){
		return mPositionZ;
	}
}
