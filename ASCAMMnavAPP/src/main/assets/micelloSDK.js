//Demo features
var navigation = true;
var popup_info = false;
var searchBar = false;

//Micello Control variables
var mapControl;
var mapData;
var mapCanvas;
var mapView;
var mapGUI;

//Navigation & position variables
var navMarker;
var destMarker;
var mapClick;

var selectedID;
var navigating = false;

function toggleNavigation() {
    if (navigation === true) {
        navigation = false;
        clearRoute();
        document.getElementById("togg_nav").value = "Navigation: Off";
    }
    else {
        navigation = true;
        document.getElementById("togg_nav").value = "Navigation: On";
    }
}

function toggleEvents() {
    if (popup_info === true) {
        popup_info = false;
        document.getElementById("togg_evt").value = "Events: Off";
    }
    else {
        popup_info = true;
        document.getElementById("togg_evt").value = "Events: On";
    }
}

var COMMUNITY_ID;

//micello.maps.init(API_KEY, mapInit);

function initMicelloMap(api_key, comm_id) {
    COMMUNITY_ID = comm_id;
    micello.maps.init(api_key, mapInit);
}

function  mapInit()  {
    mapControl = new micello.maps.MapControl('mapElement');

    mapData = mapControl.getMapData();
    mapData.mapChanged = onMapChanged;

    mapControl.onMapClick = onMapClick;
    mapCanvas = mapControl.getMapCanvas();

    mapView = mapControl.getMapView();
    mapView.customView = true;
    // Esto rota el mapa para mostrarlo en vertical (a lo largo de la pantalla)
    mapView.setBaseAngRad(1.57);

    mapGUI = mapControl.getMapGUI();
    mapGUI.NAME_VIEW = 'on';
    mapGUI.LEVELS_VIEW = 'on';
    mapGUI.GEO_UNITS = 'metric';
    mapGUI.GEO_UNITS_TOGGLE = 'off';
    mapGUI.ZOOM_VIEW = 'on';
    mapGUI.ZOOM_POSITION = 'center bottom';
    mapGUI.ZOOM_DISPLAY = 'h';

    // Directory plugin
    Directory = new micello.maps.Directory;
    if (Directory) {
        Directory.DIRECTORY_UI_POSITION = "left top";
        Directory.DIRECTORY_UI_DISPLAY = "on";
        // Directory.DIRECTORY_BLACKLIST= ["Elevator", "Escalator", "Stairs", "Telephone", "Bathroom", "Changing Station", "Family Bathroom", "ATM"];
        // Inicializar el directorio en el mapa
        mapControl.initMapPlugin(Directory);
        Directory.DIRECTORY_CLICK_OVERRIDE = onDirectoryClick;
        Directory.DirectoryInit();
    }

    // Finally,  load  the  map:
    mapData.loadCommunity(COMMUNITY_ID);
}

function show_floors_info() {
    var community = mapData.getCommunity();
    var floors = "";
    for (var i=0; i<community.d.length; ++i) {
        var drawing = community.d[i];
        for (var j=0; j<drawing.l.length; ++j) {
            var floor = drawing.l[j];
            floors += "["+j+"] = "+floor.nm+": "+floor.z+"<br/>";
        }
    }
    document.getElementById("floors_info").innerHTML = floors;
}

function onMapChanged(e) {
    if (e.comLoad) {
        overrideTheme();
        //buildDirectory();
        createTrackMarker(mapData.getCurrentDrawing().w/2, mapData.getCurrentDrawing().h /2, mapData.getCurrentLevel().id);
        //placeTrackMarker(562.34, 569.42, 56828);
        //placeTrackMarker(823.22, 571.79, 56825);
        //placeTrackMarker(504.04, 565.72, 56827);
    }
}

function onMapClick (mx, my, selected) {
    if (selected) {
        document.getElementById("selected_item").innerHTML = selected.nm + " - " + selected.t;
    }
    if (!selected || selected.t && (selected.t == "Background" || selected.t == "Hallway" || selected.t == "Parcel" || selected.t == "Inaccessible Space")) {
        mapControl.hideInfoWindow();
        if (!navigating) clearMarker();
    }
    else if (selected.t != "Selected") {
        if (!destMarker  || (selected.id != destMarker.id && selected.id != navMarker.id)) {
            //mapControl.showPopupMenu(selected, mapView.mapToCanvasX(mx, my)+","+mapView.mapToCanvasY(mx, my), null);
            var menuItems = new Array();
            var name = "";
            if (selected.id) selectedID = selected.id;
            if (selected.nm) {
                name = selected.nm;
                if (selected.t == "Level Change") {
                    if (mapData.getCurrentLevel().z < maxMapLevel()) {
                        menuItems.push(upperLevelMenu());
                    }
                    if (mapData.getCurrentLevel().z > minMapLevel()) {
                        menuItems.push(lowerLevelMenu());
                    }
                }
                else if (selected.nm != "Bathroom" && selected.nm != "Family Bathroom" && selected.nm != "Changing Station" && popup_info) {
                    var menuItems = new Array();
                    menuItems.push(eventsMenu());
                }
                else if (selected.t == "Unit") {
                    /*if (mapControl.popupFlags && micello.maps.MapControl.SHOW_INFO) {
                        mapControl.loadInfoCmd(selected, menuItems);
                    }*/
                    menuItems.push(infoMenu(selected));
                    //menuItems.push(insideMenu(selected));
                }
            }
            else if (selected.t) name = selected.t;
            if (navigation) {
                menuItems.push(navigateMenu());
            }
            //mapControl.defaultSelectAction(selected);
            //mapControl.showDefaultInfoWindow(selected);
            //clearMarker();
            if (mx && my) {
                //selectedClick = selected;
                //selectedClick["mm"] = [[mx,my],[mx+1,my+1]]
                mapControl.showPopupMenu(selected, name, menuItems);
                //placeMarker(mx, my, selected.id);
                mapClick = {
                    mx: selected.mm[0][0]+(selected.mm[1][0]-selected.mm[0][0])/2,
                    my: selected.mm[0][1]+(selected.mm[1][1]-selected.mm[0][1])/2,
                    id: selected.id
                };
            }
            //else {
                //clearAllMarkers();
            //}
        }
        else mapControl.defaultSelectAction(selected);
    }
    else {
        mapControl.hideInfoWindow();
    }
}

function onDirectoryClick(clicked) {
    if(clicked) {
        // hide the directory window
        document.getElementById("micello_dir").style.display = "none";

        // set the coorect drawing and level
        Directory.Directory_changeMap(clicked.did, clicked.lid);

        // center the map on geom
        mapControl.centerOnGeom(mapData.geomMap[clicked.gid].g);

        // show pop-up
        var geom = mapData.geomMap[clicked.gid].g
        mx = geom.l[0];
        my = geom.l[1];
        onMapClick(mx, my, geom)
    }
}

function setNavCoordinates(lat, lon, level) {
    document.getElementById("floor_label").innerHTML = level+" - "+mapData.getCurrentDrawing().l[level].nm;
    document.getElementById("latitude_label").innerHTML = lat;
    document.getElementById("longitude_label").innerHTML = lon;
    clearNavMarker();
    var mxy = mapData.latLonToMxy(lat, lon);
    if (navMarker && mapData.getCurrentDrawing().l[level].id != navMarker.lid) {
        mapData.setLevel(mapData.getCurrentDrawing().l[level]);
    }
    placeTrackMarker(mxy[0], mxy[1], mapData.getCurrentDrawing().l[level].id);
}

function findLevelIndex(id) {
    for (var i = 0; i < mapData.getCurrentDrawing().l.length; ++i) {
        if (id == mapData.getCurrentDrawing().l[i].id) {
            return i;
        }
    }
    return null;
}

function recenterOnTracker() {
    var index = findLevelIndex(navMarker.lid);
    if (index) {
        mapData.setLevel(mapData.getCurrentDrawing().l[index]);
        var lat_long = mapData.mxyToLatLon(navMarker.mx, navMarker.my);
        //var geom = mapCanvas.radialSearch(lat_long[0], lat_long[1], 2);
        var geom = mapCanvas.hitCheck(mapData.getCurrentLevel().g, navMarker.mx, navMarker.my);
        if (geom) mapControl.centerOnGeom(geom);
        //else
    }
}

function recenterOnGeom (geom_id) {
    if (mapData.geomMap[geom_id]) {
        mapData.setLevel(mapData.geomMap[geom_id].pl);
        mapControl.centerOnGeom(mapData.geomMap[geom_id].g);
    }
}

function createTrackMarker(mx, my, lid) {
    navMarker = {
        "mx" : mx,
        "my" : my,
        "lid" : lid,
        "mt" : micello.maps.markertype.NAMED,
        "mr" : "TrackingDot",
        "zi" : 6000,
        "idat": "<div>This is your location</div>",
        "anm" : "dots"
    };
    return navMarker;
}

function placeTrackMarker(mx, my, lid) {
    //var blueDot = {"src" : "dot_blue.png", "ox" : 20, "oy" : 20};
    navMarker = createTrackMarker(mx, my, lid);
    mapData.addMarkerOverlay(navMarker);
    //if (navigating) {
    //    navigateFrom();
    //}
    //mapControl.showPopupMenu(navMarker, mx+","+my, null);
}

function placeMarker(mx, my) {
    //var redMark = {"src" : "location_on_red.png", "ox" : 24, "oy" : 48};
    destMarker = {
        "mx" : mx,
        "my" : my,
        "lid" : mapData.getCurrentLevel().id,
        "mt" : micello.maps.markertype.NAMED,
        "mr" : "RedPin",
        "zi" : 5000,
        "idat": "<div>This is the destination point</div>",
        "anm" : "pins"
    };
    mapData.addMarkerOverlay(destMarker);
}

function navigateFrom() {
    var navPos = [{
        "t" : "mc",
        "mx" : navMarker.mx,
        "my" : navMarker.my,
        "lid" : navMarker.lid
    }];
    mapControl.requestNavFrom(navPos);
}

function navigateToDest() {
    clearRoute();
    placeMarker(mapClick.mx, mapClick.my);
    navigateFrom();
    var destPos = [{
        "t" : "mc",
        "mx" : destMarker.mx,
        "my" : destMarker.my,
        "lid" : destMarker.lid
    }];
    mapControl.requestNavTo(destPos);
    navigating = true;
}

function clearRoute() {
navigating = false;
    mapControl.clearRoute();
    //navigateFrom();
    clearMarker();
}

function clearAllMarkers() {
    clearNavMarker();
    clearMarker();
}

function clearNavMarker() {
    mapData.removeMarkerOverlay("dots", true);
}

function clearMarker() {
    //mapData.removeMarkerOverlay(destMarker)
    mapData.removeMarkerOverlay("pins", true);
    navigating = false;
}

function overrideTheme() {
    var themeToUse = {
        "m" : {
            "RouteStart" : {
                "src": "",
                "ox": 2,
                "oy": 2
            },
            "RouteEnd" : {
                "src": "",
                "ox": 2,
                "oy": 2
            }
        },
        "s" : {
            "Route" : {
                "w" : 2,
                "t" : "#ffffff",
                "m" : "#800000"
            },
            "Background": {
                "m": "#eaeae1",
                "o": "#808080",
                "t": "#404040",
                "w": 2,
                "img": true, // default texture
                "shadow": true // default shadow
            },
            "Parking Structure": {
                "m": "#c7c671",
                "img": true,
                "shadow": ["#999999", 5, 5, 5] // custom shadow
            },
            "Parking Lot": {
                "m": "#DEB887",
            },
            "Building": {
                "m": "#b0d2e5",
                "img": true, // default texture
                "shadow": ["#999999", 3, 5, 10] // custom shadow
            }
        }
    };
    if (themeToUse) {
        mapCanvas.setOverrideTheme(themeToUse);
    }
}

function rotateDiv(degrees) {
    var d = 0;
    while (d <= degrees) {
        document.getElementById("micello-map").style = "transform: rotate("+d+"deg);";
        d++;
    }
}

function findBuildingName() {
    var name = document.getElementById('searchBar').value;
    var dir = buildDirectoryFloor();
    var entities = "<ul style=\"list-style-type:none\">";
    for (var i = 0; i < dir.length; ++i) {
        var dirname = ""+dir[i].name+"";
        if (dirname.includes(name)) {
            entities += "<li><a href=\"" + dir[i].name + "#" + dir[i].id + "</li>";
        }
    }
    entities += "</ul>";
    document.getElementById('directory_list').innerHTML = entities;
    //document.getElementById('searchcontent').style.height = ""+window.innerHeight+"px";
    document.getElementById('directory_list').style.height = ""+10*dir.length+"px";
}

function buildDirectoryFloor() {
    var directory = new Array();
    var community = mapData.getCommunity();
    for (var i = 0; i < mapData.getCurrentLevel().g.length; ++i) {
        var area = {
            id: mapData.getCurrentLevel().g[i].id,
            name: mapData.getCurrentLevel().g[i].nm
        };
        directory.push(area);
    }
    return directory;
}

function buildCommunityInfo() {
    var community = mapData.getCommunity();
    var entities = "";
    entities += "#Community: "+community.nm+"("+community.id+")"+"<br/>";
    entities += "#Drawings: "+community.d.length+"<br/>";
    for (var i=0; i<community.d.length; ++i) {
        var drawing = community.d[i];
        entities += "#id: "+drawing.id+", name: "+drawing.nm+", root: "+drawing.r+"<br/>";
        entities += "#Dimensions: "+drawing.w+"x"+drawing.h+"<br/>";
        entities += "#Levels: "+drawing.l.length+"<br/>";
        for (var j=0; j<drawing.l.length; ++j) {
            var floor = drawing.l[j];
            entities += "##Level: "+floor.id+", "+floor.nm+"<br/>";
            entities += "zlevel: "+floor.z+"<br/>";
            entities += "Objects: "+floor.g.length+"<br/>";
            for (var k=0; k<floor.g.length; ++k) {
                if (floor.g[k].nm) {
                    entities += floor.g[k].nm+" - #id:"+floor.g[k].id+"<br/>";
                }
            }
        }
    }
    document.getElementById("directory").innerHTML = entities;
}

function buildDirectory() {
    var community = mapData.getCommunity();
    var entities = "";
    entities += "#Levels: "+community.d[0].l.length+"<br/>";
    for (var j=0; j<community.d[0].l.length; ++j) {
        entities += "Level:"+community.d[0].l.id+"<br/>";
        var floor = community.d[0].l[j];
        entities += "#Items: "+floor.g.length+"<br/>";
        for (var i=0; i<floor.g.length; ++i) {
            if (floor.g[i].nm) {
                entities += floor.g[i].nm+" - #id:"+floor.g[i].id+"<br/>";
            }
        }
    }
    document.getElementById("directory").innerHTML = entities;
}

function showSearchPopup() {
    var popup = document.getElementById("searchPopup");
    popup.style.display = "block";
    var span = document.getElementsByClassName("close")[1];
    span.onclick = function() {
        popup.style.display = "none";
    };
    window.onclick = function() {
        if (event.target == popup) {
            popup.style.display = "none";
        }
    };
}

function showPopupList() {
    var popup = document.getElementById("popup");
    popup.style.display = "block";
    document.getElementById("eventList").innerHTML = makeEventList(selectedID);
    var span = document.getElementsByClassName("close")[0];
    span.onclick = function() {
        popup.style.display = "none";
    };
    window.onclick = function() {
        if (event.target == popup) {
            popup.style.display = "none";
        }
    };
}

function makeEventList(id) {
    var events = "";
    for (var i=0; i<5; ++i) {
        events += "<li>"+i+"</li>";
    }
    return events;
}
/*
var strings_dict = {
    upper: "Go to upper level",
    lower: "Go to lower level",
    events: "View events",
    navTo: "Navigate here",
    info: "Info"
}
*/
var strings_dict = {
    upper: "Subir planta",
    lower: "Bajar planta",
    events: "Ver eventos",
    navTo: "Ir aquí",
    info: "Info"
}

function upperLevelMenu() {
    var menuItem = {
        name: strings_dict["upper"],
        func: function(){
            var index = findLevelIndex(mapData.getCurrentLevel().id);
            var nextIndex = index + 1;
            document.getElementById("stairs_info").innerHTML = "on floor " + index + " go to "+ nextIndex + ": " + mapData.getCurrentDrawing().l[nextIndex].nm
            if (index && index < mapData.getCurrentDrawing().l.length) mapData.setLevel(mapData.getCurrentDrawing().l[nextIndex]);
        }
    };
    return menuItem;
}

function lowerLevelMenu() {
    var menuItem = {
        name: strings_dict["lower"],
        func: function(){
            var index = findLevelIndex(mapData.getCurrentLevel().id);
            var nextIndex = index - 1;
            document.getElementById("stairs_info").innerHTML = "on floor " + index + " go to "+ nextIndex + ": " + mapData.getCurrentDrawing().l[nextIndex].nm
            if (index && index > 0) mapData.setLevel(mapData.getCurrentDrawing().l[nextIndex]);
        }
    };
    return menuItem;
}

function eventsMenu() {
    var menuItem = {
        name: strings_dict["events"],
        func: function() {showPopupList();}
    };
    return menuItem;
}

function navigateMenu() {
    var menuItem = {
        name: strings_dict["navTo"],
        func: function() {navigateToDest();}
    };
    return menuItem;
}

function infoMenu(obj) {
    var menuItem = {
        name: strings_dict["info"],
        func: function() {mapControl.showInfo(obj);}
    };
    return menuItem;
}

function insideMenu(obj) {
    var menuItem = {
        name: "Inside",
        func: function() {mapControl.showInside(obj);}
    };
    return menuItem;
}

function minMapLevel() {
    var levels = mapData.getCurrentDrawing().l;
    var l = levels[0].z;
    for (var i=1; i<levels.length; ++i) {
        if (levels[i].z < l) {
            l = levels[i].z;
        }
    }
    return l;
}

function maxMapLevel() {
    var levels = mapData.getCurrentDrawing().l;
    var l = levels[0].z;
    for (var i=1; i<levels.length; ++i) {
        if (levels[i].z > l) {
            l = levels[i].z;
        }
    }
    return l;
}

// Scanset Showing

function clearScansets() {
    document.getElementById("scanset_log").innerHTML = "";
}

function setScanset(mac, rss, bat, perc) {
    var current_scansets = document.getElementById("scanset_log").innerHTML;
    current_scansets += mac+" "+rss+" "+bat+"("+perc+")%"+"<br/>";
    document.getElementById("scanset_log").innerHTML = current_scansets;
}

function setSource(source) {
    document.getElementById("source_type").innerHTML = source;
}