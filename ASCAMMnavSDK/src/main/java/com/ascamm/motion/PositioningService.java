package com.ascamm.motion;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.ascamm.motion.algorithm.AutocalibrationParameters;
import com.ascamm.motion.comm.DataManager;
import com.ascamm.motion.detections.Detection;
import com.ascamm.motion.detections.DetectionListener;
import com.ascamm.motion.manage.PositionClient;
import com.ascamm.motion.routing.Route;
import com.ascamm.motion.routing.RouteListener;
import com.ascamm.motion.utils.Constants;
import com.ascamm.motion.utils.DbListener;
import com.ascamm.motion.utils.LogWriter;
import com.ascamm.motion.utils.MotionListener;
import com.ascamm.motion.utils.MotionStepsInfoListener;
import com.ascamm.motion.utils.OnlineItem;
import com.ascamm.motion.utils.PositionListener;
import com.ascamm.motion.utils.PositionsListener;
import com.ascamm.motion.utils.ScansetListener;
import com.ascamm.motion.utils.Settings;
import com.ascamm.motion.utils.UpdateAutocalibrationParametersListener;
import com.ascamm.utils.LatLng3D;
import com.ascamm.utils.Point3D;


public class PositioningService extends Service {

	private String TAG = getClass().getSimpleName();
	
	private boolean debug = Constants.DEBUGMODE;
	
	private final IBinder mBinder = new PositioningServiceBinder(this);
	
	private static boolean initialized;

	private PositionClient mPositionClient;

	private DataManager mDataManager;

	private String deviceId = "";

	/** Code added for logs **/
	private LogWriter mLogWriter;
	private boolean logsRunning = false, storeLogs = false, storeLogWorking = false;
	private String logsFilename = "logs_positioning_service.txt";
	private ScheduledExecutorService mScheduleTaskExecutor;
	private ScheduledFuture<?> mScheduledFuture = null;
	private int logWorkingSeconds = 300; // 5 minutes
	
	/** Broadcast Receiver **/
	private PositioningReceiver mPositioningReceiver;

	/** PRODUCTION LISTENERS **/
	private ArrayList<PositionListener> mPositionListeners = new ArrayList<PositionListener>();
	private ArrayList<DetectionListener> mDetectionListeners = new ArrayList<DetectionListener>();
	private ArrayList<RouteListener> mRouteListeners = new ArrayList<RouteListener>();

	/** DEBUG LISTENERS **/
	private ArrayList<UpdateAutocalibrationParametersListener> mAutocalibrationParametersListeners = new ArrayList<UpdateAutocalibrationParametersListener>();
	private ArrayList<ScansetListener> mScansetListeners = new ArrayList<ScansetListener>();
	private ArrayList<MotionListener> mMotionListeners = new ArrayList<MotionListener>();
	private ArrayList<DbListener> mDbListeners = new ArrayList<DbListener>();
	private ArrayList<MotionStepsInfoListener> mMotionStepsInfoListeners = new ArrayList<MotionStepsInfoListener>();
	//Added to test RSS transformation
	private ArrayList<PositionsListener> mPositionsListeners = new ArrayList<PositionsListener>();
	/**************************************/

	private PositionsListener mPositionsListener = new PositionsListener() {
		@Override
		public void newPositionAvailable(ArrayList<LocationData> locationsData) {
			LocationData locationData = locationsData.get(0);
			if(mDataManager!=null) {
				mDataManager.savePosition(locationData);
			}
			// If APP has bound to service
			fireNewPositionEvent(locationData);
			fireNewPositionsEvent(locationsData);

			// If APP has not bound to service
			Intent respIntent = new Intent();
			respIntent.setAction(PosActions.NEW_POSITION);
			respIntent.putExtra("location", locationData);
			sendBroadcast(respIntent);

			CharSequence txtMsg = "NEW POSITION sent";
//			Toast.makeText(PositioningService.this, txtMsg, Toast.LENGTH_SHORT).show();
			Log.d(TAG, "New Position sent");

			// If APP has not bound to service
			Intent respIntent2 = new Intent();
			respIntent2.setAction(PosActions.NEW_POSITIONS);
			respIntent2.putExtra("locations", locationsData);
			sendBroadcast(respIntent2);

//			Toast.makeText(PositioningService.this, "NEW POSITIONS sent", Toast.LENGTH_SHORT).show();
			Log.d(TAG, "New Positions sent");
		}
	};
	
	private DetectionListener mDetectionListener = new DetectionListener() {
		@Override
		public void newDetectionAvailable(ArrayList<Detection> detections, int siteId){
			if(debug){
				Log.d(TAG, "newDetections received:");
				for(Detection det : detections){
					Log.d(TAG, det.toString());
				}
			}
			if(Settings.sendDetections) {
				long ts = System.currentTimeMillis() / 1000; // seconds
				for (Detection det : detections) {
					if (det.getPosLocal() != null) {
						if (det.getPosLocal().x != 0 && det.getPosLocal().y != 0) { //only show APs that have coordinates different than (0,0,0)
							if (mDataManager != null) {
								mDataManager.saveDetection(det, ts, siteId);
							}
						}
					}
				}
			}
			// If APP has bound to service
			fireNewDetectionEvent(detections, siteId);

			// If APP has not bound to service
			Intent respIntent = new Intent();
			respIntent.setAction(PosActions.NEW_DETECTIONS);
			respIntent.putExtra("detections", detections);
			respIntent.putExtra("site_id", siteId);
			sendBroadcast(respIntent);
		}
	};

	private RouteListener mRouteListener = new RouteListener() {

		@Override
		public void newRouteCalculated(Route route) {
			// If APP has bound to service
			fireNewRouteCalculated(route);

			// If APP has not bound to service
			Intent respIntent = new Intent();
			respIntent.setAction(PosActions.CALC_ROUTE_RESP);
			respIntent.putExtra("route", route);
			sendBroadcast(respIntent);
		}
	};

	private UpdateAutocalibrationParametersListener mAutocalibrationListener = new UpdateAutocalibrationParametersListener() {
		@Override
		public void newUpdate(AutocalibrationParameters autoParams){
			//Log.d(TAG, "newUpdate received. a: " + slope + " , b: " + intersection + " , y: " + residuals);
			// If APP has bound to service
			fireNewAutocalibrationEvent(autoParams);

			// If APP has not bound to service
			Intent respIntent = new Intent();
			respIntent.setAction(PosActions.NEW_AUTO_PARAMS);
			respIntent.putExtra("slope", autoParams.getSlope());
			respIntent.putExtra("intersection", autoParams.getIntersection());
			respIntent.putExtra("residuals", autoParams.getResiduals());
			sendBroadcast(respIntent);

		}
	};

	private ScansetListener mScansetListener = new ScansetListener() {
		@Override
		public void newScansetAvailable(ArrayList<OnlineItem> scanset){
			if(debug)	Log.d(TAG, "newScanset received");
			// If APP has bound to service
			fireNewScansetEvent(scanset);

			// If APP has not bound to service
			if(Settings.scansetNeeded){
				Intent respIntent = new Intent();
				respIntent.setAction(PosActions.NEW_SCANSET);
				respIntent.putExtra("scanset", scanset);
				sendBroadcast(respIntent);
			}
		}
	};
	
	private MotionListener mMotionListener = new MotionListener() {
		@Override
		public void newMotionAvailable(int sensorState){
			if(debug)	Log.d(TAG, "newMotion received. sensorState: " + sensorState);
			// If APP has bound to service
			fireNewMotionEvent(sensorState);

			// If APP has not bound to service
			Intent respIntent = new Intent();
			respIntent.setAction(PosActions.NEW_MOTION);
			respIntent.putExtra("sensorState", sensorState);
			sendBroadcast(respIntent);

		}
	};

	private DbListener mDbListener = new DbListener() {
		@Override
		public void dbLoaded(float pxMeter){
			//Log.d(TAG, "DBListener: DB loaded. pxMeter: " + pxMeter);
			// If APP has bound to service
			fireNewDatabaseData(pxMeter);

			// If APP has not bound to service
			Intent respIntent = new Intent();
			respIntent.setAction(PosActions.NEW_DATABASE_DATA);
			respIntent.putExtra("px_meter", pxMeter);
			sendBroadcast(respIntent);

		}
	};

	private MotionStepsInfoListener mMotionStepsInfoListener = new MotionStepsInfoListener() {
		@Override
		public void newMotionStepsInfoAvailable(int motionMode, ArrayList<Double> stepsList, double orientation, double diffOrientation){
			// If APP has bound to service
			fireNewMotionStepsInfoEvent(motionMode, stepsList, orientation, diffOrientation);

			// If APP has not bound to service
			Intent respIntent = new Intent();
			respIntent.setAction(PosActions.NEW_MOTION_MODE);
			respIntent.putExtra("motion_mode", motionMode);
			respIntent.putExtra("steps_list", stepsList);
			respIntent.putExtra("orientation", orientation);
			respIntent.putExtra("diff_orientation", diffOrientation);
			sendBroadcast(respIntent);

		}
	};

	/** Broadcast Receiver **/
	private class PositioningReceiver extends BroadcastReceiver {
		private String TAG = getClass().getSimpleName();

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if(debug)	Log.d(TAG,"action: " + action);
			if(action.equalsIgnoreCase(PosActions.SET_SITE_ID)){
				int siteId = intent.getExtras().getInt("siteId");
				setSiteId(siteId);
				Intent respIntent = new Intent();
				respIntent.setAction(PosActions.SET_SITE_ID_RESP);
				respIntent.putExtra("success", true);
				sendBroadcast(respIntent);
			}
			else if(action.equalsIgnoreCase(PosActions.IS_LOCATING)){
				Intent respIntent = new Intent();
				respIntent.setAction(PosActions.IS_LOCATING_RESP);
				respIntent.putExtra("isLocating", mPositionClient.isLocating());
				sendBroadcast(respIntent);
			}
			else if(action.equalsIgnoreCase(PosActions.START_LOCATING)){
				int period = intent.getExtras().getInt("period");
				Start(period);
				Intent respIntent = new Intent();
				respIntent.setAction(PosActions.START_LOCATING_RESP);
				respIntent.putExtra("success", true);
				sendBroadcast(respIntent);
			}
			else if(action.equalsIgnoreCase(PosActions.STOP_LOCATING)){
				Stop();
				Intent respIntent = new Intent();
				respIntent.setAction(PosActions.STOP_LOCATING_RESP);
				respIntent.putExtra("success", true);
				sendBroadcast(respIntent);
			}
			else if(action.equalsIgnoreCase(PosActions.CALC_ROUTE)){
				Point3D currentPosition = (Point3D) intent.getSerializableExtra("current_position");
				Point3D destinationPosition = (Point3D) intent.getSerializableExtra("destination_position");
				calculateRoute(currentPosition, destinationPosition);
			}
			else if(action.equalsIgnoreCase(PosActions.SEND_LOCATIONS_CONFIG)){
				int seconds = intent.getExtras().getInt("seconds");
				String serverUrl = intent.getExtras().getString("server_url");
				boolean success = setSendLocationsConfig(seconds, serverUrl);
				Intent respIntent = new Intent();
				respIntent.setAction(PosActions.SEND_LOCATIONS_CONFIG_RESP);
				respIntent.putExtra("success", success);
				sendBroadcast(respIntent);
			}
		}
	};

	private String getWifiMacAddress() {
		String wifiMac = null;
		//Obtain the Wifi MAC of the device as an identifier
		WifiManager manager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		if (manager != null) {
			WifiInfo info = manager.getConnectionInfo();
			if (info != null) {
				String wifiAddress = info.getMacAddress();
				if (wifiAddress != null) {
					wifiMac = wifiAddress.toLowerCase();
				}
			}
		}
		//Log.d(TAG, "wifiMAC: " + wifiMac);
		return wifiMac;
	}

	private String getIMEI(Context context){
		String imei = null;
		TelephonyManager tManager = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
		if(tManager!=null){
			imei = tManager.getDeviceId();
		}
		//Log.d(TAG, "IMEI: " + imei);
		return imei;
	}

	private String getAndroidId(Context context){
		String androidId = android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
		//Log.d(TAG, "androidId: " + androidId);
		return androidId;
	}

	private String getSerialNumber() {
		String serialNumber = Build.SERIAL;
		//Log.d(TAG, "serialNumber: " + serialNumber);
		return serialNumber;
	}

	private String getDeviceId(Context context){
		//String uid = getWifiMacAddress();
		//String uid = getIMEI(context);
		//String uid = getAndroidId(context);
		String uid = getSerialNumber();
		return uid;
	}

	private void startKeepServiceRunning(){
		//Log.d(TAG,"starting KeepServiceRunning");
		Context ctxt = PositioningService.this;
		Intent i=new Intent(ctxt, PositioningService.class);
		i.setAction(Constants.ACTION_KEEP_SERVICE_RUNNING);
		PendingIntent pi=PendingIntent.getService(ctxt, Constants.ALARM_KEEP_SERVICE_RUNNING, i, 0);
		AlarmManager mgr = (AlarmManager)ctxt.getSystemService(Context.ALARM_SERVICE);
		mgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + Constants.KEEP_SERVICE_RUNNING_SECONDS * 1000, Constants.KEEP_SERVICE_RUNNING_SECONDS * 1000, pi);
		//mgr.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + KEEP_SERVICE_RUNNING_SECONDS*1000, KEEP_SERVICE_RUNNING_SECONDS*1000, pi);

	}

	private void stopKeepServiceRunning(){
		//Log.d(TAG,"stopping KeepServiceRunning");
		Context ctxt = PositioningService.this;
		Intent i=new Intent(ctxt, PositioningService.class);
		i.setAction(Constants.ACTION_KEEP_SERVICE_RUNNING);
		PendingIntent pi=PendingIntent.getService(ctxt, Constants.ALARM_KEEP_SERVICE_RUNNING, i, 0);
		AlarmManager mgr = (AlarmManager)ctxt.getSystemService(Context.ALARM_SERVICE);
		mgr.cancel(pi);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		IntentFilter filter = new IntentFilter();
		filter.addAction(PosActions.SET_SITE_ID);
		filter.addAction(PosActions.IS_LOCATING);
		filter.addAction(PosActions.START_LOCATING);
		filter.addAction(PosActions.STOP_LOCATING);
		filter.addAction(PosActions.CALC_ROUTE);
		filter.addAction(PosActions.SEND_LOCATIONS_CONFIG);
		mPositioningReceiver = new PositioningReceiver();
		registerReceiver(mPositioningReceiver, filter);

		deviceId = getDeviceId(this);
		if(debug)	Log.d(TAG,"deviceId: " + deviceId);

		mDataManager = new DataManager(this, deviceId);
		mPositionClient = new PositionClient(this);
		initialized = true;
						
		/** Code added to store data info into logs.txt **/
		if(storeLogs){
			mLogWriter = new LogWriter(Constants.APP_DATA_FOLDER_NAME, logsFilename);
			if(mLogWriter.startLogs()>0){
				mLogWriter.stopLogs();
			}
			logsRunning = true;
			if(storeLogWorking){
				mScheduleTaskExecutor= Executors.newScheduledThreadPool(5);
				mScheduledFuture = mScheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
					public void run() {
						if(debug)	Log.d("mScheduleTaskExecutor", "start scheduled task. Writing log into file");
						if(mLogWriter.startLogsNoText()>0){
							long ts = System.currentTimeMillis();
							mLogWriter.addTextLog(ts, "PositioningService is working");
							mLogWriter.stopLogs();
						}
					}
				}, logWorkingSeconds, logWorkingSeconds, TimeUnit.SECONDS);
			}
		}
		//Toast.makeText(this, "PositioningService onCreate", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startid){
		//Toast.makeText(this, "PositioningService onStartCommand", Toast.LENGTH_SHORT).show();
		//return flags;
		if(intent!=null && intent.getAction()!=null){
			/*
			String txt = "ACTION: " + intent.getAction();
			Log.d(TAG, txt);
			Toast.makeText(this, txt, Toast.LENGTH_SHORT).show();
			*/
			if(intent.getAction().equalsIgnoreCase(Constants.ACTION_KEEP_SERVICE_RUNNING)){
				if(!isLocating()){
					Start(Constants.DEFAULT_POSITION_REFRESH_TIME);
				}
			}
			else if(intent.getAction().equalsIgnoreCase(PosActions.ACTION_START_ON_BOOT)){
				if(!isLocating()){
					Start(Constants.DEFAULT_POSITION_REFRESH_TIME);
				}
			}
		}
		Intent respIntent = new Intent();
		respIntent.setAction(PosActions.SERVICE_OK);
		sendBroadcast(respIntent);
		return START_STICKY;
	}
	
	@Override
	public void onDestroy()
	{
		//Toast.makeText(this, "PositioningService onDestroy", Toast.LENGTH_SHORT).show();
		unregisterReceiver(mPositioningReceiver);
		mPositionClient.destroy();
		mDataManager.destroy();

		/** Code added to store data info into logs.txt **/
		if(storeLogs && logsRunning){
			if (mScheduledFuture != null) {
				//cancel execution of the future scheduled task
				//If task is already running, interrupt it here.
				mScheduledFuture.cancel(true);
			}
			logsRunning = false;
		}
		super.onDestroy();
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		//Toast.makeText(this, "Location Service binded", Toast.LENGTH_SHORT).show();
		return mBinder;
	}
	
	@Override
	public boolean onUnbind(Intent intent) {
		initialized = false;
		//Toast.makeText(this, "Location Service unbinded", Toast.LENGTH_SHORT).show();
		return true;
	}
	
	/**
	 * ------------------------------------------------------------- 
	 * Service API
	 * 
	 * ------------------------------------------------------------- 
	 */
	
	public void setUIcontext(Context context){
		mPositionClient.setUIcontext(context);
	}

	public void setSiteId(int siteId){
		Settings.siteFromSettings = siteId;
	}

	public boolean setSendLocationsConfig(int seconds, String serverUrl){
		boolean success = false;
		if(mDataManager!=null) {
			mDataManager.setSendPositionsTime(seconds);
			mDataManager.setURLtoSendPositions(serverUrl);
			success = true;
		}
		return success;
	}
	
	/**
	 * Calculates a route from start to end positions. Route is calculated in background
	 * and will send an event to RouteListeners when it has finished. You MUST register a 
	 * RouteListener to get the route calculated
	 * 
	 * @param start
	 * @param end
	 */
	public void calculateRoute(Point3D start, Point3D end) {
		mPositionClient.calculateRoute(start, end);
	}
	
	public void Start(int period){
		//Toast.makeText(this, "inside service Positioning: Start(int period)", Toast.LENGTH_SHORT).show();
		if(!isLocating()) {
			mPositionClient.addPositionsListener(mPositionsListener);
			mPositionClient.addDetectionListener(mDetectionListener);
			mPositionClient.addRouteListener(mRouteListener);
			mPositionClient.addAutocalibrationListener(mAutocalibrationListener);
			mPositionClient.addScansetListener(mScansetListener);
			mPositionClient.addDatabaseDataListener(mDbListener);
			mPositionClient.addMotionListener(mMotionListener);
			mPositionClient.addMotionStepsInfoListener(mMotionStepsInfoListener);
			mPositionClient.Start(period);
			mDataManager.startSendingPositions();
			/** Code added to store data info into logs.txt **/
			if (storeLogs) {
				if (mLogWriter.startLogsNoText() > 0) {
					long ts = System.currentTimeMillis();
					mLogWriter.addTextLog(ts, "starting positioning...");
					mLogWriter.stopLogs();
				}
			}
			startKeepServiceRunning();
		}
	}
	
	public void Stop(){
		stopKeepServiceRunning();
		mPositionClient.Stop();
		mPositionClient.removePositionsListener(mPositionsListener);
		mPositionClient.removeDetectionListener(mDetectionListener);
		mPositionClient.removeRouteListener(mRouteListener);
		mPositionClient.removeAutocalibrationListener(mAutocalibrationListener);
		mPositionClient.removeScansetListener(mScansetListener);
		mPositionClient.removeDatabaseDataListener(mDbListener);
		mPositionClient.removeMotionListener(mMotionListener);
		mPositionClient.removeMotionStepsInfoListener(mMotionStepsInfoListener);
		mDataManager.stopSendingPositions();
		/** Code added to store data info into logs.txt **/
		if(storeLogs){
			if(mLogWriter.startLogsNoText()>0){
				long ts = System.currentTimeMillis();
				mLogWriter.addTextLog(ts, "stopping positioning...");
				mLogWriter.stopLogs();
			}
		}
	}
	
	public boolean isLocating(){
		return mPositionClient.isLocating();
	}

	public List<Integer> getFloorList() {
		return mPositionClient.getFloorList();
	}

	public Point3D convertFromGlobalToLocal(LatLng3D point) {
		return mPositionClient.convertFromGlobalToLocal(point);
	}

	public LatLng3D convertFromLocalToGlobal(Point3D point) {
		return mPositionClient.convertFromLocalToGlobal(point);
	}
	


	/***** PRODUCTION LISTENERS *****/

	/* event listeners corresponding to positions */
	public synchronized void addPositionListener(PositionListener listener) {
		if(!mPositionListeners.contains(listener)) {
			mPositionListeners.add(listener);
		}
	}

	public synchronized void removePositionListener(PositionListener listener) {
		mPositionListeners.remove(listener);
	}

	private synchronized void fireNewPositionEvent(LocationData locationData){
		for(PositionListener listener : mPositionListeners){
			listener.newPositionAvailable(locationData);
		}
	}

	/* event listeners corresponding to detections */
	public synchronized void addDetectionListener(DetectionListener listener) {
		if(!mDetectionListeners.contains(listener)) {
			mDetectionListeners.add(listener);
		}
	}

	public synchronized void removeDetectionListener(DetectionListener listener) {
		mDetectionListeners.remove(listener);
	}

	private synchronized void fireNewDetectionEvent(ArrayList<Detection> detections, int siteId){
		for(DetectionListener listener : mDetectionListeners){
			listener.newDetectionAvailable(detections, siteId);
		}
	}

	// event listeners corresponding to route calculating
	public synchronized void addRouteListener(RouteListener listener) {
		if(!this.mRouteListeners.contains(listener)) {
			this.mRouteListeners.add(listener);
		}
	}

	public synchronized void removeRouteListener(RouteListener listener) {
		this.mRouteListeners.remove(listener);
	}

	private synchronized void fireNewRouteCalculated(Route route) {
		for(RouteListener listener: mRouteListeners) {
			listener.newRouteCalculated(route);
		}
	}

	/***** END OF PRODUCTION LISTENERS *****/

	/***** DEBUG LISTENERS *****/

	// event listeners corresponding to RSS transformation
	public synchronized void addAutocalibrationListener(UpdateAutocalibrationParametersListener listener) {
		if(!mAutocalibrationParametersListeners.contains(listener)) {
			mAutocalibrationParametersListeners.add(listener);
		}
	}

	public synchronized void removeAutocalibrationListener(UpdateAutocalibrationParametersListener listener) {
		mAutocalibrationParametersListeners.remove(listener);
	}

	private synchronized void fireNewAutocalibrationEvent(AutocalibrationParameters autoParams){
		for(UpdateAutocalibrationParametersListener listener : mAutocalibrationParametersListeners){
			listener.newUpdate(autoParams);
		}
	}

	// Added to test RSS transformation
	public synchronized void addPositionsListener(PositionsListener listener) {
		if(!mPositionsListeners.contains(listener)) {
			mPositionsListeners.add(listener);
		}
	}

	public synchronized void removePositionsListener(PositionsListener listener) {
		mPositionsListeners.remove(listener);
	}

	private synchronized void fireNewPositionsEvent(ArrayList<LocationData> locationsData){
		for(PositionsListener listener : mPositionsListeners){
			listener.newPositionAvailable(locationsData);
		}
	}

	// event listeners corresponding to scanset
	public synchronized void addScansetListener(ScansetListener listener) {
		if(!mScansetListeners.contains(listener)) {
			mScansetListeners.add(listener);
		}
	}

	public synchronized void removeScansetListener(ScansetListener listener) {
		mScansetListeners.remove(listener);
	}

	private synchronized void fireNewScansetEvent(ArrayList<OnlineItem> scanset){
		for(ScansetListener listener : mScansetListeners){
			listener.newScansetAvailable(scanset);
		}
	}
	
	// event listeners corresponding to sensors
	public synchronized void addMotionListener(MotionListener listener) {
		if(!mMotionListeners.contains(listener)) {
			mMotionListeners.add(listener);
		}
	}

	public synchronized void removeMotionListener(MotionListener listener) {
		mMotionListeners.remove(listener);
	}

	private synchronized void fireNewMotionEvent(int sensorState){
		for(MotionListener listener : mMotionListeners){
			listener.newMotionAvailable(sensorState);
		}
	}

	// event listeners corresponding to database data
	public synchronized void addDatabaseDataListener(DbListener listener) {
		if(!mDbListeners.contains(listener)) {
			this.mDbListeners.add(listener);
		}
	}

	public synchronized void removeDatabaseDataListener(DbListener listener) {
		this.mDbListeners.remove(listener);
	}

	private synchronized void fireNewDatabaseData(float pxMeter) {
		for(DbListener listener: mDbListeners) {
			listener.dbLoaded(pxMeter);
		}
	}

	// event listeners corresponding to motion steps info
	public synchronized void addMotionStepsInfoListener(MotionStepsInfoListener listener) {
		if(!mMotionStepsInfoListeners.contains(listener)) {
			mMotionStepsInfoListeners.add(listener);
		}
	}

	public synchronized void removeMotionStepsInfoListener(MotionStepsInfoListener listener) {
		mMotionStepsInfoListeners.remove(listener);
	}

	private synchronized void fireNewMotionStepsInfoEvent(int sensorState, ArrayList<Double> stepsList, double orientation, double diffOrientation){
		for(MotionStepsInfoListener listener : mMotionStepsInfoListeners){
			listener.newMotionStepsInfoAvailable(sensorState, stepsList, orientation, diffOrientation);
		}
	}

	/***** END OF DEBUG LISTENERS *****/

}
