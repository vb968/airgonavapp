package com.ascamm.motion.mems;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.widget.Toast;

import com.ascamm.motion.utils.Constants;
import com.ascamm.motion.utils.LogWriter;
import com.ascamm.motion.utils.Settings;

import static com.ascamm.motion.utils.Constants.RAD_TO_DEG;

public class RelativeOrientation implements SensorEventListener {

    private String TAG = getClass().getSimpleName();
	private boolean DEBUG = Constants.DEBUGMODE;

	private final Context ctx;

	private SensorManager sensorManager;
	private Sensor AccelSensor, GyroSensor;

	private float meanAccX, meanAccY, meanAccZ;
	private float gyroIncX, gyroIncY, gyroIncZ;
	private int gyroIncNum = 0;

	private static final float NS2S = 1.0f / 1000000000.0f;
	private static final float GRAVITY = 9.80665f; // m/s2

	private boolean waitingForSetup = true;
	private boolean isFirstAccMeasure = true;
	private long timeToInit = 300000000; // 300 ms
	private long timeToFinish = 2000000000; // 2s
	private int numAccMeas = 0;
	private long firstAccTimestamp;

	private long lastGyroTimestamp = 0;

	//Rotation matrix variables
	private double r11, r12, r13, r21, r22, r23, r31, r32, r33;
	private double last_r11, last_r12, last_r13, last_r21, last_r22, last_r23, last_r31, last_r32, last_r33;
	private static final double INITIAL_ROLL = -1000;
	private double lastRoll = INITIAL_ROLL, lastPitch;

	private double absoluteOrientation;

	/** Code added for logs **/
	private LogWriter mLogWriter;
	private boolean storeLogs = Settings.storeLogs;
	//private boolean storeLogs = true;
	private boolean logsRunning = false;
	private String logsFilename = Settings.logsFilename + "_sensors" + Constants.logsFilenameExt;

	public RelativeOrientation(Context ctx) {
		this.ctx = ctx;
		sensorManager = (SensorManager) ctx.getSystemService(Context.SENSOR_SERVICE);
		AccelSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		GyroSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

		if(storeLogs){
			mLogWriter = new LogWriter(Constants.APP_DATA_FOLDER_NAME, logsFilename);
		}
	}

	private void registerSensorListeners(){
		sensorManager.registerListener(this, AccelSensor , Constants.SENSOR_DELAY);
		sensorManager.registerListener(this, GyroSensor , Constants.SENSOR_DELAY);
	}

	private void unregisterSensorListeners(){
		sensorManager.unregisterListener(this);
	}

	public void Start(){
		/** Code added to store data info into logs.txt **/
		if(storeLogs && mLogWriter.startLogs()>0){
			logsRunning = true;
		}
		absoluteOrientation = 0;
		registerSensorListeners();
	}

	public void Stop(){
		unregisterSensorListeners();
		/** Code added to store data info into logs.txt **/
		if(storeLogs && logsRunning){
			mLogWriter.stopLogs();
			logsRunning = false;
		}
	}

	/* this method provides the orientation accumulated since the start of the class and the relative orientation since the last call to this method */
	public double[] getRelativeOrientation(){
		float gyroInc[] = getGyroIncValuesAndUpdate();
		long ts = System.currentTimeMillis();
		int gyroIncNum = (int) gyroInc[3];
		saveSensorLog("GYRO_INC", ts, ts, gyroIncNum, gyroInc);
		float out[] = convertFromLocalToGlobal(gyroInc[0], gyroInc[1], gyroInc[2]);
		saveSensorLog("GYRO_INC_GLOBAL", ts, ts, gyroIncNum, out);
		double relativeOrientation = out[2]*RAD_TO_DEG;
		absoluteOrientation += relativeOrientation;
		if(absoluteOrientation<0){
			absoluteOrientation += 360;
		}
		else if(absoluteOrientation>=360){
			absoluteOrientation -= 360;
		}
		if(DEBUG) {
			Log.d(TAG, "gyroInc: (" + gyroInc[0] + "," + gyroInc[1] + "," + gyroInc[2] + ") , numMeas: " + gyroIncNum);
			Log.d(TAG, "output: (" + out[0] + "," + out[1] + "," + out[2] + ")");
			Log.d(TAG, "outOrient: (" + relativeOrientation + "," + absoluteOrientation + ")");
		}
		double orients[] = new double[2];
		orients[0] = absoluteOrientation;
		orients[1] = relativeOrientation;
		return orients;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		String sensorName = sensor.getName();
		String accuracyStr = "";
		if(accuracy == SensorManager.SENSOR_STATUS_ACCURACY_LOW){
			accuracyStr = "LOW";
		}
		else if(accuracy == SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM){
			accuracyStr = "MEDIUM";
		}
		else if(accuracy == SensorManager.SENSOR_STATUS_ACCURACY_HIGH){
			accuracyStr = "HIGH";
		}
		else if(accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE){
			accuracyStr = "UNRELIABLE";
		}
		if(DEBUG) {
			Log.d(TAG, "onAccuracyChanged. (" + sensorName + " , " + accuracyStr + ")");
			Toast.makeText(ctx, sensorName + " | ACC: " + accuracyStr, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		long sysTs = System.currentTimeMillis();
		Sensor mySensor = event.sensor;
		long ts = event.timestamp; //nanoseconds
		int accuracy = event.accuracy;
		float[] values = event.values.clone();
		String sensorName = "";
		boolean saveLog = true;
		if(mySensor.getType() == Sensor.TYPE_ACCELEROMETER){
			sensorName = "ACC";
			processAccelMeasure(ts, accuracy, values);
		}
		else if (mySensor.getType() == Sensor.TYPE_GYROSCOPE) {
			sensorName = "GYRO";
			processGyroMeasure(ts, sysTs, accuracy, values);
		}
		if(saveLog) {
			saveSensorLog(sensorName, ts, sysTs, accuracy, values);
		}
	}

	/* this method updates the Rotation matrix and calculates the roll diff, the pitch diff and the total diff (depending on roll diff and pitch diff) */
	public double[] calculateRmatrix(float meanAccX, float meanAccY, float meanAccZ){
		if(DEBUG)	Log.d(TAG,"calculate R: (" + meanAccX + "," + meanAccY + "," + meanAccZ + ")");
		float accelNorm = (float) Math.sqrt(Math.pow(meanAccX,2) + Math.pow(meanAccY,2) + Math.pow(meanAccZ,2));
		//float accelNorm = GRAVITY;
		meanAccX = meanAccX/accelNorm;
		meanAccY = meanAccY/accelNorm;
		meanAccZ = meanAccZ/accelNorm;
		if(DEBUG)	Log.d(TAG,"calculate R: (" + meanAccX + "," + meanAccY + "," + meanAccZ + ")");
		//double roll = Math.atan2(meanAccY,meanAccZ);
		//double pitch = Math.atan(-meanAccX/(Math.sqrt(meanAccY*meanAccY + meanAccZ*meanAccZ)));
		double roll = Math.atan2(meanAccX,-meanAccZ);
		double pitch = Math.atan(-meanAccY/(Math.sqrt(meanAccX*meanAccX + meanAccZ*meanAccZ)));
		double yaw = 0;
		if(DEBUG)	Log.d(TAG,"roll: " + roll + " (" + roll*RAD_TO_DEG + ") , pitch: " + pitch + " (" + pitch*RAD_TO_DEG + ")");
		double r11 = Math.cos(yaw)*Math.cos(roll) - Math.sin(yaw)*Math.sin(pitch)*Math.sin(roll);
		double r12 = -Math.sin(yaw)*Math.cos(pitch);
		double r13 = Math.cos(yaw)*Math.sin(roll) + Math.sin(yaw)*Math.sin(pitch)*Math.cos(roll);
		double r21 = -Math.sin(yaw)*Math.cos(roll) - Math.cos(yaw)*Math.sin(pitch)*Math.sin(roll);
		double r22 = -Math.cos(yaw)*Math.cos(pitch);
		double r23 = -Math.sin(yaw)*Math.sin(roll) + Math.cos(yaw)*Math.sin(pitch)*Math.cos(roll);
		double r31 = -Math.cos(pitch)*Math.sin(roll);
		double r32 = Math.sin(pitch);
		double r33 = Math.cos(pitch)*Math.cos(roll);
		if(DEBUG)	Log.d(TAG, "R: ((" + r11 + "," + r12 + "," + r13 + "),(" + r21 + "," + r22 + "," + r23 + "),(" + r31 + "," + r32 + "," + r33 + "))");
		float mat[] = new float[9];
		float vector[] = new float[3];
		vector[0] = meanAccX;
		vector[1] = meanAccY;
		vector[2] = meanAccZ;
		SensorManager.getRotationMatrixFromVector(mat, vector);
		if(DEBUG)	Log.d(TAG, "Rmat: ((" + mat[0] + "," + mat[1] + "," + mat[2] + "),(" + mat[3] + "," + mat[4] + "," + mat[5] + "),(" + mat[6] + "," + mat[7] + "," + mat[8] + "))");
		//SensorManager.remapCoordinateSystem(mat, SensorManager.AXIS_X, SensorManager.AXIS_Z, rmat);
		//SensorManager.getOrientation(rmat, angles);
		setRmatrix(r11, r12, r13, r21, r22, r23, r31, r32, r33);
		//setRmatrix(mat[0], mat[1], mat[2], mat[3], mat[4], mat[5], mat[6], mat[7], mat[8]);
		//calculate roll and pitch increment
		double rollDiff = 0;
		double pitchDiff = 0;
		double totalDiff = 0;
		double totalDiffR = 0;
		if(lastRoll!=INITIAL_ROLL){
			double rollDiff1 = Math.abs(roll - lastRoll);
			double roll2;
			if(roll<0){
				roll2 = roll + 2*Math.PI;
			}
			else{
				roll2 = roll - 2*Math.PI;
			}
			double rollDiff2 = Math.abs(roll2 - lastRoll);
			rollDiff = Math.min(rollDiff1, rollDiff2);
			double pitchDiff1 = Math.abs(pitch - lastPitch);
			double pitch2;
			if(pitch<0){
				pitch2 = pitch + 2*Math.PI;
			}
			else{
				pitch2 = pitch - 2*Math.PI;
			}
			double pitchDiff2 = Math.abs(pitch2 - lastPitch);
			pitchDiff = Math.min(pitchDiff1, pitchDiff2);
			totalDiff = Math.sqrt(Math.pow(rollDiff,2) + Math.pow(pitchDiff,2));
			totalDiffR = Math.sqrt(Math.pow(r11-last_r11,2) + Math.pow(r12-last_r12,2) + Math.pow(r13-last_r13,2) + Math.pow(r21-last_r21,2) + Math.pow(r22-last_r22,2) + Math.pow(r23-last_r23,2) + Math.pow(r31-last_r31,2) + Math.pow(r32-last_r32,2) + Math.pow(r33-last_r33,2));
			//Log.d(TAG,"rollDiff: " + rollDiff + " (" + rollDiff*RAD_TO_DEG + ") , pitchDiff: " + pitchDiff + " (" + pitchDiff*RAD_TO_DEG + ") , totalDiff: " + totalDiff + " (" + totalDiff*RAD_TO_DEG + ") , totalDiffR: " + totalDiffR);
		}
		lastRoll = roll;
		lastPitch = pitch;
		last_r11 = r11;
		last_r12 = r12;
		last_r13 = r13;
		last_r21 = r21;
		last_r22 = r22;
		last_r23 = r23;
		last_r31 = r31;
		last_r32 = r32;
		last_r33 = r33;
		double[] result = new double[3];
		result[0] = rollDiff;
		result[1] = pitchDiff;
		result[2] = totalDiff;
		return result;
	}

	/* sets the coefficients of the Rotation matrix */
	private void setRmatrix(double r11, double r12, double r13, double r21, double r22, double r23, double r31, double r32, double r33){
		this.r11 = r11;
		this.r12 = r12;
		this.r13 = r13;
		this.r21 = r21;
		this.r22 = r22;
		this.r23 = r23;
		this.r31 = r31;
		this.r32 = r32;
		this.r33 = r33;
	}

	/* applies the Rotation matrix to convert local coordinates into global coordinates */
	public float[] convertFromLocalToGlobal(float x, float y, float z){
		float outX = (float) (r11*x + r12*y + r13*z);
		float outY = (float) (r21*x + r22*y + r23*z);
		float outZ = (float) (r31*x + r32*y + r33*z);
		float[] out = new float[3];
		out[0] = outX;
		out[1] = outY;
		out[2] = outZ;
		return out;
	}

	private double getNorm(float[] values){
		float sumSquare = 0;
		for(int i=0; i < values.length; i++){
			sumSquare += values[i]*values[i];
		}
		return Math.sqrt(sumSquare);
	}

	private void processAccelMeasure(long ts, int accuracy, float[] values){
		if(waitingForSetup){
			if(DEBUG) {
				double accNorm = getNorm(values);
				String msgTxt = "acc: (" + values[0] + "," + values[1] + "," + values[2] + ") , norm: " + accNorm;
				Log.d(TAG, msgTxt);
			}
			if(isFirstAccMeasure){
				firstAccTimestamp = ts;
				isFirstAccMeasure = false;
			}
			long timeDiff = ts - firstAccTimestamp;
			if(timeDiff > timeToInit){
				if(timeDiff < timeToFinish){
					//save values
					meanAccX = (meanAccX * numAccMeas + values[0]) / (numAccMeas + 1);
					meanAccY = (meanAccY * numAccMeas + values[1]) / (numAccMeas + 1);
					meanAccZ = (meanAccZ * numAccMeas + values[2]) / (numAccMeas + 1);
					numAccMeas++;
				}
				else{
					//calculate R matrix
					calculateRmatrix(meanAccX, meanAccY, meanAccZ);
					waitingForSetup = false;
				}
			}
		}
	}

	private void processGyroMeasure(long ts, long sysTs, int accuracy, float[] values){
		if(DEBUG) {
			String msgTxt = "gyro: (" + values[0] + "," + values[1] + "," + values[2] + ")";
			Log.d(TAG, msgTxt);
		}
		if(lastGyroTimestamp!=0) {
			long td = ts - lastGyroTimestamp;
			float dX = values[0]*td*NS2S;
			float dY = values[1]*td*NS2S;
			float dZ = values[2]*td*NS2S;
			float[] diffValues = new float[3];
			diffValues[0] = dX;
			diffValues[1] = dY;
			diffValues[2] = dZ;
			//Log.d(TAG,"adding gyro values: (" + dX + "," + dY + "," + dZ + ")");
			saveSensorLog("GYRO_DIFF", ts, sysTs, accuracy, diffValues);
			addGyroIncValues(dX, dY, dZ);
		}
		lastGyroTimestamp = ts;
	}

	private synchronized void addGyroIncValues(float dX, float dY, float dZ){
		gyroIncX += dX;
		gyroIncY += dY;
		gyroIncZ += dZ;
		gyroIncNum++;
	}

	private synchronized float[] getGyroIncValuesAndUpdate(){
		float incX = gyroIncX;
		float incY = gyroIncY;
		float incZ = gyroIncZ;
		float incNum = gyroIncNum;
		gyroIncX = 0;
		gyroIncY = 0;
		gyroIncZ = 0;
		gyroIncNum = 0;
		float[] out = new float[4];
		out[0] = incX;
		out[1] = incY;
		out[2] = incZ;
		out[3] = incNum;
		return out;
	}

	private void saveSensorLog(String sensor, long ts, long sysTs, int accuracy, float[] values){
		/** Code added to store data info into logs.txt **/
		if(storeLogs && logsRunning){
			mLogWriter.addSensorLog(sensor,ts, sysTs, accuracy,values);
		}
	}

}
