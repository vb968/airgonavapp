package com.ascamm.motion.mems;

import android.util.Log;

import com.ascamm.motion.utils.Constants;

public enum AccelerationState {
	WAITING,
	RISING,
	FALLING,
	NEGATIVE,
	STEP_DETECTED;

	private final static double MAX_TIME_STEP = (1/Constants.MIN_FREQ_WALKING)*1000; //milliseconds
	private final static double MIN_TIME_STEP = (1/Constants.MAX_FREQ_WALKING)*1000;; //milliseconds
	private final static double MIN_TIME_BETWEEN_STEPS = MIN_TIME_STEP; //milliseconds
	private final static double MARGIN = 0.05;
	private final static int MAX_PEAKS_FALLING = 2;
	private final static float MIN_PEAKS_DIFF = 1f;

	public AccelerationState process(float delta, float record, long sensorTimestamp){
		switch(this){
			case WAITING:
				if(delta > 0 && record >= MARGIN){
					StepCounter.setStart(sensorTimestamp) ;
					return RISING;
				}
				else{
					if(sensorTimestamp - StepCounter.getLastStepTime()>1500){
						StepCounter.setMode("STATIC");
					}
					return WAITING;
				}
			case RISING:
				if((sensorTimestamp - StepCounter.getStart()) > MAX_TIME_STEP){
					return WAITING;
				}
				else if(record >= MARGIN){
					if(delta < 0) {
						StepCounter.setHigh_peak(record);
						StepCounter.setNumPeaksFalling(0);
						StepCounter.setLastDelta(delta);
						return FALLING;
					}
					return RISING;
				}else{
					return WAITING;
				}
			case FALLING:
				if((sensorTimestamp - StepCounter.getStart()) > MAX_TIME_STEP){
					return WAITING;
				}
				else if (delta > 0 && record < -MARGIN){
					StepCounter.setLow_peak(record);
					return NEGATIVE;
				}
				else if (delta > 0 && record > -MARGIN){
					float lastDelta = StepCounter.getLastDelta();
					if (lastDelta < 0 ) {
						int numPeaksFalling = StepCounter.getNumPeaksFalling();
						//Log.d("AccelerationState", "numPeaksFalling: " + numPeaksFalling);
						if (numPeaksFalling >= MAX_PEAKS_FALLING) {
							//Log.d("AccelerationState", "maxPeaksFallingReached!!!");
							return WAITING;
						}
						StepCounter.setNumPeaksFalling(numPeaksFalling + 1);
					}
					StepCounter.setLastDelta(delta);
					return FALLING;
				}
				else{
					StepCounter.setLastDelta(delta);
					return FALLING;
				}
			case NEGATIVE:
				if(sensorTimestamp - StepCounter.getStart() > MAX_TIME_STEP){
					return WAITING;
				}
				else if(record >= -MARGIN){
					StepCounter.setDurationStep(sensorTimestamp -  StepCounter.getStart() );
					StepCounter.setDurationBetweenSteps(sensorTimestamp -  StepCounter.getLastStepTime());
					if ( StepCounter.getDurationBetweenSteps() >= MIN_TIME_BETWEEN_STEPS && StepCounter.getDurationStep() <= MAX_TIME_STEP &&  StepCounter.getDurationStep() >= MIN_TIME_STEP){
						//extra condition
						float peaskDiff = StepCounter.getHigh_peak() - StepCounter.getLow_peak();
						if(peaskDiff > MIN_PEAKS_DIFF) {
							StepCounter.increment();
							StepCounter.calculateStepLength();
							StepCounter.setMode("WALKING");
							return STEP_DETECTED;
						}
					}
					if ( StepCounter.getDurationBetweenSteps() < MIN_TIME_BETWEEN_STEPS  ||  StepCounter.getDurationStep() > MAX_TIME_STEP ||  StepCounter.getDurationStep() < MIN_TIME_STEP){
						StepCounter.setMode("IRREGULAR - TEXTING");
					}

					StepCounter.setLastStepTime(sensorTimestamp);
					return WAITING;
				}else{
					return NEGATIVE;
				}
			case STEP_DETECTED:
				return WAITING;
			default:
				return WAITING;

		}
	}



}
