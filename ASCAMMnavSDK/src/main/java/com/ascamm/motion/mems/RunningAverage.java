package com.ascamm.motion.mems;

/**
 * A class that implements running averages.
 */
public class RunningAverage
{
	double[] vals; // Values in the queue.
	int n = 0; // Current index in the queue.
	double average = 0; // Cached average.
	
	/**
	 * Construct a new running average.
	 * @param numRunningValues The number of values in the queue.
	 */
	RunningAverage (int numRunningValues) {
		vals = new double[numRunningValues];
	}
	
	/**
	 * Add a value to the running average.
	 * @param val The value to add.
	 */
	public void add (double val) {
		if (n == vals.length) {
			shiftLeft(vals);
			vals[n-1] = val;
		}
		else {
			vals[n] = val;
			n++;
		}
		average = computeAverage(vals, n);
	}
	
	static void shiftLeft (double[] vals) {
		for (int i = 0; i < vals.length-1; ++i) {
			vals[i] = vals[i+1];
		}
	}
	
	static double computeAverage (double[] vals, int n) {
		double avg = 0.0;
		for (int i = 0; i < n; ++i) avg += vals[i];
		return avg / (double) n;
	}
	
	/**
	 * Get the running average's value.
	 */
	public double getAverage () { return average; }
	
	@Override
	public String toString () { return String.valueOf(average); }
}
