package com.ascamm.motion.mems;

/**
 * Created by dfernandez on 16/12/16.
 */

public enum MotionMode {
    STATIC,
    WALKING,
    IRREGULAR_MOTION
}