package com.ascamm.motion.mems;

import android.util.Log;

/**
 * This class implements a one-dimensional Kalman Filter to be applied over the orientation coming from the INS
 * @author  Dani Fernandez EURECAT
 */ 

public class KalmanFilterOrientation {
	/* one-dimensional filter variables */

	private String TAG = getClass().getSimpleName();
	private boolean debug = true;

	//constants:
	private float r; //variance of the measurement noise
	private float q; //variance of the process noise
	//variables at each epoch
	private float x; //state scalar (filter output, i.e. filtered RSSI)
	private float p; //variance of the state scalar
	private float x_pred; //predicted state scalar
	private float p_pred; //predicted variance of the state scalar
	private float k; //Kalman gain


	/* initial values */
	//private float x_0;
	//private float p_0;
	private float r_0 = 0.9f;
	private float q_0 = 0.05f;

	public KalmanFilterOrientation(){
		//x = x_0;
		//p = p_0;
		r = r_0;
		q = q_0;
	}
	
	public float[] calculateOutput(float last_epoch_orientation, float last_epoch_p, float orientation, float userTrajectory){
		float[] kalman_output = {0,0}; 

		/*
		// prediction step
		x_pred = last_epoch_orientation + diffOrientation;
		p_pred = last_epoch_p + q;
	
		// update step
		k = p_pred / (p_pred + r);
		x = x_pred + k * (userTrajectory - x_pred);
		x = x % 360;
		p = (1 - k) * p_pred;
		*/

		x_pred = orientation;
		p_pred = last_epoch_p + q;

		/* update step */
		k = p_pred / (p_pred + r);
		x = x_pred + k * (userTrajectory - x_pred);
		x = x % 360;
		p = (1 - k) * p_pred;

		if(debug)	Log.d(TAG,"pred: (" + x_pred + "," + p_pred + ") , update: (" + k + "," + x + "," + p + ")");
				
		kalman_output[0] = x;
		kalman_output[1] = p;
		return kalman_output;
	}
}
