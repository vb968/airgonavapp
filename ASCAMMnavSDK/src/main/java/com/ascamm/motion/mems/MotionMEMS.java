package com.ascamm.motion.mems;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.FloatMath;
import android.util.Log;
import android.widget.Toast;

import com.ascamm.motion.utils.Constants;

public class MotionMEMS implements SensorEventListener {
	
	private boolean walking = false;
	
	private final String TAG = this.getClass().getSimpleName();
	SensorManager sensorManager;
	Sensor AccelSensor, GyroSensor;
	
	//linear accelerations from accel:
    private float[] accel = new float[3]; 
    private float[] lowPassFilteredAccel = new float[3];
    private float[] highPassFilteredAccel = new float[3];
	//angular speeds from gyro:
    private float[] gyro = new float[3];     
    private float[] lowPassFilteredGyro = new float[3];    
    private float[] highPassFilteredGyro = new float[3];
	
	//filters data:
    private float lowPass_a = 0.15f;
    private float highPass_a = 0.99f;
	//float[] gravity = new float[3];
    private float gravity = 9.81f;  //for accel
    private float dc = 0f; //for gyro
		
	//data processing structures:
	private LinkedList<Float> mQueueOfAccelSamples = new LinkedList<Float>();
	private LinkedList<Float> mQueueOfGyroSamples = new LinkedList<Float>();
	private LinkedList<float[]> mQueueOfGyroAxisSamples = new LinkedList<float[]>();
	private static int MAX_GYRO_BUFFER_SIZE = 1024;
	private int WINDOW_SIZE = 0;
	private int countAccel = 0;
	private int countGyro = 0;
	
	private float accelEnergy, accelVariance, gyroMean, gyroEnergy, gyroVariance;
	private float minGyroAxisEnergy, maxGyroAxisEnergy;

	private ArrayList<double[]> dominantAccelFreqs;
	private enum motionModes {
	    MODE_STATIC,
	    MODE_IRREGULAR_MOTION,
	    MODE_WALKING
	}
	private motionModes lastMotionMode = motionModes.MODE_STATIC;

	private static double ACCEL_ENERGY_THRESHOLD = 0.05;
	//private static float ACCEL_ENERGY_THRESHOLD = 0.09;
	private static double ACCEL_VARIANCE_DIFF_LOW_THRESHOLD = 0.6;
	private static double ACCEL_VARIANCE_DIFF_HIGH_THRESHOLD = 1.66; // =1/0.6
	private static double GYRO_VARIANCE_THRESHOLD = 0.05;
	private static double GYRO_ENERGY_LOW_THRESHOLD = 0.5;
	private static double GYRO_ENERGY_HIGH_THRESHOLD = 3;
	
	private FFT mFFT;
	
	// Used to track sensors sampling rate
	long lastTick = 0;
	long elapsed = 0;
	float sensorRate = 0;
	// Running averages used to compute the sensors sampling rate
	RunningAverage rt = new RunningAverage(TIME_RUNNING_AVERAGE_NUM_VALUES);
	// Running average queue size.
	static final int TIME_RUNNING_AVERAGE_NUM_VALUES = 5;

	Context mContext;

	private boolean waitingForSetup = true;
	private static int numMeasSetup = 300;
	private static int numMeasInit = 10;
	private int numMeas = 0;
	private long firstTimestamp;
	private long samplingRate = 0;

	//constructor
	public MotionMEMS(Context mContext){
		this.mContext = mContext;
		
		sensorManager = (SensorManager)mContext.getSystemService(Context.SENSOR_SERVICE);
	    AccelSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	    GyroSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
	   
	    mFFT = new FFT();
	}
	
	public void Start(){
		this.registerSensorListeners();
	}
	
	public void Stop(){
		this.unregisterSensorListeners();
	}
	
	public synchronized boolean getWalking(){
		return this.walking;
	}
	
	private void setWalking(boolean isWalking){
		this.walking = isWalking;
	}
	
	private void registerSensorListeners(){
		sensorManager.registerListener(this, AccelSensor , Constants.SENSOR_DELAY);
		sensorManager.registerListener(this, GyroSensor , Constants.SENSOR_DELAY);
	}
	
	private void unregisterSensorListeners(){
		sensorManager.unregisterListener(this);
	}
	
	@Override
	public void onSensorChanged(SensorEvent event) {
		Sensor mySensor = event.sensor;
		
		if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			
			/* for calculation of sensor sampling rate */
			/*
			long tick = SystemClock.elapsedRealtime();
			if(lastTick != 0){
				elapsed = Math.abs(tick-lastTick);
				rt.add(elapsed);
				sensorRate = (1f / (float)rt.getAverage()) * 1000;
				Log.d(TAG, "++++ accel rate: " + sensorRate + " Hz");
			}
			lastTick = tick;
			*/
						
			if(waitingForSetup){
				if(numMeas<numMeasSetup){
					if(numMeas==numMeasInit){
						firstTimestamp = event.timestamp;
					}
					numMeas++;
					//Log.d(TAG,"numMeas: " + numMeas + " , ts: " + event.timestamp);
				}
				else{
					long currentTimestamp = event.timestamp;
					long diffTs = currentTimestamp - firstTimestamp;
					long meanTs = diffTs/(numMeasSetup-numMeasInit); // nanoseconds
					samplingRate = 1000000000/meanTs; // Hz
					Log.d(TAG,"+++ meanTs: " + meanTs + " , samplingRate: " + samplingRate);
					if(samplingRate > 0 && samplingRate <= 1){
						samplingRate = 1;
					}
					else if(samplingRate > 1 && samplingRate <= 7.5){
						samplingRate = 5;
					}
					else if(samplingRate > 7.5 && samplingRate <= 15){
						samplingRate = 10;
					}
					else if(samplingRate > 15 && samplingRate <= 25){
						samplingRate = 20;
					}
					else if(samplingRate > 25 && samplingRate <= 75){
						samplingRate = 50;
					}
					else if(samplingRate > 75 && samplingRate <= 150){
						samplingRate = 100;
					}
					else if(samplingRate > 150 && samplingRate <= 250){
						samplingRate = 200;
					}
					float WINDOW_TIME = 1.28f;
					WINDOW_SIZE = (int) (samplingRate*WINDOW_TIME);
					Log.d(TAG,"+++ final samplingRate: " + samplingRate + " , WINDOW_SIZE: " + WINDOW_SIZE);
					//double[] orient = getOrientation();
					//lastStepOrientation = orient[0];
					//accumulatedOrientation = lastStepOrientation;
					//Log.d(TAG,"lastStepOrientation: " + lastStepOrientation);
					mFFT.setLength(WINDOW_SIZE);
					waitingForSetup = false;
				}
			}
			else {
				accel = event.values.clone();
				this.processingAccel(accel);
			}
	    }
		
	    if (mySensor.getType() == Sensor.TYPE_GYROSCOPE) {
	    	
			/* for calculation of sensor sampling rate */
	    	/*
			long tick = SystemClock.elapsedRealtime();
			if(lastTick != 0){
				elapsed = Math.abs(tick-lastTick);
				rt.add(elapsed);
				sensorRate = (1f / (float)rt.getAverage()) * 1000;
				Log.d(TAG, "++++ gyro rate: " + sensorRate + " Hz");
			}
			lastTick = tick;
			*/
			
			gyro = event.values.clone();
		    this.processingGyro(gyro);
	    }	    
	}
	 
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	 
	}
	
	private void processingAccel(float[] rawAccel){
		
		//Low pass filter (to filter the noise)
		/*
		float[] res = new float[2];
		for(int i=0;i<3;i++){
			res = mKalmanFilter.calculateOutput(last_accel_meas[i], last_accel_p[i], accel[i]);
			filtered_accel[i] = res[0];
			last_accel_p[i] = res[1];
			res = mKalmanFilter.calculateOutput(last_gyro_meas[i], last_gyro_p[i], gyro[i]);
			filtered_gyro[i] = res[0];
			last_gyro_p[i] = res[1];
		}
		*/
				
		//Log.d(TAG,"Raw Accel (" + String.format("%.2f", rawAccel[0]) + "," + String.format("%.2f", rawAccel[1]) + "," + String.format("%.2f", rawAccel[2]) + ")");
		//Low pass filter (to filter the noise)
		lowPassFilteredAccel = lowPass(rawAccel, lowPassFilteredAccel);

		float accel_norm = (float)Math.sqrt(lowPassFilteredAccel[0]*lowPassFilteredAccel[0] + lowPassFilteredAccel[1]*lowPassFilteredAccel[1] + lowPassFilteredAccel[2]*lowPassFilteredAccel[2]);
		float hp = highPassAccel(accel_norm);

		//put new sample into buffer
		if(mQueueOfAccelSamples.size() < WINDOW_SIZE){
			mQueueOfAccelSamples.add(hp);
		} else {
			while (mQueueOfAccelSamples.size() >= WINDOW_SIZE){
				mQueueOfAccelSamples.poll();
			}
			mQueueOfAccelSamples.add(hp);
		}
		

		//if count_samples == window_size/2 then we extract features and produce output (mode detection) 
		countAccel++;
		if(countAccel == WINDOW_SIZE/2){
			countAccel = 0;
			if(mQueueOfAccelSamples.size() >= WINDOW_SIZE){
				
				accelEnergy = signalEnergyAccelPow();
				accelVariance = signalVarianceAccel(signalEnergyAccel());
				//Log.d(TAG,"accelEnergyMEMS: " + accelEnergy);
				processGyroData();
					
				double[] arraySamplesRe = floatLinkedListToDoubleArray(mQueueOfAccelSamples);
				//double[] arraySamplesRe = generateSinusSignal(WINDOW_SIZE, 100, 2);
				double[] arraySamplesIm = new double[WINDOW_SIZE];
				for(int i=0; i<WINDOW_SIZE; i++){
					arraySamplesIm[i] = 0; 
				}
					
				mFFT.fft(arraySamplesRe, arraySamplesIm);
	
				double[] arrayFFTmagnitude = new double[WINDOW_SIZE];
				for(int i=0; i<WINDOW_SIZE; i++){
					arrayFFTmagnitude[i] = arraySamplesRe[i]*arraySamplesRe[i] + arraySamplesIm[i]*arraySamplesIm[i];
				}
				
				//peaks detection over the resulting fft, to detect the dominant freq�encies
				dominantAccelFreqs = this.findDominantFrequencies(arrayFFTmagnitude, samplingRate);
				
				this.motionModeRecognition();
			}
		}
	}
	
	private void processingGyro(float[] rawGyro){
		
		lowPassFilteredGyro = lowPass(rawGyro, lowPassFilteredGyro);
		float gyro_norm = (float)Math.sqrt(lowPassFilteredGyro[0]*lowPassFilteredGyro[0] + lowPassFilteredGyro[1]*lowPassFilteredGyro[1] + lowPassFilteredGyro[2]*lowPassFilteredGyro[2]);
		float hp = highPassGyro(gyro_norm);

		/*
		//put new sample into buffer
		if(mQueueOfGyroSamples.size() < WINDOW_SIZE){
			mQueueOfGyroSamples.add(hp);
		} else {
			while (mQueueOfGyroSamples.size() >= WINDOW_SIZE){
				mQueueOfGyroSamples.poll();
			}
			mQueueOfGyroSamples.add(hp);
		}
		
		//if count_samples == window_size/2 then we extract features and produce output (mode detection) 
		countGyro++;
		if(countGyro == WINDOW_SIZE/2){
			countGyro = 0;
			if(mQueueOfGyroSamples.size() >= WINDOW_SIZE){
					
				gyroEnergy = signalEnergyGyroPow();
				gyroVariance = signalVarianceGyro(signalEnergyGyro());
			}
		}
		*/

		addGyroSample(hp);
		addGyroAxisSample(rawGyro);
	}

	private synchronized void addGyroSample(float value){
		//put new sample into buffer
		if(mQueueOfGyroSamples.size() < MAX_GYRO_BUFFER_SIZE){
			mQueueOfGyroSamples.add(value);
		} else {
			while (mQueueOfGyroSamples.size() >= MAX_GYRO_BUFFER_SIZE){
				mQueueOfGyroSamples.poll();
			}
			mQueueOfGyroSamples.add(value);
		}
	}

	private synchronized void addGyroAxisSample(float[] values){
		//put new samples into buffer
		if(mQueueOfGyroAxisSamples.size() < MAX_GYRO_BUFFER_SIZE){
			mQueueOfGyroAxisSamples.add(values);
		} else {
			while (mQueueOfGyroAxisSamples.size() >= MAX_GYRO_BUFFER_SIZE){
				mQueueOfGyroAxisSamples.poll();
			}
			mQueueOfGyroAxisSamples.add(values);
		}
	}


	private synchronized void processGyroData(){
		float[] meanAndEnergy = signalMeanAndEnergyGyro();
		gyroMean = meanAndEnergy[0];
		gyroEnergy = meanAndEnergy[1];
		gyroVariance = signalVarianceGyro(gyroMean);
		double[] arraySamplesGyroRe = floatLinkedListToDoubleArray(mQueueOfGyroSamples);
		float[] meanAndEnergyAxis = signalMeanAndEnergyGyroAxis();
		float gyroMeanX = meanAndEnergyAxis[0];
		float gyroMeanY = meanAndEnergyAxis[1];
		float gyroMeanZ = meanAndEnergyAxis[2];
		float gyroEnergyX = meanAndEnergyAxis[3];
		float gyroEnergyY = meanAndEnergyAxis[4];
		float gyroEnergyZ = meanAndEnergyAxis[5];
		float[] gyroVarianceAxis = signalVarianceGyroAxis(gyroMeanX, gyroMeanY, gyroMeanZ);
		float gyroVarianceX = gyroVarianceAxis[0];
		float gyroVarianceY = gyroVarianceAxis[1];
		float gyroVarianceZ = gyroVarianceAxis[2];


		mQueueOfGyroSamples.clear();
		mQueueOfGyroAxisSamples.clear();
		//calculate min and max of gyro axis
		float minCandidate;
		float maxCandidate;
		if(gyroEnergyX<=gyroEnergyY){
			minCandidate = gyroEnergyX;
			maxCandidate = gyroEnergyY;
		}
		else{
			minCandidate = gyroEnergyY;
			maxCandidate = gyroEnergyX;
		}
		if(gyroEnergyZ<minCandidate){
			minGyroAxisEnergy = gyroEnergyZ;
		}
		else{
			minGyroAxisEnergy = minCandidate;
		}
		if(gyroEnergyZ>maxCandidate){
			maxGyroAxisEnergy = gyroEnergyZ;
		}
		else{
			maxGyroAxisEnergy = maxCandidate;
		}
	}
	
	private float[] lowPass( float[] input, float[] output ) {
	    if ( output == null ) return input;     
	    for ( int i=0; i<input.length; i++ ) {
	        output[i] = output[i] + lowPass_a * (input[i] - output[i]);
	    }
	    return output;
	}
	
	private float highPassAccel(float x) {
		gravity = highPass_a * gravity + (1 - highPass_a) * x;
		float filteredValue = x - gravity;
		return filteredValue;
	}
	
	private float highPassGyro(float x) {
		dc = highPass_a * dc + (1 - highPass_a) * x;
		float filteredValue = x - dc;
		return filteredValue;
	}

	
	private float[] floatLinkedListToFloatArray(LinkedList<Float> floatList){
		float[] floatArray = new float[floatList.size()];
		int i = 0;

		for (Float f : floatList) {
		    floatArray[i++] = (f != null ? f : Float.NaN); 
		}
		return floatArray;
	}
	
	private double[] floatLinkedListToDoubleArray(LinkedList<Float> floatList){
		double[] doubleArray = new double[floatList.size()];
		int i = 0;

		for (Float f : floatList) {
		    doubleArray[i++] = (double)(f != null ? f : Float.NaN); 
		}
		return doubleArray;
	}
	
	private float signalEnergyAccel(){
		float sum = 0f;
		int count = 0;
		for (int i=0; i<mQueueOfAccelSamples.size(); i++){			
			sum += mQueueOfAccelSamples.get(i);
			count++;
		}
		return sum / count;
	}
	
	private float signalEnergyAccelPow(){
		float sum = 0f;
		int count = 0;
		for (int i=0; i<mQueueOfAccelSamples.size(); i++){			
			sum += mQueueOfAccelSamples.get(i) * mQueueOfAccelSamples.get(i);
			count++;
		}
		return sum / count;
	}
	
	private float signalVarianceAccel(float avg){
		float sum = 0f;
		int count = 0;
		for (int i=0; i<mQueueOfAccelSamples.size(); i++){				
			sum += Math.pow(mQueueOfAccelSamples.get(i) - avg, 2);
			count++;
		}
		return sum / count;
	}

	private float[] signalMeanAndEnergyGyro(){
		float sumMean = 0f, sumEnergy = 0f;
		int count = 0;
		for (int i=0; i<mQueueOfGyroSamples.size(); i++){
			sumMean += mQueueOfGyroSamples.get(i);
			sumEnergy += Math.pow(mQueueOfGyroSamples.get(i),2);
			count++;
		}
		//Log.d("hpa", "SUM IS " + sum+ "COUNTER " + count);
		float[] result = new float[2];
		result[0] = sumMean / count;
		result[1] = sumEnergy / count;
		return result;
	}

	private float signalVarianceGyro(float avg){
		float sum = 0f;
		int count = 0;
		for (int i=0; i<mQueueOfGyroSamples.size(); i++){
			sum += Math.pow(mQueueOfGyroSamples.get(i) - avg, 2);
			count++;
		}
		return sum / count;
	}

	private float[] signalMeanAndEnergyGyroAxis(){
		float sumMeanX = 0f, sumMeanY = 0f, sumMeanZ = 0f, sumEnergyX = 0f, sumEnergyY = 0f, sumEnergyZ = 0f;
		int count = 0;
		for (int i=0; i<mQueueOfGyroAxisSamples.size(); i++){
			sumMeanX += mQueueOfGyroAxisSamples.get(i)[0];
			sumMeanY += mQueueOfGyroAxisSamples.get(i)[1];
			sumMeanZ += mQueueOfGyroAxisSamples.get(i)[2];
			sumEnergyX += Math.pow(mQueueOfGyroAxisSamples.get(i)[0],2);
			sumEnergyY += Math.pow(mQueueOfGyroAxisSamples.get(i)[1],2);
			sumEnergyZ += Math.pow(mQueueOfGyroAxisSamples.get(i)[2],2);
			count++;
		}
		float[] meanAndEnergy = new float[6];
		meanAndEnergy[0] = sumMeanX / count;
		meanAndEnergy[1] = sumMeanY / count;
		meanAndEnergy[2] = sumMeanZ / count;
		meanAndEnergy[3] = sumEnergyX / count;
		meanAndEnergy[4] = sumEnergyY / count;
		meanAndEnergy[5] = sumEnergyZ / count;
		return meanAndEnergy;
	}

	private float[] signalVarianceGyroAxis(float avgX, float avgY, float avgZ){
		float sumX = 0f, sumY = 0f, sumZ = 0f;
		int count = 0;
		for (int i=0; i<mQueueOfGyroAxisSamples.size(); i++){
			sumX += Math.pow(mQueueOfGyroAxisSamples.get(i)[0] - avgX, 2);
			sumY += Math.pow(mQueueOfGyroAxisSamples.get(i)[1] - avgY, 2);
			sumZ += Math.pow(mQueueOfGyroAxisSamples.get(i)[2] - avgZ, 2);
			count++;
		}
		float[] variance = new float[3];
		variance[0] = sumX / count;
		variance[1] = sumY / count;
		variance[2] = sumZ / count;
		return variance;
	}


	private ArrayList<double[]> findDominantFrequencies(double[] arr, long samplingRate){
		//find local maximums in the fft result array:
		ArrayList<double[]> localMax = new ArrayList<double[]>();
		for (int i=1; i<(arr.length / 2); i++){		//only loop half of the array (the rest is a reflex), starting from pos two		
			if (arr[i] >= arr[i-1] && arr[i] >= arr[i+1] ){				
				localMax.add(new double[] {i,arr[i]});
			}
		}
		
		//select the "dominant" local maximums, i.e. the maximums above a certain threshold relative to the maximum magnitude
		Collections.sort(localMax, new SortByFreqMagnitude());
		double MaxFreqMagnitude = localMax.get(0)[1];
		int j = 0;
		boolean done = false;
		ArrayList<double[]> dominantFreqs = new ArrayList<double[]>();
		while ((localMax.size() > j) && (done==false)){
			if (localMax.get(j)[1] > (0.5)*MaxFreqMagnitude){
			//if (localMax.get(j)[1] > (0.25)*MaxFreqMagnitude){
				dominantFreqs.add(new double[] {Double.valueOf(samplingRate * (localMax.get(j)[0]/arr.length)),localMax.get(j)[1]}); //convert to frequency value (Hz)
			}
			else{
				//the magnitude is below the threshold, so we stop since they are sorted desc by magnitude
				done = true;
			}
			j++;
		}
		Collections.sort(dominantFreqs, new SortByFreq());
		return dominantFreqs;
	}
	
	void motionModeRecognition(){
		//accelEnergy, accelVariance, gyroEnergy, gyroVariance;
		motionModes currentMotionMode = this.lastMotionMode;
				
		if (this.accelEnergy > ACCEL_ENERGY_THRESHOLD){
			//is not static
			if ((dominantAccelFreqs.size() <= 2)){
				if ((dominantAccelFreqs.get(0)[0] > 0.7) && (dominantAccelFreqs.get(0)[0] < 2)){
					/*
					//it seems that we are walking
					//if (this.lastMotionMode == motionModes.MODE_WALKING){
						//a little bit of hysteresis for detecting that we are walking
						currentMotionMode = motionModes.MODE_WALKING;
						
						/********************/
						this.setWalking(true);
						/********************//*
					//}
					this.lastMotionMode = motionModes.MODE_WALKING;
					//motionModes lastMotionMode = motionModes.MODE_STATIC;
					*/
					if(minGyroAxisEnergy<GYRO_ENERGY_LOW_THRESHOLD && maxGyroAxisEnergy<GYRO_ENERGY_HIGH_THRESHOLD){
						currentMotionMode = motionModes.MODE_WALKING;
						/********************/
						this.setWalking(true);
						/********************/
						this.lastMotionMode = motionModes.MODE_WALKING;
					}
					else{
						currentMotionMode = motionModes.MODE_IRREGULAR_MOTION;
						/********************/
						this.setWalking(false);
						/********************/
						Log.d(TAG,"IRREGULAR 3");
						this.lastMotionMode = motionModes.MODE_IRREGULAR_MOTION;
					}
				}else{
					//irregular motion
					currentMotionMode = motionModes.MODE_IRREGULAR_MOTION;
					this.lastMotionMode = motionModes.MODE_IRREGULAR_MOTION;
					/********************/
					this.setWalking(false);
					/********************/
					//Toast.makeText(mContext, "IRREGULAR 1", Toast.LENGTH_SHORT).show();
				}
			}else{
				//irregular motion
				currentMotionMode = motionModes.MODE_IRREGULAR_MOTION;
				this.lastMotionMode = motionModes.MODE_IRREGULAR_MOTION;
				/********************/
				this.setWalking(false);
				/********************/
				//Toast.makeText(mContext, "IRREGULAR 2", Toast.LENGTH_SHORT).show();
			}
			
		}else{
			//static
			currentMotionMode = motionModes.MODE_STATIC;
			this.lastMotionMode = motionModes.MODE_STATIC;
			
			/********************/
			this.setWalking(false);
			/********************/
		}
	}
	
	/*nested class for sorting object by freq magnitude IN DESC ORDER (signs of the 'one' changed with respect to ASCENDENT order)*/
	public class SortByFreqMagnitude implements Comparator<double[]>{
		public int compare(double[] arg0, double[] arg1) {
			if ( arg0[1] < arg1[1]){
				return 1;
			} else if ( arg0[1] == arg1[1] ){
				return 0;	
			} else if ( arg0[1] > arg1[1]){
				return -1;
			}
			return 0;
		}
	}
	
	/*nested class for sorting object by freq IN ASC ORDER */
	public class SortByFreq implements Comparator<double[]>{
		public int compare(double[] arg0, double[] arg1) {
			if ( arg0[0] < arg1[0]){
				return -1;
			} else if ( arg0[0] == arg1[0] ){
				return 0;	
			} else if ( arg0[0] > arg1[0]){
				return 1;
			}
			return 0;
		}
	}

}
