package com.ascamm.motion.mems;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

import android.content.Context;

import com.ascamm.motion.utils.Constants;
import com.ascamm.motion.utils.LogWriter;
import com.ascamm.motion.utils.MotionStepsInfoListener;
import com.ascamm.motion.utils.Settings;
import com.ascamm.utils.Point3D;
import com.ascamm.motion.utils.Point3DWithFloorAndEdge;

import android.hardware.SensorEventListener;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

public class MotionSTEPS implements SensorEventListener {
	
	private final String TAG = this.getClass().getSimpleName();

	private boolean debug = Constants.DEBUGMODE;

	private SensorManager sensorManager;
	private Sensor AccelSensor, GyroSensor;
	private boolean waitForTop=true;
	private boolean waitForDown=false;
	private double topFreq;
	private double minFreq;

	//filters data:
	private static final float HIGH_PASS_A = 0.99f;
	private float gravity = 9.81f;  //for accel
	private float dc = 0f; //for gyro
	//linear accelerations from accel:
    private float[] accel = new float[3]; 
	//angular speeds from gyro:
    private float[] gyro = new float[3];

	//Real High-Pass filter data:
	private final static float REAL_HIGH_PASS_A = 0.99f;
	private final static float REAL_HIGH_PASS_A_STEPS = 0.99f;
	private boolean areFirstAccelMeas = true;
	private float[] lastAccelMeas = new float[3];
	private float[] lastAccelMeasFiltered = new float[3];

	//private float[] lowPassFilteredAccel = new float[3];
	private float[] lowPassFilteredAccel;
	private float[] lowPassFilteredGyro = new float[3];
	private float[] highPassFilteredGyro = new float[3];

	private float lastAccelNorm = -1;
	private float lastAccelNormFiltered = -1;

	private float lastAccelStepsNorm = -1;
	private float lastAccelStepsNormFiltered = -1;

	private float[] lowPassFilteredAccel_steps;

	//private FIRfilter normFIRfilter;
	private FIRfilter accelxFIRfilter;
	private FIRfilter accelyFIRfilter;
	private FIRfilter accelzFIRfilter;

	//data processing structures:
	private LinkedList<Float> mQueueOfAccelSamples = new LinkedList<Float>();
	private LinkedList<float[]> mQueueOfAccelAxisSamples = new LinkedList<float[]>();
	private LinkedList<Float> mQueueOfStepsAccelSamples = new LinkedList<Float>();
	private LinkedList<Float> mQueueOfGyroSamples = new LinkedList<Float>();
	private LinkedList<float[]> mQueueOfGyroAxisSamples = new LinkedList<float[]>();
	private static int MAX_GYRO_BUFFER_SIZE = 1024;
	private int WINDOW_SIZE = 0;
	private int countAccel = 0;


	/*********** Motion Mode Threshold variables *************/
	private static final double ACCEL_ENERGY_LOW_THRESHOLD = 0.05;
	private static final double ACCEL_ENERGY_HIGH_THRESHOLD = 3;
	//private static final double ACCEL_ENERGY_THRESHOLD = 0.09;
	private static final double GYRO_ENERGY_LOW_THRESHOLD = 0.5;
	private static final double GYRO_ENERGY_HIGH_THRESHOLD = 3;
	private static final double TOTAL_DIFF_THRESHOLD = Math.PI/4;
	private static final double ROLL_DIFF_THRESHOLD = Math.PI/9;
	private static final double PITCH_DIFF_THRESHOLD = Math.PI/9;
	private static final int MAX_NUM_DOMINANT_FREQS = 2;
	private static final double MIN_FREQ_WALKING = Constants.MIN_FREQ_WALKING; // Hz
	private static final double MAX_FREQ_WALKING = Constants.MAX_FREQ_WALKING; // Hz
	private FFT mFFT;

	//Integration of new steps method:
	private float deltaFilteredNorm = 0;
	AccelerationState state = AccelerationState.WAITING;
	//End integration of new steps method:

	/*Steps detection variables*/
	private static final double STEP_LENGTH_COEFFICIENT=0.50; // old steps method (default: 0.60)
	public ArrayList<Double> mStepsList = new ArrayList<Double>();


	/*************** Orientation ***************/
	private enum OrientationType {
		magnetometer,
		geodetic,
		relative
	}
	private OrientationType orientationMethod = OrientationType.relative;

	//Adapt navigation to aisles scenario
	private boolean useAisleAdapter=false;

 	private Orientation orientationCalculator;
 	private GeodeticOrientation GeodeticOrientation;
	private RelativeOrientation relativeOrientation;

	private double orient;
	private double diffOrient;
	private double lastOrientation;
	private float pxMeter;
	private double mHeadingBias = 0;
	private double mHeadingBias_raw = 0;
	private long mTimeStampHeadingBias = 0;
	//private float lowPass_b = 0.15f;
	private float lowPass_b = 0.03f;

	//Temporal Hard Coded Variables
	//private double mapAngleToNorth =0;
	//private double mapAngleToNorth = 360 - 316.3; //casa batllo
	//private double mapAngleToNorth = 18.7; //lab ascamm
	//private double mapAngleToNorth = 109.04; //oficines eurecat
	private double mapAngleToNorth = 108.7; //Eurecat Proves


	/** Code added for logs **/
	private LogWriter mMeasurementsLogWriter;
	private LogWriter mFFTLogWriter;
	private LogWriter mStepsOrientationLogWriter;
	private LogWriter mAccelStatesLogWriter;
	private boolean storeLogs = Settings.storeLogs;
	//private boolean storeLogs = true;
	private boolean logsRunning = false;
	private String logsMeasurementsFilename = Settings.logsFilename + "_mems_measurements" + Constants.logsFilenameExt;
	private String logsFFTFilename = Settings.logsFilename + "_mems_fft" + Constants.logsFilenameExt;
	private String logsStepsOrientationFilename = Settings.logsFilename + "_steps_orientation" + Constants.logsFilenameExt;
	private String logsAccelStatesFilename = Settings.logsFilename + "_accel_states" + Constants.logsFilenameExt;


	/********** Internal class variables *********/
	private Context mContext;

	private ArrayList<MotionStepsInfoListener> mMotionStepsInfoListeners = new ArrayList<MotionStepsInfoListener>();
	private ArrayList<MotionStepsInfoListener> mMotionStepsInfoListenersMainThread = new ArrayList<MotionStepsInfoListener>();

	private boolean waitingForSetup = true;
	private static int numMeasSetup = 300;
	private static int numMeasInit = 10;
	private int numMeas = 0;
	private long firstTimestamp;
	private long samplingRate = 0;

	private boolean walking = false;
	private int motionModeOrdinal = 0;

	private DecimalFormat decFor = new DecimalFormat("#.00");

	private long lastTsWindow = 0;

	//constructor
	public MotionSTEPS(Context mContext,float pxMeter){
		this.pxMeter=pxMeter;
		this.mContext = mContext;
		
		sensorManager = (SensorManager)mContext.getSystemService(Context.SENSOR_SERVICE);
	    AccelSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		/*
		if(AccelSensor != null) {
			Log.d(TAG, "FifoMaxEventCount: " + AccelSensor.getFifoMaxEventCount());
			Log.d(TAG, "FifoReservedEventCount: " + AccelSensor.getFifoReservedEventCount());
			Log.d(TAG, "MaxRange: " + AccelSensor.getMaximumRange());
			Log.d(TAG, "MinDelay: " + AccelSensor.getMinDelay());
			Log.d(TAG, "Name: " + AccelSensor.getName());
			Log.d(TAG, "Power: " + AccelSensor.getPower());
			Log.d(TAG, "Resolution: " + AccelSensor.getResolution());
			Log.d(TAG, "Type: " + AccelSensor.getType());
			Log.d(TAG, "Vendor: " + AccelSensor.getVendor());
			Log.d(TAG, "Version: " + AccelSensor.getVersion());
		}
		*/
	    GyroSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

	    mFFT = new FFT();
	    
		//Orientation ****/
 		orientationCalculator = new Orientation(mContext);
 		//GeodeticOrientation = new GeodeticOrientation(sensorManager);
		relativeOrientation = new RelativeOrientation(mContext);
		//normFIRfilter = new FIRfilter();
		accelxFIRfilter = new FIRfilter();
		accelyFIRfilter = new FIRfilter();
		accelzFIRfilter = new FIRfilter();
	}

	public void Start(){
		/** Code added to store data info into logs.txt **/
		if(storeLogs){
			mMeasurementsLogWriter = new LogWriter(Constants.APP_DATA_FOLDER_NAME, logsMeasurementsFilename);
			mFFTLogWriter = new LogWriter(Constants.APP_DATA_FOLDER_NAME, logsFFTFilename);
			mStepsOrientationLogWriter = new LogWriter(Constants.APP_DATA_FOLDER_NAME, logsStepsOrientationFilename);
			mAccelStatesLogWriter = new LogWriter(Constants.APP_DATA_FOLDER_NAME, logsAccelStatesFilename);

			if((mMeasurementsLogWriter.startLogs()>0) && (mFFTLogWriter.startLogs()>0) && (mStepsOrientationLogWriter.startLogs()>0) && (mAccelStatesLogWriter.startLogs()>0)){
				logsRunning = true;
			}
		}

		this.registerSensorListeners();
		this.orientationCalculator.StartOrientation();
		this.relativeOrientation.Start();
		//normFIRfilter.init();
		accelxFIRfilter.init();
		accelyFIRfilter.init();
		accelzFIRfilter.init();
	}

	public void Stop(){
		this.unregisterSensorListeners();
		this.orientationCalculator.StopOrientation();
		this.relativeOrientation.Stop();

		/** Code added to store data info into logs.txt **/
		if(storeLogs && logsRunning){
			mMeasurementsLogWriter.stopLogs();
			mFFTLogWriter.stopLogs();
			mStepsOrientationLogWriter.stopLogs();
			mAccelStatesLogWriter.stopLogs();
			logsRunning = false;
		}
	}

	public void setMapAngleToNorth(double mapAngleToNorth){
		this.mapAngleToNorth = mapAngleToNorth;
	}

	public synchronized double getLastOrientation() {
		return lastOrientation;
	}

	public synchronized void setLastOrientation(double lastOrientation) {
		this.lastOrientation = lastOrientation;
	}

	public synchronized boolean getWalking(){
		return this.walking;
	}

	private synchronized void setWalking(boolean isWalking){
		this.walking = isWalking;
	}

	/* method to set PDR data once sensor data has been processed */
	private synchronized void setStepsData(int motionModeOrdinal, ArrayList<Double> stepsList, double orientation, double diffOrientation){
		this.motionModeOrdinal = motionModeOrdinal;
		this.mStepsList = (ArrayList<Double>) stepsList.clone();
		this.orient = orientation;
		this.diffOrient = diffOrientation;
	}

	public synchronized int getMotionMode(){
		return motionModeOrdinal;
	}

	public synchronized ArrayList<Double> getStepsList(){
		return mStepsList;
	}

	public synchronized double getAbsoluteOrientation(){
		return orient;
	}

	public synchronized double getDiffOrientation(){
		return diffOrient;
	}
	
	private void registerSensorListeners(){
	    sensorManager.registerListener(this, AccelSensor , Constants.SENSOR_DELAY);
	    sensorManager.registerListener(this, GyroSensor , Constants.SENSOR_DELAY);
	}
	
	private void unregisterSensorListeners(){
		sensorManager.unregisterListener(this);
	}

	
	@Override
	public void onSensorChanged(SensorEvent event) {
		Sensor mySensor = event.sensor;
		//int accuracy = event.accuracy;

		if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			if(waitingForSetup){
				if(numMeas<numMeasSetup){
					if(numMeas==numMeasInit){
						firstTimestamp = event.timestamp;
					}
					numMeas++;
					//Log.d(TAG,"numMeas: " + numMeas + " , ts: " + event.timestamp);
				}
				else{
					long currentTimestamp = event.timestamp;
					long diffTs = currentTimestamp - firstTimestamp;
					long meanTs = diffTs/(numMeasSetup-numMeasInit); // nanoseconds
					samplingRate = 1000000000/meanTs; // Hz
					Log.d(TAG,"+++ meanTs: " + meanTs + " , samplingRate: " + samplingRate);
					if(samplingRate > 0 && samplingRate <= 1){
						samplingRate = 1;
					}
					else if(samplingRate > 1 && samplingRate <= 7.5){
						samplingRate = 5;
					}
					else if(samplingRate > 7.5 && samplingRate <= 15){
						samplingRate = 10;
					}
					else if(samplingRate > 15 && samplingRate <= 25){
						samplingRate = 20;
					}
					else if(samplingRate > 25 && samplingRate <= 75){
						samplingRate = 50;
					}
					else if(samplingRate > 75 && samplingRate <= 150){
						samplingRate = 100;
					}
					else if(samplingRate > 150 && samplingRate <= 250){
						samplingRate = 200;
					}
					float WINDOW_TIME = 1.28f;
					WINDOW_SIZE = (int) (samplingRate*WINDOW_TIME);
					Log.d(TAG,"+++ final samplingRate: " + samplingRate + " , WINDOW_SIZE: " + WINDOW_SIZE);
					//double[] orient = getOrientation();
					//lastStepOrientation = orient[0];
					//accumulatedOrientation = lastStepOrientation;
					//Log.d(TAG,"lastStepOrientation: " + lastStepOrientation);
					mFFT.setLength(WINDOW_SIZE);
					waitingForSetup = false;
				}
			}
			else {
				accel = event.values.clone();
				this.processingAccel(accel, event.timestamp);
			}
	    }
		else if (mySensor.getType() == Sensor.TYPE_GYROSCOPE) {
			gyro = event.values.clone();
			this.processingGyro(gyro, event.timestamp);
	    }
	}
	 
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		String sensorName = sensor.getName();
		String accuracyStr = "";
		if(sensor.getType() == Sensor.TYPE_ACCELEROMETER){
			sensorName = "ACCELEROMETER";
		}
		else if(sensor.getType() == Sensor.TYPE_GYROSCOPE){
			sensorName = "GYROSCOPE";
		}
		if(accuracy == SensorManager.SENSOR_STATUS_ACCURACY_LOW){
			accuracyStr = "LOW";
		}
		else if(accuracy == SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM){
			accuracyStr = "MEDIUM";
		}
		else if(accuracy == SensorManager.SENSOR_STATUS_ACCURACY_HIGH){
			accuracyStr = "HIGH";
		}
		else if(accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE){
			accuracyStr = "UNRELIABLE";
		}
		//Log.d(TAG,"onAccuracyChanged. (" + sensorName + " , " + accuracyStr + ")");
	}
	
	private void processingAccel(float[] rawAccel, long sensorTs){
		/*
		// option1: different filters for Motion Mode and Steps Detection
		PreProcessing preproc_fft = new PreProcessing(rawAccel, null, lowPassFilteredAccel, null, false, false);
		PreProcessing preproc_steps = new PreProcessing(rawAccel, null, lowPassFilteredAccel_steps, null, true, false);
		this.lowPassFilteredAccel = preproc_fft.getLowPassFilteredAccel();
		this.lowPassFilteredAccel_steps = preproc_steps.getLowPassFilteredAccel();
		*/
		// option2: same FIR filter for Motion Mode and Steps Detection
		double accelXfiltered = accelxFIRfilter.applyFilter(rawAccel[0]);
		double accelYfiltered = accelyFIRfilter.applyFilter(rawAccel[1]);
		double accelZfiltered = accelzFIRfilter.applyFilter(rawAccel[2]);
		if(this.lowPassFilteredAccel == null){
			this.lowPassFilteredAccel = new float[3];
		}
		this.lowPassFilteredAccel[0] = (float) accelXfiltered;
		this.lowPassFilteredAccel[1] = (float) accelYfiltered;
		this.lowPassFilteredAccel[2] = (float) accelZfiltered;
		if(this.lowPassFilteredAccel_steps == null){
			this.lowPassFilteredAccel_steps = new float[3];
		}
		this.lowPassFilteredAccel_steps[0] = (float) accelXfiltered;
		this.lowPassFilteredAccel_steps[1] = (float) accelYfiltered;
		this.lowPassFilteredAccel_steps[2] = (float) accelZfiltered;

		float[] rawAccelGlobal = relativeOrientation.convertFromLocalToGlobal(rawAccel[0], rawAccel[1], rawAccel[2]);
		float[] lowPassAccelGlobal = relativeOrientation.convertFromLocalToGlobal(lowPassFilteredAccel[0], lowPassFilteredAccel[1], lowPassFilteredAccel[2]);
		float[] lowPassAccelStepsGlobal = relativeOrientation.convertFromLocalToGlobal(lowPassFilteredAccel_steps[0], lowPassFilteredAccel_steps[1], lowPassFilteredAccel_steps[2]);
		/*
		// option1
		float accel_norm = preproc_fft.getAccel_norm();
		float accel_norm_steps = preproc_steps.getAccel_norm();
		//float accel_norm_steps = lowPassAccelStepsGlobal[2];

		double accelNorm = Math.sqrt(Math.pow(rawAccel[0],2) + Math.pow(rawAccel[1],2) + Math.pow(rawAccel[2],2));
		double accelNormFiltered = normFIRfilter.applyFilter(accelNorm);

		accel_norm_steps = (float) accelNormFiltered;
		*/
		// option2
		float accel_norm = (float) Math.sqrt(Math.pow(lowPassFilteredAccel[0],2) + Math.pow(lowPassFilteredAccel[1],2) + Math.pow(lowPassFilteredAccel[2],2));
		//float accel_norm_steps = accel_norm; // acceleration norm
		float accel_norm_steps = lowPassAccelStepsGlobal[2]; // Z component after rotation
		//float accel_norm_steps = this.lowPassFilteredAccel_steps[2]; // Z component without rotation

		float hp;
		float hp_steps;
		if(areFirstAccelMeas){
			hp = accel_norm;
			hp_steps = accel_norm_steps;
			areFirstAccelMeas = false;
		}
		else {
			float[] values = new float[1];
			values[0] = accel_norm;
			float[] lastValues = new float[1];
			lastValues[0] = lastAccelNorm;
			float[] lastValuesFiltered = new float[1];
			lastValuesFiltered[0] = lastAccelNormFiltered;
			float[] valuesFiltered = realHighPass(values, lastValues, lastValuesFiltered, REAL_HIGH_PASS_A);
			hp = valuesFiltered[0];

			float[] valuesSteps = new float[1];
			valuesSteps[0] = accel_norm_steps;
			float[] lastValuesSteps = new float[1];
			lastValuesSteps[0] = lastAccelStepsNorm;
			float[] lastValuesStepsFiltered = new float[1];
			lastValuesStepsFiltered[0] = lastAccelStepsNormFiltered;
			float[] valuesStepsFiltered = realHighPass(valuesSteps, lastValuesSteps, lastValuesStepsFiltered, REAL_HIGH_PASS_A_STEPS);
			hp_steps = valuesStepsFiltered[0];
			deltaFilteredNorm = hp_steps - lastAccelStepsNormFiltered;
		}
		//Log.d(TAG, "hp: " + hp + " , hp_steps: " + hp_steps);
		lastAccelNorm = accel_norm;
		lastAccelNormFiltered = hp;

		lastAccelStepsNorm = accel_norm_steps;
		lastAccelStepsNormFiltered = hp_steps;

		/*
		int numValues = rawAccel.length;
		float[] rawAccelFiltered;
		if(areFirstAccelMeas){
			rawAccelFiltered = new float[numValues];
			for (int i = 0; i < numValues; i++) {
				rawAccelFiltered[i] =rawAccel[i];
			}
			lastAccelMeas = new float[numValues];
			lastAccelMeasFiltered = new float[numValues];
			areFirstAccelMeas = false;
		}
		else {
			rawAccelFiltered = realHighPass(rawAccel, lastAccelMeas, lastAccelMeasFiltered);
		}
		for (int i = 0; i < numValues; i++) {
			lastAccelMeas[i] = rawAccel[i];
			lastAccelMeasFiltered[i] = rawAccelFiltered[i];
		}
		*/

		float[] highPassFilteredAccel = new float[3];
		highPassFilteredAccel[0]=hp;
	    highPassFilteredAccel[1]=hp;
		highPassFilteredAccel[2]=hp;

		float[] highPassFilteredAccelSteps = new float[3];
		highPassFilteredAccelSteps[0]=hp_steps;
		highPassFilteredAccelSteps[1]=hp_steps;
		highPassFilteredAccelSteps[2]=hp_steps;

		long ts = System.currentTimeMillis();
		if(storeLogs){
			mMeasurementsLogWriter.addMeasurementsLog(ts,"accel",rawAccel,lowPassFilteredAccel,highPassFilteredAccel,accel_norm,rawAccelGlobal,lowPassAccelGlobal, sensorTs);
			mMeasurementsLogWriter.addMeasurementsLog(ts,"accel_steps",rawAccel,lowPassFilteredAccel_steps,highPassFilteredAccelSteps,accel_norm_steps,rawAccelGlobal,lowPassAccelStepsGlobal, sensorTs);
		}

		//put new sample into buffer
		if(mQueueOfAccelSamples.size() < WINDOW_SIZE){
			mQueueOfAccelSamples.add(hp);
		} else {
			while (mQueueOfAccelSamples.size() >= WINDOW_SIZE){
				mQueueOfAccelSamples.poll();
			}
			mQueueOfAccelSamples.add(hp);
		}

		//put new samples into buffer
		if(mQueueOfAccelAxisSamples.size() < WINDOW_SIZE){
			mQueueOfAccelAxisSamples.add(rawAccel);
		} else {
			while (mQueueOfAccelAxisSamples.size() >= WINDOW_SIZE){
				mQueueOfAccelAxisSamples.poll();
			}
			mQueueOfAccelAxisSamples.add(rawAccel);
		}

		//put new sample into steps buffer
		if(mQueueOfStepsAccelSamples.size() < WINDOW_SIZE){
			mQueueOfStepsAccelSamples.add(hp_steps);
		} else {
			while (mQueueOfStepsAccelSamples.size() >= WINDOW_SIZE){
				mQueueOfStepsAccelSamples.poll();
			}
			mQueueOfStepsAccelSamples.add(hp_steps);
		}

		// Send data to process step detection
		state = state.process(deltaFilteredNorm, hp_steps, sensorTs/1000000);

		if(storeLogs){
			int accelState = -5;
			if(state == AccelerationState.RISING){
				accelState = 1;
			}
			else if(state == AccelerationState.WAITING){
				accelState = 0;
			}
			else if(state == AccelerationState.FALLING){
				accelState = -1;
			}
			else if(state == AccelerationState.NEGATIVE){
				accelState = -2;
			}
			else if(state == AccelerationState.STEP_DETECTED){
				accelState = -3;
			}
			mAccelStatesLogWriter.addAccelState(ts, accelState);
		}

		//if count_samples == window_size/2 then we extract features and produce output (mode detection)
		countAccel++;
		//Log.d(TAG,"Count accel samples: " + countAccel);
		//Log.d(TAG,"Size Queue: " + mQueueOfAccelSamples.size());
		if(countAccel == WINDOW_SIZE/2){

			countAccel = 0;
			if(mQueueOfAccelSamples.size() >= WINDOW_SIZE){
				long ts1 = System.currentTimeMillis();
				if(lastTsWindow != 0) {
					long tsDiffWindow = ts1 - lastTsWindow;
					//Log.d(TAG,"tsDiffWindow: " + tsDiffWindow);
				}
				lastTsWindow = ts1;
				//Get step buffer
				ArrayList<Double> sb = StepCounter.getStepsbuffer();
				/*
				Log.d(TAG, "StepCounter"+String.valueOf(sb.size()));
				if(sb.size()>0)
				Log.d(TAG, "StepCounter"+String.valueOf(sb.get(0)));
                */

				LinkedList copyOfGyroSamples = copyLinkedList(mQueueOfGyroSamples);
				LinkedList copyOfGyroAxisSamples = copyLinkedList(mQueueOfGyroAxisSamples);
				mQueueOfGyroSamples.clear();
				mQueueOfGyroAxisSamples.clear();
				LinkedList copyOfAccelSamples = copyLinkedList(mQueueOfAccelSamples);
				LinkedList copyOfAccelAxisSamples = copyLinkedList(mQueueOfAccelAxisSamples);
				LinkedList copyOfStepsAccelSamples = copyLinkedList(mQueueOfStepsAccelSamples);
				try{
					ProcessDataThread pdThread = new ProcessDataThread();
					pdThread.stepsList = sb;
					pdThread.ts = ts;
					pdThread.queueOfAccelSamples = copyOfAccelSamples;
					pdThread.queueOfAccelAxisSamples = copyOfAccelAxisSamples;
					pdThread.queueOfStepsAccelSamples = copyOfStepsAccelSamples;
					pdThread.queueOfGyroSamples = copyOfGyroSamples;
					pdThread.queueOfGyroAxisSamples = copyOfGyroAxisSamples;
					new Thread(pdThread).start();
				} catch(IllegalThreadStateException e){
					e.printStackTrace();
				}
			}
		}
	}
	
	private void processingGyro(float[] rawGyro, long sensorTs){
		PreProcessing preproc_fft = new PreProcessing(null,rawGyro,null,lowPassFilteredGyro,false,false);

		lowPassFilteredGyro = preproc_fft.getLowPassFilteredGyro();

 		float gyro_norm = preproc_fft.getGyro_norm();

		float hp = highPassGyro(preproc_fft.getGyro_norm()) ;

		highPassFilteredGyro[0]=hp;
		highPassFilteredGyro[1]=hp;
		highPassFilteredGyro[2]=hp;
		
		//highPassFilteredGyro=preproc_fft.getHighPassFilteredGyro();
		//highPassFilteredGyro_steps=preproc_steps.getHighPassFilteredGyro();

		long ts = System.currentTimeMillis();
		if(storeLogs){
			mMeasurementsLogWriter.addMeasurementsLog(ts,"gyro",rawGyro,lowPassFilteredGyro,highPassFilteredGyro,gyro_norm,null,null,sensorTs);
		}
		
		addGyroSample(hp);
		addGyroAxisSample(rawGyro);

	}
	
	private synchronized void addGyroSample(float value){
		//put new sample into buffer
		if(mQueueOfGyroSamples.size() < MAX_GYRO_BUFFER_SIZE){
			mQueueOfGyroSamples.add(value);
		} else {
			while (mQueueOfGyroSamples.size() >= MAX_GYRO_BUFFER_SIZE){
				mQueueOfGyroSamples.poll();
			}
			mQueueOfGyroSamples.add(value);
		}
	}

	private synchronized void addGyroAxisSample(float[] values){
		//put new samples into buffer
		if(mQueueOfGyroAxisSamples.size() < MAX_GYRO_BUFFER_SIZE){
			mQueueOfGyroAxisSamples.add(values);
		} else {
			while (mQueueOfGyroAxisSamples.size() >= MAX_GYRO_BUFFER_SIZE){
				mQueueOfGyroAxisSamples.poll();
			}
			mQueueOfGyroAxisSamples.add(values);
		}
	}

	private float highPassGyro(float x) {
		dc = HIGH_PASS_A * dc + (1 - HIGH_PASS_A) * x;
		float filteredValue = x - dc;
		return filteredValue;
	}

	private float[] realHighPass(float[] values, float[] lastValues, float[] lastValuesFiltered, float HIGH_PASS_A){
		int numValues = values.length;
		float[] valuesFiltered = new float[numValues];
		for (int i = 0; i < numValues; i++) {
			valuesFiltered[i] = HIGH_PASS_A*lastValuesFiltered[i] + HIGH_PASS_A*(values[i] - lastValues[i]);
			//gravity = HIGH_PASS_A * gravity + (1 - HIGH_PASS_A) * values[i];
			//valuesFiltered[i] = values[i] - gravity;
		}
		return valuesFiltered;
	}

	private LinkedList copyLinkedList(LinkedList original){
		LinkedList copy = (LinkedList) original.clone();
		return copy;
	}

	private void setWaitForTop(boolean value){
		waitForTop = value;
	}

	private boolean getWaitForTop(){
		return waitForTop;
	}

	private void setWaitForDown(boolean value){
		waitForDown = value;
	}

	private boolean getWaitForDown(){
		return waitForDown;
	}

	public double getTopFreq() {
		return topFreq;
	}

	public void setTopFreq(double topFreq) {
		this.topFreq = topFreq;
	}

	public double getMinFreq() {
		return minFreq;
	}

	public void setMinFreq(double minFreq) {
		this.minFreq = minFreq;
	}

	/******************** Here starts thread data **************************/

	private static int NEW_MOTION_STEPS_DATA = 1;
	private static int SHOW_TOAST = 2;

	Handler mHandler = new Handler() {

		public void handleMessage(Message msg) {
			int what = msg.what;
			if(what==NEW_MOTION_STEPS_DATA){
				Bundle b = msg.getData();
				int motionMode = b.getInt("motion_mode");
				ArrayList<Double> stepsList = (ArrayList<Double>) b.getSerializable("steps");
				double orientation = b.getDouble("orientation");
				double diffOrientation = b.getDouble("diff_orientation");
				//Log.d(TAG,"MainThread. (" + motionMode + "," + stepsList.size() + "," + orientation + "," + diffOrientation + ")");
				if(motionMode == MotionMode.STATIC.ordinal()){
					setWalking(false);
				}
				else{
					setWalking(true);
				}
				setStepsData(motionMode, stepsList, orientation, diffOrientation);
				fireNewMotionStepsInfoEventMainThread(motionMode, stepsList, orientation, diffOrientation);
			}
			else if(what==SHOW_TOAST){
				Bundle b = msg.getData();
				String msgTxt = b.getString("msg_txt");
				Toast.makeText(mContext,msgTxt, Toast.LENGTH_SHORT).show();
			}
			else{
				Log.e(TAG,"Unkown message type");
			}
		}
	};

	class ProcessDataThread implements Runnable {

		private ArrayList<Double> stepsList;
		private long ts;
		private LinkedList<Float> queueOfAccelSamples;
		private LinkedList<float[]> queueOfAccelAxisSamples;
		private LinkedList<Float> queueOfStepsAccelSamples;
		private LinkedList<Float> queueOfGyroSamples;
		private LinkedList<float[]> queueOfGyroAxisSamples;

		private float[] getMeanAndEnergy(LinkedList<Float> list){
			float sumMean = 0f, sumEnergy = 0f;
			int count = 0;
			for (int i=0; i<list.size(); i++){
				sumMean += list.get(i);
				sumEnergy += Math.pow(list.get(i),2);
				count++;
			}
			//Log.d("hpa", "SUM IS " + sum+ "COUNTER " + count);
			float[] result = new float[2];
			result[0] = sumMean / count;
			result[1] = sumEnergy / count;
			return result;
		}

		private float getVariance(LinkedList<Float> list, float avg){
			float sum = 0f;
			int count = 0;
			for (int i=0; i<list.size(); i++){
				sum += Math.pow(list.get(i) - avg, 2);
				count++;
			}
			return sum / count;
		}

		private float[] getMeanAndEnergyAxis(LinkedList<float[]> list){
			float sumMeanX = 0f, sumMeanY = 0f, sumMeanZ = 0f, sumEnergyX = 0f, sumEnergyY = 0f, sumEnergyZ = 0f;
			int count = 0;
			for (int i=0; i<list.size(); i++){
				sumMeanX += list.get(i)[0];
				sumMeanY += list.get(i)[1];
				sumMeanZ += list.get(i)[2];
				sumEnergyX += Math.pow(list.get(i)[0],2);
				sumEnergyY += Math.pow(list.get(i)[1],2);
				sumEnergyZ += Math.pow(list.get(i)[2],2);
				count++;
			}
			float[] meanAndEnergy = new float[6];
			meanAndEnergy[0] = sumMeanX / count;
			meanAndEnergy[1] = sumMeanY / count;
			meanAndEnergy[2] = sumMeanZ / count;
			meanAndEnergy[3] = sumEnergyX / count;
			meanAndEnergy[4] = sumEnergyY / count;
			meanAndEnergy[5] = sumEnergyZ / count;
			return meanAndEnergy;
		}

		private float[] getVarianceAxis(LinkedList<float[]> list, float avgX, float avgY, float avgZ){
			float sumX = 0f, sumY = 0f, sumZ = 0f;
			int count = 0;
			for (int i=0; i<list.size(); i++){
				sumX += Math.pow(list.get(i)[0] - avgX, 2);
				sumY += Math.pow(list.get(i)[1] - avgY, 2);
				sumZ += Math.pow(list.get(i)[2] - avgZ, 2);
				count++;
			}
			float[] variance = new float[3];
			variance[0] = sumX / count;
			variance[1] = sumY / count;
			variance[2] = sumZ / count;
			return variance;
		}

		private float[] getLastHalfMeanAndEnergyAxis(LinkedList<float[]> list){
			float sumMeanX = 0f, sumMeanY = 0f, sumMeanZ = 0f, sumEnergyX = 0f, sumEnergyY = 0f, sumEnergyZ = 0f;
			int count = 0;
			int initial_index = list.size()/2; //only consider the last half of the list
			for (int i=initial_index; i<list.size(); i++){
				sumMeanX += list.get(i)[0];
				sumMeanY += list.get(i)[1];
				sumMeanZ += list.get(i)[2];
				sumEnergyX += Math.pow(list.get(i)[0],2);
				sumEnergyY += Math.pow(list.get(i)[1],2);
				sumEnergyZ += Math.pow(list.get(i)[2],2);
				count++;
			}
			float[] meanAndEnergy = new float[6];
			meanAndEnergy[0] = sumMeanX / count;
			meanAndEnergy[1] = sumMeanY / count;
			meanAndEnergy[2] = sumMeanZ / count;
			meanAndEnergy[3] = sumEnergyX / count;
			meanAndEnergy[4] = sumEnergyY / count;
			meanAndEnergy[5] = sumEnergyZ / count;
			return meanAndEnergy;
		}

		private float[] processGyroData(long ts, LinkedList mQueueOfGyroSamples, LinkedList mQueueOfGyroAxisSamples){
			float minGyroAxisEnergy = -1;
			float maxGyroAxisEnergy = -1;
			float[] meanAndEnergy = getMeanAndEnergy(mQueueOfGyroSamples);
			float gyroMean = meanAndEnergy[0];
			float gyroEnergy = meanAndEnergy[1];
			float gyroVariance = getVariance(mQueueOfGyroSamples, gyroMean);
			double[] arraySamplesGyroRe = floatLinkedListToDoubleArray(mQueueOfGyroSamples);
			float[] meanAndEnergyAxis = getMeanAndEnergyAxis(mQueueOfGyroAxisSamples);
			float gyroMeanX = meanAndEnergyAxis[0];
			float gyroMeanY = meanAndEnergyAxis[1];
			float gyroMeanZ = meanAndEnergyAxis[2];
			float gyroEnergyX = meanAndEnergyAxis[3];
			float gyroEnergyY = meanAndEnergyAxis[4];
			float gyroEnergyZ = meanAndEnergyAxis[5];
			float[] gyroVarianceAxis = getVarianceAxis(mQueueOfGyroAxisSamples, gyroMeanX, gyroMeanY, gyroMeanZ);
			float gyroVarianceX = gyroVarianceAxis[0];
			float gyroVarianceY = gyroVarianceAxis[1];
			float gyroVarianceZ = gyroVarianceAxis[2];

			if(storeLogs && mFFTLogWriter!=null){
				//mFFTLogWriter.addTimeDomainSignal(ts,"gyro",arraySamplesGyroRe, gyroMean, gyroEnergy, gyroVariance);
				mFFTLogWriter.addTimeDomainSignal(ts,"gyro",arraySamplesGyroRe, gyroMean, gyroEnergy, gyroVariance, gyroMeanX, gyroEnergyX, gyroVarianceX, gyroMeanY, gyroEnergyY, gyroVarianceY, gyroMeanZ, gyroEnergyZ, gyroVarianceZ);
			}

			//calculate min and max of gyro axis
			float minCandidate;
			float maxCandidate;
			if(gyroEnergyX<=gyroEnergyY){
				minCandidate = gyroEnergyX;
				maxCandidate = gyroEnergyY;
			}
			else{
				minCandidate = gyroEnergyY;
				maxCandidate = gyroEnergyX;
			}
			if(gyroEnergyZ<minCandidate){
				minGyroAxisEnergy = gyroEnergyZ;
			}
			else{
				minGyroAxisEnergy = minCandidate;
			}
			if(gyroEnergyZ>maxCandidate){
				maxGyroAxisEnergy = gyroEnergyZ;
			}
			else{
				maxGyroAxisEnergy = maxCandidate;
			}
			float[] res = new float[2];
			res[0] = minGyroAxisEnergy;
			res[1] = maxGyroAxisEnergy;
			return res;
		}

		private double[] floatLinkedListToDoubleArray(LinkedList<Float> floatList){
			double[] doubleArray = new double[floatList.size()];
			int i = 0;

			for (Float f : floatList) {
				doubleArray[i++] = (double)(f != null ? f : Float.NaN);
			}
			return doubleArray;
		}



		private ArrayList<double[]> findDominantFrequencies(double[] arr, long samplingRate){
			ArrayList<double[]> dominantFreqs = new ArrayList<double[]>();
			//find local maximums in the fft result array:
			ArrayList<double[]> localMax = new ArrayList<double[]>();
			for (int i=1; i<(arr.length / 2); i++){		//only loop half of the array (the rest is a reflex), starting from pos two
				if (arr[i] >= arr[i-1] && arr[i] >= arr[i+1] ){
					localMax.add(new double[] {i,arr[i]});
				}
			}

			//select the "dominant" local maximums, i.e. the maximums above a certain threshold relative to the maximum magnitude
			Collections.sort(localMax, new SortByFreqMagnitude());
			if(!localMax.isEmpty()) {
				double MaxFreqMagnitude = localMax.get(0)[1];
				int j = 0;
				boolean done = false;
				while ((localMax.size() > j) && !done) {
					if (localMax.get(j)[1] > (0.5) * MaxFreqMagnitude) {
						//if (localMax.get(j)[1] > (0.5)*MaxFreqMagnitude){
						dominantFreqs.add(new double[]{Double.valueOf(samplingRate * (localMax.get(j)[0] / arr.length)), localMax.get(j)[1]}); //convert to frequency value (Hz)
					} else {
						//the magnitude is below the threshold, so we stop since they are sorted desc by magnitude
						done = true;
					}
					j++;
				}
				Collections.sort(dominantFreqs, new SortByFreq());
			}
			return dominantFreqs;
		}

		private MotionMode motionModeRecognition(float accelEnergy, ArrayList<double[]> dominantAccelFreqs, float minGyroAxisEnergy, float maxGyroAxisEnergy){
			boolean enableToasts = false;
			boolean showToast = false;
			String msgTxt = "Text";
			MotionMode currentMotionMode;
			//Log.i(TAG,"inside MotionModeRecognition. accelEnergy: " + accelEnergy + " , gyroEnergy: " + gyroEnergy);
			if (accelEnergy > ACCEL_ENERGY_LOW_THRESHOLD){
				//is not static
				if (accelEnergy < ACCEL_ENERGY_HIGH_THRESHOLD) {
					if ((dominantAccelFreqs.size() <= MAX_NUM_DOMINANT_FREQS)) {
						if(dominantAccelFreqs.isEmpty()){
							currentMotionMode = MotionMode.IRREGULAR_MOTION;
							msgTxt = "IRREGULAR 5: No dominant frequencies";
							//Log.d(TAG, msgTxt);
							showToast = true;
						}
						else {
							if ((dominantAccelFreqs.get(0)[0] > MIN_FREQ_WALKING) && (dominantAccelFreqs.get(0)[0] < MAX_FREQ_WALKING)) {
								//it seems that we are walking
								if ((minGyroAxisEnergy < GYRO_ENERGY_LOW_THRESHOLD && maxGyroAxisEnergy < GYRO_ENERGY_HIGH_THRESHOLD) || (Float.isNaN(minGyroAxisEnergy))) {
									currentMotionMode = MotionMode.WALKING;
								} else {
									currentMotionMode = MotionMode.IRREGULAR_MOTION;
									msgTxt = "IRREGULAR 4: High gyro variance";
									//Log.d(TAG, msgTxt);
									showToast = true;
								}
							} else {
								//irregular motion
								currentMotionMode = MotionMode.IRREGULAR_MOTION;
								msgTxt = "IRREGULAR 3: freq[0] != {0.7,2}";
								//Log.d(TAG, msgTxt);
								showToast = true;
							}
						}
					} else {
						//irregular motion
						currentMotionMode = MotionMode.IRREGULAR_MOTION;
						msgTxt = "IRREGULAR 2: > 2 dominant freqs";
						//Log.d(TAG, msgTxt);
						showToast = true;
					}
				}
				else{
					//irregular motion
					currentMotionMode = MotionMode.IRREGULAR_MOTION;
					msgTxt = "IRREGULAR 1: High Energy";
					//Log.d(TAG, msgTxt);
					showToast = true;
				}
			}else{
				//static
				currentMotionMode = MotionMode.STATIC;
			}

			if(enableToasts && showToast){
				Message msg2 = Message.obtain();
				msg2.what = SHOW_TOAST;
				Bundle b = new Bundle();
				b.putString("msg_txt", msgTxt);
				msg2.setData(b);
				mHandler.sendMessage(msg2);
			}

			//Log.d(TAG,"Motion mode: " + currentMotionMode.toString() + " (based on MEMS)");
			return currentMotionMode;
		}

		private ArrayList<Double> stepsDetector(LinkedList<Float> mQueueOfStepsAccelSamples){
			ArrayList<Double> stepsList = new ArrayList<>();
			double MaxDominant;
			double MinDominant;
			double freq;
			//Find maxims and minims:
			//Find local maximums:
			ArrayList<double[]> localMax = new ArrayList<double[]>();
			ArrayList<double[]> localMin = new ArrayList<double[]>();
			ArrayList<double[]> MergeMaxMin = new ArrayList<double[]>();
			ArrayList<double[]> DominantFreqs = new ArrayList<double[]>();

			Float[] arr = mQueueOfStepsAccelSamples.toArray(new Float[mQueueOfStepsAccelSamples.size()]);
			//int initial_index = 0;
			int initial_index = arr.length/2; //only consider the last half of the buffer (corresponding to half of the window)
			for (int i=(initial_index+1); i<(arr.length-1); i++){		//loop
				if (arr[i] >= arr[i-1] && arr[i] >= arr[i+1] ){
					freq=(double)arr[i];
					localMax.add(new double[] {i-initial_index,freq});
				}
			}

			//Find local minims:
			for (int i=(initial_index+1); i<(arr.length-1); i++){		//loop
				if (arr[i] <= arr[i-1] && arr[i] <= arr[i+1] ){
					freq=(double)arr[i];
					localMin.add(new double[] {i-initial_index,freq});
				}
			}

			//Concatenate local minims and local maxims
			MergeMaxMin.addAll(localMax);
			MergeMaxMin.addAll(localMin);
			//Find dominant freq
			Collections.sort(MergeMaxMin, new SortByFreqMagnitude());
			MaxDominant=MergeMaxMin.get(0)[1];
			MinDominant=MergeMaxMin.get(MergeMaxMin.size()-1)[1];
			//Find the DominantFreqs

			for (int i = 0; i < MergeMaxMin.size(); i++) {
				if(MergeMaxMin.get(i)[1]>=MaxDominant/2  || MergeMaxMin.get(i)[1]<=MinDominant/2 ){
					//if(MergeMaxMin.get(i)[1]>=0.40  || MergeMaxMin.get(i)[1]<=-0.30 ){
					DominantFreqs.add(MergeMaxMin.get(i));
				}
			}

			// Don't find the dominant freqs: We consider all the freqs to be useful
			//DominantFreqs=MergeMaxMin;

			//Order to search for the pattern :
			Collections.sort(DominantFreqs, new SortByFreq());

			boolean waitForTop = getWaitForTop();
			boolean waitForDown = getWaitForDown();
			double topFreq = getTopFreq();
			double minFreq = getMinFreq();
			for (int i = 0; i < DominantFreqs.size()-1; i++) {
				if(waitForTop){
					if(DominantFreqs.get(i)[1]>0 ){
						waitForTop=false;
						waitForDown=true;
						topFreq=DominantFreqs.get(i)[1];
					}
				}
				if(waitForDown){
					if(DominantFreqs.get(i+1)[1]<0){
						minFreq=DominantFreqs.get(i+1)[1];
						double newStepLength = processStep(topFreq,minFreq,STEP_LENGTH_COEFFICIENT);
						stepsList.add(newStepLength);
						waitForTop=true;
						waitForDown=false;
						i++;
					}
				}
			}
			setWaitForTop(waitForTop);
			setWaitForDown(waitForDown);
			setTopFreq(topFreq);
			setMinFreq(minFreq);

			return stepsList;
		}

		private double processStep(double maxAccel,double minAccel,double c){
			double newStepLength = Math.sqrt(Math.sqrt((float) (maxAccel-minAccel)))*c;
			return newStepLength;
		}

		private double[] getOrientation(){
			double orient = 0;
			double diffOrient = 0;
			if(orientationMethod == OrientationType.geodetic){
				orient = getGeodeticOrientation() + mapAngleToNorth;
				//orient=getGeodeticOrientation();
			}
			else if(orientationMethod == OrientationType.magnetometer){
				double frameOrientation = getFrameOrientation();
				if(frameOrientation==-1){
					orient = -1;
				}
				else {
					orient = frameOrientation + mapAngleToNorth;
				}
			}
			else if(orientationMethod == OrientationType.relative){
				//Log.d(TAG,"before calling getRelativeOrientation");
				double[] orients = getRelativeOrientation();
				orient = orients[0];
				diffOrient = orients[1];
				// orient variable contains orientation calculated with magnetometer and accelerometer
				double frameOrientation = getFrameOrientation();
				if(frameOrientation==-1){
					orient = -1;
				}
				else {
					orient = frameOrientation + mapAngleToNorth;
					if(orient < 0){
						orient += 360;
					}
					else if(orient >= 360){
						orient -= 360;
					}
				}
			}
			else{
				Log.e(TAG,"wrong method");
			}
			if(useAisleAdapter){
				orient = orientationAdapterToAisle(orient);
			}
			double[] out = new double[2];
			out[0] = orient;
			out[1] = diffOrient;
			return out;
		}

		private synchronized double getFrameOrientation(){
			return orientationCalculator.getOrientation();
		}

		private synchronized double getGeodeticOrientation(){
			return GeodeticOrientation.getOrientationDegree();
		}

		private double orientationAdapterToAisle(double orientAisle){

			if(orientAisle<=45 && orientAisle>-45){
				orientAisle=0;
			}
			if(orientAisle<=135 && orientAisle>45){
				orientAisle=90;
			}
			if((orientAisle<=180 && orientAisle>135)||(orientAisle<=-135 && orientAisle>-180  )){
				orientAisle=180;
			}
			if(orientAisle <=-45 && orientAisle>-135){
				orientAisle=-90;
			}

			return orientAisle;
		}

		private double[] getRelativeOrientation(){
			return relativeOrientation.getRelativeOrientation();
		}

		/* Update rotation matrix */
		private double[] updateRmatrix(float meanAccX, float meanAccY, float meanAccZ){
			double[] result = new double[3];
			if(relativeOrientation!=null){
				result = relativeOrientation.calculateRmatrix(meanAccX, meanAccY, meanAccZ);
			}
			return result;
		}

		@Override
		public void run() {
			long ts1 = System.currentTimeMillis();

			float[] meanAndEnergy = getMeanAndEnergy(queueOfAccelSamples);
			float accelMean = meanAndEnergy[0];
			float accelEnergy = meanAndEnergy[1];
			float accelVariance = getVariance(queueOfAccelSamples, accelMean);
			float[] lastHalfMeanAndEnergyAxis = getLastHalfMeanAndEnergyAxis(queueOfAccelAxisSamples);
			float halfAccelMeanX = lastHalfMeanAndEnergyAxis[0];
			float halfAccelMeanY = lastHalfMeanAndEnergyAxis[1];
			float halfAccelMeanZ = lastHalfMeanAndEnergyAxis[2];
			float[] meanAndEnergyAxis = getMeanAndEnergyAxis(queueOfAccelAxisSamples);
			float accelMeanX = meanAndEnergyAxis[0];
			float accelMeanY = meanAndEnergyAxis[1];
			float accelMeanZ = meanAndEnergyAxis[2];
			float accelEnergyX = meanAndEnergyAxis[3];
			float accelEnergyY = meanAndEnergyAxis[4];
			float accelEnergyZ = meanAndEnergyAxis[5];
			float[] accelVarianceAxis = getVarianceAxis(queueOfAccelAxisSamples, accelMeanX, accelMeanY, accelMeanZ);
			float accelVarianceX = accelVarianceAxis[0];
			float accelVarianceY = accelVarianceAxis[1];
			float accelVarianceZ = accelVarianceAxis[2];
			//Log.d(TAG,"accelEnergySTEPS: " + accelEnergy);
			//Log.d(TAG,"accelMeanX: " + accelMeanX + " , accelMeanY: " + accelMeanY + " , accelMeanZ: " + accelMeanZ);

			float[] gyroData = processGyroData(ts, queueOfGyroSamples, queueOfGyroAxisSamples);
			float minGyroAxisEnergy = gyroData[0];
			float maxGyroAxisEnergy = gyroData[1];

			double[] arraySamplesRe = floatLinkedListToDoubleArray(queueOfStepsAccelSamples);
			//double[] arraySamplesRe = generateSinusSignal(WINDOW_SIZE, 100, 2);
			double[] arraySamplesIm = new double[WINDOW_SIZE];
			for(int i=0; i<WINDOW_SIZE; i++){
				arraySamplesIm[i] = 0;
			}

			if(storeLogs){
				mFFTLogWriter.addTimeDomainSignal(ts,"accel",arraySamplesRe, accelMean, accelEnergy, accelVariance, accelMeanX, accelEnergyX, accelVarianceX, accelMeanY, accelEnergyY, accelVarianceY, accelMeanZ, accelEnergyZ, accelVarianceZ);
				double[] arraySamplesAccelStepsRe = floatLinkedListToDoubleArray(queueOfStepsAccelSamples);
				float[] meanAndEnergySteps = getMeanAndEnergy(queueOfStepsAccelSamples);
				float accelMeanSteps = meanAndEnergySteps[0];
				float accelEnergySteps = meanAndEnergySteps[1];
				float accelVarianceSteps = getVariance(queueOfStepsAccelSamples, accelMeanSteps);
				mFFTLogWriter.addTimeDomainSignal(ts,"accel_steps",arraySamplesAccelStepsRe, accelMeanSteps, accelEnergySteps, accelVarianceSteps);
			}
			long ts2 = System.currentTimeMillis();
			long tsDiffEnergy = ts2 - ts1;

			mFFT.fft(arraySamplesRe, arraySamplesIm);
			long ts3 = System.currentTimeMillis();
			long tsDiffFFT = ts3 - ts1;

			double[] arrayFFTmagnitude = new double[WINDOW_SIZE];

			for(int i=0; i<WINDOW_SIZE; i++){
				arrayFFTmagnitude[i] = arraySamplesRe[i]*arraySamplesRe[i] + arraySamplesIm[i]*arraySamplesIm[i];
			}

			// peaks detection over the resulting FFT, to detect the dominant frequencies (obtaining an array of dominant frequencies)
			ArrayList<double[]> dominantAccelFreqs = findDominantFrequencies(arrayFFTmagnitude, samplingRate);

			long ts4 = System.currentTimeMillis();
			long tsDiffDominant = ts4 - ts1;

			String strFreqs = "";
			for (int i=0; i<dominantAccelFreqs.size(); i++){
				strFreqs += String.format("%.2f", dominantAccelFreqs.get(i)[0]) + "-" + String.format("%.2f", dominantAccelFreqs.get(i)[1]) + "  ";
			}

			/*  motion mode recognition taking as inputs:
			 *  @param accelEnergy: energy of accelerometer
			 *  @param dominantAccelFreqs: dominant accelerometer frequencies
			 *  @param minGyroAxisEnergy: minimum gyroscope energy from 3 axis
			 *  @param maxGyroAxisEnergy: maximum gyroscope energy from 3 axis
			 */
			MotionMode currentMotionMode = motionModeRecognition(accelEnergy, dominantAccelFreqs, minGyroAxisEnergy, maxGyroAxisEnergy);

			long ts5 = System.currentTimeMillis();
			long tsDiffMotionMode = ts5 - ts1;
			if(storeLogs){
				mFFTLogWriter.addFreqDomainSignal(ts,"accel",arrayFFTmagnitude,dominantAccelFreqs.size(),currentMotionMode.ordinal());
			}
			/*
			ArrayList<Double> stepsList = new ArrayList<>();
			if(currentMotionMode == MotionMode.WALKING){
				stepsList=stepsDetector(queueOfStepsAccelSamples);
			}
			*/

			// calculate orientation
			boolean highOrientationChange = false;
			double[] result = updateRmatrix(halfAccelMeanX, halfAccelMeanY, halfAccelMeanZ);
			double rollDiff = result[0];
			double pitchDiff = result[1];
			double totalDiff = result[2];
			if(totalDiff > TOTAL_DIFF_THRESHOLD){
				highOrientationChange = true;
			}
			else if(rollDiff > ROLL_DIFF_THRESHOLD && pitchDiff > PITCH_DIFF_THRESHOLD){
				highOrientationChange = true;
			}

			long ts6 = System.currentTimeMillis();
			long tsDiffRmatrix = ts6 - ts1;

			double[] orients = getOrientation();
			double orientation = orients[0];
			double diffOrientation = orients[1];
			// high orientation change disabled because it was affecting behaviour when smartphone is in a pocket
			/*
			if(highOrientationChange){
				diffOrientation = 0;
				String msgTxt = "high orientation change: (" + decFor.format(rollDiff*RAD_TO_DEG) + ";" + decFor.format(pitchDiff*RAD_TO_DEG) + ";" + decFor.format(totalDiff*RAD_TO_DEG) + ")";
				Log.d(TAG,msgTxt);
				//Toast.makeText(mContext, msgTxt, Toast.LENGTH_SHORT).show();
			}
			*/

			if(storeLogs){
				mStepsOrientationLogWriter.addStepsAndOrientation(ts, stepsList.size(), orientation, diffOrientation);
			}

			long ts7 = System.currentTimeMillis();
			long tsDiffOrientation = ts7 - ts1;

			// Send message to handler
			Message msg = Message.obtain();
			msg.what = NEW_MOTION_STEPS_DATA;
			Bundle b = new Bundle();
			b.putInt("motion_mode", currentMotionMode.ordinal());
			b.putSerializable("steps", stepsList);
			b.putDouble("orientation", orientation);
			b.putDouble("diff_orientation", diffOrientation);
			msg.setData(b);
			mHandler.sendMessage(msg);
			//Log.d(TAG,"SecondaryThread. (" + currentMotionMode.ordinal() + "," + stepsList.size() + "," + orientation + "," + diffOrientation + ")");
			fireNewMotionStepsInfoEvent(currentMotionMode.ordinal(), stepsList, orientation, diffOrientation);
			long ts8 = System.currentTimeMillis();
			long tsDiffTotal = ts8 - ts1;
			//Log.d(TAG,"tsDiff | Energy: " + tsDiffEnergy + " | FFT: " + tsDiffFFT + " | Dominant: " + tsDiffDominant + " | MotionMode: " + tsDiffMotionMode + " | Rmatrix: " + tsDiffRmatrix + " | Orientation: " + tsDiffOrientation + " | Total: " + tsDiffTotal);
		}
	}

	/*Nested class for sorting object by freq magnitude IN DESC ORDER (signs of the 'one' changed with respect to ASCENDENT order)*/
	public class SortByFreqMagnitude implements Comparator<double[]>{
		public int compare(double[] arg0, double[] arg1) {
			if ( arg0[1] < arg1[1]){
				return 1;
			} else if ( arg0[1] == arg1[1] ){
				return 0;	
			} else if ( arg0[1] > arg1[1]){
				return -1;
			}
			return 0;
		}
	}
	
	/*Nested class for sorting object by freq IN ASC ORDER */
	public class SortByFreq implements Comparator<double[]>{
		public int compare(double[] arg0, double[] arg1) {
			if ( arg0[0] < arg1[0]){
				return -1;
			} else if ( arg0[0] == arg1[0] ){
				return 0;
			} else if ( arg0[0] > arg1[0]){
				return 1;
			}
			return 0;
		}
	}

	public void calculateHeadingBias(Point3DWithFloorAndEdge currentLocationAndEdge, Point3DWithFloorAndEdge lastLocationAndEdge) {
		if(currentLocationAndEdge!=null && lastLocationAndEdge!=null){
			if(!currentLocationAndEdge.getPoint2D().equals(lastLocationAndEdge.getPoint2D())){
				if(currentLocationAndEdge.getEdge().equals(lastLocationAndEdge.getEdge())){
					//last and current edges are the same edge => the heading bias can be obtained
					/*
					 * primer hem d'esbrinar quin �s el sentit de la marxa sobre l'edge
					 * per tal d'esbrinar quin ser� endnode i startnode (per a posteriorment 
					 * poder calcular el vector director i poder calcular l'angle...)
					*/
					Point3D startNode, endNode;
					double currentLocToPointA_x = currentLocationAndEdge.getEdge().endNode.x - currentLocationAndEdge.x;
					double currentLocToPointA_y = currentLocationAndEdge.getEdge().endNode.y - currentLocationAndEdge.y;
					double lastLocToPointA_x = currentLocationAndEdge.getEdge().endNode.x - lastLocationAndEdge.x;
					double lastLocToPointA_y = currentLocationAndEdge.getEdge().endNode.y - lastLocationAndEdge.y;
					double distCurrentLocToPointA = Math.pow(currentLocToPointA_x,2) + Math.pow(currentLocToPointA_y,2);
					double distLastLocToPointA = Math.pow(lastLocToPointA_x,2) + Math.pow(lastLocToPointA_y,2);
					if(distCurrentLocToPointA > distLastLocToPointA){
						//el sentit de la marxa �s cap al punt B
						endNode = currentLocationAndEdge.getEdge().startNode;
						startNode = currentLocationAndEdge.getEdge().endNode;
					}else{
						//el sentit de la marxa �s cap al punt A
						endNode = currentLocationAndEdge.getEdge().endNode;
						startNode = currentLocationAndEdge.getEdge().startNode;			
					}
					
					/* 
					 * obtenir el heading "local" del v�rtex vol dir obtenir l'angle entre
					 * entre el vector sobre l'edge i el vector del sentit nord del mapa local 
					 */
					double vectorEdge_x = endNode.x - startNode.x;
					/*
					 * en el cas de les y �s al rev�s perqu� hem traslladat la recta a uns eixos
					 * de refer�ncia amb la y positiva apuntant al nord del mapa local (no pas la y
					 * negativa com per defecte est� en els nostres mapes). Ho hem de fer aix�
					 * perqu� Airfi ens d�na el MapAngleToNorth entre global i mapa local considerant
					 * el nord del mapa local en el sentit de les nostres y negatives per defecte, 
					 * no positives:
					*/
					double vectorEdge_y = startNode.y - endNode.y;
					double vectorLocalNorth_x = 0.0; //recta x=0 amb y positiva (vector que marca el nord del mapa local)
					double vectorLocalNorth_y = 1.0; //recta x=0 amb y positiva (vector que marca el nord del mapa local)
					
					Log.d(TAG,"*** calculating heading. current location: " + currentLocationAndEdge.x + ", " + currentLocationAndEdge.y);
					Log.d(TAG,"*** calculating heading. last location: " + lastLocationAndEdge.x + ", " + lastLocationAndEdge.y);
					Log.d(TAG,"*** calculating heading. startnode_x: " + startNode.x + " endnode_x: " + endNode.x);
					Log.d(TAG,"*** calculating heading. startnode_y: " + startNode.y + " endnode_y: " + endNode.y);
					
					double dotproduct = vectorEdge_x*vectorLocalNorth_x + vectorEdge_y*vectorLocalNorth_y;
					double magnitudeVEdge = Math.sqrt(Math.pow(vectorEdge_x,2) + Math.pow(vectorEdge_y,2));
					double magnitudeVLocalNorth = Math.sqrt(Math.pow(vectorLocalNorth_x,2) + Math.pow(vectorLocalNorth_y,2));
					double dotproductMagnitudes = magnitudeVEdge * magnitudeVLocalNorth;
					double headingEdge = Math.toDegrees(Math.acos((double)(dotproduct/dotproductMagnitudes)));
					/*
					 * L'angle entre dos rectes sempre ens d�na positiu pq �s l'angle menor dels dos possibles.
					 * Si el vector director del despla�ament detectat va cap a l'oest cal fer-lo negatiu
					 * (per definici� de heading respecte el nord):
					*/ 
					if(vectorEdge_x < 0){
						headingEdge = headingEdge * (-1);
					}
					
					double headingMEMS = 0;
					if(orientationMethod == OrientationType.geodetic){
						//headingMEMS = getGeodeticOrientation() + mapAngleToNorth; //sumem MATN per a convertir a "local"
					}
					else if(orientationMethod == OrientationType.magnetometer){
						//headingMEMS = getOrientation()[0] + mapAngleToNorth; //sumem MATN per a convertir a "local"
			 		}
					else if(orientationMethod == OrientationType.relative){
						//TODO: implement whatever functionality
					}
					else{
						Log.e(TAG, "wrong method");
					}
					
					mHeadingBias_raw = headingEdge - headingMEMS;
					
					// low pass filter
					long currentTs = System.currentTimeMillis();
					if(mTimeStampHeadingBias != -1){
						if(currentTs - mTimeStampHeadingBias < 10000){
							/*
							 * low pass filter to avoid sudden huge bias variations in changes of direction
							 * caused because user has already changed direction (headingMEMS has changed noticeably)
							 * but it is still attached to the "old" path edge
							 */						
							mHeadingBias = lowPassHeadingBias(mHeadingBias_raw, mHeadingBias);
							Log.d(TAG,"+++ New filtered heading bias obtained: " + mHeadingBias + " heading bias RAW: " + mHeadingBias_raw + " (heading Edge: " + headingEdge + ")" + " (heading MEMS: " + headingMEMS + ")");
						}
						else{
							//last heading bias estimation is too old to apply low pass filtering
							mHeadingBias = mHeadingBias_raw;
							Log.d(TAG,"+++ New heading bias obtained: " + mHeadingBias + " heading bias RAW: " + mHeadingBias_raw + " (heading Edge: " + headingEdge + ")" + " (heading MEMS: " + headingMEMS + ")");
						}		
					}
					else{
						// no past epoch Heading bias, we cannot filter
						mHeadingBias = mHeadingBias_raw;
					}
						
					mTimeStampHeadingBias = currentTs;
	
					//Log.d(TAG,"+++ New heading bias obtained: " + mHeadingBias + " heading bias RAW: " + mHeadingBias_raw + " (heading Edge: " + headingEdge + ")" + " (heading MEMS: " + headingMEMS + ")");
				}
				else{
					Log.d(TAG,"--- Heading bias could not be obtained because DIFFERENT edges");
				}
			}
			else{
				//Log.d(TAG,"--- Heading bias could not be obtained because two LAST POSITIONS ARE THE SAME");
			}
		}
		else{
			Log.d(TAG,"--- Heading bias could not be obtained because of NULL");
		}
	}
 
	private double lowPassHeadingBias(double input, double output){    
	    output = output + lowPass_b * (input - output);  
	    return output;
	}

	/* event listeners corresponding to motion steps info*/
	public synchronized void addMotionStepsInfoListener(MotionStepsInfoListener listener) {
		mMotionStepsInfoListeners.add(listener);
	}

	public synchronized void removeMotionStepsInfoListener(MotionStepsInfoListener listener) {
		mMotionStepsInfoListeners.remove(listener);
	}

	private synchronized void fireNewMotionStepsInfoEvent(int sensorState, ArrayList<Double> stepsList, double orientation, double diffOrientation){
		for(MotionStepsInfoListener listener : mMotionStepsInfoListeners){
			listener.newMotionStepsInfoAvailable(sensorState, stepsList, orientation, diffOrientation);
		}
	}

	/* event listeners corresponding to motion steps info*/
	public synchronized void addMotionStepsInfoListenerMainThread(MotionStepsInfoListener listener) {
		mMotionStepsInfoListenersMainThread.add(listener);
	}

	public synchronized void removeMotionStepsInfoListenerMainThread(MotionStepsInfoListener listener) {
		mMotionStepsInfoListenersMainThread.remove(listener);
	}

	private synchronized void fireNewMotionStepsInfoEventMainThread(int sensorState, ArrayList<Double> stepsList, double orientation, double diffOrientation){
		for(MotionStepsInfoListener listener : mMotionStepsInfoListenersMainThread){
			listener.newMotionStepsInfoAvailable(sensorState, stepsList, orientation, diffOrientation);
		}
	}
	
}


