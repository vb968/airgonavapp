package com.ascamm.motion.mems;

import java.util.ArrayList;

/*
 * This class stores data related with PDR steps
 */
public class StepCounter {
	private static float stepCoefficient = 0.5f;
	private static int steps;
	private static long start;
	private static long LastStepTime;
	private static long duration_step;
	private static long duration_between;
	private static String mode="-";
	private static float low_peak;
	private static float high_peak;
	private static double steplength;
	private static ArrayList<Double> StepsBuffer = new ArrayList<Double>();
	private static int numPeaksFalling;
	private static float lastDelta;

	public static int steps(){return steps;}
	public static void increment(){steps++;}
	public static void reset(){
		steps = 0;
	}
	public static long getStart() {
		return start;
	}
	public static void setStart(long start) {
		StepCounter.start = start;
	}
	public static void setMode(String mode) {
		StepCounter.mode = mode;
	}
	public static String getMode() {
		return mode;
	}
	public static long getDurationBetweenSteps() {
		return duration_between;
	}
	public static long getDurationStep() {
		return duration_step;
	}
	public static double getSteplength() {
		return steplength;
	}
	public static long getLastStepTime() {
		return LastStepTime;
	}
	public static void setDurationBetweenSteps(long duration_between) {StepCounter.duration_between = duration_between;}
	public static void setDurationStep(long duration_step) {StepCounter.duration_step = duration_step;}
	public static void setLastStepTime(long LastStepTime) {
		StepCounter.LastStepTime = LastStepTime;
	}
	public static void setSteps(int steps) {
		StepCounter.steps = steps;
	}
	public static float getLow_peak() {
		return low_peak;
	}
	public static void setLow_peak(float peak) {
		StepCounter.low_peak = peak;
	}
	public static float getHigh_peak() {
		return high_peak;
	}
	public static void setHigh_peak(float peak) {
		StepCounter.high_peak = peak;
	}
	public static float getLastDelta() {
		return lastDelta;
	}
	public static void setLastDelta(float lastDelta) {
		StepCounter.lastDelta = lastDelta;
	}

	public static void calculateStepLength() {
		StepCounter.steplength = Math.sqrt(Math.sqrt((StepCounter.high_peak-StepCounter.low_peak)))*stepCoefficient;
		StepsBuffer.add(steplength);
	}

	public static synchronized ArrayList<Double> getStepsbuffer(){
		ArrayList<Double> StepsBufferCopy = new ArrayList<Double>(StepsBuffer);
		StepsBuffer.clear();
		return StepsBufferCopy;
	}

	public static int getNumPeaksFalling() {
		return numPeaksFalling;
	}

	public static void setNumPeaksFalling(int numPeaksFalling) {
		StepCounter.numPeaksFalling = numPeaksFalling;
	}
}
