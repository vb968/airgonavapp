package com.ascamm.motion.mems;

import java.util.LinkedList;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.util.FloatMath;
import android.util.Log;

public class PreProcessing {
    private final String TAG = this.getClass().getSimpleName();
    SensorManager sensorManager;
    Sensor AccelSensor, GyroSensor;

    //linear accelerations from accel:
    private float[] accel = new float[3];
    //angular speeds from gyro:
    private float[] gyro = new float[3];

    float[] lowPassFilteredAccel = new float[3];
    float[] lowPassFilteredGyro = new float[3];
    float[] highPassFilteredAccel = new float[3];
    float[] highPassFilteredGyro = new float[3];
    //filters data:
    float lowPass_a;
    float highPass_a = 0.99f;
    //float[] gravity = new float[3];
    float gravity = 9.81f;  //for accel
    float dc = 0f; //for gyro
    //data processing structures:
    private LinkedList<Float> mQueueOfAccelSamples = new LinkedList<Float>();
    private LinkedList<Float> mQueueOfGyroSamples = new LinkedList<Float>();
    private final int WINDOW_SIZE = 256;
    int countAccel = 0;
    int countGyro = 0;
    float gyro_norm;
    float accel_norm;

    //filters Hertz:
    boolean realTime;

    public PreProcessing(float[] rawAccel, float[] rawGyro, float[] lowPassFilteredAccel, float[] lowPassFilteredGyro, boolean stepsLowPass, boolean realtime) {
        if (stepsLowPass) {
            //If you make this param lower you have less defined frequency then the step counter is less sensible to noise and counts more steps than real
            this.lowPass_a = 0.03f; //0.1480f
        } else {
            this.lowPass_a = 0.15f; //0.15f
        }

        this.accel = rawAccel;
        this.gyro = rawGyro;
        if (lowPassFilteredAccel != null) {
            //Log.d("hpa", "assignar " + lowPassFilteredAccel[0] + "*" +lowPassFilteredAccel[1] +"*" +lowPassFilteredAccel[2]);
        }
        this.lowPassFilteredAccel = lowPassFilteredAccel;
        this.lowPassFilteredGyro = lowPassFilteredGyro;

        if (!realtime) {
            windowExecute();
        } else {
            realTimeExecute();
        }


    }


    protected void windowExecute() {
        if (this.accel != null) {
            //LPF + NORMA + HPF (ACCEL)
            //Log.d("hpa", "antes " + lowPassFilteredAccel[0] + "*" +lowPassFilteredAccel[1] +"*" +lowPassFilteredAccel[2]);
            this.lowPassFilteredAccel = lowPass(this.accel, this.lowPassFilteredAccel);
            this.accel_norm = (float)Math.sqrt(this.lowPassFilteredAccel[0] * this.lowPassFilteredAccel[0] + this.lowPassFilteredAccel[1] * this.lowPassFilteredAccel[1] + this.lowPassFilteredAccel[2] * this.lowPassFilteredAccel[2]);

        }
        if (this.gyro != null) {
            //LPF + NORMA + HPF (GYRO)
            this.lowPassFilteredGyro = lowPass(gyro, this.lowPassFilteredGyro);
            this.gyro_norm = (float)Math.sqrt(this.lowPassFilteredGyro[0] * this.lowPassFilteredGyro[0] + this.lowPassFilteredGyro[1] * this.lowPassFilteredGyro[1] + this.lowPassFilteredGyro[2] * this.lowPassFilteredGyro[2]);

        }

    }


    protected void realTimeExecute() {


    }


    protected float[] lowPass(float[] input, float[] output) {
        if (output == null) return input;
        for (int i = 0; i < input.length; i++) {
            output[i] = output[i] + lowPass_a * (input[i] - output[i]);
        }
        return output;
    }


    public float[] getLowPassFilteredAccel() {
        return this.lowPassFilteredAccel;
    }

    public float[] getLowPassFilteredGyro() {
        return this.lowPassFilteredGyro;
    }

    public float[] getHighPassFilteredAccel() {
        return this.highPassFilteredAccel;
    }

    public float[] getHighPassFilteredGyro() {
        return this.highPassFilteredGyro;
    }


    public float getAccel_norm() {
        return this.accel_norm;
    }

    public float getGyro_norm() {
        return this.gyro_norm;
    }


}
