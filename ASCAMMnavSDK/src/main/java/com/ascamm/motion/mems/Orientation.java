package com.ascamm.motion.mems;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import com.ascamm.motion.utils.Constants;


public class Orientation implements SensorEventListener {	
	
    private String TAG = getClass().getSimpleName();

	private boolean debug = Constants.DEBUGMODE;

    private float[] outputOrientation = new float[3];
	
	final Context ctx;	
		
	SensorManager sensorManager;
	Sensor rotationSensor;
	
	static final int orientationWindow = 16; // Elapsed time between orientation messages in milliseconds.16?
	
	// Used to track time between orientation/position events.
	long lastTick = SystemClock.elapsedRealtime();
	long elapsed = 0;
	
	float mat[] = new float[9];
	float rmat[] = new float[9];
	float angles[] = new float[3];
	float orientNorth= 0f;
	// Running averages used to compute the device's orientation.
	RunningAverage rx = new RunningAverage(ROTATION_RUNNING_AVERAGE_NUM_VALUES);
	RunningAverage ry = new RunningAverage(ROTATION_RUNNING_AVERAGE_NUM_VALUES);
	RunningAverage rz = new RunningAverage(ROTATION_RUNNING_AVERAGE_NUM_VALUES);
	
	// Running average queue size.
	// Higher values result in more smoothing, but also in a slower response.
	static final int ROTATION_RUNNING_AVERAGE_NUM_VALUES = 5;
	
	static final float TO_DEG = 180.0f / (float) Math.PI;
	
	public Orientation(Context ctx) {
		this.ctx = ctx;		
	}
	public void StartOrientation(){
		sensorManager = (SensorManager) ctx.getSystemService(Context.SENSOR_SERVICE);
		rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
		//rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
		if(rotationSensor!=null){
			sensorManager.registerListener(this, rotationSensor, SensorManager.SENSOR_DELAY_NORMAL);
		}
	}

	public void StopOrientation(){
		if(rotationSensor!=null) {
			sensorManager.unregisterListener(this, rotationSensor);
		}
	}
	
	public float getOrientation(){
		float orientation = -1;
		if(rotationSensor!=null){
			float tmp = outputOrientation[1]*(-1);
			if(tmp < 0){
				tmp += 360;
			}
			else if(tmp >= 360){
				tmp -= 360;
			}
			orientation = tmp;
		}
		return orientation;
	}
	/*
	public float getNorthOrientation(){
		
		if(outputOrientation[1]>=-180 && outputOrientation[1]<=180 && outputOrientation[2]>=-180 && outputOrientation[2]<=180)
		{
			orientNorth= (-outputOrientation[1]-outputOrientation[2]);
			return orientNorth;
		}
		else{
			return orientNorth;
		}
		
	}
	*/

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		String sensorName = sensor.getName();
		String accuracyStr = "";
		if(accuracy == SensorManager.SENSOR_STATUS_ACCURACY_LOW){
			accuracyStr = "LOW";
		}
		else if(accuracy == SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM){
			accuracyStr = "MEDIUM";
		}
		else if(accuracy == SensorManager.SENSOR_STATUS_ACCURACY_HIGH){
			accuracyStr = "HIGH";
		}
		else if(accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE){
			accuracyStr = "UNRELIABLE";
		}
		if(debug) {
			Log.d(TAG, "onAccuracyChanged. (" + sensorName + " , " + accuracyStr + ")");
			Toast.makeText(ctx, sensorName + " | ACC: " + accuracyStr, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		long tick = SystemClock.elapsedRealtime();
		elapsed += Math.abs(tick-lastTick);
		lastTick = tick;
		
		if (elapsed > orientationWindow) {
			// Process orientation.
			SensorManager.getRotationMatrixFromVector(mat, event.values);
			SensorManager.remapCoordinateSystem(mat, SensorManager.AXIS_X, SensorManager.AXIS_Z, rmat);
			SensorManager.getOrientation(rmat, angles);
			double ax = -angles[1]*TO_DEG;
			double ay = -angles[0]*TO_DEG; //azimuth, orientation around the z axis
			double az = -angles[2]*TO_DEG;
			rx.add(Math.abs(ax));
			ry.add(Math.abs(ay));
			rz.add(Math.abs(az));
			double x = rx.getAverage() * Math.signum(ax);
			double y = ry.getAverage() * Math.signum(ay);
			double z = rz.getAverage() * Math.signum(az);
			
			//handler.onNewOrientation(x, y, z);
			outputOrientation[0] = (float)x;
			outputOrientation[1] = (float)y;
			outputOrientation[2] = (float)z;
			
			elapsed = elapsed % orientationWindow;
			
			//Log.d("ORI","orientation at orientation class: " + outputOrientation[1]);
		}
	}

}
