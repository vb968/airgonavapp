package com.ascamm.motion.algorithm;

import com.ascamm.motion.utils.Point3DWithFloor;

public class PointWeight {
	
	private Point3DWithFloor mPoint;
	private double mWeight;
	
	public PointWeight(Point3DWithFloor point, double weight) {
		mPoint = point;
		mWeight = weight;
	}
	
	public Point3DWithFloor getPoint(){
		return mPoint;
	}

	public double getWeight(){
		return mWeight;
	}

}
