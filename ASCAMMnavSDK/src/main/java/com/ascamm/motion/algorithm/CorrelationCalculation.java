package com.ascamm.motion.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.util.Log;

import com.ascamm.motion.database.CalibrationItem;
import com.ascamm.motion.database.PointWithCalibrationItems;
import com.ascamm.motion.utils.Point3DWithFloor;
import com.ascamm.motion.utils.ResponseObject;

import com.ascamm.motion.utils.Constants;

public class CorrelationCalculation {

	private final String TAG = this.getClass().getSimpleName();
	
	private boolean debug = Constants.DEBUGMODE;
	
	private float DEFAULT_RSS = Constants.mDefaultRss;
	private float DEFAULT_RSS_STDEV = Constants.mDefaultRssStdev;
	private int MIN_COUNT = Constants.mMinCount;
	private float MIN_COUNT_PERCENTAGE = Constants.mMinCountPercentage;
	private int mKNumOfNeighbors = Constants.mSettingKNumOfNeighbors;
	
	private float minRSS = -150;
	private float maxRSS = 50;
	private float resolution = 1;
	
	private boolean usePenalization = false;
	private boolean useSumCorr = true;
	
	private ArrayList<PointCorrelation> pointsWithCorr;
	private double maxCorr = 0;
	
	public CorrelationCalculation(){
		
	}
	
	private void applyWeights(double corr){
		if(corr<=1 && corr>=0.5){
			corr *= 4;
		}
		else if(corr<0.5 && corr>=0.3){
			corr *= 2;
		}
		else if(corr<0.3 && corr>=0.1){
			// Do nothing
		}
		else{
			// 0<corr<0.1
			corr = 0;
		}
	
	}
	
	private double adjustRssStdev(double rssStdev, int count, float countPercentage){
		double minRssStdev = 0.5;
		if(countPercentage<MIN_COUNT_PERCENTAGE || count<MIN_COUNT){
			rssStdev = DEFAULT_RSS_STDEV;
		}
		else if(rssStdev<minRssStdev){
			rssStdev = minRssStdev;
		}
		return rssStdev;
	}

	/** update the correlation of a point */
	private double updatePointCorrelation(double pointCorrelation, double apCorrelation){
		if(useSumCorr){
			pointCorrelation += apCorrelation;
		}
		else{
			if(pointCorrelation==0){
				pointCorrelation = apCorrelation;
			}
			else{
				pointCorrelation *= apCorrelation;
			}
		}
		return pointCorrelation;
	}

	/** this method calculates the correlation between an online measure and an offline measure */
	private double calculateCorrelation(float rssAvgOnline, double rssStdevOnline, float rssAvgCalib, double rssStdevCalib){
		double corr = 0;
		float rss = minRSS;
		while(rss<=maxRSS){
			double apOnline = (1/(Math.sqrt(2*Math.PI)*rssStdevOnline)) * Math.exp(-1*(Math.pow((rss-rssAvgOnline),2))/(2*Math.pow(rssStdevOnline,2)));
			double apCalib = (1/(Math.sqrt(2*Math.PI)*rssStdevCalib)) * Math.exp(-1*(Math.pow((rss-rssAvgCalib),2))/(2*Math.pow(rssStdevCalib,2)));
			corr += apOnline*apCalib*resolution;
			/*
			if(corr>1){
				Log.d(TAG,"apOnline: " + apOnline + " , apCalib: " + apCalib + " , corr: " + corr + " , rssAvgOnline: " + rssAvgOnline + " , rssStdevOnline: " + rssStdevOnline + " , rssAvgCalib: " + rssAvgCalib + " , rssStdevCalib: " + rssStdevCalib);
			}
			*/
			rss += resolution;
		}
		if(corr>1){
			Log.d(TAG,"AP corr: " + corr + " , rssAvgOnline: " + rssAvgOnline + " , rssStdevOnline: " + rssStdevOnline + " , rssAvgCalib: " + rssAvgCalib + " , rssStdevCalib: " + rssStdevCalib);
		}
		//applyWeights(corr);
		//Log.d(TAG,"AP corr: " + corr);
		return corr;
	}

	/** this method calculates correlation of point
	 * @param cis	Calibration items (offline measures)
	 * @param onlineAPs	Online APs (online measures)
	 */
	public double calculatePointCorrelation(ArrayList<CalibrationItem> cis, ArrayList<APData> onlineAPs){
		double pointCorrelation = 0;
		double apCorrelation = 0;
		int pen1 = 0;
		int pen2 = 0;
		
		// First we only consider online measurements corresponding to APs of the system
		for(APData ap : onlineAPs){
			float rssCalib = DEFAULT_RSS;
			double rssStdevCalib = DEFAULT_RSS_STDEV;
			String bssidOnline = ap.getBSSID();
			float rssOnline = ap.getRSS();
			double rssStdevOnline = ap.getRSSstdev();
			//rssStdevOnline = adjustRssStdev(rssStdevOnline);	/* rssStdev already adjusted in PositionEstimation */
			boolean bssidFound = false;
			for(CalibrationItem ci : cis){
				if(ci.getBSSID().equalsIgnoreCase(bssidOnline)){
					//bssidOnline appears in this PointWithCalibrationItems
					rssCalib = ci.getRSS();
					rssStdevCalib = adjustRssStdev(ci.getRSSstdev(), ci.getCount(), ci.getPercentage());
					bssidFound = true;
					apCorrelation = calculateCorrelation(rssOnline, rssStdevOnline, rssCalib, rssStdevCalib);
					if(!Double.isNaN(apCorrelation) && apCorrelation!=0){
						pointCorrelation = updatePointCorrelation(pointCorrelation, apCorrelation);
					}
					break;
				}
			}
			if(!bssidFound && usePenalization){
				// we penalize when we have an online AP that is not in the calibration point (i.e. offline) data
				pen1 += 1;
				apCorrelation = calculateCorrelation(rssOnline, rssStdevOnline, rssCalib, rssStdevCalib);
				if(!Double.isNaN(apCorrelation) && apCorrelation!=0){
					pointCorrelation = updatePointCorrelation(pointCorrelation, apCorrelation);
				}
			}
		}
		
		// Then we penalize when we have an offline AP that is not in the online data
		if(usePenalization){
			for(CalibrationItem ci : cis){
				boolean bssidFound = false;
				for(APData ap : onlineAPs){
					if(ap.getBSSID().equalsIgnoreCase(ci.getBSSID())){
						bssidFound = true;
						break;
					}
				}
				if(!bssidFound){
					float rssCalib = ci.getRSS();
					double rssStdevCalib = adjustRssStdev(ci.getRSSstdev(), ci.getCount(), ci.getPercentage());
					float rssOnline = DEFAULT_RSS;
					float rssStdevOnline = DEFAULT_RSS_STDEV;
					pen2 += 1;
					
					// We calculate the posterior probability
					apCorrelation = calculateCorrelation(rssOnline, rssStdevOnline, rssCalib, rssStdevCalib);
					if(!Double.isNaN(apCorrelation) && apCorrelation!=0){
						pointCorrelation = updatePointCorrelation(pointCorrelation, apCorrelation);
					}
				}
			}
		}
		
		//Log.i(TAG,"corr: " + pointCorrelation + " , pen1: " + pen1 + " , pen2: " + pen2);
		
		return pointCorrelation;
	}

	/* this method calculates correlations of given points and sorts them by correlation */
	public ArrayList<PointCorrelation> calcCorrelations(ArrayList<PointWithCalibrationItems> calibrationSet, ArrayList<APData> onlineAPs){
		pointsWithCorr = new ArrayList<PointCorrelation>();
		double sumCorr = 0;
		for(PointWithCalibrationItems pwci : calibrationSet){
			//Log.i(TAG,"point: ( " + pwci.getPositionX() + " , " + pwci.getPositionY() + " , " + pwci.getPositionZ() + " )");
			
			ArrayList<CalibrationItem> cis = pwci.getCalibrationItems();
			double pointCorrelation = calculatePointCorrelation(cis, onlineAPs);
			//Log.d(TAG,"point corr: " + pointCorrelation);
			sumCorr += pointCorrelation;
			
			// We add the correlation of the Point3D to the list of probabilities
			pointsWithCorr.add(new PointCorrelation(pwci.getPoint(), pointCorrelation));
		}
		// Normalize correlations
		for(PointCorrelation pp : pointsWithCorr){
			double normCorr = pp.getCorrelation() / sumCorr;
			if(normCorr>maxCorr){
				maxCorr = normCorr;
			}
			pp.setCorrelation(normCorr);
			//System.out.println("point: " + pp.getPoint().toString() + " , prob: " + pp.getProbability() + " , id: " + pp.getPoint().getId());
		}
				
		Collections.sort(pointsWithCorr, new SortByCorrelation());
		
		return pointsWithCorr;
	}

	/* this method calculates an estimated position and also the N points with higher correlation */
	public ResponseObject calcPositionCorrelation(ArrayList<PointWithCalibrationItems> calibrationSet, ArrayList<APData> onlineAPs){
				
		ArrayList<PointCorrelation> pointsWithCorr = calcCorrelations(calibrationSet, onlineAPs);
				
		/* Find K points with highest correlation. 
		 * Then we use these K points to calculate the position
		 * with an optimal estimator given the correlations
		 * */
		
		int numCandidatesShow = 6;
		int numPointsWithCorr = pointsWithCorr.size();
		if(numPointsWithCorr<numCandidatesShow){
			numCandidatesShow = numPointsWithCorr; 
		}
		if(debug){
			for(int i=0; i<numCandidatesShow;i++){
				PointCorrelation p = pointsWithCorr.get(i);
				System.out.println("cand " + i + " : " + p.getPoint().toString() + " , corr: " + p.getCorrelation());
			}
		}
		
		double sumPosX=0 , sumPosY=0 , sumPosZ=0 , sumPosFloor=0 , sumCorr = 0;
		ArrayList<Point3DWithFloor> candidates = new ArrayList<>();
		int numNeighbors = mKNumOfNeighbors;
		if(pointsWithCorr.size() < numNeighbors){
			numNeighbors = pointsWithCorr.size(); 
		}
		for (int i=0; i<numNeighbors; i++){
			double posX	= pointsWithCorr.get(i).getPoint().x;
			double posY	= pointsWithCorr.get(i).getPoint().y;
			double posZ	= pointsWithCorr.get(i).getPoint().z;
			double posFloor = pointsWithCorr.get(i).getPoint().getFloor();
			double corr	= pointsWithCorr.get(i).getCorrelation();
			
			sumPosX += posX*corr;
			sumPosY += posY*corr;
			sumPosZ += posZ*corr;
			sumPosFloor += posFloor*corr;
			sumCorr += corr;
			candidates.add(new Point3DWithFloor(posX,posY,posZ,posFloor));
		}
		Point3DWithFloor estimatedPos = new Point3DWithFloor(sumPosX/sumCorr, sumPosY/sumCorr, sumPosZ/sumCorr, Math.round(sumPosFloor/sumCorr));
		/*
		System.out.println("estimated pos: (" + estimatedPos.x + " , " + estimatedPos.y + " , " + estimatedPos.z + " )");
		for(Point3D cand : candidates){
			System.out.println("candidate: (" + cand.x + " , " + cand.y + " , " + cand.z + " )");
		}
		*/
		return new ResponseObject(estimatedPos,candidates,null, null);
	}

	/** this method returns all the points with their correlation value */
	public ArrayList<PointWeight> getPointsWithCorr(){
		ArrayList<PointWeight> pointsWithWeight = new ArrayList<PointWeight>();
		for (PointCorrelation pc : pointsWithCorr){
			pointsWithWeight.add(new PointWeight(pc.getPoint(),pc.getCorrelation()/maxCorr));
		}
		return pointsWithWeight;
	}
	
	/*nested class for sorting object by Correlation IN DESC ORDER (signs of the 'one' changed with respect to ASCENDENT order)*/
	public class SortByCorrelation implements Comparator<PointCorrelation>{
		public int compare(PointCorrelation arg0, PointCorrelation arg1) {
			if ( arg0.getCorrelation() < arg1.getCorrelation()){
				return 1;
			} else if ( arg0.getCorrelation() == arg1.getCorrelation() ){
				return 0;	
			} else if ( arg0.getCorrelation() > arg1.getCorrelation() ){
				return -1;
			}
			return 0;
		}
	}
}
