package com.ascamm.motion.algorithm;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.ascamm.motion.database.DBHelper;
import com.ascamm.motion.utils.Edge;
import com.ascamm.motion.utils.Point3DWithFloor;
import com.ascamm.motion.utils.Point3DWithFloorAndEdge;
import com.ascamm.utils.Point3D;


/**
 * This class provides the fusion of the positioning output with the topological 
 * information of the environment. This information consists of the paths (defined by edges and nodes) 
 * that are allowed to be walked by the mobile target 
 * @author  ASCAMM
 */

public class MapFusion {
	public Hashtable <Integer,ArrayList<Edge>> mEdgesByFloor; //stores edges data separated by floor
	//DBHelper dBHelper;
	
	public MapFusion(DBHelper dBHelper){
		//System.out.println("creating MapFusion class");
		//load paths information from the database (edges table joint with nodes info) to memory
		mEdgesByFloor = dBHelper.getEdgesByFloor();
	}
	
	public MapFusion(Hashtable<Integer, ArrayList<Edge>> edgesByFloor) {
		this.mEdgesByFloor = edgesByFloor;
	}

	/**
	 * Load Map Fusion class generating edges from a path
	 * @param path path of points to construct map fusion from
	 */
	/*
	public MapFusion(List<Point3D> path) {

		Point3D lastPoint = null;
		Edge edge;
		this.mEdgesByFloor = new Hashtable <Integer,ArrayList<Edge>>();
		int i = 0;

		for(Point3D point: path) {
			if(lastPoint != null) {
				edge = new Edge(i, lastPoint.getId(), point.getId(), lastPoint.x, lastPoint.y, lastPoint.z, point.x, point.y, point.z, (float) getDistance(lastPoint, point));
				i++;
				if(!this.mEdgesByFloor.containsKey((int) point.z)) {
					this.mEdgesByFloor.put((int) point.z, new ArrayList<Edge>());
				}
				if(!this.mEdgesByFloor.containsKey((int) lastPoint.z)) {
					this.mEdgesByFloor.put((int) lastPoint.z, new ArrayList<Edge>());
				}
				if(lastPoint.z != point.z) {
					this.mEdgesByFloor.get((int) lastPoint.z).add(edge);
					this.mEdgesByFloor.get((int) point.z).add(edge);
				} else {
					this.mEdgesByFloor.get((int) point.z).add(edge);
				}
			}
			lastPoint = point;
		}
	}
	*/
	
	/**
	 * Function do path matching for a certain input position, given the defined paths (edges and nodes)
	 * @param input_pos - Point3D 
	 * @return output_pos - Point3D
	 */
	public Point3DWithFloorAndEdge doPathMatching(Point3DWithFloor input_pos){
		//obtain the point of the defined paths (edges) that is closer to the input_pos
		Point3DWithFloorAndEdge output_pos = calcClosestPointAtPaths(input_pos);
		//output_pos.z = input_pos.z;
		return output_pos;
	}
	
	/**
	 * Function calculate closest point (at the paths) to a certain point
	 * @param point - Point3D 
	 * @return Edge
	 */
	private Point3DWithFloorAndEdge calcClosestPointAtPaths(Point3DWithFloor point){
		double minDist = 10000;
		Point3DWithFloor outputPoint = new Point3DWithFloor();
		Point3DWithFloorAndEdge closestPoint = new Point3DWithFloorAndEdge();
		//System.out.println("point: " + point.toString() + " , mEdgesByFloor: " + mEdgesByFloor);
		for (Edge e : mEdgesByFloor.get((int)point.getFloor())){
			double dist = distanceBetweenPointAndEdge_andClosestPoint(point,e,outputPoint);
			if(dist<minDist){
				minDist = dist;
				closestPoint.set(outputPoint.x, outputPoint.y, outputPoint.z, outputPoint.getFloor(), e);
			}
		}
		return closestPoint;
	}
	
	/** calculate distance between point and an edge, and also return closest point **/
	public double distanceBetweenPointAndEdge_andClosestPoint(Point3DWithFloor inputPoint, Edge e, Point3DWithFloor closestPoint){
		double normDist_A_closestPoint;
		double dist;
		double vectorAP_x, vectorAP_y, vectorAB_x, vectorAB_y;
		double vectorAP_z, vectorAB_z, vectorAB_floor; //dani
		double magnitudeAB;
		double ABAPdotproduct;
		
		/*
		 * The edge is a line segment between two points A and B  
		 * (A is the startNode and B the endNode)
		 * The inputPoint is called point P
		*/	
		vectorAP_x = inputPoint.x - e.startNode.x;
		vectorAP_y = inputPoint.y - e.startNode.y;
		vectorAP_z = inputPoint.z - e.startNode.z; //dani
		vectorAB_x = e.endNode.x - e.startNode.x;
		vectorAB_y = e.endNode.y - e.startNode.y;
		vectorAB_z = e.endNode.z - e.startNode.z; //dani

		vectorAB_floor = e.endNode.getFloor() - e.startNode.getFloor(); //dani
		
		//magnitudeAB = Math.pow(vectorAB_x,2) + Math.pow(vectorAB_y,2);
		//ABAPdotproduct = vectorAB_x*vectorAP_x + vectorAB_y*vectorAP_y;
		magnitudeAB = Math.pow(vectorAB_x,2) + Math.pow(vectorAB_y,2) + Math.pow(vectorAB_z,2); //dani
		ABAPdotproduct = vectorAB_x*vectorAP_x + vectorAB_y*vectorAP_y + vectorAB_z*vectorAP_z; //dani
		//The normalized "distance" from A to the closest point:
		normDist_A_closestPoint = ABAPdotproduct / magnitudeAB;	
		
		if(normDist_A_closestPoint < 0){
			//the projection of the inputPoint is out of edge bounds (first region)
			closestPoint.x = e.startNode.x;
			closestPoint.y = e.startNode.y;
			closestPoint.z = e.startNode.z; //dani
			closestPoint.setFloor(e.startNode.getFloor());
			//dist = Math.sqrt(Math.pow(vectorAP_x,2) + Math.pow(vectorAP_y,2));
			dist = Math.sqrt(Math.pow(vectorAP_x,2) + Math.pow(vectorAP_y,2) + Math.pow(vectorAP_z,2)); //dani
		}else if(normDist_A_closestPoint > 1){
			//the projection of the inputPoint is out of edge bounds (second region)
			closestPoint.x = e.endNode.x;
			closestPoint.y = e.endNode.y;
			closestPoint.z = e.endNode.z; //dani
			closestPoint.setFloor(e.endNode.getFloor());
			//dist = Math.sqrt(Math.pow(inputPoint.x - e.endNode.x,2) + Math.pow(inputPoint.y - e.endNode.y,2));
			dist = Math.sqrt(Math.pow(inputPoint.x - e.endNode.x,2) + Math.pow(inputPoint.y - e.endNode.y,2) + Math.pow(inputPoint.z - e.endNode.z,2)); //dani
		}else{
			//the projection of the inputPoint belongs to the edge
			closestPoint.x = e.startNode.x + vectorAB_x*normDist_A_closestPoint;
			closestPoint.y = e.startNode.y + vectorAB_y*normDist_A_closestPoint;
			closestPoint.z = e.startNode.z + vectorAB_z*normDist_A_closestPoint; //dani
			closestPoint.setFloor(e.startNode.getFloor() + vectorAB_floor*normDist_A_closestPoint); //dani
			//dist = Math.sqrt(Math.pow(inputPoint.x - closestPoint.x,2) + Math.pow(inputPoint.y - closestPoint.y,2));
			dist = Math.sqrt(Math.pow(inputPoint.x - closestPoint.x,2) + Math.pow(inputPoint.y - closestPoint.y,2) + Math.pow(inputPoint.z - closestPoint.z,2)); //dani
		}
		//closestPoint.z = inputPoint.z;
		return dist;		
	}

	private double getDistance(Point3D start, Point3D end) {
		return Math.sqrt(Math.pow(end.x-start.x, 2) + Math.pow(end.y-start.y, 2) + Math.pow(end.z-start.z, 2));
	}
}
