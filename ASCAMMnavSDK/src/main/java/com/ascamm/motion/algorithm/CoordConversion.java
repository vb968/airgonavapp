package com.ascamm.motion.algorithm;

import com.ascamm.utils.Point3D;
import com.ascamm.utils.LatLng3D;

/**
 * Class used to convert coordinates from global to local and from local to global
 *
 * @author Arnau Alcázar Lleopart <arnau.alcazar@eurecat.org>
 *
 */

public class CoordConversion {

    private double m11, m12, m21, m22, latRef, lngRef;

    //inverse matrix
    private double i11, i12, i21, i22;

    public CoordConversion(double m11, double m12, double m21, double m22, double latRef, double lngRef) {
        this.m11 = m11;
        this.m12 = m12;
        this.m21 = m21;
        this.m22 = m22;
        this.latRef = latRef;
        this.lngRef = lngRef;

        //calculate the inverse, to convert from global to local
        double idet = 1/(m11*m22-m12*m21);
        this.i11 = idet*m22;
        this.i12 = (-1)*idet*m12;
        this.i21 = (-1)*idet*m21;
        this.i22 = idet*m11;
    }

    public LatLng3D convertFromLocalToGlobal(Point3D point) {
        double lng = m11*point.x + m12*point.y + lngRef;
        double lat = m21*point.x + m22*point.y + latRef;

        return new LatLng3D(lat, lng, point.z);
    }

    public Point3D convertFromGlobalToLocal(LatLng3D point) {
        double x = i11*(point.getLng() - this.lngRef) + i12*(point.getLat() - this.latRef);
        double y = i21*(point.getLng() - this.lngRef) + i22*(point.getLat() - this.latRef);
        //double x = i11*(point.getLat() - this.latRef) + i12*(point.getLng() - this.lngRef);
        //double y = i21*(point.getLat() - this.latRef) + i22*(point.getLng() - this.lngRef);

        return new Point3D(x, y, point.getZ());
    }

    public String toString(){
        return "[["+m11+","+m12+"],["+m21+","+m22+"]], latRef: " + latRef + " , lngRef: " + lngRef;
    }

}