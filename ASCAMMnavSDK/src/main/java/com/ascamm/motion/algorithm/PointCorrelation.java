package com.ascamm.motion.algorithm;

import com.ascamm.motion.utils.Point3DWithFloor;

public class PointCorrelation {
	
	private Point3DWithFloor mPoint;
	private double mCorrelation;
	
	public PointCorrelation(Point3DWithFloor point, double correlation) {
		mPoint = point;
		mCorrelation = correlation;
	}
	
	public Point3DWithFloor getPoint() {
		return mPoint;
	}

	public void setPoint(Point3DWithFloor mPoint) {
		this.mPoint = mPoint;
	}

	public double getCorrelation() {
		return mCorrelation;
	}

	public void setCorrelation(double correlation) {
		this.mCorrelation = correlation;
	}
}
