package com.ascamm.motion.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.util.FloatMath;
import android.util.Log;

import com.ascamm.motion.utils.LogWriter;
import com.ascamm.motion.database.CalibrationItem;
import com.ascamm.motion.database.PointWithCalibrationItems;
import com.ascamm.motion.utils.Point3DWithFloor;
import com.ascamm.motion.utils.ResponseObject;

import com.ascamm.motion.utils.Constants;
import com.ascamm.motion.utils.Settings;
import com.ascamm.motion.utils.Util;
import com.ascamm.utils.Point3D;

public class ProbabilityCalculation {

	private final String TAG = this.getClass().getSimpleName();
	
	private boolean debug = Constants.DEBUGMODE;
	
	private float DEFAULT_RSS = Constants.mDefaultRss;
	private float DEFAULT_RSS_STDEV = Constants.mDefaultRssStdev;
	private int MIN_COUNT = Constants.mMinCount;
	private float MIN_COUNT_PERCENTAGE = Constants.mMinCountPercentage;
	private int mKNumOfNeighbors = Constants.mSettingKNumOfNeighbors;
	
	private boolean outdoorDet = Constants.mOutdoorDet;
	private static String outdoorDetMethod = Constants.mOutdoorDetMethod;
	private static double outdoorProbThreshold = Constants.mOutdoorProbThreshold;
	private static int outdoorProbNumPoints = Constants.mOutdoorProbNumPoints;
	
	private static int INIT_PROB = -1;
	private ArrayList<PointProbability> pointsWithProb;
	private double maxProb = 0;
	private ResponseObject resEmpty = new ResponseObject(new Point3DWithFloor(0,0,0,0),null, new Point3D(0,0,0),null);
	
	private boolean usePenalization = false;
	private boolean useSumProb = false;
	private boolean normalizeProb = false;
	private boolean discardSmallProb = false;
	private double MIN_PROB = 0.001;
	private boolean useMaxAPs = false;
	private int MAX_APS = 10;
	private boolean useMinAPs = false;
	private double MIN_PERC_APS = 0.9;
	private boolean useRssWeights = false;
	private boolean useProbWeights = false;
	private boolean useProbByRssStdev = false;
	private boolean useMatches = false;
	private double MIN_PERC_MATCHES = 0.75;
	private boolean useProbWithLimit = false;
	private boolean applyWeightsByRss = false;
	private int N_STD = 2;
	private int M_STD = 5;

	// prior probability parameters
	private boolean use_Prior_LastEpochPosterior = false;
	private ArrayList<PointProbability> pointsWithProb_LastEpoch = null;
	private boolean use_Prior_Distance = true;
	private Point3DWithFloor lastEstimatedPos = null;
	private long lastEstimatedPosTs = -1;
	private float priorDistanceNonWalking = 0;
	private float priorDistanceWalking = 0;
	private double priorWeightNonWalking = Math.pow(10, -4);
	private double priorWeightWalking = Math.pow(10, -4);
	//private double priorWeightWalking = 0.9;
	
	/** Code added for logs **/
	private LogWriter mLogWriter;
	private boolean logsRunning = false, storeLogs = Settings.storeLogs;
	private String logsFilename = Settings.logsFilename + "_prob" + Constants.logsFilenameExt;
	
	public ProbabilityCalculation(){
		/** Code added to store data info into logs.txt **/
		if(storeLogs){
			mLogWriter =new LogWriter(Constants.APP_DATA_FOLDER_NAME, logsFilename);
			if(mLogWriter.startLogs()>0){
				logsRunning = true;
			}
		}
	}
	
	public void stop(){
		/** Code added to store data info into logs.txt **/
		if(storeLogs && logsRunning){
			mLogWriter.stopLogs();
			logsRunning = false;
		}
	}
	
	public void setPriorDistance(float priorDistNonWalking, float priorDistWalking){
		//priorDistanceNonWalking = 1*priorDist;
		//priorDistanceWalking = 4*priorDist;
		priorDistanceNonWalking = priorDistNonWalking;
		priorDistanceWalking = priorDistWalking;
	}
	
	private double adjustRssStdev(double rssStdev, int count, float countPercentage){
		/*
		if(countPercentage<MIN_COUNT_PERCENTAGE || count<MIN_COUNT){
			//double rssStdevOld = rssStdev;
			rssStdev = DEFAULT_RSS_STDEV;
			//Log.d(TAG,"rssStdev adjusted for count: (" + rssStdevOld + " , " + rssStdev + ") , (" + count + " , " + countPercentage + ")");
		}
		else if(rssStdev==0){
			//rssStdev = Math.pow(10,-16);
			rssStdev = 0.1;
			//Log.d(TAG,"rssStdev adjusted for being 0. new rssStdev: " + rssStdev);
		}
		else if(rssStdev > DEFAULT_RSS_STDEV){
			rssStdev = DEFAULT_RSS_STDEV;
		}
		*/
		return rssStdev;
	}

	/*
	private double adjustRssStdev(double rssStdev, int count, float countPercentage){
		return DEFAULT_RSS_STDEV;
	}
	*/

	/** update the probability of a point */
	private double updatePointProbability(double pointProbability, double apProbability){
		if(pointProbability==INIT_PROB){
			pointProbability = apProbability;
		}
		else{
			if(useSumProb){
				pointProbability += apProbability;
			}
			else{
				if(discardSmallProb){
					if(apProbability >= MIN_PROB){
						pointProbability *= apProbability;
					}
					else{
						Log.d(TAG, "probability too small: " + apProbability);
					}
				}
				else{
					pointProbability *= apProbability;
				}
			}
		}
		return pointProbability;
	}

	/** evaluate online average RSS into offline gaussian defined by average and standard deviation */
	private double calculateApProb(float rssAvgOnline, float rssAvgCalib, double rssStdevCalib){
		double apProb = (1/(Math.sqrt(2*Math.PI)*rssStdevCalib)) * Math.exp(-1*(Math.pow((rssAvgCalib-rssAvgOnline),2))/(2*Math.pow(rssStdevCalib,2)));
		apProb += 1; // add one to probability to convert range [0,1] to [1,2]
		return apProb;
	}

	/** divide RSS range into small ranges and assign a value to each of them depending on how closer online RSS is to offline RSS */
	private double calculateApRssWeights(float rssOnline, float rssCalib, double rssStdevCalib){
		double apProb = 0.1;
		int N = 20;
		int M = 20;
		int i;
		for(i=1; i<=N; i++){
			if((rssOnline <= (rssCalib + ((float)i/N)*N_STD*rssStdevCalib)) && (rssOnline >= (rssCalib - ((float)i/N)*N_STD*rssStdevCalib))){
				break;
			}
		}
		if(i<N){
			// apply weights higher than 1
			apProb = (N - i);
			//apProb = Math.pow((N - i),2);
		}
		else{
			
			// apply weights lower than 1
			for(i=1; i<=M; i++){
				if((rssOnline <= (rssCalib + (N_STD + ((float)i/M)*M_STD)*rssStdevCalib)) && (rssOnline >= (rssCalib - (N_STD + ((float)i/M)*M_STD)*rssStdevCalib))){
					break;
				}
			}
			if(i<M){
				apProb = (float)(M - i)/M;
			}
			else{
				apProb = 1/M;
			}
			
			// apply probability lower than 1
			//apProb = calculateApProbByRssStdev(rssOnline, rssCalib, rssStdevCalib);
		}
		apProb *= apProb;
		return apProb;
	}

	/** divide offline gaussian variable function into small ranges and assign a value to each of them depending on how closer online probability is to maximum probability of the offline gaussian function */
	private double calculateApProbWeights(float rssOnline, float rssCalib, double rssStdevCalib){
		double weightedApProb = 0;
		double apProb = calculateApProb(rssOnline, rssCalib, rssStdevCalib);
		double maxApProb = calculateApProb(rssCalib, rssCalib, rssStdevCalib);
		int N = 50;
		int i;
		for(i=N-1; i>0; i--){
			if(apProb >= ((double)i/N)*maxApProb){
				break;
			}
		}
		weightedApProb = i;
		return weightedApProb;
	}

	/** normalize offline gaussian function and calculate probability */
	private double calculateApProbByRssStdev(float rssOnline, float rssCalib, double rssStdevCalib){
		double apProb = calculateApProb(rssOnline, rssCalib, rssStdevCalib);
		/*
		if((rssOnline <= (rssCalib + N_STD*rssStdevCalib)) && (rssOnline >= (rssCalib - N_STD*rssStdevCalib))){
			apProb *= 100;
		}
		*/
		double apProbNstd = calculateApProb(rssCalib + (float)(N_STD*rssStdevCalib), rssCalib, rssStdevCalib);
		apProb /= apProbNstd; 
		return apProb;
	}

	/** if online measure is out of a range defined, online measures is forced to a value (i.e. large errors are avoided) */
	private double calculateApProbWithLimit(float rssAvgOnline, float rssAvgCalib, double rssStdevCalib){
		if((rssAvgOnline > (rssAvgCalib + N_STD*rssStdevCalib)) || (rssAvgOnline < (rssAvgCalib - N_STD*rssStdevCalib))){
			rssAvgOnline = rssAvgCalib + (float)(N_STD*rssStdevCalib);
			Log.i(TAG,"rssOnline modified");
		}
		double apProb = (1/(Math.sqrt(2*Math.PI)*rssStdevCalib)) * Math.exp(-1*(Math.pow((rssAvgCalib-rssAvgOnline),2))/(2*Math.pow(rssStdevCalib,2)));
		//apProb += 1;
		return apProb;
	}

	/** assign weights depending on the difference of online and offline RSS measures */
	private double calculateWeightsByRss(float rssRefOnline, float rssOnline, float rssRefCalib, float rssCalib){
		double weight = 1;
		float rssDiffOnline = rssRefOnline - rssOnline;
		float rssDiffCalib = rssRefCalib - rssCalib;
		/*
		if(rssDiffOnline < 10){
			weight = 4;
		}
		else if(rssDiffOnline < 20){
			weight = 3;
		}
		else if(rssDiffOnline < 30){
			weight = 2;
		}
		*/
		if(rssDiffOnline < 5 && rssDiffCalib < 5){
			weight = 16;
		}
		else if(rssDiffOnline < 10 && rssDiffCalib < 10){
			weight = 8;
		}
		else if(rssDiffOnline < 15&& rssDiffCalib < 15){
			weight = 4;
		}
		else if(rssDiffOnline < 20 && rssDiffCalib < 20){
			weight = 2;
		}
		/*
		else if(rssDiffOnline < 25){
			weight = 4;
		}
		else if(rssDiffOnline < 30){
			weight = 3;
		}
		else if(rssDiffOnline < 35){
			weight = 2;
		}
		*/
		return weight;
	}

	/** this method calculates probability of point
	 * @param cis	Calibration items (offline measures)
	 * @param onlineAPs	Online APs (online measures)
	 * @param showLogs
	 */
	public ProbAndMatches calculatePosteriorProb(ArrayList<CalibrationItem> cis, ArrayList<APData> onlineAPs, boolean showLogs){
		double pointPostProb = INIT_PROB;
		double apPostProb = 0;
		int pen1 = 0;
		int pen2 = 0;
		int numAPs = 0;
		float rssRefOnline = onlineAPs.get(0).getRSS();
		float rssRefCalib = cis.get(0).getRSS();
		int numMatches = 0;
		
		if(cis==null){
			Log.e(TAG,"cis is null");
			//return 0.0;
			return new ProbAndMatches(0,0);
		}
		// First we only consider online measurements corresponding to APs of the system
		for(APData ap : onlineAPs){
			float rssCalib = DEFAULT_RSS;
			double rssStdevCalib = DEFAULT_RSS_STDEV;
			float rssOnline = ap.getRSS();
			String bssidOnline = ap.getBSSID();
			boolean bssidFound = false;
			for(CalibrationItem ci : cis){
				if(ci.getBSSID().equalsIgnoreCase(bssidOnline)){
					//bssidOnline appears in this PointWithCalibrationItems
					rssCalib = ci.getRSS();
					//Log.d(TAG, "AP found: (" + ci.getBSSID() + "," + ci.getRSS() + "," + ci.getRSSstdev() + "," + ci.getCount() + "," + ci.getPercentage() + ")");
					rssStdevCalib = adjustRssStdev(ci.getRSSstdev(),ci.getCount(),ci.getPercentage());
					/*
					if(ci.getCount()>(MAX_COUNT/2)){
						rssStdevCalib = ci.getRSSstdev();
						if(rssStdevCalib==0){
							//rssStdevCalib = 10^-16;
							rssStdevCalib = Math.pow(10,-16);
						}
					}
					*/
					bssidFound = true;
					if(useRssWeights){
						apPostProb = calculateApRssWeights(rssOnline, rssCalib, rssStdevCalib);
					}
					else if(useProbWeights){
						apPostProb = calculateApProbWeights(rssOnline, rssCalib, rssStdevCalib);
					}
					else if(useProbByRssStdev){
						apPostProb = calculateApProbByRssStdev(rssOnline, rssCalib, rssStdevCalib);
					}
					else if(useProbWithLimit){
						apPostProb = calculateApProbWithLimit(rssOnline, rssCalib, rssStdevCalib);
					}
					else{
						apPostProb = calculateApProb(rssOnline, rssCalib, rssStdevCalib);
					}
					
					if(applyWeightsByRss){
						double weight = calculateWeightsByRss(rssRefOnline, rssOnline, rssRefCalib, rssCalib);
						apPostProb *= weight;
					}
					
					if(showLogs){
						Log.d(TAG,"id: " + bssidOnline + " , prob: " + apPostProb);
					}
					
					//pointPostProb = pointPostProb * apPostProb;
					pointPostProb = updatePointProbability(pointPostProb, apPostProb);
					numAPs++;
					numMatches++;
					break;
				}
			}
			if(!bssidFound && usePenalization){
				// we penalize when we have an online AP that is not in the calibration point (i.e. offline) data
				pen1 += 1;
				//double rssStdevOnline = ap.getRSSstdev();
				double rssStdevOnline = rssStdevCalib;
				apPostProb = calculateApProb(rssOnline, rssCalib, rssStdevOnline);
				//pointPostProb = pointPostProb * apPostProb;
				pointPostProb = updatePointProbability(pointPostProb, apPostProb);
				numAPs++;
			}
			if(useMaxAPs && numAPs>=MAX_APS){
				break;
			}
		}
		
		// Then we penalize when we have an offline AP that is not in the online data
		if(usePenalization){
			for(CalibrationItem ci : cis){
				boolean bssidFound = false;
				for(APData ap : onlineAPs){
					if(ap.getBSSID().equalsIgnoreCase(ci.getBSSID())){
						bssidFound = true;
						break;
					}
				}
				if(!bssidFound){
					float rssCalib = ci.getRSS();
					double rssStdevCalib = adjustRssStdev(ci.getRSSstdev(),ci.getCount(),ci.getPercentage());
					/*
					double rssStdevCalib = DEFAULT_RSS_STDEV;
					if(ci.getCount()>(MAX_COUNT/2)){
						rssStdevCalib = ci.getRSSstdev();
						if(rssStdevCalib==0){
							rssStdevCalib = Math.pow(10,-16);
						}
					}
					*/
					float rssOnline = DEFAULT_RSS;
					pen2 += 1;
					
					// We calculate the posterior probability
					apPostProb = calculateApProb(rssOnline, rssCalib, rssStdevCalib);
					//pointPostProb = pointPostProb * apPostProb;
					pointPostProb = updatePointProbability(pointPostProb, apPostProb);
					numAPs++;
				}
			}
		}
		
		if(useMinAPs && numAPs<=MIN_PERC_APS*onlineAPs.size()){
			// Discard this point
			pointPostProb = 0;
		}
		if(normalizeProb){
			pointPostProb = pointPostProb/numAPs;
		}
		if(showLogs){
			Log.d(TAG,"prob: " + pointPostProb + " , pen1: " + pen1 + " , pen2: " + pen2 + " , numAPs: " + numAPs);
		}
		//return pointPostProb;
		return new ProbAndMatches(pointPostProb, numMatches);
	}

	/* this method calculates probabilities of given points and sorts them by probability */
	public ArrayList<PointProbability> calcProbabilities(ArrayList<PointWithCalibrationItems> calibrationSet, ArrayList<APData> onlineAPs, boolean useRssDiff, float onlineApRssRef){
		if(useRssDiff){
			//DEFAULT_RSS = -100;
			DEFAULT_RSS = Constants.mDefaultRss - onlineApRssRef;
		}
		else{
			DEFAULT_RSS = Constants.mDefaultRss;
		}
		//Log.d(TAG,"DEFAULT_RSS: " + DEFAULT_RSS);
		ArrayList<PointProbability> pointsWithProb = new ArrayList<PointProbability>();
		double sumProb = 0;
		maxProb = 0;
		int maxMatches = 0;
		int minMatches = 10000;
		for(PointWithCalibrationItems pwci : calibrationSet){
			
			ArrayList<CalibrationItem> cis = pwci.getCalibrationItems();
			boolean showLogs = false;
			/*
			if(pwci.getPoint().getId()==330 || pwci.getPoint().getId()==515){
				showLogs = true;
			}
			*/
			//double pointPostProb = calculatePosteriorProb(cis, onlineAPs, showLogs);
			ProbAndMatches probAndMatches = calculatePosteriorProb(cis, onlineAPs, showLogs);
			double pointPostProb = probAndMatches.prob;
			sumProb += pointPostProb;
			int numMatches = probAndMatches.matches;
			if(numMatches>maxMatches){
				maxMatches = numMatches;
			}
			if(numMatches<minMatches){
				minMatches = numMatches;
			}
			// We add the probability of the Point3D to the list of probabilities
			if(showLogs){
				Log.i(TAG,"id: " + pwci.getPoint().getId() + " , point: " + pwci.getPoint().toString() + " , prob: " + pointPostProb + " , matches: " + numMatches);
			}
			pointsWithProb.add(new PointProbability(pwci.getPoint(), pointPostProb, numMatches));
		}
		//Log.i(TAG,"minMatches: " + minMatches + " , maxMatches: " + maxMatches);
		if(useMatches){
			for(PointProbability pp : pointsWithProb){
				if(pp.getMatches()<MIN_PERC_MATCHES*maxMatches){
					pp.setProbability(0);
				}
			}
		}
		
		// Normalize probabilities
		for(PointProbability pp : pointsWithProb){
			double normProb = pp.getProbability() / sumProb;
			if(normProb>maxProb){
				maxProb = normProb;
			}
			pp.setProbability(normProb);
			//System.out.println("point: " + pp.getPoint().toString() + " , prob: " + pp.getProbability() + " , id: " + pp.getPoint().getId());
		}
		
		Collections.sort(pointsWithProb, new SortByProbability());
		
		return pointsWithProb;
	}
	
	/** get index of the calibration point that corresponds to a given position **/
	private static int getIndex(ArrayList<PointProbability> pointsWithProbability, Point3D position){
		for (int i=0; i<pointsWithProbability.size(); i++){
			if (pointsWithProbability.get(i).getPoint().equals(position)){
				return i;
			}
		}
		return -1;
	}
	
	/* this method calculates an estimated position and also the N more probable points */
	public ResponseObject calcPositionProb(ArrayList<PointWithCalibrationItems> calibrationSet, ArrayList<APData> onlineAPs, boolean useRssDiff, float onlineApRssRef, boolean usePrior, boolean walking, boolean calculateEstimatedError){
		long currentTs = System.currentTimeMillis();
		//Log.d(TAG,"inside calcPositionProb");
		pointsWithProb = calcProbabilities(calibrationSet, onlineAPs, useRssDiff, onlineApRssRef);
		float priorDistResp = 0;
		
		if(usePrior){
			if(use_Prior_LastEpochPosterior){
				
				/*
				ArrayList<PointProbability> pointsWithProb_copy = new ArrayList<PointProbability>();
				for(PointProbability pwb : pointsWithProb){
					pointsWithProb_copy.add((PointProbability)pwb.clone());
				}
				*/
				
				if(this.pointsWithProb_LastEpoch != null){
					
					//double sumProb = 0.0;
					double avgProb = 0.0;
					for(PointProbability pwb : this.pointsWithProb_LastEpoch){
						avgProb += pwb.getProbability();
					}
					avgProb = avgProb / this.pointsWithProb_LastEpoch.size();
					
					float weight = 0.0f;
					int size = this.pointsWithProb_LastEpoch.size();
					
					
					for(PointProbability pwb : pointsWithProb){
						
						int j = getIndex(this.pointsWithProb_LastEpoch, pwb.getPoint());
						if(j>=0){
							//pwb.setProbability(pwb.getProbability() * pointsWithProb_LastEpoch.get(j).getProbability());
							/*
							if(j<3){
								pwb.setProbability(pwb.getProbability());
							}else{
								pwb.setProbability(pwb.getProbability() * 0.1);
							}
							*/
							
							weight = 1 - ((float)j / size);
							pwb.setProbability(pwb.getProbability() * weight);
							
						}else{
							//this calibration point was not in the last epoch subset
							//pwb.setProbability(pwb.getProbability() * avgProb);
							//pwb.setProbability(pwb.getProbability() * 0.1);
							pwb.setProbability(pwb.getProbability() * 0.5);
						}
						//sumProb += pwb.getProbability();
					}
					/*
					for(PointProbability pwb : pointsWithProb){
						pwb.setProbability(pwb.getProbability() / sumProb);
					}
					*/								
					Collections.sort(pointsWithProb, new SortByProbability());
				}
				this.pointsWithProb_LastEpoch = pointsWithProb;
			}
			
			if(use_Prior_Distance){
				/*
				double sumProb2 = 0.0;
				for(PointProbability pwb : pointsWithProb){
					sumProb2 += pwb.getProbability();
				}
				*/
				//double sumProbDist = 0;
				//ArrayList<PointProbability> selectedPoints = new ArrayList<PointProbability>();
				if(lastEstimatedPos != null){
					float priorDistance = priorDistanceNonWalking;
					double priorWeight = priorWeightNonWalking;
					if(walking){
						float diffTs = 1;
						// Activate next 3 lines to activate diffTs when user is walking
						if(lastEstimatedPosTs!=-1){
							diffTs = (currentTs - lastEstimatedPosTs)/(float)1000;
						}
						
						priorDistance = diffTs*priorDistanceWalking;
						priorWeight = priorWeightWalking;
						//Log.d(TAG,"priorDistanceWalking: " + priorDistance + " , diffTs: " + diffTs);
					}
					priorDistResp = priorDistance;
					/*
					for(PointProbability pwb : pointsWithProb){
						float dist = distanceBetweenPoints(pwb.getPoint(), lastEstimatedPos);
						if(dist<50){
							selectedPoints.add(pwb);
							sumProbDist += pwb.getProbability();
						}	
					}					
					*/
					
					for(PointProbability pwb : pointsWithProb){
						float dist = (float) Util.distanceBetweenPoints(pwb.getPoint(), lastEstimatedPos);
						
						if(dist>priorDistance){
							pwb.setProbability(pwb.getProbability() * priorWeight);
						}
						
						/*
						if(dist>100){
							pwb.setProbability(pwb.getProbability() * 0.05);
						}else if(dist>50){
							pwb.setProbability(pwb.getProbability() * 0.1);
						}
						*/
					}					
				}
				
				/*
				if(selectedPoints.size() > 0){
					for(PointProbability pwb : selectedPoints){
						pwb.setProbability(pwb.getProbability() / sumProbDist);
					}
					pointsWithProb = selectedPoints;
					Log.i(TAG,"selected Points size :" + selectedPoints.size());
				}else{
					Log.i(TAG,"NO selected Points");
				}
				*/
				Collections.sort(pointsWithProb, new SortByProbability());
			}
		}
		
		int numPointsWithProb = pointsWithProb.size();
		
		// Code added to store data info into logs.txt
		if(storeLogs && logsRunning){
			long ts = System.currentTimeMillis();
			// obtain candidates id
			int numCandidatesLog = 10;
			ArrayList<Integer> candidatesId = new ArrayList<Integer>();
			if(numPointsWithProb<numCandidatesLog){
				numCandidatesLog = numPointsWithProb; 
			}
			for(int i=0; i<numCandidatesLog;i++){
				PointProbability p = pointsWithProb.get(i);
				Log.i(TAG,"cand " + i + " : " + p.getPoint().toString() + " , prob: " + p.getProbability() + " , id: " + p.getPoint().getId());
				candidatesId.add(p.getPoint().getId());
			}
			// obtain candidates distance and probability
			int numCandidatesDist = 3;
			ArrayList<Double> candidatesDist = new ArrayList<Double>();
			ArrayList<Double> candidatesProb = new ArrayList<Double>();
			if(numPointsWithProb<numCandidatesDist){
				numCandidatesDist = numPointsWithProb; 
			}
			for(int i=0; i<numCandidatesDist;i++){
				PointProbability p = pointsWithProb.get(i);
				if(i==1){
					double dist10 = p.getPoint().distanceTo(pointsWithProb.get(0).getPoint());
					Log.i(TAG,"dist10 : " + dist10);
					candidatesDist.add(dist10);
				}
				else if(i==2){
					double dist20 = p.getPoint().distanceTo(pointsWithProb.get(0).getPoint());
					Log.i(TAG,"dist20 : " + dist20);
					candidatesDist.add(dist20);
					double dist21 = p.getPoint().distanceTo(pointsWithProb.get(1).getPoint());
					Log.i(TAG,"dist21 : " + dist21);
					candidatesDist.add(dist21);
				}
				candidatesProb.add(p.getProbability());
				Log.i(TAG,"cand " + i + ". prob: " + p.getProbability() + " , id: " + p.getPoint().getId());
			}
			mLogWriter.addProbMeasuresLog(ts, candidatesId, candidatesDist, candidatesProb);
		}
	
		/* Find K points with highest probability. 
		 * Then we use these K points to calculate the position
		 * with an optimal estimator given the probabilities
		 * */
		if(debug){
			int numCandidatesShow = 6;
			if(numPointsWithProb<numCandidatesShow){
				numCandidatesShow = numPointsWithProb;
			}
			Log.i(TAG,"numPointsWithProb :" + numPointsWithProb);
			for(int i=0; i<numCandidatesShow;i++){
				PointProbability p = pointsWithProb.get(i);				
				Log.i(TAG,"cand " + i + " : " + p.getPoint().toString() + " , prob: " + p.getProbability() + " , id: " + p.getPoint().getId());
			}
		}
				
		if(outdoorDet && outdoorDetMethod.equalsIgnoreCase("PROB")){
			/* This method considers device out of the system area when all the first N points are below a threshold
			boolean isOut = true;
			int numPoints = outdoorProbNumPoints;
			if(numPointsWithProb < numPoints){
				numPoints = numPointsWithProb; 
			}
			for(int i = 0; i < numPoints; i++){
				if(pointsWithProb.get(i).getProbability() >= outdoorProbThreshold){
					isOut = false;
					break;
				}
			}
			if(isOut){
				Log.d(TAG,"Too low probability. We consider device out of the system area");
				return resEmpty;
			}
			*/
			
			//this method considers device out of the system area when one of the first N points are below a threshold
			boolean isOut = false;
			int numPoints = outdoorProbNumPoints;
			if(numPointsWithProb < numPoints){
				numPoints = numPointsWithProb; 
			}
			if(pointsWithProb.get(numPoints-1).getProbability() < outdoorProbThreshold){
				isOut = true;
			}
			if(isOut){
				Log.d(TAG,"Too low probability. We consider device out of the system area");
				return resEmpty;
			}
		}
		
		double sumPosX=0 , sumPosY=0 , sumPosZ=0 , sumPosFloor=0 , sumProb = 0;
		ArrayList<Point3DWithFloor> candidates = new ArrayList<>();
		
		int numNeighbors = mKNumOfNeighbors;
		//int numNeighbors = pointsWithProb.size(); //weighted mean of the posterior
		
		if(numPointsWithProb < numNeighbors){
			numNeighbors = pointsWithProb.size(); 
		}
		for (int i=0; i<numNeighbors; i++){
			int id = pointsWithProb.get(i).getPoint().getId();
			double posX	= pointsWithProb.get(i).getPoint().x;
			double posY	= pointsWithProb.get(i).getPoint().y;
			double posZ	= pointsWithProb.get(i).getPoint().z;
			double posFloor = pointsWithProb.get(i).getPoint().getFloor();
			double prob	= pointsWithProb.get(i).getProbability();
			
			sumPosX += posX*prob;
			sumPosY += posY*prob;
			sumPosZ += posZ*prob;
			sumPosFloor += posFloor*prob;
			sumProb += prob;
			Point3DWithFloor cand = new Point3DWithFloor(posX,posY,posZ,Math.round(posFloor));
			cand.setId(id);
			candidates.add(cand);
		}
		Point3DWithFloor estimatedPos = new Point3DWithFloor(sumPosX/sumProb, sumPosY/sumProb, sumPosZ/sumProb, Math.round(sumPosFloor/sumProb));

		//commented because now it is updated from PositionEstimation after PF and Map Fusion:
		/*
		if(usePrior){
			lastEstimatedPos = estimatedPos; 
			lastEstimatedPosTs = currentTs;
		}
		*/

		/*
		System.out.println("estimated pos: (" + estimatedPos.x + " , " + estimatedPos.y + " , " + estimatedPos.z + " )");
		for(Point3D cand : candidates){
			System.out.println("candidate: (" + cand.x + " , " + cand.y + " , " + cand.z + " )");
		}
		*/

		// calculate estimated error
		double estimatedError = -1;
		if(calculateEstimatedError) {
			double sumDist = 0;
			double sumWeight = 0;
			int numPointsError = 25;
			//int numPointsError = numPointsWithProb;
			if (numPointsWithProb < numPointsError) {
				numPointsError = pointsWithProb.size();
			}
			Point3D refPoint = pointsWithProb.get(0).getPoint();
			//double refProb = pointsWithProb.get(0).getProbability();
			for (int i = 1; i < numPointsError; i++) {
				Point3D curPoint = pointsWithProb.get(i).getPoint();
				double curProb = pointsWithProb.get(i).getProbability();
				//double weight = curProb/refProb;
				double weight = curProb;
				double dist = Util.distanceBetweenPoints(curPoint, refPoint);
				sumDist += dist * weight;
				sumWeight += weight;
			}
			if (sumDist != 0) {
				//estimatedError = sumDist/(numPointsError-1);
				estimatedError = sumDist / sumWeight;
			}
		}
		//Log.d(TAG,"estimatedError: " + estimatedError);

		ResponseObject resObj = new ResponseObject(estimatedPos,candidates,null, null);
		resObj.setPositionError(estimatedError);
		if(lastEstimatedPos!=null){
			resObj.setLastPositionLocal(new Point3DWithFloor(lastEstimatedPos.x, lastEstimatedPos.y, lastEstimatedPos.z, lastEstimatedPos.getFloor()));
			resObj.setPriorDistance(priorDistResp);
		}
		return resObj;
	}

	/** this method returns all the points with their probability */
	public ArrayList<PointWeight> getPointsWithProb(){
		ArrayList<PointWeight> pointsWithWeight = new ArrayList<PointWeight>();
		for (PointProbability pp : pointsWithProb){
			pointsWithWeight.add(new PointWeight(pp.getPoint(),pp.getProbability()/maxProb));
		}
		return pointsWithWeight;
	}
	
	/*nested class for sorting object by Probability IN DESC ORDER (signs of the 'one' changed with respect to ASCENDENT order)*/
	public class SortByProbability implements Comparator<PointProbability>{
		public int compare(PointProbability arg0, PointProbability arg1) {
			if ( arg0.getProbability() < arg1.getProbability()){
				return 1;
			} else if ( arg0.getProbability() == arg1.getProbability() ){
				return 0;	
			} else if ( arg0.getProbability() > arg1.getProbability()){
				return -1;
			}
			return 0;
		}
	}
	
	public void setLastEstimatedPos(Point3DWithFloor currentEstimatedPos){
		lastEstimatedPos = currentEstimatedPos; 
		lastEstimatedPosTs = System.currentTimeMillis();
	}

	public class ProbAndMatches{
		
		public double prob;
		public int matches;
		
		public ProbAndMatches(double prob, int matches){
			this.prob = prob;
			this.matches = matches;
		}
		
	}
}