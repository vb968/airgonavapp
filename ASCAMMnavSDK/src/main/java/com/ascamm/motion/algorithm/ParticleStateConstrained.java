package com.ascamm.motion.algorithm;

import java.io.Serializable;

import com.ascamm.motion.utils.Edge;
import com.ascamm.motion.utils.Point3DWithFloor;

public class ParticleStateConstrained implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** edge to which the particle belongs */
	Edge stateEdge;
	
	/** distance of the particle over the edge */
	double stateDist;
	
	/** velocity of the particle */
	double stateVel;

	/** angle of the particle **/
	double stateAngle;

	/** Weight associated to the particle state */
    double Weight;

	Point3DWithFloor particleFree;

	/** angle of the particle considering edge **/
	double stateAngleEdge;

	/** variable indicating of correction is needed **/
	boolean correctionNeeded;
    
    /**
     * Constructor. Creates a particle state from the given parameters.
     * @param edge		edge to which the particle belongs
     * @param dist		distance of the particle over the edge
     * @param vel		velocity of the particle
	 * @param angle		angle of the particle
	 * @param W		Weight associated to the particle state
     */
    public ParticleStateConstrained(Edge edge, double dist, double vel, double angle, double W) {
				
		this.stateEdge = edge;
		this.stateDist = dist;
		this.stateVel = vel;
		this.stateAngle = angle;
		this.Weight = W;
	}
    
    public Edge getEdge(){
    	return stateEdge;
    }
    public double getDist(){
    	return stateDist;
    }
    public double getVel(){
    	return stateVel;
    }
	public double getAngle(){ return stateAngle; }

	public Point3DWithFloor getParticleFree() {
		return particleFree;
	}

	public void setParticleFree(Point3DWithFloor particleFree) {
		this.particleFree = particleFree;
	}
}
