package com.ascamm.motion.algorithm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.ascamm.motion.mems.MotionSTEPS;
import com.ascamm.motion.utils.LogWriter;
import com.ascamm.motion.algorithm.ProbabilityCalculation.ProbAndMatches;
import com.ascamm.motion.manage.IndoorPosClient;
import com.ascamm.motion.database.APWithPosition;
import com.ascamm.motion.database.CalibrationItem;
import com.ascamm.motion.database.DBHelper;
import com.ascamm.motion.database.PointWithCalibrationItems;
import com.ascamm.motion.detections.Detection;
import com.ascamm.motion.routing.Route;
import com.ascamm.motion.utils.MotionStepsInfoListener;
import com.ascamm.motion.utils.OnlineItem;
import com.ascamm.motion.utils.Point3DWithFloor;
import com.ascamm.motion.utils.Point3DWithFloorAndEdge;
import com.ascamm.motion.utils.ResponseObject;
import com.ascamm.motion.utils.UpdateAutocalibrationParametersListener;

import com.ascamm.motion.utils.Constants;
import com.ascamm.motion.utils.Settings;
import com.ascamm.utils.LatLng3D;
import com.ascamm.utils.Point3D;

public class PositionEstimation {

	private final String TAG = this.getClass().getSimpleName();
	
	private ResponseObject resEmpty = new ResponseObject(new Point3DWithFloor(0,0,0,0),null, new Point3D(0,0,0),null);
	private ResponseObject resError = new ResponseObject(new Point3DWithFloor(-1,-1,-1,-1),null, new Point3D(-1,-1,-1),null); // new line
	private ResponseObject resLoadError = new ResponseObject(new Point3DWithFloor(-3,-3,-3,-3),null, new Point3D(-3,-3,-3),null); // new line
	private ResponseObject resWaiting = new ResponseObject(new Point3DWithFloor(-5,-5,-5,-5),null, new Point3D(-5,-5,-5),null);
		
	private Hashtable<Integer,PointWithCalibrationItems> mPointsWithCalibrations;
	private Hashtable<Integer,PointWithCalibrationItems> mZonesWithCalibrations;
	private ArrayList<PointWithCalibrationItems> mPointsFloorDetermination;
	private ArrayList<String> mListOfAps;
	private ArrayList<APWithPosition> mApsWithPosition;
	//private ArrayList<APData> onlineAPs = new ArrayList<APData>();
	private ArrayList<APData> onlineAPsAfterTransformation = new ArrayList<APData>();

	private boolean debug = Constants.DEBUGMODE;
	//private boolean debug = true;

	private Context mContext;
	DBHelper dBHelper;
	
	private boolean loadingDB = false;
	private boolean DBloaded = false;
	private boolean loadError = false;
	
	private String mEstimationType;
	private boolean mUseParticleFilter;
	private boolean mUseMapFusion;
	private boolean mDemoMode = false;
	
	public final float DEFAULT_RSS_STDEV = Constants.mDefaultRssStdev;
	private int MIN_COUNT = Constants.mMinCount;
	private float MIN_COUNT_PERCENTAGE = Constants.mMinCountPercentage;
	
	private boolean outdoorDet = Constants.mOutdoorDet;
	private static String outdoorDetMethod = Constants.mOutdoorDetMethod;
	private static int outdoorRssNumAPs = Constants.mOutdoorRssNumAPs;
	private static float outdoorRssThreshold = Constants.mOutdoorRssThreshold;

	private static boolean floorDet = Constants.mFloorDet;
	private static String floorDetMethod = Constants.mFloorDetMethod;
	private static int nUnderground = Constants.nUnderground;	// n equals the number of underground floors
	private static int numFloors = Constants.mNumFloors;
	private ArrayList<Double> levels = new ArrayList<Double>(numFloors);
	
	private static String	dbFilt = Constants.mDbFilt;
	private static int	numVSAP = Constants.mNumVSAP;
	private static int numZONES = Constants.mNumZONES;
	private int zonesMethod = 2;	// 1 -> use mean and stdev of all points of zone ; 2 -> use representative point of zone 
	
	/* Matrix used for CAR Sant Cugat */
	//Matrix matrixLocalToGlobal = new Matrix(-8.0726854022474e-7,-5.1752668303031e-8,2.0758443037591,-4.4609614173995e-8,6.0440166579628e-7,41.483322016051,(double) 0,(double) 0,(double) 1);
	
	/* Matrix used for ASCAMM Lab */
	//Matrix matrixLocalToGlobal = new Matrix(7.3377540409051e-7,2.4797687366568e-7,2.1253579068142,1.4041416851798e-7,-5.3982702323456e-7,41.489573582267,(double) 0,(double) 0,(double) 1);
	//Matrix matrixLocalToGlobal = new Matrix(7.6235246259386e-7,2.5804207223574e-7,2.1253579179929,1.9299226745177e-7,-5.7017109294884e-7,41.489573593072,(double) 0,(double) 0,(double) 1);
	
	/* Matrix used for Mercat Rubi */
	//Matrix matrixLocalToGlobal = new Matrix(1.0136901121309e-6,0,2.03434322,0,-7.7533195020349e-7,41.48972017,(double) 0,(double) 0,(double) 1);
	
	// matrixLocalToGlobal is now obtained from DBHelper
	//Matrix matrixLocalToGlobal = null;
	private CoordConversion coordConv;

	private static int RSS_DIFF_METHOD_REF_MAX = 1;
	private static int RSS_DIFF_METHOD_PAIRS = 2;

	private boolean useRssDiff = true;
	private static int rssDiffMethod = RSS_DIFF_METHOD_PAIRS;
	private boolean useRssStdevSum = false;
	private String onlineApMacRef = "";
	private float onlineApRssRef = 0;

	private boolean usePriorProb = false;
	
	private boolean transformRSS = false;
	private boolean autocalibrationStopped = false;
	private double a=1, b=0, residuals=0, lastRes = 0, resDiff = 0, minRes = -1, minA = 1, minB = 0;
	private int numRssAuto = 0; //20
	private int iterRssAuto = 0;
	private double a2=1, b2=0, residuals2 = 0, y2=0, y50=0, y70=0, y80=0;
	private int numConvergence = 50; //number of consecutive measures to consider that the algorithm has converged
	private float range = 1; // (in dBm)
	private boolean hasConverged = false;
	private LinkedList<Double> mBuffer = new LinkedList<Double>();
	/**  Array with devices' parameters **/
	private ArrayList<AutoParam> devices = new ArrayList<AutoParam>();
	private String macAddress;
	
	/** Code added for logs **/
	private LogWriter mLogger;
	private boolean logsRunning = false, storeLogs = false;
	private String logsFilename = Settings.logsFilename + "_autolearning" + Constants.logsFilenameExt;
	
	private CorrelationCalculation mCorrCalc;
	private ProbabilityCalculation mProbCalc;
	private DistanceCalculation mDistCalc;
	private RSSAutolearning mRssAutolearning;
	private ParticleFilter mParticleFilter;
	private MapFusion mMapFusion;
	private MotionSTEPS mMotionSTEPS;

	//attributes for avoiding position jumps, based on maximum walked distance between positions
	private Point3DWithFloor mCurrentLocation = null;
	private Point3DWithFloor mLastLocation = null;
	private boolean mAvoidJumpsMechanism = false;
	
	// Particle filter parameters
	public int TimeBetweenPositionEstimations = 500;  //in milliseconds (it is modified in runtime by the IndoorPosClient)
	
	public static final int PF_NUM_OF_PARTICLES = Constants.PF_NUM_OF_PARTICLES;
	public static final double PF_TS = Constants.PF_TS;
	public static final double PF_SIGMA_STATIC = Constants.PF_SIGMA_STATIC;
	public static final double PF_SIGMA_WALKING = Constants.PF_SIGMA_WALKING;
	public static final double PF_SIGMA_0 = Constants.PF_SIGMA_0;
	public static final double PF_SIGMA_DIST = Constants.PF_SIGMA_DIST;

	/**************** FP positions simulation ********************/
	private ArrayList<Point3D> ArraySimulatedPositions;
	int k=0;
	/*********************** End of simulation *******************/
	
	private IndoorPosClient mIndoorPosClient; //instance for accessing the object of class RouteCalculation

	private ArrayList<UpdateAutocalibrationParametersListener> mAutocalibrationParametersListeners = new ArrayList<UpdateAutocalibrationParametersListener>();
	private ArrayList<MotionStepsInfoListener> mMotionStepsInfoListeners = new ArrayList<MotionStepsInfoListener>();

	private MotionStepsInfoListener mMotionStepsInfoListener = new MotionStepsInfoListener() {
		@Override
		public void newMotionStepsInfoAvailable(int motionMode, ArrayList<Double> stepsList, double orientation, double diffOrientation){
			fireNewMotionStepsInfoEvent(motionMode, stepsList, orientation, diffOrientation);
		}
	};
	
	public PositionEstimation(Context context, IndoorPosClient refIndoorPosClient) {
		
		mContext = context;
		mIndoorPosClient = refIndoorPosClient;
		macAddress = "00:00";
		/************************************************/
		mEstimationType = Settings.estimationType;
		
		/* Initialize all the types of position calculation while testing algorithms */
		mCorrCalc = new CorrelationCalculation();
		mProbCalc = new ProbabilityCalculation();
		mDistCalc = new DistanceCalculation();
		
		if(mEstimationType.equalsIgnoreCase("CORR")){
			//mCorrCalc = new CorrelationCalculation();
			useRssStdevSum = true;
		}
		else if(mEstimationType.equalsIgnoreCase("PROB")){
			//mProbCalc = new ProbabilityCalculation();
			useRssStdevSum = true;
		}
		else if (mEstimationType.equalsIgnoreCase("KNN")){
			//mDistCalc = new DistanceCalculation();
		}
		transformRSS = Settings.rssTransf;
		if(transformRSS){
			mRssAutolearning = new RSSAutolearning();
			if(storeLogs){
				mLogger =new LogWriter(Constants.APP_DATA_FOLDER_NAME, logsFilename);
				/** Code added to store data info into logs.txt **/
				if(mLogger.startLogs()>0){
					logsRunning = true;
				}
			}
		}
		mUseParticleFilter = Settings.useParticleFilter;
		mUseMapFusion = Settings.useMapFusion;
		mDemoMode = Settings.demoMode;
		
		/**************** FP positions simulation ********************
		int i,j;
		double PosY_gir, PosX_gir;
		this.ArraySimulatedPositions = new ArrayList<Point3D>();
		int PosX_init = 20; int PosY_init = 20;
		int Inc = 25;
		
		
		for(j=0;j<5;j++){
			for(i=0;i<10;i++){
				ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init,0));
			}
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 100,0)); //salt puntual
		}
		for(j=0;j<5;j++){
			for(i=0;i<10;i++){
				ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init,0));
			}
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 200,0)); //salt puntual
		}
		for(j=0;j<5;j++){
			for(i=0;i<10;i++){
				ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init,0));
			}
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 300,0)); //salt puntual
		}
		for(j=0;j<5;j++){
			for(i=0;i<10;i++){
				ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init,0));
			}
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 400,0)); //salt puntual
		}
		
		
		
		for(j=0;j<5;j++){
			for(i=0;i<10;i++){
				ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init,0));
			}
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 100,0)); //salt "sostingut"
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 105,0));
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 95,0));
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 100,0));
		}
		for(j=0;j<5;j++){
			for(i=0;i<10;i++){
				ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init,0));
			}
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 200,0)); //salt "sostingut"
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 205,0));
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 195,0));
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 200,0));
		}
		for(j=0;j<5;j++){
			for(i=0;i<10;i++){
				ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init,0));
			}
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 300,0)); //salt "sostingut"
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 305,0));
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 295,0));
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 300,0));
		}
		for(j=0;j<5;j++){
			for(i=0;i<10;i++){
				ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init,0));
			}
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 400,0)); //salt "sostingut"
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 405,0));
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 395,0));
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + 400,0));
		}
		
			
		/*
		for(i=0;i<22;i++){
			ArraySimulatedPositions.add(new Point3D(PosX_init,PosY_init + i*Inc,0));			
		} 
		PosY_gir = PosY_init + i*Inc;
		for(i=0;i<16;i++){
			ArraySimulatedPositions.add(new Point3D(PosX_init + i*Inc,PosY_gir,0));			
		}
		PosX_gir = PosX_init + i*Inc;
		for(i=0;i<22;i++){
			ArraySimulatedPositions.add(new Point3D(PosX_gir,PosY_gir - i*Inc,0));			
		}	
		
		for(i=0;i<16;i++){
			ArraySimulatedPositions.add(new Point3D(PosX_gir - i*Inc,PosY_init,0));			
		}	
		*/
		/*
		ArraySimulatedPositions.add(new Point3D(304,160,0));
		ArraySimulatedPositions.add(new Point3D(280,160,0));
		ArraySimulatedPositions.add(new Point3D(280,210,0));
		ArraySimulatedPositions.add(new Point3D(280,260,0));
		ArraySimulatedPositions.add(new Point3D(280,280,0));
		ArraySimulatedPositions.add(new Point3D(350,300,0));
		ArraySimulatedPositions.add(new Point3D(350,320,0));
		ArraySimulatedPositions.add(new Point3D(280,610,0));
		ArraySimulatedPositions.add(new Point3D(270,630,0));
		ArraySimulatedPositions.add(new Point3D(250,630,0));
		ArraySimulatedPositions.add(new Point3D(140,651,0));
		
		ArraySimulatedPositions.add(new Point3D(150,430,0));
		ArraySimulatedPositions.add(new Point3D(150,400,0));
		ArraySimulatedPositions.add(new Point3D(125,300,0));
		ArraySimulatedPositions.add(new Point3D(70,200,0));
		ArraySimulatedPositions.add(new Point3D(80,440,0));
		ArraySimulatedPositions.add(new Point3D(90,450,0));
		*/
		
		/*
		ArraySimulatedPositions.add(new Point3D(dsfd,fdfd,0));
		ArraySimulatedPositions.add(new Point3D(dsfd,fdfd,0));
		ArraySimulatedPositions.add(new Point3D(dsfd,fdfd,0));
		ArraySimulatedPositions.add(new Point3D(dsfd,fdfd,0));
		ArraySimulatedPositions.add(new Point3D(dsfd,fdfd,0));
		*/	
		k=0;
		/*********************** End of simulation *******************/		
	}
	
	public void stop(){
		if(mCorrCalc!=null){
			//mCorrCalc.stop();
		}
		if(mProbCalc!=null){
			mProbCalc.stop();
		}
		if(mDistCalc!=null){
			//mDistCalc.stop();
		}

		if(mMotionSTEPS!=null){
			mMotionSTEPS.removeMotionStepsInfoListenerMainThread(mMotionStepsInfoListener);
			mMotionSTEPS.Stop();
		}

	}
	
	private synchronized void setLoadingDB(boolean loading){
		loadingDB = loading;
	}
	
	public synchronized boolean getLoadingDB(){
		return loadingDB;
	}

	// initializes data coming from PDB and creates objects that also use these data
	public void initDbHelper(DBHelper DBHelper, final boolean useWifi, final boolean useBle){
		dBHelper = DBHelper;
		mListOfAps =  dBHelper.getAllAPsMac();
		if(debug)	Log.d(TAG,"numAPs: " + mListOfAps.size());
		float pxMeter = dBHelper.getPxMeter();
		float res = dBHelper.getResolution();
		float priorDistNonWalking = res*pxMeter*1.8f;
		//float priorDistWalking = 4*priorDistNonWalking;
		float maxVelWalking = 3; //(m/s)
		float priorDistWalking = maxVelWalking*pxMeter;
		if(debug)	Log.d(TAG,"priorDistNonWalking: " + priorDistNonWalking + " , priorDistWalking: " + priorDistWalking);
		coordConv = dBHelper.getConversionClass();
		if(debug){
			// LQ: was null, might crash
			String coordConvStr = "";
			if(coordConv!=null){
				coordConvStr = coordConv.toString();
			}
			Log.d(TAG,"coordConv: " + coordConvStr);
		}
		if(mEstimationType.equalsIgnoreCase("PROB")){
			mProbCalc.setPriorDistance(priorDistNonWalking, priorDistWalking);
		}
		double mapAngleToNorth = dBHelper.getMapAngleToNorth();
		if(debug)	Log.d(TAG,"mapAngleToNorth: " + mapAngleToNorth);
		if(Settings.useParticleFilterSensors) {
			if (mMotionSTEPS == null)
                mMotionSTEPS = new MotionSTEPS(mContext, pxMeter);
			mMotionSTEPS.addMotionStepsInfoListenerMainThread(mMotionStepsInfoListener);
			mMotionSTEPS.setMapAngleToNorth(mapAngleToNorth);
			mMotionSTEPS.Start();
		}
		if(mUseParticleFilter){
			if(pxMeter == -1){
				mParticleFilter = null;
				Log.e(TAG,"pxMeter not found");
			}
			else{
				if (mMapFusion == null)
					mMapFusion = new MapFusion(dBHelper);
				if (mParticleFilter == null)
					mParticleFilter = new ParticleFilter(mContext, pxMeter, PF_NUM_OF_PARTICLES, PF_TS, PF_SIGMA_STATIC * pxMeter, PF_SIGMA_WALKING * pxMeter, PF_SIGMA_0 * pxMeter, PF_SIGMA_DIST * pxMeter, mMapFusion, Settings.useParticleFilterSensors, mMotionSTEPS);
			}
		}
		// open database and load calibration points into memory
		if(dBHelper.isDatabaseOk()){
			new Thread() {
				public void run() {
					setLoadingDB(true);
					loadDBCalibrationPoints(useWifi,useBle);
					setLoadingDB(false);
					if(debug){
						Log.d(TAG,"Data loaded");
						Message msg = Message.obtain();
		        		Bundle b = new Bundle();
		        		b.putString("msg","Data loaded");
		        		msg.setData(b);
		        		handlerShowMsg.sendMessage(msg);
		        	}
				}
			}.start();
		}else{
			loadError = true;
		}
		if(mUseMapFusion){
			if(mMapFusion==null){
				mMapFusion = new MapFusion(dBHelper);
			}
		}
	}
	
	protected void loadDBCalibrationPoints(boolean useWifi, boolean useBle){
		Calendar c = Calendar.getInstance();		
		long utctime = c.getTimeInMillis();
		Date d = new Date(utctime);
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String s = f.format(d);
		if(debug)	Log.d(TAG,"before obtaining PointsWithCalibrations. Time: " + s);
		mPointsWithCalibrations = dBHelper.getPointsWithCalibrations(useWifi, useBle);
		if(floorDet && (floorDetMethod.equalsIgnoreCase("PROB") || floorDetMethod.equalsIgnoreCase("PROB_VSAP"))){
			if(floorDetMethod.equalsIgnoreCase("PROB")){
				mPointsFloorDetermination = dBHelper.getPointsFloorDetermination();
			}
			if(mProbCalc == null){
				mProbCalc = new ProbabilityCalculation();
			}
		}
		if(dbFilt.equalsIgnoreCase("ZONES_PROB") || dbFilt.equalsIgnoreCase("ZONES_CORR") || (floorDet && (floorDetMethod.equalsIgnoreCase("ZONES_PROB") || floorDetMethod.equalsIgnoreCase("ZONES_CORR")))){
			if(zonesMethod==1){
				mZonesWithCalibrations = dBHelper.getZonesWithCalibrations();
				Log.d(TAG,"mZonesWithCalibrations size: " + mZonesWithCalibrations.size());
			}
			else{
				mZonesWithCalibrations = dBHelper.getZonesWithCalibrationsOfRepresentativePoint();
			}
		}
		mApsWithPosition = dBHelper.getAllAPs();
		//mApsWithPosition = loadSimulatedAPs();
		if(debug)	Log.d(TAG,"numPointsCalibrations: " + mPointsWithCalibrations.size());
		Calendar c2 = Calendar.getInstance();		
		long utctime2 = c2.getTimeInMillis();
		Date d2 = new Date(utctime2);
		//SimpleDateFormat f2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String s2 = f.format(d2);
		if(debug)	Log.d(TAG,"after obtaining PointsWithCalibrations. Time: " + s2);
		DBloaded = true;
	}
	
	private ArrayList<APWithPosition> loadSimulatedAPs(){
		mApsWithPosition = new ArrayList<APWithPosition>();
		int x = 0;
		int y = 0;
		int z = 0;
		String ssid = "mwc2014";
		String bssid_base = "11:11:11:11:11:";
		for(int i=0;i<1000;i++){
			x = i;
			y = i;
			String bssid = bssid_base + Integer.toHexString(i);
			APWithPosition apwp = new APWithPosition(ssid, bssid, x, y, z);
			mApsWithPosition.add(apwp);
		}
		mApsWithPosition.add(new APWithPosition(ssid,"3c:08:f6:e6:e7:1d",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"3c:08:f6:e6:e7:1c",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:06:ce:8c",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:06:ce:be",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:06:ce:bf",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:06:ce:8d",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"e2:a7:25:b8:67:63",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:06:ce:bd",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:06:ce:bc",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"d0:22:be:32:b5:d4",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:d0:f6:cf",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:06:ce:8e",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:d0:f6:cc",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:d0:f6:cd",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:d0:f6:c2",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:d0:f6:c0",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:06:ce:8f",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"3c:08:f6:e6:e7:1f",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"02:1a:11:f1:e7:23",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:7d:84:7f",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:7d:84:7d",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:7d:84:7b",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:7d:84:7c",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"dc:d2:fc:1e:18:60",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"3c:08:f6:ff:9c:1e",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"58:a2:b5:d2:75:64",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:01:5e:2d",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:01:5e:2f",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:06:cf:8f",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:06:cf:8d",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"3c:08:f6:fb:41:fd",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:06:cf:8e",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"e4:71:85:30:01:30",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"a4:99:47:8e:ce:04",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"00:24:36:ad:cc:9e",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"00:66:4b:72:3d:84",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"3c:08:f6:fb:41:fe",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"3c:08:f6:ff:99:7c",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"78:6a:89:76:99:b4",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"3c:08:f6:e5:30:ec",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:01:55:8d",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"3c:08:f6:e5:30:ed",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"3c:08:f6:fb:41:fc",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:d0:f6:c3",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"4c:8b:ef:39:1e:13",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:01:5e:2c",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"3c:08:f6:fb:41:ff",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"ac:22:0b:95:02:1c",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"dc:d2:fc:1e:17:a0",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:01:55:8f",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"dc:d2:fc:1e:17:a5",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"3c:08:f6:ff:9c:1d",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"02:18:60:70:c6:9a",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:7d:6f:7f",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"a4:99:47:8e:ce:00",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"58:a2:b5:d2:74:82",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"e8:ed:f3:d8:db:bc",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"3c:08:f6:ff:9c:1f",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:d1:40:5c",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b0:c7:45:6d:4c:8f",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"3c:08:f6:ff:99:7f",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:01:5d:be",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:67:a1:6c",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:67:a1:6a",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:01:55:8c",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"d4:6e:5c:df:89:d0",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"04:46:65:c9:ae:84",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"24:69:a5:b3:e4:ba",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:7d:8d:7d",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:06:d0:cd",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"3c:08:f6:fb:4e:9f",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:06:cf:8c",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"00:24:36:ad:cc:9d",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"3c:08:f6:e6:e7:1e",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"34:db:fd:b5:68:ea",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:d0:ea:1a",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:d1:40:5d",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:01:5e:2e",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"d4:6e:5c:df:89:0c",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:d0:ea:1c",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:7d:6f:7c",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:06:d0:ce",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:01:5d:7e",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"e4:71:85:20:00:24",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:7d:84:72",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"10:0d:7f:46:be:40",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"b8:38:61:01:55:8e",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:7d:8d:7c",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"30:f3:1d:04:83:1e",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"78:6a:89:a1:a3:8a",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"3c:08:f6:ff:9c:1c",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"34:db:fd:b5:68:ec",x,y,z)); x++; y++;
		mApsWithPosition.add(new APWithPosition(ssid,"64:e9:50:67:a1:6f",x,y,z)); x++; y++;
		return mApsWithPosition;
	}

	// LQ: get outdoor APs
	public  ArrayList<APData> getOutdoorAPs(ArrayList<OnlineItem> avgScanSet) {

		ArrayList<APData> onlineScanSet = new ArrayList<>();
		ArrayList<APWithPosition> outdoorAPs = new ArrayList<>();
		// LQ: TODO: check bssid for aps
        outdoorAPs.add(new APWithPosition("", "0023:0060", 0, 0, -1));
        outdoorAPs.add(new APWithPosition("", "0023:0061", 0, 0, 0));
		outdoorAPs.add(new APWithPosition("", "0023:0062", 0, 0, 0));
		outdoorAPs.add(new APWithPosition("", "0023:0063", 0, 0, 0));
		outdoorAPs.add(new APWithPosition("", "0023:0064", 0, 0, 0));

		Log.d("outdoor beacons", outdoorAPs.size() + " outdoor beacons loaded");
		for (APWithPosition ap : outdoorAPs)
			Log.d("outdoor beacons", "outdoor ap " + ap.getBSSID());

		for (OnlineItem oi : avgScanSet)
			Log.d("outdoor beacons", "seen aps " + oi.getBSSID());

		for(OnlineItem scan : avgScanSet){
			for (APWithPosition ap : outdoorAPs) {
				Log.d("Outdoor Beacons", "scanset " + scan.getBSSID() + " outdoor " + ap.getBSSID() + " point " + ap.getPoint());
				if(scan.getBSSID().equalsIgnoreCase(ap.getBSSID())) {
					double rssStdev = adjustRssStdev(scan.getRSSstdev(), scan.getCount(), scan.getCountPercentage());
					APData onlineAP = new APData(ap.getSSID(),ap.getBSSID(),ap.getPoint(),scan.getRSS(),rssStdev);
					onlineAP.setCount(scan.getCount());
					onlineAP.setCountPercentage(scan.getCountPercentage());
					onlineScanSet.add(onlineAP);
					break;
				}
			}
		}

		// Sort APs by RSS
		Collections.sort(onlineScanSet, new SortByRSS());
		if(debug){
			Log.d(TAG,"Number of outdoor APs: " + onlineScanSet.size());
			for(APData ap : onlineScanSet){
				Log.i(TAG,"(" + ap.getSSID() + " , " + ap.getBSSID() + " , " + ap.getRSS() + " , " + ap.getRSSstdev() + " , " + ap.getCount() + " , " + ap.getCountPercentage() + ") , z: " + ap.getPoint().z);
			}
		}
		return onlineScanSet;
	}
	
	private double adjustRssStdev(double rssStdev, int count, float countPercentage){
		//if(countPercentage<MIN_COUNT_PERCENTAGE || count<MIN_COUNT){
		if(countPercentage<MIN_COUNT_PERCENTAGE){
			rssStdev = DEFAULT_RSS_STDEV;
		}
		return rssStdev;
	}
	
	protected ArrayList<APData> getOnlineSystemAPs(ArrayList<OnlineItem> avgScanSet){
		
		ArrayList<APData> onlineScanSet = new ArrayList<APData>();
		//Log.i(TAG,"Before filtering APs. numScans: " + avgScanSet.size() + " , numAPs: " + mApsWithPosition.size());
		for(OnlineItem scan : avgScanSet){
			for(APWithPosition ap : mApsWithPosition){
				if(scan.getBSSID().equalsIgnoreCase(ap.getBSSID())){
				//if(scan.getBSSID().equalsIgnoreCase(ap.getBSSID()) && scan.getCountPercentage() >= 50){
					double rssStdev = adjustRssStdev(scan.getRSSstdev(), scan.getCount(), scan.getCountPercentage());
					APData onlineAP = new APData(ap.getSSID(),ap.getBSSID(),ap.getPoint(),scan.getRSS(),rssStdev);
					onlineAP.setCount(scan.getCount());
					onlineAP.setCountPercentage(scan.getCountPercentage());
					onlineScanSet.add(onlineAP);
					break;
				}
			}
		}
		// Sort APs by RSS
		Collections.sort(onlineScanSet, new SortByRSS());
		/*
		int N = 13;
		int numScansets = onlineScanSet.size();
		if(numScansets>N){
			ArrayList<APData> onlineScansetFiltered = new ArrayList<>();
			for(int i=0; i<N; i++){
				onlineScansetFiltered.add(onlineScanSet.get(i));
			}
			Log.d(TAG,"filtering scansets from " + numScansets + " to " + N);
			onlineScanSet = onlineScansetFiltered;
		}
		*/
		//Log.i(TAG,"After filtering APs, after sorting APs by RSS. numAPs: " + onlineScanSet.size());
		if(debug){
			Log.d(TAG,"Number of online APs: " + onlineScanSet.size());
			for(APData ap : onlineScanSet){
				Log.i(TAG,"(" + ap.getSSID() + " , " + ap.getBSSID() + " , " + ap.getRSS() + " , " + ap.getRSSstdev() + " , " + ap.getCount() + " , " + ap.getCountPercentage() + ") , z: " + ap.getPoint().z);
			}
		}
		return onlineScanSet;
	}
	
	protected ArrayList<APData> getDiffOnlineSystemAPs(ArrayList<APData> onlineAPs){
		// LQ: checking if onlineAPs is sorted
		/*String onlineMeasuresStr = "";
		for (APData onlineAP: onlineAPs) {
			onlineMeasuresStr += onlineAP.getRSS()+", ";
		}*/
		//Log.d("sorted online aps", "online APs: "+onlineMeasuresStr);

		ArrayList<APData> diffOnlineAPs = new ArrayList<APData>();
		if(onlineAPs != null && !onlineAPs.isEmpty()) {
			onlineApMacRef = onlineAPs.get(0).getBSSID();
			onlineApRssRef = onlineAPs.get(0).getRSS();
			double onlineApStdevRef = onlineAPs.get(0).getRSSstdev();
			for (int i = 0; i < onlineAPs.size(); i++) {
				float newRss = onlineAPs.get(i).getRSS() - onlineApRssRef;
				double newRssStdev = 0.0;
				if (useRssStdevSum) {
					newRssStdev = Math.sqrt(Math.pow(onlineAPs.get(i).getRSSstdev(), 2) + Math.pow(onlineApStdevRef, 2));
				} else {
					newRssStdev = onlineAPs.get(i).getRSSstdev();
				}
				APData apData = new APData(onlineAPs.get(i).getSSID(), onlineAPs.get(i).getBSSID(), onlineAPs.get(i).getPoint(), newRss, newRssStdev);
				apData.setCount(onlineAPs.get(i).getCount());
				apData.setCountPercentage(onlineAPs.get(i).getCountPercentage());
				diffOnlineAPs.add(apData);
			}
		}
		if(debug){
			Log.d(TAG,"Number of diffOnline APs: " + diffOnlineAPs.size());
			for(APData ap : diffOnlineAPs){
				Log.i(TAG,"(" + ap.getSSID() + " , " + ap.getBSSID() + " , " + ap.getRSS() + " , " + ap.getRSSstdev() + " , " + ap.getCount() + " , " + ap.getCountPercentage() + ") , z: " + ap.getPoint().z);
			}
		}
		return diffOnlineAPs;
	}
	
	/*
	protected ArrayList<APData> getDiffOnlineSystemAPs(ArrayList<APData> onlineAPs){
		ArrayList<APData> diffOnlineAPs = new ArrayList<APData>();
		if(!onlineAPs.isEmpty()){
			onlineApMacRef = onlineAPs.get(0).getBSSID();
			float onlineApRssRef = onlineAPs.get(0).getRSS();
			double onlineApStdevRef = onlineAPs.get(0).getRSSstdev();
			for(int i=0; i<onlineAPs.size();i++){
				float newRss = onlineAPs.get(i).getRSS() - onlineApRssRef;
				double newRssStdev = 0.0;
				if(useRssStdevSum){
					newRssStdev = Math.sqrt(Math.pow(onlineAPs.get(i).getRSSstdev(),2) + Math.pow(onlineApStdevRef,2));
				}
				else{
					newRssStdev = onlineAPs.get(i).getRSSstdev();
				}
				APData apData = new APData(onlineAPs.get(i).getSSID(), onlineAPs.get(i).getBSSID(), onlineAPs.get(i).getPoint(), newRss, newRssStdev);
				apData.setCount(onlineAPs.get(i).getCount());
				apData.setCountPercentage(onlineAPs.get(i).getCountPercentage());
				diffOnlineAPs.add(apData);
			}
		}
		if(debug){
			Log.d(TAG,"Number of diffOnline APs: " + diffOnlineAPs.size());
			for(APData ap : diffOnlineAPs){
				Log.i(TAG,"(" + ap.getSSID() + " , " + ap.getBSSID() + " , " + ap.getRSS() + " , " + ap.getRSSstdev() + " , " + ap.getCount() + " , " + ap.getCountPercentage() + ") , z: " + ap.getPoint().z);
			}
		}
		return diffOnlineAPs;
	}
	*/

	private ArrayList<APData> getDiffPairsSystemAPs(ArrayList<APData> aps){
		ArrayList<APData> pairsAPs = new ArrayList<APData>();
		if(aps != null && !aps.isEmpty()) {
            /*
			Sort APs measurements by AP BSSID before computing the RSS differences of AP pairs, in order
            to guarantee a proper online-offline RSS comparison (fingerprinting localization algorithm)
             */
			Collections.sort(aps, new SortByBSSID());
			//compute the RSS differences of AP pairs:
			for (int i = 0; i < aps.size()-1; i++) {
				for (int j = i+1; j < aps.size(); j++) {
					float newRss = aps.get(i).getRSS() - aps.get(j).getRSS();
					double newRssStdev = 0.0;
					if (useRssStdevSum) {
						newRssStdev = Math.sqrt(Math.pow(aps.get(i).getRSSstdev(), 2) + Math.pow(aps.get(j).getRSSstdev(), 2));
					} else {
						newRssStdev = aps.get(i).getRSSstdev();
					}
					String newPairBSSID = aps.get(i).getBSSID() + "_" + aps.get(j).getBSSID();
					APData apData = new APData(aps.get(i).getSSID(), newPairBSSID, aps.get(i).getPoint(), newRss, newRssStdev);
					apData.setCount(aps.get(i).getCount());
					apData.setCountPercentage(aps.get(i).getCountPercentage());
					pairsAPs.add(apData);
				}
			}
			//now add the "normal" AP RSS measurements to the AP pairs differences array:
			for (int i = 0; i < aps.size(); i++) {
				pairsAPs.add(aps.get(i));
			}
		}
		if(debug){
			Log.d(TAG,"Number of diffPairsSystemAPs: " + pairsAPs.size());
			for(APData ap : pairsAPs){
				//Log.i(TAG,"(" + ap.getSSID() + " , " + ap.getBSSID() + " , " + ap.getRSS() + " , " + ap.getRSSstdev() + " , " + ap.getCount() + " , " + ap.getCountPercentage() + ") , z: " + ap.getPoint().z);
				Log.i(TAG,"(" + ap.getSSID() + " , " + ap.getBSSID() + " , " + ap.getRSS() + " , " + ap.getRSSstdev() + " , " + ap.getCount() + " , " + ap.getCountPercentage() + ")");
			}
		}
		return pairsAPs;
	}
	
	protected boolean isOutdoorRSS(ArrayList<APData> onlineAPs){
		/** Device in when N APs receive RSSI above threshold  **
		boolean isOut = false;
		int numAPs = outdoorRssNumAPs;
		if(numAPs > onlineAPs.size()){
			numAPs = onlineAPs.size();
		}
		for(int i=0;i<numAPs;i++){
			if(onlineAPs.get(i).getRSS()<outdoorRssThreshold){
				isOut = true;
				break;
			}
		}
		/******************************************************/
		
		/** Device in when 1 AP receives RSSI above threshold **/
		boolean isOut = true;
		if(onlineAPs.size() != 0){
			// TODO: trigger only if next to an exit
			if(onlineAPs.get(0).getRSS()>outdoorRssThreshold){
				isOut = false;
			}
		}
		/*******************************************************/
		
		return isOut;
	}
	
	protected int rssFloorDetermination(ArrayList<APData> onlineAPs){
		if(debug)	Log.d(TAG, "Inside rssFloorDetermination");
		levels.clear();
		for(int i=0;i<numFloors;i++){
			levels.add(0.0);
		}
		float rssX = 100;
		float rssFilt = -100;
		ArrayList<String> filtAPs = new ArrayList<String>();
		filtAPs.add("6c:9c:ed:ea:2c:10");
		for(APData ap : onlineAPs){
			if(ap.getRSS() >= rssFilt){
				if(filtAPs.contains(ap.getBSSID())){
					double temp = levels.get(-1 + nUnderground) + ap.getRSS() + rssX;
					levels.set(-1 + nUnderground, temp);
				}
				else{
					double temp = levels.get((int)ap.getPoint().z + nUnderground) + ap.getRSS() + rssX;
					levels.set((int)ap.getPoint().z + nUnderground, temp);
				}
			}
		}
		if(debug){
			for(double lev : levels){
				Log.d(TAG, "lev: " + lev);
			}
		}
		double max = Collections.max(levels);
		int maxIndex = levels.indexOf(max);
		int floor = maxIndex - nUnderground;
		return floor;
	}
	
	protected int probFloorDetermination(ArrayList<APData> onlineAPs){
		if(debug)	Log.d(TAG, "Inside probFloorDetermination");
		levels.clear();
		for(int i=0;i<numFloors;i++){
			levels.add(0.0);
		}
		
		// The points involved in the probabilistic floor determination are previously obtained in loadDBCalibrationPoints() 
		for(PointWithCalibrationItems pwci : mPointsFloorDetermination){
			
			ArrayList<CalibrationItem> cis = pwci.getCalibrationItems();
			
			//double pointPostProb = mProbCalc.calculatePosteriorProb(cis, onlineAPs, false);
			ProbAndMatches probAndMatches = mProbCalc.calculatePosteriorProb(cis, onlineAPs, false);
			double pointPostProb = probAndMatches.prob;
			
			// We add the probability of the Point3D to the corresponding floor
			//Log.i(TAG,"point: (" + pwci.getPositionX() + " , " + pwci.getPositionY() + " , " + pwci.getPositionZ() + ") , prob: " + pointPostProb);
			double temp = levels.get((int)pwci.getPoint().z + nUnderground) + pointPostProb;
			levels.set((int)pwci.getPoint().z + nUnderground, temp);
			
		}
		if(debug){
			for(double lev : levels){
				Log.d(TAG, "lev: " + lev);
			}
		}
		// We obtain the floor with the maximum probability
		double max = Collections.max(levels);
		int maxIndex = levels.indexOf(max);
		int floor = maxIndex - nUnderground;
		return floor;
	}
	
	protected int probVSAPFloorDetermination(ArrayList<APData> onlineAPs, ArrayList<APData> diffOnlineAPs, boolean useRssDiff){
		if(debug)	Log.d(TAG, "Inside probVSAPFloorDetermination");
		int noFloor = -100;
		int floor = -100;
		ArrayList<Integer> usePoints = new ArrayList<Integer>();
		ArrayList<PointWithCalibrationItems> dbSubset = new ArrayList<PointWithCalibrationItems>();
		ResponseObject resObj = new ResponseObject(resEmpty.getPositionLocal(),resEmpty.getCandidatesLocal(),resEmpty.getPositionGlobal(),resEmpty.getCandidatesGlobal());
		// Obtain the subset of the DB
		usePoints = dBHelper.getPointsToUseWithVSAP(onlineAPs.get(0).getBSSID(), noFloor);
		if(debug)	Log.d(TAG, "Number of use points of VSAP Floor determination: " + usePoints.size());
		if(usePoints.size()>0){
			if(useRssDiff){
				for(int p : usePoints){
					PointWithCalibrationItems pwci = mPointsWithCalibrations.get(p);
					Point3DWithFloor point = pwci.getPoint();
					ArrayList<CalibrationItem> cis = pwci.getCalibrationItems();
					ArrayList<CalibrationItem> diffCalibrationItems = getVSAPRssDiffCalibration(cis);
					dbSubset.add(new PointWithCalibrationItems(point, diffCalibrationItems));
				}
				//Calculate the probability and select the most probable point
				resObj = mProbCalc.calcPositionProb(dbSubset, diffOnlineAPs, true, onlineApRssRef, false, false, false);
			}
			else{
				for(int p : usePoints){
					PointWithCalibrationItems pwci = mPointsWithCalibrations.get(p);
					Point3DWithFloor point = pwci.getPoint();
					ArrayList<CalibrationItem> calibItems = pwci.getCalibrationItems();
					dbSubset.add(new PointWithCalibrationItems(point,calibItems));
				}
				//Calculate the probability and select the most probable point
				resObj = mProbCalc.calcPositionProb(dbSubset, onlineAPs, false, 0, false, false, false);
			}
			floor = (int) resObj.getPositionLocal().getFloor();
		}
		return floor;
	}
	
	protected int zonesFloorDetermination(ArrayList<APData> onlineAPs, ArrayList<APData> diffOnlineAPs, boolean useRssDiff, String type){
		if(debug)	Log.d(TAG, "Inside zonesFloorDetermination. useRssDiff: " + useRssDiff);
		int floor = -100;
		ArrayList<PointWithCalibrationItems> dbSubset = new ArrayList<PointWithCalibrationItems>();
		ResponseObject resObj = new ResponseObject(resEmpty.getPositionLocal(),resEmpty.getCandidatesLocal(),resEmpty.getPositionGlobal(),resEmpty.getCandidatesGlobal());
		// Obtain the subset of the DB
		Iterator<Entry<Integer, PointWithCalibrationItems>> it = mZonesWithCalibrations.entrySet().iterator();
		if(useRssDiff){
			while(it.hasNext()){
				Entry<Integer, PointWithCalibrationItems> entry = it.next();
				PointWithCalibrationItems pwci = entry.getValue();
				Point3DWithFloor point = pwci.getPoint();
				ArrayList<CalibrationItem> calibItems = pwci.getCalibrationItems();
				ArrayList<CalibrationItem> diffCalibrationItems = getRssDiffCalibration(calibItems,onlineApMacRef);
				//Log.i(TAG,"pos: " + pos.toString() + " , numCalibs: " + diffCalibrationItems.size());
				if(diffCalibrationItems != null){
					dbSubset.add(new PointWithCalibrationItems(point, diffCalibrationItems));
				}
			}
			if(type.equalsIgnoreCase("PROB")){
				//Calculate the most probable zone
				resObj = mProbCalc.calcPositionProb(dbSubset, diffOnlineAPs, true, onlineApRssRef, false, false, false);
			}
			else if(type.equalsIgnoreCase("CORR")){
				//Calculate the zone with the highest correlation
				resObj = mCorrCalc.calcPositionCorrelation(dbSubset, diffOnlineAPs);
			}
			else{
				Log.e(TAG,"Wrong type of Zones Floor Determination");
			}
		}
		else{
			while(it.hasNext()){
				Entry<Integer, PointWithCalibrationItems> entry = it.next();
				PointWithCalibrationItems pwci = entry.getValue();
				Point3DWithFloor point = pwci.getPoint();
				ArrayList<CalibrationItem> calibItems = pwci.getCalibrationItems();
				dbSubset.add(new PointWithCalibrationItems(point, calibItems));
			}
			if(type.equalsIgnoreCase("PROB")){
				//Calculate the most probable zone
				resObj = mProbCalc.calcPositionProb(dbSubset, onlineAPs, false, 0, false, false, false);
			}
			else if(type.equalsIgnoreCase("CORR")){
				//Calculate the zone with the highest correlation
				resObj = mCorrCalc.calcPositionCorrelation(dbSubset, onlineAPs);
			}
			else{
				Log.e(TAG,"Wrong type of Zones Floor Determination");
			}
		}
		floor = (int) resObj.getPositionLocal().z;
		return floor;
	}

	// obtain the subset of the PDB (i.e. select the points from the PDB that will be considered to calculate its probability)
	protected ArrayList<PointWithCalibrationItems> retrieveCalibrations(ArrayList<APData> onlineAPs, boolean useRssDiff, ArrayList<APData> diffOnlineAPs, int floor){
		if(debug)	Log.d(TAG, "Filter type: " + dbFilt);
		ArrayList<Integer> usePoints = new ArrayList<Integer>();
		ArrayList<PointWithCalibrationItems> dbSubset = new ArrayList<PointWithCalibrationItems>();
		if(dbFilt.equalsIgnoreCase("OAP")){
			//Log.d(TAG, "Before getPointsToUseWithOAP");
			usePoints = dBHelper.getPointsToUseWithOAP(onlineAPs, floor);
		}
		else if(dbFilt.equalsIgnoreCase("SAP")){
			//Log.d(TAG, "Before getPointsToUseWithSAP");
			usePoints = dBHelper.getPointsToUseWithSAP(onlineAPs.get(0).getBSSID(), floor);
		}
		else if(dbFilt.equalsIgnoreCase("VSAP")){
			//Log.d(TAG, "Before getPointsToUseWithVSAP");
			usePoints = dBHelper.getPointsToUseWithVSAP(onlineAPs.get(0).getBSSID(), floor);
		}
		else if(dbFilt.equalsIgnoreCase("NVSAP")){
			//Log.d(TAG, "Before getPointsToUseWithNVSAP");
			usePoints = dBHelper.getPointsToUseWithNVSAP(onlineAPs, numVSAP, floor);
		}
		else if(dbFilt.equalsIgnoreCase("ZONES_PROB")){
			//Log.d(TAG, "Before getPointsToUseWithZONES_PROB");
			usePoints = retrieveCalibrationsByZone(onlineAPs, diffOnlineAPs, useRssDiff, floor, "PROB");
		}
		else if(dbFilt.equalsIgnoreCase("ZONES_CORR")){
			//Log.d(TAG, "Before getPointsToUseWithZONES_CORR");
			usePoints = retrieveCalibrationsByZone(onlineAPs, diffOnlineAPs, useRssDiff, floor, "CORR");
		}
		else {
			Log.e(TAG, "Wrong calibration subset filter type");
		}
		if(debug)	Log.d(TAG, "Number of use points: " + usePoints.size());
		if(usePoints.size()>0){
			for(int p : usePoints){
				PointWithCalibrationItems pwci = mPointsWithCalibrations.get(p);
				Point3DWithFloor point = pwci.getPoint();
				ArrayList<CalibrationItem> calibItems = pwci.getCalibrationItems();
				dbSubset.add(new PointWithCalibrationItems(point,calibItems));
			}
		}
		return dbSubset;
	}
	
	protected ArrayList<Integer> retrieveCalibrationsByZone(ArrayList<APData> onlineAPs, ArrayList<APData> diffOnlineAPs, boolean useRssDiff, int floor, String type){
		if(debug)	Log.d(TAG, "Inside retrieveCalibrationsByZone. useRssDiff: " + useRssDiff);
		int numZones = numZONES;
		ArrayList<Integer> usePoints = new ArrayList<>();
		ArrayList<PointWithCalibrationItems> dbSubset = new ArrayList<PointWithCalibrationItems>();
		ArrayList<PointProbability> pointsProb = new ArrayList<PointProbability>();
		ArrayList<PointCorrelation> pointsCorr = new ArrayList<PointCorrelation>();
		// Obtain the subset of the DB
		Iterator<Entry<Integer, PointWithCalibrationItems>> it = mZonesWithCalibrations.entrySet().iterator();
		if(useRssDiff){
			while(it.hasNext()){
				Entry<Integer, PointWithCalibrationItems> entry = it.next();
				PointWithCalibrationItems pwci = entry.getValue();
				Point3DWithFloor point = pwci.getPoint();
				ArrayList<CalibrationItem> calibItems = pwci.getCalibrationItems();
				if(floor==-100 || (floor!=-100 && point.getFloor()==floor)){
					ArrayList<CalibrationItem> diffCalibrationItems = getRssDiffCalibration(calibItems,onlineApMacRef);
					//Log.i(TAG,"pos: " + pos.toString() + " , numCalibs: " + diffCalibrationItems.size());
					if(diffCalibrationItems != null){
						dbSubset.add(new PointWithCalibrationItems(point, diffCalibrationItems));
					}
				}
			}
			if(type.equalsIgnoreCase("PROB")){
				//Calculate the probabilities
				pointsProb = mProbCalc.calcProbabilities(dbSubset, diffOnlineAPs, true, onlineApRssRef);
			}
			else if(type.equalsIgnoreCase("CORR")){
				//Calculate the correlations
				pointsCorr = mCorrCalc.calcCorrelations(dbSubset, diffOnlineAPs);
			}
			else{
				Log.e(TAG,"Wrong type of Zones DB Subset");
			}
		}
		else{
			while(it.hasNext()){
				Entry<Integer, PointWithCalibrationItems> entry = it.next();
				PointWithCalibrationItems pwci = entry.getValue();
				Point3DWithFloor point = pwci.getPoint();
				ArrayList<CalibrationItem> calibItems = pwci.getCalibrationItems();
				if(floor==-100 || (floor!=-100 && point.getFloor()==floor)){
					dbSubset.add(new PointWithCalibrationItems(point, calibItems));
				}
			}
			if(type.equalsIgnoreCase("PROB")){
				//Calculate the probabilities
				pointsProb = mProbCalc.calcProbabilities(dbSubset, onlineAPs, false, 0);
			}
			else if(type.equalsIgnoreCase("CORR")){
				//Calculate the correlations
				pointsCorr = mCorrCalc.calcCorrelations(dbSubset, onlineAPs);
			}
			else{
				Log.e(TAG,"Wrong type of Zones DB Subset");
			}
		}
		ArrayList<Integer> zonesId = new ArrayList<Integer>();
		if(type.equalsIgnoreCase("PROB")){
			if(numZones>pointsProb.size()){
				numZones = pointsProb.size();
			}
			for(int i=0; i<numZones; i++){
				Log.i(TAG,"zonePoint: " + pointsProb.get(i).getPoint().toString() + " , id: " + pointsProb.get(i).getPoint().getId() + " , prob: " + pointsProb.get(i).getProbability());
				zonesId.add(pointsProb.get(i).getPoint().getId());
			}
		}
		else if(type.equalsIgnoreCase("CORR")){
			if(numZones>pointsCorr.size()){
				numZones = pointsCorr.size();
			}
			for(int i=0; i<numZones; i++){
				Log.i(TAG,"zonePoint: " + pointsCorr.get(i).getPoint().toString() + " , id: " + pointsCorr.get(i).getPoint().getId() + " , corr: " + pointsCorr.get(i).getCorrelation());
				zonesId.add(pointsCorr.get(i).getPoint().getId());
			}
		}
		usePoints = dBHelper.getPointsZones(zonesId);
		return usePoints;
	}

	// apply RSS difference method to Calibration samples using VSAP method
	protected ArrayList<CalibrationItem> getVSAPRssDiffCalibration(ArrayList<CalibrationItem> cis){
		// Sort APs by RSS
		Collections.sort(cis, new SortCiByRSS());
		ArrayList<CalibrationItem> diffCalibrationItems = new ArrayList<CalibrationItem>();
		float rssApRef = cis.get(0).getRSS();
		float stdevApRef = cis.get(0).getRSSstdev();
		for(int i=0; i<cis.size();i++){
			float newRss = cis.get(i).getRSS() - rssApRef;
			float newRssStdev = 0;
			if(useRssStdevSum){
				newRssStdev = (float) Math.sqrt(Math.pow(cis.get(i).getRSSstdev(),2) + Math.pow(stdevApRef,2));
			}
			else{
				newRssStdev = cis.get(i).getRSSstdev();
			}
			CalibrationItem ci = new CalibrationItem(cis.get(i).getSSID(), cis.get(i).getBSSID(), newRss, newRssStdev, cis.get(i).getTech(), cis.get(i).getCount(), cis.get(i).getPercentage(), cis.get(i).getOrder());
			diffCalibrationItems.add(ci);
		}
		return diffCalibrationItems;
	}
	
	/*
	protected ArrayList<CalibrationItem> getRssDiffCalibration(ArrayList<CalibrationItem> cis, String apRef){
		float rssApRef = 0;
		float stdevApRef = 0;
		ArrayList<CalibrationItem> diffCalibrationItems = null;
		apRef = cis.get(0).getBSSID();  // Take as reference AP the one with highest RSS in offline mode
		//Obtain the RSS and stdev of the reference AP
		boolean apRefFound = false;
		for(CalibrationItem ci : cis){
			if(ci.getBSSID().equalsIgnoreCase(apRef)){
				rssApRef = ci.getRSS();
				stdevApRef = ci.getRSSstdev();
				apRefFound = true;
				break;
			}
		}
		if(apRefFound){
			diffCalibrationItems = new ArrayList<CalibrationItem>();
			for(CalibrationItem ci : cis){
				float newRss = ci.getRSS() - rssApRef;
				float newRssStdev = 0;
				if(useRssStdevSum){
					newRssStdev = (float) Math.sqrt(Math.pow(ci.getRSSstdev(),2) + Math.pow(stdevApRef,2));
				}
				else{
					newRssStdev = ci.getRSSstdev();
				}
				CalibrationItem ciTemp = new CalibrationItem(ci.getSSID(), ci.getBSSID(), newRss, newRssStdev, ci.getTech(), ci.getCount(), ci.getPercentage(), ci.getOrder());
				diffCalibrationItems.add(ciTemp);
			}
		}
		return diffCalibrationItems;
	}
	*/

	// apply RSS difference method to Calibration samples
	protected ArrayList<CalibrationItem> getRssDiffCalibration(ArrayList<CalibrationItem> cis, String apRef){
		ArrayList<CalibrationItem> diffCalibrationItems = null;
		//Obtain the RSS and stdev of the reference AP
		boolean apRefWifiFound = false;
		float rssApRefWifi = 0;
		float stdevApRefWifi = 0;
		boolean apRefBleFound = false;
		float rssApRefBle = 0;
		float stdevApRefBle = 0;
		if(cis!=null){
			for(CalibrationItem ci : cis){
				//Log.i(TAG,"(" + ci.getBSSID() + "," + ci.getTech() + "," + ci.getOrder() + ")");
				if(ci.getOrder()==1){
					if(ci.getTech().equalsIgnoreCase("wifi")){
						rssApRefWifi = ci.getRSS();
						stdevApRefWifi = ci.getRSSstdev();
						apRefWifiFound = true;
					}
					else if(ci.getTech().equalsIgnoreCase("ble")){
						rssApRefBle = ci.getRSS();
						stdevApRefBle = ci.getRSSstdev();
						apRefBleFound = true;
					}
					if(apRefWifiFound && apRefBleFound){
						break;
					}
				}
			}
		}
		// Calculate the RSS difference for WiFi and BLE
		if(apRefWifiFound || apRefBleFound){
			diffCalibrationItems = new ArrayList<CalibrationItem>();
			for(CalibrationItem ci : cis){
				boolean useCI = false;
				float rssApRef = 0;
				float stdevApRef = 0;
				if(ci.getTech().equalsIgnoreCase("wifi") && apRefWifiFound){
					useCI = true;
					rssApRef = rssApRefWifi;
					stdevApRef = stdevApRefWifi;
				}
				else if(ci.getTech().equalsIgnoreCase("ble") && apRefBleFound){
					useCI = true;
					rssApRef = rssApRefBle;
					stdevApRef = stdevApRefBle;
				}
				if(useCI){
					float newRss = ci.getRSS() - rssApRef;
					float newRssStdev = 0;
					if(useRssStdevSum){
						newRssStdev = (float) Math.sqrt(Math.pow(ci.getRSSstdev(),2) + Math.pow(stdevApRef,2));
					}
					else{
						newRssStdev = ci.getRSSstdev();
					}
					CalibrationItem ciTemp = new CalibrationItem(ci.getSSID(), ci.getBSSID(), newRss, newRssStdev, ci.getTech(), ci.getCount(), ci.getPercentage(), ci.getOrder());
					diffCalibrationItems.add(ciTemp);
				}
			}
		}
		return diffCalibrationItems;
	}

	protected ArrayList<CalibrationItem> getRssDiffPairsCalibration(ArrayList<CalibrationItem> cis){
		ArrayList<CalibrationItem> diffPairsData = null;
		ArrayList<APData> apsWiFi = new ArrayList<APData>();
		ArrayList<APData> apsBle = new ArrayList<APData>();
		//first we separate wifi and ble (a rss pair difference can not mix wifi and ble)
		if(cis!=null){
			for(CalibrationItem ci : cis){
				//Log.i(TAG,"(" + ci.getBSSID() + "," + ci.getTech() + "," + ci.getOrder() + ")");
				if(ci.getTech().equalsIgnoreCase("wifi")){
					APData ap = new APData(ci.getSSID(),ci.getBSSID(),null,ci.getRSS(),ci.getRSSstdev());
					ap.setCount(ci.getCount());
					ap.setCountPercentage(ci.getPercentage());
					apsWiFi.add(ap);
				}
				else if(ci.getTech().equalsIgnoreCase("ble")){
					APData ap = new APData(ci.getSSID(),ci.getBSSID(),null,ci.getRSS(),ci.getRSSstdev());
					ap.setCount(ci.getCount());
					ap.setCountPercentage(ci.getPercentage());
					apsBle.add(ap);
				}
			}
		}

		//now we calculate the rss pairs differences for wifi and ble
		if(!apsWiFi.isEmpty()) {
			if(diffPairsData==null){
				diffPairsData = new ArrayList<CalibrationItem>();
			}
			ArrayList<APData> apsWiFiPairs = getDiffPairsSystemAPs(apsWiFi);
			for(APData ap : apsWiFiPairs){
				CalibrationItem ci = new CalibrationItem(ap.getSSID(), ap.getBSSID(), ap.getRSS(), (float) ap.getRSSstdev(), "wifi", ap.getCount(), ap.getCountPercentage(), -1);
				diffPairsData.add(ci);
			}
		}
		if(!apsBle.isEmpty()) {
			if(diffPairsData==null){
				diffPairsData = new ArrayList<CalibrationItem>();
			}
			ArrayList<APData> apsBlePairs = getDiffPairsSystemAPs(apsBle);
			for(APData ap : apsBlePairs){
				CalibrationItem ci = new CalibrationItem(ap.getSSID(), ap.getBSSID(), ap.getRSS(), (float) ap.getRSSstdev(), "ble", ap.getCount(), ap.getCountPercentage(), -1);
				diffPairsData.add(ci);
			}
		}

		//now we add the "normal" rss measurements for wifi and ble
		if(!apsWiFi.isEmpty()) {
			for (APData ap : apsWiFi) {
				CalibrationItem ci = new CalibrationItem(ap.getSSID(), ap.getBSSID(), ap.getRSS(), (float) ap.getRSSstdev(), "wifi", ap.getCount(), ap.getCountPercentage(), -1);
				diffPairsData.add(ci);
			}
		}
		if(!apsBle.isEmpty()) {
			for (APData ap : apsBle) {
				CalibrationItem ci = new CalibrationItem(ap.getSSID(), ap.getBSSID(), ap.getRSS(), (float) ap.getRSSstdev(), "ble", ap.getCount(), ap.getCountPercentage(), -1);
				diffPairsData.add(ci);
			}
		}

		return diffPairsData;
	}

	// convert point coordinates from local to global
	public Point3D convertLocalToGlobal(Point3D localPoint){
		Point3D pGlobal = null;
		if(coordConv == null){
			pGlobal = new Point3D(-1,-1,-1);
		} else {
			LatLng3D global = coordConv.convertFromLocalToGlobal(localPoint);

			if(global != null) {
				pGlobal = new Point3D(global.lat, global.lng, global.z);
			} else  {
				pGlobal = new Point3D(-1, -1, -1);
			}
		}
		return pGlobal;
	}

	// RP: implement conversion from Point3DWithFloor to Point3D
	public Point3D convertPoint3DWithFloorToPoint3D(Point3DWithFloor pwf){
		// We define an empty (pwof) point without floor and we fill it with the
		// x, y and floor of the (pwf) point with floor.
		Point3D pwof = null;
		pwof = new Point3D(pwf.x, pwf.y, pwf.getFloor());

		return pwof;
	}

	// calculate two positions (for testing purposes) using WiFi and BLE measurements and sensors' information. Particle Filter and Map Fusion can also be applied
	public ArrayList<ResponseObject> calcPosition(boolean wifiMeas, ArrayList<OnlineItem> avgScansetWifi, boolean bleMeas, ArrayList<OnlineItem> avgScansetBle, boolean walking){
		if(debug)	Log.d(TAG, "DataLoaded: " + DBloaded);
		if(DBloaded){
			ArrayList<PointWithCalibrationItems> calibrationSet = new ArrayList<PointWithCalibrationItems>();
			// Lines added to test RSS transformation
			ArrayList<PointWithCalibrationItems> calibrationSetNoRssDiff = new ArrayList<PointWithCalibrationItems>();
			ResponseObject res = new ResponseObject(new Point3DWithFloor(0,0,0,0),null, new Point3D(0,0,0),null);
			ResponseObject resNoRssDiff = new ResponseObject(new Point3DWithFloor(0,0,0,0),null, new Point3D(0,0,0),null);
			ArrayList<ResponseObject> result = new ArrayList<ResponseObject>();
			
			ArrayList<APData> onlineAPs = new ArrayList<APData>();
			ArrayList<APData> onlineAPsWifi = null;
			ArrayList<APData> onlineAPsBle = null;
			ArrayList<APData> onlineAPsSubset = null;
			ArrayList<APData> diffOnlineAPs = new ArrayList<APData>();
			ArrayList<APData> diffOnlineAPsWifi = null;
			ArrayList<APData> diffOnlineAPsBle = null;
			ArrayList<APData> diffOnlineAPsSubset = null;
			ArrayList<APData> diffPairsOnlineAPs = new ArrayList<APData>();
			ArrayList<APData> diffPairsOnlineAPsWiFi = null;
			ArrayList<APData> diffPairsOnlineAPsBle = null;
			if(wifiMeas && avgScansetWifi!=null && !avgScansetWifi.isEmpty()){
				onlineAPsWifi = getOnlineSystemAPs(avgScansetWifi);
				for(APData ap : onlineAPsWifi){
					onlineAPs.add(ap);
				}
			}
			if(bleMeas && avgScansetBle!=null && !avgScansetBle.isEmpty()){
				onlineAPsBle = getOnlineSystemAPs(avgScansetBle);
				for(APData ap : onlineAPsBle){
					onlineAPs.add(ap);
				}
			}
			if(onlineAPs.isEmpty()){
				if(debug)	Log.d(TAG, "none of the seen APs belong to the system, we consider device out of the system area");
				result.add(0,res);
				result.add(1,resNoRssDiff);
				return result;
			}
			else{
				if(useRssDiff){
					//Log.d(TAG,"Before calculating diff online APs");
					if(wifiMeas && avgScansetWifi!=null && !avgScansetWifi.isEmpty()){
						diffOnlineAPsWifi = getDiffOnlineSystemAPs(onlineAPsWifi);
						for(APData ap : diffOnlineAPsWifi){
							diffOnlineAPs.add(ap);
						}
						if(rssDiffMethod == RSS_DIFF_METHOD_PAIRS){
							diffPairsOnlineAPsWiFi = getDiffPairsSystemAPs(onlineAPsWifi);
							for(APData ap : diffPairsOnlineAPsWiFi){
								diffPairsOnlineAPs.add(ap);
							}
						}
					}
					if(bleMeas && avgScansetBle!=null && !avgScansetBle.isEmpty()){
						diffOnlineAPsBle = getDiffOnlineSystemAPs(onlineAPsBle);
						for(APData ap : diffOnlineAPsBle){
							diffOnlineAPs.add(ap);
						}
						if(rssDiffMethod == RSS_DIFF_METHOD_PAIRS){
							diffPairsOnlineAPsBle = getDiffPairsSystemAPs(onlineAPsBle);
							for(APData ap : diffPairsOnlineAPsBle){
								diffPairsOnlineAPs.add(ap);
							}
						}
					}
					//Log.d(TAG,"After calculating diff online APs");

				}
				// assign onlineAPs to use to calculate subset of PDB
				if(wifiMeas && bleMeas){
					onlineAPsSubset = onlineAPsBle;
					diffOnlineAPsSubset = diffOnlineAPsBle;
				}
				else{
					onlineAPsSubset = onlineAPs;
					diffOnlineAPsSubset = diffOnlineAPs;
				}
				if(transformRSS){
					onlineAPsAfterTransformation.clear();
					for(APData ap : onlineAPs){
						double rss = a2*ap.getRSS() + b2;
						APData apAfter = new APData(ap.getSSID(),ap.getBSSID(),ap.getPoint(),(float) rss,ap.getRSSstdev());
						apAfter.setCount(ap.getCount());
						apAfter.setCountPercentage(ap.getCountPercentage());
						onlineAPsAfterTransformation.add(apAfter);
						//double rss = a2*ap.getRSS() + b2;
						//ap.setRSS((float) rss);
					}
					
					Log.i(TAG,"APs after transformation: ");
					for(APData ap : onlineAPsAfterTransformation){
						Log.i(TAG,"(" + ap.getSSID() + " , " + ap.getBSSID() + " , " + ap.getRSS() + " , " + ap.getRSSstdev() + " , " + ap.getCount() + " , " + ap.getCountPercentage() + ") , z: " + ap.getPoint().z);
					}
					/*
					Log.i(TAG,"APs before transformation: ");
					for(APData ap : onlineAPsBeforeTransformation){
						Log.i(TAG,"mac: " + ap.getBSSID() + " , rss: " + ap.getRSS() + " , z: " + ap.getPositionZ());
					}
					*/
					
				}
				// check if device is out of the site area
				boolean isOut = false;
				if(outdoorDet && outdoorDetMethod.equalsIgnoreCase("RSS")){
					isOut = isOutdoorRSS(onlineAPs);
					if(debug)	Log.d(TAG, "isOut: " + isOut);
				}
				if(isOut){
					if(debug) {
						Log.d(TAG, "Device out of site area");
						Message msg = Message.obtain();
						Bundle b = new Bundle();
						b.putString("msg", "Device out of indoor site area");
						msg.setData(b);
						handlerShowMsg.sendMessage(msg);

						Log.d(TAG, "RSS below RSS_threshold. We consider device out of the site area");
					}

					result.add(0,res);
					result.add(1,resNoRssDiff);
					return result;
				}
				else{
					// calculate the current floor if floor detection is enabled
					int floor = -100; // no floor determination
					int floor2 = -100; // no floor determination
					if(floorDet){
						if(floorDetMethod.equalsIgnoreCase("RSS")){
							floor = rssFloorDetermination(onlineAPs);
							if(debug)	Log.d(TAG, "floor returned (RSS): " + floor);
							if(transformRSS){
								// Lines added to test RSS transformation
								floor2 = rssFloorDetermination(onlineAPsAfterTransformation);
								if(debug)	Log.d(TAG, "floor2 returned (RSS): " + floor2);
							}
						}
						else if(floorDetMethod.equalsIgnoreCase("PROB")){
							floor = probFloorDetermination(onlineAPs);
							if(debug)	Log.d(TAG, "floor returned (PROB): " + floor);
							if(transformRSS){
								// Lines added to test RSS transformation
								floor2 = probFloorDetermination(onlineAPsAfterTransformation);
								if(debug)	Log.d(TAG, "floor2 returned (PROB): " + floor2);
							}
						}
						else if(floorDetMethod.equalsIgnoreCase("PROB_VSAP")){
							if(useRssDiff){
								floor = probVSAPFloorDetermination(onlineAPsSubset, diffOnlineAPsSubset, true);
							}
							else{
								floor = probVSAPFloorDetermination(onlineAPsSubset, null, false);
							}
							if(debug)	Log.d(TAG, "floor returned (PROB_VSAP): " + floor);
							if(transformRSS){
								// Lines added to test RSS transformation
								floor2 = probVSAPFloorDetermination(onlineAPsAfterTransformation, null, false);
								if(debug)	Log.d(TAG, "floor2 returned (PROB_VSAP): " + floor2);
							}
						}
						else if(floorDetMethod.equalsIgnoreCase("ZONES_PROB")){
							if(useRssDiff){
								floor = zonesFloorDetermination(onlineAPs, diffOnlineAPs, true, "PROB");
							}
							else{
								floor = zonesFloorDetermination(onlineAPs, null, false, "PROB");
							}
							if(debug)	Log.d(TAG, "floor returned (ZONES_PROB): " + floor);
							if(transformRSS){
								// Lines added to test RSS transformation
								floor2 = zonesFloorDetermination(onlineAPsAfterTransformation, null, false, "PROB");
								if(debug)	Log.d(TAG, "floor2 returned (ZONES_PROB): " + floor2);
							}
						}
						else if(floorDetMethod.equalsIgnoreCase("ZONES_CORR")){
							if(useRssDiff){
								floor = zonesFloorDetermination(onlineAPs, diffOnlineAPs, true, "CORR");
							}
							else{
								floor = zonesFloorDetermination(onlineAPs, null, false, "CORR");
							}
							if(debug)	Log.d(TAG, "floor returned (ZONES_CORR): " + floor);
							if(transformRSS){
								// Lines added to test RSS transformation
								floor2 = zonesFloorDetermination(onlineAPsAfterTransformation, null, false, "CORR");
								if(debug)	Log.d(TAG, "floor2 returned (ZONES_CORR): " + floor2);
							}
						}
						else{
							Log.e(TAG, "Wrong floor determination method");
						}
					}
					
					// We obtain the subset of the PDB considering the current floor (or no floor (floor=-100))
					if(useRssDiff){
						calibrationSet = retrieveCalibrations(onlineAPsSubset, true, diffOnlineAPsSubset, floor);
					}
					else{
						calibrationSet = retrieveCalibrations(onlineAPsSubset, false, null, floor);
					}
					if(transformRSS){
						// Lines added to test RSS transformation
						calibrationSetNoRssDiff = retrieveCalibrations(onlineAPsAfterTransformation, false, null, floor2);
					}
					// if subset is empty, position can not be calculated. Return
					if(calibrationSet.isEmpty()){
						if(debug)	Log.d(TAG, "Empty calibration set");
						result.add(0,res);
						result.add(1,resNoRssDiff);
						return result;
					}
					else{
						if(debug)	Log.d(TAG, "Position Estimation type: " + mEstimationType);
						ResponseObject resObj = new ResponseObject(resEmpty.getPositionLocal(),resEmpty.getCandidatesLocal(),resEmpty.getPositionGlobal(),resEmpty.getCandidatesGlobal());
						// Line added to test RSS transformation
						ResponseObject resObjNoRssDiff = new ResponseObject(resEmpty.getPositionLocal(),resEmpty.getCandidatesLocal(),resEmpty.getPositionGlobal(),resEmpty.getCandidatesGlobal());
						
						ArrayList<PointWeight> subsetPoints = new ArrayList<PointWeight>();
						ArrayList<PointWeight> subsetPointsNoRssDiff = new ArrayList<PointWeight>();

						if(useRssDiff){
							// apply RSS difference method to subset points' measures, and also set diffOnlineAPs as onlineAPs to use RSS difference method in online measures
							if(rssDiffMethod == RSS_DIFF_METHOD_REF_MAX) {
								if (dbFilt.equalsIgnoreCase("VSAP")) {
									// diff of onlineAPs
									onlineAPs = diffOnlineAPs;
									ArrayList<PointWithCalibrationItems> diffCalibrationSet = new ArrayList<PointWithCalibrationItems>();
									for (PointWithCalibrationItems pwci : calibrationSet) {
										ArrayList<CalibrationItem> cis = pwci.getCalibrationItems();
										ArrayList<CalibrationItem> diffCalibrationItems = getVSAPRssDiffCalibration(cis);
										pwci.setCalibrationItems(diffCalibrationItems);
										diffCalibrationSet.add(pwci);
									}
									calibrationSet = diffCalibrationSet;
								}
								//else if(dbFilt.equalsIgnoreCase("SAP") || dbFilt.equalsIgnoreCase("NVSAP")){
								else {
									//String apRef = onlineAPs.get(0).getBSSID();
									onlineAPs = diffOnlineAPs;
									ArrayList<PointWithCalibrationItems> diffCalibrationSet = new ArrayList<PointWithCalibrationItems>();
									for (PointWithCalibrationItems pwci : calibrationSet) {
										ArrayList<CalibrationItem> cis = pwci.getCalibrationItems();
										ArrayList<CalibrationItem> diffCalibrationItems = getRssDiffCalibration(cis, onlineApMacRef);
										if (diffCalibrationItems != null) {
											pwci.setCalibrationItems(diffCalibrationItems);
											diffCalibrationSet.add(pwci);
										}
									}
									calibrationSet = diffCalibrationSet;
								}
								/*
								else{
									Log.e(TAG, "RSS difference can only work with SAP, VSAP or NVSAP method");
								}
								*/
							}
							else if(rssDiffMethod == RSS_DIFF_METHOD_PAIRS){
								onlineAPs = diffPairsOnlineAPs;
								ArrayList<PointWithCalibrationItems> diffCalibrationSet = new ArrayList<PointWithCalibrationItems>();
								for (PointWithCalibrationItems pwci : calibrationSet) {
									ArrayList<CalibrationItem> cis = pwci.getCalibrationItems();
									ArrayList<CalibrationItem> diffCalibrationItems = getRssDiffPairsCalibration(cis);
									if (diffCalibrationItems != null) {
										pwci.setCalibrationItems(diffCalibrationItems);
										diffCalibrationSet.add(pwci);
									}
								}
								calibrationSet = diffCalibrationSet;
							}
						}
						
						/*
						for(PointWithCalibrationItems pwci : calibrationSet){
							subsetPoints.add(pwci.getPoint());
						}
						for(PointWithCalibrationItems pwci : calibrationSetNoRssDiff){
							subsetPointsNoRssDiff.add(pwci.getPoint());
						}
						*/
						/*
						Log.i(TAG,"--- DATA with diff ---");
						for(APData ap : onlineAPs){
							Log.i(TAG,"mac: " + ap.getBSSID() + " , RSS: " + ap.getRSS());
						}
						for(PointWithCalibrationItems pwci : calibrationSet){
							Log.i(TAG,"Point: (" + pwci.getPositionX() + "," + pwci.getPositionY() + "," + pwci.getPositionZ() + ")");
							for(CalibrationItem ci : pwci.getCalibrationItems()){
								Log.i(TAG,"mac: " + ci.getBSSID() + " , RSS: " + ci.getRSS() + " , stdev: " + ci.getRSSstdev());
							}
						}
						*/

						// calculate an estimated position using one of the possible estimation types (CORR, PROB or KNN) and get the weight of each point in the subset used
						if(mEstimationType.equalsIgnoreCase("CORR")){
							// The result of calcPositionCorrealtion will be a ResponseObject with only local point and local candidates
							resObj = mCorrCalc.calcPositionCorrelation(calibrationSet, onlineAPs);
							subsetPoints = mCorrCalc.getPointsWithCorr();
							if(transformRSS){
								// Line added to test RSS transformation
								resObjNoRssDiff = mCorrCalc.calcPositionCorrelation(calibrationSetNoRssDiff, onlineAPsAfterTransformation);
								subsetPointsNoRssDiff = mCorrCalc.getPointsWithCorr();
							}
						}
						else if(mEstimationType.equalsIgnoreCase("PROB")){
							// The result of calcPositionProb will be a ResponseObject with only local point and local candidates
							resObj = mProbCalc.calcPositionProb(calibrationSet, onlineAPs, useRssDiff, onlineApRssRef, usePriorProb, walking, true);
							subsetPoints = mProbCalc.getPointsWithProb();
							// LQ: debug message:
							Log.d("CALC FINGERPRINTING POS", "milis: " + System.currentTimeMillis());
							if(transformRSS){
								// Line added to test RSS transformation
								resObjNoRssDiff = mProbCalc.calcPositionProb(calibrationSetNoRssDiff, onlineAPsAfterTransformation, false, onlineApRssRef, usePriorProb, walking, true);
								//resObjNoRssDiff = mProbCalc.calcPositionProb(calibrationSet, onlineAPs, useRssDiff, onlineApRssRef, false, walking);
								subsetPointsNoRssDiff = mProbCalc.getPointsWithProb();
							}
						}
						else if(mEstimationType.equalsIgnoreCase("KNN")){
							// The result of calcPositionDist will be a ResponseObject with only local point and local candidates
							resObj = mDistCalc.calcPositionDist(calibrationSet, onlineAPs);
							if(transformRSS){
								// Line added to test RSS transformation
								resObjNoRssDiff = mDistCalc.calcPositionDist(calibrationSetNoRssDiff, onlineAPsAfterTransformation);
							}
						}
						else{
							Log.e(TAG, "Wrong position estimation method");
							result.add(0,resError);
							result.add(1,resError);
							return result;
						}
						if(debug){
							Log.d(TAG,"estimated position: (" + resObj.getPositionLocal().x + " , " + resObj.getPositionLocal().y + " , " + resObj.getPositionLocal().z + " , " + resObj.getPositionLocal().getFloor() + ")");
							Log.d(TAG,"estimated position (no RSS diff): (" + resObjNoRssDiff.getPositionLocal().x + " , " + resObjNoRssDiff.getPositionLocal().y + " , " + resObjNoRssDiff.getPositionLocal().z + " , " + resObjNoRssDiff.getPositionLocal().getFloor() + ")");
						}
						if(resObj.getPositionLocal().x == 0 && resObj.getPositionLocal().y == 0 && resObj.getPositionLocal().z == 0){
							result.add(0,res);
							result.add(1,resNoRssDiff);
							return result;
						}
						else{
							/**************** Fingerprinting positions simulation ********/
							/*
							resObj.setPositionLocal(ArraySimulatedPositions.get(k));
							k++;
							if(k==ArraySimulatedPositions.size()) k=0;
							*/
							/*********************** End of simulation *******************/

							if(mUseParticleFilter && mParticleFilter != null){
								// apply Particle Filter. If demoMode is enabled, position calculated with Particle Filter will be set as position local
								mParticleFilter.updateTsAndABMatrix(this.TimeBetweenPositionEstimations/1000f);

								Point3DWithFloor mCurrentLocationFiltered_3D = mParticleFilter.execute(resObj.getPositionLocal(), resObj.getPositionError(), walking);
								
								resObj.setPositionParticleFilter(mCurrentLocationFiltered_3D);
								resObj.setFilterParticles(mParticleFilter.getParticles());
								resObj.setFilterIsResampled(mParticleFilter.isResampled);
								resObj.setNumSteps(mParticleFilter.getNumSteps());
								resObj.setUserOrientation(mParticleFilter.getUserOrientation());
								resObj.setUserOrientationReliability(mParticleFilter.getUserOrientationReliability());
								if(debug)	Log.d(TAG,"particle filter position estimate: (" + resObj.getPositionParticleFilter().x + " , " + resObj.getPositionParticleFilter().y + " , " + resObj.getPositionParticleFilter().z + " , " + resObj.getPositionParticleFilter().getFloor() + ")");
								if(mDemoMode){
									resObj.setPositionLocal(mCurrentLocationFiltered_3D);
								}
							}
							if(mUseMapFusion){
								// apply Map Matching. If demoMode is enabled, position calculated with Map Fusion will be set as position local
								Point3DWithFloor mCurrentLocation = new Point3DWithFloor();
								if(mUseParticleFilter){
									mCurrentLocation.set(resObj.getPositionParticleFilter().x, resObj.getPositionParticleFilter().y, resObj.getPositionParticleFilter().z);
									mCurrentLocation.setFloor(resObj.getPositionParticleFilter().getFloor());
								}else{
									mCurrentLocation.set(resObj.getPositionLocal().x, resObj.getPositionLocal().y, resObj.getPositionLocal().z);
									mCurrentLocation.setFloor(resObj.getPositionLocal().getFloor());
								}
								Point3DWithFloorAndEdge mLocationMapFused = mMapFusion.doPathMatching(mCurrentLocation);
								Point3DWithFloor mCurrentLocationMapFused = new Point3DWithFloor(mLocationMapFused.x, mLocationMapFused.y, mLocationMapFused.z, mLocationMapFused.getFloor());
								resObj.setPositionMapFusion(mCurrentLocationMapFused);
								if(debug)	Log.d(TAG,"map fusion (path matching) position estimate: (" + resObj.getPositionMapFusion().x + " , " + resObj.getPositionMapFusion().y + " , " + resObj.getPositionMapFusion().z + " , " + resObj.getPositionMapFusion().getFloor() + ")");
								// RP: comment apply MFusion, so it is done
								// when demoMode == OFF
								//if(mDemoMode){
									resObj.setPositionLocal(mCurrentLocationMapFused);
								//}
								// RP: LOGs. TODO: delete 'hem
								if(resObj.getPositionLocal().getFloor() != 1.0
										&& resObj.getPositionLocal().getFloor() != 0.0){
                                    Double a = resObj.getPositionLocal().getFloor();
                                    Log.d(TAG,"Not a integer number (with " +
											"float type)" + resObj.getPositionLocal().getFloor());

								}
								
							}
							
							//for prior distance:
							if(mUseMapFusion){
								mProbCalc.setLastEstimatedPos(resObj.getPositionMapFusion());
							}
							else if(mUseParticleFilter){
								mProbCalc.setLastEstimatedPos(resObj.getPositionParticleFilter());
							}
							else{
								mProbCalc.setLastEstimatedPos(resObj.getPositionLocal());
							}
							
							/*
							 * mechanism for avoiding position jumps, 
							 * based on maximum walked distance between positions
							 */
							if(mAvoidJumpsMechanism){
								this.mCurrentLocation = resObj.getPositionLocal();
								if(this.mLastLocation != null){
									Route route = mIndoorPosClient.routeCalculation.calculateRoute(this.mLastLocation, this.mCurrentLocation);
									if(route.getDistance() > 10){  //in meters
										/*
										 * The walked distance from the last position is too long,
										 * then we consider it a wrong position calculation.
										 * We do not update the position (we force that the new position 
										 * is the last epoch one)
										 * and we do not update last_location for the next epoch
										 */
										resObj.setPositionLocal(this.mLastLocation);
										Log.d(TAG,"+++ New calculated position WILL NOT BE UPDATED because jump detected (distance=" + route.getDistance() + ")"); 
									}
									else{
										/* 
										 * The walked distance from the last position is short enough.
										 * The current position will be updated (we do not force anything)
										 * and we update the last_location for the next epoch
										 */
										this.mLastLocation = this.mCurrentLocation;
										Log.d(TAG,"--- New calculated position updated because no jump detected (distance=" + route.getDistance() + ")");
									}
								}
								else{
									this.mLastLocation = this.mCurrentLocation;
								}
							}

							// calculate global coordinates of the estimated position and its candidates
							// RP: convertPoint3DWithFloorToPoint3D the resObj
							Point3D pointGlobal = convertLocalToGlobal
									(convertPoint3DWithFloorToPoint3D(resObj
											.getPositionLocal()));
							ArrayList<Point3D> candidatesGlobal = new ArrayList<Point3D>();
							for (Point3D cpl : resObj.getCandidatesLocal()){
								Point3D cpg = convertLocalToGlobal(cpl);
								candidatesGlobal.add(cpg);
							}
							
							if(transformRSS){
								if(iterRssAuto<numRssAuto){
									/*
									// Obtain RSS values of the calibration point with higher probability and update a and b parameters
									ArrayList<CalibrationItem> calibItems = mPointsWithCalibrations.get(resObjNoRssDiff.getCandidatesLocal().get(0));
									ArrayList<CalibrationItem> calibItemsTransf = mPointsWithCalibrations.get(resObj.getCandidatesLocal().get(0));
									if(useRssDiff){
										ArrayList<APData> diffOnlineAPsBeforeTransformation = new ArrayList<APData>();
										String apRef = onlineAPsBeforeTransformation.get(0).getBSSID();
										for(int i=0; i<onlineAPsBeforeTransformation.size();i++){
											if(i!=0){
												APData apData = new APData(onlineAPsBeforeTransformation.get(i).getSSID(), onlineAPsBeforeTransformation.get(i).getBSSID(), onlineAPsBeforeTransformation.get(i).getPositionX(), onlineAPsBeforeTransformation.get(i).getPositionY(), onlineAPsBeforeTransformation.get(i).getPositionZ(), onlineAPsBeforeTransformation.get(i).getRSS() - onlineAPsBeforeTransformation.get(0).getRSS());
												diffOnlineAPsBeforeTransformation.add(apData);
											}
										}
										onlineAPsBeforeTransformation = diffOnlineAPsBeforeTransformation;
										ArrayList<CalibrationItem> diffCalibItems = getRssDiffCalibration(calibItems,apRef);
										ArrayList<CalibrationItem> diffCalibItemsTransf = getRssDiffCalibration(calibItemsTransf,apRef);
										calibItems = diffCalibItems;
										calibItemsTransf = diffCalibItemsTransf;
									}
									if(calibItems != null){
										updateRssAutolearningParameters(onlineAPsBeforeTransformation,calibItems);
									}
									if(calibItemsTransf != null){
										updateRssAutolearningParameters2(onlineAPsBeforeTransformation,calibItemsTransf);
									}
									// add the result to the end of the queue, remove head if it is full
									if(mBuffer.size() < numConvergence){
										mBuffer.add(y2);
									} else {
										while (mBuffer.size() >= numConvergence){
											mBuffer.poll();
										}
										mBuffer.add(y2);
										// Loop through all the elements of the array
										hasConverged = true;
										double ref = mBuffer.get(0);
										for (int i=mBuffer.size()-1; i>0; i--){
											if(Math.abs(mBuffer.get(i)-ref)>(range/2)){
												hasConverged = false;
												break;
											}
										}
										Log.d(TAG,"hasConverged: " + hasConverged);
									}
									*/
									/** Manual Parameters **/
									for(AutoParam autoParam : devices){
										if(autoParam.getMac().equalsIgnoreCase(macAddress)){
											a = a2 = autoParam.getSlope();
											b = b2 = autoParam.getIntersection();
											Log.i(TAG,"Autolearning parameters found. a: " + a + " , b: " + b);
										}
									}
									minA = a;
									minB = b;
									/***********************/
									/** Code added to store data info into logs.txt **/
									if(storeLogs && logsRunning){
										mLogger.addAutolearningLog("ITERATION", iterRssAuto, a, b, residuals, a2, b2, residuals2, y2, hasConverged);
									}
									if(debug){
										Log.d("Transformation Params","a: " + a + " , b: " + b);
										Log.d("Transformation Params","a2: " + a2 + " , b2: " + b2);
									}
									AutocalibrationParameters autoParams = new AutocalibrationParameters(a2,b2,residuals2);
									fireNewAutocalibrationEvent(autoParams);
									iterRssAuto++;
								}
								else{
									if(!autocalibrationStopped){
										a = minA;
										b = minB;
										/** Code added to store data info into logs.txt **/
										if(storeLogs && logsRunning){
											mLogger.addAutolearningLog("MIN", iterRssAuto, a, b, minRes, 0,0,0,0,false);
											mLogger.stopLogs();
										}
										AutocalibrationParameters autoParams = new AutocalibrationParameters(a,b,minRes);
										fireNewAutocalibrationEvent(autoParams);
										autocalibrationStopped = true;
										if(debug)	Log.d("Transformation Params", "Autocalibration Stopped. a: " + minA + " , b: " + minB + " , y: " + minRes);
									}
								}
							}

							// We prepare the result of the algorithm
							ResponseObject resultRssDiff = new ResponseObject(resObj.getPositionLocal(),resObj.getCandidatesLocal(),pointGlobal,candidatesGlobal,resObj.getPositionParticleFilter(),resObj.getFilterParticles(),resObj.getFilterIsResampled(),resObj.getPositionMapFusion());
							resultRssDiff.setPositionError(resObj.getPositionError());
							resultRssDiff.setLastPositionLocal(resObj.getLastPositionLocal());
							resultRssDiff.setPriorDistance(resObj.getPriorDistance());
							resultRssDiff.setListPositions(subsetPoints);
							if(Settings.useParticleFilterSensors) {
								// get orientation from MotionSTEPS
								double orientation = mMotionSTEPS.getAbsoluteOrientation();
								mMotionSTEPS.setLastOrientation(orientation);
								resultRssDiff.setRelativeOrientation(orientation);
								// get numSteps from ParticleFilter response class
								double numSteps = resObj.getNumSteps();
								resultRssDiff.setNumSteps(numSteps);
								resultRssDiff.setUserOrientation(resObj.getUserOrientation());
								resultRssDiff.setUserOrientationReliability(resObj.getUserOrientationReliability());
								//resultRssDiff.setAbsoluteOrientation(resObj.getAbsoluteOrientation());
								double diffOrientation = mMotionSTEPS.getDiffOrientation();
								resultRssDiff.setAbsoluteOrientation(diffOrientation);
							}

							//Lines added to test RSS transformation
							ResponseObject resultNoRssDiff = new ResponseObject(resObjNoRssDiff.getPositionLocal(),resObjNoRssDiff.getCandidatesLocal(),resEmpty.getPositionGlobal(),resEmpty.getCandidatesGlobal());
							resultNoRssDiff.setPositionError(resObjNoRssDiff.getPositionError());
							if(transformRSS){
								// RP: resObjNoRssDiff is a 3d point with floor
								Point3D pointGlobalNoRssDiff =
										convertLocalToGlobal
												(convertPoint3DWithFloorToPoint3D(resObjNoRssDiff.getPositionLocal()));
								ArrayList<Point3D> candidatesGlobalNoRssDiff = new ArrayList<Point3D>();
								for (Point3D cpl : resObjNoRssDiff.getCandidatesLocal()){
									Point3D cpg = convertLocalToGlobal(cpl);
									candidatesGlobalNoRssDiff.add(cpg);
								}
								resultNoRssDiff = new ResponseObject(resObjNoRssDiff.getPositionLocal(),resObjNoRssDiff.getCandidatesLocal(),pointGlobalNoRssDiff,candidatesGlobalNoRssDiff);
								resultNoRssDiff.setListPositions(subsetPointsNoRssDiff);
							}

							result.add(0,resultRssDiff);
							result.add(1,resultNoRssDiff);

							return result;
						}
					}
				}
			}
		}else{
			// DB not loaded
			if(loadError){
				if(debug)	Log.d(TAG,"Data not loaded correctly. Try outdoor positioning...");
				ArrayList<ResponseObject> result = new ArrayList<ResponseObject>();
				result.add(0, resLoadError);
				result.add(1, resLoadError);
				return result;
			}
			else{
				if(debug)	Log.d(TAG,"Data not loaded yet. Waiting for it...");
				ArrayList<ResponseObject> result = new ArrayList<ResponseObject>();
				result.add(0,resWaiting);
				result.add(1,resWaiting);
				return result;
			}
		}
	}
	
	// Method used to update RSS autolearning parameters
	protected void updateRssAutolearningParameters(ArrayList<APData> onlineAPs,ArrayList<CalibrationItem> calibItems){
		RSSAutolearningResponse mAutoResp = new RSSAutolearningResponse(1,0,0);
		if(useRssDiff){
			mAutoResp = mRssAutolearning.calculateRssDiffAutocalibrationParameter(onlineAPs, calibItems);
		}
		else{
			mAutoResp = mRssAutolearning.calculateRssAutocalibrationParameters(onlineAPs, calibItems);
		}
		a = mAutoResp.getSlope();
		b = mAutoResp.getIntersection();
		residuals = mAutoResp.getResiduals();
		resDiff = residuals - lastRes;
		lastRes = residuals;
		if(residuals<minRes || minRes == -1){
			minRes = residuals;
			minA = a;
			minB = b;
			if(debug)	Log.d("LinearRegression", "minRes found. y: " + minRes + " , a: " + a + " , b: " + b);
		}	
	}
	
	// Method used to update RSS autolearning parameters
	protected void updateRssAutolearningParameters2(ArrayList<APData> onlineAPs,ArrayList<CalibrationItem> calibItems){
		RSSAutolearningResponse mAutoResp = new RSSAutolearningResponse(1,0,0);
		if(useRssDiff){
			mAutoResp = mRssAutolearning.calculateRssDiffAutocalibrationParameter(onlineAPs, calibItems);
		}
		else{
			mAutoResp = mRssAutolearning.calculateRssAutocalibrationParameters(onlineAPs, calibItems);
		}
		a2 = mAutoResp.getSlope();
		b2 = mAutoResp.getIntersection();
		residuals2 = mAutoResp.getResiduals();
		float x2 = onlineAPs.get(0).getRSS();
		y2 = a2*x2 + b2;
		y50 = a2*(-50) + b2;
		y70 = a2*(-70) + b2;
		y80 = a2*(-80) + b2;
	}
	
	/*nested class for sorting APData objects by RSS IN DESC ORDER (signs of the 'one' changed with respect to ASCENDENT order)*/
	public class SortByRSS implements Comparator<APData>{
		public int compare(APData arg0, APData arg1) {
			if ( arg0.getRSS() < arg1.getRSS()){
				return 1;
			} else if ( arg0.getRSS() == arg1.getRSS() ){
				return 0;	
			} else if ( arg0.getRSS() > arg1.getRSS() ){
				return -1;
			}
			return 0;
		}
	}

	/*nested class for sorting APData objects by BSSID IN ASC ORDER */
	public class SortByBSSID implements Comparator<APData>{
		public int compare(APData arg0, APData arg1) {
			return arg0.getBSSID().compareTo(arg1.getBSSID());
		}
	}
	
	/*nested class for sorting CalibrationItem objects by RSS IN DESC ORDER (signs of the 'one' changed with respect to ASCENDENT order)*/
	public class SortCiByRSS implements Comparator<CalibrationItem>{
		public int compare(CalibrationItem arg0, CalibrationItem arg1) {
			if ( arg0.getRSS() < arg1.getRSS()){
				return 1;
			} else if ( arg0.getRSS() == arg1.getRSS() ){
				return 0;	
			} else if ( arg0.getRSS() > arg1.getRSS() ){
				return -1;
			}
			return 0;
		}
	}
	
	private Handler handlerShowMsg = new Handler() {
		
		public void handleMessage(Message msg) {
			// do stuff with UI
			Bundle b = msg.getData();
			String message = b.getString("msg");
			Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
		}
	};

	/* event listeners corresponding to RSS transformation */
	public synchronized void addAutocalibrationListener(UpdateAutocalibrationParametersListener listener) {
		mAutocalibrationParametersListeners.add(listener);
	}

	public synchronized void removeAutocalibrationListener(UpdateAutocalibrationParametersListener listener) {
		mAutocalibrationParametersListeners.remove(listener);
	}

	private synchronized void fireNewAutocalibrationEvent(AutocalibrationParameters autoParams){
		for(UpdateAutocalibrationParametersListener listener : mAutocalibrationParametersListeners){
			listener.newUpdate(autoParams);
		}
	}

	/* event listeners corresponding to motion steps info*/
	public synchronized void addMotionStepsInfoListener(MotionStepsInfoListener listener) {
		mMotionStepsInfoListeners.add(listener);
	}

	public synchronized void removeMotionStepsInfoListener(MotionStepsInfoListener listener) {
		mMotionStepsInfoListeners.remove(listener);
	}

	private synchronized void fireNewMotionStepsInfoEvent(int sensorState, ArrayList<Double> stepsList, double orientation, double diffOrientation){
		for(MotionStepsInfoListener listener : mMotionStepsInfoListeners){
			listener.newMotionStepsInfoAvailable(sensorState, stepsList, orientation, diffOrientation);
		}
	}
	
	public Detection getAPposition(String bssid, float rss){
		Detection det = null;
		if(mApsWithPosition!=null){
			for(APWithPosition apwp : mApsWithPosition){
				if(apwp.getBSSID().equalsIgnoreCase(bssid)){
					Point3D posLocal = apwp.getPoint();
					Point3D posGlobal = null;
					if(posLocal!=null){
						posGlobal = convertLocalToGlobal(posLocal);
					}
					det = new Detection(bssid, rss, posLocal, posGlobal);
					break;
				}
			}
		}
		return det;
	}
	
	// class used to assign RSS autolearning parameters manually
	private class AutoParam{
		
		private String MAC = "";
		private double slope, intersection;
		
		private AutoParam(String MAC, double slope, double intersection){
			this.MAC = MAC;
			this.slope = slope;
			this.intersection = intersection;
		}
		
		private String getMac(){
			return MAC;
		}
		
		private double getSlope(){
			return slope;
		}
		
		private double getIntersection(){
			return intersection;
		}
		
		private void setMac(String MAC){
			this.MAC = MAC;
		}
		
		private void setSlope(double slope){
			this.slope = slope;
		}
		
		private void setIntersection(double intersection){
			this.intersection = intersection;
		}
	}

}
