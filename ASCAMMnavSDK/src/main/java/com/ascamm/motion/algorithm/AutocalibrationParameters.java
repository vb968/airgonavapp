package com.ascamm.motion.algorithm;

public class AutocalibrationParameters {
	
	private double slope;
	private double intersection;
	private double residuals;

	public AutocalibrationParameters(double slope, double intersection, double residuals){
		this.slope = slope;
		this.intersection = intersection;
		this.residuals = residuals;
	}

	public double getSlope() {
		return slope;
	}

	public void setSlope(double slope) {
		this.slope = slope;
	}

	public double getIntersection() {
		return intersection;
	}

	public void setIntersection(double intersection) {
		this.intersection = intersection;
	}

	public double getResiduals() {
		return residuals;
	}

	public void setResiduals(double residuals) {
		this.residuals = residuals;
	}

}

