package com.ascamm.motion.algorithm;

import com.ascamm.utils.Point3D;

public class APData {
	private String mSSID;
	private String mBSSID;
	private Point3D mPoint;
	private float mRSS;
	private double mRSSstdev;
	private int mCount;
	private float mCountPercentage;
	
	public APData(String ssid, String bssid, Point3D point, float rss, double rssStdev) {
		mSSID = ssid;
		mBSSID = bssid;
		mPoint = point;
		mRSS = rss;
		mRSSstdev = rssStdev;
	}
	
	public String getSSID(){
		return mSSID;
	}
	
	public String getBSSID(){
		return mBSSID;
	}
	
	public Point3D getPoint(){
		return mPoint;
	}
	
	public float getRSS(){
		return mRSS;
	}
	
	public double getRSSstdev() {
		return mRSSstdev;
	}
	
	public int getCount(){
		return mCount;
	}
	
	public float getCountPercentage(){
		return mCountPercentage;
	}

	public void setRSS(float rss){
		mRSS = rss;
	}
	
	public void setRSSstdev(float RSSstdev) {
		this.mRSSstdev = RSSstdev;
	}
	
	public void setCount(int count){
		mCount = count;
	}
	
	public void setCountPercentage(float countPercentage){
		mCountPercentage = countPercentage;
	}
}
