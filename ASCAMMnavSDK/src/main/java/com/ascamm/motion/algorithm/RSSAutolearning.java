package com.ascamm.motion.algorithm;

import java.util.ArrayList;

import android.util.Log;

import com.ascamm.motion.database.CalibrationItem;

import com.ascamm.motion.utils.Constants;

public class RSSAutolearning {

	private boolean debug = Constants.DEBUGMODE;
	private RSSAutolearningResponse mRssAutolearningResponse;
	
	public RSSAutolearning(){
		mRssAutolearningResponse = new RSSAutolearningResponse(0.0,0.0,0.0);
	}
	
	// Method used to update RSS autolearning parameters
	public RSSAutolearningResponse calculateRssAutocalibrationParameters(ArrayList<APData> onlineAPs,ArrayList<CalibrationItem> calibItems){
		double sumx = 0.0,
				sumy = 0.0,
				sumx2 = 0.0,
				sumy2 = 0.0,
				sumxy = 0.0,
				res = 0.0,
				a = 0.0,
				b = 0.0;
		float x = 0, y = 0;
		int n = 0;
		double meanX = 0, meanY = 0;
		
		for(APData ap : onlineAPs){
			for(CalibrationItem ci : calibItems){
				if(ci.getBSSID().equalsIgnoreCase(ap.getBSSID())){
					x = ap.getRSS();
					y = ci.getRSS();
					sumx += x;
					sumx2 += x * x;
					sumy += y;
					sumy2 += y * y;
					sumxy += x * y;
					res += (x-y)*(x-y);
					n++;
					break;
				}
			}
		}
		
		res = Math.sqrt(res);
		meanX = sumx / n;
		meanY = sumy / n;
		a = (sumxy - sumx * meanY) / (sumx2 - sumx * meanX);
		b = meanY - a* meanX;
		if(debug){
			Log.d("LinearRegression", "sumx: " + sumx);
			Log.d("LinearRegression", "sumy: " + sumy);
			Log.d("LinearRegression", "sumx2: " + sumx2);
			Log.d("LinearRegression", "sumy2: " + sumy2);
			Log.d("LinearRegression", "meanX: " + meanX);
			Log.d("LinearRegression", "meanY: " + meanY);
			Log.d("LinearRegression", "sumxy: " + sumxy);
			Log.d("LinearRegression", "slope: " + a);
			Log.d("LinearRegression", "intersection: " + b);
			Log.d("LinearRegression", "residuals: " + res);
		}
		mRssAutolearningResponse.setSlope(a);
		mRssAutolearningResponse.setIntersection(b);
		mRssAutolearningResponse.setResiduals(res);
		
		return mRssAutolearningResponse;
	}
	
	// Method used to update RSS autolearning parameters
	public RSSAutolearningResponse calculateRssDiffAutocalibrationParameter(ArrayList<APData> onlineAPs,ArrayList<CalibrationItem> calibItems){
		double sumx2 = 0.0,
				sumxy = 0.0,
				res = 0.0,
				a = 0.0;
		float x = 0, y = 0;
				
		for(APData ap : onlineAPs){
			for(CalibrationItem ci : calibItems){
				if(ci.getBSSID().equalsIgnoreCase(ap.getBSSID())){
					x = ap.getRSS();
					y = ci.getRSS();
					sumx2 += x * x;
					sumxy += x * y;
					res += (x-y)*(x-y);
					break;
				}
			}
		}
		
		res = Math.sqrt(res);
		a = sumxy / sumx2;
		if(debug){
			Log.d("LinearRegression", "sumx2: " + sumx2);
			Log.d("LinearRegression", "sumxy: " + sumxy);
			Log.d("LinearRegression", "slope: " + a);
			Log.d("LinearRegression", "residuals: " + res);
		}
		mRssAutolearningResponse.setSlope(a);
		mRssAutolearningResponse.setResiduals(res);
		mRssAutolearningResponse.setIntersection(0);
		
		return mRssAutolearningResponse;
	}
	
}
