package com.ascamm.motion.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.util.Log;

import com.ascamm.motion.database.CalibrationItem;
import com.ascamm.motion.database.PointWithCalibrationItems;
import com.ascamm.motion.utils.Point3DWithFloor;
import com.ascamm.motion.utils.ResponseObject;

import com.ascamm.motion.utils.Constants;

import com.ascamm.utils.Point3D;

public class DistanceCalculation {

	private final String TAG = this.getClass().getSimpleName();
	
	private boolean debug = Constants.DEBUGMODE;
	
	private float DEFAULT_RSS = Constants.mDefaultRss;
	private int mKNumOfNeighbors = Constants.mSettingKNumOfNeighbors;
	
	public DistanceCalculation(){
		
	}

	/** this method calculates distance of point
	 * @param cis	Calibration items (offline measures)
	 * @param onlineAPs	Online APs (online measures)
	 */
	public double calculateDistance(ArrayList<CalibrationItem> cis, ArrayList<APData> onlineAPs){
		
		double sumDist = 0;
		int pen1 = 0;
		int pen2 = 0;
		
		// First we only consider online measurements corresponding to APs of the system
		for(APData ap : onlineAPs){
			float rssCalib = DEFAULT_RSS;
			float rssOnline = ap.getRSS();
			String bssidOnline = ap.getBSSID();
			boolean bssidFound = false;
			for(CalibrationItem ci : cis){
				if(ci.getBSSID().equalsIgnoreCase(bssidOnline)){
					//bssidOnline appears in this PointWithCalibrationItems
					rssCalib = ci.getRSS();
					bssidFound = true;
					break;
				}
			}
			if(!bssidFound){
				// we penalize when we have an online AP that is not in the calibration point (i.e. offline) data
				pen1 += 1;
			}
			
			// We calculate the distance of this particular AP
			sumDist += Math.pow((rssCalib-rssOnline), 2);
		}
		
		// Then we penalize when we have an offline AP that is not in the online data
		for(CalibrationItem ci : cis){
			boolean bssidFound = false;
			for(APData ap : onlineAPs){
				if(ap.getBSSID().equalsIgnoreCase(ci.getBSSID())){
					bssidFound = true;
					break;
				}
			}
			if(!bssidFound){
				float rssCalib = ci.getRSS();
				float rssOnline = DEFAULT_RSS;
				pen2 += 1;
				
				// We calculate the distance of this particular AP
				sumDist += Math.pow((rssCalib-rssOnline), 2);
			}
		}
		
		double euclideanDist = Math.sqrt(sumDist)/(onlineAPs.size() + pen2); 	//avoid divide zero
		//Log.i(TAG,"sumDist: " + sumDist + " , euclideanDist: " + euclideanDist + " , pen1: " + pen1 + " , pen2: " + pen2);
		//System.out.println("euclideanDist: " + euclideanDist + " , pen1: " + pen1 + " , pen2: " + pen2);
		
		return euclideanDist;
	}

	/* this method calculates an estimated position and also the N points with lower distance */
	public ResponseObject calcPositionDist(ArrayList<PointWithCalibrationItems> calibrationSet, ArrayList<APData> onlineAPs){
		ArrayList<PointDistance> pointsWithDist = new ArrayList<PointDistance>();
		
		for(PointWithCalibrationItems pwci : calibrationSet){
			//Log.i(TAG,"point: ( " + pwci.getPositionX() + " , " + pwci.getPositionY() + " , " + pwci.getPositionZ() + " )");
			
			ArrayList<CalibrationItem> cis = pwci.getCalibrationItems();
			double pointDist = calculateDistance(cis, onlineAPs);
			
			// We add the distance of the Point3D to the list of distances
			//System.out.println("point: (" + pwci.getPositionX() + " , " + pwci.getPositionY() + " , " + pwci.getPositionZ() + ") , dist: " + pointDist);
			pointsWithDist.add(new PointDistance(pwci.getPoint(), pointDist));
		}
		
		/* Find K points with highest probability. 
		 * Then we use these K points to calculate the position
		 * with an optimal estimator given the probabilities
		 * */
		Collections.sort(pointsWithDist, new SortByDistance());
		
		int numCandidatesShow = 12;
		int numPointsWithDist = pointsWithDist.size();
		if(numPointsWithDist<numCandidatesShow){
			numCandidatesShow = numPointsWithDist; 
		}
		if(debug){
			for(int i=0; i<numCandidatesShow;i++){
				PointDistance p = pointsWithDist.get(i);
				Log.i(TAG,"cand " + i + " : " + p.getPoint().toString() + " , dist: " + p.getDistance());
			}
		}
				
		double sumPosX=0 , sumPosY=0 , sumPosZ=0 , sumPosFloor=0 , sumDist = 0;
		ArrayList<Point3DWithFloor> candidates = new ArrayList<>();
		int numNeighbors = mKNumOfNeighbors;
		if(pointsWithDist.size() < numNeighbors){
			numNeighbors = pointsWithDist.size(); 
		}
		for (int i=0; i<numNeighbors; i++){
			double posX	= pointsWithDist.get(i).getPoint().x;
			double posY	= pointsWithDist.get(i).getPoint().y;
			double posZ	= pointsWithDist.get(i).getPoint().z;
			double posFloor	= pointsWithDist.get(i).getPoint().getFloor();
			double dist = pointsWithDist.get(i).getDistance();
			
			sumPosX += posX/dist;
			sumPosY += posY/dist;
			sumPosZ += posZ/dist;
			sumPosFloor += posFloor/dist;
			sumDist += 1/dist;
			candidates.add(new Point3DWithFloor(posX,posY,posZ,posFloor));
		}
		
		Point3DWithFloor estimatedPos = new Point3DWithFloor(sumPosX/sumDist, sumPosY/sumDist, sumPosZ/sumDist, Math.round(sumPosFloor/sumDist));
		/*
		System.out.println("estimated pos: (" + estimatedPos.x + " , " + estimatedPos.y + " , " + estimatedPos.z + " )");
		for(Point3D cand : candidates){
			System.out.println("candidate: (" + cand.x + " , " + cand.y + " , " + cand.z + " )");
		}
		*/
		return new ResponseObject(estimatedPos,candidates,null, null);
	}
	
	/*nested class for sorting object by Distance IN ASC ORDER */
	public class SortByDistance implements Comparator<PointDistance>{
		public int compare(PointDistance arg0, PointDistance arg1) {
			if ( arg0.getDistance() < arg1.getDistance()){
				return -1;
			} else if ( arg0.getDistance() == arg1.getDistance() ){
				return 0;	
			} else if ( arg0.getDistance() > arg1.getDistance()){
				return 1;
			}
			return 0;
		}
	}
	
}
