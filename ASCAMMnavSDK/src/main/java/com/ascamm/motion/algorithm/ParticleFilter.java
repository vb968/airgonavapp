package com.ascamm.motion.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Random;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.ascamm.motion.mems.KalmanFilterOrientation;
import com.ascamm.motion.mems.MotionMode;
import com.ascamm.motion.mems.MotionSTEPS;
import com.ascamm.motion.routing.Connection;
import com.ascamm.motion.utils.Constants;
import com.ascamm.motion.utils.Edge;
import com.ascamm.motion.utils.EdgeWithWeight;
import com.ascamm.motion.utils.MotionStepsInfoListener;
import com.ascamm.motion.utils.Point3DWithFloor;
import com.ascamm.motion.utils.Point3DWithFloorAndEdge;
import com.ascamm.motion.utils.Settings;
import com.ascamm.motion.utils.Util;
import com.ascamm.utils.Point3D;

/**
 * This class provides the methods for executing the recursive Particle Filter steps
 * and calculate the Filter outputs. 
 * @author  CTAE
 */   
public class ParticleFilter {
	
	private String TAG = getClass().getSimpleName();
	
	private boolean debug = false;

	private Context mContext;
	private MapFusion mMapFusion;

	// filter motionSTEPS integration - Fusion of sensors with particle filter
	private MotionSTEPS motionSteps;

	private ArrayList<Double> mListOfStepsCopy;
	private double orientationMEMS;
	private double diffOrientationMEMS;
	private float mPxMeter;
	
	// filter outputs
	public double[] out;

	// Matrix containing a gaussian dispersion of values
	//private Matrix GaussianNoise = new Matrix (4, 1);
	private double GaussianNoise = 0.0;
	
	// Attributes containing the previous state of the particle
	Edge edgek_1;
	double distk_1;
	double velk_1;
	double anglek_1;
	Point3DWithFloor particlePosk_1 = new Point3DWithFloor();
	
	// Attributes containing the current state of the particle
	Edge edgek;
	double distk;
	double velk;
	double anglek;
	Point3DWithFloor particlePosk = new Point3DWithFloor();
	Point3DWithFloor particlePosk_constrained = new Point3DWithFloor();
	double anglek_to_apply;

	// array of particles with previous state and weights
	private ArrayList<ParticleStateConstrained> particlestate_Xk_1;

	// array of particles with present state and weights
	private ArrayList<ParticleStateConstrained> particlestate_Xk;

	// array of particles with present state that are sent to app to be represented
	private ArrayList<ParticleState> particlestate_Xk_toPlot;
	
	// variable containing the sum of all the weights of the particles
	private double Wsum;
	// variable containing the maximum weight of all the particles
	private double maxWeight;
	// variable containing the edge that has the maximum sum of weights of the particles in it
	private Edge maxWeightedEdge;
	// variable containing the sum of the weights of the particles of the maximum weighted edge
	private double maxWeightedEdgeWeight;

	// Array with the Replication factors
	private int[] replicationFactor;

	// x coordinate  used by the Particle Filter out
	private int outPFx=0;
	// y coordinate  used by the Particle Filter out
	private int outPFy=0;
	// z coordinate  used by the Particle Filter out
	private int outPFz=0;
	// floor used by the Particle Filter out
	private int outPFfloor=0;

	// x coordinate  (FREE, not constrained to the grid) used by the Particle Filter out
	private double outPFx_free=0;
	// y coordinate  (FREE, not constrained to the grid) used by the Particle Filter out
	private double outPFy_free=0;
	// z coordinate  (FREE, not constrained to the grid) used by the Particle Filter out
	private double outPFz_free=0;
	// floor (FREE, not constrained to the grid) used by the Particle Filter out
	private double outPFfloor_free=0;

	// PF initial control variable
	private int toInitFilter=0;

	// Number of particles for the filter
	private int mNumOfParticle;


	/////////////////
	//Sampling step//
	/////////////////

	// Initial weight for the initial distribution
	private double Wo;
	// Time sampling (elapsed time between two measurements) for the Importance Function
	private double mTs;
	// Variance used in the Importance Function that try to simulate the human motion walk acceleration
	private double mSigmaStatic;
	private double mSigmaWalking;
	private double mSigma;
	// Variance used in the Importance Function for the initial distribution
	private double mSigma0;//Value used based in the paper: Experimental Deployment of Particle Filters in WiFi Networks - sgma_0

	//////////////////////////////////////
	//importance weight computation step//
	//////////////////////////////////////

	// settings for likelihood function - update  weights
	private double sigmaDist;
	private static double RANGE_DIST = Constants.PF_RANGE;
	private static final boolean useVariableSigmaDist = false;

	///////////////////
	//Resampling step//
	///////////////////

	// Threshold number for trigger Resampling step
	private int Nt;

	private boolean mUseParticleFilterSensors;
	private boolean useOnlySteps = false;
	private boolean useMotionStepsListener = true;
	private Point3DWithFloor fingerprintPosition;
	private boolean newFingerprintPosition = false;

	private synchronized Point3DWithFloor getFingerprintPosition(){
		if(fingerprintPosition==null){
			return null;
		}
		Point3DWithFloor position = new Point3DWithFloor(fingerprintPosition.x, fingerprintPosition.y, fingerprintPosition.z, fingerprintPosition.getFloor());
		return position;
	}

	private synchronized void setFingerprintPosition(Point3DWithFloor position){
		fingerprintPosition = position;
		newFingerprintPosition = true;
	}

	private synchronized boolean getNewFingerprintPosition(){
		return newFingerprintPosition;
	}

	private synchronized void setNewFingerprintPosition(boolean value){
		newFingerprintPosition = value;
	}

	private MotionStepsInfoListener mMotionStepsInfoListener = new MotionStepsInfoListener() {
		@Override
		public void newMotionStepsInfoAvailable(int motionMode, ArrayList<Double> stepsList, double orientation, double diffOrientation){
			Point3DWithFloor FPposition = getFingerprintPosition();
			if(FPposition != null) {
				boolean newFingerprintPosition = getNewFingerprintPosition();
				//Log.d(TAG,"inside newMotionStepsInfoAvailable. FPposition: " + FPposition.toString() + " , newFP: " + newFingerprintPosition);
				setNewFingerprintPosition(false);
				processNewStepsInfo(motionMode, stepsList, orientation, diffOrientation);
				recursiveSIRF(FPposition, newFingerprintPosition);
			}
			else{
				//Log.d(TAG,"inside newMotionStepsInfoAvailable. FPposition is null");
			}
		}
	};

	//RP: Fix Mechanical stairs problem
	private boolean inMechanicalStairsSituation = false;
	private double edgeTransitionResamplingThreashold = 1.5;
	private int edgeTransitionResamplingNumOfParticles = (int) (float)
            mNumOfParticle/4;
	
	public static int isResampled = 0;
	public static int numResets = 0;

	private boolean particleForwarded = false;

	private boolean usePathConstrains = false;
	private boolean useHeadingChange = true;
	private boolean useMovementWeight = false;

	private Hashtable<Edge,Double> mTableEdges = new Hashtable<Edge,Double>();

	private Point3DWithFloor userPosition;
	private float userOrientation = -1;
	private float userOrientationReliability = 0;

	private boolean useKalmanHeading = false;
	private KalmanFilterOrientation mKalmanHeading;
	private float lastEpochOrientation, lastEpochP;

	private int mMotionMode = 0;

	//private double STEP_LENGTH_NOISE_WALKING = 0.10; // meters
	//private double STEP_LENGTH_NOISE_WALKING = 0.15; // meters
	private double STEP_LENGTH_NOISE_WALKING = 0.50; // meters
	//private double STEP_LENGTH_NOISE_WALKING = 0; // meters

	private double HEADING_NOISE_WALKING = 5; // degrees
	//private double HEADING_NOISE_WALKING = 0.25; // degrees
	//private double HEADING_NOISE_WALKING = 20; // degrees
	//private double HEADING_NOISE_WALKING = 30; // degrees
	//private double HEADING_NOISE_WALKING = 90; // degrees
	//private double HEADING_NOISE_WALKING = 0; // degrees

	private double STEP_LENGTH_NOISE_STATIC = 0.25; // meters
	//private double STEP_LENGTH_NOISE_STATIC = 0; // meters

	//private double HEADING_NOISE_STATIC = 30; // degrees
	private double HEADING_NOISE_STATIC = 90; // degrees
	//private double HEADING_NOISE_STATIC = 0.25; // degrees
	private double HEADING_NOISE_STATIC_REAL = 0.25; // degrees

	private double STEP_LENGTH_NOISE_IRREGULAR_WALKING = 0.25; // meters

	private double HEADING_NOISE_IRREGULAR_WALKING = 2.5; // degrees

	private double STEP_LENGTH_NOISE_IRREGULAR_STATIC = 0.25; // meters

	private double HEADING_NOISE_IRREGULAR_STATIC = 2.5; // degrees

	private double HEADING_NOISE_CORRECTION = 1; // degrees

	private int numSteps = 0;

	private int epochsSinceResampling = 0;

	private static double SIGMA_MOVEMENT = 0.20; // meters

	//Particle heading correction method variables
	private double mStepsDistance; // meters
	private double accumulatedDistance = 0; // meters
	private double diffOrientationThreshold = 10; // degrees
	private double accumulatedDistanceThreshold = 30; // meters (default=4)
	private double diffParticleAngleEdgeThreshold = 15; // degrees
	private boolean applyCorrectionMethod = false;
	private boolean resetAngleEdgeParticles = false;

	//Initial period of PF variables
	private boolean isInInitPeriod = true;
	private double initAccumulatedDistance = 0; // meters
	private static double INIT_DISTANCE = 30; // meters
	private double INIT_SIGMA_DIST = 3; // meters (default: 1.5)

	private double sigmaDistToApply;

	private boolean useMagneticFieldOrientationInInit = false;

	// variables for creating particles when initializing filter
	private static final double INIT_SIGMA_DIST_0 = 2;
	private static final double INIT_SIGMA_ANGLE = 90;

	private boolean lastStateWalking = false;

	private synchronized void addNumSteps(int numSteps){
		this.numSteps += numSteps;
	}

	public void updateTsAndABMatrix(double ts){
		mTs = ts;
	}

	/**
	 *  If any of the changeable parameters is modified, 
	 *  this function updates all the related parameters accordingly. 
	 */
	public ParticleFilter(Context context, float pxMeter,int numberOfParticles, double ts, double sigmaStatic, double sigmaWalking, double sigma0, double sigma_dist, MapFusion mapFusionRef, boolean useParticleFilterSensors, MotionSTEPS motionStepsObject){

		mContext = context;

		mMapFusion = mapFusionRef;

		mNumOfParticle = numberOfParticles;
		mSigmaStatic = sigmaStatic;
		mSigmaWalking = sigmaWalking;

		mSigma0 = sigma0; 

		sigmaDist = sigma_dist;

		Wo = 1./mNumOfParticle;
		
		mUseParticleFilterSensors = useParticleFilterSensors;
		//Log.d(TAG,"useParticleFilterSensors: " + mUseParticleFilterSensors);
		if(mUseParticleFilterSensors){
			motionSteps = motionStepsObject;
			if(useMotionStepsListener){
				motionSteps.addMotionStepsInfoListener(mMotionStepsInfoListener);
			}

			mListOfStepsCopy= new ArrayList<Double>();
			mPxMeter = pxMeter;
			//Log.d(TAG,"pxMeter: " + pxMeter);
		}
		else{
			sigmaDistToApply = sigmaDist;
		}

		if(useKalmanHeading){
			mKalmanHeading = new KalmanFilterOrientation();
		}

		updateTsAndABMatrix(ts);

		// Threshold number for trigger Resampling step
		//Nt=Math.round(mNumOfParticle/8);
		Nt=Math.round(mNumOfParticle/2);

		// settings for likelihood function - update  weights
		RANGE_DIST *= pxMeter;
		INIT_SIGMA_DIST *= pxMeter;

	}

	// process info coming from MotionSTEPS class related to PDR
	private void processNewStepsInfo(int motionMode, ArrayList<Double> stepsList, double orientation, double diffOrientation){
		mMotionMode = motionMode;
		mListOfStepsCopy = new ArrayList<Double>();
		mStepsDistance = 0;
		if(mMotionMode != MotionMode.STATIC.ordinal()) {
			for (double stepDistance : stepsList) {
				mListOfStepsCopy.add(stepDistance);
				mStepsDistance += stepDistance;
			}
		}
		orientationMEMS = orientation;
		diffOrientationMEMS = diffOrientation;
		addNumSteps(stepsList.size());
		if(isInInitPeriod){
			initAccumulatedDistance += mStepsDistance;
			if(initAccumulatedDistance>INIT_DISTANCE){
				isInInitPeriod = false;
				if(debug)	Toast.makeText(mContext,"End of initial period", Toast.LENGTH_SHORT).show();
			}
			sigmaDistToApply = INIT_SIGMA_DIST;
		}
		else{
			sigmaDistToApply = sigmaDist;
		}
	}

	/**
	 * Generate random value with gaussian distribution - simulate the acceleration of the particle
	 * @param sigma		Standard deviation of the gaussian distribution
	 */
	private void generateRandomDist(double sigma) {
		GaussianNoise = Util.gaussian(0, sigma);
	}
	
	/** 
	 * This method calculates the new edge on which a particle will propagate
	 * once it reaches an end node 'node_from' of the current edge 'edge_from' 
	 * @return New edge
	 */
	private Edge obtainNewEdge(Edge edge_from, int id_node_from,
							   Point3DWithFloor node_from, double heading) {
		
		/*
		 * get the edges "in contact" with the node node_from (edges that contain the node node_from
		 * that are not the own edge edge_from)
		 */
		ArrayList<Edge> edgesInContact = new ArrayList<Edge>();
		for (Edge e : mMapFusion.mEdgesByFloor.get((int)node_from.getFloor())){
			if((id_node_from == e.startNodeId || id_node_from == e.endNodeId) && (e.edgeId != edge_from.edgeId)){
				edgesInContact.add(e);
			}
		}
		
		
		if(edgesInContact.size() == 1){
			//it is a turn, no choice
			return edgesInContact.get(0);
		}
		if(edgesInContact.size() != 0){

			if(heading == -1) {

				//select one of the edges based on a pure random choice
				Random r = new Random();
				//random int between 0 (inclusive) and edgesize (exclusive):
				int edgeindex = r.nextInt(edgesInContact.size());
				return edgesInContact.get(edgeindex);



				/*
				 * edge selection that gives more probability to straight
				 * trajectories
				 */
				/*
				int edgeindex = 0;
				double[] ProbIntervals = new double[edgesInContact.size()+1];
				double probEdge, SumProbs = 0.0;
				double angle;

				ProbIntervals[0] = 0;
				int i = 1;
				Point3D Anodefrom = node_from;
				Point3D Bnodefrom;
				if(edge_from.getStartNodeId() == id_node_from){
					Bnodefrom = edge_from.getEndNode();
				}else{
					Bnodefrom = edge_from.getStartNode();
				}

				for (Edge e : edgesInContact){
					Point3D Anodeedge = node_from;
					Point3D Bnodeedge;
					if(e.getStartNode().equals(node_from)){
						Bnodeedge = e.getEndNode();
					}else{
						Bnodeedge = e.getStartNode();
					}
					double vectorEdgeFrom_x = Bnodefrom.x - Anodefrom.x;
					double vectorEdgeFrom_y = Bnodefrom.y - Anodefrom.y;
					double vectorEdge_x = Bnodeedge.x - Anodeedge.x;
					double vectorEdge_y = Bnodeedge.y - Anodeedge.y;
					double dotproduct = vectorEdgeFrom_x*vectorEdge_x + vectorEdgeFrom_y*vectorEdge_y;
					double magnitudeVEdgeFrom = Math.sqrt(Math.pow(vectorEdgeFrom_x,2) + Math.pow(vectorEdgeFrom_y,2));
					double magnitudeVEdge = Math.sqrt(Math.pow(vectorEdge_x,2) + Math.pow(vectorEdge_y,2));
					double dotproductMagnitudes = magnitudeVEdgeFrom * magnitudeVEdge;
					double cosAngle = Math.abs((double)(dotproduct/dotproductMagnitudes));

					probEdge = 1 + cosAngle;
					ProbIntervals[i] = probEdge;
					SumProbs += probEdge;
					i++;
				}
				for (int j=1; j<edgesInContact.size()+1; j++){
					ProbIntervals[j] = ProbIntervals[j]/SumProbs;
				}
				for (int j=1; j<edgesInContact.size()+1; j++){
					ProbIntervals[j] += ProbIntervals[j-1];
				}

				Random r = new Random();
				double a = r.nextDouble(); //random number in [0,1)
				i=0;
				boolean found = false;
				while(!found){
					if((a>=ProbIntervals[i]) && (a<=ProbIntervals[i+1])){
						edgeindex = i;
						found = true;
					}
					i++;
				}
				return edgesInContact.get(edgeindex);
				*/
			}
			else {

				/*
				 * edge selection that gives more probability to trajectories similar to heading
				 */

				int edgeindex = 0;
				double[] ProbIntervals = new double[edgesInContact.size() + 1];
				double probEdge, SumProbs = 0.0;

				ProbIntervals[0] = 0;
				int i = 1;
				boolean useMaxProb = true;
				Edge mostProbableEdge = null;
				double maxProb = 0;

				for (Edge e : edgesInContact) {
					double edgeAngle;
					if (e.getStartNode().equals(node_from)) {
						edgeAngle = e.getLineAngle1();
					} else {
						edgeAngle = e.getLineAngle2();
					}

					double angleDiff = edgeAngle - heading;
					probEdge = Math.cos(Math.toRadians(angleDiff));
					if(probEdge<0){
						probEdge = 0.001;
					}
					if(probEdge>maxProb){
						mostProbableEdge = e;
						maxProb = probEdge;
					}
					//Log.d(TAG,"probEdge ("+i+"): " + probEdge + " , angleDiff: " + angleDiff + " , edgeAngle: " + edgeAngle + " , heading: " + heading);
					ProbIntervals[i] = probEdge;
					SumProbs += probEdge;
					i++;
				}
				if(useMaxProb){
					//Log.d(TAG,"mostProbableEdge: " + mostProbableEdge.edgeId);
					return mostProbableEdge;
				}

				//Log.d(TAG, "normalizing prob intervals...");
				for (int j = 1; j < edgesInContact.size() + 1; j++) {
					ProbIntervals[j] = ProbIntervals[j] / SumProbs;
					//Log.d(TAG,"("+j+","+ProbIntervals[j]+")");
				}

				//Log.d(TAG, "accumulating prob intervals...");
				for (int j = 1; j < edgesInContact.size() + 1; j++) {
					ProbIntervals[j] += ProbIntervals[j - 1];
					//Log.d(TAG,"("+j+","+ProbIntervals[j]+")");
				}

				Random r = new Random();
				double a = r.nextDouble(); //random number in [0,1)
				i = 0;
				boolean found = false;
				while (!found) {
					if ((a >= ProbIntervals[i]) && (a <= ProbIntervals[i + 1])) {
						edgeindex = i;
						found = true;
					}
					i++;
				}
				//Log.d(TAG,"found: " + found + " , a: " + a + " , i: " + edgeindex);
				return edgesInContact.get(edgeindex);
			}

		}
		else{
			//the particle has reached a node with no connection with other edges
			return null;
		}
	}

	/** 
	 * This method calculates the sum of the replication factors
	 * @return Sum of the replication factors
	 */
	private int calcRsum() {
		int Rsum=0;
		for (int j = 0; j < mNumOfParticle; j++) 
			Rsum=Rsum + replicationFactor[j];

		return Rsum;
	}

	/* This method generates an array of replication factors for each particle */
	private void rGeneration (){

		//uniform random number according to U~[0,1/Np]
		double U = (1./mNumOfParticle)*Math.random();
		double ss=0; 	//initialize cumulative sum of weights
		int kk;   		//Replication factor counter

		for (int j = 0; j < mNumOfParticle; j++) {

			kk=0; //initialize replication factor counter
			ss= ss + particlestate_Xk.get(j).Weight; //update the cumulative sum of weights

			//Resampling loop
			while (ss>U) {	
				kk=kk+1;
				U=U+(1./mNumOfParticle);
			}
			replicationFactor[j]=kk; //storing replication factors
		}

	}

	/* This method based in the replication factor array assigns the new state variables and weights */
	void assignNewSamplesWeights (){

		int counter=0; 

		for (int mm = 0; mm < mNumOfParticle; mm++) {

			for (int nn = 0; nn < replicationFactor[mm]; nn++) {

				Edge edge = particlestate_Xk.get(mm).stateEdge;
				double weight = 1./mNumOfParticle;

				particlestate_Xk_1.get(counter).stateEdge	=	edge;
				particlestate_Xk_1.get(counter).stateDist	=	particlestate_Xk.get(mm).stateDist;
				particlestate_Xk_1.get(counter).stateVel	=	particlestate_Xk.get(mm).stateVel;
				particlestate_Xk_1.get(counter).stateAngle	=	particlestate_Xk.get(mm).stateAngle;
				particlestate_Xk_1.get(counter).Weight		=	weight;
				particlestate_Xk_1.get(counter).stateAngleEdge = particlestate_Xk.get(mm).stateAngleEdge;
				particlestate_Xk_1.get(counter).correctionNeeded = particlestate_Xk.get(mm).correctionNeeded;

				counter++;
				if(mTableEdges.containsKey(edge)){
					double oldWeight = mTableEdges.get(edge);
					mTableEdges.put(edge, oldWeight + weight);
				}
				else{
					mTableEdges.put(edge, weight);
				}
			}
		}

	}

	/**
	 * Implementation of the likelihood function - one-dimensional Gaussian function
	 * @param estimatedPos	Estimated position by the FingerPrint algorithm
	 * @param particlePos	Predicted position of the particle
	 * @return		Value of likelihood
	 */
	private double Likeli(Point3D estimatedPos, Point3D particlePos) {
		double distance = Util.distanceBetweenPoints(estimatedPos, particlePos);
		double like = evaluateGaussian(distance, 0, sigmaDistToApply);
		like *= evaluateRange(distance, RANGE_DIST);
		return like;
	}

	/* method used to evaluate point in a gaussian function */
	private double evaluateGaussian(double x, double mu, double sigma){
		double amp = 1/(Math.sqrt(2*Math.PI)*sigma);
		double res = amp*Math.exp(-1*Math.pow((x-mu),2)/(2*Math.pow(sigma,2)));
		return res;
	}

	/* method used to evaluate point in a range */
	private double evaluateRange(double distance, double range){
		double res = 0;
		if(distance <= range){
			res = 1;
		}
		return res;
	}

	/**
	 * This method normalizes the weights and calculates effective sample size
	 */
	private double normWeightsAndCalculateNeff() {
		//Log.e(TAG,"Wsum: " + Wsum + " , maxWeight: " + maxWeight);
		if(Wsum==0)
			return -1;

		maxWeight = -1;
		double aux = 0;
		for (int j = 0; j < particlestate_Xk.size(); j++){
			double normWeight = particlestate_Xk.get(j).Weight / Wsum;
			//Log.i(TAG,"normWeight: " + normWeight);
			particlestate_Xk.get(j).Weight = normWeight;
			if(normWeight>maxWeight){
				maxWeight = normWeight;
			}
			aux = aux+ Math.pow(normWeight,2);
		}
		double Neff = 1./aux;
		return Neff;

	}

	/* method used to calculate propagation of particle depending on its heading */
	private void calculateDistKfromHeading(double heading, double stepLength, Edge edge){
		float angleTolerance = 180;
		float edgeAngle1 = edge.lineAngle1;
		float edgeAngle2 = edge.lineAngle2;
		float extraAngle1 = 1000;
		float extraAngle2 = 1000;
		if(edgeAngle1==0){
			extraAngle1 = 360;
		}
		else if(edgeAngle1>(360-angleTolerance/2)){
			extraAngle1 = edgeAngle1 - 360;
		}
		else if(edgeAngle1<angleTolerance/2){
			extraAngle1 = edgeAngle1 + 360;
		}
		if(edgeAngle2==0){
			extraAngle2 = 360;
		}
		else if(edgeAngle2>(360-angleTolerance/2)) {
			extraAngle2 = edgeAngle2 - 360;
		}
		else if(edgeAngle2<angleTolerance/2){
			extraAngle2 = edgeAngle2 + 360;
		}
		float limit1Up = (edgeAngle1 + angleTolerance/2);
		float limit1Down = (edgeAngle1 - angleTolerance/2);
		float limit2Up = (edgeAngle2 + angleTolerance/2);
		float limit2Down = (edgeAngle2 - angleTolerance/2);
		float limit1ExtraUp = (extraAngle1 + angleTolerance/2);
		float limit1ExtraDown = (extraAngle1 - angleTolerance/2);
		float limit2ExtraUp = (extraAngle2 + angleTolerance/2);
		float limit2ExtraDown = (extraAngle2 - angleTolerance/2);
		//Log.i(TAG,"edge angles: (" + edgeAngle1 + " , " + edgeAngle2 + ") , extraAngle: " + extraAngle);
		//Log.i(TAG,"limits: (" + limit1Up + "," + limit1Down + ") , (" + limit2Up + "," + limit2Down + ") , (" + limitExtraUp + "," + limitExtraDown + ")");
		if(((heading<limit1Up) && (heading>limit1Down)) || ((heading<limit1ExtraUp) && (heading>limit1ExtraDown))){
			//velk = 1;
			distk = distk_1 + stepLength;
			//Log.d(TAG,"MEMS MOV --- moving particle FORWARD. stepLength: "+stepLength+" , distk: "+distk+")");
		}
		else if(((heading<limit2Up) && (heading>limit2Down)) || ((heading<limit2ExtraUp) && (heading>limit2ExtraDown))){
			//velk = -1;
			distk = distk_1 - stepLength;
			//Log.d(TAG,"MEMS MOV --- moving particle BACKWARD. stepLength: "+stepLength+" , distk: "+distk+")");
		}
		else{
			//velk = velk_1;
			distk = distk_1;
		}
	}

	/* method used to calculate propagation of particle depending on forward input */
	private void calculateDistKwithoutHeading(double stepLength, boolean forward){
		if(forward) {
			distk = distk_1 + stepLength;
			//Log.d(TAG,"MEMS MOV --- moving particle FORWARD. stepLength: "+stepLength+" , distk: "+distk+")");
		}
		else{
			distk = distk_1 - stepLength;
			//Log.d(TAG,"MEMS MOV --- moving particle BACKWARD. stepLength: "+stepLength+" , distk: "+distk+")");
		}
	}

	/* method used to calculate new particle state variables in case an edge change occurred */
	private void checkEdgeChange(double finalHeading){
		float longEdgeK_1 = edgek_1.length;
		if((distk >= 0) && (distk <= longEdgeK_1)){
			edgek = edgek_1; //no edge change, same edge
			//same distance distk, we do not update it
			//same velocity velk, we do not update it
		}
		else{
			//Log.d(TAG,"before while");
			//int i = 0;
			while((distk > longEdgeK_1) || (distk < 0)){
				//i++;
				if(distk > longEdgeK_1){
					edgek = obtainNewEdge(edgek_1, edgek_1.getEndNodeId(), edgek_1.getEndNode(), finalHeading);
					if(edgek != null){
						if(edgek_1.endNodeId == edgek.startNodeId){
							distk = distk - longEdgeK_1;
						}else if(edgek_1.endNodeId == edgek.endNodeId){
							distk = edgek.length - (distk - longEdgeK_1);
							velk = velk * (-1);
						}
						// update current edge (in case it is necessary for next iteration)
						edgek_1 = edgek;
						longEdgeK_1 = edgek.length;
					}
					else{
						/*
						 * the particle has reached a node with no connection with other edges
						 * we must "reflect" the particle on the same current edge
						 */
						edgek = edgek_1; //no edge change, same edge
						distk = edgek_1.length - (distk - longEdgeK_1);
						velk = velk * (-1);
					}

				}else if(distk < 0){
					edgek = obtainNewEdge(edgek_1, edgek_1.getStartNodeId(), edgek_1.getStartNode(), finalHeading);
					if(edgek != null){
						if(edgek_1.startNodeId == edgek.startNodeId){
							distk = Math.abs(distk);
							velk = velk * (-1);
						}else if(edgek_1.startNodeId == edgek.endNodeId){
							distk = edgek.length - Math.abs(distk);
						}
						// update current edge (in case it is necessary for next iteration)
						edgek_1 = edgek;
						longEdgeK_1 = edgek.length;
					}
					else{
						/*
						 * the particle has reached a node with no connection with other edges
						 * we must "reflect" the particle on the same current edge
						 */
						edgek = edgek_1; //no edge change, same edge
						distk = Math.abs(distk);
						velk = velk * (-1);
					}
				}
			}
			//Log.d(TAG,"after while. iterations: " + i);
		}
	}

	/* this method propagates particles without PDR information */
	private void particlePredictionPathConstrainedWithoutSensors(){
		//Generate random vector with gaussian distribution - simulate the acceleration of the particle
		generateRandomDist(mSigma);

		//Calculus of Importance equation
		velk = velk_1 + (GaussianNoise * mTs);
		distk = distk_1 + (velk * mTs) + ((GaussianNoise * Math.pow(mTs, 2))/2);

		double finalHeading = -1;
		checkEdgeChange(finalHeading);
		//we calculate the 3D coordinates of the particle, from its edge and distance over the edge
		particlePosk.x = edgek.startNode.x + ((edgek.dX / edgek.length) * distk);
		particlePosk.y = edgek.startNode.y + ((edgek.dY / edgek.length) * distk);
		particlePosk.z = edgek.startNode.z + ((edgek.dZ / edgek.length) * distk);
		particlePosk.setFloor(edgek.startNode.getFloor() + ((edgek.dFloor / edgek.length) * distk));
		anglek = angleLine(particlePosk_1, particlePosk);
	}

	/* this method propagates particles with steps information from PDR and random orientation */
	private void particlePredictionPathConstrainedWithSensorsOnlySteps(){
		double finalHeading = -1;
		if(mListOfStepsCopy.size() > 0){
			Iterator iterator = mListOfStepsCopy.iterator();
			double stepLength;
			double randomNumber = Util.generateRandomDoubleBetween(-1,1);
			double multiplyNumber = 1;
			if(randomNumber<0){
				multiplyNumber = -1;
			}
			while (iterator.hasNext()) {
				Double step = (Double)iterator.next();
				stepLength = Util.gaussian(step, STEP_LENGTH_NOISE_WALKING);
				stepLength = stepLength * this.mPxMeter;
				distk = distk_1 + multiplyNumber*2*stepLength;
			}
		}else{
			double noise = STEP_LENGTH_NOISE_STATIC*this.mPxMeter;
			distk = Util.gaussian(distk_1, noise);
		}
		velk = velk_1;
		checkEdgeChange(finalHeading);
		//we calculate the 3D coordinates of the particle, from its edge and distance over the edge
		particlePosk.x = edgek.startNode.x + ((edgek.dX / edgek.length) * distk);
		particlePosk.y = edgek.startNode.y + ((edgek.dY / edgek.length) * distk);
		particlePosk.z = edgek.startNode.z + ((edgek.dZ / edgek.length) * distk);
		particlePosk.setFloor(edgek.startNode.getFloor() + ((edgek.dFloor / edgek.length) * distk));
		anglek = angleLine(particlePosk_1, particlePosk);
	}

	/* this method propagates particles with steps information from PDR, but conditioned to paths */
	private void particlePredictionPathConstrainedWithSensors(){
		double finalHeading = -1;
		if(mListOfStepsCopy.size() > 0){
			Iterator iterator = mListOfStepsCopy.iterator();
			double stepLength, heading;
			if(particleForwarded){
				particleForwarded = false;
			}
			else{
				particleForwarded = true;
			}

			while (iterator.hasNext()) {
				Double step= (Double)iterator.next();
				stepLength = Util.gaussian(step, STEP_LENGTH_NOISE_WALKING);
				stepLength = stepLength * this.mPxMeter;

				heading = Util.gaussian(orientationMEMS, HEADING_NOISE_WALKING);
				finalHeading = heading;
				//Log.d(TAG,"MEMS MOV --- stepLength: "+stepLength +" , heading: "+heading+" ,  angle1: "+edgek_1.getLineAngle1()+" , angle2: "+edgek_1.getLineAngle2()+" , startNodeId: " + edgek_1.getStartNodeId() + " , endNodeId: " + edgek_1.getEndNodeId());

				calculateDistKfromHeading(heading, stepLength, edgek_1);
				//calculateDistKwithoutHeading(stepLength, particleForwarded);
			}

		}else{
			//velk = velk_1;
			//distk = distk_1;
			double noise = STEP_LENGTH_NOISE_STATIC*this.mPxMeter;
			distk = Util.gaussian(distk_1, noise);
		}

		checkEdgeChange(finalHeading);
		//we calculate the 3D coordinates of the particle, from its edge and distance over the edge
		particlePosk.x = edgek.startNode.x + ((edgek.dX / edgek.length) * distk);
		particlePosk.y = edgek.startNode.y + ((edgek.dY / edgek.length) * distk);
		particlePosk.z = edgek.startNode.z + ((edgek.dZ / edgek.length) * distk);
		particlePosk.setFloor(edgek.startNode.getFloor() + ((edgek.dFloor / edgek.length) * distk));
	}

	/* this method propagates particles with steps information from PDR */
	private void particlePredictionNonPathConstrainedWithSensors(){
		double stepLength = 0;
		double particleHeading = orientationMEMS;
		if(useHeadingChange){
			particleHeading = anglek_1 + diffOrientationMEMS;
		}
		double headingNoise;
		double stepNoise;
		if(mMotionMode == MotionMode.WALKING.ordinal()){
			stepNoise = Util.gaussian(0, STEP_LENGTH_NOISE_WALKING);
			headingNoise = Util.gaussian(0, HEADING_NOISE_WALKING);
			anglek = particleHeading + headingNoise;
		}
		else if(mMotionMode == MotionMode.STATIC.ordinal()){
			stepNoise = Util.gaussian(0, STEP_LENGTH_NOISE_STATIC);
			headingNoise = Util.gaussian(0, HEADING_NOISE_STATIC);
			anglek = particleHeading + Util.gaussian(0, HEADING_NOISE_STATIC_REAL);
		}
		else{
			if(mStepsDistance != 0) {
				stepNoise = Util.gaussian(0, STEP_LENGTH_NOISE_IRREGULAR_WALKING);
				headingNoise = Util.gaussian(0, HEADING_NOISE_IRREGULAR_WALKING);
			}
			else{
				stepNoise = Util.gaussian(0, STEP_LENGTH_NOISE_IRREGULAR_STATIC);
				headingNoise = Util.gaussian(0, HEADING_NOISE_IRREGULAR_STATIC);
			}
			anglek = particleHeading + headingNoise;
		}
		/*
		if(mStepsDistance != 0){
			stepNoise = Util.gaussian(0, STEP_LENGTH_NOISE_WALKING);
			headingNoise = Util.gaussian(0, HEADING_NOISE_WALKING);
		}
		else{
			stepNoise = Util.gaussian(0, STEP_LENGTH_NOISE_STATIC);
			headingNoise = Util.gaussian(0, HEADING_NOISE_STATIC);
		}
		*/
		double stepsDistance = mStepsDistance;
		if(edgek_1.dFloor!=0){
			//it is a transition edge
			double angleZ = edgek_1.getAngleZ();
			if(angleZ != Math.PI/2){
				stepsDistance /= Math.cos(angleZ);
			}
			stepsDistance *= 0.5;
		}
		stepLength = stepsDistance + stepNoise;
		// convert meters to pixels
		stepLength = stepLength * this.mPxMeter;
		double headingToUse = particleHeading + headingNoise;
		//we calculate the 3D coordinates of the particle, applying stepLength and heading
		particlePosk.x = particlePosk_1.x + stepLength*Math.sin(Math.toRadians(headingToUse));
		particlePosk.y = particlePosk_1.y - stepLength*Math.cos(Math.toRadians(headingToUse));
		particlePosk.z = particlePosk_1.z;
		particlePosk.setFloor(particlePosk_1.getFloor());

		edgek = edgek_1; //no edge change, same edge
		distk = distk_1;
		anglek_to_apply = headingToUse;
	}

	/* this method applies weight depending on the distance from the particle to the edge */
	private double proximityWeight(double dist){
		double distMeter = dist/mPxMeter;
		double weight = 1 / (1 + distMeter);
		//Log.d(TAG, "proximityWeight: " + weight);
		return weight;
	}

	/* this method applies weight depending on the difference of angle between the particle and the edge */
	private double orientationWeight(double heading, Edge e){
		double angleDiff = e.lineAngle1 - heading;
		double angleWeight = Math.cos(Math.toRadians(angleDiff));
		if(angleWeight<0){
			angleDiff = e.lineAngle2 - heading;
			angleWeight = Math.cos(Math.toRadians(angleDiff));
			if(angleWeight<0) {
				angleWeight = 0;
			}
		}
		double weight = 1 + angleWeight;
		//Log.d(TAG, "orientationWeight: " + weight);
		return weight;
	}

	/*
	private double orientationWeight(double heading, Edge e){
		double weight;
		double angleDiff1 = e.lineAngle1 - heading;
		double weight1 = Math.cos(Math.toRadians(angleDiff1));
		double angleDiff2 = e.lineAngle2 - heading;
		double weight2 = Math.cos(Math.toRadians(angleDiff2));
		if(weight1 != -weight2) {
			Log.d(TAG, "orientationWeight: ( " + weight1 + " , " + weight2 + " )");
		}
		weight = Math.max(weight1, weight2);
		return weight;
	}
	*/

	/* this method applies weight comparing the movement of the particle and the movement calculated from PDR */
	private double movementWeight(double sensorsMovement, double particleMovement) {
		double like = evaluateGaussian(particleMovement, sensorsMovement, SIGMA_MOVEMENT);
		return like;
	}

	/* this method updates particles' weight when conditioned to paths */
	private double particleUpdateConstrained(Point3DWithFloor resultPos){
		double likelihood = Likeli(resultPos, particlePosk);
		return likelihood;
	}

	/* this method updates particles' weight when not conditioned to paths */
	private double particleUpdateNonConstrained(Point3DWithFloor resultPos, double particleHeading){
		double currentWeight;
		Edge currentEdge = edgek_1;
		ArrayList<EdgeWithWeight> candidateEdges = new ArrayList<EdgeWithWeight>();
		// get current edge weight
		Point3DWithFloor closestPoint = new Point3DWithFloor();
		double dist = mMapFusion.distanceBetweenPointAndEdge_andClosestPoint(particlePosk, currentEdge, closestPoint);
		double weightProximity = proximityWeight(dist);
		double weightOrientation = orientationWeight(particleHeading, currentEdge);
		double weight = calculateEdgeWeight(weightProximity, weightOrientation);
		//Log.d(TAG,"edgeId: " + currentEdge.edgeId + " , weights: (" + weightProximity + "," + weightOrientation + "," + likelihood + ") , total: " + weight);
		EdgeWithWeight candidateEdge = new EdgeWithWeight(currentEdge, weight, closestPoint);
		candidateEdges.add(candidateEdge);
		//Log.d(TAG,"particlePosFloor: " + particlePosk.getFloor());
		// get neighbor edges weight
		int particleFloor = (int) Math.round(particlePosk.getFloor());
		for (Edge e : mMapFusion.mEdgesByFloor.get(particleFloor)){
			if(e.edgeId != currentEdge.edgeId) {
				boolean considerEdge = false;
				double distToFinishEdge = 0;
				if (e.startNodeId == currentEdge.startNodeId || e.endNodeId == currentEdge.startNodeId) {
					considerEdge = true;
					distToFinishEdge = Util.distanceBetweenPoints(particlePosk_1,currentEdge.getStartNode());
				} else if (e.startNodeId == currentEdge.endNodeId || e.endNodeId == currentEdge.endNodeId) {
					considerEdge = true;
					distToFinishEdge = Util.distanceBetweenPoints(particlePosk_1,currentEdge.getEndNode());
				}
				if (considerEdge){
					closestPoint = new Point3DWithFloor();
					dist = mMapFusion.distanceBetweenPointAndEdge_andClosestPoint(particlePosk, e, closestPoint);
					weightProximity = proximityWeight(dist);
					weightOrientation = orientationWeight(particleHeading, e);
					weight = calculateEdgeWeight(weightProximity, weightOrientation);
					//Log.d(TAG,"edgeId: " + e.edgeId + " , weights: (" + weightProximity + "," + weightOrientation + "," + likelihood + ") , total: " + weight);
					candidateEdge = new EdgeWithWeight(e, weight, closestPoint);
					candidateEdges.add(candidateEdge);
					//Log.d(TAG, "edge length: " + e.getLength());
					if (isSmallEdge(e,distToFinishEdge)) {
						int nodeIdInCommon = e.startNodeId;
						if (e.startNodeId == currentEdge.startNodeId || e.startNodeId == currentEdge.endNodeId) {
							nodeIdInCommon = e.endNodeId;
						}
						// consider second level neighbor edges
						for (Edge e2 : mMapFusion.mEdgesByFloor.get(particleFloor)) {
							if ((e2.edgeId != e.edgeId) && (e2.startNodeId == nodeIdInCommon || e2.endNodeId == nodeIdInCommon)) {
								closestPoint = new Point3DWithFloor();
								dist = mMapFusion.distanceBetweenPointAndEdge_andClosestPoint(particlePosk, e2, closestPoint);
								weightProximity = proximityWeight(dist);
								weightOrientation = orientationWeight(particleHeading, e2);
								weight = calculateEdgeWeight(weightProximity, weightOrientation);
								//Log.d(TAG,"edgeId: " + e.edgeId + " , weights: (" + weightProximity + "," + weightOrientation + "," + likelihood + ") , total: " + weight);
								candidateEdge = new EdgeWithWeight(e2, weight, closestPoint);
								candidateEdges.add(candidateEdge);
								//Log.d(TAG, "small edge neighbor: (" + e2.edgeId + "," + weight + ")");
							}
						}

					}
				}
			}
		}
		// Sort candidateEdges by WEIGHT
		Collections.sort(candidateEdges, new SortByWeight());
		/*
		if(debug){
			Log.d(TAG,"Number of candidateEdges: " + candidateEdges.size());
			for(EdgeWithWeight e : candidateEdges){
				Log.i(TAG,"(" + e.getEdge().edgeId + " , " + e.getWeight() + " , " + e.getClosestPoint().toString() + ")");
			}
		}
		*/
		// select the edge with maximum weight
		edgek = candidateEdges.get(0).getEdge();
		Point3DWithFloor cPoint = candidateEdges.get(0).getClosestPoint();
		particlePosk_constrained = cPoint;
		distk = Math.sqrt(Math.pow(cPoint.x - edgek.startNode.x, 2) + Math.pow(cPoint.y - edgek.startNode.y, 2) + Math.pow(cPoint.z - edgek.startNode.z, 2));
		currentWeight = Likeli(resultPos, cPoint);
		if(useMovementWeight){
			double particleMovementPx = Util.distanceBetweenPoints(particlePosk_1,particlePosk_constrained);
			double particleMovement = particleMovementPx/mPxMeter;
			double movementWeight = movementWeight(mStepsDistance, particleMovement);
			currentWeight *= movementWeight;
		}
		return currentWeight;
	}

	/* this method assigns a weight to a candidate edge */
	private double calculateEdgeWeight(double weightProximity, double weightOrientation){
		//double weight = (10*weightProximity + weightOrientation);
		double weight = weightProximity*100*weightOrientation;
		//double weight = weightProximity;
		return weight;
	}

	/* this method performs the prediction of a particle depending on the configuration assigned */
	private void particlePrediction(){
		if(mUseParticleFilterSensors){
			if(usePathConstrains) {
				if (useOnlySteps) {
					particlePredictionPathConstrainedWithSensorsOnlySteps();
				} else{
					particlePredictionPathConstrainedWithSensors();
				}
			}
			else{
				particlePredictionNonPathConstrainedWithSensors();
			}
		}
		else{
			particlePredictionPathConstrainedWithoutSensors();
		}
	}

	/* this method performs the update of a particle depending on the configuration assigned */
	private double particleUpdate(Point3DWithFloor resultPos, double particleHeading){
		double currentWeight;
		if(mUseParticleFilterSensors){
			if(usePathConstrains) {
				currentWeight = particleUpdateConstrained(resultPos);
			}
			else{
				currentWeight = particleUpdateNonConstrained(resultPos, particleHeading);
			}
		}
		else{
			currentWeight = particleUpdateConstrained(resultPos);
		}
		return currentWeight;
	}
	
	/**
	 * This method generates the initial set of Np particles (state samples) and give them an equal weight
	 * @param resultPos		Initial estimated position ( by unfiltered fingerprint method )
	 */
	private void InitialSampling (Point3DWithFloor resultPos){
		//Log.d(TAG, "inside InitialSampling. orientationMEMS: " + orientationMEMS);
		Point3DWithFloorAndEdge mFingerprintingLocationMapFused = mMapFusion.doPathMatching(resultPos);

		//initial state conditions for the filter 
		edgek_1 = mFingerprintingLocationMapFused.getEdge();
		distk_1 = Math.sqrt(Math.pow(mFingerprintingLocationMapFused.x - edgek_1.startNode.x, 2) + Math.pow(mFingerprintingLocationMapFused.y - edgek_1.startNode.y, 2) + Math.pow(mFingerprintingLocationMapFused.z - edgek_1.startNode.z, 2));
		velk_1 = 0.0;
		anglek_1 = edgek_1.lineAngle1;

		userOrientation = (float) anglek_1;
		particlePosk_1.x = edgek_1.startNode.x + ((edgek_1.dX / edgek_1.length) * distk_1);
		particlePosk_1.y = edgek_1.startNode.y + ((edgek_1.dY / edgek_1.length) * distk_1);
		particlePosk_1.z = edgek_1.startNode.z + ((edgek_1.dZ / edgek_1.length) * distk_1);
		particlePosk_1.setFloor(edgek_1.startNode.getFloor() + ((edgek_1.dFloor / edgek_1.length) * distk_1));

		replicationFactor = new int [mNumOfParticle];
		particlestate_Xk = new ArrayList<ParticleStateConstrained>(mNumOfParticle);
		particlestate_Xk_1 = new ArrayList<ParticleStateConstrained>(mNumOfParticle);
		
		particlestate_Xk_toPlot = new ArrayList<ParticleState>(mNumOfParticle);

		accumulatedDistance = 0;

		isInInitPeriod = true;
		initAccumulatedDistance = 0;

		// Calculate Np particle states.	 
		for ( int i = 0; i < mNumOfParticle; i++ )	{
			generateRandomDist(mSigma0);

			double particleWeight = Wo;
			if(mUseParticleFilterSensors){
				/*
				anglek_1 = (360d/mNumOfParticle)*i;
				Log.d(TAG,"anglek_1: " + anglek_1);
				if(useMagneticFieldOrientationInInit) {
					double particleOrientationDiff = anglek_1-orientationMEMS;
					double cosParticleOrientationDiff = Math.cos(Math.toRadians(particleOrientationDiff));
					if (cosParticleOrientationDiff>0){
						particleWeight *= 2;
					}
					else{
						particleWeight *= 0.5;
					}
				}
				particlePrediction();
				*/
				particleWeight = InitialSamplingWithSensors(resultPos,1,particleWeight);
			}
			else {
				//particlePrediction();
				particlePredictionPathConstrainedWithoutSensors();
			}
			
			// Add Particle State and equal weight to all the particles
			ParticleStateConstrained psc = new ParticleStateConstrained(edgek,distk,velk,anglek,particleWeight);
			psc.stateAngleEdge = getAngleEdge(anglek,edgek);
			psc.correctionNeeded = true;
			particlestate_Xk_1.add(psc);

			// Init the current particle state list
			particlestate_Xk.add(new ParticleStateConstrained(null,0.0,0.0,0.0,0.0));
			//Log.d(TAG,"init. (" + i + " , " + edgek.edgeId + " , " + distk + " , " + anglek + " , " + particleWeight + ")");
		}
	}


//RP: Fix Mechanical stairs problem
	/**
	 * This method relocate the particles with small weight values to a point
	 * near the FP position. It is supposed to be called when the estimated
	 * position is in a transition edge and the FP position is moving along
	 * the Z axis. Conditions met when the user is going up of a mechanical
	 * stair or an elevator.
	 * @param resultPos: initial estimated position ( by unfiltered fingerprint
	 *                            method )
	 */
	private void edgeTransitionResampling(Point3DWithFloor resultPos){

		// Get finger print map position
		Point3DWithFloorAndEdge mFingerprintingLocationMapFused = mMapFusion
                .doPathMatching(resultPos);

		// Get edge and distance of FP position
		edgek_1 = mFingerprintingLocationMapFused.getEdge();
		distk_1 = Math.sqrt(Math.pow(mFingerprintingLocationMapFused.x - edgek_1.startNode.x, 2) + Math.pow(mFingerprintingLocationMapFused.y - edgek_1.startNode.y, 2) + Math.pow(mFingerprintingLocationMapFused.z - edgek_1.startNode.z, 2));
        velk_1 = 0.0;
        anglek_1 = edgek_1.lineAngle1;
        userOrientation = (float) anglek_1;
		// Set particles initial position from starting node
		particlePosk_1.x = edgek_1.startNode.x + ((edgek_1.dX /
				edgek_1.length) * distk_1);
		particlePosk_1.y = edgek_1.startNode.y + ((edgek_1.dY /
				edgek_1.length) * distk_1);
		particlePosk_1.z = edgek_1.startNode.z + ((edgek_1.dZ /
				edgek_1.length) * distk_1);

		particlePosk_1.setFloor(edgek_1.startNode.getFloor() + (
				(edgek_1.dFloor / edgek_1.length) * distk_1));


		// Change some (edgeTransitionResamplingParticles) of the particles only
		double particleWeight = Wo;
        edgeTransitionResamplingNumOfParticles = (int) (float)
        mNumOfParticle/4;
		for ( int i = 0; i < edgeTransitionResamplingNumOfParticles; i++ )	{

			// Pick a particle randomly and regenerate it around the FP
			// position
			int particle_to_pop = (int) Math.floor(Math.random() *
					(mNumOfParticle));


			double tmp_anglek = anglek;
			try{
				// LQ RP: angle from particle
				tmp_anglek = particlestate_Xk_1.get(particle_to_pop).stateAngle;

				// Remove it from the particles state lists
                particlestate_Xk_1.remove(particle_to_pop);
                particlestate_Xk.remove(particle_to_pop);


            }catch(Exception e){
                System.out.println(e.getMessage());
                e.printStackTrace();
                // Remove it from the particles state lists
                particlestate_Xk_1.remove(mNumOfParticle-1);
                particlestate_Xk.remove(mNumOfParticle-1);
            }


			// Propagate particle randomly without inertials but with
			// Gaussian noise in distance and angle
			particlePredictionPathConstrainedWithoutSensors();

			// RP: Review from Here!!
			// Add Particle State and equal weight to all the particles
			ParticleStateConstrained psc = new ParticleStateConstrained
					(edgek, distk, velk, tmp_anglek, particleWeight);
			psc.stateAngleEdge = getAngleEdge(tmp_anglek, edgek);
			psc.correctionNeeded = true;
			particlestate_Xk_1.add(psc);
			// Init the current particle state list
			// RP: prepare next epoch particles
			particlestate_Xk.add(psc);
		}
	}



	/* this method initializes particles' state variables when using PDR.
	 * If angleOption is 1, orientation obtained with magnetometer is used. If angleOption is not 1, magnetometer is not used
	 */
	private double InitialSamplingWithSensors(Point3DWithFloor resultPos, int angleOption, double particleWeight){
		particlePosk.x = resultPos.x + Util.gaussian(0,INIT_SIGMA_DIST_0)*mPxMeter;
		particlePosk.y = resultPos.y + Util.gaussian(0,INIT_SIGMA_DIST_0)*mPxMeter;
		particlePosk.z = resultPos.z;
		particlePosk.setFloor(resultPos.getFloor());
		Point3DWithFloorAndEdge mParticleMapFused = mMapFusion.doPathMatching(particlePosk);
		edgek = mParticleMapFused.getEdge();
		distk = Math.sqrt(Math.pow(mParticleMapFused.x - edgek.startNode.x, 2) + Math.pow(mParticleMapFused.y - edgek.startNode.y, 2) + Math.pow(mParticleMapFused.z - edgek.startNode.z, 2));
		anglek = Util.randomNumberBetween(0, 360);
		if (useMagneticFieldOrientationInInit && orientationMEMS!=-1) {
			if (angleOption == 1) {
				double particleOrientationDiff = anglek - orientationMEMS;
				double cosParticleOrientationDiff = Math.cos(Math.toRadians(particleOrientationDiff));
				if (cosParticleOrientationDiff > 0) {
					particleWeight *= 2;
				} else {
					particleWeight *= 0.5;
				}
			} else {
				anglek = orientationMEMS + Util.gaussian(0, INIT_SIGMA_ANGLE);
			}
		}
		return particleWeight;
	}

	/** 
	 * This method calculates the Sampling, Importance weight calculation, resampling steps of the Particle Filter
	 * @param resultPos		Initial estimated position ( by unfiltered fingerprint method )
	 */
	private boolean SIRF(Point3DWithFloor resultPos, boolean updateParticles){

		double Neff;	// effective sample size
		Wsum=0;
		int numParticlesCorrected = 0;
		ArrayList<Double> particleAngleEdges = new ArrayList<Double>();

		//check correction method
		checkCorrectionMethodParams(diffOrientationMEMS);

		// Sampling, Importance weight calculation, resampling
		for ( int i=0; i<mNumOfParticle; i++ )	{
			//retrieve values from last epoch to start the prediction step
			edgek_1 = particlestate_Xk_1.get(i).stateEdge;
			distk_1 = particlestate_Xk_1.get(i).stateDist;
			velk_1 = particlestate_Xk_1.get(i).stateVel;
			anglek_1 = particlestate_Xk_1.get(i).stateAngle;
			particlePosk_1.x = edgek_1.startNode.x + ((edgek_1.dX / edgek_1.length) * distk_1);
			particlePosk_1.y = edgek_1.startNode.y + ((edgek_1.dY / edgek_1.length) * distk_1);
			particlePosk_1.z = edgek_1.startNode.z + ((edgek_1.dZ / edgek_1.length) * distk_1);
			particlePosk_1.setFloor(edgek_1.startNode.getFloor() + ((edgek_1.dFloor / edgek_1.length) * distk_1));
			double lastParticleAngleEdge = particlestate_Xk_1.get(i).stateAngleEdge;
			boolean lastCorrectionNeeded = particlestate_Xk_1.get(i).correctionNeeded;
			/*
			Log.d(TAG, "--- Before particle prediction ---");
			Log.d(TAG, "edgek_1: " + edgek_1.toString());
			Log.d(TAG, "distk_1: " + distk_1);
			Log.d(TAG, "anglek_1: " + anglek_1);
			Log.d(TAG, "particlePosk_1: " + particlePosk_1.toString());
			*/
			int edgek_1Id = edgek_1.edgeId;
			double distk_previous = distk_1;

			particlePrediction();
			/*
			Log.d(TAG,"after particle prediction: (" + anglek_1 + "," + anglek + ")");
			Log.d(TAG, "--- After particle prediction ---");
			Log.d(TAG, "(" + edgek_1Id + " , " + edgek.edgeId + ") , (" + distk_previous + " , " + distk + ") , (" + particlePosk_1.toString() + " , " + particlePosk.toString() + ")");
			Log.d(TAG,"velk: (" + velk_1 + "," + velk + ")");
			Log.d(TAG, "--- Before particle update ---");
			*/
			if(anglek < 0){
				anglek += 360;
			}
			else if(anglek >= 360){
				anglek -= 360;
			}
			double particleHeading = anglek_to_apply;
			double currentWeight = particleUpdate(resultPos, particleHeading);
			//Log.d(TAG, "--- After particle update ---");
			double weight = particlestate_Xk_1.get(i).Weight;
			if(updateParticles) {
				weight *= currentWeight;
			}

			particlestate_Xk.get(i).stateEdge = edgek;
			particlestate_Xk.get(i).stateDist = distk;
			particlestate_Xk.get(i).stateVel = velk;
			particlestate_Xk.get(i).stateAngle = anglek;
			particlestate_Xk.get(i).Weight = weight;
			particlestate_Xk.get(i).setParticleFree(new Point3DWithFloor(particlePosk.x,particlePosk.y,particlePosk.z,particlePosk.getFloor()));
			particlestate_Xk.get(i).stateAngleEdge = lastParticleAngleEdge;
			particlestate_Xk.get(i).correctionNeeded = lastCorrectionNeeded;

			Wsum += weight;

			//only to plot the position of the particles:
			//particlestate_Xk_toPlot.add(new ParticleState(particlePosX,particlePosY,0.0,0.0,0.0));

			double particleAngleEdge = getAngleEdge(anglek, edgek);
			if(resetAngleEdgeParticles){
				particlestate_Xk.get(i).stateAngleEdge = particleAngleEdge;
				particlestate_Xk.get(i).correctionNeeded = true;
			}
			else {
				if(lastCorrectionNeeded) {
					double diffParticleAngleEdge = particleAngleEdge - lastParticleAngleEdge;
					if (Math.abs(diffParticleAngleEdge) > diffParticleAngleEdgeThreshold) {
						particlestate_Xk.get(i).correctionNeeded = false;
					}
				}
			}

			//apply correction if needed
			if(applyCorrectionMethod){
				boolean particleNeedsCorrection = particlestate_Xk.get(i).correctionNeeded;
				if(particleNeedsCorrection){
					double headingNoiseCorrection = Util.gaussian(0, HEADING_NOISE_CORRECTION);
					particlestate_Xk.get(i).stateAngle = particleAngleEdge + headingNoiseCorrection;
					numParticlesCorrected++;
					if(!particleAngleEdges.contains(particleAngleEdge)){
						particleAngleEdges.add(particleAngleEdge);
					}
				}
				//Log.d(TAG, "applying particle correction: (" + particleNeedsCorrection + "," + anglek + "," + particleAngleEdge + ")");
				//reset angle edge particle
				particlestate_Xk.get(i).stateAngleEdge = particleAngleEdge;
				particlestate_Xk.get(i).correctionNeeded = true;
			}
		}

		if(applyCorrectionMethod){
			accumulatedDistance = 0;
			int numAngleEdges = particleAngleEdges.size();
			String msgTxt = "Correction - numParticles: " + numParticlesCorrected + " - edges: " + numAngleEdges + "\n\n";
			for(int p=0; p<numAngleEdges; p++){
				msgTxt += particleAngleEdges.get(p);
				if(p!=numAngleEdges){
					msgTxt += "\n";
				}
			}
			if(debug) {
				Log.d(TAG,msgTxt);
				Message msg2 = Message.obtain();
				Bundle b2 = new Bundle();
				b2.putString("msg",msgTxt);
				msg2.setData(b2);
				handlerShowMsg.sendMessage(msg2);
			}
		}

		//Log.d(TAG,"edgek_1 angles: (" + edgek_1.getLineAngle1() + "," + edgek_1.getLineAngle2() + ")");

		// Normalize the array of weights and calculate effective sample size
		Neff = normWeightsAndCalculateNeff();
		if(Neff==-1){
			numResets++;
			if(debug)	Log.d(TAG,"++++ SIRF - Reset filter. Num Resets: " + numResets);
			initFilter();
			return false;
		}

		if(debug)	Log.d(TAG,"SIRF - Neff: " + Neff + " (Nt: " + Nt + ")");

		mTableEdges.clear();

		//criterion to trigger a resampling
		if (Neff < Nt) {
			if(debug) {
				Log.d(TAG, "++++ SIRF - Resample");
				String msgTxt = "resampling - epochs: " + epochsSinceResampling;
				Message msg = Message.obtain();
				Bundle b = new Bundle();
				b.putString("msg",msgTxt);
				msg.setData(b);
				handlerShowMsg.sendMessage(msg);
			}
			//Resampling with systematic resampling algorithm		
			rGeneration(); 				// Generation of the replication factors
			assignNewSamplesWeights();		// Assign the new state positions and weights
			//Validation for Resampling algorithm. Ksum should be equal to Np
			//Log.d(TAG,"Rsum:" +  calcRsum() + ", Np:" + mNumOfParticle);

			isResampled++;
			epochsSinceResampling = 0;
		}else{
			epochsSinceResampling++;
			// Assign the new state positions and weights ONLY based in the previous ones
			for (int nn = 0; nn < mNumOfParticle; nn++) {
				Edge edge = particlestate_Xk.get(nn).stateEdge;
				double weight = particlestate_Xk.get(nn).Weight;
				particlestate_Xk_1.get(nn).stateEdge	=	edge;
				particlestate_Xk_1.get(nn).stateDist	=	particlestate_Xk.get(nn).stateDist;
				particlestate_Xk_1.get(nn).stateVel		=	particlestate_Xk.get(nn).stateVel;
				particlestate_Xk_1.get(nn).stateAngle	=	particlestate_Xk.get(nn).stateAngle;
				particlestate_Xk_1.get(nn).Weight		=	weight;
				particlestate_Xk_1.get(nn).stateAngleEdge 	= particlestate_Xk.get(nn).stateAngleEdge;
				particlestate_Xk_1.get(nn).correctionNeeded = particlestate_Xk.get(nn).correctionNeeded;

				if(mTableEdges.containsKey(edge)){
					double oldWeight = mTableEdges.get(edge);
					mTableEdges.put(edge, oldWeight + weight);
				}
				else{
					mTableEdges.put(edge, weight);
				}
			}
		}

		//RP: Fix Mechanical stairs problem
		// If we are in a transition edge
		if(inMechanicalStairsSituation){

			inMechanicalStairsSituation = false;
			// Check Z delta from FP and last epoch position is "big"
			// if it is, trigger resampling of least weight particles to new
			// floor.
			double zDiff = Math.abs(outPFz - resultPos.z);
			// TODO: check if the difference is significative enough.
			if(zDiff > edgeTransitionResamplingThreashold){

			if(debug) {
				Log.d(TAG, "---- MechStairs - Resample");
				String msgTxt = "resampling of mechanical stairs";
				Message msg = Message.obtain();
				Bundle b = new Bundle();
				b.putString("msg",msgTxt);
				msg.setData(b);
				handlerShowMsg.sendMessage(msg);
			}
				//RP: mechanical stairs resampling commented
				edgeTransitionResampling(resultPos);
			}

		}

		// select the edge with highest cumulative weight
		if(mUseParticleFilterSensors && !usePathConstrains) {
			double maxWeight = 0;
			//Log.d(TAG, "--- TABLE EDGES:");
			for (Edge e : mTableEdges.keySet()) {
				double weight = mTableEdges.get(e);
				//Log.d(TAG, "edgeId: " + e.edgeId + " , weight: " + weight);
				if (weight > maxWeight) {
					maxWeight = weight;
					maxWeightedEdge = e;
				}
			}
			maxWeightedEdgeWeight = maxWeight;

			//RP: Fix Mechanical stairs problem
			// TODO: Settings on UI
			if(motionSteps.getMotionMode() == MotionMode.STATIC.ordinal() ||
				motionSteps.getMotionMode() == MotionMode.IRREGULAR_MOTION.ordinal()) {
				// See if the final edge is a transition one
				if (maxWeightedEdge.dFloor != 0) {
					inMechanicalStairsSituation = true;
				}
//				 Check if contiguous edges are transition edges.
				else{
					// TODO: check condition only if next to start/end i.e. node
					// dist similar to 0 or longitude of edge. Use outPF?
					int current_floor = (int) maxWeightedEdge.endNode
							.getFloor();
					for (Edge adjacent_edge : mMapFusion.mEdgesByFloor.get
							(current_floor)) {
						if (maxWeightedEdge.edgeId == adjacent_edge.edgeId){
							continue;
						}
						if (maxWeightedEdge.startNodeId == adjacent_edge
								.startNodeId || maxWeightedEdge.startNodeId
								== adjacent_edge.endNodeId || maxWeightedEdge
								.endNodeId == adjacent_edge.startNodeId ||
								maxWeightedEdge.endNodeId == adjacent_edge
										.endNodeId){
							if(adjacent_edge.dFloor != 0 ){
								inMechanicalStairsSituation = true;
							}
						}


					}
				}
			}

		}
		//Log.d(TAG, particlestate_Xk_1.size()+"");
		return true;
	}


	/** This method calculates the Filter output   */
	private void SIRFOutput(){

		// initialize array for the filter outputs
		out= new double [4];

		//Calculate the outputs of the filter 
		out[0]=0;
		out[1]=0;
		out[2]=0;
		out[3]=0;
		
		particlestate_Xk_toPlot.clear();

		double particlePosX, particlePosY, particlePosZ, particlePosFloor;

		if(mUseParticleFilterSensors && !usePathConstrains && maxWeightedEdge != null){
			int maxWeightedEdgeId = maxWeightedEdge.edgeId;
			double totalDist = 0;
			int numParticles = 0;
			double totalAngle = 0;
			double totalX = 0;
			double totalY = 0;
			double allParticlesX = 0;
			double allParticlesY = 0;
			double allParticlesWeightsSum = 0;
			for (int jj = 0; jj < mNumOfParticle; jj++) {
				Edge edge = particlestate_Xk.get(jj).getEdge();
				double dist = particlestate_Xk.get(jj).getDist();
				double weight = particlestate_Xk.get(jj).Weight;
				double particleAngle = particlestate_Xk.get(jj).getAngle();
                try{
                    particlePosX = edge.startNode.x + ((edge.dX / edge.length) * dist);
                    particlePosY = edge.startNode.y + ((edge.dY / edge.length) * dist);
                    particlePosZ = edge.startNode.z + ((edge.dZ / edge.length) * dist);
                    particlePosFloor = edge.startNode.getFloor() + ((edge.dFloor / edge.length) * dist);
                }catch(Exception e){
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                    particlePosX = edge.startNode.x + ((edge.dX / edge.length) * dist);
                    particlePosY = edge.startNode.y + ((edge.dY / edge.length) * dist);
                    particlePosZ = edge.startNode.z + ((edge.dZ / edge.length) * dist);
                    particlePosFloor = edge.startNode.getFloor() + ((edge.dFloor / edge.length) * dist);
                }

				boolean exceedsLength = false;
				if (dist > edge.length || dist < 0) {
					exceedsLength = true;
				}

				double particleAngleX = Math.sin(Math.toRadians(particleAngle))*weight;
				double particleAngleY = Math.cos(Math.toRadians(particleAngle))*weight;
				allParticlesX += particleAngleX;
				allParticlesY += particleAngleY;
				allParticlesWeightsSum += weight;

				if(maxWeightedEdgeId==edge.edgeId){
					totalDist += dist*weight;
					numParticles++;
					totalAngle += particleAngle*weight;
					totalX += particleAngleX;
					totalY += particleAngleY;
				}
				else{
					weight = 0;
				}

				boolean isMaxWeight = false;
				if(Double.compare(weight,maxWeight)==0){
					isMaxWeight = true;
					//Log.d(TAG,"maxWeight particle. " + weight);
				}
				//only to plot the position of the particles:
				ParticleState mParticleState = new ParticleState(particlePosX, particlePosY, particlePosZ, particlePosFloor, 0.0, 0.0, weight / maxWeight, exceedsLength);
				//Log.d(TAG,"particle free: " + particlestate_Xk.get(jj).getParticleFree());
				mParticleState.setParticleFree(particlestate_Xk.get(jj).getParticleFree());
				mParticleState.isMaxWeight = isMaxWeight;
				particlestate_Xk_toPlot.add(mParticleState);

			}
			//calculate meanDist of all the particles of the edge and calculate an output position
			//double meanDist = totalDist/numParticles;
			double meanDist = totalDist/maxWeightedEdgeWeight;
			out[0] = maxWeightedEdge.startNode.x + ((maxWeightedEdge.dX / maxWeightedEdge.length) * meanDist);
			out[1] = maxWeightedEdge.startNode.y + ((maxWeightedEdge.dY / maxWeightedEdge.length) * meanDist);
			out[2] = maxWeightedEdge.startNode.z + ((maxWeightedEdge.dZ / maxWeightedEdge.length) * meanDist);
			out[3] = maxWeightedEdge.startNode.getFloor() + ((maxWeightedEdge.dFloor / maxWeightedEdge.length) * meanDist);
			//calculate meanAngleError of particles belonging to maxWeightedEdge
			double meanAngle = (float) (totalAngle/maxWeightedEdgeWeight);

			double meanX = totalX/maxWeightedEdgeWeight;
			double meanY = totalY/maxWeightedEdgeWeight;
			//double alpha = Math.atan2(meanX,meanY);
			double alpha = Math.atan(meanX/meanY);
			double alphaDeg = Math.toDegrees(alpha);
			if(meanY<0){
				alphaDeg += 180;
			}
			else if(meanX<0 && meanY>0){
				alphaDeg += 360;
			}
			double radius = Math.sqrt(Math.pow(meanX,2) + Math.pow(meanY,2));

			double allMeanX = allParticlesX/allParticlesWeightsSum;
			double allMeanY = allParticlesY/allParticlesWeightsSum;
			double allRadius = Math.sqrt(Math.pow(allMeanX,2) + Math.pow(allMeanY,2));
			//Log.d(TAG,"meanAngle: " + meanAngle + " , alpha: " + alpha + " , alphaDeg: " + alphaDeg + " , meanX: " + meanX + " , meanY: " + meanY + " , radius: " + radius);
			//Log.d(TAG,"radius: " + radius + " , allRadius: " + allRadius);
			//userOrientation = (float) meanAngle;
			userOrientation = (float) alphaDeg;
			//userOrientationReliability = (float) radius;
			userOrientationReliability = (float) allRadius;
		}
		else {
			for (int jj = 0; jj < mNumOfParticle; jj++) {
				//first we calculate the 3D coordinates of the particle, from its edge and distance over the edge
				Edge edge = particlestate_Xk.get(jj).getEdge();
				double dist = particlestate_Xk.get(jj).getDist();
				double weight = particlestate_Xk.get(jj).Weight;
				particlePosX = edge.startNode.x + ((edge.dX / edge.length) * dist);
				particlePosY = edge.startNode.y + ((edge.dY / edge.length) * dist);
				particlePosZ = edge.startNode.z + ((edge.dZ / edge.length) * dist);
				particlePosFloor = edge.startNode.getFloor() + ((edge.dFloor / edge.length) * dist);
				boolean exceedsLength = false;
				if (dist > edge.length || dist < 0) {
					exceedsLength = true;
				}
				//only to plot the position of the particles:
				particlestate_Xk_toPlot.add(new ParticleState(particlePosX, particlePosY, particlePosZ, particlePosFloor, 0.0, 0.0, weight / maxWeight, exceedsLength));

				out[0] = out[0] + (particlePosX * weight);
				out[1] = out[1] + (particlePosY * weight);
				out[2] = out[2] + (particlePosZ * weight);
				out[3] = out[3] + (particlePosFloor * weight);
			}
		}

		// round the outputs of the filter
		outPFx=(int) Math.round(out[0]);
		outPFy=(int) Math.round(out[1]);
		outPFz=(int) Math.round(out[2]);
		outPFfloor=(int) Math.round(out[3]);

		// outputs of the filter without rounding
		outPFx_free=out[0];
		outPFy_free=out[1];
		outPFz_free=out[2];
		outPFfloor_free=Math.round(out[3]);
		
		if(debug){
			Log.d(TAG,"x_outPF_Grid: ("+ outPFx + "," + outPFy + "," + outPFz + "," + outPFfloor + ")");
			Log.d(TAG,"x_outPF_Free: ("+ outPFx_free + "," + outPFy_free + "," + outPFz_free + "," + outPFfloor_free + ")");
		}
	}
	
	/** This method enables the recursivity nature of the SIRF **/
	private Point3DWithFloor recursiveSIRF(Point3DWithFloor location, boolean updateParticles){
		//long ts1 = System.currentTimeMillis();

		if(debug)	Log.d(TAG,"recursiveSIRF with params - sigmaDist=" + sigmaDist + " sigma=" + mSigma + " Np=" + mNumOfParticle + " Nt=" + Nt);
		//Log.d(TAG,"recursiveSIRF with params - sigmaDist=" + sigmaDist + " sigma=" + mSigma + " Np=" + mNumOfParticle + " Nt=" + Nt);

		// Initial distribution
		if (toInitFilter == 0){
			InitialSampling(location);
			SIRF(location, updateParticles);	//Sampling
			toInitFilter = 1;
		}else{
			//assure the sequential execution of the offline phase algorithmSampling
			boolean isOK = SIRF(location, updateParticles);
			if (!isOK) {
				if (debug) Log.d(TAG, "recursiveSIRF - SIRF NOT OK");
				return new Point3DWithFloor((int) outPFx_free, (int) outPFy_free, location.z, location.getFloor());
			}
		}

		//calculate the output
		SIRFOutput();

		Point3DWithFloor outLocation = new Point3DWithFloor(outPFx_free, outPFy_free, outPFz_free, outPFfloor_free);
		//double userTrajectory = angleLine(userPosition, outLocation); //changed to accumulated angle from particles
		userPosition = outLocation;
		//long ts2 = System.currentTimeMillis();
		//long diffTs = ts2 - ts1;
		//Log.d(TAG, "inside recursiveSIRF. diffTs: " + diffTs);
		boolean applyFloorPeriod = checkFloor(location.getFloor(), outLocation.getFloor());
		if(applyFloorPeriod){
			//TODO: apply if next to transition floor
			initFilter(); // force reset
			if(debug) {
				String msgTxt = "apply floor period";
				Log.d(TAG, msgTxt);
				Message msg3 = Message.obtain();
				Bundle b3 = new Bundle();
				b3.putString("msg", msgTxt);
				msg3.setData(b3);
				handlerShowMsg.sendMessage(msg3);
			}
		}
		return outLocation;
	}

	/* this method is called from PositionEstimation. It has 3 different ways of function:
	*  If we want to use PDR info (mUseParticleFilterSensors=true), and new Particle Filter calculation will be performed when new PDR data is received, then it only updates Fingerprint position
	*  If we want to use PDR info (mUseParticleFilterSensors=true), and new Particle Filter calculation will be performed when new position is received, then it executes new Particle Filter calculation
	*  If we don't want to use PDR info (mUseParticleFilterSensors=false), then it executes new Particle Filter calculation
	* */
	public Point3DWithFloor execute(Point3DWithFloor location, double locationError, boolean walking){
		Point3DWithFloor outLocation;
		if(useVariableSigmaDist){
			sigmaDist = locationError;
		}
		if(mUseParticleFilterSensors){
			if(useMotionStepsListener){
				Point3DWithFloor FPposition = getFingerprintPosition();
				if(FPposition==null){
					// it's the first execution of particle filter. outPF is not initialized
					outPFx_free = location.x;
					outPFy_free = location.y;
					outPFz_free = location.z;
					outPFfloor_free = location.getFloor();
				}
				outLocation = new Point3DWithFloor(outPFx_free, outPFy_free, outPFz_free, outPFfloor_free);
				setFingerprintPosition(location);
			}
			else{
				int motionMode = motionSteps.getMotionMode();
				ArrayList<Double> stepsList = motionSteps.getStepsList();
				double orientation = motionSteps.getAbsoluteOrientation();
				double diffOrientation = motionSteps.getDiffOrientation();
				processNewStepsInfo(motionMode, stepsList, orientation, diffOrientation);
				outLocation = recursiveSIRF(location, true);
			}
		}
		else{
			if (Settings.useSensors) {
				if (walking) {
					mSigma = mSigmaWalking;
					sigmaDistToApply = sigmaDist / 2;
					lastStateWalking = true;
				} else {
					mSigma = mSigmaStatic;
					sigmaDistToApply = sigmaDist;
					if (lastStateWalking) {
						location = userPosition;
						toInitFilter = 0;
						lastStateWalking = false;
					}
				}
			}
			else {//this is the case useSensors=false
				mSigma = mSigmaWalking;
				sigmaDistToApply = sigmaDist/2;
				lastStateWalking = true;
			}
			outLocation = recursiveSIRF(location, true);
		}
		//Log.d(TAG, "inside execute. outLocation: " + outLocation.toString());
		return outLocation;
	}

	public void initFilter(){
		toInitFilter = 0;
	}

	public ArrayList<ParticleState> getParticles(){
		return particlestate_Xk_toPlot;
	}

	public synchronized int getNumSteps(){
		int tmpNumSteps = this.numSteps;
		this.numSteps = 0;
		return tmpNumSteps;
	}

	/* this method calculates the angle between 2 points of the map */
	public float angleLine(Point3D start, Point3D end) {
		float angle = (float) Math.atan2( (end.y - start.y), (end.x - start.x) );
		float angleDeg = ((float)Math.toDegrees(angle)) % 360;
		angleDeg += 90; // to make 0 degrees point to relative north of the map
		if(angleDeg<0){
			angleDeg += 360;
		}
		return angleDeg;
	}

	public float getUserOrientation(){
		return userOrientation;
	}

	public float getUserOrientationReliability() {
		return userOrientationReliability;
	}

	public double getKalmanHeading(){
		return lastEpochOrientation;
	}

	/*nested class for sorting EdgeWithWeight objects by WEIGHT IN DESC ORDER (signs of the 'one' changed with respect to ASCENDENT order)*/
	public class SortByWeight implements Comparator<EdgeWithWeight> {
		public int compare(EdgeWithWeight arg0, EdgeWithWeight arg1) {
			if ( arg0.getWeight() < arg1.getWeight()){
				return 1;
			} else if ( arg0.getWeight() == arg1.getWeight() ){
				return 0;
			} else if ( arg0.getWeight() > arg1.getWeight() ){
				return -1;
			}
			return 0;
		}
	}

	/* this method calculates which of the angles of an edge is more similar to orientation of the particle */
	private double getAngleEdge(double particleHeading, Edge e){
		double angleEdge = e.lineAngle1;
		double angleDiff = e.lineAngle1 - particleHeading;
		double weight = Math.cos(Math.toRadians(angleDiff));
		if(weight<0){
			angleEdge = e.lineAngle2;
		}
		//Log.d(TAG, "angleEdge: " + angleEdge);
		return angleEdge;
	}

	/* this method updates variables that indicate if a correction has to be performed to the orientation of the particles */
	private void checkCorrectionMethodParams(double diffOrientation){
		if(Math.abs(diffOrientation) < diffOrientationThreshold){
			//acumulate distance into variable
			accumulatedDistance += mStepsDistance;
			// check condition to apply correction
			if(accumulatedDistance > accumulatedDistanceThreshold){
				applyCorrectionMethod = true;
			}
			else{
				applyCorrectionMethod = false;
			}
			resetAngleEdgeParticles = false;
		}
		else{
			applyCorrectionMethod = false;
			accumulatedDistance = 0;
			resetAngleEdgeParticles = true;
		}
		//Log.d(TAG,"accumDist: " + accumulatedDistance + " , applyCM: " + applyCorrectionMethod + " , resetAEP: " + resetAngleEdgeParticles);
	}

	/* this method determines if an edge is considered as small or not */
	private boolean isSmallEdge(Edge e, double distToFinishEdge){
		double thresholdDist = 5; // meters (default=2.5)
		boolean isSmall = false;
		double totalDist = (e.getLength() + distToFinishEdge)/mPxMeter;
		if(totalDist < thresholdDist){
			isSmall = true;
		}
		return isSmall;
	}

	private Handler handlerShowMsg = new Handler() {

		public void handleMessage(Message msg) {
			// do stuff with UI
			Bundle b = msg.getData();
			String message = b.getString("msg");
			Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
		}
	};

	// check floor period parameters
	private boolean isInCheckFloorPeriod = false;
	private long floorPeriodTs = 0;
	private long floorPeriodThreshold = 20*1000; // consecutive time that FP position is in different floor than PF position (in milliseconds)

	private boolean checkFloor(double fingerprintingFloor, double particleFilterFloor){
		boolean applyFloorPeriod = false;
		long ts = System.currentTimeMillis();
		double floorDiff = Math.abs(fingerprintingFloor - particleFilterFloor);
		if(isInCheckFloorPeriod){
			if(floorDiff>=1){
				//check if floorPeriodTs is above threshold
				long tsDiff = ts - floorPeriodTs;
				if(tsDiff > floorPeriodThreshold){
					applyFloorPeriod = true;
				}
			}
			else{
				isInCheckFloorPeriod = false;
			}
		}
		else{
			// check if we have to enter to floorPeriod
			if(floorDiff>=1){
				floorPeriodTs = ts;
				isInCheckFloorPeriod = true;
			}
		}
		return applyFloorPeriod;
	}
}