package com.ascamm.motion.algorithm;

import com.ascamm.motion.utils.Point3DWithFloor;

public class PointDistance {
	
	private Point3DWithFloor mPoint;
	private double mDistance;
	
	public PointDistance(Point3DWithFloor point, double distance) {
		mPoint = point;
		mDistance = distance;
	}
	
	public Point3DWithFloor getPoint(){
		return mPoint;
	}
	
	public double getDistance(){
		return mDistance;
	}

}
