package com.ascamm.motion.algorithm;

import com.ascamm.motion.utils.Point3DWithFloor;

public class PointProbability implements Cloneable {
	
	private Point3DWithFloor mPoint;
	private double mProbability;
	private int mMatches;
	
	public PointProbability(Point3DWithFloor point, double probability, int matches) {
		mPoint = point;
		mProbability = probability;
		mMatches = matches;
	}
		
	public Point3DWithFloor getPoint(){
		return mPoint;
	}

	public double getProbability(){
		return mProbability;
	}
	
	public void setProbability(double prob){
		mProbability = prob;
	}
	
	public int getMatches() {
		return mMatches;
	}

	public void setMatches(int matches) {
		this.mMatches = matches;
	}

	public Object clone(){  
	    try{  
	        return super.clone();  
	    }catch(Exception e){ 
	        return null; 
	    }
	}

}
