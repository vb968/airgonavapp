package com.ascamm.motion.algorithm;

/**
 * This class implements a one-dimensional Kalman Filter to be applied over the measured RSSI 
 * @author  Marc Ciurana ASCAMM
 */ 

public class KalmanFilter {
	/* one-dimensional filter variables */
	
	//constants:
	private float r; //variance of the measurement noise
	private float q; //variance of the process noise
	//variables at each epoch
	private float x; //state scalar (filter output, i.e. filtered RSSI)
	private float p; //variance of the state scalar
	private float x_pred; //predicted state scalar 
	private float p_pred; //predicted variance of the state scalar
	private float k; //Kalman gain
	
	
	/* initial values */
	//private float x_0;
	//private float p_0;
	private float r_0 = 0.9f;
	private float q_0 = 0.05f;
	
	public KalmanFilter(){
		//x = x_0;
		//p = p_0;
		r = r_0;
		q = q_0;
	}
	
	public float[] calculateOutput(float last_epoch_rssi, float last_epoch_p, float measured_rssi){
		float[] kalman_output = {0,0}; 
		
		/* prediction step */
		x_pred = last_epoch_rssi;  //because the system "matrix" is the identity
		p_pred = last_epoch_p + q;
	
		/* update step */
		k = p_pred / (p_pred + r);
		x = x_pred + k * (measured_rssi - x_pred);
		p = (1 - k) * p_pred;
				
		kalman_output[0] = x;
		kalman_output[1] = p;
		return kalman_output;
	}
}
