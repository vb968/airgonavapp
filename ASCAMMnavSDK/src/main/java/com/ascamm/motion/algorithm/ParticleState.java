package com.ascamm.motion.algorithm;

import com.ascamm.motion.utils.Point3DWithFloor;
import com.ascamm.utils.Point3D;

import java.io.Serializable;

public class ParticleState implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** x coordinate of the particle position */
	double stateX;
	
	/** y coordinate of the particle position */
	double stateY;
	
	/** z coordinate of the particle position */
	double stateZ;

	/** floor of the particle */
	double stateFloor;
	
	/** x coordinate of the particle velocity */
    double stateVx;	
    
    /** y coordinate of the particle velocity */
    double stateVy;
    
    /** Weight associated to the particle state */
    double Weight;
    
    boolean exceedsLength;

	Point3DWithFloor particleFree;

	boolean isMaxWeight;
    
    /**
     * Constructor. Creates a particle state from the given parameters.
     * @param X		x coordinate of the particle position
     * @param Y		y coordinate of the particle position
     * @param Z		z coordinate of the particle position
	 * @param floor	floor of the particle
     * @param Vx	x coordinate of the particle velocity
     * @param Vy	y coordinate of the particle velocity
     * @param W		Weight associated to the particle state
     */
    public ParticleState(double X,double Y, double Z, double floor, double Vx, double Vy, double W, boolean exceedsLength) {
		this.stateX  = X;
		this.stateY  = Y;
		this.stateZ  = Z;
		this.stateFloor = floor;
		this.stateVx = Vx;
		this.stateVy = Vy;
		this.Weight = W;
		this.exceedsLength = exceedsLength;
	}
    
    public float getX(){
    	return (float) stateX;
    }
    public float getY(){
    	return (float) stateY;
    }
    public double getZ(){
    	return stateZ;
    }
    public int getVx(){
    	return (int) stateVx;
    }
    public int getVy(){
    	return (int) stateVy;
    }
    public boolean exceedsLength(){
    	return exceedsLength;
    }
    public double getWeight(){
    	return Weight;
    }

	public Point3DWithFloor getParticleFree() {
		return particleFree;
	}

	public void setParticleFree(Point3DWithFloor particleFree) {
		this.particleFree = particleFree;
	}

	public boolean isMaxWeight(){
		return isMaxWeight;
	}

	public double getFloor() {
		return stateFloor;
	}

	public void setFloor(double stateFloor) {
		this.stateFloor = stateFloor;
	}
}
