package com.ascamm.motion.ble;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import com.ascamm.motion.mems.MotionMEMS;
import com.ascamm.motion.utils.LogWriter;
import com.ascamm.motion.algorithm.KalmanFilter;
import com.ascamm.motion.utils.LogReader;
import com.ascamm.motion.utils.LogReader.FileType;
import com.ascamm.motion.utils.OnlineItem;
import com.ascamm.motion.utils.OnlineMeasure;
import com.ascamm.motion.utils.SimOnlineMeasure;

import com.ascamm.motion.utils.Constants;
import com.ascamm.motion.utils.Settings;

/**
 * This class provides the methods for executing BLE scans and obtain the RSS measures
 * @author  Daniel Fernandez [ASCAMM]
 */
public class BleService extends Service {

	private final String TAG = getClass().getSimpleName();

	private boolean debug = Constants.DEBUGMODE;
	//private boolean debug = true;

	private final IBinder mBinder = new BleBinder();

	private BluetoothAdapter mBluetoothAdapter;
	public static final long REPORT_MEASURES_TIME = 1000; // milliseconds

	private boolean useStartStopTimer = true;
	private static final long START_STOP_TIME = 1000; // milliseconds
	private static final long ANDROID_N_MIN_SCAN_CYCLE_MILLIS = 6000;
	private long mLastScanCycleStartTime = 0;

	private List<BleType> bleScansList = new CopyOnWriteArrayList<BleType>();

	private boolean bleOk = false;
	private boolean bluetoothTurningOn = false;
	private boolean waitingForBluetooth = false;
	private boolean mIsScanOn = false;
	private boolean mIsBound = false;

	private ScanCallback mLeScanCallbackNew;
	private BluetoothAdapter.LeScanCallback mLeScanCallback;
	private int SCAN_MODE = ScanSettings.SCAN_MODE_LOW_LATENCY;

	private int apiVersion;

	private boolean useIBeacons = true;
	private int UUIDlength = 16;
	private int majorLength = 2;
	private int minorLength = 2;
	private int rssiLength = 1;
	private String AscammUUID = "414952474F5F410090499E52B547F14F";
	private String OtherUUID =  "504F4C45535441520000000000000000";

	private LinkedList<OnlineMeasure> mQueueOfOnlineMeasures = new LinkedList<OnlineMeasure>(); // buffer where scansets are stored
	private final int MAX_SCAN_BUFFER_SIZE = 60;
	private boolean loadingFile = false;

	private KalmanFilter mKalmanFilter;
	private boolean mUseKalmanRSSI;
	private KalmanFilter mKalmanFilterAvg;
	private boolean mUseKalmanAvg = true;
	private boolean mUseKalmanAvgOnlyNonWalking = true;
	private ArrayList<OnlineItem> lastAvgOnlineItems;

	private boolean mUseWeightedRSSI;
	private boolean mUseHighRSSIvaluesOnly = false;
	private boolean mUseMaxRawRSSI = false;
	private boolean mUseAllRSSIvalues = true;
	private boolean mUseMaxOfRSSIvalues = false;
	private boolean mUseHighestRSSIvalues = false;

	private MotionMEMS mMotionMEMS;

	//LQ RP: store data from beacons
//	private boolean retrieveBeaconsData = true;
//	private String sniffedDataFile = "retrieved_beacons_data" + Constants.logsFilenameExt;
//	private LogWriter sniffedDataWriter;

	/** Code added for logs **/
	private LogWriter mLogWriter;
	private boolean logsRunning = false;
	private boolean storeLogs = Settings.storeLogs;
	//private boolean storeLogs = true;
	private String logsFilename = Settings.logsFilename + "_RSS_ble" + Constants.logsFilenameExt;

	/** Code added for Kalman logs **/
	private LogWriter mKalmanLogWriter;
	private boolean kalmanLogsRunning = false;
	//private boolean storeKalmanLogs = Settings.storeLogs;
	private boolean storeKalmanLogs = false;
	private String kalmanLogsFilename = Settings.logsFilename + "_RSS_ble_kalman" + Constants.logsFilenameExt;

	/** Code added for raw logs **/
	private LogWriter mRawLogWriter;
	private boolean rawLogsRunning = false;
	private boolean storeRawLogs = Settings.storeLogs;
	//private boolean storeRawLogs = true;
	private String rawLogsFilename = Settings.logsFilename + "_RSS_ble_raw" + Constants.logsFilenameExt;

	/************** Simulation variables ************/
	private boolean useSimulationBle = false;
	//private LinkedList<ArrayList<SimScanResult>> simScansQueue;
	private LinkedList<SimOnlineMeasure> simScansQueue;
	private int pos = 0, simQueueSize = 0;
	//private String fileToRead = "logs_RSS_CAR.txt";
	//private String fileToRead = "logs_RSS_wifi_ASCAMM_lab.txt";
	//private String fileToRead = "logs_RSS_ble.txt";
	//private String fileToRead = "logs_Mercat_Rubi_RSS_ble.txt";
	//private String fileToRead = "logs_Acuario_Medellin_transition_RSS_ble.txt";
	//private String fileToRead = "logs_Mercat_Rubi_route_7_5s_with_weights_0$0001_DB_kalman_RSS_ble.txt";
	//private String fileToRead = "logs_ASCAMM_Supermarket_route_1_RSS_ble_3s.txt";
	//private String fileToRead = "logs_Acuario_Medellin_transition_RSS_ble.txt";
	//private String fileToRead = "logs_ASCAMM_Supermarket_wifi_ble_route_9_RSS_ble.txt";
	//private String fileToRead = "logs_BCN_Languages_Joanic_route_1_RSS_ble.txt";
	//private String fileToRead = "logs_Casa_Batllo_route_3_RSS_ble.txt";
	//private String fileToRead = "logs_Casa_Batllo_route_8_m2_RSS_ble.txt";
	private String fileToRead = "logs_Casa_Batllo_static_6_m1_RSS_ble.txt";
	//private String fileToRead = "logs_Renault_Eurecat_static_1_m1_RSS_ble.txt";
	private long fromTs = 0;
	private long toTs = (long) Math.pow(10, 15);
	/************** End of simulation variables **************/

	private ArrayList<BleListener> mScanBleListeners = new ArrayList<BleListener>();

	public class BleBinder extends Binder {
		public BleService getService() {
			return BleService.this;
		}
	}

	private Timer mReportMeasuresTimer;
	private TimerTask mReportMeasuresTimerTask;

	private Timer mStartStopTimer;
	private TimerTask mStartStopTimerTask;

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	/************* Simulation *************/
	private void createSimulatedScans(){
		LogReader mLogReader = new LogReader(Constants.APP_DATA_FOLDER_NAME, fileToRead);
		mLogReader.readFile(FileType.RSS, fromTs, toTs);
		simScansQueue = mLogReader.getSimScansQueue();
		simQueueSize = simScansQueue.size();
	}
	/************* End of simulation **********/

	private final BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {
		//private boolean debug = true;
		public void onReceive (Context context, Intent intent) {
			String action = intent.getAction();

			if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
				int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
				if(state == BluetoothAdapter.STATE_OFF){
					// Bluetooth has turned off
					if(debug)       Log.d(TAG,"Bluetooth turned off");
				}
				else if(state == BluetoothAdapter.STATE_ON){
					// Bluetooth has turned on
					if(debug)       Log.d(TAG,"Bluetooth turned on. WaitingForBluetooth: " + waitingForBluetooth);
					bleOk = true;
					if(waitingForBluetooth){
						waitingForBluetooth = false;
						bluetoothTurningOn = false;
						startScan();
					}
				}
				else if(state == BluetoothAdapter.STATE_TURNING_OFF){
					if(debug)       Log.d(TAG,"Bluetooth turning off");
				}
				else if(state == BluetoothAdapter.STATE_TURNING_ON){
					if(debug)       Log.d(TAG,"Bluetooth turning on");
				}
			}

		}

	};

	@Override
	public void onCreate() {
		//Toast.makeText(this, "BleService Created", Toast.LENGTH_SHORT).show();
		registerReceiver(mBluetoothReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
		// Initializes Bluetooth adapter
		final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = bluetoothManager.getAdapter();

		// Ensures Bluetooth is available on the device and it is enabled. If not,
		// displays a Toast indicating Bluetooth is not enabled.
		if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
			bleOk = false;
			if(mBluetoothAdapter!=null){
				bluetoothTurningOn = mBluetoothAdapter.enable();
				if(debug)	Log.d(TAG, "bluetoothTurningOn: " + bluetoothTurningOn);
			}
			else{
				if(debug)	Toast.makeText(this, "BLE is not enabled", Toast.LENGTH_LONG).show();
			}
		}else{
			bleOk = true;
		}

		apiVersion = android.os.Build.VERSION.SDK_INT;
		if(debug)	Log.d(TAG,"apiVersion: " + apiVersion);

		if(apiVersion >= Build.VERSION_CODES.LOLLIPOP) {
				mLeScanCallbackNew = new ScanCallback() {
				@Override
				public void onScanResult(int callbackType, ScanResult result){
					super.onScanResult(callbackType, result);
					BluetoothDevice dev = result.getDevice();
					int rssi = result.getRssi();
					ScanRecord scanRecord = result.getScanRecord();
					byte[] scan = scanRecord.getBytes();
					//long tsNanos = result.getTimestampNanos();
					//Log.d(TAG,"inside onScanResult. callbackType: " + callbackType);
					processScan(dev,rssi,scan);
				}

				@Override
				public void onBatchScanResults (List<ScanResult> results){
					super.onBatchScanResults(results);
					int numResults = results.size();
					//Log.d(TAG,"inside onBatchScanResults. numResults: " + numResults);
				}

				@Override
				public void onScanFailed (int errorCode){
					super.onScanFailed(errorCode);
					Log.e(TAG,"inside onScanFailed. errorCode: " + errorCode);
				}
			};
		}
		else{
			mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
				@Override
				public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
					processScan(device,rssi,scanRecord);
				}
			};
		}

		// LQ RP:
//		if (retrieveBeaconsData) {
			//sniffedDataWriter = new LogWriter(Constants.APP_DATA_FOLDER_NAME, sniffedDataFile);
			//sniffedDataWriter.startLogs();
//		}


		/** Code added to store data info into logs.txt **/
		if(storeLogs){
			mLogWriter = new LogWriter(Constants.APP_DATA_FOLDER_NAME, logsFilename);
			if(mLogWriter.startLogsNoText()>0){
				logsRunning = true;
			}
		}

		/** Code added to store kalman data info into logs.txt **/
		if(storeKalmanLogs){
			mKalmanLogWriter = new LogWriter(Constants.APP_DATA_FOLDER_NAME, kalmanLogsFilename);
			if(mKalmanLogWriter.startLogs()>0){
				kalmanLogsRunning = true;
			}
		}

		/** Code added to store raw data info into logs.txt **/
		if(storeRawLogs){
			mRawLogWriter = new LogWriter(Constants.APP_DATA_FOLDER_NAME, rawLogsFilename);
			if(mRawLogWriter.startLogsNoText()>0){
				rawLogsRunning = true;
			}
		}

		if(useSimulationBle){
			/******** Simulation ************/
			new Thread() {
				public void run() {
					loadingFile = true;
					Log.i(TAG,"Loading File...");
					createSimulatedScans();
					loadingFile = false;
					Log.i(TAG,"File Loaded");
				}
			}.start();
			/********************************/
		}
	}

	@Override
	public void onDestroy() {
		//Toast.makeText(this, "BleService Stopped", Toast.LENGTH_SHORT).show();
		if(mBluetoothAdapter != null){
			stopScan();
		}
		/** Code added to store data info into logs.txt **/
		if(storeLogs && logsRunning){
			mLogWriter.stopLogs();
			logsRunning = false;
		}
		/** Code added to store kalman data info into logs.txt **/
		if(storeKalmanLogs && kalmanLogsRunning){
			mKalmanLogWriter.stopLogs();
			kalmanLogsRunning = false;
		}
		/** Code added to store raw data info into logs.txt **/
		if(storeRawLogs && rawLogsRunning){
			mRawLogWriter.stopLogs();
			rawLogsRunning = false;
		}
		unregisterReceiver(mBluetoothReceiver);
		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startid){
		//Toast.makeText(this, "BleService Started", Toast.LENGTH_SHORT).show();
		return START_STICKY;
	}

	public void setMotionMEMS(MotionMEMS motionMEMS){
		mMotionMEMS = motionMEMS;
	}

	/** start scanning if not started */
	public void startScan(){
		if(bluetoothTurningOn){
			waitingForBluetooth = true;
		}
		else{
			if(!mIsScanOn && bleOk) {
				executeStart();
				startReportMeasuresTimer();
				startStartStopTimer();
				mIsScanOn = true;
			}
		}
	}

	/** stop scanning */
	public void stopScan(){
		stopStartStopTimer();
		stopReportMeasuresTimer();
		mIsScanOn = false;
		executeStop();
		// SLEEP 1 SECOND AND TRY TO STOP SCAN AGAIN
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			public void run() {
				executeStop();
			}
		}, 1000);
	}

	private void executeStart(){
		if(apiVersion >= Build.VERSION_CODES.LOLLIPOP) {
			BluetoothLeScanner mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
			/*
			String firstByteStr = Constants.AscammUUID.substring(0,2);
			byte[] firstByte = firstByteStr.getBytes();
			Log.d(TAG,"firstByte: " + firstByte);
			int manufacturerId = (int) 0x004c;
			byte manufacturerData[] = {0x02,0x15,(byte)0x41,0x49};
			byte manufacturerDataMask[] = {(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF};
			ScanFilter beaconFilter = new ScanFilter.Builder()
					//.setDeviceAddress("F5:D6:07:D1:C6:90")
					.setManufacturerData(manufacturerId,manufacturerData,manufacturerDataMask)
					.build();
			filters = new ArrayList<ScanFilter>();
			filters.add(beaconFilter);
			*/
			List<ScanFilter> filters = null;
			ScanSettings settings = new ScanSettings.Builder()
					.setScanMode(SCAN_MODE)
					.build();
			int callbackType = settings.getCallbackType();
			int scanMode = settings.getScanMode();
			int scanResultType = settings.getScanResultType();
			long reportDelay = settings.getReportDelayMillis();
			if(debug)	Log.d(TAG, "callbackType: " + callbackType + " , scanMode: " + scanMode + " , scanResultType: " + scanResultType + " , reportDelay: " + reportDelay);
			mBluetoothLeScanner.startScan(filters, settings, mLeScanCallbackNew); // filters=null to not filter ADV packets
		}else {
			mBluetoothAdapter.startLeScan(mLeScanCallback);
		}
		mLastScanCycleStartTime = SystemClock.elapsedRealtime();
	}

	private void executeStop(){
		if(apiVersion >= Build.VERSION_CODES.LOLLIPOP) {
			BluetoothLeScanner mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
			mBluetoothLeScanner.stopScan(mLeScanCallbackNew);
		}else {
			mBluetoothAdapter.stopLeScan(mLeScanCallback);
		}
	}

	Handler mHandlerLocal = new Handler() {

		public void handleMessage(Message msg) {
			// generate New Scan event to IndoorPosClient
			Bundle b = msg.getData();
			boolean locRes = b.getBoolean("locRes");
			if(locRes){
				int numDevices = b.getInt("num_devices");
				fireNewScanAvailableEvent(numDevices);
			}
		}
	};

	private class ReportMeasuresTimerTask extends TimerTask {

		/** get index of a bssid in a scanSet **/
		private int getIndex(ArrayList<BleType> bleItemSet, String bssid){
			for (int i=0; i<bleItemSet.size(); i++){
				if (bleItemSet.get(i).getMac().equalsIgnoreCase(bssid)){
					return i;
				}
			}
			return -1;
		}

		@Override
		public void run() {
			if(!loadingFile){
				long ts = System.currentTimeMillis();
				boolean walking = false;
				if(mMotionMEMS!=null){
					walking = mMotionMEMS.getWalking();
				}
				OnlineMeasure newOnlineMeasure = new OnlineMeasure(ts, walking);
				int numDevices = 0;

				if(!useSimulationBle){
					/******************* Real measurements **********************/
					//Copy results into bleList
					ArrayList<BleType> bleList = new ArrayList<BleType>();
					//Log.d("Shared List", "blescans pre-iterator: "+ bleScansList.size());
					Iterator<BleType> iter = bleScansList.iterator();
					//Log.d("Shared List", "blescans post-iterator: "+ bleScansList.size());
					// LQ RP: number of measures to process
					int num_processed = 0;
					while(iter.hasNext()){
						num_processed++;
						BleType bleScan = iter.next();
						int j = getIndex(bleList, bleScan.getMac());
						if (j < 0){
							// if this bssid never seen before create a new entry in bleList
							BleType bleType = new BleType(bleScan.getMac(), bleScan.getRssi());
							if(mUseAllRSSIvalues) {
								bleType.addRssiValue(bleScan.getRssi());
							}
							bleList.add(bleType);
						} else {
							float newRSSvalue = bleScan.getRssi();
							float b_rss = bleList.get(j).getRssi();
							int b_count = bleList.get(j).getCount();
							if(mUseMaxRawRSSI){
								if(newRSSvalue>b_rss){
									bleList.get(j).setRssi(newRSSvalue);
								}
							}
							else {
								// calculate new avg rss level and update count
								bleList.get(j).setRssi((b_rss * b_count + newRSSvalue) / (b_count + 1));
								bleList.get(j).setCount(b_count + 1);
								if(mUseAllRSSIvalues){
									bleList.get(j).addRssiValue(newRSSvalue);
								}
							}
						}
					}
					// LQ RP: Testing timer sleep
					/*try {
						Thread.sleep(5000);
					}
					catch (InterruptedException ex) {
						android.util.Log.d("Ble service sleep", ex.toString());

					}*/
					//Clear scans' list
					//Log.d("Shared List", "blescans pre-clear: "+ bleScansList.size());
					// LQ RP: only clear the processed measures
					// LQ: this crashes with some time of inactivity
					bleScansList.subList(0, num_processed).clear();
					//Log.d("Shared List", "blescans post-clear: "+ bleScansList.size());

					if(debug){
						for(BleType bleScan : bleList){
							Log.i(TAG,"(" + bleScan.getMac() + "," + bleScan.getRssi() + "," + bleScan.getCount() + ")");
						}
					}

					newOnlineMeasure.setOnlineItemsBle(bleList);
					numDevices = bleList.size();
					/******************* End of real measurements ****************/
				}
				else{
					/*********************** Simulation *************************/
					//Clear scans' list
					bleScansList.clear();
					if(pos >= simQueueSize){
						pos = 0;
					}
					/*
					Log.i(TAG,"pos: " + pos);
					ArrayList<SimScanResult> newSimScanResults = simScansQueue.get(pos);
					newOnlineMeasure.setOnlineItemsSim(newSimScanResults);
					*/
					SimOnlineMeasure simScans = simScansQueue.get(pos);
					Log.i(TAG,"pos: " + pos + " , ts: " + simScans.getTs());
					ts = simScans.getTs();
					ArrayList<OnlineItem> onlineItemsBle = simScans.getOnlineItems();
					newOnlineMeasure.setOnlineItems(onlineItemsBle);
					pos++;
					numDevices = onlineItemsBle.size();
					/*********************** End of simulation *******************/
				}

				// Code added to store data info into logs.txt
				if(storeLogs && logsRunning){
					mLogWriter.addArrayRowOnlineMeasuresLog(ts, newOnlineMeasure.getOnlineItems());
				}

				/******** Kalman filter for the measured RSSI from each BLE beacon ****/
				if(mUseKalmanRSSI){
					try{
						int size = mQueueOfOnlineMeasures.size();
						OnlineMeasure lastOnLineMeasure = mQueueOfOnlineMeasures.getLast();
						ArrayList<OnlineItem> kalmanOnlineItems = new ArrayList<OnlineItem>();
						for(OnlineItem oli : newOnlineMeasure.getOnlineItems()){

							//Log.d(TAG, "--- KALMAN log for AP: " + oli.getBSSID() + "  measured RSSI: " + oli.getRSS());
							OnlineItem koi = new OnlineItem(null, oli.getBSSID(), oli.getRSS(), oli.getRSS(), 0);
							int j = BleService.getIndex(lastOnLineMeasure.getOnlineItems(), oli.getBSSID());
							if(j>=0){
                                                                /*
                                                                 * we got a measurement from this BLE beacon in the last epoch,
                                                                 * therefore we can run Kalman filter to improve the current
                                                                 * measurement based on the last epoch measurement
                                                                 */
								float last_epoch_rssi = lastOnLineMeasure.getOnlineItems().get(j).getRSS();
								float measured_rssi = oli.getRSS();
								float p = lastOnLineMeasure.getOnlineItems().get(j).getP_kalman();
								float[] filter_output = mKalmanFilter.calculateOutput(last_epoch_rssi, p, measured_rssi);
								oli.setRSS(filter_output[0]);
								oli.setP_kalman(filter_output[1]);

								//Log.d(TAG, "--- KALMAN log for AP: " + oli.getBSSID() + " last epoch: " + last_epoch_rssi + " filtered: " + filter_output[0]);
								koi.setRSSstdev(filter_output[0]);
							}
							else{
								//it was not found in the last epoch, then we try with the BEFORE last epoch:

								if(size > 1){
									OnlineMeasure beforelastOnLineMeasure = mQueueOfOnlineMeasures.get(size-2);

									j = BleService.getIndex(beforelastOnLineMeasure.getOnlineItems(), oli.getBSSID());
									if(j>=0){
                                                                                /*
                                                                                 * we got a measurement from this BLE beacon in the before last epoch,
                                                                                 * therefore we can run Kalman filter to improve the current
                                                                                 * measurement based on the before last epoch measurement
                                                                                 */
										float last_epoch_rssi = beforelastOnLineMeasure.getOnlineItems().get(j).getRSS();
										float measured_rssi = oli.getRSS();
										float p = beforelastOnLineMeasure.getOnlineItems().get(j).getP_kalman();
										float[] filter_output = mKalmanFilter.calculateOutput(last_epoch_rssi, p, measured_rssi);
										oli.setRSS(filter_output[0]);
										oli.setP_kalman(filter_output[1]);

										//Log.d(TAG, "uuu KALMAN log for AP: " + oli.getBSSID() + " before last epoch: " + last_epoch_rssi + " filtered: " + filter_output[0]);
										koi.setRSSstdev(filter_output[0]);
									}
									else{
										//it was not found in the before last epoch, then we try with the BEFORE BEFORE last epoch:

										if(size > 2){
											OnlineMeasure befbeflastOnLineMeasure = mQueueOfOnlineMeasures.get(size-3);

											j = BleService.getIndex(befbeflastOnLineMeasure.getOnlineItems(), oli.getBSSID());
											if(j>=0){
                                                                                                /*
                                                                                                 * we got a measurement from this BLE beacon in the before before last epoch,
                                                                                                 * therefore we can run Kalman filter to improve the current
                                                                                                 * measurement based on the before before last epoch measurement
                                                                                                 */
												float last_epoch_rssi = befbeflastOnLineMeasure.getOnlineItems().get(j).getRSS();
												float measured_rssi = oli.getRSS();
												float p = befbeflastOnLineMeasure.getOnlineItems().get(j).getP_kalman();
												float[] filter_output = mKalmanFilter.calculateOutput(last_epoch_rssi, p, measured_rssi);
												oli.setRSS(filter_output[0]);
												oli.setP_kalman(filter_output[1]);

												//Log.d(TAG, "ooo KALMAN log for AP: " + oli.getBSSID() + " befbef last epoch: " + last_epoch_rssi + " filtered: " + filter_output[0]);
												koi.setRSSstdev(filter_output[0]);
											}

										}
									}
								}

							}
							kalmanOnlineItems.add(koi);
						}
						// Code added to store data info into logs.txt
						if(storeKalmanLogs && kalmanLogsRunning){
							mKalmanLogWriter.addArrayRowKalmanLog(ts, kalmanOnlineItems);
						}
					}catch (NoSuchElementException e) {
						//mQueueOfOnlineMeasures is empty: it is the first scan and therefore we cannot apply Kalman yet
					}
				}

				/*****************************************************************/


				// add the result to the end of the queue, remove head if it is full
				if(mQueueOfOnlineMeasures.size() < MAX_SCAN_BUFFER_SIZE){
					mQueueOfOnlineMeasures.add(newOnlineMeasure);
				} else {
					while (mQueueOfOnlineMeasures.size() >= MAX_SCAN_BUFFER_SIZE){
						mQueueOfOnlineMeasures.poll();
					}
					mQueueOfOnlineMeasures.add(newOnlineMeasure);
				}

				if(debug)       Log.d(TAG, "Buffer size: " + mQueueOfOnlineMeasures.size());

				// Execute handler to fire newScanAvailable event
				Message msg = Message.obtain();
				Bundle b = new Bundle();
				b.putBoolean("locRes", true);
				b.putInt("num_devices", numDevices);
				msg.setData(b);
				mHandlerLocal.sendMessage(msg);
			}
		}
	}

	private class StartStopTimerTask extends TimerTask {

		@Override
		public void run() {
			if(debug) Log.d(TAG,"Before startStopLeScan "+System.currentTimeMillis());
			executeStop();
			executeStart();
		}
	}

	/** this method decodes BLE packets and obtains iBeacon info (if packet has iBeacon format) */
	private IBeaconInfo getIBeaconInfo(byte[] scan, boolean onlyASCAMMbeacons){
		boolean isIBeacon = false;
		String UUIDstring = null;
		String majorString = null;
		String minorString = null;
		String rssiString = null;
		int length = scan.length;
		for(int i=0;i<length;i++){
			if(scan[i] == (byte)0xFF){
				//Log.d(TAG,"Manufacturer Specific Data found");
				if((scan[i+1] == (byte)0x4c) && (scan[i+2] == (byte)0x00) && (scan[i+3] == (byte)0x02) && (scan[i+4] == (byte)0x15)){
					// obtain info of the iBeacon
					int startUUID = i+5;
					byte byteUUID[] = new byte[UUIDlength];
					for(int j=0;j<UUIDlength;j++){
						byteUUID[j] = scan[startUUID + j];
					}
					//UUIDstring = new String(byteUUID);
					UUIDstring = byteArrayToHexString(byteUUID);
					//Log.d(TAG,"UUID: " + UUIDstring);

					if(onlyASCAMMbeacons){
						if(UUIDstring.equalsIgnoreCase(AscammUUID)){
							// obtain info of the ASCAMM beacon
							isIBeacon = true;
						}
					}
					else{
						//Log.d(TAG,"iBeacon found");
						isIBeacon = true;
					}

					// LQ RP: other non-ascamm beacons
//					if(UUIDstring.equalsIgnoreCase(OtherUUID)){
//						isIBeacon = true;
//					}

					if(isIBeacon){
						int startMajor = startUUID + UUIDlength;
						byte byteMajor[] = new byte[majorLength];
						for(int j=0;j<majorLength;j++){
							byteMajor[j] = scan[startMajor + j];
						}
						majorString = byteArrayToHexString(byteMajor);
						//Log.d(TAG,"major: " + majorString);

						int startMinor = startMajor + majorLength;
						byte byteMinor[] = new byte[minorLength];
						for(int j=0;j<minorLength;j++){
							byteMinor[j] = scan[startMinor + j];
						}
						minorString = byteArrayToHexString(byteMinor);
						//Log.d(TAG,"minor: " + minorString);

						int startRSSI = startMinor + minorLength;
						byte byteRSSI[] = new byte[rssiLength];
						for(int j=0;j<rssiLength;j++){
							byteRSSI[j] = scan[startRSSI + j];
						}
						rssiString = byteArrayToHexString(byteRSSI);
						//Log.d(TAG,"RSSI: " + rssiString);
					}
				}
				break;
			}
		}

		return new IBeaconInfo(isIBeacon,UUIDstring,majorString,minorString,rssiString);
	}

	private static String byteArrayToHexString(byte[] b) {
		String result = "";
		for (int i = 0; i < b.length; i++) {
			result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
		}
		return result;
	}

	/** this method processes new BLE packet received */
	private void processScan(final BluetoothDevice device, int rssi, byte[] scanRecord){
		long ts = System.currentTimeMillis();
		if(useIBeacons){
			IBeaconInfo mIBeaconInfo = getIBeaconInfo(scanRecord, true);
			if(mIBeaconInfo.getIsIBeacon()){
				if(debug)
					Log.d(TAG,"iBeacon: (" + mIBeaconInfo.getUUID() + " , " + mIBeaconInfo.getMajor() + " , " + mIBeaconInfo.getMinor() + ") , RSSI: " + rssi);

				// LQ RP:
//				String beaconType = "unknown";
//				if(mIBeaconInfo.getUUID().equalsIgnoreCase(OtherUUID))
//					beaconType = "other";
//				else if(mIBeaconInfo.getUUID().equalsIgnoreCase(AscammUUID) && mIBeaconInfo.getMajor().equalsIgnoreCase("0024") && mIBeaconInfo.getMinor().equalsIgnoreCase("0012"))
//					beaconType= "airgo";
//				if (!beaconType.equalsIgnoreCase("unknown"))
//					Log.v("BLEdata","iBeacon: (" + System.currentTimeMillis() + ", " + rssi + ", " + beaconType + ")");

				String id = mIBeaconInfo.getMajor().toLowerCase() + ":" + mIBeaconInfo.getMinor().toLowerCase();
				bleScansList.add(new BleType(id, rssi));
				// Code added to store raw data info into logs.txt
				if(storeRawLogs && rawLogsRunning){
					mRawLogWriter.addRawOnlineMeasureLog(ts,id,rssi);
				}
			}
		}
		else{
			if(debug)
					Log.d(TAG,"device: " + device + " , rssi: " + rssi + " , ms: " + ts);
			bleScansList.add(new BleType(device.toString().toLowerCase(), rssi));
			// Code added to store raw data info into logs.txt
			if(storeRawLogs && rawLogsRunning){
				mRawLogWriter.addRawOnlineMeasureLog(ts,device.toString().toLowerCase(),rssi);
			}
		}
	}

	/** Get an average RSS set of scan results **/
	public ArrayList<OnlineItem> calcAvgScanSet(int mSecondsOfScansToUse, boolean walking){
		ArrayList<OnlineItem> avgOnlineItems = new ArrayList<OnlineItem>();
		long currentTs = System.currentTimeMillis();
		int numScans = 0;
		long minMilliSeconds = mSecondsOfScansToUse*1000;
		long maxMilliSeconds = 30*1000;

		float c_rss = 0.0f;
		int c_count = 0;
		float weight = 0.0f;
		float min_rss;
		float max_rss;

		//Make a copy of the buffer in order to work with the copy instead of the main buffer
		OnlineMeasure[] onlineList = (OnlineMeasure[]) mQueueOfOnlineMeasures.toArray(new OnlineMeasure[mQueueOfOnlineMeasures.size()]);
		int onlineListLength = onlineList.length;

		boolean walkingDetected = false;

		// Loop through all last number of scans
		for (int i=onlineListLength; i>0; i--){
			OnlineMeasure onlineMeasure = onlineList[i-1];
			long tsDiff = currentTs - onlineMeasure.getTs();
			boolean measureWalking = onlineMeasure.isWalking();
			//Log.d(TAG,"measureWalking: " + measureWalking + " , tsDiff: " + tsDiff + " ms");
			if(tsDiff < minMilliSeconds){
				if(measureWalking){
					walkingDetected = true;
				}
			}
			else{
				if(walking) {
					break;
				}
				else{
					if(walkingDetected) {
						break;
					}
					else{
						if (tsDiff < maxMilliSeconds) {
							// stop when walking measures detected
							if (measureWalking) {
								break;
							}
						} else {
							// stop when max time reached
							break;
						}
					}
				}
			}

			// process measurements
			if(mUseWeightedRSSI){
				weight = 1.0f / (currentTs - onlineMeasure.getTs());
			}

			numScans++;
			for ( OnlineItem oi : onlineMeasure.getOnlineItems() ){
				int j = getIndex(avgOnlineItems, oi.getBSSID());
				if (j < 0){
					// if this bssid never seen before create a new entry in scan set
					OnlineItem newOI = new OnlineItem(oi.getSSID(), oi.getBSSID(), oi.getRSS(), 1);
					newOI.setTech("BLE");
					if(mUseWeightedRSSI){
						newOI.addRSSweight(weight);
						newOI.setSumWeights(weight);
					}
					if(mUseHighRSSIvaluesOnly){
						// set first value as min and max value
						newOI.setMinRss(oi.getRSS());
						newOI.setMaxRss(oi.getRSS());
					}
					if(mUseAllRSSIvalues){
						//int count = 0;
						float minRss = 200;
						float maxRss = -200;
						for(float rss : oi.getRSSvalues()){
							newOI.addRSSvalue(rss);
							if(rss<minRss){
								minRss = rss;
							}
							if(rss>maxRss){
								maxRss = rss;
							}
							//count++;
						}
						newOI.setMinRss(minRss);
						newOI.setMaxRss(maxRss);
						//newOI.setCount(count);
					}
					else{
						newOI.addRSSvalue(oi.getRSS());
					}
					avgOnlineItems.add(newOI);
				} else {
					c_rss = avgOnlineItems.get(j).getRSS();
					c_count = avgOnlineItems.get(j).getCount();
					min_rss = avgOnlineItems.get(j).getMinRss();
					max_rss = avgOnlineItems.get(j).getMaxRss();
					if(mUseWeightedRSSI){
						// calculate new weighted avg rss level, and update sum_weights
						avgOnlineItems.get(j).setRSS(((c_rss * avgOnlineItems.get(j).getSumWeights()) + weight*oi.getRSS()) / (avgOnlineItems.get(j).getSumWeights() + weight));
						avgOnlineItems.get(j).setSumWeights(avgOnlineItems.get(j).getSumWeights() + weight);
						avgOnlineItems.get(j).addRSSweight(weight);
					}else{
						// calculate new avg rss level
						avgOnlineItems.get(j).setRSS((c_rss * c_count + oi.getRSS()) / (c_count + 1));
					}
					if(mUseHighRSSIvaluesOnly){
						float rssMeasured = oi.getRSS();
						// check min value
						if(rssMeasured < min_rss){
							avgOnlineItems.get(j).setMinRss(rssMeasured);
						}
						// check max value
						if(rssMeasured > max_rss){
							avgOnlineItems.get(j).setMaxRss(rssMeasured);
						}
					}
					if(mUseAllRSSIvalues){
						for(float rss : oi.getRSSvalues()){
							avgOnlineItems.get(j).addRSSvalue(rss);
							if(rss<min_rss){
								min_rss = rss;
							}
							if(rss>max_rss){
								max_rss = rss;
							}
							//c_count++;
						}
						avgOnlineItems.get(j).setMinRss(min_rss);
						avgOnlineItems.get(j).setMaxRss(max_rss);
						//avgOnlineItems.get(j).setCount(c_count);
						avgOnlineItems.get(j).setCount(c_count + 1);
					}
					else {
						//update count
						avgOnlineItems.get(j).setCount(c_count + 1);
						avgOnlineItems.get(j).addRSSvalue(oi.getRSS());
					}
				}
			}
		}

		if(mUseHighRSSIvaluesOnly){
			for(OnlineItem oi : avgOnlineItems){
				float oldMean = oi.getRSS();
				float count = oi.getCount();
				float minRSS = oi.getMinRss();
				float maxRSS = oi.getMaxRss();
				//Log.d(TAG,"AP: " + oi.getBSSID() + " , oldMean: " + oldMean + " , oldCount: " + count + " , minRSS: " + minRSS + " , maxRSS: " + maxRSS);
				//filter measures using rssThreshold
				float rssThreshold = (minRSS + maxRSS)/2;
				float rssSum = 0;
				int rssCount = 0;
				ArrayList<Float> filteredRssValues = new ArrayList<Float>();
				for( float rss : oi.getRSSvalues() ){
					if(rss>=rssThreshold){
						filteredRssValues.add(rss);
						rssSum += rss;
						rssCount++;
					}
				}
				//calculate new mean
				float newMean = rssSum/rssCount;
				//Log.d(TAG,"AP: " + oi.getBSSID() + " , newMean: " + newMean + " , newCount: " + rssCount + " , rssThreshold: " + rssThreshold);
				oi.setRSS(newMean);
				oi.setRssArray(filteredRssValues);
				oi.setCount(rssCount);
			}
		}

		if(mUseWeightedRSSI){
			// Calculate the weighted stdev and the count percentage of each scan
			for(OnlineItem oi : avgOnlineItems){
				float weighted_mean = oi.getRSS();
				float count = oi.getCount();
				float sumweights = oi.getSumWeights();
				double weighted_stdev = 0;
				float sum = 0;
				float next_weight = 0;

				Iterator<Float> rss_value = oi.getRSSvalues().iterator();
				Iterator<Float> rss_weight = oi.getRSSWeights().iterator();
				while(rss_value.hasNext() && rss_weight.hasNext()) {
					next_weight = rss_weight.next();
					sum += next_weight * Math.pow(rss_value.next() - weighted_mean,2);
				}
				float den = ((count-1)/count) * sumweights;

				weighted_stdev = Math.sqrt(sum/den);
				oi.setRSSstdev(weighted_stdev);
				oi.setCountPercentage((count/numScans)*100);
			}
		}
		else if(mUseMaxOfRSSIvalues){
			for(OnlineItem oi : avgOnlineItems){
				//get max RSS value
				float maxRss = -200;
				for( float rss : oi.getRSSvalues() ){
					if(rss>maxRss){
						maxRss = rss;
					}
				}
				//set max RSS value as representative value
				oi.setRSS(maxRss);
			}
		}
		else if(mUseHighestRSSIvalues){
			for(OnlineItem oi : avgOnlineItems){
				float oldMean = oi.getRSS();
				float count = oi.getCount();
				float minRSS = oi.getMinRss();
				float maxRSS = oi.getMaxRss();
				//Log.d(TAG,"AP: " + oi.getBSSID() + " , oldMean: " + oldMean + " , oldCount: " + count + " , minRSS: " + minRSS + " , maxRSS: " + maxRSS);
				//filter measures using rssThreshold
				float rssThreshold = (minRSS + maxRSS)/2;
				//float rssThreshold = minRSS + (maxRSS-minRSS)*3/4;
				//float rssThreshold = maxRSS - 12;
				float rssSum = 0;
				int rssCount = 0;
				ArrayList<Float> filteredRssValues = new ArrayList<Float>();
				for( float rss : oi.getRSSvalues() ){
					if(rss>=rssThreshold){
						filteredRssValues.add(rss);
						rssSum += rss;
						rssCount++;
					}
				}
				//calculate new mean
				float newMean = rssSum/rssCount;
				//Log.d(TAG,"AP: " + oi.getBSSID() + " , newMean: " + newMean + " , newCount: " + rssCount + " , rssThreshold: " + rssThreshold);
				oi.setRSS(newMean);
				oi.setRssArray(filteredRssValues);
				oi.setCount(rssCount);
			}
		}
		else{
			// Calculate the stdev of each scan
			for(OnlineItem oi : avgOnlineItems){
				float mean = oi.getRSS();
				float count = oi.getCount();
				float stdev = 0;
				float sum = 0;
				for( float rss : oi.getRSSvalues() ){
					sum += Math.pow(rss-mean,2);
				}
				stdev = (float) Math.sqrt(sum/count);
				oi.setRSSstdev(stdev);
				oi.setCountPercentage((count/numScans)*100);
			}
		}

		if(mUseKalmanAvg){
			boolean executeKalman = true;
			if(mUseKalmanAvgOnlyNonWalking){
				if(walking){
					executeKalman = false;
					lastAvgOnlineItems=null;
				}
			}
			if(executeKalman) {
				if (lastAvgOnlineItems == null) {
					// it is the first epoch. Copy results in lastAvgOnlineItems array
					lastAvgOnlineItems = new ArrayList<OnlineItem>();
					for (OnlineItem avgOI : avgOnlineItems) {
						OnlineItem newOI = new OnlineItem(avgOI.getSSID(), avgOI.getBSSID(), avgOI.getRSS(), avgOI.getRSSstdev(), avgOI.getCount(), avgOI.getCountPercentage());
						newOI.setP_kalman(avgOI.getRSS());
						lastAvgOnlineItems.add(newOI);
					}
				} else {
					// apply Kalman filter and save current values for next epoch
					//Log.d(TAG, "--- Applying KALMAN");
					ArrayList<OnlineItem> currentOnlineItems = new ArrayList<OnlineItem>();
					for (OnlineItem avgOI : avgOnlineItems) {
						float newRss = avgOI.getRSS();
						float newPkalman = newRss;
						int j = BleService.getIndex(lastAvgOnlineItems, avgOI.getBSSID());
						if (j >= 0) {
							/*
							 * we got a measurement from this BLE beacon in the last epoch,
							 * therefore we can run Kalman filter to improve the current
							 * measurement based on the last epoch measurement
							 */
							float last_epoch_rssi = lastAvgOnlineItems.get(j).getRSS();
							float measured_rssi = avgOI.getRSS();
							float p = lastAvgOnlineItems.get(j).getP_kalman();
							float[] filter_output = mKalmanFilterAvg.calculateOutput(last_epoch_rssi, p, measured_rssi);
							newRss = filter_output[0];
							newPkalman = filter_output[1];
							avgOI.setRSS(newRss);
							//Log.d(TAG, "--- KALMAN log for AP: " + avgOI.getBSSID() + " , last epoch rss: " + last_epoch_rssi + " , measured: " + measured_rssi + " , filtered: " + newRss);
						}
						OnlineItem newOI = new OnlineItem(avgOI.getSSID(), avgOI.getBSSID(), newRss, avgOI.getRSSstdev(), avgOI.getCount(), avgOI.getCountPercentage());
						newOI.setP_kalman(newPkalman);
						currentOnlineItems.add(newOI);
					}
					lastAvgOnlineItems = currentOnlineItems;
				}
			}
		}

		if(debug)	Log.d(TAG,"inside calcAvgScanSet (BLE). numScans: " + numScans + " , numDevices: " + avgOnlineItems.size());
		return avgOnlineItems;
	}

	/** Get last set of scan results **/
	public ArrayList<OnlineItem> getLastScanSet(){
		ArrayList<OnlineItem> lastOnlineItems = new ArrayList<OnlineItem>();
		OnlineMeasure om = mQueueOfOnlineMeasures.getLast();
		for(OnlineItem oi : om.getOnlineItems()){
			lastOnlineItems.add(new OnlineItem(oi.getSSID(), oi.getBSSID(), oi.getRSS()));
		}
		return lastOnlineItems;
	}



	/** return size of BLE scan buffer **/
	public int getScanBufferSize(){
		return mQueueOfOnlineMeasures.size();
	}

	public boolean isScanning(){
		return mIsScanOn;
	}

	public boolean isBound(){
		return mIsBound;
	}

	public void setBound(boolean isBound){
		mIsBound = isBound;
	}

	public void setParameters(boolean useKalmanRSSI, boolean useWeightedRSSI, boolean useIBeacons){
		this.mUseKalmanRSSI = useKalmanRSSI;
		this.mUseWeightedRSSI = useWeightedRSSI;
		this.useIBeacons = useIBeacons;
		if(this.mUseKalmanRSSI) mKalmanFilter = new KalmanFilter();
		if(this.mUseKalmanAvg) mKalmanFilterAvg = new KalmanFilter();
	}

	/** get index of a bssid in a scanSet **/
	private static int getIndex(ArrayList<OnlineItem> scanSet, String bssid){
		for (int i=0; i<scanSet.size(); i++){
			if (scanSet.get(i).getBSSID().equalsIgnoreCase(bssid)){
				return i;
			}
		}
		return -1;
	}

	private void startReportMeasuresTimer(){
		mReportMeasuresTimer = new Timer();
		mReportMeasuresTimerTask = new ReportMeasuresTimerTask();
		mReportMeasuresTimer.schedule(mReportMeasuresTimerTask, 0, REPORT_MEASURES_TIME);
	}

	private void stopReportMeasuresTimer(){
		if (mReportMeasuresTimer != null) {
			mReportMeasuresTimer.cancel();
			mReportMeasuresTimer = null;
		}
		if (mReportMeasuresTimerTask != null) {
			mReportMeasuresTimerTask.cancel();
			mReportMeasuresTimerTask = null;
		}
	}

	private void startStartStopTimer(){
		if(useStartStopTimer) {
			mStartStopTimer = new Timer();
			mStartStopTimerTask = new StartStopTimerTask();
			if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
				mStartStopTimer.schedule(mStartStopTimerTask,
										 START_STOP_TIME,
										 ANDROID_N_MIN_SCAN_CYCLE_MILLIS);
			}
			else{
				mStartStopTimer.schedule(mStartStopTimerTask,
										 START_STOP_TIME,
										 START_STOP_TIME);
			}

		}
	}

	private void stopStartStopTimer(){
		if(useStartStopTimer){
			if (mStartStopTimer != null) {
				mStartStopTimer.cancel();
				mStartStopTimer = null;
			}

			if (mStartStopTimerTask != null) {
				mStartStopTimerTask.cancel();
				mStartStopTimerTask = null;
			}
		}
	}

	/* event listeners */
	public synchronized void addBleNewScanListener(BleListener listener) {
		mScanBleListeners.add(listener);
	}

	public synchronized void removeBleNewScanListener(BleListener listener) {
		mScanBleListeners.remove(listener);
	}

	private synchronized void fireNewScanAvailableEvent(int numDevices){
		for(BleListener listener : mScanBleListeners){
			listener.newBleScanAvailable(numDevices);
		}
	}

}