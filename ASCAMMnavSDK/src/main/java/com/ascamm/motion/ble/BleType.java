package com.ascamm.motion.ble;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Class used to store BLE scan data
 */
public class BleType implements Serializable{

	private static final long serialVersionUID = 1L;
	private String mac;
	private float rssi;
	private int count;
	private ArrayList<Float> rssiValues;
	
	public BleType(String mac, float rssi){
		this.mac = mac;
		this.rssi = rssi;
		this.count = 1;
		rssiValues = new ArrayList<Float>();
	}
	
	public void setMac(String mac){
		this.mac = mac;
	}
	
	public void setRssi(float rssi){
		this.rssi = rssi;
	}
	
	public void setCount(int count) {
		this.count = count;
	}
	
	public int getCount() {
		return count;
	}
	
	public String getMac(){
		return mac;
	}
	
	public float getRssi(){
		return rssi;
	}

	public void addRssiValue(float rssi){
		rssiValues.add(rssi);
	}

	public ArrayList<Float> getRssiValues(){
		return rssiValues;
	}
}
