package com.ascamm.motion.ble;

/**
 * Class used to store iBeacon data
 */
public class IBeaconInfo {

	private boolean isIBeacon;
	private String UUID;
	private String major;
	private String minor;
	private String RSSI_1m;
	
	public IBeaconInfo(boolean isIBeacon, String UUID, String major, String minor, String RSSI_1m){
		this.isIBeacon = isIBeacon;
		this.UUID = UUID;
		this.major = major;
		this.minor = minor;
		this.RSSI_1m = RSSI_1m;
	}
	
	public boolean getIsIBeacon(){
		return isIBeacon;
	}
	
	public void setIsIBeacon(boolean isIBeacon) {
		this.isIBeacon = isIBeacon;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String uUID) {
		UUID = uUID;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getMinor() {
		return minor;
	}

	public void setMinor(String minor) {
		this.minor = minor;
	}

	public String getRSSI_1m() {
		return RSSI_1m;
	}

	public void setRSSI_1m(String rSSI_1m) {
		RSSI_1m = rSSI_1m;
	}
	
}
