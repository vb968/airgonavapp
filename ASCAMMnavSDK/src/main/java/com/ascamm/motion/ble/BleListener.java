package com.ascamm.motion.ble;



/**
 * This interface defines the methods related with BLE scans.
 * @author Daniel Fernandez [ASCAMM]
 *
 */
public interface BleListener {
	
	/**
	 * inform a scan is finished
	 */
	public void newBleScanAvailable(int numDevices);
}
