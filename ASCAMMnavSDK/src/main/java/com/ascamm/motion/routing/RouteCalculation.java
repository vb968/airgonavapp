package com.ascamm.motion.routing;

/**
 * Implementation of Route Calculation
 * @author Arnau Alcázar Lleopart <arnau.alcazar@eurecat.org>
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

import android.util.Log;
import com.ascamm.motion.algorithm.MapFusion;
import com.ascamm.motion.database.DBHelper;
import com.ascamm.motion.utils.Constants;
import com.ascamm.motion.utils.Edge;
import com.ascamm.motion.utils.FloorData;
import com.ascamm.motion.utils.Point3DWithFloor;
import com.ascamm.motion.utils.Point3DWithFloorAndEdge;
import com.ascamm.utils.Point3D;

public class RouteCalculation {
	
	private final String TAG = getClass().getSimpleName();

	private boolean debug = Constants.DEBUGMODE;
	
	private DBHelper dbHelper;
	//all edges loaded from database
	private Hashtable<Integer, ArrayList<Edge>> edges;
	
	//graf loaded. its status depends on variable grafLoadedStatus and grafFloor
	private Map<Integer, Node> graf;
	
	/** loaded nodes **/
	private static final int NONE = 0;
	private static final int ALL = 1;
	private static final int ONE = 2;
	private int grafLoadedStatus = NONE;
	private int grafFloor = 0;
	
	//heuristic class used to estimate the distance to the final goal
	private HeuristicFunctionImpl heuristic;
	
	//class used to map a point to a point of an edge
	private MapFusion mapFusion;

	private HashMap<Integer,Double> floorsMap;
	private ArrayList<FloorData> floorsArray;


	public RouteCalculation(DBHelper dbHelper, HeuristicFunctionImpl heuristic) {
		this.dbHelper = dbHelper;
		this.edges = this.dbHelper.getEdgesByFloor();
		this.mapFusion = new MapFusion(this.edges);
		this.heuristic = heuristic;
		this.floorsMap = this.dbHelper.getFloorsData();
		this.floorsArray = this.dbHelper.getFloorsDataArrayComplete();

		this.loadAllFloorNodes();
	}
	
	/**
	 * default contructor which initializes the heuristic function with a default 
	 * euclidean distance estimation
	 *  
	 * @param dbHelper database with the edges and nodes
	 */
	public RouteCalculation(DBHelper dbHelper) {
		this.dbHelper = dbHelper;
		this.edges = this.dbHelper.getEdgesByFloor();
		this.mapFusion = new MapFusion(this.edges);
		this.heuristic = new EuclideanDistanceHeuristic();
		this.floorsMap = this.dbHelper.getFloorsData();
		this.floorsArray = this.dbHelper.getFloorsDataArrayComplete();
		
		this.loadAllFloorNodes();
		printMap(edges);
	}
	
	public void reloadNodes() {
		this.grafLoadedStatus = NONE;
	}
	

	public Route calculateRoute(Point3D start, Point3D end) {

		//nodes already evaluated
		HashSet<Node> closedSet = new HashSet<Node>();
		
		//nodes to be evaluated
		TreeSet<Node> openSet = new TreeSet<Node>(new AscFNodeComparator());
		
		Route route;
		double tentativeGScore = 0.0;
		
		//only load one floor (all points are in the same floor)
		/*if(start.z == end.z && this.grafLoadedStatus != ALL) {
			//but only if it is not already loaded
			if(this.grafLoadedStatus != ONE || this.grafFloor != start.z) {
				this.loadFloorNodes((int) start.z); 
			}
		} else {
			//if each point is from differents floors, we need to load all floors
			//but only if they are not loaded yet
			if(this.grafLoadedStatus != ALL) {
				this.loadAllFloorNodes();
			}
		}*/

		/*
		if(grafLoadedStatus != ALL) {
			this.loadAllFloorNodes();
		}
		*/
		
		//first let's create a node for start and end in the graf
		double startHeight = dbHelper.getHeightOfPoint(start.z, floorsMap, floorsArray);
		Point3DWithFloor startWithFloor = new Point3DWithFloor(start.x, start.y, startHeight, start.z);
		Point3DWithFloorAndEdge startPE = mapFusion.doPathMatching(startWithFloor);
		Node startNode = addPointBetweenNodes(startPE);

		double endHeight = dbHelper.getHeightOfPoint(end.z, floorsMap, floorsArray);
		Point3DWithFloor endWithFloor = new Point3DWithFloor(end.x, end.y, endHeight, end.z);
		Point3DWithFloorAndEdge endPE = mapFusion.doPathMatching(endWithFloor);
		Node endNode = addPointBetweenNodes(endPE);

		openSet.add(startNode);
		startNode.setGScore(0.0);
		startNode.setFScore(this.heuristic.costEstimate(startNode, endNode));
		Node currentNode;
		
		while(openSet.size() > 0) {
			currentNode = openSet.pollFirst();
			
			if(debug)	Log.d(TAG, "Analyzing node " + currentNode.getPoint().getId());
			
			if(currentNode.getPoint().getId() == endNode.getPoint().getId()) {
				//we have reached the final node
				route = this.reconstructPath(startNode, endNode);
				return route;
			}
			
			openSet.remove(currentNode);
			closedSet.add(currentNode);
			
			//iterate through its connections
			for(Connection conn: currentNode.getConnections()) {
				if(closedSet.contains(conn.getEndNode())) {
					continue;
				}
				
				tentativeGScore = currentNode.getGScore() + conn.getDistance();
				
				if(!openSet.contains(conn.getEndNode()) || tentativeGScore < conn.getEndNode().getGScore()) {
					conn.getEndNode().setParent(currentNode);
					conn.getEndNode().setGScore(tentativeGScore);
					conn.getEndNode().setFScore(conn.getEndNode().getGScore() + this.heuristic.costEstimate(conn.getEndNode(), endNode));
					
					if(!openSet.contains(conn.getEndNode())) {
						openSet.add(conn.getEndNode());
					}
				}
			}
		}
		
		return null;
	}
	
	
	/**
	 * Generate the route from start and endNode, iterating through it
	 *	(every node is connected to its neighbours)
	 *
	 * Remove start and end node from graf
	 * 
	 * @param startNode
	 * @param endNode
	 * @return Route generated route
	 */
	private Route reconstructPath(Node startNode, Node endNode) {
		Route route = new Route();
		double distance = 0.0;
		Node lastNode = endNode;
		
		Node currentNode = endNode;
		while(startNode.getPoint().getId() != currentNode.getPoint().getId()) {
			Point3D nodeToAdd = new Point3D(currentNode.getPoint().x, currentNode.getPoint().y, currentNode.getPoint().getFloor());
			route.getRoutePoints().addFirst(nodeToAdd);
			currentNode = currentNode.getParent();
			distance += currentNode.getDistance(lastNode);
			lastNode = currentNode;
		}
		distance = distance / this.dbHelper.getPxMeter(); //convert from pixels to meters
		route.setDistance(distance);
		Point3D nodeToAdd = new Point3D(currentNode.getPoint().x, currentNode.getPoint().y, currentNode.getPoint().getFloor());
		route.getRoutePoints().addFirst(nodeToAdd);
		
		//remove start and end nodes from graf
		this.removeNodeFromGraf(startNode);
		this.removeNodeFromGraf(endNode);
		
		return route;
	}
	
	
	/**
	 * Remove node from graf, taking care of removing the connections between its neighbours
	 * @param node
	 */
	public void removeNodeFromGraf(Node node) {
		
		for(Connection conn: node.getConnections()) {
			conn.getEndNode().removeNodeConnection(node);
		}
		
		node.getConnections().clear();
		graf.remove(node.getPoint().getId());
		
	}
	
	/**
	 * This method adds a node in a existant edge, reconnecting the node
	 * to its adjacents nodes
	 * 
	 * @param pe point to add in the graf
	 * @return inserted node
	 */
	private Node addPointBetweenNodes(Point3DWithFloorAndEdge pe) {
		
		Connection conn;
		double distance;

		Node newNode = new Node(pe);
		Node node1 = this.graf.get(pe.getEdge().endNodeId);
		Node node2 = this.graf.get(pe.getEdge().startNodeId);

		if(node1 == null) {
			Log.d(TAG, "Node1 is null");
		}
		
		if(node2 == null) {
			Log.d(TAG, "Node2 is null");
		}

		int i = -1;
		while(this.graf.containsKey(i)) {
			i--;
		}
		newNode.getPoint().setId(i);
		this.graf.put(i, newNode);
		
		distance = newNode.getDistance(node1);
		conn = new Connection(node1, distance);
		newNode.addConnection(conn);
		conn = new Connection(newNode, distance);
		node1.addConnection(conn);
		
		distance = newNode.getDistance(node2);
		conn = new Connection(node2, distance);
		newNode.addConnection(conn);
		conn = new Connection(newNode, distance);
		node2.addConnection(conn);
		
		return newNode;
	}
	
	private void loadAllFloorNodes() {
		
		this.graf = new HashMap<Integer, Node>();
		
		Node startNode;
		Node endNode;
		ArrayList<Edge> edges;
		
		for(int key: this.edges.keySet()) {
			edges = this.edges.get(key);
			for(Edge e: edges) {
				if(!this.graf.containsKey(e.endNodeId)) {
					endNode = new Node(e.endNode);
					endNode.getPoint().setId(e.endNodeId);
					this.graf.put(e.endNodeId, endNode);
				} else {
					endNode = this.graf.get(e.endNodeId);
				}
				
				if(!this.graf.containsKey(e.startNodeId)) {
					startNode = new Node(e.startNode);
					startNode.getPoint().setId(e.startNodeId);
					this.graf.put(e.startNodeId, startNode);
				} else {
					startNode = this.graf.get(e.startNodeId);
				}
				
				boolean found = false;
				
				//check if node has a connection with the other node (transition edges are loaded in the two floors)
				for(Connection c: endNode.getConnections()) {
					if(c.getEndNode().getPoint().getId() == startNode.getPoint().getId()) {
						found = true;
						break;
					}
				}
				if(!found) {
					endNode.addConnection(new Connection(startNode, e.length));
				}
				
				found = false;
				//do the same with the other side of edge
				for(Connection c: startNode.getConnections()) {
					if(c.getEndNode().getPoint().getId() == endNode.getPoint().getId()) {
						found = true;
					}
				}
				if(!found) {
					startNode.addConnection(new Connection(endNode, e.length));
				}
				
			}
		}
		
	}
	
	private void loadFloorNodes(int floor) {
		
		this.graf = new HashMap<Integer, Node>();
		
		Node startNode;
		Node endNode;
		
		if(!edges.contains(floor)) {
			Log.d(TAG, "Floor number " + floor + "does not exist");
		}
		
		for(Edge e: edges.get(floor)) {
			
			//do not load transition edges
			if(e.endNode.z != floor || e.startNode.z != floor) {
				continue;
			}
			
			//add nodes in the list if it is not there yet
			if(!this.graf.containsKey(e.endNodeId)) {
				endNode = new Node(e.endNode);
				endNode.getPoint().setId(e.endNodeId);
				this.graf.put(e.endNodeId, endNode);
			} else {
				endNode = this.graf.get(e.endNodeId);
			}
			if(!this.graf.containsKey(e.startNodeId)) {
				startNode = new Node(e.startNode);
				startNode.getPoint().setId(e.startNodeId);
				this.graf.put(e.startNodeId, startNode);
			} else {
				startNode = this.graf.get(e.startNodeId);
			}
			
			//connect the two nodes between them
			startNode.addConnection(new Connection(endNode, e.length));
			endNode.addConnection(new Connection(startNode, e.length));
			
		}
	}

	private void printMap(Map mp) {
		Iterator it = mp.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			System.out.println(pair.getKey() + " = " + pair.getValue().toString());
			//it.remove(); // avoids a ConcurrentModificationException
		}
	}

}