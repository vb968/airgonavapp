package com.ascamm.motion.routing;

public interface RouteListener {

	public void newRouteCalculated(Route route);
	
}
