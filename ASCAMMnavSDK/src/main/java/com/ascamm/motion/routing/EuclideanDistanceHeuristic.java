package com.ascamm.motion.routing;

public class EuclideanDistanceHeuristic implements HeuristicFunctionImpl {

	//private static final double FLOOR_JUMP_PENALIZATION = 200.0;
	private static final double FLOOR_JUMP_PENALIZATION = 0.0;

	@Override
	public double costEstimate(Node node, Node goal) {
		
		//penalize the distance if nodes are in different floors
		if(node.getPoint().getFloor() != goal.getPoint().getFloor()) {
			return node.getDistance(goal) + this.FLOOR_JUMP_PENALIZATION;
		} else {
			return node.getDistance(goal);
		}
	}
}
