package com.ascamm.motion.routing;

public interface HeuristicFunctionImpl {

	public double costEstimate(Node node, Node goal);
	
}
