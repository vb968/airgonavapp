package com.ascamm.motion.routing;

public class Connection {

	private Node endNode;
	private double distance;
	
	public Connection(Node endNode, double distance) {
		this.endNode = endNode;
		this.distance = distance;
	}
	
	public Node getEndNode() {
		return this.endNode;
	}
	
	public double getDistance() {
		return this.distance;
	}
	
	public void setEndNode(Node endNode) {
		this.endNode = endNode;
	}
	
	public void setDistance(Float distance) {
		this.distance = distance;
	}
	
}
