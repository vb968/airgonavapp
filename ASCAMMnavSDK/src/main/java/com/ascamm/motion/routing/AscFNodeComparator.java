package com.ascamm.motion.routing;

import java.util.Comparator;

public class AscFNodeComparator implements Comparator<Node> {

	@Override
	public int compare(Node lhs, Node rhs) {
		if(lhs.getFScore() < rhs.getFScore()) {
			return -1;
		} else if(lhs.getFScore() == rhs.getFScore()) {
			return 0;
		} else {
			return 1;
		}
	}

}
