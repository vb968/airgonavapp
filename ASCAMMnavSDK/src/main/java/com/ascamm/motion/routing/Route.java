package com.ascamm.motion.routing;

import com.ascamm.utils.Point3D;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * Class used to store a calculated Route
 * @author Arnau Alcázar Lleopart <arnau.alcazar@eurecat.org>
 */

public class Route implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private LinkedList<Point3D> points = new LinkedList<Point3D>();
	private double distance;
	
	public void addNextRoutePoint(Point3D point) {
		this.points.addFirst(point);
	}
	
	public LinkedList<Point3D> getRoutePoints() {
		return this.points;
	}
	
	public double getDistance() {
		return this.distance;
	}
	
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
}