package com.ascamm.motion.routing;


import com.ascamm.motion.utils.Point3DWithFloor;

import java.util.ArrayList;

public class Node {

	private Node parent;
	private ArrayList<Connection> connections = new ArrayList<Connection>();
	private Point3DWithFloor point;
	private double GScore = 0.0;
	private double FScore = 0.0;
	
	public Node(Point3DWithFloor point) {
		this.point = point;
	}
	
	public void addConnection(Connection connection) {
		this.connections.add(connection);
	}
	
	public double getDistance(Node n) {
		double var = Math.sqrt(Math.pow(this.point.x - n.point.x, 2) + Math.pow(this.point.y - n.point.y, 2) + Math.pow(this.point.z - n.point.z, 2));
		return var;
	}
	
	public Point3DWithFloor getPoint() {
		return this.point;
	}
	
	public ArrayList<Connection> getConnections() {
		return this.connections;
	}
	
	public boolean removeNodeConnection(Node node) {
		//search the connection
		for(Connection conn: this.connections) {
			if(conn.getEndNode().getPoint().getId() == node.getPoint().getId()) {
				this.connections.remove(conn);
				return true;
			}
		}
		return false;
	}
	
	public void setGScore(double score) {
		this.GScore = score;
	}
	
	public double getGScore() {
		return this.GScore;
	}
	
	public double getFScore() {
		return this.FScore;
	}
	
	public void setFScore(double score) {
		this.FScore = score;
	}
	
	public void setParent(Node node) {
		this.parent = node;
	}
	
	public Node getParent() {
		return this.parent;
	}

	public String toString(){
		//return "Node: (" + this.point.x + ", " + this.point.y + ", " + this.point.z + ")";
		return "Node: (" + this.point.x + ", " + this.point.y + ", " + this.point.z + ", " + this.point.getFloor() + ")";
	}
	
}
 