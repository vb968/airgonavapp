package com.ascamm.motion;


/**
 * This class contains PosActions used tp communicate APP with SDK.
 */

public final class PosActions {

	public static final String SERVICE_OK = "com.ascamm.motion.ServiceOk";
	public static final String SET_SITE_ID = "com.ascamm.motion.SetSiteId";
	public static final String SET_SITE_ID_RESP = "com.ascamm.motion.SetSiteIdResp";
	public static final String SEND_LOCATIONS_CONFIG = "com.ascamm.motion.SendLocationsConfig";
	public static final String SEND_LOCATIONS_CONFIG_RESP = "com.ascamm.motion.SendLocationsConfigResp";
	public static final String IS_LOCATING = "com.ascamm.motion.IsLocating";
	public static final String IS_LOCATING_RESP = "com.ascamm.motion.IsLocatingResp";
	public static final String START_LOCATING = "com.ascamm.motion.StartLocating";
	public static final String START_LOCATING_RESP = "com.ascamm.motion.StartLocatingResp";
	public static final String STOP_LOCATING = "com.ascamm.motion.StopLocating";
	public static final String STOP_LOCATING_RESP = "com.ascamm.motion.StopLocatingResp";
	public static final String NEW_POSITION = "com.ascamm.motion.Position";
	public static final String NEW_DETECTIONS = "com.ascamm.motion.Detections";
	public static final String CALC_ROUTE = "com.ascamm.motion.CalcRoute";
	public static final String CALC_ROUTE_RESP = "com.ascamm.motion.CalcRouteResp";
	public static final String ERROR = "com.ascamm.motion.Error";

	public static final String NEW_POSITIONS = "com.ascamm.motion.Positions";
	public static final String NEW_SCANSET = "com.ascamm.motion.Scanset";
	public static final String NEW_MOTION = "com.ascamm.motion.Motion";
	public static final String NEW_AUTO_PARAMS = "com.ascamm.motion.AutoParams";
	public static final String NEW_DATABASE_DATA = "com.ascamm.motion.DatabaseData";
	public static final String NEW_MOTION_MODE = "com.ascamm.motion.MotionMode";

	public static final String ACTION_START_ON_BOOT = "com.ascamm.motion.ACTION_START_ON_BOOT";

}
