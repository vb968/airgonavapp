package com.ascamm.motion;

import java.io.Serializable;
import java.util.ArrayList;

import com.ascamm.motion.algorithm.ParticleState;

/**
 * Class used to store the data related with a calculated position
 * @author Marc Ciurana, ASCAMM
 */

public class LocationData implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public String sourceType; //(required). Type of technology ('gps', 'wifi', 'ble', 'wifi_ble')
	public int reliability; //(optional). Level of information reliability. Values between [0-9]
	public double accuracy; //(optional). Accuracy of the returned position.
	public double accuracy_meters; //(optional). Accuracy of the returned position (in meters)
	public long utctime; //(required). Time in seconds.
	public long utctimeMillis; //(required). Time in milliseconds
	public int siteId; //(required). Identifier of the site where position is being calculated.
	public double[] positionLocal; //(required). Position in local coordinates.
	public ArrayList<double[]> candidatesLocal = new ArrayList<double[]>(); //(optional). Candidates to be a final position
	public double[] positionGlobal; //(required). Position in global coordinates.
	public ArrayList<double[]> candidatesGlobal = new ArrayList<double[]>(); //(optional). Candidates to be a final position
	public double velocity; //(optional). Velocity expressed in m/s
	public int numSat = -1; //(optional). Number of GPS satellites seen
	public double userOrientation; //(optional). Orientation of the user (in degrees) relative to local coordinates
	public double userOrientationReliability; //(optional). Level of orientation reliability. Values between [0-1]

	public ArrayList<ParticleState> filterParticles; //(only for test purposes)
	public int filterIsResampled; //(only for test purposes)
	public ArrayList<double[]> listPositions = new ArrayList<double[]>(); //(only for test purposes)
	public double[] lastPositionLocal; //(only for test purposes)
    public float priorDistance; //(only for test purposes)
	public double relativeOrientation; //(only for test purposes)
	public double absoluteOrientation; //(only for test purposes)
	public double numSteps; //(only for test purposes)

	
    public LocationData(){
    	sourceType     = "";
		double[] coordLocal = {0,0,0};
    	positionLocal = coordLocal;
    	double[] coordGlobal = {0,0,0};
    	positionGlobal = coordGlobal;
    	double[] coordLastLocal = {0,0,0};
        lastPositionLocal = coordLastLocal;
    }
    
    public LocationData copy(){
    	LocationData locCopy = new LocationData();
    	locCopy.sourceType = sourceType;
    	locCopy.reliability = reliability;
    	locCopy.accuracy = accuracy;
    	locCopy.utctime = utctime;
    	locCopy.utctimeMillis = utctimeMillis;
    	locCopy.siteId = siteId;
    	locCopy.positionLocal = positionLocal;
    	locCopy.positionGlobal = positionGlobal;
    	locCopy.velocity = velocity;
    	return locCopy;
    }
    
    
}
