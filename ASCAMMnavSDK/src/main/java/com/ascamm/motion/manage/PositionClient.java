package com.ascamm.motion.manage;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.ascamm.motion.algorithm.APData;
import com.ascamm.motion.utils.Constants;
import com.ascamm.motion.LocationData;
import com.ascamm.motion.utils.Point3DWithFloor;
import com.ascamm.motion.utils.Settings;
import com.ascamm.motion.algorithm.AutocalibrationParameters;
import com.ascamm.motion.algorithm.PointWeight;
import com.ascamm.motion.ble.BleService;
import com.ascamm.motion.ble.BleService.BleBinder;
import com.ascamm.motion.detections.Detection;
import com.ascamm.motion.detections.DetectionListener;
import com.ascamm.motion.routing.Route;
import com.ascamm.motion.routing.RouteListener;
import com.ascamm.motion.utils.DbListener;
import com.ascamm.motion.utils.IndoorPositionListener;
import com.ascamm.motion.utils.IndoorPositionsListener;
import com.ascamm.motion.utils.LogWriter;
import com.ascamm.motion.utils.MotionListener;
import com.ascamm.motion.utils.MotionStepsInfoListener;
import com.ascamm.motion.utils.OnlineItem;
import com.ascamm.motion.utils.PositionListener;
import com.ascamm.motion.utils.PositionsListener;
import com.ascamm.motion.utils.ResponseObject;
import com.ascamm.motion.utils.ScansetListener;
import com.ascamm.motion.utils.UpdateAutocalibrationParametersListener;
import com.ascamm.motion.wifi.WiFiService;
import com.ascamm.motion.wifi.WiFiService.WiFiBinder;
import com.ascamm.utils.LatLng3D;
import com.ascamm.utils.Point3D;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;


public class PositionClient implements LocationListener {

	private String TAG = getClass().getSimpleName();
	
	private boolean debug = Constants.DEBUGMODE;

	private boolean locating = false, waiting = false;

	private Context mContext;
	private Context UIcontext;
	
	private WiFiService mWiFiService;
	boolean mWiFiServiceBound = false;
	private BleService mBleService;
	boolean mBleServiceBound = false;
	private IndoorPosClient mIndoorPosClient;

	private LocationManager mLocationManager; 
	private String mLocationProviderFine;
	private String mLocationProviderCoarse;
	
	private Timer mTimer;
	private TimerTask mTimerTask;

	private int mTimePeriod; //time period between position calculations (in milliseconds)

	private boolean mIsIndoorPosOn = false;
	private boolean mIsOutdoorPosOn = false;

	private int currentFloor = -100, lastFloor = -100, hystCounter = 0;
	private boolean updateLoc = true;

	/** Code added for logs **/
	private LogWriter mLogWriter;
	private boolean logsRunning = false, storeLogs = false, storeLogPositions = true;
	private String logsFilename = "logs_position_client.txt";

	private float pxMeter = -1;

	private LinkedList<LocationData> mQueueOfLocations = new LinkedList<LocationData>();
	private final int MAX_LOCATION_BUFFER_SIZE = 3;
	private boolean useAvgVelocity = true;

	// LQ: check pdb downloaded
	private boolean pdbDownloaded = Settings.siteInfoManual;
	
	private ArrayList<UpdateAutocalibrationParametersListener> mAutocalibrationParametersListeners = new ArrayList<UpdateAutocalibrationParametersListener>();
	
	/** Added to test RSS transformation **/
	private ArrayList<PositionsListener> mPositionsListeners = new ArrayList<PositionsListener>();
	//private ArrayList<LocationData> mLocationsData = new ArrayList<LocationData>();
	//private LocationData mLocationDataNoRssTransf = new LocationData(); //used to store the data related with a calculated position
	/**************************************/

	private ArrayList<PositionListener> mPositionListeners = new ArrayList<PositionListener>();
	private ArrayList<DetectionListener> mDetectionListeners = new ArrayList<DetectionListener>();
	private ArrayList<RouteListener> mRouteListeners = new ArrayList<RouteListener>();
	private ArrayList<ScansetListener> mScansetListeners = new ArrayList<ScansetListener>();
	private ArrayList<MotionListener> mMotionListeners = new ArrayList<MotionListener>();
	private ArrayList<DbListener> mDbListeners = new ArrayList<DbListener>();
	private ArrayList<MotionStepsInfoListener> mMotionStepsInfoListeners = new ArrayList<MotionStepsInfoListener>();

	private UpdateAutocalibrationParametersListener mAutocalibrationListener = new UpdateAutocalibrationParametersListener() {
		@Override
		public void newUpdate(AutocalibrationParameters autoParams){
			//Log.d(TAG, "newUpdate received. a: " + slope + " , b: " + intersection + " , y: " + residuals);
			fireNewAutocalibrationEvent(autoParams);
		}
	};
	
	
	private IndoorPositionListener mIndoorPositionListener = new IndoorPositionListener() {
		@Override
		public void newPositionAvailable(ResponseObject locationResponse) {
			//we have something coming from IndoorPosClient. Let's see if it is a position estimate
			int siteId = locationResponse.getSiteId();
			String tech = locationResponse.getTech();
			Point3DWithFloor mPositionLocal = locationResponse.getPositionLocal();
			ArrayList<Point3DWithFloor> mNeighborsLocal = locationResponse.getCandidatesLocal();
			Point3D mPositionGlobal = locationResponse.getPositionGlobal();
			ArrayList<Point3D> mNeighborsGlobal = locationResponse.getCandidatesGlobal();
			double positionError = locationResponse.getPositionError();

			if(debug)	Log.d(TAG,"Inside newPositionAvailable. (PosLocal) x: " + mPositionLocal.x + " , y: " + mPositionLocal.y + " , z: " + mPositionLocal.z);
			/*
			for(Point3D neighbor : mNeighborsLocal){
				System.out.println("Inside newPositionAvailable. (Cand) x: " + neighbor.x + " , y: " + neighbor.y + " , z: " + neighbor.z);
			}
			*/
			if(debug)	Log.d(TAG,"Inside newPositionAvailable. (PosGlobal) x: " + mPositionGlobal.x + " , y: " + mPositionGlobal.y + " , z: " + mPositionGlobal.z);
			/*
			for(Point3D neighbor : mNeighborsGlobal){
				System.out.println("Inside newPositionAvailable. (Cand) x: " + neighbor.x + " , y: " + neighbor.y + " , z: " + neighbor.z);
			}
			*/
			/** Code added to store data info into logs.txt **/
			if(storeLogs && logsRunning && storeLogPositions){
				long ts = System.currentTimeMillis();
				mLogWriter.addTextLog(ts, "POSITION: {" + mPositionLocal.x + "," + mPositionLocal.y + "," + mPositionLocal.z + "}");
			}
			
			if (mPositionLocal.x + mPositionLocal.y + mPositionLocal.z <= 0){
				if(Settings.useGps){
					if(mIsIndoorPosOn)
						stopIndoorPositioning();
	
					if(!mIsOutdoorPosOn && locating)
						startOutdoorPositioning();
				}
			} else {

				// RP: Apply floor hyst??

				// Implementation of a hysteresis cycle when changing the floor
				/*
				Log.d(TAG,"RP LQ hyst evaluation mPositionLocal.z != " +
						"currentFloor: " + (mPositionLocal.z != currentFloor));

				Log.d(TAG,"RP LQ hyst evaluation mPositionLocal.z == " +
						"lastFloor: " + (mPositionLocal.z == lastFloor));
				*/
				// Implementation of a hysteresis cycle when changing the floor
				if(Settings.numPointsHyst > 1){
					Log.d(TAG,"RP LQ INSIDE hyst evaluation");
					if((int) (Math.round(mPositionLocal.z)) != currentFloor && currentFloor != -100){
						if((int) (Math.round(mPositionLocal.z)) == lastFloor){
							Log.d(TAG,"RP LQ UPDATE LOC");
							hystCounter++;
							if(hystCounter == (Settings.numPointsHyst-1)){
								hystCounter = 0;
								updateLoc = true;
							}
							else{
								updateLoc = false;
							}
						}
						else{
							lastFloor = (int) (Math.round(mPositionLocal.z));
							hystCounter = 0;
							updateLoc = false;
						}
					}
					else{
						lastFloor = (int) (Math.round(mPositionLocal.z));
						hystCounter = 0;
						updateLoc = true;
					}
				}
				else{
					updateLoc = true;
				}
				
				if(updateLoc){
					// update current floor
					currentFloor = (int) (Math.round(mPositionLocal.z));
					
					//update new position estimate for the consumer of the "library"
					LocationData mLocationData = new LocationData();
					mLocationData.sourceType = tech;
					mLocationData.accuracy = positionError;
					mLocationData.accuracy_meters = positionError/pxMeter;
					mLocationData.reliability = 5;
			    	mLocationData.siteId = siteId;
					Calendar c = Calendar.getInstance();		
					mLocationData.utctime = (int) (c.getTimeInMillis()/1000); 
					mLocationData.utctimeMillis = c.getTimeInMillis(); 
					mLocationData.positionLocal[0] = mPositionLocal.x;  
			    	mLocationData.positionLocal[1] = mPositionLocal.y;
			    	mLocationData.positionLocal[2] = mPositionLocal.getFloor();
			    	mLocationData.positionGlobal[0] = mPositionGlobal.x;  
			    	mLocationData.positionGlobal[1] = mPositionGlobal.y;
			    	mLocationData.positionGlobal[2] = mPositionGlobal.z;
			    	//mLocationData.numSat = -1;
		    		/*
					//update candidates
					mLocationData.candidatesLocal.clear();
					for(Point3D neighbor : mNeighborsLocal){
						double[] coord = { neighbor.x, neighbor.y, neighbor.z };
						mLocationData.candidatesLocal.add(coord);
					}
					mLocationData.candidatesGlobal.clear();
					for(Point3D neighbor : mNeighborsGlobal){
						double[] coord = { neighbor.x, neighbor.y, neighbor.z };
						mLocationData.candidatesGlobal.add(coord);
					}
							
					doVelocityCalculations(mLocationData);
					*/
					if(Settings.useGps){
						if(mIsOutdoorPosOn){
							stopOutdoorPositioning();
						}
						
						// Codi comentat perque només entra quan parem el posicionament i estem a mig càlcul d'una posició indoor
						// Codi descomentat perque també es pot donar la situació de rebre una nova posició indoor quan mIsOutdoorPosOn = true && mIsIndoorPosOn = false. 
						// Llavors actualitzem mLocationData i posem mIsOutdoorPos a false, quedant mIsOutdoorPosOn = false i mIsIndoorPosOn = false (queda parada l'actualitzacio de posicio)
						// S'ha de pensar una solució. De moment he incorporat la variable locating 
						if(!mIsIndoorPosOn && locating){
							Log.e(TAG,"Ha entrat aqui");
							startIndoorPositioning();
						}
					}
				}
				if(debug)	Log.d(TAG,"currentFloor: " + currentFloor + " , lastFloor: " + lastFloor + " , hystCounter: " + hystCounter + " , updateLoc: " + updateLoc);
				
			}
		}
	};
	
	
	private IndoorPositionsListener mIndoorPositionsListener = new IndoorPositionsListener() {
		@Override
		public void newPositionAvailable(ArrayList<ResponseObject> locationResponses) {
			// LQ: log debug
			Log.d(TAG, "mlocation POSITIONCLIENT I HAVE A NEW POSITION AVAILABLE");

			ResponseObject locationResponse = locationResponses.get(0);
			
			//we have something coming from IndoorPosClient. Let's see if it is a position estimate
			int siteId = locationResponse.getSiteId();
			String tech = locationResponse.getTech();
			Point3DWithFloor mPositionLocal = locationResponse.getPositionLocal();
			ArrayList<Point3DWithFloor> mNeighborsLocal = locationResponse.getCandidatesLocal();
			Point3D mPositionGlobal = locationResponse.getPositionGlobal();
			ArrayList<Point3D> mNeighborsGlobal = locationResponse.getCandidatesGlobal();
			Point3DWithFloor mPositionLocalParticleFilter = locationResponse.getPositionParticleFilter();
			Point3DWithFloor mPositionLocalMapFusion = locationResponse.getPositionMapFusion();
			ArrayList<PointWeight> mListPositions = locationResponse.getListPositions();
			Point3D lastPositionLocal = locationResponse.getLastPositionLocal();
            float priorDistance = locationResponse.getPriorDistance();
			double relativeOrientation = locationResponse.getRelativeOrientation();
			double numSteps = locationResponse.getNumSteps();
			double userOrientation = locationResponse.getUserOrientation();
			double userOrientationReliability = locationResponse.getUserOrientationReliability();
			double absoluteOrientation = locationResponse.getAbsoluteOrientation();
			double positionError = locationResponse.getPositionError();
			//Log.i(TAG,"userTrajectory: " + userTrajectory);
			//Log.i(TAG,"numSteps: " + numSteps);
            //Log.i(TAG,"lastPos: " + lastPositionLocal + " , priorDist: " + priorDistance);
			//Log.i(TAG,"positionError: " + positionError);
			
			if(debug)	Log.d(TAG,"Inside newPositionAvailable. (PosLocal) x: " + mPositionLocal.x + " , y: " + mPositionLocal.y + " , z: " + mPositionLocal.z + " , floor: " + mPositionLocal.getFloor());
			/*
			for(Point3D neighbor : mNeighborsLocal){
				System.out.println("Inside newPositionAvailable. (Cand) x: " + neighbor.x + " , y: " + neighbor.y + " , z: " + neighbor.z);
			}
			*/
			if(debug)	Log.d(TAG,"Inside newPositionAvailable. (PosGlobal) x: " + mPositionGlobal.x + " , y: " + mPositionGlobal.y + " , z: " + mPositionGlobal.z);
			/*
			for(Point3D neighbor : mNeighborsGlobal){
				System.out.println("Inside newPositionAvailable. (Cand) x: " + neighbor.x + " , y: " + neighbor.y + " , z: " + neighbor.z);
			}
			*/
						
			/** Lines added to test RSS transformation **/
			ResponseObject locationResponseNoRssDiff = locationResponses.get(1);
			
			//we have something coming from IndoorPosClient. Let's see if it is a position estimate:
			Point3DWithFloor mPositionLocalNoRssDiff = locationResponseNoRssDiff.getPositionLocal();
			ArrayList<Point3DWithFloor> mNeighborsLocalNoRssDiff = locationResponseNoRssDiff.getCandidatesLocal();
			Point3D mPositionGlobalNoRssDiff = locationResponseNoRssDiff.getPositionGlobal();
			ArrayList<Point3D> mNeighborsGlobalNoRssDiff = locationResponseNoRssDiff.getCandidatesGlobal();
			ArrayList<PointWeight> mListPositionsNoRssDiff = locationResponseNoRssDiff.getListPositions();
			double positionErrorNoRssDiff = locationResponseNoRssDiff.getPositionError();
			
			if(debug)	Log.d(TAG,"Inside newPositionAvailable. (PosLocal) x: " + mPositionLocalNoRssDiff.x + " , y: " + mPositionLocalNoRssDiff.y + " , z: " + mPositionLocalNoRssDiff.z + " , floor: " + mPositionLocalNoRssDiff.getFloor());
			/*
			for(Point3D neighbor : mNeighborsLocal){
				System.out.println("Inside newPositionAvailable. (Cand) x: " + neighbor.x + " , y: " + neighbor.y + " , z: " + neighbor.z);
			}
			*/
			if(debug)	Log.d(TAG,"Inside newPositionAvailable. (PosGlobal) x: " + mPositionGlobalNoRssDiff.x + " , y: " + mPositionGlobalNoRssDiff.y + " , z: " + mPositionGlobalNoRssDiff.z);
			/*
			for(Point3D neighbor : mNeighborsGlobal){
				System.out.println("Inside newPositionAvailable. (Cand) x: " + neighbor.x + " , y: " + neighbor.y + " , z: " + neighbor.z);
			}
			*/
			/*****************************************/
			
			/** Code added to store data info into logs.txt **/
			if(storeLogs && storeLogPositions){
				if(mLogWriter.startLogsNoText()>0){
					long ts = System.currentTimeMillis();
					mLogWriter.addTextLog(ts, "POSITION: {" + mPositionLocal.x + "," + mPositionLocal.y + "," + mPositionLocal.z + "} , {" + mPositionGlobal.x + "," + mPositionGlobal.y + "," + mPositionGlobal.z + "} , ts: " + ts);
					mLogWriter.stopLogs();
				}
			}
			
			if (mPositionLocal.x + mPositionLocal.y + mPositionLocal.z <= 0){
				if(Settings.useGps){
					if(mIsIndoorPosOn)
						stopIndoorPositioning();
	
					if(!mIsOutdoorPosOn && locating)
						startOutdoorPositioning();
				}
			} else {

				// RP: Apply floor hyst??

				// Implementation of a hysteresis cycle when changing the floor
				/*
				Log.d(TAG,"RP LQ hyst evaluation mPositionLocal.z != " +
						"currentFloor: " + (mPositionLocal.z != currentFloor));

				Log.d(TAG,"RP LQ hyst evaluation mPositionLocal.z == " +
						"lastFloor: " + (mPositionLocal.z == lastFloor));

				Log.d(TAG,"RP LQ pre eval mP.z " + mPositionLocal.z + " " +
						"cf " + currentFloor + " lf " + lastFloor + " " +
						"hystCounter " + hystCounter + " up " + updateLoc);
				*/
				// Implementation of a hysteresis cycle when changing the floor
				if(Settings.numPointsHyst > 1){
					// Log.d(TAG,"RP LQ INSIDE hyst evaluation");
					if((int) (Math.round(mPositionLocal.z)) != currentFloor && currentFloor != -100){
						if((int) (Math.round(mPositionLocal.z)) == lastFloor){
							// Log.d(TAG,"RP LQ UPDATE LOC");
							hystCounter++;
							if(hystCounter == (Settings.numPointsHyst-1)){
								hystCounter = 0;
								updateLoc = true;
							}
							else{
								updateLoc = false;
							}
						}
						else{
							lastFloor = (int) (Math.round(mPositionLocal.z));
							hystCounter = 0;
							updateLoc = false;
						}
					}
					else{
						lastFloor = (int) (Math.round(mPositionLocal.z));
						hystCounter = 0;
						updateLoc = true;
					}
				}
				else{
					updateLoc = true;
				}
				/*
				Log.d(TAG,"RP LQ post eval mP.z " + mPositionLocal.z + " " +
						"cf " + currentFloor + " lf " + lastFloor + " " +
						"hystCounter " + hystCounter + " up " + updateLoc);
				*/

				if(updateLoc){

					// Log.d(TAG,"RP LQ INSIDE UPDATE LOC");
							// update current floor
					currentFloor = (int) (Math.round(mPositionLocal.z));
					
					//update new position estimate for the consumer of the "library"
					LocationData mLocationData = new LocationData();
					mLocationData.sourceType = tech;		
			    	mLocationData.accuracy = positionError;
					mLocationData.accuracy_meters = positionError/pxMeter;
			    	mLocationData.reliability = 5;
			    	mLocationData.siteId = siteId;
			    	Calendar c = Calendar.getInstance();
					mLocationData.utctime = (int) (c.getTimeInMillis()/1000); 
					mLocationData.utctimeMillis = c.getTimeInMillis(); 
					mLocationData.positionLocal[0] = mPositionLocal.x;  
			    	mLocationData.positionLocal[1] = mPositionLocal.y;
			    	mLocationData.positionLocal[2] = mPositionLocal.getFloor();
			    	mLocationData.positionGlobal[0] = mPositionGlobal.x;  
			    	mLocationData.positionGlobal[1] = mPositionGlobal.y;
			    	mLocationData.positionGlobal[2] = mPositionGlobal.z;
			    	mLocationData.numSat = -1;
					mLocationData.relativeOrientation = relativeOrientation;
					mLocationData.numSteps = numSteps;
					mLocationData.userOrientation = userOrientation;
					mLocationData.userOrientationReliability = userOrientationReliability;
					mLocationData.absoluteOrientation = absoluteOrientation;

					//update candidates
			    	if(mNeighborsLocal != null){
						mLocationData.candidatesLocal.clear();
						for(Point3DWithFloor neighbor : mNeighborsLocal){
							double[] coord = { neighbor.x, neighbor.y, neighbor.getFloor() };
							mLocationData.candidatesLocal.add(coord);
						}
			    	}
			    	if(mNeighborsGlobal != null){
						mLocationData.candidatesGlobal.clear();
						for(Point3D neighbor : mNeighborsGlobal){
							double[] coord = { neighbor.x, neighbor.y, neighbor.z };
							mLocationData.candidatesGlobal.add(coord);
						}
			    	}
			    	//update list positions
			    	if(mListPositions != null){
						mLocationData.listPositions.clear();
						for(PointWeight pw : mListPositions){
							double[] coord = { pw.getPoint().x, pw.getPoint().y, pw.getPoint().getFloor(), pw.getWeight() };
							mLocationData.listPositions.add(coord);
						}
			    	}
			    	//update last position and priorDistance
			    	if(lastPositionLocal!=null){
			    		mLocationData.lastPositionLocal[0] = lastPositionLocal.x;
			    		mLocationData.lastPositionLocal[1] = lastPositionLocal.y;
			    		mLocationData.lastPositionLocal[2] = lastPositionLocal.z;
			    		mLocationData.priorDistance = priorDistance;
		    		}
			    	else{
			    		//Log.e(TAG,"lastPos is null");
		    		}

			    	LocationData mLocationDataParticleFilter = new LocationData();
					if(Settings.useParticleFilter && mPositionLocalParticleFilter != null){
			    		mLocationDataParticleFilter.sourceType = tech;
						mLocationDataParticleFilter.accuracy = 3*pxMeter;
						mLocationDataParticleFilter.accuracy_meters = 3;
				    	mLocationDataParticleFilter.reliability = 5;
				    	mLocationDataParticleFilter.siteId = siteId;
						Calendar ca = Calendar.getInstance();		
						mLocationDataParticleFilter.utctime = (int) (ca.getTimeInMillis()/1000); 
						mLocationDataParticleFilter.utctimeMillis = ca.getTimeInMillis(); 
						
						//Point3D mPositionLocalParticleFilter = locationResponse.getPositionParticleFilter();
						mLocationDataParticleFilter.positionLocal[0] = mPositionLocalParticleFilter.x;  
				    	mLocationDataParticleFilter.positionLocal[1] = mPositionLocalParticleFilter.y;
				    	mLocationDataParticleFilter.positionLocal[2] = mPositionLocalParticleFilter.getFloor();
				    	
				    	mLocationDataParticleFilter.filterParticles = locationResponse.getFilterParticles();
				    	mLocationDataParticleFilter.filterIsResampled = locationResponse.getFilterIsResampled();
	   		    	}
			    	
			    	LocationData mLocationDataMapFusion = new LocationData();
					if(Settings.useMapFusion && mPositionLocalMapFusion != null){
			    		mLocationDataMapFusion.sourceType = tech;
						mLocationDataMapFusion.accuracy = 3*pxMeter;
						mLocationDataMapFusion.accuracy_meters = 3;
				    	mLocationDataMapFusion.reliability = 5;
				    	mLocationDataMapFusion.siteId = siteId;
						Calendar cal = Calendar.getInstance();		
						mLocationDataMapFusion.utctime = (int) (cal.getTimeInMillis()/1000); 
						mLocationDataMapFusion.utctimeMillis = cal.getTimeInMillis(); 
						
						//Point3D mPositionLocalMapFusion = locationResponse.getPositionMapFusion();
						mLocationDataMapFusion.positionLocal[0] = mPositionLocalMapFusion.x;  
				    	mLocationDataMapFusion.positionLocal[1] = mPositionLocalMapFusion.y;
				    	mLocationDataMapFusion.positionLocal[2] = mPositionLocalMapFusion.getFloor();
		   		    }			    	

			    	
					/** Lines added to test RSS transformation **/
			    	LocationData mLocationDataNoRssTransf = new LocationData();
					mLocationDataNoRssTransf.sourceType = tech;		
			    	mLocationDataNoRssTransf.accuracy = positionErrorNoRssDiff;
					mLocationDataNoRssTransf.accuracy_meters = positionErrorNoRssDiff/pxMeter;
			    	mLocationDataNoRssTransf.reliability = 5;
			    	mLocationDataNoRssTransf.siteId = siteId;
					mLocationDataNoRssTransf.utctime = (int) (c.getTimeInMillis()/1000); 
					mLocationDataNoRssTransf.utctimeMillis = c.getTimeInMillis(); 
					mLocationDataNoRssTransf.positionLocal[0] = mPositionLocalNoRssDiff.x;  
			    	mLocationDataNoRssTransf.positionLocal[1] = mPositionLocalNoRssDiff.y;
			    	mLocationDataNoRssTransf.positionLocal[2] = mPositionLocalNoRssDiff.getFloor();
			    	mLocationDataNoRssTransf.positionGlobal[0] = mPositionGlobalNoRssDiff.x;  
			    	mLocationDataNoRssTransf.positionGlobal[1] = mPositionGlobalNoRssDiff.y;
			    	mLocationDataNoRssTransf.positionGlobal[2] = mPositionGlobalNoRssDiff.z;
			    	mLocationDataNoRssTransf.numSat = -1;

					//update candidates
			    	if(mNeighborsLocalNoRssDiff != null){
						mLocationDataNoRssTransf.candidatesLocal.clear();
						for(Point3DWithFloor neighbor : mNeighborsLocalNoRssDiff){
							double[] coord = { neighbor.x, neighbor.y, neighbor.getFloor() };
							mLocationDataNoRssTransf.candidatesLocal.add(coord);
						}
			    	}
			    	if(mNeighborsGlobalNoRssDiff != null){
						mLocationDataNoRssTransf.candidatesGlobal.clear();
						for(Point3D neighbor : mNeighborsGlobalNoRssDiff){
							double[] coord = { neighbor.x, neighbor.y, neighbor.z };
							mLocationDataNoRssTransf.candidatesGlobal.add(coord);
						}		
			    	}
			    	//update list positions
			    	if(mListPositionsNoRssDiff != null){
						mLocationDataNoRssTransf.listPositions.clear();
						for(PointWeight pw : mListPositionsNoRssDiff){
							double[] coord = { pw.getPoint().x, pw.getPoint().y, pw.getPoint().getFloor(), pw.getWeight() };
							mLocationDataNoRssTransf.listPositions.add(coord);
						}
			    	}

					/**************************************/

					// RP: DEBUG to see events sent
					/*
					Log.d(TAG,
							"New event sent. mLocationsData array position 0 ," +
									" mLocationData.sourceType: " + mLocationData.sourceType +
									" , mLocationData.accuracy: " + mLocationData.accuracy +
									" , mLocationData.accuracy_meters: " + mLocationData.accuracy_meters +
									" , mLocationData.utctimeMillis: " + mLocationData.utctimeMillis +
									" , mLocationData.positionLocal[0]: " + mLocationData.positionLocal[0] +
									" , mLocationData.positionLocal[1]: " + mLocationData.positionLocal[1] +
									" , mLocationData.positionLocal[2]: " + mLocationData.positionLocal[2] +
									" , mLocationData.positionGlobal[0]: " + mLocationData.positionGlobal[0] +
									" , mLocationData.positionGlobal[1]: " + mLocationData.positionGlobal[1] +
									" , mLocationData.positionGlobal[2]: " + mLocationData.positionGlobal[2] +
									" , mLocationData.relativeOrientation: " + mLocationData.relativeOrientation +
									" , mLocationData.numSteps: " + mLocationData.numSteps +
									" , mLocationData.userOrientation: " + mLocationData.userOrientation +
									" , mLocationData.userOrientationReliability: " + mLocationData.userOrientationReliability +
									" , mLocationData.absoluteOrientation: " + mLocationData.absoluteOrientation
					);
					*/
			    	
			    	doVelocityCalculations(mLocationData);
			    	
			    	ArrayList<LocationData> mLocationsData = new ArrayList<LocationData>();
					mLocationsData.add(0, mLocationData);
					mLocationsData.add(1, mLocationDataNoRssTransf);
					mLocationsData.add(2, mLocationDataParticleFilter);
					mLocationsData.add(3, mLocationDataMapFusion);
					// fire New Position Event
					fireNewPositionsEvent(mLocationsData);

					// RP: DEBUG to see events sent
					Log.d(TAG, "mlocation New event sent OK.");

					if(Settings.useGps){
						if(mIsOutdoorPosOn){
							stopOutdoorPositioning();
						}
						
						// Codi comentat perque només entra quan parem el posicionament i estem a mig càlcul d'una posició indoor
						// Codi descomentat perque també es pot donar la situació de rebre una nova posició indoor quan mIsOutdoorPosOn = true && mIsIndoorPosOn = false. 
						// Llavors actualitzem mLocationData i posem mIsOutdoorPos a false, quedant mIsOutdoorPosOn = false i mIsIndoorPosOn = false (queda parada l'actualitzacio de posicio)
						// S'ha de pensar una solució. De moment he incorporat la variable locating 
						if(!mIsIndoorPosOn && locating){
							Log.e(TAG,"Ha entrat aqui");
							startIndoorPositioning();
						}
					}
				}
				if(debug)	Log.d(TAG,"currentFloor: " + currentFloor + " , lastFloor: " + lastFloor + " , hystCounter: " + hystCounter + " , updateLoc: " + updateLoc);
				
			}
		}
	};
	
	private DetectionListener mDetectionListener = new DetectionListener() {
		@Override
		public void newDetectionAvailable(ArrayList<Detection> detections, int siteId){
			if(debug){
				Log.d(TAG, "newDetections received:");
				for(Detection det : detections){
					Log.d(TAG, det.toString());
				}
			}
			fireNewDetectionEvent(detections, siteId);
		}
	};

	private RouteListener mRouteListener = new RouteListener() {

		@Override
		public void newRouteCalculated(Route route) {
			fireNewRouteCalculated(route);
		}
	};


	private ScansetListener mScansetListener = new ScansetListener() {
		@Override
		public void newScansetAvailable(ArrayList<OnlineItem> scanset){
			if(debug)	Log.d(TAG, "newScanset received");
			fireNewScansetEvent(scanset);
		}
	};
	
	private MotionListener mMotionListener = new MotionListener() {
		@Override
		public void newMotionAvailable(int sensorState){
			if(debug)	Log.d(TAG, "newMotion received. sensorState: " + sensorState);
			fireNewMotionEvent(sensorState);
		}
	};

	private DbListener mDbListener = new DbListener() {
		@Override
		public void dbLoaded(float pxMet){
			//Log.d(TAG, "DBListener: DB loaded. pxMeter: " + pxMet);
			pxMeter = pxMet;
			fireNewDatabaseData(pxMeter);
		}
	};

	private MotionStepsInfoListener mMotionStepsInfoListener = new MotionStepsInfoListener() {
		@Override
		public void newMotionStepsInfoAvailable(int motionMode, ArrayList<Double> stepsList, double orientation, double diffOrientation){
			fireNewMotionStepsInfoEvent(motionMode, stepsList, orientation, diffOrientation);
		}
	};

	private ServiceConnection mWiFiServiceConn = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// we have bound to WiFiService, cast the IBinder and get WiFiService instance
			WiFiBinder binder = (WiFiBinder) service;
			mWiFiService = binder.getService();
			mWiFiService.setBound(true);
			mWiFiService.setParameters(Settings.useKalmanRSSI, Settings.useWeightedRSSI);
			
			mWiFiServiceBound = true;
			
			// now that the service is connected
			initLBSClient();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mWiFiService.setBound(false);
		}
	};
	
	private ServiceConnection mBleServiceConn = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// we have bound to BleService, cast the IBinder and get BleService instance
			BleBinder binder = (BleBinder) service;
			mBleService = binder.getService();
			mBleService.setBound(true);
			mBleService.setParameters(Settings.useKalmanRSSI, Settings.useWeightedRSSI, Settings.useIBeacons);
			
			mBleServiceBound = true;
			
			// now that the service is connected
			initLBSClient();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mBleService.setBound(false);
		}
	};

	public PositionClient(Context context){
		this.mContext = context;
		/** Code added to store data info into logs.txt **/
		if(storeLogs){
			mLogWriter = new LogWriter(Constants.APP_DATA_FOLDER_NAME, logsFilename);
			if(mLogWriter.startLogs()>0){
				mLogWriter.stopLogs();
			}
			logsRunning = true;
		}
	}

	public void destroy(){
		if (mWiFiServiceBound) {		// Unbind from the service
			mContext.unbindService(mWiFiServiceConn);
			mWiFiServiceBound = false;
		}
		if (mBleServiceBound) {		// Unbind from the service
			mContext.unbindService(mBleServiceConn);
			mBleServiceBound = false;
		}
	}
	
	protected void initialConfig(){
		if(Settings.useGps){
			mLocationManager = (LocationManager) mContext.getSystemService(mContext.LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			criteria.setAltitudeRequired(false);
			criteria.setBearingRequired(false);
			criteria.setCostAllowed(false);
			criteria.setPowerRequirement(Criteria.POWER_LOW);      
			 
			criteria.setAccuracy(Criteria.ACCURACY_FINE); //GPS
			mLocationProviderFine = mLocationManager.getBestProvider(criteria, true);
			if(debug)	Log.i(TAG,"mLocationProviderFine: " + mLocationProviderFine);
		}
		
		if(Settings.useWifi){
			// Bind to our WiFi service
			Intent intentwifi = new Intent(mContext, WiFiService.class);
			mContext.bindService(intentwifi, mWiFiServiceConn, Context.BIND_AUTO_CREATE);
		}
		if(Settings.useBle){
			// Bind to our BLE service
			Intent intentble = new Intent(mContext, BleService.class);
			mContext.bindService(intentble, mBleServiceConn, Context.BIND_AUTO_CREATE);
		}
	}

	private void addLocation(LocationData location){
		// add the result to the end of the queue, remove head if it is full
		if(mQueueOfLocations.size() < MAX_LOCATION_BUFFER_SIZE){
			mQueueOfLocations.add(location);
		} else {
			while (mQueueOfLocations.size() >= MAX_LOCATION_BUFFER_SIZE){
				mQueueOfLocations.poll();
			}
			mQueueOfLocations.add(location);
		}
	}

	private void doVelocityCalculations(LocationData locData){
		double vel = calculateVelocity(locData);
    	locData.velocity = vel;
    	LocationData locCopy = locData.copy();
		addLocation(locCopy);
    	if(useAvgVelocity){
    		double avgVel = calculateAvgVelocity();
    		locData.velocity = avgVel;
    	}
	}
	
	private double calculateAvgVelocity(){
		int numLocationsToUse = MAX_LOCATION_BUFFER_SIZE;
		double totalVel = 0;
		if(mQueueOfLocations.size() < MAX_LOCATION_BUFFER_SIZE){
			numLocationsToUse = mQueueOfLocations.size(); 
		}
		for(int i=0; i<numLocationsToUse; i++){
			totalVel += mQueueOfLocations.get(i).velocity;
		}
		double avgVel = totalVel/numLocationsToUse;
		if(debug)	Log.d(TAG,"avgVel: " + avgVel);
		return avgVel;
	}
	
	private double calculateVelocity(LocationData location){
		double vel = 0;
		if(mQueueOfLocations.size() > 0){
			LocationData lastLocation = mQueueOfLocations.getLast();
			double dist = distanceBetweenLocations(lastLocation, location);
			long timeMillis = timeBetweenLocations(lastLocation, location);
			vel = (dist/timeMillis)*1000;
			if(vel<1){
				vel = 0;
			}
			if(debug)	Log.d(TAG,"dist: " + dist + " , timeMillis: " + timeMillis + " , vel: " + vel);
		}
		return vel;
	}
	
	private double distanceBetweenLocations(LocationData loc1, LocationData loc2){
		double dist = -1;
		if(loc1.siteId==-1 || loc2.siteId==-1){
			//use global coordinates to calculate distance
			Location locGlobal1 = new Location("loc1");
			locGlobal1.setLatitude(loc1.positionGlobal[0]);
			locGlobal1.setLongitude(loc1.positionGlobal[1]);
			Location locGlobal2 = new Location("loc2");
			locGlobal2.setLatitude(loc2.positionGlobal[0]);
			locGlobal2.setLongitude(loc2.positionGlobal[1]);
			dist = locGlobal1.distanceTo(locGlobal2);	
		}
		else{
			//use local coordinates to calculate distance
			double x = (loc1.positionLocal[0] - loc2.positionLocal[0]);
			double y = (loc1.positionLocal[1] - loc2.positionLocal[1]);
			double z = (loc1.positionLocal[2] - loc2.positionLocal[2]);
			double distPx = Math.sqrt(x * x + y * y + z * z);
			dist = distPx/pxMeter;
		}
		return dist;
	}
	
	private long timeBetweenLocations(LocationData loc1, LocationData loc2){
		return Math.abs(loc1.utctimeMillis - loc2.utctimeMillis);
	}
	
	/**************** Public methods ***************/

	public void setUIcontext(Context context){
		UIcontext = context;
		if(mIndoorPosClient!=null){
			mIndoorPosClient.setUIcontext(context);
		}
	}

	/**
	 * Calculates a route from start to end positions. Route is calculated in background
	 * and will send an event to RouteListeners when it has finished. You MUST register a 
	 * RouteListener to get the route calculated
	 * 
	 * @param start
	 * @param end
	 */
	public void calculateRoute(Point3D start, Point3D end) {
		if(mIndoorPosClient!=null) {
			//LQ: DEBUG start, end
			Log.d("LQ POIs", "start: "+start+" end: "+end);
			mIndoorPosClient.calculateRoute(start, end);
		}
	}
	
	public void Start(int period){
		//Toast.makeText(this, "inside service Positioning Dipacemo: Start(int period)", Toast.LENGTH_SHORT).show();
		initialConfig();
		mTimePeriod = period; //time period between position calculations (in milliseconds)
		
		if(mIndoorPosClient == null){
			// We have to wait to create indoorPosClient
			waiting = true;
		}
		else{
			executeStart();
		}
		/** Code added to store data info into logs.txt **/
		if(storeLogs){
			if(mLogWriter.startLogsNoText()>0){
				long ts = System.currentTimeMillis();
				mLogWriter.addTextLog(ts, "starting positioning...");
				mLogWriter.stopLogs();
			}
		}
	}
	
	private void executeStart(){
		locating = true;
		startIndoorPositioning();
		if(Settings.useGps){
			startTryIndoor();
		}
	}
	
	public void Stop(){
		locating = false;
		if(debug){
			Log.d(TAG,"Inside Stop. mIsIndoorPosOn: " + mIsIndoorPosOn + " , mIsOutdoorPosOn: " + mIsOutdoorPosOn);
			//Log.d(TAG, "stopping Try Indoor");
		}
		if(Settings.useGps){
			stopTryIndoor();
		}
		if(mIsIndoorPosOn){
			if(debug)	Log.d(TAG, "stopping indoor positioning");
			stopIndoorPositioning();
		}
		if(mIsOutdoorPosOn){
			if(debug)	Log.d(TAG, "stopping outdoor positioning");
			stopOutdoorPositioning();
		}
		/** Code added to store data info into logs.txt **/
		if(storeLogs){
			if(mLogWriter.startLogsNoText()>0){
				long ts = System.currentTimeMillis();
				mLogWriter.addTextLog(ts, "stopping positioning...");
				mLogWriter.stopLogs();
			}
		}
	}
	
	public boolean isLocating(){
		return locating;
	}

	public List<Integer> getFloorList() {
		if(this.mIndoorPosClient != null) {
			return this.mIndoorPosClient.getFloorList();
		} else {
			return null;
		}
	}

	public Point3D convertFromGlobalToLocal(LatLng3D point) {
		Point3D convPoint = null;
		if(this.mIndoorPosClient != null) {
			convPoint = this.mIndoorPosClient.convertFromGlobalToLocal(point);
		}
		return convPoint;
	}

	public LatLng3D convertFromLocalToGlobal(Point3D point) {
		LatLng3D convPoint = null;
		if(this.mIndoorPosClient != null) {
			convPoint = this.mIndoorPosClient.convertFromLocalToGlobal(point);
		}
		return convPoint;
	}
		
	@Override
	public void onLocationChanged(Location location) {
		if(debug)	Log.d(TAG, "new "+ location.getProvider() +" position");		

		LocationData mLocationData = new LocationData();
		mLocationData.sourceType = location.getProvider();
		mLocationData.siteId = -1;
		mLocationData.positionLocal[0] = -1; 
    	mLocationData.positionLocal[1] = -1;
    	mLocationData.positionLocal[2] = -100;
		// LQ: change floor in outdoors
		// if is currently recieving GPS positions check for any visible outdoor AP
		ArrayList<APData> outdoorAPs = mIndoorPosClient.getOudoorAPs();
		if (!outdoorAPs.isEmpty()) {
			Log.d("outdoor beacons", "outdoor aps [0] " + outdoorAPs.get(0).getBSSID() + " " + outdoorAPs.get(0).getRSS() + " " + outdoorAPs.get(0).getPoint());
			// set the floor for local position
			mLocationData.positionLocal[2] = outdoorAPs.get(0).getPoint().z;
		}
    	mLocationData.positionGlobal[0] = location.getLatitude();
    	mLocationData.positionGlobal[1] = location.getLongitude();
    	mLocationData.positionGlobal[2] = location.getAltitude();
		mLocationData.accuracy = location.getAccuracy();
		mLocationData.accuracy_meters = location.getAccuracy();
    	mLocationData.reliability = 5;
		Calendar c = Calendar.getInstance();		
		mLocationData.utctime = (int) (c.getTimeInMillis()/1000);
		mLocationData.utctimeMillis = c.getTimeInMillis();

		mLocationData.candidatesLocal.clear();
		mLocationData.candidatesGlobal.clear();
		Bundle extras = location.getExtras();
		if (extras.containsKey("satellites")){
			Object value = extras.get("satellites");
			if(debug)	Log.d(TAG,"satellites found: " + value);
			if(value!=null){
				try {
					mLocationData.numSat = Integer.parseInt(value.toString());
				} catch(NumberFormatException e){
					Log.e(TAG,"Error converting numSat to int");
				}
				if(debug)	Log.d(TAG,"numSat: " + mLocationData.numSat);
			}
		}
		if(debug){
			Set<String> keys = extras.keySet();
			int numKeys = keys.size();
			Log.d(TAG,"GPS Bundle extras(" + numKeys + "):");
			for(String key : keys){
				Object value = extras.get(key);
				Log.d(TAG,"( " + key + " , " + value + ")");
			}
		}
		doVelocityCalculations(mLocationData);

		// fire New Position Event
		ArrayList<LocationData> mLocationsData = new ArrayList<LocationData>();
		mLocationsData.add(0, mLocationData);
		LocationData mLocationDataNoRssTransf = new LocationData();
		mLocationsData.add(1, mLocationDataNoRssTransf);
		fireNewPositionsEvent(mLocationsData);
	}

	@Override
	public void onProviderDisabled(String provider) {
		/* this is called if/when the GPS or Network is disabled in settings */
		if(debug)	Log.d(TAG, provider + " is Disabled");

		/* bring up the GPS settings */
		Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		mContext.startActivity(intent);
	}

	@Override
	public void onProviderEnabled(String provider) {
		if(debug)	Log.d(TAG, provider + " Enabled");
		//Toast.makeText(this, "GPS Enabled", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		/* This is called when the GPS status alters */
		switch (status) {
		case LocationProvider.OUT_OF_SERVICE:
			if(debug)	Log.d(TAG, "Status Changed: Out of Service");
			//Toast.makeText(this, "Status Changed: Out of Service",Toast.LENGTH_SHORT).show();
			break;
		case LocationProvider.TEMPORARILY_UNAVAILABLE:
			if(debug)	Log.d(TAG, "Status Changed: Temporarily Unavailable");
			//Toast.makeText(this, "Status Changed: Temporarily Unavailable",Toast.LENGTH_SHORT).show();
			break;
		case LocationProvider.AVAILABLE:
			if(debug)	Log.d(TAG, "Status Changed: Available");
			//Toast.makeText(this, "Status Changed: Available",Toast.LENGTH_SHORT).show();
			break;
		}
	}
	
	private void startIndoorPositioning(){
		mIndoorPosClient.startPositioning(mTimePeriod, Settings.siteFromSettings, pdbDownloaded);
		mIsIndoorPosOn = true;
		if(debug)	Log.d(TAG, "start indoor positioning");
	}

	private void stopIndoorPositioning(){
		mIndoorPosClient.stopPositioning();
		mIsIndoorPosOn = false;
		if(debug)	Log.d(TAG, "stop indoor positioning");
	}

	private void startOutdoorPositioning(){
		if (!pdbDownloaded) {
			pdbDownloaded = true;
		}
		boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if(!isGPSEnabled){
			Log.e(TAG,"GPS is not enabled. APP will crash");
		}
		mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, mTimePeriod, 0f, this);
				
		// add location listener and request updates every TimePeriod * 1000ms or 3m
		/*
		if (mLocationProviderCoarse != null) {
			mLocationManager.requestLocationUpdates(mLocationProviderCoarse, mTimePeriod*1000, 0f, this);
			Log.d(TAG, "start outdoor positioning with Network");
		}
		if (mLocationProviderFine != null) {
			mLocationManager.requestLocationUpdates(mLocationProviderFine, mTimePeriod*1000, 0f, this);
			Log.d(TAG, "start outdoor positioning with GPS");
		}
		*/
		
		mIsOutdoorPosOn = true;
		if(debug)	Log.d(TAG, "start outdoor positioning");
	}

	private void stopOutdoorPositioning(){
		mLocationManager.removeUpdates(this);
		mIsOutdoorPosOn = false;
		if(debug)	Log.d(TAG, "stop outdoor positioning");
	}

	private void initLBSClient(){
		boolean servicesBound = false;
		if(Settings.useBle && Settings.useWifi){
			if(mWiFiServiceBound && mBleServiceBound){
				servicesBound = true;
			}
		}else if(Settings.useWifi){
			if(mWiFiServiceBound){
				servicesBound = true;
			}
		}else if(Settings.useBle){
			if(mBleServiceBound){
				servicesBound = true;
			}
		}
		
		if(servicesBound){
			mIndoorPosClient = new IndoorPosClient(mContext, UIcontext, mWiFiService, mBleService);
			//System.out.println("mIndoorPosClient has been created: " + mIndoorPosClient);
			mIndoorPosClient.addPositionListener(mIndoorPositionListener);
			// Line added to test RSS transformation
			mIndoorPosClient.addAutocalibrationListener(mAutocalibrationListener);
			mIndoorPosClient.addPositionsListener(mIndoorPositionsListener);
			mIndoorPosClient.addScansetListener(mScansetListener);
			if(Settings.useDetections){
				mIndoorPosClient.addDetectionListener(mDetectionListener);
			}
			mIndoorPosClient.addDbLoadedListener(mDbListener);
			mIndoorPosClient.addMotionListener(mMotionListener);
			mIndoorPosClient.addRouteListener(mRouteListener);
			mIndoorPosClient.addMotionStepsInfoListener(mMotionStepsInfoListener);
			if(waiting){
				// Start method was previously called and now we are able to start positioning
				executeStart();
			}
		}
	}

	private void startTryIndoor(){
		mTimer = new Timer();
		mTimerTask = new TryIndoorPosTask();
		mTimer.schedule(mTimerTask, 0, Constants.TRY_INDOOR_NAV);
	}  

	private void stopTryIndoor(){
		if (mTimer != null) {  
			mTimer.cancel();  
			mTimer = null;  
		}  
		if (mTimerTask != null) {  
			mTimerTask.cancel();  
			mTimerTask = null;  
		}     
	}
	
	private class TryIndoorPosTask extends TimerTask{
		@Override
		public void run() {
			if(debug){
				Log.d(TAG, ">>> Starting scheduled TASK TryIndoorPosTask...");
				Log.d(TAG, "mIsOutdoorPosOn: " + mIsOutdoorPosOn + " , mIsIndoorPosOn: " + mIsIndoorPosOn);
			}
			/*
			do {
				Log.d(TAG, ">>> Iteration inside scheduled TASK TryIndoorPosTask...");
				try {  
					Thread.sleep(1000);  
				} catch (InterruptedException e) {}     
			} while (!mIsOutdoorPosOn);
			*/
			
			//jo faria if(mIsOutdoorPosOn) { startIndoorPositioning() } pq si fem el do-while es queda intentant-ho cada segon 
			//if(mIsOutdoorPosOn) {
			if(mIsOutdoorPosOn && !mIsIndoorPosOn) {
				if(debug)	Log.d(TAG, ">>> Trying indoor positioning...");
				startIndoorPositioning();
			}
			
			/*
			Log.d(TAG, ">>> Trying indoor positioning...");
			startIndoorPositioning();
			*/
		}
	}

	/****************************************/

	/* event listeners corresponding to positions */
	/*
	public synchronized void addPositionListener(PositionListener listener) {
		mPositionListeners.add(listener);
	}

	public synchronized void removePositionListener(PositionListener listener) {
		mPositionListeners.remove(listener);
	}

	private synchronized void fireNewPositionEvent(LocationData locationData){
		for(PositionListener listener : mPositionListeners){
			listener.newPositionAvailable(locationData);
		}
	}
	*/

	/* event listeners corresponding to detections */
	public synchronized void addDetectionListener(DetectionListener listener) {
		mDetectionListeners.add(listener);
	}

	public synchronized void removeDetectionListener(DetectionListener listener) {
		mDetectionListeners.remove(listener);
	}

	private synchronized void fireNewDetectionEvent(ArrayList<Detection> detections, int siteId){
		for(DetectionListener listener : mDetectionListeners){
			listener.newDetectionAvailable(detections, siteId);
		}
	}

	// event listeners corresponding to route calculating
	public synchronized void addRouteListener(RouteListener listener) {
		this.mRouteListeners.add(listener);
	}

	public synchronized void removeRouteListener(RouteListener listener) {
		this.mRouteListeners.remove(listener);
	}

	private synchronized void fireNewRouteCalculated(Route route) {
		for(RouteListener listener: mRouteListeners) {
			listener.newRouteCalculated(route);
		}
	}

	/****************************************/

	/* event listeners corresponding to RSS transformation */
	public synchronized void addAutocalibrationListener(UpdateAutocalibrationParametersListener listener) {
		mAutocalibrationParametersListeners.add(listener);
	}

	public synchronized void removeAutocalibrationListener(UpdateAutocalibrationParametersListener listener) {
		mAutocalibrationParametersListeners.remove(listener);
	}

	private synchronized void fireNewAutocalibrationEvent(AutocalibrationParameters autoParams){
		for(UpdateAutocalibrationParametersListener listener : mAutocalibrationParametersListeners){
			listener.newUpdate(autoParams);
		}
	}

	/** Added to test RSS transformation **/
	public synchronized void addPositionsListener(PositionsListener listener) {
		mPositionsListeners.add(listener);
	}

	public synchronized void removePositionsListener(PositionsListener listener) {
		mPositionsListeners.remove(listener);
	}

	private synchronized void fireNewPositionsEvent(ArrayList<LocationData> locationsData){
		for(PositionsListener listener : mPositionsListeners){
			listener.newPositionAvailable(locationsData);
		}
	}

	/* event listeners corresponding to scanset */
	public synchronized void addScansetListener(ScansetListener listener) {
		mScansetListeners.add(listener);
	}

	public synchronized void removeScansetListener(ScansetListener listener) {
		mScansetListeners.remove(listener);
	}

	private synchronized void fireNewScansetEvent(ArrayList<OnlineItem> scanset){
		for(ScansetListener listener : mScansetListeners){
			listener.newScansetAvailable(scanset);
		}
	}
	
	/* event listeners corresponding to sensors */
	public synchronized void addMotionListener(MotionListener listener) {
		mMotionListeners.add(listener);
	}

	public synchronized void removeMotionListener(MotionListener listener) {
		mMotionListeners.remove(listener);
	}

	private synchronized void fireNewMotionEvent(int sensorState){
		for(MotionListener listener : mMotionListeners){
			listener.newMotionAvailable(sensorState);
		}
	}

	// event listeners corresponding to database data
	public synchronized void addDatabaseDataListener(DbListener listener) {
		this.mDbListeners.add(listener);
	}

	public synchronized void removeDatabaseDataListener(DbListener listener) {
		this.mDbListeners.remove(listener);
	}

	private synchronized void fireNewDatabaseData(float pxMeter) {
		for(DbListener listener: mDbListeners) {
			listener.dbLoaded(pxMeter);
		}
	}

	/* event listeners corresponding to motion steps info */
	public synchronized void addMotionStepsInfoListener(MotionStepsInfoListener listener) {
		mMotionStepsInfoListeners.add(listener);
	}

	public synchronized void removeMotionStepsInfoListener(MotionStepsInfoListener listener) {
		mMotionStepsInfoListeners.remove(listener);
	}

	private synchronized void fireNewMotionStepsInfoEvent(int sensorState, ArrayList<Double> stepsList, double orientation, double diffOrientation){
		for(MotionStepsInfoListener listener : mMotionStepsInfoListeners){
			listener.newMotionStepsInfoAvailable(sensorState, stepsList, orientation, diffOrientation);
		}
	}

}
