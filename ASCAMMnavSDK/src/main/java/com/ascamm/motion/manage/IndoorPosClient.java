package com.ascamm.motion.manage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.ascamm.motion.algorithm.APData;
import com.ascamm.motion.algorithm.CoordConversion;
import com.ascamm.motion.comm.HttpConnection;
import com.ascamm.motion.utils.LogWriter;
import com.ascamm.motion.algorithm.AutocalibrationParameters;
import com.ascamm.motion.algorithm.PositionEstimation;
import com.ascamm.motion.ble.BleListener;
import com.ascamm.motion.ble.BleService;
import com.ascamm.motion.database.DBHelper;
import com.ascamm.motion.database.LocalDBHelper;
import com.ascamm.motion.detections.Detection;
import com.ascamm.motion.detections.DetectionListener;
import com.ascamm.motion.mems.MotionMEMS;
import com.ascamm.motion.routing.Route;
import com.ascamm.motion.routing.RouteCalculation;
import com.ascamm.motion.routing.RouteListener;
import com.ascamm.motion.utils.DbListener;
import com.ascamm.motion.utils.IndoorPositionListener;
import com.ascamm.motion.utils.IndoorPositionsListener;
import com.ascamm.motion.utils.JsonClientMessage;
import com.ascamm.motion.utils.JsonMessageDecoder;
import com.ascamm.motion.utils.LogReader;
import com.ascamm.motion.utils.LogReader.FileType;
import com.ascamm.motion.utils.MotionListener;
import com.ascamm.motion.utils.MotionStepsInfoListener;
import com.ascamm.motion.utils.OnlineItem;
import com.ascamm.motion.utils.Point3DWithFloor;
import com.ascamm.motion.utils.ResponseObject;
import com.ascamm.motion.utils.ScansetListener;
import com.ascamm.motion.utils.SimOnlineMeasure;
import com.ascamm.motion.utils.UpdateAutocalibrationParametersListener;
import com.ascamm.motion.utils.WS_URLS;
import com.ascamm.motion.wifi.WiFiListener;
import com.ascamm.motion.wifi.WiFiService;
import com.ascamm.motion.ws.DownloadFileCompleteListener;
import com.ascamm.motion.ws.DownloadFileResult;
import com.ascamm.motion.ws.DownloadFileWithProgressAT;
import com.ascamm.motion.ws.WebService.RequestMethod;
import com.ascamm.motion.ws.WebServiceAT;
import com.ascamm.motion.ws.WebServiceCompleteListener;
import com.ascamm.motion.ws.WebServiceResult;

import com.ascamm.motion.utils.Constants;
import com.ascamm.motion.utils.Settings;
import com.ascamm.utils.LatLng3D;
import com.ascamm.utils.Point3D;

/**
 * Class that offers a WiFi/BLE positioning and LB service managing 
 * http-based communications with the positioning server 
 * 
 * @author Marc Ciurana, ASCAMM
 */

public class IndoorPosClient {
	
	private final String TAG = this.getClass().getSimpleName();
	
	private boolean debug = Constants.DEBUGMODE;

	private Context mContext;
	private Context mUIcontext;
	private ScheduledExecutorService mScheduleTaskExecutor;
	private ScheduledFuture<?> mScheduledFuture;
	
	private LocalDBHelper mLocalDBHelper;
	private DBHelper dBHelper;
	private boolean dbFromAssets = Settings.pdbFromAssets;
	
	private int mSecondsOfScansToUse = Constants.SECONDS_OF_SCANS_STATIC; 		// in seconds
	private int secondsOfScansToUseWalking = Constants.SECONDS_OF_SCANS_WALKING; // Casa Batllo
	private int secondsOfScansToUseGoingToStatic = Constants.SECONDS_OF_SCANS_GOING_TO_STATIC;
	private int calcPosTime = Constants.DEFAULT_POSITION_REFRESH_TIME; // in milliseconds (default value)

	private JsonClientMessage mEncoder;
	private JsonMessageDecoder mDecoder;

	private ResponseObject locationResponse;
	private WiFiService mWiFiService;
	private BleService mBleService;
	boolean useWifi = false;
	boolean useBle = false;
	
	public RouteCalculation routeCalculation;
	private LinkedList<RouteListener> mRouteListeners = new LinkedList<RouteListener>();
		
	private String LNS_URL = "";
	private boolean calcPosLocal = true;
	private boolean mUseDetections = false;
	
	private PositionEstimation mPosEstimation;
	
	private boolean loadingRssFile = false;
	private boolean loadingPosFile = false;
	
	private float mWifiRssDetection = Constants.mWifiRssDetection;
	private float mBleRssDetection = Settings.bleRssDetection;
	private int WIFI_DETECTION = 1;
	private int BLE_DETECTION = 2;
	
	/** Code added for logs **/
	private LogWriter mLogWriter;
	private boolean logsRunning = false, storeLogs = Settings.storeLogs;
	private String logsFilename = Settings.logsFilename + "_RSS_avg" + Constants.logsFilenameExt;
	
	/** Listeners **/
	private ArrayList<IndoorPositionListener> mPositionListeners = new ArrayList<IndoorPositionListener>();
	private ArrayList<DetectionListener> mDetectionListeners = new ArrayList<DetectionListener>();
	private ArrayList<UpdateAutocalibrationParametersListener> mAutocalibrationParametersListeners = new ArrayList<UpdateAutocalibrationParametersListener>();
	private ArrayList<ScansetListener> mScansetListeners = new ArrayList<ScansetListener>();
	private ArrayList<MotionListener> mMotionListeners = new ArrayList<MotionListener>();
	private ArrayList<DbListener> mDbListeners = new ArrayList<DbListener>();
	private ArrayList<MotionStepsInfoListener> mMotionStepsInfoListeners = new ArrayList<MotionStepsInfoListener>();

	//ArrayList<OnlineItem> scanset = new ArrayList<OnlineItem>();
	private List<OnlineItem> scanset = Collections.synchronizedList(new ArrayList<OnlineItem>());
	private int POSITION_EVENT = 1;
	private int SCANSET_EVENT = 2;
	private int MOTION_EVENT = 3;
	private int AUTOCALIBRATION_EVENT = 4;
	private int ROUTE_EVENT_CALCULATED = 5;
	private int DETECT_SITE_EVENT = 6;
	private boolean filterOnlineItems = false;
	private static int minCount = Constants.mMinCount;
	private static float minPercentage = Constants.mMinCountPercentage;
		
	/** Added to test RSS transformation **/
	//private ArrayList<ResponseObject> locationResponses = new ArrayList<ResponseObject>();
	private List<ResponseObject> locationResponses = Collections.synchronizedList(new ArrayList<ResponseObject>());
	private ArrayList<IndoorPositionsListener> mPositionsListeners = new ArrayList<IndoorPositionsListener>();
	/**************************************/
	
	/************** RSS simulation variables ************/
	private boolean useRssSimulation = false;
	private LinkedList<SimOnlineMeasure> simScansQueue;
	private int scanIndex = 0, simScansQueueSize = 0;
	//private String rssFileToRead = "logs_RSS_avg_CAR.txt";
	//private String rssFileToRead = "logs_RSS_avg_ASCAMM_lab_route_1.txt";
	//private String rssFileToRead = "logs_ASCAMM_Supermarket_wifi_ble_RSS_avg.txt";
	//private String rssFileToRead = "logs_Casa_Batllo_static_10_m1_RSS_avg.txt";
	//private String rssFileToRead = "logs_Casa_Batllo_route_3_RSS_avg.txt";
	//private String rssFileToRead = "logs_Casa_Batllo_route_10_m2_RSS_avg.txt";
	//private String rssFileToRead = "logs_ASCAMM_lab_route_1_RSS_avg.txt";
	//private String rssFileToRead = "prova_CB_3.txt";
	//private String rssFileToRead = "logs_Renault_Eurecat_static_1_m1_RSS_avg.txt";
	//private String rssFileToRead = "logs_Renault_Eurecat_route5_m1_RSS_avg.txt";
	//private String rssFileToRead = "logs_Renault_Valladolid_ruta1_m3_RSS_avg.txt";
	//private String rssFileToRead = "logs_Renault_Valladolid2_static6_m2_RSS_avg.txt";
	//private String rssFileToRead = "logs_Alabama_m1_RSS_avg.txt";
	//private String rssFileToRead = "logs_smart_retail_static1_m2_RSS_avg.txt";
	//private String rssFileToRead = "logs_illa_static1_motog_m2_RSS_avg.txt";
	//private String rssFileToRead = "logs_illa_static1_huaweiALE_RSS_avg.txt";
	//private String rssFileToRead = "illaNegre_RSS_avg.txt";
	private String rssFileToRead = "logsMatriuP1Llarg_RSS_avg.txt";
	private long rssFromTs = 0;
	//private long rssFromTs = 1440070150136l;
	//private long rssFromTs = 1440070900136l;
	//private long rssFromTs = 1440071110143l;
	//private long rssFromTs = 1443003700013l;
	//private long rssFromTs = 1446555898680l;
	//private long rssFromTs = 1446555904990l;
	//private long rssFromTs = 1447322336294l;
	//private long rssFromTs = 1447255935729l; // Renault Eurecat
	//private long rssFromTs = 1443520845527l;	// Casa Batllo static 6 m2
	//private long rssFromTs = 1449140121071l;	// Casa Batllo static 10 m1
	private long rssToTs = (long) Math.pow(10, 15);
	//private long rssToTs = 1446555898689l;
	//private long rssToTs = 1446555904998l;
	//private long rssToTs = 1447322346294l;
	//private long rssToTs = 1447255935729l; // Renault Eurecat
	//private long rssToTs = 1443520845527l;	// Casa Batllo static 6 m2
	/************** End of RSS simulation variables **************/
	
	/************** Position simulation variables ************/
	private boolean usePosSimulation = false;
	private LinkedList<Point3D> simPositionsQueue;
	private int posIndex = 0, simPositionsQueueSize = 0;
	private String posFileToRead = "MWC_square_positions.txt";
	private long posFromTs = 0;
	private long posToTs = (long) Math.pow(10, 15);
	
	/************** End of Position simulation variables **************/
	
	/************* RSS simulation *************/
	private void createSimulatedScans(){
		LogReader mLogReader = new LogReader(Constants.APP_DATA_FOLDER_NAME, rssFileToRead);
		mLogReader.readFile(FileType.RSS_AVG, rssFromTs, rssToTs);
		simScansQueue = mLogReader.getAvgSimScansQueue();
		simScansQueueSize = simScansQueue.size();
	}
	/************* End of RSS simulation **********/
	
	/************* Position simulation *************/
	private void createSimulatedPositions(){
		LogReader mLogReader = new LogReader(Constants.APP_DATA_FOLDER_NAME, posFileToRead);
		mLogReader.readFile(FileType.POS, posFromTs, posToTs);
		simPositionsQueue = mLogReader.getSimPositionsQueue();
		simPositionsQueueSize = simPositionsQueue.size();
	}
	/************* End of Position simulation **********/
	
	/** Sensor variables **/
	private MotionMEMS mMotionMEMS;
	private int STATIC = Constants.STATIC;
	private int WALKING = Constants.WALKING;
	private int GOING_TO_STATIC = Constants.GOING_TO_STATIC;
	private int sensorState = STATIC;
	private int lastSensorState = STATIC;
	
	/** PDB downloading variables **/
	private boolean firstScanReceivedWifi = false;
	private boolean firstScanReceivedBle = false;
	private int siteId = 0;
	private boolean siteInfoNeeded = true;
	private boolean hasLocalVersionData = true;
	private boolean pdbOk = false;
	private boolean pdbDownloadOk = false;
	private boolean pdbNeedsUpdate = false;
	private int pdbVersion = 0;
	private String pdbUrl = null;
	private String pdbName = null;
	
	private UpdateAutocalibrationParametersListener mAutocalibrationListener = new UpdateAutocalibrationParametersListener() {
		@Override
		public void newUpdate(AutocalibrationParameters autoParams){
			// Execute handler to fire New Autocalibration Event
			Message msg = Message.obtain();
			msg.what = AUTOCALIBRATION_EVENT;
			msg.obj = autoParams;
			mHandlerLocal.sendMessage(msg);
		}
	};

	private MotionStepsInfoListener mMotionStepsInfoListener = new MotionStepsInfoListener() {
		@Override
		public void newMotionStepsInfoAvailable(int motionMode, ArrayList<Double> stepsList, double orientation, double diffOrientation){
			fireNewMotionStepsInfoEvent(motionMode, stepsList, orientation, diffOrientation);
		}
	};

	/**
	 * This handles the possible events after sending an HTTP request with the RSSI data to the server.
	 * Especially interested in handling the response message (estimated position) from the server
	 */
	Handler mHandler = new Handler() {
		public void handleMessage(Message message) {
			switch (message.what) {
			case HttpConnection.DID_START:
				break;
			case HttpConnection.DID_SUCCEED:
				String response = (String) message.obj;
				// Process the response from the server (the response must be the calculated position)
				/*
				mPosition = decodePosition(response);
				Log.d(TAG, mPosition.x + "," + mPosition.y + "," + mPosition.z);
				fireNewPositionEvent(mPosition);
				*/
				
				locationResponse = decodePositionAndNeighbors(response);
				/*
				mPositionLocal = locationResponse.getPositionLocal();
				mNeighborsLocal = locationResponse.getCandidatesLocal();
				mPositionGlobal = locationResponse.getPositionGlobal();
				mNeighborsGlobal = locationResponse.getCandidatesGlobal();
				Log.d(TAG, mPositionLocal.x + "," + mPositionLocal.y + "," + mPositionLocal.z);
				*/
				if(storeLogs && logsRunning){
					/** Code added to store data info into logs.txt **/
					long timestamp2 = System.currentTimeMillis();
					String ts2 = Long.toString(timestamp2);
					mLogWriter.addPositionLog(""+locationResponse.getPositionLocal().x, ""+locationResponse.getPositionLocal().y, ""+locationResponse.getPositionLocal().z, ""+locationResponse.getPositionGlobal().x, ""+locationResponse.getPositionGlobal().y, ""+locationResponse.getPositionGlobal().z, ts2);
				}
				fireNewPositionEvent(locationResponse);
				
				break;
			case HttpConnection.DID_ERROR:
				locationResponse = new ResponseObject(new Point3DWithFloor(-2,-2,-2,-2),null, new Point3D(-2,-2,-2),null);
				//mPositionLocal = new Point3D(-2,-2,-2);	//(-2,-2,-2) means not able to connect to the positioning server
				//Log.d(TAG, mPositionLocal.x + "," + mPositionLocal.y + "," + mPositionLocal.z);
				if(storeLogs && logsRunning){
					/** Code added to store data info into logs.txt **/
					long timestamp2 = System.currentTimeMillis();
					String ts2 = Long.toString(timestamp2);
					mLogWriter.addPositionLog(""+locationResponse.getPositionLocal().x, ""+locationResponse.getPositionLocal().y, ""+locationResponse.getPositionLocal().z, ""+locationResponse.getPositionGlobal().x, ""+locationResponse.getPositionGlobal().y, ""+locationResponse.getPositionGlobal().z, ts2);
				}
				fireNewPositionEvent(locationResponse);				
				/*
				Exception e = (Exception) message.obj;
				e.printStackTrace();
				*/
				break;
			}
		}
	};
	
	/**
	 * This handles multiple events like newPosition, newScanset, newMotion, newAutocalibration, newRoute, newSiteDetection
	 */
	Handler mHandlerLocal = new Handler() {
		
		public void handleMessage(Message msg) {
			// do stuff with UI
			int type = msg.what;
			if(type==POSITION_EVENT){
				//Log.d(TAG,"fireNewPositionEvent Local");
				//fireNewPositionEvent(locationResponse);
				// Line added to test RSS transformation
				ArrayList<ResponseObject> responseObjects = new ArrayList<ResponseObject>();
				Iterator<ResponseObject> iterator = locationResponses.iterator();
				while (iterator.hasNext()) {
					ResponseObject ro = iterator.next();
					responseObjects.add(ro);
				}
				// LQ: log debug
				Log.d(TAG, "mLocation : FIRE new POSITION");
				fireNewPositionsEvent(responseObjects);
				Log.d(TAG, "mLocation : DONE! FIRED new POSITION");
			}
			else if(type==SCANSET_EVENT){
				/*
				Bundle b = msg.getData();
				ArrayList<OnlineItemParcelable> mScanSet = b.getParcelableArrayList("online_items");
				Log.d(TAG,"mScanSet size: " + mScanSet.size());
				ArrayList<OnlineItem> scansetOI = new ArrayList<OnlineItem>();
				for (OnlineItemParcelable oip : mScanSet){
					OnlineItem oi = oip.getOnlineItem();
					Log.d(TAG,"OnlineItem: " + oi.toString());
					scansetOI.add(oi);
				}
				fireNewScansetEvent(scansetOI);
				*/
				ArrayList<OnlineItem> onlineItems = new ArrayList<OnlineItem>();
				Iterator<OnlineItem> iterator = scanset.iterator();
				while (iterator.hasNext()) {
					OnlineItem oi = iterator.next();
					onlineItems.add(oi);
				}
				fireNewScansetEvent(onlineItems);
			}
			else if(type==MOTION_EVENT){
				int valueArg1 = msg.arg1;
				fireNewMotionEvent(valueArg1);
			}
			else if(type==AUTOCALIBRATION_EVENT){
				AutocalibrationParameters autoParams = (AutocalibrationParameters) msg.obj;
				//Log.d(TAG, "newUpdate received. a: " + autoParams.getSlope() + " , b: " + autoParams.getIntersection() + " , y: " + autoParams.getResiduals());
				fireNewAutocalibrationEvent(autoParams);
			}
			else if(type == ROUTE_EVENT_CALCULATED) {
				fireNewRouteCalculated((Route) msg.obj);
			}
			else if(type == DETECT_SITE_EVENT) {
				if(siteId == 0){
					// Execute WS to detect site
					getSiteData();
				}
				else{
					// Execute WS to obtain pdb info
					getPdbInfo();
				}
			}
		}
	};
	
	Handler mHandlerToast = new Handler() {
		
		public void handleMessage(Message msg) {
			// do stuff with UI
			Bundle b = msg.getData();
			String messageText = b.getString("message");
			Toast.makeText(mContext, messageText, Toast.LENGTH_SHORT).show();
		}
	};
	
	Handler mHandlerSensorState = new Handler(); 

	public IndoorPosClient(Context context, Context UIcontext, WiFiService wifiService, BleService bleService) {
		mContext = context;
		mUIcontext = UIcontext;
		mLocalDBHelper = new LocalDBHelper(context);
		mScheduleTaskExecutor= Executors.newScheduledThreadPool(5);
		mScheduledFuture = null;
		
		String macAddress = "00:00";
		mDecoder = new JsonMessageDecoder();
		mEncoder = new JsonClientMessage(macAddress);		

		mWiFiService = wifiService;
		mBleService = bleService;
		calcPosLocal = Settings.posLocal;
		LNS_URL = Settings.lnsUrl + "/api/get_position_with_scans/";
		mSecondsOfScansToUse = Settings.timeWindow;
		mUseDetections = Settings.useDetections;
		if(calcPosLocal || mUseDetections){
			mPosEstimation = new PositionEstimation(mContext, this);
			mPosEstimation.addAutocalibrationListener(mAutocalibrationListener);
			mPosEstimation.addMotionStepsInfoListener(mMotionStepsInfoListener);
		}
		if(storeLogs){
			mLogWriter = new LogWriter(Constants.APP_DATA_FOLDER_NAME, logsFilename);
		}
				
		if(useRssSimulation){
			/******** Simulation ************/
			new Thread() {
				public void run() {
					loadingRssFile = true;
					Log.i(TAG,"Loading RSS File...");
					createSimulatedScans();
					loadingRssFile = false;
					Log.i(TAG,"RSS File Loaded");
				}
			}.start();
			/********************************/
		}
		
		if(usePosSimulation){
			/******** Simulation ************/
			new Thread() {
				public void run() {
					loadingPosFile = true;
					Log.i(TAG,"Loading POS File...");
					createSimulatedPositions();
					loadingPosFile = false;
					Log.i(TAG,"POS File Loaded");
				}
			}.start();
			/********************************/
		}
		
		if(Settings.useSensors){
			mMotionMEMS = new MotionMEMS(context);
		}
	}
	
	public void setUIcontext(Context context){
		mUIcontext = context;
	}

	private class calculateRouteThread extends Thread {
		
		public Point3D start;
		public Point3D end;
		
		public void run() {
			Route route = routeCalculation.calculateRoute(start, end);
			Message msg = new Message();
			msg.what = ROUTE_EVENT_CALCULATED;
			msg.obj = route;
			mHandlerLocal.sendMessage(msg);
		}
	};

	/**
	 * Start a thread which calculates a route between two points.
	 * It will fire an event when the calculation has finished
	 * 
	 * @param start start route point
	 * @param end end route point
	 */
	public void calculateRoute(Point3D start, Point3D end) {
		calculateRouteThread thread = new calculateRouteThread();
		thread.start = new Point3D();
		thread.start.x = start.x;
		thread.start.y = start.y;
		thread.start.z = start.z;
		
		thread.end = new Point3D();
		thread.end.x = end.x;
		thread.end.y = end.y;
		thread.end.z = end.z;
		
		thread.start();
	}

	// LQ: TESTING outdoor get outdoor APs
	public ArrayList<APData> getOudoorAPs() {
		return mPosEstimation.getOutdoorAPs(new ArrayList<OnlineItem>(scanset));
	}

	private void pdbProcessCompleted(){
		Log.d(TAG,"PDB process completed");
		if(mLocalDBHelper.isOpened()){
			mLocalDBHelper.close();
		}
		if(useWifi && mWiFiService!=null && !mWiFiService.isScanning()){
			// resume WiFi scanning (that was stopped to improve communications with server)
			mWiFiService.startScan();
		}
		if(useBle && mBleService!=null && !mBleService.isScanning()){
			// resume BLE scanning (that was stopped to improve communications with server)
			mBleService.startScan();
		}
		createDbHelper(siteId,pdbName,useWifi,useBle);
		pdbOk = true;
		
		//initialize routes calculation
		this.routeCalculation = new RouteCalculation(this.dBHelper);

		// This schedule a task (updatePosition) to run every calcPosTime seconds, with an initial delay of mSecondsOfScansToUse
		mScheduledFuture = mScheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
			public void run() {
				if(debug)	Log.d("mScheduleTaskExecutor", "start scheduled task: updatePosition");
				updatePosition();
				//Log.i(TAG,"Update position");
			}
		}, mSecondsOfScansToUse*1000, calcPosTime, TimeUnit.MILLISECONDS);
	}
	
	private void createDbHelper(int siteId, String pdbName, final boolean useWifi, final boolean useBle){
		if(debug)	Log.d(TAG,"Creating DBHelper ...");
		dBHelper = new DBHelper(mContext, siteId, pdbName, dbFromAssets);
		mPosEstimation.initDbHelper(dBHelper,useWifi,useBle);
		float pxMeter = dBHelper.getPxMeter();
		fireNewDbLoadedEvent(pxMeter);
	}
	
	private void destroyDbHelper(){
		if(dBHelper != null && !mPosEstimation.getLoadingDB()){
			dBHelper.closeDatabase();
		}
	}
	
	
	/**
	 * This method starts the indoor positioning doing the following:
	 * - It checks if WiFiService and BleService are running
	 * - It checks if PDB has to be downloaded, and if site detection has to be performed
	 * - If PDB does not need to be downloaded, it executes pdbProcessCompleted method
	 * - It starts WiFi and BLE scans
	 */
	public void startPositioning(int numOfMilliseconds, int siteId, boolean siteInfoManual) {
		if(debug)	Log.d(TAG,"inside IndoorPosClient -> startPositioning");
		if(!Settings.useParticleFilter) {
			// only set calcPosTime if not using Particle Filter
			calcPosTime = numOfMilliseconds;
		}
		mPosEstimation.TimeBetweenPositionEstimations = numOfMilliseconds; //for the Particle Filter
		useWifi = false;
		useBle = false;
		if(mWiFiService!=null){
			useWifi = true;
		}
		if(mBleService!=null){
			useBle = true;
		}
		this.siteId = siteId;
		mLocalDBHelper.open();
		// LQ: (temporary) simulation does not download PDB
		if(usePosSimulation){
			siteInfoNeeded = false;
			pdbName = null;
			pdbProcessCompleted();
		}
		else{
			if(siteInfoManual){
				siteInfoNeeded = false;
				pdbName = null;
				pdbProcessCompleted();
			}
			else{
				// TODO: check if siteid == 0 or not and do as in DETECT_SITE_EVENT event
				if(siteId != 0){
					getPdbInfo();
				}
				else {
					siteInfoNeeded = true;
					pdbOk = false;
					firstScanReceivedWifi = false;
					firstScanReceivedBle = false;
				}
			}
		}
		if(mWiFiService != null){
			mWiFiService.addWiFiNewScanListener(mWiFiListener);
			mWiFiService.setMotionMEMS(mMotionMEMS);
			mWiFiService.startScan();
		}
		if(mBleService != null){
			mBleService.addBleNewScanListener(mBleListener);
			mBleService.setMotionMEMS(mMotionMEMS);
			mBleService.startScan();
		}
		
		/** Code added to store data info into logs.txt **/
		if(storeLogs && mLogWriter.startLogs()>0){
			logsRunning = true;
		}
		
		if(mMotionMEMS != null){
			mMotionMEMS.Start();
		}
	}


	public void stopPositioning() {
		if(debug)	Log.d(TAG,"inside IndoorPosClient -> stopPositioning");
		if (mScheduledFuture != null) {
			//cancel execution of the future scheduled task
			//If task is already running, interrupt it here.
			boolean cancelled = mScheduledFuture.cancel(true);
			//Log.i(TAG,"mScheduledFuture cancelled: " + cancelled);
		}
			
		//stop WiFi scans and unsubscribe the WiFi listener
		if(mWiFiService != null){
			mWiFiService.stopScan();
			mWiFiService.removeWiFiNewScanListener(mWiFiListener);
		}
		//stop BLE scans and unsubscribe the BLE listener
		if(mBleService != null){
			mBleService.stopScan();
			mBleService.removeBleNewScanListener(mBleListener);
		}
		destroyDbHelper();
		mPosEstimation.stop();
		
		if(mMotionMEMS != null){
			mMotionMEMS.Stop();
		}
		
		/** Code added to store data info into logs.txt **/
		if(storeLogs && logsRunning){
			mLogWriter.stopLogs();
			logsRunning = false;
		}
	}
	
	/** This method is executed inside the timer, i.e. inside a thread. It does the following:
	 * - It checks if the user is walking
	 * - It calculates an average of the RSS measurements
	 * - If calcPosLocal is enabled, it calculates a position. If it is not enabled, it sends the RSS measurements to a server
	 * - It can also simulate RSS measurements or positions
	 * **/
	protected void updatePosition(){
		if(debug)	Log.d(TAG,"loadingRssFile: " + loadingRssFile + " , pdbOk: " + pdbOk);
		if(!loadingRssFile && !loadingPosFile && pdbOk){
			int secondsOfScans = mSecondsOfScansToUse;
			boolean sensorWalking = false;
			boolean wifiMeas = false;
			boolean bleMeas = false;
			ArrayList<OnlineItem> avgScansWifiBle = new ArrayList<OnlineItem>();
			ArrayList<OnlineItem> avgScansWifi = null;
			ArrayList<OnlineItem> avgScansBle = null;
			if(!useRssSimulation){
				if(mMotionMEMS != null){
	    			//get value of the variable walking
	    			sensorWalking = mMotionMEMS.getWalking();
	    			//sensorWalking = true;	// simulate that user is walking
	    			//sensorWalking = false;	// simulate that user is not walking
	    			secondsOfScans = getSecondsOfScansToUse(sensorWalking);
	    		}
				/********************* Real measurements *********************/
				// Obtain last measurements avg, merge all the scans into one array
				if(mWiFiService != null){
					if(debug)	Log.i(TAG,"Calculating WiFi AvgScanSet...");
					try{
						avgScansWifi = mWiFiService.calcAvgScanSet(secondsOfScans, sensorWalking);
						if(!avgScansWifi.isEmpty()){
							wifiMeas = true;
							for(OnlineItem oi : avgScansWifi){
								if(!filterOnlineItems || (filterOnlineItems && oi.getCount()>minCount && oi.getCountPercentage()>minPercentage)){
									OnlineItem oiTemp = new OnlineItem(oi.getSSID(), oi.getBSSID(), oi.getRSS(), oi.getRSSstdev(), oi.getCount());
									oiTemp.setCountPercentage(oi.getCountPercentage());
									oiTemp.setRssArray(oi.getRSSvalues());
									oiTemp.setTech("WiFi");
									avgScansWifiBle.add(oiTemp);
								}
							}
						}
					}catch(Exception e){
						Log.e(TAG,"Error calculating RSS avg with WiFi",e);
					}
					if(debug)	Log.i(TAG,"After Calculating WiFi AvgScanSet...");
				}
				if(mBleService != null){
					if(debug)	Log.i(TAG,"Calculating BLE AvgScanSet...");
					try{
						avgScansBle = mBleService.calcAvgScanSet(secondsOfScans, sensorWalking);
						if(!avgScansBle.isEmpty()){
							bleMeas = true;
							for(OnlineItem oi : avgScansBle){
								if(!filterOnlineItems || (filterOnlineItems && oi.getCount()>minCount && oi.getCountPercentage()>minPercentage)){
									OnlineItem oiTemp = new OnlineItem(oi.getSSID(), oi.getBSSID(), oi.getRSS(), oi.getRSSstdev(), oi.getCount());
									oiTemp.setCountPercentage(oi.getCountPercentage());
									oiTemp.setRssArray(oi.getRSSvalues());
									oiTemp.setTech("BLE");
									avgScansWifiBle.add(oiTemp);
								}
							}
						}
					}catch(Exception e){
						Log.e(TAG,"Error calculating RSS avg with BLE",e);
					}
					if(debug)	Log.i(TAG,"After Calculating BLE AvgScanSet...");
				}
				/*********************** End of real measurements ********************/
			}
			else{

				/*********************** Simulation *************************/
				if(scanIndex >= simScansQueueSize){
					scanIndex = 0;
					/*
					Log.i(TAG,"End of round. Starting again");
					Message msg = Message.obtain();
		    		Bundle b = new Bundle();
		    		b.putString("message", "End of round. Starting again");
		    		msg.setData(b);
		    		mHandlerToast.sendMessage(msg);
		    		*/
				}
				SimOnlineMeasure avgSimScans = simScansQueue.get(scanIndex);
				Log.i(TAG,"scanIndex: " + scanIndex + " , ts: " + avgSimScans.getTs());
				sensorWalking = avgSimScans.getWalking();
				//secondsOfScans = getSecondsOfScansToUse(sensorWalking);
				sensorState = avgSimScans.getSensorState();
				avgScansWifiBle = avgSimScans.getOnlineItems();
				avgScansWifi = new ArrayList<OnlineItem>();
				avgScansBle = new ArrayList<OnlineItem>();
				for(OnlineItem oi : avgScansWifiBle){
					if(!filterOnlineItems || (filterOnlineItems && oi.getCount()>minCount && oi.getCountPercentage()>minPercentage)){
						OnlineItem oiTemp = new OnlineItem(oi.getSSID(), oi.getBSSID(), oi.getRSS(), oi.getRSSstdev(), oi.getCount(), oi.getCountPercentage());
						String tech = oi.getTech();
						//Log.d(TAG,"tech: " + tech + " , ssid: " + oi.getSSID());
						if(tech==null){
							// old logs without tech info
							if(oi.getSSID()==null || oi.getSSID().equalsIgnoreCase("null")){
								oiTemp.setTech("BLE");
								avgScansBle.add(oiTemp);
							}
							else{
								oiTemp.setTech("WiFi");
								avgScansWifi.add(oiTemp);
							}
						}
						else{
							oiTemp.setTech(tech);
							if(tech.equalsIgnoreCase("wifi")){
								avgScansWifi.add(oiTemp);
							}
							else if(tech.equalsIgnoreCase("ble")){
								avgScansBle.add(oiTemp);
							}
							else{
								Log.e(TAG,"No tech detected");
							}
						}
					}
				}
				if(mWiFiService != null){
					wifiMeas = true;
				}
				if(mBleService != null){
					bleMeas = true;
				}
				Log.d(TAG,"wifiMeas: " + wifiMeas + " , bleMeas: " + bleMeas + " , numWifiMeas: " + avgScansWifi.size() + " , numBleMeas: " + avgScansBle.size());
				scanIndex++;
				/*********************** End of simulation *******************/
			}
			if(sensorState!=lastSensorState){
				// update the value of the sensor variable
				lastSensorState = sensorState;
    			// Execute handler to fire New Motion Event
    			Message msg3 = Message.obtain();
    			msg3.what = MOTION_EVENT;
    			msg3.arg1 = sensorState;
    			mHandlerLocal.sendMessage(msg3);
			}
			/** Code added to store data info into logs.txt **/
			if(storeLogs && logsRunning){
				long ts = System.currentTimeMillis();
				/*
				String ts = Long.toString(timestamp);
				mLogger.addArrayMeasuresLog(avgScansWifiBle, ts);
				*/
				mLogWriter.addArrayAverageMeasuresLog(ts, avgScansWifiBle, sensorWalking, sensorState, secondsOfScans);
			}
			// Copy the average scans, send message to the handler and fireNewScansetEvent
			scanset.clear();
			for(OnlineItem oi : avgScansWifiBle){
				//Log.i(TAG,"ssid: " + oi.getSSID() + " , mac: " + oi.getBSSID() + " , rss: " + oi.getRSS() + " , stdev: " + oi.getRSSstdev() + " , tech: " + oi.getTech() + " , count: " + oi.getCount() + " , count(%): " + oi.getCountPercentage());
				scanset.add(oi);
			}
			// Sort APs by RSS
			Collections.sort(scanset, new SortByRSS());
			// Execute handler to fire New Scanset Event
			Message msg2 = Message.obtain();
			msg2.what = SCANSET_EVENT;
			mHandlerLocal.sendMessage(msg2);
			
			if(usePosSimulation){
				/*********************** Simulation *************************/
				if(posIndex >= simPositionsQueueSize){
					posIndex = 0;
					
					Log.i(TAG,"End of round. Starting again");
					Message msg = Message.obtain();
		    		Bundle b = new Bundle();
		    		b.putString("message", "End of round. Starting again");
		    		msg.setData(b);
		    		mHandlerToast.sendMessage(msg);
		    		
				}
				Point3D pos = simPositionsQueue.get(posIndex);
				Point3DWithFloor posLocal = new Point3DWithFloor(pos.x, pos.y, pos.z, pos.z);
				Point3D posGlobal = mPosEstimation.convertLocalToGlobal(pos);
				Log.i(TAG,"posIndex: " + posIndex);
				//Log.i(TAG,"posLocal: " + pos.toString() + " , posGlobal: " + posGlobal.toString());
				posIndex++;
				/*********************** End of simulation *******************/
				String tech = getTech(wifiMeas, bleMeas);
				ResponseObject resultRssDiff = new ResponseObject(posLocal,null,posGlobal,null);
				resultRssDiff.setTech(tech);
				resultRssDiff.setSiteId(siteId);
				ResponseObject resultNoRssDiff = new ResponseObject(new Point3DWithFloor(),null,new Point3D(),null);
				resultNoRssDiff.setTech(tech);
				resultNoRssDiff.setSiteId(siteId);
				// Copy the positions, send message to the handler and fireNewPositionEvent
				locationResponses.clear();
				locationResponses.add(resultRssDiff);
				locationResponses.add(resultNoRssDiff);
				// Execute handler to fire New Position Event
				Message msg = Message.obtain();
				msg.what = POSITION_EVENT;
        		mHandlerLocal.sendMessage(msg);
			}
			else{
				if(calcPosLocal){
					//calculate a position
					//Log.d(TAG, "inside service Positioning API: just before calculating position with local algorithm");
					try{
						boolean walking = true;
						if(sensorState==STATIC){
							walking = false;
						}
						ArrayList<ResponseObject> responses = mPosEstimation.calcPosition(wifiMeas, avgScansWifi, bleMeas, avgScansBle, walking);
						//Log.d(TAG, "after calculating position with local algorithm. pos: (" + responses.get(0).getPositionLocal().x + " , " + responses.get(0).getPositionLocal().y + " , " + responses.get(0).getPositionLocal().z + ")");
						if((responses.get(0).getPositionLocal().x) !=-5 && (responses.get(0).getPositionLocal().y !=-5) && (responses.get(0).getPositionLocal().z !=-5)){
							if(storeLogs && logsRunning){
								/** Code added to store data info into logs.txt **
								long timestamp2 = System.currentTimeMillis();
								String ts2 = Long.toString(timestamp2);
								mLogWriter.addPositionLog(""+responses.get(0).getPositionLocal().x, ""+responses.get(0).getPositionLocal().y, ""+responses.get(0).getPositionLocal().z, "0", "0", "0", ts2);
								/*************************************************/
							}
							// Copy the positions, send message to the handler and fireNewPositionEvent
							locationResponses.clear();
							for(ResponseObject ro : responses){
								//OnlineItem newOIP = new OnlineItem(oi);
								String tech = getTech(wifiMeas, bleMeas);
								ro.setTech(tech);
								ro.setSiteId(siteId);
								locationResponses.add(ro);
							}
							// Execute handler to fire New Position Event
							Message msg = Message.obtain();
							msg.what = POSITION_EVENT;
			        		mHandlerLocal.sendMessage(msg);
						}
					}catch(Exception e){
						// TOAST to see when exception is raised
						Log.e(TAG,"calcPosition exception!!!");
						e.printStackTrace();
					}
					
				}else{
					// send measurements to a server, which will calculate a position
					String strMeasureJson = encodeRssiMeasurements(avgScansWifiBle);
					//Log.d(TAG, strMeasureJson);
										
					//Toast.makeText(mContext, "inside service Positioning Dipacemo: just before sending RSSI measurements", Toast.LENGTH_SHORT).show();
					Log.d(TAG, "inside service Positioning API: just before sending RSSI measurements to " + LNS_URL);
					new HttpConnection(mHandler).post(LNS_URL, strMeasureJson);
					Log.d(TAG, "--- HttpConnection.post to send RSSI to server has been done ---");
				}
			}
		}	
	}

	// calculates the seconds of scans to use depending on user's movement
	private int getSecondsOfScansToUse(boolean sensorWalking){
		int secondsOfScans = mSecondsOfScansToUse;
		if(sensorState==STATIC){
			if(sensorWalking){
				secondsOfScans = secondsOfScansToUseWalking;
				sensorState = WALKING;
			}
			else{
				secondsOfScans = mSecondsOfScansToUse;
			}
		}
		else if(sensorState==WALKING){
			if(sensorWalking){
				secondsOfScans = secondsOfScansToUseWalking;
			}
			else{
				secondsOfScans = secondsOfScansToUseGoingToStatic;
				sensorState = GOING_TO_STATIC;
				// SLEEP X SECONDS AND CHECK STATE OF SENSOR
				mHandlerSensorState.postDelayed(new Runnable() { 
			         public void run() { 
			        	 if(sensorState==GOING_TO_STATIC){
			        		 sensorState=STATIC;
			        	 }
			         } 
			    }, secondsOfScansToUseGoingToStatic*1000);
			    
			}
		}
		else if(sensorState==GOING_TO_STATIC){
			if(sensorWalking){
				secondsOfScans = secondsOfScansToUseWalking;
				sensorState = WALKING;
				mHandlerSensorState.removeCallbacksAndMessages(null);
			}
			else{
				secondsOfScans = secondsOfScansToUseGoingToStatic;
			}
		}
		return secondsOfScans;
	}
	
	private String getTech(boolean wifi, boolean ble){
		String tech = null;
		if(wifi && ble){
			tech = "wifi_ble";
		}
		else if(wifi){
			tech = "wifi";
		}
		else if(ble){
			tech = "ble";
		}
		else{
			tech = "sim";
		}
		return tech;
	}
	
	private boolean checkFirstScans(){
		boolean firstScansReceived = false;
		if(useBle && useWifi){
			if(firstScanReceivedWifi && firstScanReceivedBle){
				firstScansReceived = true;
			}
		}else if(useWifi){
			if(firstScanReceivedWifi){
				firstScansReceived = true;
			}
		}else if(useBle){
			if(firstScanReceivedBle){
				firstScansReceived = true;
			}
		}
		//Log.d(TAG,"inside checkFirstScans. firstScansReceived: " + firstScansReceived);
		return firstScansReceived;
	}
	
	private void trySiteDetection(){
		//Log.d(TAG,"inside trySiteDetection");
		if(siteInfoNeeded){
			if(checkFirstScans()){
				// obtain site data
				siteInfoNeeded = false;
				detectSite();
			}
		}
	}
	
	private void detectSite(){
		//Log.d(TAG,"inside detectSite");
		if(useWifi && mWiFiService!=null && mWiFiService.isScanning()){
			// stop scanning to improve communication with server
			mWiFiService.stopScan();
		}
		if(useBle && mBleService!=null && mBleService.isScanning()){
			// stop scanning to improve communication with server
			mBleService.stopScan();
		}
		// SLEEP 1 SECOND AND TRY TO STOP SCAN AGAIN 
		mHandlerLocal.postDelayed(new Runnable() { 
	         public void run() { 
	        	Message msg = new Message();
	 			msg.what = DETECT_SITE_EVENT;
	 			mHandlerLocal.sendMessage(msg);
	         } 
	    }, 1000);
	}
	
	private WiFiListener mWiFiListener = new WiFiListener() {
		@Override
		public void newScanAvaiable() {
			//if(debug)	Log.d(TAG,"newWifiScan. TS: " + System.currentTimeMillis());
			if(mUseDetections){
				performDetection(WIFI_DETECTION);
			}

			if(!firstScanReceivedWifi){
				if(!mWiFiService.getLastScanSet().isEmpty()){
					firstScanReceivedWifi = true;
				}
			}
			
			trySiteDetection();
		}
	};
	
	private BleListener mBleListener = new BleListener() {
		@Override
		public void newBleScanAvailable(int numDevices) {
			//if(debug)	Log.d(TAG,"newBleScan. TS: " + System.currentTimeMillis());
			if(numDevices==0){
				if(debug)	Toast.makeText(mContext,"No BLE devices found", Toast.LENGTH_SHORT).show();
			}
			if(mUseDetections){
				performDetection(BLE_DETECTION);
			}

			if(!firstScanReceivedBle){
				if(!mBleService.getLastScanSet().isEmpty()){
					firstScanReceivedBle = true;
				}
			}
			if(siteId == 0){
				trySiteDetection();
			}
		}
	};
	
	private void performDetection(int type){
		ArrayList<OnlineItem> measures = null;
		if(type==WIFI_DETECTION && mWiFiService!=null){
			measures = mWiFiService.getLastScanSet();
		}
		else if(type==BLE_DETECTION && mBleService!=null){
			measures = mBleService.getLastScanSet();
		}
		if(measures==null){
			Log.e(TAG,"performDetection. Something strange ocurred");
		}
		else{
			if(measures.isEmpty()){
				if(debug)	Log.i(TAG,"performDetection. There are no measures");
			}
			else{
				// Sort measures by RSS
				Collections.sort(measures, new SortOiByRSS());
				ArrayList<Detection> detections = new ArrayList<Detection>();
				float mRssDetection = mBleRssDetection;
				if(type==WIFI_DETECTION){
					mRssDetection = mWifiRssDetection;
				}
				for(OnlineItem oi : measures){
					if(oi.getRSS()>mRssDetection){
						//Item Detected. Obtain position and add it to the list
						Detection det = mPosEstimation.getAPposition(oi.getBSSID(),oi.getRSS());
						if(det==null){
							det = new Detection(oi.getBSSID(),oi.getRSS(),null,null);
						}
						detections.add(det);
					}
					else{
						break;
					}
				}
				// option 1: Fire event when there is at least one detection above mRssDetection
				/*
				if(!detections.isEmpty()){
					fireNewDetectionEvent(detections, siteId);
				}
				*/
				// option 2: Fire event always
				fireNewDetectionEvent(detections, siteId);
			}
		}
	}
	
	/*nested class for sorting OnlineItem objects by RSS IN DESC ORDER (signs of the 'one' changed with respect to ASCENDENT order)*/
	public class SortByRSS implements Comparator<OnlineItem>{
		public int compare(OnlineItem arg0, OnlineItem arg1) {
			if ( arg0.getRSS() < arg1.getRSS()){
				return 1;
			} else if ( arg0.getRSS() == arg1.getRSS() ){
				return 0;	
			} else if ( arg0.getRSS() > arg1.getRSS() ){
				return -1;
			}
			return 0;
		}
	}
	
	private void getSiteData() {
		ArrayList<OnlineItem> lastScansWifi = null;
		ArrayList<OnlineItem> lastScansBle = null;
		if(mWiFiService != null){
			lastScansWifi = mWiFiService.getLastScanSet();
		}
		if(mBleService != null){
			lastScansBle = mBleService.getLastScanSet();
		}
		WebServiceAT wsAT = new WebServiceAT(mUIcontext, new WebServiceOnCompleteListener(), WS_URLS.LOOKUP_SITE, RequestMethod.POST);
		try {
			JSONObject json = new JSONObject();
			// Add WiFi measures
			JSONArray measuresWifiJSON = new JSONArray();
			if(lastScansWifi != null){
	    		for(OnlineItem oi : lastScansWifi){
	    			JSONObject jsonObj = new JSONObject();
	    			//if(debug)	Log.d(TAG,"id: " + oi.getBSSID());
	    			jsonObj.put("id", oi.getBSSID());
	    			jsonObj.put("rss", oi.getRSS());
	    			measuresWifiJSON.put(jsonObj);
	    		}
			}
        	json.put("measures_wifi", measuresWifiJSON);
        	// Add BLE measures
			JSONArray measuresBleJSON = new JSONArray();
			if(lastScansBle != null){
	    		for(OnlineItem oi : lastScansBle){
	    			JSONObject jsonObj = new JSONObject();
	    			if(debug)	Log.d(TAG,"id: " + oi.getBSSID());
	    			jsonObj.put("id", oi.getBSSID());
	    			jsonObj.put("rss", oi.getRSS());
	    			measuresBleJSON.put(jsonObj);
	    		}
			}
        	json.put("measures_ble", measuresBleJSON);
    	    //wsAT.addHeader("TOKEN", "ascamm");
			wsAT.AddDialog();
			wsAT.setDialogMessage("Obtaining Site Data...");
			wsAT.AddName("site_data");
			wsAT.addJSON(json);
			wsAT.execute();
		} catch (Exception e) {
			Log.e("WebService AT", "Error!", e);
		}
	}
	
	private void getPdbInfo() {
		WebServiceAT wsAT = new WebServiceAT(mUIcontext, new WebServiceOnCompleteListener(), WS_URLS.PDB_INFO, RequestMethod.POST);
		try {
			JSONObject json = new JSONObject();
			json.put("site_id", siteId);
			//wsAT.addHeader("TOKEN", "ascamm");
			wsAT.AddDialog();
			wsAT.setDialogMessage("Obtaining PDB Data...");
			wsAT.AddName("pdb_info");
			wsAT.addJSON(json);
			wsAT.execute();
		} catch (Exception e) {
			Log.e("WebService AT", "Error!", e);
		}
	}
	
	private int obtainLocalVersion() {
		return mLocalDBHelper.getVersion(siteId);
	}

	private void compareVersions() {
		pdbNeedsUpdate = true;
		int version = obtainLocalVersion();
		if (version == -1) {
			// It is the first time that we use the site. Download pdb data
			hasLocalVersionData = false;
		} else {
			hasLocalVersionData = true;
			if(debug){
				Log.i(TAG,"Local Version: " + version);
				Log.i(TAG, "Remote Version: " + pdbVersion);
			}
			if (version == pdbVersion) {
				pdbNeedsUpdate = false;
			}
		}
		if (pdbNeedsUpdate) {
			// Local version is not the same as the remote version
			downloadPdb();
		}
		else{
			// Local version is the same as the remote version
			pdbName = mLocalDBHelper.getPdbName(siteId);
			pdbProcessCompleted();
		}
	}
	
	private void downloadPdb() {
		DownloadFileWithProgressAT fileAT = new DownloadFileWithProgressAT(mUIcontext,
				new DownloadFileOnCompleteListener());
		try {
			fileAT.AddDialog();
			fileAT.setDialogMessage("Downloading pdb...");
			fileAT.setSiteId(siteId);
			fileAT.execute(pdbUrl);
		} catch (Exception e) {
			Log.e("DownloadImage AT", "Error!", e);
		}
	}
	
	private void checkDownloadFinished() {
		if(pdbDownloadOk){
			// Update DB with new pdb file and version
			SaveDataAT sdAT = new SaveDataAT(mUIcontext);
			sdAT.AddDialog();
			sdAT.execute();
		}
		else{
			showDialogMessage("Site Data could not be downloaded. Please try again later");
			stopPositioning();
		}
	}
	
	private void showDialogMessage(String msgTxt) {
		if(mUIcontext != null){
			AlertDialog.Builder builder = new AlertDialog.Builder(mUIcontext);
			builder.setMessage(msgTxt);
			builder.setPositiveButton("Close",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// YES button clicked
							dialog.dismiss();
						}
					});
			AlertDialog alert = builder.create();
			alert.show();
		}
	}
	
	public class WebServiceOnCompleteListener implements WebServiceCompleteListener<String> {

		private String ws_res = null;
		
		@Override
		public void onTaskComplete(WebServiceResult result) {
			// do something with the response of the Web Service
			ws_res = result.getResponse();
			try {
				int statusCode = result.getStatus();
				if (statusCode == 0 && ws_res == null) {
					if (result.getName().equalsIgnoreCase("site_data")) {
						stopPositioning();
						String txtMsg = "Site data could not be obtained. Try again later";
						showDialogMessage(txtMsg);
					} else if (result.getName().equalsIgnoreCase("pdb_info")) {
						int version = obtainLocalVersion();
						if (version == -1) {
							// It is the first time that we use the site. Download all the data
							String txtMsg = "Site data could not be obtained. Try again later";
							showDialogMessage(txtMsg);
							stopPositioning();
						} else {
							// It has previous versions. Continue with last local version
							if(debug){
								Log.i(TAG,"Local Version: " + version);
							}
							String txtMsg = "Site data could not be obtained. Working in offline mode";
							showDialogMessage(txtMsg);
							pdbName = mLocalDBHelper.getPdbName(siteId);
							pdbProcessCompleted();
						}
					}
				} else if (statusCode == 200) {
					if (result.getName().equalsIgnoreCase("site_data")) {
						JSONObject jsonObject = new JSONObject(ws_res);
						siteId = jsonObject.getInt("site_id");
						if(siteId==-1){
							// No site was detected
							stopPositioning();
							String txtMsg = "No Site was detected. Try again later";
							showDialogMessage(txtMsg);
						}
						else{
							pdbVersion = jsonObject.getInt("version");
							pdbUrl = jsonObject.getString("filename");
							if(pdbVersion>0){
								// Compare local version with remote version
								compareVersions();
							}
							else{
								stopPositioning();
								String txtMsg = "Site data not generated yet. Try again later";
								showDialogMessage(txtMsg);
							}
						}
					} else if (result.getName().equalsIgnoreCase("pdb_info")) {
						JSONObject jsonObject = new JSONObject(ws_res);
						pdbVersion = jsonObject.getInt("version");
						pdbUrl = jsonObject.getString("filename");
						if(pdbVersion>0){
							// Compare local version with remote version
							compareVersions();
						}
						else{
							stopPositioning();
							String txtMsg = "Site data not generated yet. Try again later";
							showDialogMessage(txtMsg);
						}
					} else {
						Log.e(TAG, "Unknown WS name");
					}
				} else {
					String txtMsg = "Error. Status: " + statusCode;
					showDialogMessage(txtMsg);
					stopPositioning();
				}
			} catch (Exception e) {
				Log.e("WebService AT", "Parsing JSON Error!", e);
			}
		}
	}
	
	public class DownloadFileOnCompleteListener implements DownloadFileCompleteListener<String> {

		@Override
		public void onTaskComplete(DownloadFileResult result) {
			pdbName = result.getFilename();
			if (pdbName == null) {
				pdbDownloadOk = false;
				Toast.makeText(mContext,"pdb " + result.getId() + " could not be downloaded", Toast.LENGTH_LONG).show();
			}
			else{
				pdbDownloadOk = true;
			}
			checkDownloadFinished();
		}
	}

	// AsyncTask that saves PDB related data into local database
	private class SaveDataAT extends AsyncTask<Void, Void, Integer> {

		private ProgressDialog dialog;
		private Context context;
		private Boolean use_dialog;
		private String message;

		public SaveDataAT(Context context) {
			this.context = context;
			this.use_dialog = false;
		}

		public void AddDialog() {
			if(context!=null){
				use_dialog = true;
			}
		}

		public void setDialogMessage(String message) {
			this.message = message;
		}

		@Override
		protected void onPreExecute() {
			if (use_dialog) {
				super.onPreExecute();
				dialog = new ProgressDialog(context);
				if (message != null) {
					dialog.setMessage(message);
				} else {
					dialog.setMessage("Saving Data...");
				}
				// dialog.setTitle("Title");
				dialog.setIndeterminate(true);
				dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				dialog.setCancelable(true);
				dialog.show();
			}
		}

		@Override
		protected Integer doInBackground(Void... values) {

			int problemsSaving = 0;
			
			if (hasLocalVersionData) {
				// update version data in versions table
				int updateOk = mLocalDBHelper.updatePDB(siteId, pdbVersion, pdbName);
				if (updateOk > 0) {
					if(debug)	Log.i(TAG, "PDB version info updated");
				} else {
					if(debug)	Log.i(TAG, "Problems updating PDB version info");
					problemsSaving = 1;
				}
			} else {
				// create a new register in versions table
				long id = mLocalDBHelper.insertPDB(siteId, pdbVersion, pdbName);
				if (id != -1) {
					if(debug)	Log.i(TAG, "PDB version info created");
				} else {
					if(debug)	Log.i(TAG, "Problems creating PDB version info");
					problemsSaving = 2;
				}
			}

			return problemsSaving;
		}

		@Override
		protected void onPostExecute(Integer problemsSaving) {
			if (problemsSaving == 2) {
				String txtMsg = "Error. PDB version data could not be created. Please try again";
				showDialogMessage(txtMsg);
				stopPositioning();
			} else if (problemsSaving == 1) {
				String txtMsg = "PDB version data could not be updated. Working in offline mode";
				showDialogMessage(txtMsg);
				//Toast.makeText(context, txtMsg, Toast.LENGTH_LONG).show();
				pdbName = mLocalDBHelper.getPdbName(siteId);
				pdbProcessCompleted();
			} else {
				pdbProcessCompleted();
			}
			if (use_dialog) {
				if (dialog.isShowing()) {
					dialog.dismiss();
				}
			}
		}

	}

	
	
	

	/**
	 * Method that encodes (JSON) the RSSI avg measurements to be sent to the server
	 */
	private String encodeRssiMeasurements(ArrayList<OnlineItem> avgScanSets){
		Hashtable<String, String> measures = new Hashtable<String, String>();
		for (OnlineItem scan : avgScanSets){
			measures.put(scan.getBSSID(), String.valueOf(scan.getRSS()));
		}
		String message_out = mEncoder.scanData(measures, null, "RSSI_Fingerprinting");
		return message_out;
	}

	/**
	 * Method that decodes (JSON) the calculated position received from the server
	 */
	/*
	private Point3D decodePosition(String dataPos){
		Hashtable<String, String> measures = new Hashtable<String, String>();
		double position[] = new double[3];
		String pos_tech = new String();
		ArrayList<Point3D> candidates = new ArrayList<Point3D>();
		mDecoder.JsonDecodeMessageData(dataPos, measures, pos_tech, position, candidates);
		return new Point3D(position[0],position[1],position[2]);	
	}
	*/
	
	/**
	 * Method that decodes (JSON) the calculated position and the involved calibration points received from the server
	 */
	private ResponseObject decodePositionAndNeighbors(String dataPos){
		Hashtable<String, String> measures = new Hashtable<String, String>();
		String pos_tech = new String();
		double positionLocal[] = new double[3];
		ArrayList<Point3DWithFloor> candidatesLocal = new ArrayList<>();
		double positionGlobal[] = new double[3];
		ArrayList<Point3D> candidatesGlobal = new ArrayList<Point3D>();
		mDecoder.JsonDecodeMessageData(dataPos, measures, pos_tech, positionLocal, candidatesLocal, positionGlobal, candidatesGlobal);
		//return new Point3D(position[0],position[1],position[2]);	
		Point3DWithFloor posLocal = new Point3DWithFloor(positionLocal[0],positionLocal[1],positionLocal[2],positionLocal[2]);
		Point3D posGlobal = new Point3D(positionGlobal[0],positionGlobal[1],positionGlobal[2]);
		return new ResponseObject(posLocal,candidatesLocal, posGlobal,candidatesGlobal);
	}
	
	/* event listeners corresponding to position*/
	public synchronized void addPositionListener(IndoorPositionListener listener) {
		mPositionListeners.add(listener);
	}

	public synchronized void removePositionListener(IndoorPositionListener listener) {
		mPositionListeners.remove(listener);
	}

	private synchronized void fireNewPositionEvent(ResponseObject locationResponse){
		for(IndoorPositionListener listener : mPositionListeners){
			listener.newPositionAvailable(locationResponse);
		}
	}
	
	/** Added to test RSS transformation **/
	public synchronized void addPositionsListener(IndoorPositionsListener listener) {
		mPositionsListeners.add(listener);
	}

	public synchronized void removePositionsListener(IndoorPositionsListener listener) {
		mPositionsListeners.remove(listener);
	}

	private synchronized void fireNewPositionsEvent(ArrayList<ResponseObject> locationResponses){
		for(IndoorPositionsListener listener : mPositionsListeners){
			// LQ: print listener
			Log.d(TAG, "mlocation LISTENER " + listener.toString());
			listener.newPositionAvailable(locationResponses);
			Log.d(TAG, "mlocation LISTENER new pos available OK");
		}
	}
	/****************************************/
	
	/* event listeners corresponding to RSS transformation */
	public synchronized void addAutocalibrationListener(UpdateAutocalibrationParametersListener listener) {
		mAutocalibrationParametersListeners.add(listener);
	}

	public synchronized void removeAutocalibrationListener(UpdateAutocalibrationParametersListener listener) {
		mAutocalibrationParametersListeners.remove(listener);
	}

	private synchronized void fireNewAutocalibrationEvent(AutocalibrationParameters autoParams){
		for(UpdateAutocalibrationParametersListener listener : mAutocalibrationParametersListeners){
			listener.newUpdate(autoParams);
		}
	}
	
	/* event listeners corresponding to detection*/
	public synchronized void addDetectionListener(DetectionListener listener) {
		mDetectionListeners.add(listener);
	}

	public synchronized void removeDetectionListener(DetectionListener listener) {
		mDetectionListeners.remove(listener);
	}

	private synchronized void fireNewDetectionEvent(ArrayList<Detection> detections, int siteId){
		for(DetectionListener listener : mDetectionListeners){
			listener.newDetectionAvailable(detections, siteId);
		}
	}
	
	/* event listeners corresponding to scansets*/
	public synchronized void addScansetListener(ScansetListener listener) {
		mScansetListeners.add(listener);
	}

	public synchronized void removeScansetListener(ScansetListener listener) {
		mScansetListeners.remove(listener);
	}

	private synchronized void fireNewScansetEvent(ArrayList<OnlineItem> scanset){
		for(ScansetListener listener : mScansetListeners){
			listener.newScansetAvailable(scanset);
		}
	}
	
	/* event listeners corresponding to sensors*/
	public synchronized void addMotionListener(MotionListener listener) {
		mMotionListeners.add(listener);
	}

	public synchronized void removeMotionListener(MotionListener listener) {
		mMotionListeners.remove(listener);
	}

	private synchronized void fireNewMotionEvent(int sensorState){
		for(MotionListener listener : mMotionListeners){
			listener.newMotionAvailable(sensorState);
		}
	}
	
	// event listeners corresponding to route calculating
	public synchronized void addRouteListener(RouteListener listener) {
		this.mRouteListeners.add(listener);
	}
	
	public synchronized void removeRouteListener(RouteListener listener) {
		this.mRouteListeners.remove(listener);
	}
	
	private synchronized void fireNewRouteCalculated(Route route) {
		for(RouteListener listener: mRouteListeners) {
			listener.newRouteCalculated(route);
		}
	}
	
	/* event listeners corresponding to DB loaded*/
	public synchronized void addDbLoadedListener(DbListener listener) {
		mDbListeners.add(listener);
	}

	public synchronized void removeDbLoadedListener(DbListener listener) {
		mMotionListeners.remove(listener);
	}

	private synchronized void fireNewDbLoadedEvent(float pxMeter){
		for(DbListener listener : mDbListeners){
			listener.dbLoaded(pxMeter);
		}
	}

	/* event listeners corresponding to motion modes */
	public synchronized void addMotionStepsInfoListener(MotionStepsInfoListener listener) {
		mMotionStepsInfoListeners.add(listener);
	}

	public synchronized void removeMotionStepsInfoListener(MotionStepsInfoListener listener) {
		mMotionStepsInfoListeners.remove(listener);
	}

	private synchronized void fireNewMotionStepsInfoEvent(int sensorState, ArrayList<Double> stepsList, double orientation, double diffOrientation){
		for(MotionStepsInfoListener listener : mMotionStepsInfoListeners){
			listener.newMotionStepsInfoAvailable(sensorState, stepsList, orientation, diffOrientation);
		}
	}
	
	/*nested class for sorting OnlineItem objects by RSS IN DESC ORDER (signs of the 'one' changed with respect to ASCENDENT order)*/
	public class SortOiByRSS implements Comparator<OnlineItem>{
		public int compare(OnlineItem arg0, OnlineItem arg1) {
			if ( arg0.getRSS() < arg1.getRSS()){
				return 1;
			} else if ( arg0.getRSS() == arg1.getRSS() ){
				return 0;	
			} else if ( arg0.getRSS() > arg1.getRSS() ){
				return -1;
			}
			return 0;
		}
	}

	public List<Integer> getFloorList() {

		if(this.dBHelper != null) {
			return this.dBHelper.getFloorList();
		} else {
			return null;
		}

	}

	public Point3D convertFromGlobalToLocal(LatLng3D point) {

		Point3D convPoint = null;

		if(this.dBHelper != null) {
			CoordConversion convClass = this.dBHelper.getConversionClass();
			if(convClass != null) {
				convPoint = convClass.convertFromGlobalToLocal(point);
			}
		}
		return convPoint;
	}

	public LatLng3D convertFromLocalToGlobal(Point3D point) {

		LatLng3D convPoint = null;

		if(this.dBHelper != null) {
			convPoint = this.dBHelper.getConversionClass().convertFromLocalToGlobal(point);
		}
		return convPoint;
	}

}
