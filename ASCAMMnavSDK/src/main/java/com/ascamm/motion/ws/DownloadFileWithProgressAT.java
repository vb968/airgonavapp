package com.ascamm.motion.ws;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.ascamm.motion.utils.Constants;

public class DownloadFileWithProgressAT extends AsyncTask<String, Integer, DownloadFileResult>{

	private String TAG = getClass().getSimpleName();
	
	private ProgressDialog dialog;
	private Context context;
	private DownloadFileCompleteListener<String> listener;
	private DownloadFileResult res;
	private int idSite;
	private boolean use_dialog;
	private String message;
			
	public DownloadFileWithProgressAT(Context context, DownloadFileCompleteListener<String> listener){
		this.context = context;
		this.listener = listener;
	}
	
	public void AddDialog(){
		if(context!=null){
			use_dialog = true;
		}
    }
	
	public void setDialogMessage(String message){
		this.message = message; 
	}
		
	public void setSiteId(int id){
		this.idSite = id;
	}
	
	@Override
    protected void onPreExecute() {
		super.onPreExecute();
		if(use_dialog){
			super.onPreExecute();
	        dialog = new ProgressDialog(context);
			if(message!=null){
				dialog.setMessage(message);
			}
			else{
				dialog.setMessage("Downloading file...");
			}
	        //dialog.setTitle("Title");
			dialog.setIndeterminate(false);
			dialog.setMax(100);
			dialog.setProgressNumberFormat("%1d/%2d KB");
	        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	        dialog.setCancelable(true);
	    	dialog.show();
		}
    }
		
	@Override
    protected DownloadFileResult doInBackground(String... urls)
    {
        String filename = null;
        if(urls[0]!=null){
	        String ext = urls[0].substring((urls[0].lastIndexOf(".")), urls[0].length());
	        //System.out.println("ext: " + ext);
	        checkFolderStructure();
	        filename = "pdb_site_" + idSite + ext;
	        boolean downloadOk = downloadFile(urls[0], filename);
	        if(!downloadOk){
	        	filename = null;
	        }
        }
        
        res = new DownloadFileResult(idSite, filename);
        return res;
    }
	
	@Override
	protected void onProgressUpdate(Integer... progress) {
		if(use_dialog){
			dialog.setProgress(progress[0]);
		}
    }
	
	@Override
    protected void onPostExecute(DownloadFileResult result) {
		listener.onTaskComplete(result);
		if(use_dialog){
			if (dialog.isShowing()) {
	            dialog.dismiss();
	        }
		}
    }
	
	private boolean downloadFile(String fileUrl, String filename) {
		boolean downloadOk = false;
        try
        {
            URL url = new URL(fileUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url
					.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.setDoOutput(false);
			urlConnection.connect();
			InputStream inputStream = urlConnection.getInputStream();
			int totalSize = urlConnection.getContentLength();
			if(use_dialog){
				dialog.setMax(totalSize/1024);
			}
			
			File siteDirectory = new File(Constants.APP_DATA_FOLDER, String.valueOf(idSite));
			boolean folderExists = siteDirectory.exists();
		    if (!folderExists) {
		    	folderExists = siteDirectory.mkdir();
			}
		    File siteFile = new File(siteDirectory.toString(), filename);
			
			FileOutputStream fileOutput = new FileOutputStream(siteFile);
			int downloadedSize = 0;
			byte[] buffer = new byte[1024];
			int bufferLength = 0;
			while ((bufferLength = inputStream.read(buffer)) > 0) {

				fileOutput.write(buffer, 0, bufferLength);
				downloadedSize += bufferLength;
				//Log.d(TAG,"Percentage:"	+ String.valueOf((float) downloadedSize	/ totalSize * 100) + " %");
				//publishProgress((int) ((downloadedSize / (float) totalSize) * 100));
				publishProgress(downloadedSize/1024);
				
			}
			fileOutput.close();
			urlConnection.disconnect();
            downloadOk = true;
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
            downloadOk = false;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            downloadOk = false;
        }
        return downloadOk;
    }
	
	private void checkFolderStructure(){
		//create folders if it is the first time
		File appFolder = Constants.APP_DATA_FOLDER;
		boolean appFolderExists = appFolder.exists();
	    if (!appFolderExists) {
	    	appFolderExists = appFolder.mkdir();
		}
	}
    
}