package com.ascamm.motion.ws;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.ascamm.motion.utils.Constants;

public class DownloadFileAT extends AsyncTask<String, Void, DownloadFileResult>{

	private ProgressDialog dialog;
	private Context context;
	private DownloadFileCompleteListener<String> listener;
	private DownloadFileResult res;
	private int idSite;
	private boolean use_dialog;
	private String message;
			
	public DownloadFileAT(Context context, DownloadFileCompleteListener<String> listener){
		this.context = context;
		this.listener = listener;
	}
	
	public void AddDialog(){
		if(context!=null){
			use_dialog = true;
		}
    }
	
	public void setDialogMessage(String message){
		this.message = message; 
	}
		
	public void setSiteId(int id){
		this.idSite = id;
	}
	
	@Override
    protected void onPreExecute() {
		super.onPreExecute();
		if(use_dialog){
			super.onPreExecute();
	        dialog = new ProgressDialog(context);
			if(message!=null){
				dialog.setMessage(message);
			}
			else{
				dialog.setMessage("Downloading image...");
			}
	        //dialog.setTitle("Title");
			dialog.setIndeterminate(true);
	        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	        dialog.setCancelable(true);
	    	dialog.show();
		}
    }
		
	@Override
    protected DownloadFileResult doInBackground(String... urls)
    {
        //Log.d("DEBUG", "drawable");
		String filename = null;
        InputStream is = downloadFile(urls[0]);
        if(is != null){
	        String ext = urls[0].substring((urls[0].lastIndexOf(".")), urls[0].length());
	        //System.out.println("ext: " + ext);
	        checkFolderStructure();
	        filename = "pdb_site_" + idSite + ext;
	        boolean savedOk = saveFile(filename, is);
	        if(!savedOk){
	        	filename = null;
	        }
        }
        
        res = new DownloadFileResult(idSite, filename);
        return res;
    }
	
	@Override
    protected void onPostExecute(DownloadFileResult result) {
		listener.onTaskComplete(result);
		if(use_dialog){
			if (dialog.isShowing()) {
	            dialog.dismiss();
	        }
		}
    }
	
	private InputStream downloadFile(String fileUrl)
    {
        try
        {
            URL url = new URL(fileUrl);
            InputStream is = (InputStream)url.getContent();
            /*
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);                   
            urlConnection.connect();
            InputStream is = urlConnection.getInputStream();
            */
            return is;
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
            return null;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }
	
	private void checkFolderStructure(){
		//create folders if it is the first time
		File appFolder = Constants.APP_DATA_FOLDER;
		boolean appFolderExists = appFolder.exists();
	    if (!appFolderExists) {
	    	appFolderExists = appFolder.mkdir();
		}
	}
	
	private boolean saveFile(String filename, InputStream is) {
		boolean savedOk = false;
		try {
			//Log.i("Local filename:", "" + filename);
			File siteDirectory = new File(Constants.APP_DATA_FOLDER, String.valueOf(idSite));
			boolean folderExists = siteDirectory.exists();
		    if (!folderExists) {
		    	folderExists = siteDirectory.mkdir();
			}
		    File siteFile = new File(siteDirectory.toString(), filename);
		    /*
		    if(!siteFile.exists()){
				siteFile.createNewFile();
			}
		    else if(siteFile.exists()){
				siteFile.delete();
				siteFile.createNewFile();
			}
		    */
			FileOutputStream fileOutput = new FileOutputStream(siteFile);
			//InputStream inputStream = is;
			byte[] buffer = new byte[1024];
			int bufferLength = 0;
			while ( (bufferLength = is.read(buffer)) > 0 ) 
			{                 
				fileOutput.write(buffer, 0, bufferLength);                  
			}             
			fileOutput.close();
			savedOk = true;
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return savedOk;
	}
    
}