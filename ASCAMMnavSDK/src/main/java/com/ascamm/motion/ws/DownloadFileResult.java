package com.ascamm.motion.ws;


public class DownloadFileResult{
	private int id;
	private String filename;
	
	public DownloadFileResult(int id, String filename){
		this.id = id;
		this.filename = filename;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public void setFilename(String filename){
		this.filename = filename;
	}
	
	public int getId(){
		return id;
	}
	
	public String getFilename(){
		return filename;
	}
}