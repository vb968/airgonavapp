package com.ascamm.motion.ws;

/**
 * Created by Dani Fernandez on 09/05/16.
 */
public class KeyValue {

    private String key, value;

    public KeyValue(String key, String value){
        this.key = key;
        this.value = value;
    }

    public String getKey(){
        return key;
    }

    public String getValue(){
        return value;
    }
}
