package com.ascamm.motion.ws;

import java.util.ArrayList;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.ascamm.motion.ws.WebService.RequestMethod;

public class WebServiceAT extends AsyncTask<Void, Void, WebServiceResult>{

	private ProgressDialog dialog;
	private boolean use_dialog, use_name, use_ids, use_ts;
	private String ws_name, message;
	private ArrayList<Integer> ws_ids;
	private long ws_ts;
	private Context context;

	private String url;
	private RequestMethod method;
	private ArrayList<KeyValue> headers;
	private ArrayList<KeyValue> params;
	private JSONObject json;
	private WebServiceCompleteListener<String> listener;

	public WebServiceAT(Context context, WebServiceCompleteListener<String> listener, String url, RequestMethod method){
		this.context = context;
		this.listener = listener;
		this.url = url;
		this.method = method;
		this.use_dialog = false;
		this.use_name = false;
		this.use_ids = false;
		this.use_ts = false;
		this.headers = new ArrayList<KeyValue>();
		this.params = new ArrayList<KeyValue>();
	}
	
	public void AddDialog(){
		if(context!=null){
			use_dialog = true;
		}
    }
	
	public void setDialogMessage(String message){
		this.message = message; 
	}
	
	public void AddName(String name){
		ws_name = name;
		use_name = true;
	}
	
	public void addIds(ArrayList<Integer> ids){
		ws_ids = ids;
		use_ids = true;
	}
	
	public void addTS(long timestamp){
		ws_ts = timestamp;
		use_ts = true;
	}

	public void addHeader(String name, String value){
		headers.add(new KeyValue(name, value));
	}

	public void AddParam(String name, String value) {
		params.add(new KeyValue(name, value));
	}

	public void addJSON(JSONObject json){
		this.json = json;
	}
		
	@Override
    protected void onPreExecute() {
		if(use_dialog){
			super.onPreExecute();
	        dialog = new ProgressDialog(context);
			if(message!=null){
				dialog.setMessage(message);
			}
			else{
				dialog.setMessage("Loading...");
			}
	        //dialog.setTitle("Title");
			dialog.setIndeterminate(true);
	        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	        dialog.setCancelable(true);
	    	dialog.show();
		}
    }
	
	@Override
    protected WebServiceResult doInBackground(Void... nothing) {
		
		WebService ws = new WebService(url);
		
		try {
			ws.AddHeader("Content-Type", "application/json");
			for(KeyValue header : headers){
				ws.AddHeader(header.getKey(), header.getValue());
			}
			if(method.equals(RequestMethod.GET) || method.equals(RequestMethod.DELETE)){
				for(KeyValue param : params){
					ws.AddParam(param.getKey(), param.getValue());
				}
			}
			else if(method.equals(RequestMethod.POST) || method.equals(RequestMethod.PUT)) {
				ws.AddJSON(json);
			}
			ws.Execute(method);
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		WebServiceResult ws_res = new WebServiceResult(ws.getResponseCode(),ws.getResponse());
		if(use_name){
			ws_res.setName(ws_name);
		}
		if(use_ids){
			ws_res.setIds(ws_ids);
		}
		if(use_ts){
			ws_res.setTs(ws_ts);
		}
		
		return ws_res;
    }
	
	@Override
    protected void onPostExecute(WebServiceResult result) {
		listener.onTaskComplete(result);
		if(use_dialog){
			if (dialog.isShowing()) {
	            dialog.dismiss();
	        }
		}
    }
}