package com.ascamm.motion.ws;

import java.util.ArrayList;

public class WebServiceResult{
	private int status;
	private String response, ws_name;
	private ArrayList<Integer> ids;
	private long ts;
	
	public WebServiceResult(int status, String response){
		this.status = status;
		this.response = response;
	}
	
	public void setStatus(int status){
		this.status = status;
	}
	
	public void setResponse(String response){
		this.response = response;
	}
	
	public void setName(String name){
		this.ws_name = name;
	}
	
	public void setIds(ArrayList<Integer> ids){
		this.ids = ids;
	}
	
	public int getStatus(){
		return status;
	}
	
	public String getResponse(){
		return response;
	}
	
	public String getName(){
		return ws_name;
	}
	
	public ArrayList<Integer> getIds(){
		return ids;
	}

	public long getTs() {
		return ts;
	}

	public void setTs(long ts) {
		this.ts = ts;
	}
	
}