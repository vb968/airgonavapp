package com.ascamm.motion.detections;

import com.ascamm.utils.Point3D;

import java.io.Serializable;

public class Detection implements Serializable{

	private static final long serialVersionUID = 1L;
	private String mac;
	private float rss;
	private Point3D posLocal;
	private Point3D posGlobal;
	
	public Detection(){
		
	}
	
	public Detection(String mac, float rss, Point3D posLocal, Point3D posGlobal){
		this.mac = mac;
		this.rss = rss;
		this.posLocal = posLocal;
		this.posGlobal = posGlobal;
	}
	
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	public float getRss() {
		return rss;
	}
	public void setRss(float rss) {
		this.rss = rss;
	}
	public Point3D getPosLocal() {
		return posLocal;
	}
	public void setPosLocal(Point3D posLocal) {
		this.posLocal = posLocal;
	}
	public Point3D getPosGlobal() {
		return posGlobal;
	}
	public void setPosGlobal(Point3D posGlobal) {
		this.posGlobal = posGlobal;
	}

	public String toString(){
		String local = "null";
		String global = "null";
		if(posLocal!=null){
			local = posLocal.toString();
		}
		if(posGlobal!=null){
			global = posGlobal.toString();
		}
		return "Detection: (" + mac + "," + rss + "," + local + "," + global + ")";
	}
	
}
