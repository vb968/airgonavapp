package com.ascamm.motion.detections;

import com.ascamm.utils.Point3D;

/**
 * Created by indoorpositioning on 04/05/16.
 */

public class DbDetection extends Detection{

    private static final long serialVersionUID = 1L;
    private int id;
    private long ts;
    private int siteId;
    private String userId;

    public DbDetection(){

    }

    public DbDetection(int id, String mac, Point3D posLocal, Point3D posGlobal, long ts, int siteId, String userId){
        this.id = id;
        super.setMac(mac);
        super.setPosLocal(posLocal);
        super.setPosGlobal(posGlobal);
        this.ts = ts;
        this.siteId = siteId;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }

    public int getSiteId() {
        return siteId;
    }

    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
