package com.ascamm.motion.detections;

import java.util.ArrayList;


/**
 * A interface defines the methods related with WiFi scans. 
 * @author Junzi Sun [CTAE]
 *
 */
public interface DetectionListener {
	
	/**
	 * inform a scan is finished
	 */
	public void newDetectionAvailable(ArrayList<Detection> detections, int siteId);
}
