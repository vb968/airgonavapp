package com.ascamm.motion.wifi;


/**
 * A interface defines the methods related with WiFi scans. 
 * @author Junzi Sun [CTAE]
 *
 */
public interface WiFiListener {
	
	/**
	 * inform a scan is finished
	 */
	public void newScanAvaiable();
}
