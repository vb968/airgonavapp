package com.ascamm.motion.wifi;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.ascamm.motion.mems.MotionMEMS;
import com.ascamm.motion.utils.LogWriter;
import com.ascamm.motion.algorithm.KalmanFilter;
import com.ascamm.motion.utils.LogReader;
import com.ascamm.motion.utils.LogReader.FileType;
import com.ascamm.motion.utils.OnlineItem;
import com.ascamm.motion.utils.OnlineMeasure;
import com.ascamm.motion.utils.SimOnlineMeasure;

import com.ascamm.motion.utils.Constants;
import com.ascamm.motion.utils.Settings;

public class WiFiService extends Service {

	private final String TAG = getClass().getSimpleName();
	
	private static boolean debug = Constants.DEBUGMODE;
	
	private final IBinder mBinder = new WiFiBinder();

	private WifiManager mWifiManager;
	private BroadcastReceiver mWifiScanReceiver;
	private IntentFilter mIntentFilter;
	private boolean mIsScanOn = false;
	private boolean mIsBound = false;
	private boolean wifiOk = false;

	private MotionMEMS mMotionMEMS;
	
	private Timer mTimer;
	private TimerTask mTimerTask;

	private LinkedList<OnlineMeasure> mQueueOfOnlineMeasures = new LinkedList<OnlineMeasure>();
	//private LinkedBlockingDeque<OnlineMeasure> mQueueOfOnlineMeasures = new LinkedBlockingDeque<OnlineMeasure>();
	//private ConcurrentLinkedQueue<OnlineMeasure> mQueueOfOnlineMeasures = new ConcurrentLinkedQueue<OnlineMeasure>();
	private final int MAX_SCAN_BUFFER_SIZE = 200;
	private boolean loadingFile = false;
	
	private KalmanFilter mKalmanFilter;
	boolean mUseKalmanRSSI;
	boolean mUseWeightedRSSI;
	
	/** Code added for logs **/
	private LogWriter mLogWriter;
	private boolean logsRunning = false;
	private boolean storeLogs = Settings.storeLogs;
	//private boolean storeLogs = false;
	private String logsFilename = Settings.logsFilename + "_RSS_wifi" + Constants.logsFilenameExt;
	
	/** Code added for Kalman logs **/
	private LogWriter mKalmanLogWriter;
	private boolean kalmanLogsRunning = false;
	//private boolean storeKalmanLogs = Settings.storeLogs;
	private boolean storeKalmanLogs = false;
	private String kalmanLogsFilename = Settings.logsFilename + "_RSS_wifi_kalman" + Constants.logsFilenameExt;
	
	/************** Simulation variables ************/
	private boolean useSimulationWifi = false;
	//private LinkedList<ArrayList<SimScanResult>> simScansQueue;
	private LinkedList<SimOnlineMeasure> simScansQueue;
	private int pos = 0, simQueueSize = 0;
	//private String fileToRead = "logs_RSS_CAR.txt";
	//private String fileToRead = "logs_RSS_wifi_ASCAMM_lab.txt";
	//private String fileToRead = "logs_RSS_wifi_ASCAMM_lab_route_1.txt";
	private String fileToRead = "logs_ASCAMM_Supermarket_wifi_ble_route_9_RSS_wifi.txt";
	//private String fileToRead = "logs_CAR_route_11_RSS_wifi.txt";
	private long fromTs = 0;
	private long toTs = (long) Math.pow(10, 15);
	/************** End of simulation variables **************/
	
	private ArrayList<WiFiListener> mScanWifiListeners = new ArrayList<WiFiListener>();

	public class WiFiBinder extends Binder {
		public WiFiService getService() {
			return WiFiService.this;
		}
	}


	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	/************* Simulation *************/
	private void createSimulatedScans(){
		LogReader mLogReader = new LogReader(Constants.APP_DATA_FOLDER_NAME, fileToRead);
		mLogReader.readFile(FileType.RSS, fromTs, toTs);
		simScansQueue = mLogReader.getSimScansQueue();
		simQueueSize = simScansQueue.size();
	}
	/************* End of simulation **********/
	
	@Override
	public void onCreate() {
		//Toast.makeText(this, "WiFiService Created", Toast.LENGTH_SHORT).show();
				
		if(mUseKalmanRSSI) mKalmanFilter = new KalmanFilter();

		mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		if(!mWifiManager.isWifiEnabled()){
			wifiOk = false;
			Toast.makeText(this, "WiFi is not enabled", Toast.LENGTH_LONG).show();
		}
		else{
			wifiOk = true;
		}
		mIntentFilter = new IntentFilter();
		mIntentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);  	// when scan is completed
		
		/** Code added to store data info into logs.txt **/
		if(storeLogs){
			mLogWriter =new LogWriter(Constants.APP_DATA_FOLDER_NAME, logsFilename);
			if(mLogWriter.startLogs()>0){
				logsRunning = true;
			}
		}
		
		/** Code added to store kalman data info into logs.txt **/
		if(storeKalmanLogs){
			mKalmanLogWriter =new LogWriter(Constants.APP_DATA_FOLDER_NAME, kalmanLogsFilename);
			if(mKalmanLogWriter.startLogs()>0){
				kalmanLogsRunning = true;
			}
		}
		
		if(useSimulationWifi){
			/******** Simulation ************/
			new Thread() {
				public void run() {
					loadingFile = true;
					Log.i(TAG,"Loading File...");
					createSimulatedScans();
					loadingFile = false;
					Log.i(TAG,"File Loaded");
				}
			}.start();
			/********************************/
		}
		
		mWifiScanReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if(!loadingFile){
					long ts = System.currentTimeMillis();
					if(debug)	Log.d(TAG,"onReceiveWifiScan. TS: " + ts);
					//Log.d(TAG,"onReceiveWifiScan. TS: " + ts);
					boolean walking = false;
					if(mMotionMEMS!=null){
						walking = mMotionMEMS.getWalking();
					}
					OnlineMeasure newOnlineMeasure = new OnlineMeasure(ts, walking);

					if(!useSimulationWifi){
						/******************* Real measurements **********************/
						ArrayList<ScanResult> newScanResults = (ArrayList<ScanResult>) mWifiManager.getScanResults();
						newOnlineMeasure.setOnlineItemsWiFi(newScanResults);
						/******************* End of real measurements ****************/
					}
					else{
						/*********************** Simulation *************************/
						if(pos >= simQueueSize){
							pos = 0; 
						}
						/*
						Log.i(TAG,"pos: " + pos);
						ArrayList<SimScanResult> newSimScanResults = simScansQueue.get(pos);
						newOnlineMeasure.setOnlineItemsSim(newSimScanResults);
						*/
						SimOnlineMeasure simScans = simScansQueue.get(pos);
						Log.i(TAG,"pos: " + pos + " , ts: " + simScans.getTs());
						ts = simScans.getTs();
						ArrayList<OnlineItem> onlineItemsWiFi = simScans.getOnlineItems();
						newOnlineMeasure.setOnlineItems(onlineItemsWiFi);
						pos++;
						/*********************** End of simulation *******************/
					}
					
					// Code added to store data info into logs.txt
					if(storeLogs && logsRunning){
						mLogWriter.addArrayRowOnlineMeasuresLog(ts, newOnlineMeasure.getOnlineItems());
					}
										
					/******** Kalman filter for the measured RSSI from each AP *******/
					if(mUseKalmanRSSI){
						try{
							OnlineMeasure lastOnLineMeasure = mQueueOfOnlineMeasures.getLast();
							ArrayList<OnlineItem> kalmanOnlineItems = new ArrayList<OnlineItem>();
							for(OnlineItem oli : newOnlineMeasure.getOnlineItems()){
								if(debug)	Log.d(TAG, "AP: " + oli.getBSSID() + "  RSSI: " + oli.getRSS());
								
								OnlineItem koi = new OnlineItem(null, oli.getBSSID(), oli.getRSS(), oli.getRSS(), 0);
								int j = getIndex(lastOnLineMeasure.getOnlineItems(), oli.getBSSID());
								if(j>=0){
									/* 
									 * we got a measurement from this AP in the last epoch, 
									 * therefore we can run Kalman filter to improve the current
									 * measurement based on the last epoch measurement
									 */
									float last_epoch_rssi = lastOnLineMeasure.getOnlineItems().get(j).getRSS();
									float measured_rssi = oli.getRSS();
									float p = lastOnLineMeasure.getOnlineItems().get(j).getP_kalman();
									if(debug)	Log.d(TAG,"last_epoch_rssi: " + last_epoch_rssi + " , measured_rssi: " + measured_rssi + " , p: " + p);
									float[] filter_output = mKalmanFilter.calculateOutput(last_epoch_rssi, p, measured_rssi);
									oli.setRSS(filter_output[0]);
									oli.setP_kalman(filter_output[1]);
									
									if(debug)	Log.d(TAG, "AP: " + oli.getBSSID() + "  filtered RSSI: " + filter_output[0]);
									koi.setRSSstdev(filter_output[0]);
								}
								kalmanOnlineItems.add(koi);
							}
							// Code added to store data info into logs.txt
							if(storeKalmanLogs && kalmanLogsRunning){
								mKalmanLogWriter.addArrayRowKalmanLog(ts, kalmanOnlineItems);
							}
						}catch (NoSuchElementException e) {
							//mQueueOfOnlineMeasures is empty: it is the first scan and therefore we cannot apply Kalman yet
							if(debug){
								Log.d(TAG, "///// mQueueOfOnlineMeasures is empty ///////");
								for(OnlineItem oli : newOnlineMeasure.getOnlineItems()){
									Log.d(TAG, "AP: " + oli.getBSSID() + "  RSSI: " + oli.getRSS());
								}
							}
						}
					}				
					/*****************************************************************/
						
					// add the result to the end of the queue, remove head if it is full
					if(mQueueOfOnlineMeasures.size() < MAX_SCAN_BUFFER_SIZE){
						mQueueOfOnlineMeasures.add(newOnlineMeasure);
					} else {
						while (mQueueOfOnlineMeasures.size() >= MAX_SCAN_BUFFER_SIZE){
							mQueueOfOnlineMeasures.poll();
						}
						mQueueOfOnlineMeasures.add(newOnlineMeasure);
					}
	
					//if(debug)	Log.d(TAG, "Buffer size: " + mQueueOfOnlineMeasures.size());
					
					//long ts2 = System.currentTimeMillis();
					fireNewScanAvailableEvent();
					//long ts3 = System.currentTimeMillis();
					//Log.d(TAG,"onReceiveWifiScan. Before startScan. dTS2: " + (ts2-ts) + " , dTS3: " + (ts3-ts));
				}
				if (mIsScanOn){
					mWifiManager.startScan();
				}
			}
		};

		/* register the receiver */
		registerReceiver(mWifiScanReceiver, mIntentFilter);
		
	}

	@Override
	public void onDestroy() {
		//Toast.makeText(this, "WiFiService Stopped", Toast.LENGTH_SHORT).show();
		unregisterReceiver(mWifiScanReceiver);
		/** Code added to store data info into logs.txt **/
		if(storeLogs && logsRunning){
			mLogWriter.stopLogs();
			logsRunning = false;
		}
		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startid){
		//Toast.makeText(this, "WiFiService Started", Toast.LENGTH_SHORT).show();
		return START_STICKY;
	}


	public void setMotionMEMS(MotionMEMS motionMEMS){
		mMotionMEMS = motionMEMS;
	}

	public void startScan(){
		if(!mIsScanOn && wifiOk) {
			mWifiManager.startScan();
			mIsScanOn = true;

			startTimer();
		}
	}

	public void stopScan(){
		mIsScanOn = false;
		stopTimer();
	}

	/* event listeners */
	public synchronized void addWiFiNewScanListener(WiFiListener listener) {
		mScanWifiListeners.add(listener);
	}

	public synchronized void removeWiFiNewScanListener(WiFiListener listener) {
		mScanWifiListeners.remove(listener);
	}

	private synchronized void fireNewScanAvailableEvent(){
		for(WiFiListener listener : mScanWifiListeners){
			listener.newScanAvaiable();
		}
	}

	/** Get an average RSS set of scan results **/
	public ArrayList<OnlineItem> calcAvgScanSet(int mSecondsOfScansToUse, boolean walking){
		ArrayList<OnlineItem> avgOnlineItems = new ArrayList<OnlineItem>();
		long currentTs = System.currentTimeMillis();
		int numScans = 0;
		long minMilliSeconds = mSecondsOfScansToUse*1000;
		long maxMilliSeconds = 30*1000;
		
		float c_rss = 0.0f;
		int c_count = 0;
		float weight = 0.0f;
				
		//Make a copy of the buffer in order to work with the copy instead of the main buffer
		OnlineMeasure[] onlineList = (OnlineMeasure[]) mQueueOfOnlineMeasures.toArray(new OnlineMeasure[mQueueOfOnlineMeasures.size()]);
		int onlineListLength = onlineList.length;

		boolean walkingDetected = false;
				
		// Loop through all last number of scans
		for (int i=onlineListLength; i>0; i--){
			OnlineMeasure onlineMeasure = onlineList[i-1];
			long tsDiff = currentTs - onlineMeasure.getTs();
			boolean measureWalking = onlineMeasure.isWalking();
			//Log.d(TAG,"measureWalking: " + measureWalking + " , tsDiff: " + tsDiff + " ms");
			if(tsDiff < minMilliSeconds){
				if(measureWalking){
					walkingDetected = true;
				}
			}
			else{
				if(walking) {
					break;
				}
				else{
					if(walkingDetected) {
						break;
					}
					else{
						if (tsDiff < maxMilliSeconds) {
							// stop when walking measures detected
							if (measureWalking) {
								break;
							}
						} else {
							// stop when max time reached
							break;
						}
					}
				}
			}

			// process measurements
			if(mUseWeightedRSSI){
				weight = 1.0f / (currentTs - onlineMeasure.getTs());
			}

			numScans++;
			for ( OnlineItem oi : onlineMeasure.getOnlineItems() ){
				int j = getIndex(avgOnlineItems, oi.getBSSID());
				if (j < 0){
					// if this bssid never seen before create a new entry in scan set
					OnlineItem newOI = new OnlineItem(oi.getSSID(), oi.getBSSID(), oi.getRSS(), 1);
					newOI.setTech("WiFi");
					newOI.addRSSvalue(oi.getRSS());
					if(mUseWeightedRSSI){
						newOI.addRSSweight(weight);
						newOI.setSumWeights(weight);
					}
					avgOnlineItems.add(newOI);
				} else {
					c_rss = avgOnlineItems.get(j).getRSS();
					c_count = avgOnlineItems.get(j).getCount();
					if(mUseWeightedRSSI){
						// calculate new weighted avg rss level, and update sum_weights
						avgOnlineItems.get(j).setRSS(((c_rss * avgOnlineItems.get(j).getSumWeights()) + weight*oi.getRSS()) / (avgOnlineItems.get(j).getSumWeights() + weight));
						avgOnlineItems.get(j).setSumWeights(avgOnlineItems.get(j).getSumWeights() + weight);
						avgOnlineItems.get(j).addRSSweight(weight);
					}else{
						// calculate new avg rss level
						avgOnlineItems.get(j).setRSS((c_rss * c_count + oi.getRSS()) / (c_count + 1));
					}
					// update count
					avgOnlineItems.get(j).setCount(c_count + 1);
					avgOnlineItems.get(j).addRSSvalue(oi.getRSS());
				}
			}

		}
				
		
		if(mUseWeightedRSSI){
			// Calculate the weighted stdev and the count percentage of each scan 
			for(OnlineItem oi : avgOnlineItems){
				float weighted_mean = oi.getRSS();
				float count = oi.getCount();
				float sumweights = oi.getSumWeights();
				double weighted_stdev = 0;
				float sum = 0;
				float next_weight = 0;
									
				Iterator<Float> rss_value = oi.getRSSvalues().iterator();
				Iterator<Float> rss_weight = oi.getRSSWeights().iterator();
				while(rss_value.hasNext() && rss_weight.hasNext()) {
					next_weight = rss_weight.next();
					sum += next_weight * Math.pow(rss_value.next() - weighted_mean,2);
				}
				float den = ((count-1)/count) * sumweights;

				weighted_stdev = Math.sqrt(sum/den);
				oi.setRSSstdev(weighted_stdev);
				oi.setCountPercentage((count/numScans)*100);				
			}		
		}else{
			// Calculate the stdev and the count percentage of each scan 
			for(OnlineItem oi : avgOnlineItems){
				float mean = oi.getRSS();
				float count = oi.getCount();
				double stdev = 0;
				float sum = 0;
				for( float rss : oi.getRSSvalues() ){
					sum += Math.pow(rss-mean,2);
				}
				stdev = Math.sqrt(sum/count);
				oi.setRSSstdev(stdev);
				oi.setCountPercentage((count/numScans)*100);
			}		
		}
		

		if(debug)	Log.d(TAG,"inside calcAvgScanSet (WiFi). numScans: " + numScans);
		return avgOnlineItems;
	}

	/** Get last set of scan results **/
	public ArrayList<OnlineItem> getLastScanSet(){
		ArrayList<OnlineItem> lastOnlineItems = new ArrayList<OnlineItem>();
		OnlineMeasure om = mQueueOfOnlineMeasures.getLast();
		for(OnlineItem oi : om.getOnlineItems()){
			lastOnlineItems.add(new OnlineItem(oi.getSSID(), oi.getBSSID(), oi.getRSS()));
		}
		return lastOnlineItems;
	}

	/** return size of wifi scan buffer **/
	public int getScanBufferSize(){
		return mQueueOfOnlineMeasures.size();
	}

	public boolean isScanning(){
		return mIsScanOn;
	}

	public boolean isBound(){
		return mIsBound;
	}

	public void setBound(boolean isBound){
		mIsBound = isBound;
	}
	
	public void setParameters(boolean useKalmanRSSI, boolean useWeightedRSSI){
		this.mUseKalmanRSSI = useKalmanRSSI;
		this.mUseWeightedRSSI = useWeightedRSSI;
		if(this.mUseKalmanRSSI) mKalmanFilter = new KalmanFilter();
	}

	/** get index of a bssid in a scanSet **/
	private static int getIndex(ArrayList<OnlineItem> onlineItems, String bssid){
		for (int i=0; i<onlineItems.size(); i++){
			if (onlineItems.get(i).getBSSID().equalsIgnoreCase(bssid)){
				return i;
			}
		}
		return -1;
	}

	private void startTimer(){  
		mTimer = new Timer();  
		mTimerTask = new KeepScanTask();
		mTimer.schedule(mTimerTask, 0, Constants.RETRY_WIFI_SCANS);  
	}  

	private void stopTimer(){  
		if (mTimer != null) {  
			mTimer.cancel();  
			mTimer = null;  
		}  
		if (mTimerTask != null) {  
			mTimerTask.cancel();  
			mTimerTask = null;  
		}     
	}
	
	private class KeepScanTask extends TimerTask {
		@Override
		public void run() {
			mWifiManager.startScan();  

			do {  
				try {  
					Thread.sleep(1000);  
				} catch (InterruptedException e) {  
				}     
			} while (!mIsScanOn);   
		}
	}

}