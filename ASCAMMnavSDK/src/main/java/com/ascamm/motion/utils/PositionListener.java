package com.ascamm.motion.utils;

import com.ascamm.motion.LocationData;

import java.util.ArrayList;

public interface PositionListener {
	public void newPositionAvailable(LocationData locationData);
}
