package com.ascamm.motion.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import android.net.wifi.ScanResult;
import android.os.Environment;
import android.util.Log;

import com.ascamm.motion.ble.BleType;
import com.ascamm.utils.Point3D;

public class LogWriter {
	
	private String TAG = "LogWriter";
	private String filename = "logs.txt";
	private String foldername = "ASCAMM_Location";
	private BufferedWriter out;
	
	private String decFor = "%.2f";
	
	public LogWriter(){
		
	}
	
	public LogWriter(String folder, String file){
		foldername = folder;
		filename = file;
	}
	
	public int startLogs(){
		/*
		int success = 0;
		File Root = Environment.getExternalStorageDirectory();
        if(Root.canWrite()){
        	File dir = new File(Root, foldername);
        	if(!dir.exists()) {
                dir.mkdirs();
            }
            File LogFile = new File(dir, filename);
            if (!LogFile.exists()) {
            	try
            	{
            		Log.i(TAG, "File created with name: " + LogFile.getPath());
            		LogFile.createNewFile();
            	} 
            	catch (IOException e)
            	{
            		// TODO Auto-generated catch block
        			e.printStackTrace();
        			Log.e(TAG, LogFile.getPath() + " could not be created");
            		success = -1;
        			return success;
    			}
            }
            FileWriter LogWriter;
            try {
            	LogWriter = new FileWriter(LogFile, true);
            } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.e(TAG, "LogWriter could not be created");
        		success = -1;
				return success;
            }
            out = new BufferedWriter(LogWriter);
            Date date = new Date();
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			//f.setTimeZone(TimeZone.getTimeZone("GMT");
			String s = f.format(date);
            try {
				out.write("-----   New app execution ( " + s + " )   -----\n");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.e(TAG, "Problem writing execution line");
        		success = -1;
        		return success;
			}
            success = 1;
        }
        return success;
        */
		int success = startLogsWithText(true);
		return success;
	}
	
	public int startLogsNoText(){
		int success = startLogsWithText(false);
		return success;
	}
	
	private int startLogsWithText(boolean writeText){
		int success = 0;
		File Root = Environment.getExternalStorageDirectory();
        if(Root.canWrite()){
        	File dir = new File(Root, foldername);
        	if(!dir.exists()) {
                dir.mkdirs();
            }
            File LogFile = new File(dir, filename);
            if (!LogFile.exists()) {
            	try
            	{
            		boolean fileCreated = LogFile.createNewFile();
					if(fileCreated) {
						Log.i(TAG, "File created with name: " + LogFile.getPath());
					}
					else{
						Log.e(TAG, LogFile.getPath() + " could not be created");
						success = -1;
						return success;
					}
            	} 
            	catch (IOException e) {
            		e.printStackTrace();
        			Log.e(TAG, LogFile.getPath() + " could not be created");
            		success = -1;
        			return success;
    			}
            }
            FileWriter LogWriter;
            try {
            	LogWriter = new FileWriter(LogFile, true);
            } catch (IOException e) {
				e.printStackTrace();
				Log.e(TAG, "LogWriter could not be created");
        		success = -1;
				return success;
            }
            out = new BufferedWriter(LogWriter);
            if(writeText){
	            Date date = new Date();
	            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				//f.setTimeZone(TimeZone.getTimeZone("GMT");
				String s = f.format(date);
	            try {
					out.write("-----   New app execution ( " + s + " )   -----\n");
				} catch (IOException e) {
					e.printStackTrace();
					Log.e(TAG, "Problem writing execution line");
	        		success = -1;
	        		return success;
				}
            }
            success = 1;
        }
        return success;
	}
	
	public int stopLogs(){
		int success = 0;
		try {
			out.flush();
			out.close();
			success = 1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG, "MyLogger could not be correctly closed");
    		success = -1;
		}
        return success;
	}
	
	private String convertTsToDate(long ts){
		Date date = new Date(ts);
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//f.setTimeZone(TimeZone.getTimeZone("GMT");
		String s = f.format(date);
		return s;
	}
	
	public int addMeasureLog(String mac, String rssi, String tech, String timestamp){
		int success = 0;
		try {
			out.write("MEASURE" + "," + timestamp + ",\"" + mac + "\"," + rssi + ",\"" + tech + "\"\n");
			success = 1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG, "Problem adding measure log");
    		success = -1;
		}
		return success;
	}
	
	public int addArrayMeasuresLog(ArrayList<OnlineItem> scans, String timestamp){
		int success = 0;
		try {
			for(OnlineItem oi : scans){
				out.write("MEASURE" + "," + timestamp + ",\"" + oi.getBSSID() + "\"," + oi.getRSS() + ",\"" + oi.getTech() + "\"\n");
			}
			success = 1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG, "Problem adding measures to log");
    		success = -1;
		}
		return success;
	}
	
	public int addArrayRowMeasuresLog(long ts, ArrayList<ScanResult> scans){
		int success = 0;
		try {
			out.write(ts + "\n");
			for(ScanResult sr : scans){
				out.write(sr.SSID + ","+ sr.BSSID + "," + sr.level + "\n");
			}
			success = 1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG, "Problem adding measures to log");
    		success = -1;
		}
		return success;
	}
	
	public int addArrayRowBleMeasuresLog(long ts, ArrayList<BleType> scans){
		int success = 0;
		try {
			out.write(ts + "\n");
			for(BleType sr : scans){
				out.write(","+ sr.getMac() + "," + sr.getRssi() + "\n");
			}
			success = 1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG, "Problem adding measures to log");
    		success = -1;
		}
		return success;
	}

	public int addRawOnlineMeasureLog(long ts, String id, float value){
		int success = 0;
		try {
			out.write(ts + "," + id + "," + value + "\n");
			success = 1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG, "Problem adding measures to log");
			success = -1;
		}
		return success;
	}
	
	public int addArrayRowOnlineMeasuresLog(long ts, ArrayList<OnlineItem> scans){
		int success = 0;
		try {
			out.write(ts + "\n");
			for(OnlineItem oi : scans){
				out.write(oi.getSSID() + ","+ oi.getBSSID() + "," + oi.getRSS());
				int numValues = oi.getRSSvalues().size();
				if(numValues > 0) {
					out.write(",");
					for (int i = 0; i < numValues; i++) {
						if (i == (numValues - 1)) {
							out.write(oi.getRSSvalues().get(i) + "");
						} else {
							out.write(oi.getRSSvalues().get(i) + ";");
						}
					}
				}
				out.write("\n");
			}
			success = 1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG, "Problem adding measures to log");
    		success = -1;
		}
		return success;
	}
	
	public int addArrayAverageMeasuresLog(long ts, ArrayList<OnlineItem> scans, boolean walking, int sensorState, int window){
		int success = 0;
		int walkingValue = 0;
		if(walking){
			walkingValue = 1;
		}
		try {
			out.write(ts + "," + walkingValue + "," + sensorState + "," + window + "\n");
			for(OnlineItem oi : scans){
				out.write(oi.getSSID() + "," + oi.getBSSID() + "," + oi.getRSS() + "," + oi.getRSSstdev() + "," + oi.getCount() + "," + String.format(Locale.ENGLISH, decFor, oi.getCountPercentage()) + "," + oi.getTech() + ",");
				int numValues = oi.getRSSvalues().size();
				for(int i=0; i<numValues; i++){
					if(i==(numValues-1)){
						out.write(oi.getRSSvalues().get(i) + "");
					}
					else{
						out.write(oi.getRSSvalues().get(i) + ";");
					}
				}
				out.write("\n");
			}
			success = 1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG, "Problem adding measures to log");
    		success = -1;
		}
		return success;
	}
	
	public int addPositionLog(String pos_x, String pos_y, String pos_z, String pos_lat, String pos_lon, String pos_alt, String timestamp){
		int success = 0;
		try {
			out.write("POSITION" + "," + timestamp + "," + pos_x + "," + pos_y + "," + pos_z + "," + pos_lat + "," + pos_lon + "," + pos_alt + "\n");
			success = 1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG, "Problem adding position log");
    		success = -1;
		}
		return success;
	}
	
	public int addAutolearningLog(String type, int iter, double a, double b, double res, double a2, double b2, double res2, double y2, boolean hasConverged){
		int success = 0;
		int converged = 0;
		if(hasConverged){
			converged = 1;
		}
		try {
			out.write(type + "," + iter + "," + a + "," + b + "," + res + "," + a2 + "," + b2 + "," + res2 + "," + y2 + "," + converged + "\n");
			success = 1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG, "Problem adding autolearning log");
    		success = -1;
		}
		return success;
	}
	
	public int addPositionErrorLog(long ts, Point3D realPos, Point3D estimatedPos, float error){
		int success = 0;
		try {
			out.write(ts + "," + realPos.x + "," + realPos.y + "," + realPos.z + "," + estimatedPos.x + "," + estimatedPos.y + "," + estimatedPos.z + "," + error + "\n");
			success = 1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG, "Problem adding position_error log");
    		success = -1;
		}
		return success;
	}
	
	public int addPositionErrorLog2(long ts, Point3D realPos, Point3D estimatedPos1, float error1, Point3D estimatedPos2, float error2){
		int success = 0;
		try {
			out.write(ts + "," + realPos.x + "," + realPos.y + "," + realPos.z + "," + estimatedPos1.x + "," + estimatedPos1.y + "," + estimatedPos1.z + "," + error1 + "," + estimatedPos2.x + "," + estimatedPos2.y + "," + estimatedPos2.z + "," + error2 + "\n");
			success = 1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG, "Problem adding position_error log");
    		success = -1;
		}
		return success;
	}
	
	public int addProbMeasuresLog(long ts, ArrayList<Integer> ids, ArrayList<Double> dist, ArrayList<Double> prob){
		int success = 0;
		String log = String.valueOf(ts) + ";";
		int idsSize = ids.size();
		for (int i=0; i<idsSize; i++){
			if(i==(idsSize-1)){
				log += ids.get(i) + ";";
			}else{
				log += ids.get(i) + ",";
			}
		}
		int distSize = dist.size();
		for (int i=0; i<distSize; i++){
			if(i==(distSize-1)){
				log += dist.get(i) + ";";
			}else{
				log += dist.get(i) + ",";
			}
		}
		int probSize = prob.size();
		for (int i=0; i<probSize; i++){
			if(i==(probSize-1)){
				log += prob.get(i);
			}else{
				log += prob.get(i) + ",";
			}
		}
		try {
			out.write(log + "\n");
			success = 1;
		} catch (IOException e) {
			e.printStackTrace();
			Log.e(TAG, "Problem adding prob measures to log");
    		success = -1;
		}
		return success;
	}
	
	public int addArrayRowKalmanLog(long ts, ArrayList<OnlineItem> scans){
		int success = 0;
		try {
			out.write(ts + "\n");
			for(OnlineItem oi : scans){
				out.write(oi.getSSID() + "," + oi.getBSSID() + "," +  String.format(Locale.ENGLISH, decFor, oi.getRSS()) + "," + String.format(Locale.ENGLISH, decFor, oi.getRSSstdev()) + "\n");
			}
			success = 1;
		} catch (IOException e) {
			e.printStackTrace();
			Log.e(TAG, "Problem adding kalman measures to log");
    		success = -1;
		}
		return success;
	}
	
	public int addTextLog(long ts, String text){
		int success = 0;
		try {
			out.write(convertTsToDate(ts) + " | " + text + "\n");
			success = 1;
		} catch (IOException e) {
			e.printStackTrace();
			Log.e(TAG, "Problem adding text log");
    		success = -1;
		}
		return success;
	}

	public synchronized int addSensorLog(String sensor, long ts, long sysTs, int accuracy, float[] values){
		int success = 0;
		try {
			out.write(sensor + "," + ts + "," + sysTs + "," + accuracy + ",");
			int numValues = values.length;
			for(int i=0; i<numValues; i++){
				if(i==(numValues-1)){
					out.write(values[i] + "");
				}
				else{
					out.write(values[i] + ";");
				}
			}
			out.write("\n");
			success = 1;
		} catch (IOException e) {
			e.printStackTrace();
			Log.e(TAG, "Problem adding text log");
			success = -1;
		}
		return success;
	}

	public int addMeasurementsLog(long ts, String sensor, float[] raw, float[] lpfilt, float[] hpfilt, float norm, float[] rawGlobal, float[] lowPassGlobal, long sensorTs){
		int success = 0;
		String log = sensor + ";" + String.valueOf(ts) + ";";
		int rawSize = raw.length;
		for (int i=0; i<rawSize; i++){
			if(i==(rawSize-1)){
				log += raw[i] + ";";
			}else{
				log += raw[i] + ",";
			}
		}
		int lpfiltSize = lpfilt.length;
		for (int i=0; i<lpfiltSize; i++){
			if(i==(lpfiltSize-1)){
				log += lpfilt[i] + ";";
			}else{
				log += lpfilt[i] + ",";
			}
		}
		int hpfiltSize = hpfilt.length;
		for (int i=0; i<hpfiltSize; i++){
			if(i==(hpfiltSize-1)){
				log += hpfilt[i] + ";";
			}else{
				log += hpfilt[i] + ",";
			}
		}
		log += norm + ";";
		if(rawGlobal!=null){
			int rawGlobalSize = rawGlobal.length;
			for (int i = 0; i < rawGlobalSize; i++) {
				if (i == (rawGlobalSize - 1)) {
					log += rawGlobal[i] + ";";
				} else {
					log += rawGlobal[i] + ",";
				}
			}
		}
		if(lowPassGlobal!=null){
			int lowPassGlobalSize = lowPassGlobal.length;
			for (int i = 0; i < lowPassGlobalSize; i++) {
				if (i == (lowPassGlobalSize - 1)) {
					log += lowPassGlobal[i] + ";";
				} else {
					log += lowPassGlobal[i] + ",";
				}
			}
		}
		log += String.valueOf(sensorTs) + ";";
		try {
			out.write(log + "\n");
			success = 1;
		} catch (IOException e) {
			e.printStackTrace();
			Log.e(TAG, "Problem adding sensors measurements to log");
			success = -1;
		}
		return success;
	}

	public int addTimeDomainSignal(long ts, String sensor, double[] samples, double mean, double energy, double variance){
		int success = 0;
		String log = String.valueOf(ts) + ";" + "time" + ";" + sensor + ";";
		try {
			int samplesSize = samples.length;
			for (int i=0; i<samplesSize; i++){
				if(i==(samplesSize-1)){
					log += samples[i] + ";";
				}else{
					log += samples[i] + ",";
				}
			}
			log += mean + ";" + energy + ";" + variance + ";";
			out.write(log + "\n");
			success = 1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG, "Problem adding time domain signal log");
			success = -1;
		}
		return success;
	}

	public int addTimeDomainSignal(long ts, String sensor, double[] samples, double mean, double energy, double variance, double meanX, double energyX, double varianceX, double meanY, double energyY, double varianceY, double meanZ, double energyZ, double varianceZ){
		int success = 0;
		String log = String.valueOf(ts) + ";" + "time" + ";" + sensor + ";";
		try {
			int samplesSize = samples.length;
			for (int i=0; i<samplesSize; i++){
				if(i==(samplesSize-1)){
					log += samples[i] + ";";
				}else{
					log += samples[i] + ",";
				}
			}
			log += mean + ";" + energy + ";" + variance + ";" + meanX + ";" + energyX + ";" + varianceX + ";" + meanY + ";" + energyY + ";" + varianceY + ";" + meanZ + ";" + energyZ + ";" + varianceZ + ";";
			out.write(log + "\n");
			success = 1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG, "Problem adding time domain signal log");
			success = -1;
		}
		return success;
	}

	public int addFreqDomainSignal(long ts, String sensor, double[] samples, int numDominantFreqs, int motionMode){
		int success = 0;
		String log = String.valueOf(ts) + ";" + "freq" + ";" + sensor + ";";
		try {
			int samplesSize = samples.length;
			for (int i=0; i<samplesSize; i++){
				if(i==(samplesSize-1)){
					log += samples[i] + ";";
				}else{
					log += samples[i] + ",";
				}
			}
			log += numDominantFreqs + ";" + motionMode + ";";
			out.write(log + "\n");
			success = 1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG, "Problem adding frequency domain signal log");
			success = -1;
		}
		return success;
	}

	public int addStepsAndOrientation(long ts, int numSteps, double orientation, double diffOrientation){
		int success = 0;
		String log = String.valueOf(ts) + ";" + numSteps + ";" + String.format(Locale.ENGLISH, decFor, orientation) + ";" + String.format(Locale.ENGLISH, decFor, diffOrientation);
		try {
			out.write(log + "\n");
			success = 1;
		} catch (IOException e) {
			e.printStackTrace();
			Log.e(TAG, "Problem adding steps and orientation log");
			success = -1;
		}
		return success;
	}

	public int addAccelState(long ts, int state){
		int success = 0;
		String log = String.valueOf(ts) + ";" + state;
		try {
			out.write(log + "\n");
			success = 1;
		} catch (IOException e) {
			e.printStackTrace();
			Log.e(TAG, "Problem adding steps and orientation log");
			success = -1;
		}
		return success;
	}
}