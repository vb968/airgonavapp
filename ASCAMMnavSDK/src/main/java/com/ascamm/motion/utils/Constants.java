package com.ascamm.motion.utils;

import java.io.File;

import android.hardware.SensorManager;
import android.os.Environment;

/**
 * This class contains constants used by the program.
 */

public final class Constants {
	
	public static final boolean DEBUGMODE = false;
	public static String logsFilenameExt = ".txt";
	
	public static final String APP_DATA_FOLDER_NAME = "ASCAMM_Location";
	
	public static final File APP_DATA_FOLDER = new File (Environment.getExternalStorageDirectory() 
			+ "/" + APP_DATA_FOLDER_NAME);

	public static final int SECONDS_OF_SCANS_STATIC = 10;
	public static final int SECONDS_OF_SCANS_WALKING = 10; // Illa Diagonal
	//public static final int SECONDS_OF_SCANS_WALKING = 3; // Renault Valladolid, Casa Batllo, Mercat Rubi, Eurecat Proves
	//public static final int SECONDS_OF_SCANS_WALKING = 5; // Renault Eurecat, BCN Languages
	//public static final int SECONDS_OF_SCANS_WALKING = 6; // Mercat Rubi sense inercials (mode walking)
	//public static final int SECONDS_OF_SCANS_WALKING = 8; // CAR sense inercials (mode walking)
	public static final int SECONDS_OF_SCANS_GOING_TO_STATIC = 10;

	public static final int RETRY_WIFI_SCANS = 10000;
	public static final int TRY_INDOOR_NAV = 5000;
	
	//Presence detection parameters
	public static final float mWifiRssDetection = -65;
	//public static final float mBleRssDetection = -80;
	
	// Algorithm parameters
	public static final int mSettingKNumOfNeighbors = 3;
	public static final float mDefaultRss = -100;
	public static final float mDefaultRssStdev = 5;
	public static final float mMaxRssStdev = 5;
	//public static final float mMaxCount = 20;
	public static final int mMinCount = 3;
	public static final float mMinCountPercentage = 50;
	
	public static boolean mOutdoorDet = true;
	/**
	 *  Type of outdoor detection:
	 *  - RSS (RSS corresponding to the N APs with higher RSS below a threshold) or 
	 *	- PROB (Probabilities corresponding to the N Points with higher probability below a threshold)
	 */
	public static String mOutdoorDetMethod = "RSS";
	/** Parameters for detecting device out of the indoor area **/
	public static int mOutdoorRssNumAPs = 3;
	public static float mOutdoorRssThreshold = -85;
	public static double mOutdoorProbThreshold = Math.pow(10, -20);
	public static int mOutdoorProbNumPoints = 5;
	
	public static boolean mFloorDet = true;
	/**
	 *  Type of floor detection:
	 *  - RSS (Sum of RSS of the APs per floor) or 
	 *	- PROB (Sum of Probabilities of Points per floor) or
	 *	- PROB_VSAP (VSAP DB filter method used to obtain the floor number)
	 *  - ZONES_PROB
	 *  - ZONES_CORR
	 */
	public static String mFloorDetMethod = "PROB_VSAP";
	public static int nUnderground = 2;	// n equals the number of underground floors
	public static int mNumFloors = 7;
	
	/** 
	 * Type of filter to obtain database subset: 
	 * - OAP (Online Access Points) or 
	 * - SAP (Strongest Access Point) or
	 * - VSAP (Very Strongest Access Point) or
	 * - NVSAP (N Very Strongest Access Point) or
	 * - ZONES_PROB or
	 * - ZONES_CORR
	 */
	public static String	mDbFilt = "NVSAP";
	public static int	mNumVSAP = 4; // 7
	public static int 	mNumZONES = 5;
	
	// Particle filter parameters
	public static final int PF_NUM_OF_PARTICLES = 720; //720
	public static final double PF_TS = 0.5;
	public static final double PF_SIGMA_STATIC = 0.001;
	public static final double PF_SIGMA_WALKING = 0.1;
	//public static final double PF_SIGMA = 0.3;
	public static final double PF_SIGMA_0 = 0.05;
	public static final double PF_SIGMA_DIST = 6; // 3
	public static final double PF_RANGE = 20; // distance from particle to FP position to force a particle death (in meters) (default=9)

	//DBHelper parameters
	public static final float DB_N_STD = 1f;
	//public static final float DB_N_STD = 1.71f; // Casa Batllo

	// Sensor States
	public static int STATIC = 0;
	public static int WALKING = 2;
	public static int GOING_TO_STATIC = 1;

	public static int SENSOR_DELAY = SensorManager.SENSOR_DELAY_GAME; // 20 ms

	// Steps detection parameters
	public static final double MIN_FREQ_WALKING = 0.5; // Hz
	public static final double MAX_FREQ_WALKING = 3; // Hz

	public static String ACTION_KEEP_SERVICE_RUNNING = "com.ascamm.motion.ACTION_KEEP_SERVICE_RUNNING";
	public static int ALARM_KEEP_SERVICE_RUNNING = 1;
	public static int KEEP_SERVICE_RUNNING_SECONDS = 5*60; // 5 minutes

	public static int DEFAULT_POSITION_REFRESH_TIME = 1000; //milliseconds

	public static final double RAD_TO_DEG = 180.0d/Math.PI;

}
