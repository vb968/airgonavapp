package com.ascamm.motion.utils;

/**
 * This class contains constants used by the program.
 */

public final class WS_URLS {

	//public static final String URL_BASE = "http://edumotion.ascamm.com:9002/views/";
	public static final String URL_BASE = "http://ips-dt.eurecat.org/views/";
	//public static final String URL_BASE = "http://192.168.140.194:8000/views/";

	public static final String LOOKUP_SITE = URL_BASE + "lookupsite/";
	public static final String PDB_INFO = URL_BASE + "pdbinfo/";

}
