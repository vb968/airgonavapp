package com.ascamm.motion.utils;

import java.util.ArrayList;

import android.net.wifi.ScanResult;

import com.ascamm.motion.ble.BleType;

public class OnlineMeasure {

	private long ts;
	private boolean walking;
	private ArrayList<OnlineItem> mOnlineItems;

	public OnlineMeasure(long ts){
		this.ts = ts;
		this.mOnlineItems = new ArrayList<OnlineItem>();
	}

	public OnlineMeasure(long ts, boolean walking){
		this.ts = ts;
		this.walking = walking;
		this.mOnlineItems = new ArrayList<OnlineItem>();
	}
	
	public long getTs() {
		return ts;
	}

	public void setTs(long ts) {
		this.ts = ts;
	}

	public boolean isWalking() {
		return walking;
	}

	public void setWalking(boolean walking) {
		this.walking = walking;
	}

	public ArrayList<OnlineItem> getOnlineItems() {
		return mOnlineItems;
	}

	public void setOnlineItemsWiFi(ArrayList<ScanResult> mScanResults) {
		for(ScanResult sr : mScanResults){
			OnlineItem newOnlineItem = new OnlineItem(sr.SSID, sr.BSSID, sr.level);
			this.mOnlineItems.add(newOnlineItem);
		}
	}
	
	public void setOnlineItemsSim(ArrayList<SimScanResult> mScanResults) {
		for(SimScanResult sr : mScanResults){
			OnlineItem newOnlineItem = new OnlineItem(sr.SSID, sr.BSSID, sr.level);
			this.mOnlineItems.add(newOnlineItem);
		}
	}
	
	public void setOnlineItemsBle(ArrayList<BleType> mScanResults) {
		for(BleType sr : mScanResults){
			OnlineItem newOnlineItem = new OnlineItem(null, sr.getMac(), sr.getRssi());
			newOnlineItem.setRssArray(sr.getRssiValues());
			this.mOnlineItems.add(newOnlineItem);
		}
	}
	
	public void setOnlineItems(ArrayList<OnlineItem> mScanResults) {
		for(OnlineItem oi : mScanResults){
			OnlineItem newOnlineItem = new OnlineItem(oi.getSSID(), oi.getBSSID(), oi.getRSS(), oi.getRSSstdev(), oi.getCount());
			this.mOnlineItems.add(newOnlineItem);
		}
	}
	
}
