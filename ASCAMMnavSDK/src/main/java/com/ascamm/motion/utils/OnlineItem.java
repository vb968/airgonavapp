package com.ascamm.motion.utils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Class used to store a Scan Information
 * @author Dani, ASCAMM
 */
public class OnlineItem implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String mSSID;
	private String mBSSID;
	private float mRSS;
	private double mRSSstdev;
	private String mTech;
	private int mCount;
	private float mCountPercentage;
	private ArrayList<Float> rssValues;
	private ArrayList<Float> rssWeights; //needed for calculating the weighted standard dev
	private float mSumWeights; //needed for calculating the weighted average
	private float p_kalman = 0.5f; //variance of the state scalar (Kalman filter)
	private float minRss;
	private float maxRss;
	
	/*
	public OnlineItem(String bssid, float rss) {
		mBSSID = bssid;
		mRSS = rss;
		rssValues = new ArrayList<Float>();
	}
	*/
	public OnlineItem(String ssid, String bssid, float rss) {
		mSSID = ssid;
		mBSSID = bssid;
		mRSS = rss;
		rssValues = new ArrayList<Float>();
		rssWeights = new ArrayList<Float>();
	}
	/*
	public OnlineItem(String bssid, float rss, float rssStdev, int count) {
		mBSSID = bssid;
		mRSS = rss;
		mRSSstdev = rssStdev;
		mCount = count;
		rssValues = new ArrayList<Float>();
	}
	
	public OnlineItem(String bssid, float avgRss, int count){
		mBSSID = bssid;
		mRSS = avgRss;
		mCount = count;
		rssValues = new ArrayList<Float>();
	}
	*/
	
	public OnlineItem(String ssid, String bssid, float rss, int count) {
		mSSID = ssid;
		mBSSID = bssid;
		mRSS = rss;
		mCount = count;
		rssValues = new ArrayList<Float>();
		rssWeights = new ArrayList<Float>();
	}
	
	public OnlineItem(String ssid, String bssid, float rss, double rssStdev, int count) {
		mSSID = ssid;
		mBSSID = bssid;
		mRSS = rss;
		mRSSstdev = rssStdev;
		mCount = count;
		rssValues = new ArrayList<Float>();
		rssWeights = new ArrayList<Float>();
	}
	
	public OnlineItem(String ssid, String bssid, float rss, double rssStdev, int count, float percentage) {
		mSSID = ssid;
		mBSSID = bssid;
		mRSS = rss;
		mRSSstdev = rssStdev;
		mCount = count;
		mCountPercentage = percentage;
		rssValues = new ArrayList<Float>();
		rssWeights = new ArrayList<Float>();
	}
	
	public String getSSID() {
		return mSSID;
	}
	public String getBSSID() {
		return mBSSID;
	}
	public float getRSS() {
		return mRSS;
	}
	public double getRSSstdev() {
		return mRSSstdev;
	}
	public String getTech(){
		return mTech;
	}
	public int getCount(){
		return mCount;
	}
	public float getCountPercentage(){
		return mCountPercentage;
	}
	public ArrayList<Float> getRSSvalues(){
        return rssValues;
	}
	public ArrayList<Float> getRSSWeights(){
        return rssWeights;
	}
	public float getP_kalman(){
		return p_kalman;
	}
	public float getSumWeights(){
		return mSumWeights;
	}
	
	public void setSSID(String ssid){
		mSSID = ssid;
	}
	public void setBSSID(String bssid){
		mBSSID = bssid;
	}
	public void setRSS(float rss){
		mRSS = rss;
	}
	public void setRSSstdev(double rssStdev){
		mRSSstdev = rssStdev;
	}
	public void setTech(String tech){
		mTech = tech;
	}
	public void setCount(int count){
		mCount = count;
	}
	public void setCountPercentage(float countPercentage){
		mCountPercentage = countPercentage;
	}
	public void setRssArray(ArrayList<Float> rssArray){
		for(float value : rssArray){
			addRSSvalue(value);
        }
	}
	public void setWeightsArray(ArrayList<Float> weightsArray){
		for(float value : weightsArray){
			addRSSweight(value);
        }
	}
	public void setSumWeights(float SumWeights){
		mSumWeights = SumWeights;
	}

	public void addRSSvalue(float rss){
		rssValues.add(rss);
	}
	public void addRSSweight(float weight){
		rssWeights.add(weight);
	}
	public void setP_kalman(float p){
		p_kalman = p;
	}

	public float getMinRss() {
		return minRss;
	}

	public void setMinRss(float minRss) {
		this.minRss = minRss;
	}

	public float getMaxRss() {
		return maxRss;
	}

	public void setMaxRss(float maxRss) {
		this.maxRss = maxRss;
	}

	public String toString(){
		return "ssid: " + mSSID + " , mac: " + mBSSID + " , rss: " + mRSS + " , stdev: " + mRSSstdev + " , tech: " + mTech + " , count: " + mCount + " , count(%): " + mCountPercentage;
	}
}
