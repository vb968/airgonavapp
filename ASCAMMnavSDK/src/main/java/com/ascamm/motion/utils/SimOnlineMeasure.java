package com.ascamm.motion.utils;

import java.util.ArrayList;

import android.net.wifi.ScanResult;

import com.ascamm.motion.ble.BleType;

public class SimOnlineMeasure {

	private long ts;
	private boolean walking;
	private int sensorState;
	private ArrayList<OnlineItem> mOnlineItems;
	
	public SimOnlineMeasure(long ts, boolean walking, int sensorState){
		this.ts = ts;
		this.walking = walking;
		this.sensorState = sensorState;
		this.mOnlineItems = new ArrayList<OnlineItem>();
	}
	
	public long getTs() {
		return ts;
	}

	public void setTs(long ts) {
		this.ts = ts;
	}
	
	public boolean getWalking() {
		return walking;
	}

	public void setWalking(boolean walking) {
		this.walking = walking;
	}

	public int getSensorState() {
		return sensorState;
	}

	public void setSensorState(int sensorState) {
		this.sensorState = sensorState;
	}

	public ArrayList<OnlineItem> getOnlineItems() {
		return mOnlineItems;
	}

	public void setOnlineItemsWiFi(ArrayList<ScanResult> mScanResults) {
		for(ScanResult sr : mScanResults){
			OnlineItem newOnlineItem = new OnlineItem(sr.SSID, sr.BSSID,sr.level);
			this.mOnlineItems.add(newOnlineItem);
		}
	}
	
	public void setOnlineItemsSim(ArrayList<SimScanResult> mScanResults) {
		for(SimScanResult sr : mScanResults){
			OnlineItem newOnlineItem = new OnlineItem(sr.SSID, sr.BSSID,sr.level);
			this.mOnlineItems.add(newOnlineItem);
		}
	}
	
	public void setOnlineItemsBle(ArrayList<BleType> mScanResults) {
		for(BleType sr : mScanResults){
			OnlineItem newOnlineItem = new OnlineItem(null, sr.getMac(), sr.getRssi());
			this.mOnlineItems.add(newOnlineItem);
		}
	}
	
	public void setOnlineItems(ArrayList<OnlineItem> mScanResults) {
		for(OnlineItem oi : mScanResults){
			OnlineItem newOnlineItem = new OnlineItem(oi.getSSID(), oi.getBSSID(), oi.getRSS(), oi.getRSSstdev(), oi.getCount(), oi.getCountPercentage());
			newOnlineItem.setTech(oi.getTech());
			this.mOnlineItems.add(newOnlineItem);
		}
	}
	
}
