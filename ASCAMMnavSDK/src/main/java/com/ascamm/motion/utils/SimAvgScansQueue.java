package com.ascamm.motion.utils;

import java.util.ArrayList;
import java.util.LinkedList;

public class SimAvgScansQueue {
	private LinkedList<ArrayList<OnlineItem>> queue;
	private int NUM_SCANS = 0;
	
	public SimAvgScansQueue(){
		queue = new LinkedList<ArrayList<OnlineItem>>();
	}
	
	public LinkedList<ArrayList<OnlineItem>> generateScansQueue(){
		for(int j=0;j<NUM_SCANS;j++){
			ArrayList<OnlineItem> simScans = generateScans(j);
			queue.add(simScans);
		}
		return queue;
	}
	
	private ArrayList<OnlineItem> generateScans(int i){
		ArrayList<OnlineItem> scans = new ArrayList<OnlineItem>();
		/*
		if(i==0){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-66.0f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-62.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-76.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-71.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-84.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-89.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-74.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-89.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-85.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-87.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-86.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-90.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-95.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.0f));
		}
		if(i==1){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-67.666664f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-62.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-72.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.333336f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-77.666664f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-80.666664f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-70.333336f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-83.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.333336f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.666664f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.333336f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-86.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-90.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.0f));
		}
		if(i==2){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-67.5f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-61.5f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-71.75f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-64.75f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.5f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-79.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-70.25f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-82.5f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.5f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.5f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.25f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-86.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-90.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.0f));
		}
		if(i==3){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-67.333336f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-61.5f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-70.833336f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-64.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.166664f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-76.666664f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-70.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-82.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.333336f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-86.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-90.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.0f));
		}
		if(i==4){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-67.0f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-61.57143f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-71.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.714287f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.14286f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-76.42857f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-70.42857f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-81.71429f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.42857f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.42857f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.71429f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.5f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-90.0f));
		}
		if(i==5){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-67.44444f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-61.88889f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-71.111115f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.11111f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.22222f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-76.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-70.888885f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-81.55556f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.333336f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.44444f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.77778f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.55556f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-89.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-90.0f));
		}
		if(i==6){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-67.2f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-62.1f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-70.8f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.4f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.9f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-70.9f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-81.6f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.7f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.8f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.7f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.4f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-89.2f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-90.0f));
		}
		if(i==7){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-67.416664f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-62.166668f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-70.083336f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.75f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.583336f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-71.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-81.666664f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.75f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-93.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.583336f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.333336f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-89.42857f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-90.0f));
		}
		if(i==8){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-68.5f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-62.57143f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-70.07143f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.857143f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.92857f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.35714f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-71.21429f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-81.21429f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.71429f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-93.28571f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.57143f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.28571f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.111115f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-90.0f));
		}
		if(i==9){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-68.33333f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-62.933334f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-69.933334f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.933334f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-77.13334f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.6f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-70.8f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-81.26667f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.6f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-93.4f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.53333f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.26667f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.4f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-90.0f));
		}
		if(i==10){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-68.35294f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-63.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-68.82353f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.235294f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-77.23529f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.882355f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-70.117645f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-81.35294f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.47059f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-93.17647f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.47059f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.411766f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.833336f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-90.0f));
		}
		if(i==11){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-68.0f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-62.944443f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-68.55556f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.444443f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-77.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.833336f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-69.88889f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-81.22222f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.388885f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-93.05556f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.611115f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.22222f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-89.84615f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.76923f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-90.0f));
		}
		if(i==12){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-67.7f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-62.9f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-68.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.7f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.65f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.6f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-69.649994f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-81.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.75001f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.75f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.95f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-89.86667f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.666664f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-90.0f));
		}
		if(i==13){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-67.809525f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-62.857143f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-67.76191f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.761906f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.71429f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.52381f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-69.38097f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-80.90476f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.95238f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.61905f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.61905f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.85714f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-89.8125f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.6875f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-90.0f));
		}
		if(i==14){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-67.434784f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-62.869564f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-67.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.913044f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.47826f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-69.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-80.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.86957f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.73913f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.652176f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-89.72222f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.72222f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-90.0f));
		}
		if(i==15){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-67.26087f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-63.173912f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.652172f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.00001f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-68.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-80.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.652176f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.73913f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.391304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-89.833336f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
		}
		if(i==16){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-66.78261f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-63.73914f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-64.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-68.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-80.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.86956f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.86957f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.13043f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.1f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
		}
		if(i==17){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-66.565216f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-63.869564f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-64.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.782616f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-68.08695f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-80.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.04348f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.86957f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.13043f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.14286f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
		}
		if(i==18){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-66.13043f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.91304f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-64.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-67.6087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-80.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.95653f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.95652f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.13043f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.91304f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
		}
		if(i==19){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-66.04348f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-64.7826f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-67.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.95651f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.82609f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.95652f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.21739f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.86957f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
		}
		if(i==20){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-65.434784f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-63.52174f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.26088f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.95653f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-66.52173f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.391304f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.565216f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.82609f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.434784f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.73913f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-96.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
		}
		if(i==21){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-65.166664f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.75001f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-63.083332f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.62501f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.916664f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.083336f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-66.12499f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.666664f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.12499f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.791664f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.666664f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.833336f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.416664f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.583336f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-96.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-95.0f));
		}
		if(i==22){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-64.782616f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-62.652172f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-66.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.2174f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-65.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.99999f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.73913f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.391304f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.478264f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-96.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
		}
		if(i==23){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-63.652172f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-65.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-61.826088f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-66.52175f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.47827f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.30435f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-64.82608f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.78261f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.91303f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.82609f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.13043f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.26087f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-96.28571f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
		}
		if(i==24){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-63.47826f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-65.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-61.52174f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-66.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.130424f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-64.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.78261f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.78261f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-89.91304f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-96.5f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.27273f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
		}
		if(i==25){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.95652f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-65.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-61.52174f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-66.82608f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-64.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.608696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.95652f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.565216f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.478264f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-96.8f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.36364f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
		}
		if(i==26){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.95652f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-66.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-61.347828f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-66.91304f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-64.52174f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.608696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.652176f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.73913f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-96.90909f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.5f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
		}
		if(i==27){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.782608f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-66.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-61.130436f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-66.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.00001f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-64.17392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.739136f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.78261f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.08696f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.40909f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
		}
		if(i==28){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.47826f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-66.91304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-61.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-67.08695f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.04349f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-64.13044f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.04348f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.82609f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.304344f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.27273f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.181816f));
		}
		if(i==29){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.391304f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-67.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-67.43477f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.47825f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-64.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.86957f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.21739f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.73913f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.318184f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.09091f));
		}
		if(i==30){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.391304f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-67.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.695652f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-67.60869f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.60869f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.86957f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.304344f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.608696f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.545456f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.95455f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-95.0f));
		}
		if(i==31){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.347828f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-66.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-67.91305f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-78.91304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.78261f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.478264f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.434784f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.90909f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.772736f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.333336f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==32){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.391304f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-66.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.47826f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-68.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.39131f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.652172f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-78.95653f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.782585f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.391304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.521736f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.818184f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.772736f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.25f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==33){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.434784f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-66.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-68.30435f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.00001f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.52174f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.13044f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.95652f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.695656f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.78261f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.72727f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.318184f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.166664f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==34){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.416668f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-66.958336f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.333332f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-68.54166f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.416664f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.00001f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.416668f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.125f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.708336f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.041664f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.916664f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.125f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.434784f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.73913f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.14286f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==35){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.434784f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-67.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.260868f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-68.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.95651f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.260868f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.65217f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.21739f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.95652f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.478264f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.318184f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-93.13636f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.14286f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.125f));
		}
		if(i==36){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.434784f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-67.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.260868f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-68.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.08696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.95652f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.82609f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.40909f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.26667f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-93.28571f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.14286f));
		}
		if(i==37){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.391304f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-67.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.260868f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-68.34783f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.91304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.65217f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.95652f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.86957f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.5f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.333336f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-93.4f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.14286f));
		}
		if(i==38){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.434784f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-67.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-68.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.00001f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.82609f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.95652f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.681816f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.333336f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-93.31579f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.14286f));
		}
		if(i==39){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.391304f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-67.13044f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-68.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.39131f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.782616f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.695656f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.26667f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-93.10526f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.14286f));
		}
		if(i==40){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.347828f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-67.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.260868f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-68.34782f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.08697f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.08696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.04348f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.95652f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.68421f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.14286f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.14286f));
		}
		if(i==41){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.347828f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-66.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.304348f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-68.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.95652f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.04348f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.08696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.68421f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.07692f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.14286f));
		}
		if(i==42){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.217392f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-66.56522f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.260868f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-68.17393f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.17392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.91303f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.17391f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.434784f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.789474f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.14286f));
		}
		if(i==43){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.130436f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-66.17392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.260868f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-68.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.91305f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.82609f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.21739f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.608696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-93.05f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.14286f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.14286f));
		}
		if(i==44){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.086956f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-66.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.304348f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-67.99999f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.08697f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.130436f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.00001f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.478264f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.652176f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-93.35f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.333336f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.125f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.14286f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==45){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.04348f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-66.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.304348f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-67.91304f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.65217f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.086956f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.86956f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.04349f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.695656f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.521736f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-93.4f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.4f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.25f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==46){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.913044f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-66.13045f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-67.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.04347f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.13043f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.521736f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.26087f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-93.35f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.5f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.4f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==47){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.869564f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-66.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.52174f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-67.52173f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.347828f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-78.95651f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.26087f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.434784f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.17391f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-93.3f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.35f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.5f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==48){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.739132f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-65.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.52174f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-67.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.47826f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.08695f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.78259f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.91303f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.17391f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.6f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.5f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.2f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.5f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==49){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.68182f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-65.40909f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-60.68182f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-66.909096f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-74.954544f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.36364f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.863636f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.318184f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-79.77272f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.681816f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.90909f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.27273f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.052635f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.75f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-96.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.5f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==50){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.913044f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-65.34783f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-61.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-66.95653f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.39131f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.695656f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.91304f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.434784f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.7f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-95.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.7f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-97.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.5f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==51){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.909092f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-65.318184f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-61.31818f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-67.363625f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.5f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.90909f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.454544f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-79.954544f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.5f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.63636f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.954544f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.681816f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.5f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-95.09091f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.42105f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-97.2f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.5f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==52){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.086956f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-65.04347f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-61.826088f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-67.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.086945f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.260868f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-80.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.434784f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.04348f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.478264f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.40909f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.3f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-97.42857f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.333336f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-95.09091f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.5f));
		}
		if(i==53){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.045456f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-65.181816f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-62.18182f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-66.95454f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.13636f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.40909f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-63.227272f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-80.95454f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.77273f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.545456f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.95454f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.545456f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.27273f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.73684f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-97.5f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.6f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-95.09091f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.6f));
		}
		if(i==54){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.086956f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.7826f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-62.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-66.65218f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.6087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-62.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-81.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.91303f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.478264f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.08696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.608696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.391304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.2f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.833336f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-97.5f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-95.09091f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.666664f));
		}
		if(i==55){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.045456f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.318184f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-62.909092f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-66.5f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.818184f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.77273f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-62.81818f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-81.90909f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.954544f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.5f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.86364f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.40909f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.40909f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.1579f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-97.5f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-95.09091f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.85714f));
		}
		if(i==56){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.04348f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-63.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-66.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-77.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.95653f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-62.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-82.43478f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.478264f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.82609f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.26087f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.086945f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.25f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-97.5f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-95.09091f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-97.0f));
		}
		if(i==57){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.090908f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.77273f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-63.590908f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-66.454544f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-77.63636f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-76.13636f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-62.409092f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-83.045456f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.181816f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.454544f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.04546f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.954544f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.77273f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.1579f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-97.5f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-95.09091f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-97.0f));
		}
		if(i==58){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.173912f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.826096f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-63.956516f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-66.2174f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-78.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-76.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-62.130436f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-83.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.434784f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.13043f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.91304f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.521736f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.95f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-97.5f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-95.09091f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-97.0f));
		}
		if(i==59){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.18182f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.59091f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.36364f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-66.090904f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-79.181816f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-76.63636f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-61.81818f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.13636f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.36364f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.454544f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.13636f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.86364f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.22727f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.04762f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-97.5f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-95.2f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-97.0f));
		}
		if(i==60){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.173912f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-66.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-79.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-76.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-61.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.478264f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.21739f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.26087f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.782616f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-97.5f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-95.333336f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-97.0f));
		}
		if(i==61){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.272728f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.5f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.954544f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.863625f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-79.77273f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.318184f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-61.363636f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.49999f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.36364f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.318184f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.59091f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.86364f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.318184f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.77273f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-97.5f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-95.71429f));
		}
		if(i==62){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.217392f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.13044f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-61.086956f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.91304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.26087f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.86957f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.04348f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.565216f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.652176f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-97.5f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-96.0f));
		}
		if(i==63){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.260868f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.30436f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.56521f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.69565f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.782608f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.565216f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.13043f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.347824f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.86957f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-97.71429f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-96.0f));
		}
		if(i==64){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.260868f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.4348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.739136f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.56521f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-81.00001f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.304348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.782616f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.26087f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.304344f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.08696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.86957f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-98.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-96.0f));
		}
		if(i==65){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.0f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.91305f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.43477f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.52174f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.3913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.86957f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.608696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.21739f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.17391f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.95652f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-98.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-96.0f));
		}
		if(i==66){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.954544f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-63.863636f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.86364f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-64.99999f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.954544f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.5f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.227272f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.72727f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.045456f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.22727f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.77272f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.27273f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.09091f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.318184f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-98.0f));
		}
		if(i==67){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.782608f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-63.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.08695f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.69565f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.39131f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.304348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.91304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.304344f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.04348f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.304344f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.17391f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.47825f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-98.0f));
		}
		if(i==68){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.826088f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-63.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.304348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.95651f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.347824f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.08697f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.21739f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.26087f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-93.304344f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
		}
		if(i==69){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.782608f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-63.47826f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.13044f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.347828f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.00001f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.521736f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.95652f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.04348f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.434784f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.95652f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
		}
		if(i==70){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.826088f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-63.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.173904f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.304348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.478264f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.04348f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.95651f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.60869f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.78261f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
		}
		if(i==71){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.826088f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-63.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-81.173904f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.39131f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.73913f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.21739f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.95652f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.347824f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
		}
		if(i==72){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.826088f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-63.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-81.00001f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.39131f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.347828f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.08697f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.652176f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.17391f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.17391f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
		}
		if(i==73){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.782608f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-63.47826f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.65217f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.39131f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.99999f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.652176f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.304344f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.91304f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.95652f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.08696f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
		}
		if(i==74){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.869564f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-63.52174f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.60869f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.56522f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.47826f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.652176f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.478264f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.82609f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.00001f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.04348f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
		}
		if(i==75){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.95652f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-63.739136f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.34783f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.17392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.73913f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.26087f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.565216f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.13043f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.08696f));
		}
		if(i==76){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.95652f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.34783f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.17392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.78261f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.13043f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.565216f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.21739f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.13045f));
		}
		if(i==77){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.086956f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.13045f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.86957f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.434784f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.478264f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.17391f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.13043f));
		}
		if(i==78){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.130436f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.60869f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.695656f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.08697f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.86957f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.454544f));
		}
		if(i==79){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.304348f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.826096f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.652172f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.82609f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.73913f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.04348f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.78261f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.36364f));
		}
		if(i==80){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.652172f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.3913f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.69565f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.695652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-86.04347f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.478264f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.73913f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.04348f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.608696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.18182f));
		}
		if(i==81){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.52174f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-64.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.34783f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.739136f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.782608f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.91305f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-80.91304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-91.26087f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.82609f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.08696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.434784f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.0f));
		}
		if(i==82){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.739132f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-63.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-60.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.826096f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.217384f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-94.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.08696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.17391f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.86363f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.0f));
		}
		if(i==83){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.47826f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-63.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-65.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-80.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-59.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.17392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.73913f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-94.21739f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.08696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.08696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
		}
		if(i==84){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-62.086956f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-63.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-64.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-79.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-77.04347f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-59.130436f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.478264f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-94.304344f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.91304f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.818184f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
		}
		if(i==85){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.625f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-62.375f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.08334f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-64.458336f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-79.00001f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-76.625f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-58.583332f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.458336f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.5f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.375f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-94.291664f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.041664f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.041664f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.73913f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.666664f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
		}
		if(i==86){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-61.347828f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-61.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.08697f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-64.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-78.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-76.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-58.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.08696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-94.434784f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.21739f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.59091f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.42857f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.0f));
		}
		if(i==87){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-60.916668f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-61.083332f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.125f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.958332f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-77.70832f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.833336f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-57.791668f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.33333f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.83333f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.083336f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-94.25f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.166664f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.208336f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.608696f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.111115f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.125f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.5f));
		}
		if(i==88){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-60.95652f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-60.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.782608f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-77.173904f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-75.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-57.347828f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.34783f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.13043f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-94.304344f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.26087f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.347824f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.63636f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.44444f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.0f));
		}
		if(i==89){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-60.791668f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-60.083332f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.083336f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.541668f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.791664f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.99999f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-56.916668f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.33333f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.041664f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.125f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-94.166664f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.25f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.291664f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.73913f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.166664f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.90909f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.6f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.0f));
		}
		if(i==90){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-60.47826f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-59.47826f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-76.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-56.47826f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.99999f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-94.26086f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.17391f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.434784f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.72727f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.23077f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.833336f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-96.166664f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.0f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
		}
		if(i==91){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-60.083332f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-58.625f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.041664f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.041668f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.91666f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-74.041664f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-56.166668f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.08333f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.208336f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.041664f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-94.291664f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-92.04168f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.541664f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.73913f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.4f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.71429f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-96.875f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.166664f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
		}
		if(i==92){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-59.652172f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-58.086956f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.08695f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.782608f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-75.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-73.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.86957f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-94.04348f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.95651f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.652176f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.72727f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.5625f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.666664f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-96.77778f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.28571f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
		}
		if(i==93){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-59.217392f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-56.913044f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-74.913055f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-73.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.260868f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.826096f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.608696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.78261f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.78261f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.82609f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.608696f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.833336f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.64706f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-96.63636f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.22222f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
		}
		if(i==94){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.782608f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-56.260868f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.173912f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-74.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.086956f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.652176f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.73913f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.695656f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.78261f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.73913f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.947365f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.611115f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-96.583336f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.2f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.2f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
		}
		if(i==95){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.391304f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-61.695652f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-74.043465f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.652172f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.826096f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.82609f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.478264f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.652176f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.73913f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.55f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-96.5f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.166664f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.42857f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.05f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
		}
		if(i==96){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.541668f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.541668f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.208336f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-61.666668f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-73.875f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.958336f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.375f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.958336f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.62499f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.833336f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-93.125f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.583336f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.75f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.16668f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.5f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.55556f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.15385f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-96.5f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.05f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
		}
		if(i==97){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.95652f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.13042f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-61.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-73.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.04348f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.695656f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.608696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.86957f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.304344f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.478264f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.6f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.15385f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-96.5f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.0f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
		}
		if(i==98){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.086956f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.52174f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-61.782608f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-73.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.6087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.82608f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.08696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.434784f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.608696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.956535f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.21739f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.478264f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.6f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.15385f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-96.5f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.94444f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
		}
		if(i==99){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.086956f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-73.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.695652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.91305f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.347824f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.478264f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-92.17391f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.304344f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.478264f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.05882f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.07143f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.6f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-96.5f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
		}
		if(i==100){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.173912f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.086956f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-73.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.826088f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.91304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.21739f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.521736f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.99999f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.347824f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.17647f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.9375f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.52381f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.6f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-96.5f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
		}
		if(i==101){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.217392f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.086956f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-73.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.826088f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.91303f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.95651f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-92.04348f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.565216f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.91304f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.391304f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.29412f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.882355f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.55f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.6f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-96.5f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
		}
		if(i==102){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.52174f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-61.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-73.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.95651f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.39131f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.91304f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.347824f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.411766f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.8421f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.454544f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.55556f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-96.666664f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
		}
		if(i==103){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.347828f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-61.913044f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-73.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.956535f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.086956f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.7826f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.95651f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.26087f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.78261f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.304344f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.588234f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.73684f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.333336f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.411766f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-96.90909f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
		}
		if(i==104){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.0f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-61.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.99999f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.130436f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.86957f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.95652f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.17391f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.695656f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.391304f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.70588f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.73684f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.14286f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.29412f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-97.0f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
		}
		if(i==105){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.04348f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.347828f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.30435f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.91304f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.13044f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.130436f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.82609f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.91304f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.26087f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.608696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.347824f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.76471f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.789474f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.066666f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.411766f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-96.75f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
		}
		if(i==106){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.260868f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.304348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.34782f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.086956f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.130436f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.695656f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.695656f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.347824f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.434784f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.26087f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.8421f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.64706f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.625f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
		}
		if(i==107){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.304348f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.304348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.086956f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.130436f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.17392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.652176f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.565216f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.391304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.391304f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.21739f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.117645f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.8421f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.76471f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.5f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
			scans.add(new OnlineItem("f0:25:72:71:fc:20",-92.0f));
		}
		if(i==108){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.260868f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.086956f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.956535f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.086956f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.17392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.56521f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.695656f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.21739f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.478264f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.304344f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.26087f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.117645f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.947365f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.25f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.8f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
		}
		if(i==109){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.260868f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.130436f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.173904f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.565216f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.043465f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.521736f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.26087f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.21739f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.125f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-91.85714f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.90909f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
		}
		if(i==110){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.086956f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.260868f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.173912f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.565216f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.04349f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.521736f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.13043f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.13043f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.833336f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.052635f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.875f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.666664f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
		}
		if(i==111){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.136364f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.272728f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.09091f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.18182f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.5f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.22727f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.22727f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.59091f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.545456f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.09091f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.545456f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.045456f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.045456f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.789474f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.10526f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.888885f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.181816f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.28571f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
		}
		if(i==112){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.130436f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.95651f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.173912f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.56523f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.565216f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.17391f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.521736f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.04348f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.04348f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.190475f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.25f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.75f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.888885f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
		}
		if(i==113){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.04348f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.173912f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.956535f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.43479f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.173904f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.521736f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.08696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.521736f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.181816f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.333336f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.75f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.888885f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
			scans.add(new OnlineItem("f0:25:72:71:fc:21",-90.0f));
		}
		if(i==114){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.0f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.086956f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.34783f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.695656f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.478264f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.86957f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.13043f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.21739f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.583336f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.789474f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.888885f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==115){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.045456f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.227272f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.77273f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.090908f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.318184f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.90909f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.36364f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.545456f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.72727f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.09091f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.22728f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.86363f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.22727f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.36364f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.61539f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.882355f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.888885f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==116){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.913044f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.130436f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.173904f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.78261f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.304344f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.04348f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.95651f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.304344f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.478264f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.666664f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.9375f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.888885f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==117){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.636364f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.18182f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.63636f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.272728f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.181816f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.86364f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.863636f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.545456f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.18182f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.72727f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.36364f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.36364f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.59091f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.6875f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.46667f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.888885f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==118){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.608696f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.173912f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.52173f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.347828f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.30435f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.826096f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.826088f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.82609f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.347824f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.08696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.08696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.434784f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.73913f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.9375f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.70588f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.888885f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==119){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.652172f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.130436f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.347828f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.869564f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.82609f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.26087f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.08696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.08696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.347824f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.4375f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.8f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.75f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.809525f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==120){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.608696f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.130436f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.826088f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.86957f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.26087f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.13043f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.13043f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.347824f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.6875f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.72727f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.75f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.8f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==121){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.47826f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.130436f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.347828f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.782608f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.95652f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.26087f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.26087f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.26087f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.434784f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.125f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.63636f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.6875f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.77778f));
		}
		if(i==122){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.47826f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.130436f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.30434f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.347828f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.956535f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.95651f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.26087f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.347824f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.304344f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.478264f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.75f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.545456f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.625f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.76471f));
		}
		if(i==123){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.739132f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.173912f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.26086f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.34784f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.60869f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.00001f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.347824f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.521736f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.391304f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.521736f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.25f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.5f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.70588f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.4f));
		}
		if(i==124){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.913044f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.52174f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-83.04347f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.0f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.434784f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.478264f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.478264f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.434784f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.375f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.42105f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.64706f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.333336f));
		}
		if(i==125){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.695652f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.69566f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.60869f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.08696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.434784f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.391304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.652176f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.347824f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.625f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.57143f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.588234f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.14286f));
		}
		if(i==126){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.434784f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.695652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.13043f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.434784f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.434784f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.652176f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.304344f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.5625f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.666664f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.52941f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.0f));
		}
		if(i==127){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.391304f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.260868f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.91303f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.695652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.04348f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.391304f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.565216f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.521736f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.26087f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.411766f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.85714f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.411766f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.0f));
		}
		if(i==128){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.391304f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.304348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.69565f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.04348f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.478264f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.608696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.565216f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.26087f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.333336f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.95238f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.411766f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.0f));
		}
		if(i==129){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.652172f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.08696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.39131f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.652176f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.565216f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.25001f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.411766f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.25f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.22727f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-93.0f));
		}
		if(i==130){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.782608f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.13043f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.21739f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.652176f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.521736f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.04762f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.35294f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.44444f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.190475f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-93.0f));
		}
		if(i==131){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.826088f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.52174f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.869564f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.2174f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.95651f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.17391f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.652176f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.347824f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.78261f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.47059f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.72727f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.09524f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-93.0f));
		}
		if(i==132){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.826088f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.52174f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.869564f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.217384f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.95651f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.782608f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.82608f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.86957f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.26087f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.565216f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.347824f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.521736f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.64706f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.833336f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.047615f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-93.0f));
		}
		if(i==133){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.04348f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.47826f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.913044f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.695656f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.21739f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.478264f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.26087f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.35294f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.14286f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-93.0f));
		}
		if(i==134){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.045456f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.454544f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.77273f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.954544f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.36364f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.09091f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.727272f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.545456f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.99999f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.59091f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.181816f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.545456f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.27273f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.5f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.22222f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.25f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.9375f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.07692f));
		}
		if(i==135){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.04348f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.82608f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.13044f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.04349f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.826088f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.608696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.21739f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.39131f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.21739f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.3f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.666664f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.94118f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.22727f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.166664f));
		}
		if(i==136){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.0f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.590908f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.90909f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.13637f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.09091f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.81818f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.59091f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.681816f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.545456f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.22727f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.31819f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.09091f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.333336f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.899994f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-93.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.35f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.4f));
		}
		if(i==137){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-58.04348f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.826096f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.869564f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.608696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.21739f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.391304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.08696f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.391304f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.85714f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-93.05882f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.454544f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.42105f));
		}
		if(i==138){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.772728f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.636364f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.86363f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.045456f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.045456f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.00001f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.81818f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.454544f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.86363f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.59091f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.22727f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.318184f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.86363f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.40909f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.9f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-93.25f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.545456f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.588234f));
		}
		if(i==139){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.565216f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.86957f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.086956f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.04347f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.782608f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.43479f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.91304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.521736f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.13043f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.521736f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.78261f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.521736f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.952385f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-93.35294f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.46154f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.4375f));
		}
		if(i==140){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.909092f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.590908f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.86363f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.136364f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.27273f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-71.99999f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.772728f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.27273f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.86363f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.318184f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.99999f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.63636f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.72727f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.59091f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.05f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-93.25f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.42857f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.26667f));
		}
		if(i==141){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.913044f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.86956f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.173912f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.34783f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.826088f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.2174f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.91304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.26087f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.95652f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.652176f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.78261f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.652176f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.190475f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-93.05882f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.25f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.625f));
		}
		if(i==142){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.954544f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.545456f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.81819f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.227272f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.50001f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.13636f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.863636f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-85.09091f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.99999f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.22727f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.95455f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.63636f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.77273f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.72727f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-92.2f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.8125f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.17647f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.8f));
		}
		if(i==143){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.826088f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.52174f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.56521f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.869564f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.91304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.95651f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.13043f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.04348f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.652176f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.565216f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.695656f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.952385f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.52941f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.10526f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.875f));
		}
		if(i==144){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.545456f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-55.545456f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.818184f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.227272f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.59091f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.27273f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.909092f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.77272f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.90909f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-88.95454f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.181816f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.63636f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.63636f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.681816f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.85714f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.44444f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.111115f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.73333f));
		}
		if(i==145){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.217392f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-56.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.73913f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.34783f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.913044f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.69565f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.08696f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.21739f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.73913f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.73913f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.565216f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.652176f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.25f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.052635f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.1875f));
		}
		if(i==146){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.18182f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-56.045456f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.72727f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.18182f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.72727f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.318184f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.681816f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.72727f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.13637f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.18182f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.86363f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.77273f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.36364f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.63636f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.14286f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.94444f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.6f));
		}
		if(i==147){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-57.130436f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-56.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.130436f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.52173f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-54.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.17391f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.13043f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.86955f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.82609f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.08696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.521736f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.21739f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.875f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.888885f));
		}
		if(i==148){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.608696f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-56.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.304344f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.08696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.91304f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.13043f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.26087f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.21739f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.3125f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.75f));
		}
		if(i==149){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.608696f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-56.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-63.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.60869f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.391304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.08697f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.91303f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.95651f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.17391f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-91.04348f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.21739f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.5625f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.76471f));
		}
		if(i==150){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.52174f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-56.869564f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.913044f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.086956f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.173904f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.347824f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.95652f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-90.95652f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.08696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.608696f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.391304f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.882355f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.789474f));
		}
		if(i==151){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.52174f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-57.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-64.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.913044f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.260868f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.65217f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.391304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.04349f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.86957f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.04348f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.08696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.391304f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.478264f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.94444f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-98.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.8f));
		}
		if(i==152){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.391304f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-57.52174f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.782608f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.30435f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.47826f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.478264f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.13043f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.695656f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.13043f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.17391f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.391304f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.565216f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.8f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.71429f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-94.0f));
		}
		if(i==153){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.363636f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-57.68182f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.318184f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.636364f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.86363f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.454544f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.636364f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.04546f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.40909f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.59091f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.045456f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.454544f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.318184f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.13636f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.40909f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.545456f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.5f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.95f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.77778f));
		}
		if(i==154){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.434784f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-57.869564f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.47826f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.82609f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.56521f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.826088f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.695656f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.434784f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.17391f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.26087f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.26087f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.434784f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.565216f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.2f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.86364f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.76471f));
		}
		if(i==155){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.130436f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-57.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-65.826096f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.69565f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-55.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.73913f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.434784f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-91.13043f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.26087f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.17391f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.347824f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.565216f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.09091f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.818184f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.75f));
		}
		if(i==156){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.0f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-58.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.13043f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.347828f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-56.260868f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.82609f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.347824f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.95652f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.08696f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-96.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.08696f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.63636f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-92.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.63636f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.85714f));
		}
		if(i==157){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.04348f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-58.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.347824f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.260868f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.65217f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-56.304348f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.86957f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.26087f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.82609f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.13043f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.91304f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.08696f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.5f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-92.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.818184f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-94.92308f));
		}
		if(i==158){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.0f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-58.869564f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.65217f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.65217f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-56.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.00001f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.26087f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.95652f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.08697f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.695656f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.347824f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.82609f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.17391f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.36364f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-92.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-93.181816f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-97.0f));
		}
		if(i==159){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.0f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-58.869564f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.260868f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.65218f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-56.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.0f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.304344f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.043465f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.0f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.695656f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.304344f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.78261f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.21739f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.36364f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-92.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-93.36364f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-96.76923f));
		}
		if(i==160){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.130436f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-58.739132f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.56521f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.304348f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.43477f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.60869f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-56.52174f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.13045f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.34783f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.95652f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.91304f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.521736f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.304344f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.78261f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.21739f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-93.181816f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-93.5f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-96.4f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-92.0f));
		}
		if(i==161){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.130436f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-59.086956f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.65217f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.304348f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.434784f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.652176f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-56.47826f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.391304f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.91304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.82609f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.478264f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.434784f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.82609f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.17391f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.90909f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-93.454544f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-96.25f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-92.0f));
		}
		if(i==162){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.130436f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-59.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.78261f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.304348f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.65216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-56.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.17391f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.39131f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.95652f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.82609f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.391304f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.347824f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.82609f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.13043f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.36364f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-93.09091f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-96.05556f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-92.0f));
		}
		if(i==163){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.272728f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-59.31818f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.954544f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.272728f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-72.72727f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.59091f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-56.636364f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.09091f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-82.318184f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.90909f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.81818f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.318184f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.27273f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.818184f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-90.09091f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.23808f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.90476f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-95.76471f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-92.0f));
		}
		if(i==164){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.52174f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-59.347828f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.608696f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-73.04348f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.478264f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-57.217392f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.95652f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.95651f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.86957f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.304344f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.391304f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.78261f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-89.91304f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-92.272736f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-95.55556f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.71429f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-92.0f));
		}
		if(i==165){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.590908f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-59.409092f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.54544f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.18182f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-73.13636f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.5f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-57.590908f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.27273f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.818184f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-89.954544f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-90.954544f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-90.13636f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.36364f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.77273f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-89.77273f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.95238f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-95.23529f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-92.85714f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.47369f));
		}
		if(i==166){
			scans.add(new OnlineItem("fc:fb:fb:d8:61:31",-56.695652f));
			scans.add(new OnlineItem("fc:fb:fb:d8:61:30",-59.782608f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:cb",-66.521736f));
			scans.add(new OnlineItem("1c:7e:e5:48:22:71",-62.260868f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:53",-73.08696f));
			scans.add(new OnlineItem("1c:7e:e5:48:25:55",-72.47826f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:0b",-57.565216f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:17",-84.21739f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:11",-81.695656f));
			scans.add(new OnlineItem("1c:7e:e5:48:23:91",-90.04348f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f0",-91.08696f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:21",-89.95652f));
			scans.add(new OnlineItem("34:a8:4e:fd:55:f1",-91.17391f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f0",-95.73913f));
			scans.add(new OnlineItem("fc:fb:fb:6b:6b:20",-89.695656f));
			scans.add(new OnlineItem("f0:25:72:70:ed:00",-91.72727f));
			scans.add(new OnlineItem("08:17:35:02:71:00",-94.0f));
			scans.add(new OnlineItem("08:17:35:02:71:01",-95.05882f));
			scans.add(new OnlineItem("f0:25:72:cb:d6:f1",-95.0f));
			scans.add(new OnlineItem("f0:25:72:70:ed:01",-92.333336f));
		}
		*/
		return scans;
		
	}
	
	public ArrayList<OnlineItem> getScans(int i){
		return queue.get(i);
	}
	
	public int getQueueSize(){
		return queue.size();
	}
}
