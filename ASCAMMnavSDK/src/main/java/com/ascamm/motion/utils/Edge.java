package com.ascamm.motion.utils;

import java.io.Serializable;

public class Edge implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public int edgeId;
	public int startNodeId;
	public int endNodeId;
	public Point3DWithFloor startNode;
	public Point3DWithFloor endNode;
	public float length;
	public float lineAngle1, lineAngle2;
	public double a = 0, b = 0, dX = 0, dY = 0, dZ = 0, dFloor = 0;
	private int status=0;
	private double angleZ = 0; // in radians
	
	public Edge(int edgeId, int startNodeId, int endNodeId, double startX, double startY, double startZ, double startFloor, double endX, double endY, double endZ, double endFloor, float length){
		this.edgeId = edgeId;
		this.startNodeId = startNodeId;
		this.endNodeId = endNodeId;
		this.startNode = new Point3DWithFloor(startX, startY, startZ, startFloor);
		this.endNode = new Point3DWithFloor(endX, endY, endZ, endFloor);
		this.length = length;
		initialCalculations();
	}

	public Edge(int edgeId, int startNodeId, int endNodeId, Point3DWithFloor startNode, Point3DWithFloor endNode, float length){
		this.edgeId = edgeId;
		this.startNodeId = startNodeId;
		this.endNodeId = endNodeId;
		this.startNode = startNode;
		this.endNode = endNode;
		this.length = length;
		initialCalculations();
	}

	public Edge(int startNodeId, int endNodeId){
		this.startNodeId = startNodeId;
		this.endNodeId = endNodeId;
	}

	private void initialCalculations(){
		dX = endNode.x - startNode.x;
		dY = endNode.y - startNode.y;
		dZ = endNode.z - startNode.z;
		dFloor = endNode.getFloor() - startNode.getFloor();
		if(dFloor!=0) {
			double lengthXY = Math.sqrt(Math.pow(dX, 2) + Math.pow(dY, 2));
			if(lengthXY==0){
				angleZ = Math.PI/2;
			}
			else{
				angleZ = Math.atan(Math.abs(dZ)/lengthXY);
			}
		}
	}

	public int getEdgeId() {
		return edgeId;
	}

	public void setEdgeId(int edgeId) {
		this.edgeId = edgeId;
	}

	public int getStartNodeId() {
		return startNodeId;
	}

	public void setStartNodeId(int startNodeId) {
		this.startNodeId = startNodeId;
	}

	public int getEndNodeId() {
		return endNodeId;
	}

	public void setEndNodeId(int endNodeId) {
		this.endNodeId = endNodeId;
	}

	public Point3DWithFloor getStartNode() {
		return startNode;
	}

	public void setStartNode(Point3DWithFloor startNode) {
		this.startNode = startNode;
	}

	public Point3DWithFloor getEndNode() {
		return endNode;
	}

	public void setEndNode(Point3DWithFloor endNode) {
		this.endNode = endNode;
	}
	
	public float getLength() {
		return length;
	}

	public void setLength(float length) {
		this.length = length;
	}
	
	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public float getLineAngle1() {
		return lineAngle1;
	}

	public void setLineAngle1(float lineAngle1) {
		this.lineAngle1 = lineAngle1;
	}

	public float getLineAngle2() {
		return lineAngle2;
	}

	public void setLineAngle2(float lineAngle2) {
		this.lineAngle2 = lineAngle2;
	}

	public double getdX() {
		return dX;
	}

	public void setdX(double dX) {
		this.dX = dX;
	}

	public double getdY() {
		return dY;
	}

	public void setdY(double dY) {
		this.dY = dY;
	}

	public double getdZ() {
		return dZ;
	}

	public void setdZ(double dZ) {
		this.dZ = dZ;
	}

	public double getdFloor() {
		return dFloor;
	}

	public void setdFloor(double dFloor) {
		this.dFloor = dFloor;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public double getAngleZ() {
		return angleZ;
	}

	public void setAngleZ(double angleZ) {
		this.angleZ = angleZ;
	}

	public String toString(){
		return "Edge: ("+edgeId+","+startNodeId+","+endNodeId+","+lineAngle1+","+lineAngle2+","+length+","+angleZ+")";
	}
	
	
}
