package com.ascamm.motion.utils;

import java.util.ArrayList;

public interface IndoorPositionsListener {
	public void newPositionAvailable(ArrayList<ResponseObject> locationResponses);
}
