package com.ascamm.motion.utils;

public final class Settings {
	public static boolean pdbFromAssets = false;

	public static boolean useWifi = false;
	public static boolean useBle = true;
	public static boolean useIBeacons = true;
	public static boolean useGps = true;
	public static boolean useKalmanRSSI = false;
	public static boolean useWeightedRSSI = false;
	public static boolean useDetections = false;
	public static float bleRssDetection = -58;
	public static boolean useSensors = true;
	public static boolean siteInfoManual = false;
	public static int siteFromSettings = 35;
	public static boolean demoMode = false;
	public static boolean storeLogs = false;
	public static String logsFilename = "logs"; // without extension
	public static int sendPositionsTime = 0;	// in seconds
	public static String sendPositionsServer = "http://192.168.140.243/ws/import/positions/";	// URL to send positions
	//public static String sendPositionsServer = "http://10.3.2.118/ws/import/positions/";	// URL to send positions
	public static boolean sendDetections = false;

	public static boolean posLocal = true;
	public static String lnsUrl = "0.0.0.0";
	public static int timeWindow = 8;	// in seconds
	public static String estimationType = "PROB";
	public static int numPointsHyst = 3;
	public static boolean rssTransf = false;
	public static boolean useParticleFilter = true;
	public static boolean useParticleFilterSensors = true;
	public static boolean useMapFusion = true;

	public static boolean scansetNeeded = false;

}
