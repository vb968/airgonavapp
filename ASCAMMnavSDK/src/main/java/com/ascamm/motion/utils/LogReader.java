package com.ascamm.motion.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import android.os.Environment;
import android.util.Log;

import com.ascamm.utils.Point3D;

public class LogReader {
	
	private String TAG = getClass().getSimpleName();
	private String filename = "logs.txt";
	private String foldername = "ASCAMM_Location";
	private ArrayList<OnlineItem> mOnlineItems;
	private SimOnlineMeasure mSimOnlineMeasure;
	private LinkedList<SimOnlineMeasure> simScansQueue = new LinkedList<SimOnlineMeasure>();
	private LinkedList<SimOnlineMeasure> simAvgScansQueue = new LinkedList<SimOnlineMeasure>();;
	private LinkedList<Point3D> simPosQueue = new LinkedList<Point3D>();;
	private boolean debug = false;
	private boolean getLogs = false;
	
	public enum FileType {
    	RSS,
    	RSS_AVG,
    	POS
    }
	
	public LogReader(){
		
	}
	
	public LogReader(String folder, String file){
		foldername = folder;
		filename = file;
	}
	
	public void setFoldername(String folder){
		foldername = folder;
	}
	
	public void setFilename(String file){
		filename = file;
	}
	
	public void readFile(FileType ft, long fromTs, long toTs){
		File Root = Environment.getExternalStorageDirectory();
		File dir = new File(Root, foldername);
    	if(!dir.exists()) {
    		Log.e(TAG,"Foldername does not exist");
    	}
    	File LogFile = new File(dir, filename);
        if (!LogFile.exists()) {
        	Log.e(TAG,"Filename does not exist");
        }

		try {
		    BufferedReader br = new BufferedReader(new FileReader(LogFile));
		    String line;
	    	
		    if(ft==FileType.RSS){
		    	while ((line = br.readLine()) != null) {
			        //Log.i(TAG, line);
			        addRSSMeasure(line, fromTs, toTs);
			    }
	        	saveLastRSSMeasures();
	        }
	        else if(ft==FileType.RSS_AVG){
	        	while ((line = br.readLine()) != null) {
			        //Log.i(TAG, line);
			        addAvgRSSMeasure(line, fromTs, toTs); // New format (ASCAMM Lab)
			        //addAvgRSSMeasure_old(line); // Old format (CAR)
			    }
	        	saveLastAvgRSSMeasures();
	        }
	        else if(ft==FileType.POS){
	        	while ((line = br.readLine()) != null) {
			        //Log.i(TAG, line);
			        addPos(line, fromTs, toTs); // New format (ASCAMM Lab)
	        	}
	        }
	        else{
	        	Log.e(TAG,"Unknown fileType");
	        }
		    br.close();
		}
		catch (FileNotFoundException e) {
			Log.e(TAG,"Filename does not exist");
		}
		catch (IOException e) {
		    e.printStackTrace();
		}
	}
	
	private boolean addRSSMeasure(String line, long fromTs, long toTs){
		try{
			String[] separated = line.split(",");
			int length = separated.length; 
			if(length<3){
				if(length==1){
					if(!line.contains("-----")){
						if(getLogs){
							//add the array of OnlineItems to SimOnlineMeasure, and SimOnlineMeasure to the queue
							mSimOnlineMeasure.setOnlineItems(mOnlineItems);
							simScansQueue.add(mSimOnlineMeasure);
						}
						long timestamp = Long.parseLong(line);
						if(debug){
							Log.i(TAG,"timestamp: " + timestamp);
						}
						if(timestamp >= fromTs && timestamp <= toTs){
							getLogs = true;
							mSimOnlineMeasure = new SimOnlineMeasure(timestamp, false, 0);
							mOnlineItems = new ArrayList<OnlineItem>();
						}
						else{
							getLogs = false;
						}
						
					}
				}else{
					Log.e(TAG,"Incorrect number of elements");
				}
			}
			else{
				if(getLogs){
					String ssid = separated[0];
					String bssid = separated[1];
					float rss = Float.parseFloat(separated[2]);
					if(debug)	Log.i(TAG,"(" + ssid + " , " + bssid + " , " + rss + ")");
					//add the measure to the array
					mOnlineItems.add(new OnlineItem(ssid, bssid, rss));
				}
			}
		}catch(NullPointerException e){
			//e.printStackTrace();
			Log.e(TAG,"Error decoding RSS measure");
			return false;
		}
		return true;
	}
	
	private boolean addAvgRSSMeasure(String line, long fromTs, long toTs){
		try{
			String[] separated = line.split(",");
			int length = separated.length;
			if(length==1 || length==2 || length==3){
				if(!line.contains("-----")){
					if(getLogs){
						//add the array of OnlineItems to SimOnlineMeasure, and SimOnlineMeasure to the queue
						mSimOnlineMeasure.setOnlineItems(mOnlineItems);
						simAvgScansQueue.add(mSimOnlineMeasure);
					}
					long timestamp = 0;
					boolean walking = false;
					int sensorState = 0;
					if(length==1){
						timestamp = Long.parseLong(line);
					}else if(length==2){
						timestamp = Long.parseLong(separated[0]);
						int walkingValue = Integer.parseInt(separated[1]);
						if(walkingValue==1){
							walking = true;
						}
					}
					else if(length==3){
						timestamp = Long.parseLong(separated[0]);
						int walkingValue = Integer.parseInt(separated[1]);
						if(walkingValue==1){
							walking = true;
						}
						sensorState = Integer.parseInt(separated[2]);
					}
					if(debug){
						Log.i(TAG,"timestamp: " + timestamp + " , walking: " + walking);
					}
					if(timestamp >= fromTs && timestamp <= toTs){
						getLogs = true;
						mSimOnlineMeasure = new SimOnlineMeasure(timestamp, walking, sensorState);
						mOnlineItems = new ArrayList<OnlineItem>();
					}
					else{
						getLogs = false;
					}
				}
			}
			else if(length==7 || length==8){
				if(getLogs){
					String ssid = separated[0];
					String bssid = separated[1];
					float rss = Float.parseFloat(separated[2]);
					double rssStdev = Double.parseDouble(separated[3]);
					int count = Integer.parseInt(separated[4]);
					float countPercentage = Float.parseFloat(separated[5]);
					if(debug)	Log.i(TAG,"(" + ssid + " , " + bssid + " , " + rss + " , " + rssStdev + " , " + count + " , " + countPercentage + ")");
					//add the measure to the array
					OnlineItem oi = new OnlineItem(ssid, bssid, rss, rssStdev, count, countPercentage);
					//oi.setCountPercentage(countPercentage);
					if(length==8){
						String tech = separated[6];
						if(debug)	Log.i(TAG,"tech: " + tech);
						oi.setTech(tech);
					}
					mOnlineItems.add(oi);
				}
			}
			else{
				Log.e(TAG,"Incorrect number of elements");
			}
		}catch(NullPointerException e){
			//e.printStackTrace();
			Log.e(TAG,"Error decoding avgRSS measure");
			return false;
		}
		return true;
	}
	
	/*
	private boolean addAvgRSSMeasure_old(String line){
		try{
			String[] separated = line.split(",");
			int length = separated.length; 
			if(length!=4){
				if(length==1){
					if(!line.contains("-----")){
						if(debug){
							long timestamp = Long.parseLong(line);
							Log.i(TAG,"timestamp: " + timestamp);
						}
						if(mOnlineItems!=null){
							//add the array to the queue
							simAvgScansQueue.add(mOnlineItems);
							mOnlineItems = new ArrayList<OnlineItem>();
						}
						else{
							// it is the first log of the file
							mOnlineItems = new ArrayList<OnlineItem>();
						}
					}
				}else{
					Log.e(TAG,"Incorrect number of elements");
				}
			}
			else{
				String ssid = separated[0];
				String bssid = separated[1];
				float rss = Float.parseFloat(separated[2]);
				double rssStdev = Double.parseDouble(separated[3]);
				if(debug)	Log.i(TAG,"(" + ssid + " , " + bssid + " , " + rss + " , " + rssStdev + ")");
				//add the measure to the array
				mOnlineItems.add(new OnlineItem(ssid, bssid, rss, rssStdev, 1));
			}
		}catch(NullPointerException e){
			//e.printStackTrace();
			Log.e(TAG,"Error decoding avgRSS measure");
			return false;
		}
		return true;
	}
	*/
	/*
	private boolean addAvgRSSMeasure_old_old(String line){
		try{
			String[] separated = line.split(",");
			int length = separated.length; 
			if(length!=2){
				if(length==1){
					if(!line.contains("-----")){
						if(debug){
							long timestamp = Long.parseLong(line);
							Log.i(TAG,"timestamp: " + timestamp);
						}
						if(mOnlineItems!=null){
							//add the array to the queue
							simAvgScansQueue.add(mOnlineItems);
							mOnlineItems = new ArrayList<OnlineItem>();
						}
						else{
							// it is the first log of the file
							mOnlineItems = new ArrayList<OnlineItem>();
						}
					}
				}else{
					Log.e(TAG,"Incorrect number of elements");
				}
			}
			else{
				String bssid = separated[0];
				float rss = Float.parseFloat(separated[1]);
				if(debug)	Log.i(TAG,"(" + bssid + " , " + rss + ")");
				//add the measure to the array
				mOnlineItems.add(new OnlineItem(null, bssid, rss));
			}
		}catch(NullPointerException e){
			//e.printStackTrace();
			Log.e(TAG,"Error decoding avgRSS measure");
			return false;
		}
		return true;
	}
	*/
	
	private boolean addPos(String line, long fromTs, long toTs){
		try{
			String[] separated = line.split(",");
			int length = separated.length; 
			if(length==3 || length==4){
				Point3D pos = new Point3D();
				if(length==3){
					pos.x = Double.parseDouble(separated[0]);
					pos.y = Double.parseDouble(separated[1]);
					pos.z = Double.parseDouble(separated[2]);
					getLogs = true;
				}
				else{
					long timestamp = Long.parseLong(separated[0]);
					if(debug){
						Log.i(TAG,"timestamp: " + timestamp);
					}
					if(timestamp >= fromTs && timestamp <= toTs){
						pos.x = Double.parseDouble(separated[1]);
						pos.y = Double.parseDouble(separated[2]);
						pos.z = Double.parseDouble(separated[3]);
						getLogs = true;
					}
					else{
						getLogs = false;
					}
				}
				if(getLogs){
					if(debug)	Log.i(TAG,pos.toString());
					//add the position to the array
					simPosQueue.add(pos);
				}
			}
			else{
				Log.e(TAG,"Incorrect number of elements");
			}
		}catch(NullPointerException e){
			//e.printStackTrace();
			Log.e(TAG,"Error decoding POS log");
			return false;
		}
		return true;
	}
	
	private void saveLastRSSMeasures(){
		if(getLogs && mOnlineItems!=null && mSimOnlineMeasure!=null){
			//add the array of OnlineItems to SimOnlineMeasure, and SimOnlineMeasure to the queue
			mSimOnlineMeasure.setOnlineItems(mOnlineItems);
			simScansQueue.add(mSimOnlineMeasure);
		}
	}
	
	private void saveLastAvgRSSMeasures(){
		if(getLogs && mOnlineItems!=null && mSimOnlineMeasure!=null){
			//add the array of OnlineItems to SimOnlineMeasure, and SimOnlineMeasure to the queue
			mSimOnlineMeasure.setOnlineItems(mOnlineItems);
			simAvgScansQueue.add(mSimOnlineMeasure);
		}
	}
	
	public LinkedList<SimOnlineMeasure> getSimScansQueue(){
		if(debug){
			for(int i=0;i<simScansQueue.size();i++){
				Log.i(TAG,"Measures at index " + i + ":");
				for(OnlineItem oi : simScansQueue.get(i).getOnlineItems()){
					Log.i(TAG,"(" + oi.getBSSID() + " , " + oi.getRSS() + " , " + oi.getRSSstdev() + " , " + oi.getCount() + " , " + oi.getCountPercentage() + ")");
				}
			}
		}
		return simScansQueue;
	}
	
	public LinkedList<SimOnlineMeasure> getAvgSimScansQueue(){
		Log.d(TAG,"AvgSimScansQueue size: " + simAvgScansQueue.size());
		if(debug){
			for(int i=0;i<simAvgScansQueue.size();i++){
				Log.i(TAG,"Measures at index " + i + ":");
				for(OnlineItem oi : simAvgScansQueue.get(i).getOnlineItems()){
					Log.i(TAG,"(" + oi.getBSSID() + " , " + oi.getRSS() + " , " + oi.getRSSstdev() + " , " + oi.getCount() + " , " + oi.getCountPercentage() + ")");
				}
			}
		}
		return simAvgScansQueue;
	}
	
	public LinkedList<Point3D> getSimPositionsQueue(){
		if(debug){
			int i=0;
			for(Point3D pos : simPosQueue){
				Log.i(TAG,"Position at index " + i + ": " + pos.toString());
				i++;
			}
		}
		return simPosQueue;
	}
	
}
