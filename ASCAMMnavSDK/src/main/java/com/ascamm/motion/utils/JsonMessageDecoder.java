package com.ascamm.motion.utils;

import com.ascamm.utils.Point3D;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonMessageDecoder {
	JSONObject jsonObjMessageFromServer; 

	//constructor of the class
	public JsonMessageDecoder() {
	}

	public void JsonDecodeMessage(String message_in, StringBuilder msgTypeStr, StringBuilder param1, StringBuilder param2) {
		try {
			this.jsonObjMessageFromServer = new JSONObject(message_in);

			String msgType = this.getMsgType();
			msgTypeStr.append(this.getMsgType());

			if (msgType.equals("SACK")) {
				//we do not have to extract any parameter from the message
			} else if ((msgType.equals("SPOS")) || (msgType.equals("SMEA"))) {
				param1.append(this.jsonObjMessageFromServer.get("PosTech").toString());
			} else if (msgType.equals("SINF")) {
				param1.append(this.jsonObjMessageFromServer.get("Msg").toString());
			} else if (msgType.equals("SCHA")) {
				param1.append(this.jsonObjMessageFromServer.get("PosMode").toString());
				param2.append(this.jsonObjMessageFromServer.get("TracFreq").toString());
			} else if (msgType.equals("SERR")) {
				param1.append(this.jsonObjMessageFromServer.get("ErrCode").toString());
			} else {
				msgTypeStr = null;
			}
		} catch (JSONException e) {
			System.out.println("JSON Exception! " + e);
		}
	}
	

	public void JsonDecodeMessageData(String message_in, Hashtable<String, String> measures, String pos_tech, double[] positionLocal, ArrayList<Point3DWithFloor> candidatesLocal, double[] positionGlobal, ArrayList<Point3D> candidatesGlobal) {
		try {
			this.jsonObjMessageFromServer = new JSONObject(message_in);

			String msgType = this.getMsgType();			

			if (msgType.equals("CPOS")) {
				JSONObject objPositionLocal = this.jsonObjMessageFromServer.getJSONObject("Position_local");
				if (objPositionLocal != null) {
					positionLocal[0] = objPositionLocal.getDouble("pos_x");
					positionLocal[1] = objPositionLocal.getDouble("pos_y");
					positionLocal[2] = objPositionLocal.getDouble("pos_z");
				}
				
				JSONArray arrayCandidatesLocal = this.jsonObjMessageFromServer.getJSONArray("Candidates_local");
				if(arrayCandidatesLocal != null && arrayCandidatesLocal.length()!=0){
					for (int i = 0; i < arrayCandidatesLocal.length(); i++) {
						JSONObject candidate = arrayCandidatesLocal.getJSONObject(i);
						candidatesLocal.add(new Point3DWithFloor(candidate.getDouble("pos_x"),candidate.getDouble("pos_y"),candidate.getDouble("pos_z"),candidate.getDouble("pos_z")));
					}
				}
				
				JSONObject objPositionGlobal = this.jsonObjMessageFromServer.getJSONObject("Position_global");
				if (objPositionGlobal != null) {
					positionGlobal[0] = objPositionGlobal.getDouble("pos_x");
					positionGlobal[1] = objPositionGlobal.getDouble("pos_y");
					positionGlobal[2] = objPositionGlobal.getDouble("pos_z");
				}
				
				JSONArray arrayCandidatesGlobal = this.jsonObjMessageFromServer.getJSONArray("Candidates_global");
				if(arrayCandidatesGlobal != null && arrayCandidatesGlobal.length()!=0){
					for (int i = 0; i < arrayCandidatesGlobal.length(); i++) {
						JSONObject candidate = arrayCandidatesGlobal.getJSONObject(i);
						candidatesGlobal.add(new Point3D(candidate.getDouble("pos_x"),candidate.getDouble("pos_y"),candidate.getDouble("pos_z")));
					}
				}
				
				pos_tech = this.jsonObjMessageFromServer.getString("PosTech").toString();
			} else if (msgType.equals("CMEA")){
				JSONObject objMeasurements = this.jsonObjMessageFromServer.getJSONObject("Measures");
				if (objMeasurements != null) {
					Iterator i = objMeasurements.keys();
					while (i.hasNext()) {
						String key = i.next().toString();  //MAC address of the AP
			            String value = objMeasurements.getString(key);  //RSSI from this AP
			            measures.put(key, value);
					}
				}
			} else {
				JSONException e = new JSONException("Msg type does not match expected type CDATA");
				throw e; 
			}
		} catch (JSONException e) {
			System.out.println("JSON Exception! " + e);
		}
	}	
	
	// Get message type from server
	private String getMsgType() throws JSONException{
		String type = null;
		type = this.jsonObjMessageFromServer.get("MsgType").toString();
		return type;
	}

}
