package com.ascamm.motion.utils;

import java.util.ArrayList;

import com.ascamm.motion.algorithm.ParticleState;
import com.ascamm.motion.algorithm.PointWeight;
import com.ascamm.utils.Point3D;

public class ResponseObject {

	private Point3DWithFloor positionLocal;
	private ArrayList<Point3DWithFloor> candidatesLocal;
	private Point3D positionGlobal;
	private ArrayList<Point3D> candidatesGlobal;
	private Point3DWithFloor positionParticleFilter;
	private ArrayList<ParticleState> filterParticles;
	private int filterIsResampled;
	private Point3DWithFloor positionMapFusion;
	private ArrayList<PointWeight> listPositions;
	private String tech;
	private int siteId;
	private Point3DWithFloor lastPositionLocal;
	private float priorDistance;
	private double relativeOrientation;
	private double absoluteOrientation;
	private double numSteps;
	private double userOrientation;
	private double userOrientationReliability;
	private double positionError;
	
	//ResponseObject constructor for storing data of fingerprinting positioning only:
	public ResponseObject(Point3DWithFloor positionLocal, ArrayList<Point3DWithFloor> candidatesLocal, Point3D positionGlobal, ArrayList<Point3D> candidatesGlobal){
		this.setPositionLocal(positionLocal);
		this.setCandidatesLocal(candidatesLocal);
		this.setPositionGlobal(positionGlobal);
		this.setCandidatesGlobal(candidatesGlobal);
		this.setPositionParticleFilter(null);
		this.setFilterParticles(null);
		this.setFilterIsResampled(0);
		this.setPositionMapFusion(null);
	}
	
	//ResponseObject constructor for storing data of fingerprinting, particle filter and map fusion positioning:
	public ResponseObject(Point3DWithFloor positionLocal, ArrayList<Point3DWithFloor> candidatesLocal, Point3D positionGlobal, ArrayList<Point3D> candidatesGlobal, Point3DWithFloor positionParticleFilter, ArrayList<ParticleState> particles, int isResampled, Point3DWithFloor positionMapFusion){
		this.setPositionLocal(positionLocal);
		this.setCandidatesLocal(candidatesLocal);
		this.setPositionGlobal(positionGlobal);
		this.setCandidatesGlobal(candidatesGlobal);
		this.setPositionParticleFilter(positionParticleFilter);
		this.setFilterParticles(particles);
		this.setFilterIsResampled(isResampled);
		this.setPositionMapFusion(positionMapFusion);
	}

	public Point3DWithFloor getPositionLocal() {
		return positionLocal;
	}

	public void setPositionLocal(Point3DWithFloor position) {
		this.positionLocal = position;
	}

	public ArrayList<Point3DWithFloor> getCandidatesLocal() {
		return candidatesLocal;
	}

	public void setCandidatesLocal(ArrayList<Point3DWithFloor> candidates) {
		this.candidatesLocal = candidates;
	}
	
	public Point3D getPositionGlobal() {
		return positionGlobal;
	}

	public void setPositionGlobal(Point3D position) {
		this.positionGlobal = position;
	}

	public ArrayList<Point3D> getCandidatesGlobal() {
		return candidatesGlobal;
	}

	public void setCandidatesGlobal(ArrayList<Point3D> candidates) {
		this.candidatesGlobal = candidates;
	}
	
	public Point3DWithFloor getPositionParticleFilter() {
		return positionParticleFilter;
	}

	public void setPositionParticleFilter(Point3DWithFloor position) {
		this.positionParticleFilter = position;
	}
	
	public ArrayList<ParticleState> getFilterParticles() {
		return filterParticles;
	}
	
	public void setFilterParticles(ArrayList<ParticleState> particles) {
		this.filterParticles = particles;
	}
	
	public int getFilterIsResampled() {
		return filterIsResampled;
	}
	
	public void setFilterIsResampled(int isResampled) {
		this.filterIsResampled = isResampled;
	}
	
	public Point3DWithFloor getPositionMapFusion() {
		return positionMapFusion;
	}

	public void setPositionMapFusion(Point3DWithFloor position) {
		this.positionMapFusion = position;
	}
	
	public ArrayList<PointWeight> getListPositions() {
		return listPositions;
	}

	public void setListPositions(ArrayList<PointWeight> listPositions) {
		this.listPositions = listPositions;
	}

	public String getTech() {
		return tech;
	}

	public void setTech(String tech) {
		this.tech = tech;
	}

	public int getSiteId() {
		return siteId;
	}

	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}

	public Point3DWithFloor getLastPositionLocal() {
		return lastPositionLocal;
	}

	public void setLastPositionLocal(Point3DWithFloor lastPositionLocal) {
		this.lastPositionLocal = lastPositionLocal;
	}

	public float getPriorDistance() {
		return priorDistance;
	}

	public void setPriorDistance(float priorDistance) {
		this.priorDistance = priorDistance;
	}

	public double getRelativeOrientation() {
		return relativeOrientation;
	}

	public void setRelativeOrientation(double relativeOrientation) {
		this.relativeOrientation = relativeOrientation;
	}

	public double getAbsoluteOrientation() {
		return absoluteOrientation;
	}

	public void setAbsoluteOrientation(double absoluteOrientation) {
		this.absoluteOrientation = absoluteOrientation;
	}

	public double getNumSteps() {
		return numSteps;
	}

	public void setNumSteps(double numSteps) {
		this.numSteps = numSteps;
	}

	public double getUserOrientation() {
		return userOrientation;
	}

	public void setUserOrientation(double userOrientation) {
		this.userOrientation = userOrientation;
	}

	public double getUserOrientationReliability() {
		return userOrientationReliability;
	}

	public void setUserOrientationReliability(double userOrientationReliability) {
		this.userOrientationReliability = userOrientationReliability;
	}

	public double getPositionError() {
		return positionError;
	}

	public void setPositionError(double positionError) {
		this.positionError = positionError;
	}
}
