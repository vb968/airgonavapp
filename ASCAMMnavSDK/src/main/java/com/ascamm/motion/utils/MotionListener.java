package com.ascamm.motion.utils;



public interface MotionListener {
	public void newMotionAvailable(int sensorState);
}
