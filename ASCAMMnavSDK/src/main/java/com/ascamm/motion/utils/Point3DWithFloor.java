package com.ascamm.motion.utils;

import com.ascamm.utils.Point3D;

/**
 * Created by dfernandez on 20/01/17.
 */

public class Point3DWithFloor extends Point3D {

    private double floor;

    public Point3DWithFloor(){
        super();
    }

    public Point3DWithFloor(double x, double y, double z, double floor){
        this.x = x;
        this.y = y;
        this.z = z;
        this.floor = floor;
    }

    public double getFloor() {
        return floor;
    }

    public void setFloor(double floor) {
        this.floor = floor;
    }

    public Point3D getPoint3DWithoutHeight(){
        return new Point3D(x,y,floor);
    }

    public String toString(){
        return "Point3DWithFloor(" + this.x + ", " + this.y + ", " + this.z + ", " + this.floor + ")";
    }

    /*
    // Next two methods added to use Point3D objects as Keys in Hashtable (Dani)
    public boolean equals(Object obj) {
        if (!(obj instanceof Point3DWithFloor)) {
            return false;
        } else {
            Point3DWithFloor that = (Point3DWithFloor)obj;
            return ((this.x==that.x) && (this.y==that.y) && (this.floor==that.floor));
        }
    }

    public int hashCode() {
        int hash = toString().hashCode();
        return hash;
    }
    */
}
