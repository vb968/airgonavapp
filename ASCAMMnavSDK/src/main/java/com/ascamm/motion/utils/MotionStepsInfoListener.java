package com.ascamm.motion.utils;


import java.util.ArrayList;

public interface MotionStepsInfoListener {
	public void newMotionStepsInfoAvailable(int motionMode, ArrayList<Double> stepsList, double orientation, double diffOrientation);
}
