package com.ascamm.motion.utils;


public interface IndoorPositionListener {
	public void newPositionAvailable(ResponseObject locationResponse);
}
