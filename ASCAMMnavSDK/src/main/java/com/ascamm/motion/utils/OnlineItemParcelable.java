package com.ascamm.motion.utils;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class OnlineItemParcelable implements Parcelable {

	private String TAG = getClass().getSimpleName();
	
	private String mBSSID;
	
	private String mSSID;
	private float mRSS;
	private double mRSSstdev;
	private String mTech;
	private int mCount;
	private float mCountPercentage;
	
	private OnlineItem oi;
	
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
    	Log.d(TAG, "inside writeToParcel. flags: " + flags);
        out.writeString(mBSSID);
        out.writeString(mSSID);
        out.writeFloat(mRSS);
        out.writeDouble(mRSSstdev);
        out.writeString(mTech);
        out.writeInt(mCount);
        out.writeFloat(mCountPercentage);
    }
    
    public static final Parcelable.Creator<OnlineItemParcelable> CREATOR
            = new Parcelable.Creator<OnlineItemParcelable>() {
        public OnlineItemParcelable createFromParcel(Parcel in) {
            return new OnlineItemParcelable(in);
        }

        public OnlineItemParcelable[] newArray(int size) {
            return new OnlineItemParcelable[size];
        }
    };
    
    
    private OnlineItemParcelable(Parcel in) {
    	Log.d(TAG, "inside OnlineItemParcelable");
        mBSSID = in.readString();
        mSSID = in.readString();
        mRSS = in.readFloat();
        mRSSstdev = in.readDouble();
        mTech = in.readString();
        mCount = in.readInt();
        mCountPercentage = in.readFloat();
    }
    
    public OnlineItemParcelable(OnlineItem oi){
    	/*
    	mSSID = oi.getSSID();
    	mBSSID = oi.getBSSID();
    	mRSS = oi.getRSS();
    	mRSSstdev = oi.getRSSstdev();
    	mTech = oi.getTech();
    	mCount = oi.getCount();
    	mCountPercentage = oi.getCountPercentage();
    	*/
    	this.oi = oi;
    }
    
    public OnlineItem getOnlineItem(){
    	/*
    	OnlineItem oi = new OnlineItem(mSSID, mBSSID, mRSS, mRSSstdev, mCount, mCountPercentage);
		oi.setTech(mTech);
		*/
    	return oi;
    }
    
}
