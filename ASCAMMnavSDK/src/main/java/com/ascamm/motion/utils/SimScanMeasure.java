package com.ascamm.motion.utils;

import java.util.ArrayList;

public class SimScanMeasure {

	private long ts;
	private ArrayList<SimScanResult> mSimScanResults;
	
	public SimScanMeasure(long ts, ArrayList<SimScanResult> mSimScanResults){
		this.ts = ts;
		this.mSimScanResults = new ArrayList<SimScanResult>();
		for(SimScanResult ssr : mSimScanResults){
			SimScanResult newSimScanResult = new SimScanResult(ssr.getSSID(),ssr.getBSSID(),ssr.getLevel());
			this.mSimScanResults.add(newSimScanResult);
		}
	}

	public long getTs() {
		return ts;
	}

	public void setTs(long ts) {
		this.ts = ts;
	}

	public ArrayList<SimScanResult> getScanResults() {
		return mSimScanResults;
	}

	public void setScanResults(ArrayList<SimScanResult> mSimScanResults) {
		for(SimScanResult ssr : mSimScanResults){
			SimScanResult newSimScanResult = new SimScanResult(ssr.getSSID(),ssr.getBSSID(),ssr.getLevel());
			this.mSimScanResults.add(newSimScanResult);
		}
	}
	
}
