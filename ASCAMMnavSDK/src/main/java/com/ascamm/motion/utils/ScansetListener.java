package com.ascamm.motion.utils;

import java.util.ArrayList;


public interface ScansetListener {
	public void newScansetAvailable(ArrayList<OnlineItem> scanset);
}
