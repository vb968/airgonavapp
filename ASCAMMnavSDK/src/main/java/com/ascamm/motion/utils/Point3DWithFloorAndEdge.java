package com.ascamm.motion.utils;

/**
 * Class to allow to pass a point and its related edge when the point is contained in an edge
 * 
 * @author Arnau Alcázar Lleopart <arnau.alcazar@eurecat.org>
 *
 */

public class Point3DWithFloorAndEdge extends Point3DWithFloor {

	private Edge mEdge;
	
	public Point3DWithFloorAndEdge(double x, double y, double z, double floor, Edge e) {
		super(x, y, z, floor);
		this.mEdge = e;
	}
	
	public Point3DWithFloorAndEdge() {
		super();
	}
	
	public Edge getEdge() {
		return this.mEdge;
	}
	
	public void setEdge(Edge e) {
		this.mEdge = e;
	}
	
	public void set(double x, double y, double z, double floor, Edge e) {
		super.set(x, y, z);
		setFloor(floor);
		this.mEdge = e;
	}

}
