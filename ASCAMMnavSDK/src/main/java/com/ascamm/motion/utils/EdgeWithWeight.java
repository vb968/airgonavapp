package com.ascamm.motion.utils;

/**
 * Created by indoorpositioning on 29/03/16.
 */
public class EdgeWithWeight {

    private Edge edge;
    private double weight;
    private Point3DWithFloor closestPoint;

    public EdgeWithWeight(Edge edge, double weight, Point3DWithFloor closestPoint){
        this.edge = edge;
        this.weight = weight;
        this.closestPoint = closestPoint;
    }

    public Edge getEdge() {
        return edge;
    }

    public void setEdge(Edge edge) {
        this.edge = edge;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Point3DWithFloor getClosestPoint() {
        return closestPoint;
    }

    public void setClosestPoint(Point3DWithFloor closestPoint) {
        this.closestPoint = closestPoint;
    }
}
