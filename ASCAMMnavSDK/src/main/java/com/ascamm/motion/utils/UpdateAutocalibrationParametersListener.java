package com.ascamm.motion.utils;

import com.ascamm.motion.algorithm.AutocalibrationParameters;

public interface UpdateAutocalibrationParametersListener {
	public void newUpdate(AutocalibrationParameters autoParams);
}
