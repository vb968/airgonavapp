package com.ascamm.motion.utils;

import com.ascamm.utils.Point3D;

import java.util.ArrayList;

public class Marker {

	private long ts;
	private Point3D pos;
	private ArrayList<Integer> currentEdgesId;
	
	public Marker(long ts, Point3D pos, ArrayList<Integer> currentEdgesId){
		this.ts = ts;
		this.pos = pos;
		this.currentEdgesId = currentEdgesId;
	}

	public long getTs() {
		return ts;
	}

	public void setTs(long ts) {
		this.ts = ts;
	}

	public Point3D getPos() {
		return pos;
	}

	public void setPos(Point3D pos) {
		this.pos = pos;
	}

	public ArrayList<Integer> getCurrentEdgesId() {
		return currentEdgesId;
	}

	public void setCurrentEdgesId(ArrayList<Integer> currentEdgesId) {
		this.currentEdgesId = currentEdgesId;
	}
	
}
