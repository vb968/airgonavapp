package com.ascamm.motion.utils;

import java.util.Enumeration;
import java.util.Hashtable;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

public class JsonClientMessage {
    /**********************************************************
     * Message type:     
     *         CALI  - Alive message
     *         CDATA - Position and Measurement message
     *         CERR  - error message
     *         CINF  - information message
     * 
     * Error code:
     *         E001 - Error 1
     *         E002 - Error 2
     * 
     * Message:
     *         Length < 140 chars
     **********************************************************/
    
    public String macAddr;
    public String jsonMsg;
    
    public JsonClientMessage(String c_mac){
        macAddr     = c_mac;
        jsonMsg     = null;
    }
    
    public String alive() {
        /**********************************************************
         * example of an alive message:
         * {
         *   "MsgType"  : "CALI",
         *   "DevId"    : "MAC_ADDRESS_CLIENT",
         * }
         **********************************************************/
        try {
            jsonMsg = new JSONStringer()
                            .object()
                                .key("DevId").value(macAddr)
                                .key("MsgType").value("CALI")
                            .endObject().toString();
        } catch (JSONException e) {
            System.out.println("JSON Exception! " + e);
        }
        return jsonMsg;
    }

    public String scanData(Hashtable<String, String> measures, double[] positon, String pos_tech) {
        /**********************************************************
         * example of a position message:
         * {
         *   "MsgType"  : "CDATA",
         *   "DevId"    : "MAC_ADDRESS_CLIENT",
         *   "Measures"    : 
         *                 {
         *                   "AP_1_MAC_ADDR" : "MEASUREMENT_1",
         *                   "AP_2_MAC_ADDR" : "MEASUREMENT_2",
         *                   "AP_3_MAC_ADDR" : "MEASUREMENT_3",
         *                   ................................
         *                   "AP_N_MAC_ADDR" : "MEASUREMENT_N",
         *                 }
         *   "Positions" :
         *                 {
         *                   "PosX"        : "POSITION_X",
         *                   "PosY"        : "POSITION_Y",
         *                   "PosZ"        : "POSITION_Z",
         *                 }
         *   "PosTec"   : "POSITION_TECHNIQUE_NAME",
         * }
         **********************************************************/
        try{
        	// construct measurements in json object
        	JSONObject jsonObjMeasure =  new JSONObject();
        	JSONObject jsonObjPosition =  new JSONObject();

        	if(measures!=null){
	        	if(measures.size()>0){
		        	Enumeration<String> e = measures.keys();
		            while (e.hasMoreElements()){
		                String key       = (String) e.nextElement();
		                String value     = measures.get(key);
		                jsonObjMeasure.put(key, value);
		            }
	            }
        	}
            
            // construct positions in json object
            if(positon!=null){
	        	if(positon.length==3){
	        		jsonObjPosition.put("PosX", positon[0]);
	        		jsonObjPosition.put("PosY", positon[1]);
	        		jsonObjPosition.put("PosZ", positon[2]);
	        	}
        	}
        	
            // join all parameters
            jsonMsg = new JSONStringer()
                            .object()
                                .key("DevId").value(macAddr)
                                .key("MsgType").value("CDATA")
                                .key("Measures").value(jsonObjMeasure)
                                .key("Positions").value(jsonObjPosition)
                                .key("PosTech").value(pos_tech)
                            .endObject().toString();
        } catch (JSONException e) {
            System.out.println("JSON Exception! " + e);
        }
        
        return jsonMsg;
    }
    
    public String info(String msg) {
        /**********************************************************
         * example of a information message:
         * {
         *   "MsgType"  : "CINF",
         *   "DevId"    : "MAC_ADDRESS_CLIENT",
         *   "Msg"      : "Client battery low!",
         * }
         **********************************************************/
        try {
            jsonMsg = new JSONStringer()
                            .object()
                                .key("DevId").value(macAddr)
                                .key("MsgType").value("CINF")
                                .key("Msg").value(msg)
                            .endObject().toString();
        } catch (JSONException e) {
            System.out.println("JSON Exception! " + e);
        }
        return jsonMsg;
    }
    
    public String error(String error_code) {
        /**********************************************************
         * example of an error message:
         * {
         *   "MsgType"  : "CERR",
         *   "DevId"    : "MAC_ADDRESS_CLIENT",
         *   "ErrCode"  : "E001",
         * }
         **********************************************************/
        try {
            jsonMsg = new JSONStringer()
                            .object()
                                .key("DevId").value(macAddr)
                                .key("MsgType").value("CERR")
                                .key("ErrCode").value(error_code)
                            .endObject().toString();
        } catch (JSONException e) {
            System.out.println("JSON Exception! " + e);
        }
        return jsonMsg;
    }
}
