package com.ascamm.motion.utils;

public class MapInfo {
	
	private int id;
	private int floor;
	private String url;
	private String filename;
	
	public MapInfo(int id, int floor, String url, String filename){
		this.id = id;
		this.floor = floor;
		this.url = url;
		this.filename = filename;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFloor() {
		return floor;
	}

	public void setFloor(int floor) {
		this.floor = floor;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
	
}
