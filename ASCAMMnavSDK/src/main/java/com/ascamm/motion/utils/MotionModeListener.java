package com.ascamm.motion.utils;



public interface MotionModeListener {
	public void newMotionModeAvailable(int motionMode, int steps);
}
