package com.ascamm.motion.utils;

/**
 * Created by dfernandez on 20/01/17.
 */

public class FloorData {

    private int num;
    private double height;
    private double min;
    private double max;
    private boolean useIncrement = false;
    private boolean useDecrement = false;
    private double increment;
    private double decrement;

    public FloorData(int num, double height){
        this.num = num;
        this.height = height;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public boolean isUseIncrement() {
        return useIncrement;
    }

    public void setUseIncrement(boolean useIncrement) {
        this.useIncrement = useIncrement;
    }

    public boolean isUseDecrement() {
        return useDecrement;
    }

    public void setUseDecrement(boolean useDecrement) {
        this.useDecrement = useDecrement;
    }

    public double getIncrement() {
        return increment;
    }

    public void setIncrement(double increment) {
        this.increment = increment;
    }

    public double getDecrement() {
        return decrement;
    }

    public void setDecrement(double decrement) {
        this.decrement = decrement;
    }
}
