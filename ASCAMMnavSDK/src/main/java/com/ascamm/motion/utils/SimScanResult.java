package com.ascamm.motion.utils;

public class SimScanResult {
	  
	public String SSID;
  
	public String BSSID;
  
	public String capabilities;
  
	public float level;
  
	public int frequency;
  
	public long timestamp;
  
	public SimScanResult(String ssid, String bssid, float level) {
		this.SSID = ssid;
		this.BSSID = bssid;
		this.level = level;
	}

	public String getSSID() {
		return SSID;
	}

	public void setSSID(String sSID) {
		SSID = sSID;
	}

	public String getBSSID() {
		return BSSID;
	}

	public void setBSSID(String bSSID) {
		BSSID = bSSID;
	}

	public String getCapabilities() {
		return capabilities;
	}

	public void setCapabilities(String capabilities) {
		this.capabilities = capabilities;
	}

	public float getLevel() {
		return level;
	}

	public void setLevel(float level) {
		this.level = level;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	 
}