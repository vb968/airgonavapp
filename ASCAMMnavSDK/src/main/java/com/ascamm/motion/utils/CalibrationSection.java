package com.ascamm.motion.utils;

import java.util.ArrayList;

public class CalibrationSection {
	
	private Marker marker1;
	private Marker marker2;
	private ArrayList<PointWithOnlineItems> pointsWithOnlineItems;
	
	public CalibrationSection(Marker m1, Marker m2, ArrayList<PointWithOnlineItems> pwoi){
		marker1 = m1;
		marker2 = m2;
		pointsWithOnlineItems = pwoi;
	}

	public Marker getMarker1() {
		return marker1;
	}

	public void setMarker1(Marker marker1) {
		this.marker1 = marker1;
	}

	public Marker getMarker2() {
		return marker2;
	}

	public void setMarker2(Marker marker2) {
		this.marker2 = marker2;
	}

	public ArrayList<PointWithOnlineItems> getPointsWithOnlineItems() {
		return pointsWithOnlineItems;
	}

	public void setPointsWithOnlineItems(
			ArrayList<PointWithOnlineItems> pointsWithOnlineItems) {
		this.pointsWithOnlineItems = pointsWithOnlineItems;
	}
	
	
}
