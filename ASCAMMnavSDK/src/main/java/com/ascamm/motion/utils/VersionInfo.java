package com.ascamm.motion.utils;

public class VersionInfo {
	
	private int aps;
	private int maps;
	private int nodes_edges;
	
	public VersionInfo(int aps, int maps, int nodes_edges){
		this.aps = aps;
		this.maps = maps;
		this.nodes_edges = nodes_edges;
	}

	public int getAps() {
		return aps;
	}

	public void setAps(int aps) {
		this.aps = aps;
	}

	public int getMaps() {
		return maps;
	}

	public void setMaps(int maps) {
		this.maps = maps;
	}

	public int getNodes_edges() {
		return nodes_edges;
	}

	public void setNodes_edges(int nodes_edges) {
		this.nodes_edges = nodes_edges;
	}

}
