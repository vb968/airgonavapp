package com.ascamm.motion.utils;

import java.util.ArrayList;

import com.ascamm.motion.LocationData;

public interface PositionsListener {
	public void newPositionAvailable(ArrayList<LocationData> locationsData);
}
