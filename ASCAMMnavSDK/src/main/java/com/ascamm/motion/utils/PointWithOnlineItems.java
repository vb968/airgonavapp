package com.ascamm.motion.utils;

import java.util.ArrayList;

import android.graphics.Point;

import com.ascamm.utils.Point3D;

public class PointWithOnlineItems {
	private Point3D mPoint;
	private ArrayList<OnlineItem> mOnlineItems;
	private String mTech;
	private int edgeId;
	
	public PointWithOnlineItems(Point3D p, ArrayList<OnlineItem> listOfOnlineItems, String tech) {
		mPoint = p;
		mOnlineItems = listOfOnlineItems;
		mTech = tech;
	}

	public Point3D getPoint() {
		return mPoint;
	}

	public void setPoint(Point3D mPoint) {
		this.mPoint = mPoint;
	}

	public ArrayList<OnlineItem> getOnlineItems() {
		return mOnlineItems;
	}

	public void setOnlineItems(ArrayList<OnlineItem> mOnlineItems) {
		this.mOnlineItems = mOnlineItems;
	}

	public String getTech() {
		return mTech;
	}

	public void setTech(String mTech) {
		this.mTech = mTech;
	}

	public int getEdgeId() {
		return edgeId;
	}

	public void setEdgeId(int edgeId) {
		this.edgeId = edgeId;
	}
	
}
