package com.ascamm.motion;

import android.os.Binder;

/**
 * Created by indoorpositioning on 28/04/16.
 */
public class PositioningServiceBinder extends Binder {

    private PositioningService posService;

    public PositioningServiceBinder(PositioningService posService){
        this.posService = posService;
    }

    public PositioningService getService(){
        return this.posService;
    }
}
