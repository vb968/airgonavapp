package com.ascamm.motion.comm;

import android.content.Context;
import android.util.Log;

import com.ascamm.motion.utils.Constants;
import com.ascamm.motion.LocationData;
import com.ascamm.motion.utils.Settings;
import com.ascamm.motion.database.DataDBHelper;
import com.ascamm.motion.database.LocalDBHelper;
import com.ascamm.motion.database.Position;
import com.ascamm.motion.detections.DbDetection;
import com.ascamm.motion.detections.Detection;
import com.ascamm.motion.utils.LogWriter;
import com.ascamm.motion.ws.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by Dani Fernandez on 03/05/16.
 * this class stores positions in a database and sends them to the server every X seconds
 */
public class DataManager {

    private String TAG = getClass().getSimpleName();
    private boolean debug = Constants.DEBUGMODE;

    private ScheduledExecutorService mSendScheduledExecutor;
    private ScheduledFuture<?> mSendScheduledFuture = null;

    private DataDBHelper mDataDBHelper;
    private boolean storeDBinSDcard = false;

    private String userId = "123ascamm";
    private String deviceId = "";
    private String urlToSendPositions = Settings.sendPositionsServer;
    private int sendPositionsTime = Settings.sendPositionsTime; // in seconds
    private boolean sendDetections = Settings.sendDetections;

    /** Code added for logs **/
    private LogWriter mLogWriter;
    private boolean storeLogs = false, storeLogSendPos = true;
    private String logsFilename = "logs_data_manager.txt";

    public DataManager(Context context, String deviceId){
        this.deviceId = deviceId;
        if(storeDBinSDcard){
            mDataDBHelper = new DataDBHelper(context, true);	// creates file without userId in filename
            //mDataDBHelper = new DataDBHelper(this, true, userId);	// creates file with userId in filename
        }
        else{
            mDataDBHelper = new DataDBHelper(context);
        }
        mDataDBHelper.open();
        //mDataDBHelper.exportDB(Constants.APP_DATA_FOLDER.getPath());
        mDataDBHelper.updateAllPositions(LocalDBHelper.STATUS_READY);
        /** Code added to store data info into logs.txt **/
        if(storeLogs){
            mLogWriter = new LogWriter(Constants.APP_DATA_FOLDER_NAME, logsFilename);
            if(mLogWriter.startLogs()>0){
                mLogWriter.stopLogs();
            }
        }
    }

    public void destroy(){
        if(mDataDBHelper!=null) {
            mDataDBHelper.close();
        }
    }

    public synchronized void setUserId(String userId){
        this.userId = userId;
    }

    public synchronized void setURLtoSendPositions(String url){
        this.urlToSendPositions = url;
    }

    public synchronized void setSendPositionsTime(int seconds){
        this.sendPositionsTime = seconds;
    }

    public void startSendingPositions(){
        if(sendPositionsTime>0){
            if(mSendScheduledExecutor==null){
                mSendScheduledExecutor= Executors.newScheduledThreadPool(5);
            }
            // It schedules a task to run every sendPositionsTime seconds
            mSendScheduledFuture = mSendScheduledExecutor.scheduleAtFixedRate(checkPosRunnable, sendPositionsTime, sendPositionsTime, TimeUnit.SECONDS);
        }
    }

    public void stopSendingPositions(){
        if (mSendScheduledFuture != null) {
            //cancel execution of the future scheduled task
            //If task is already running, interrupt it here.
            mSendScheduledFuture.cancel(true);
        }
    }

    final Runnable checkPosRunnable = new Runnable() {
        public void run() {
            if(debug){
                Log.d(TAG, ">>> Starting scheduled TASK CheckPositionsToSend...");
            }
            checkPositionsToSend();
        }
    };

    public void savePosition(LocationData locationData){
        if(sendPositionsTime==-1 || sendPositionsTime>0){
            long id = mDataDBHelper.insertPosition(locationData, userId); //save position in DB
            if(debug)	Log.d(TAG,"inserting position into DB. id: " + id);
        }
    }

    public void saveDetection(Detection detection, long ts, int siteId){
        if(sendDetections && (sendPositionsTime==-1 || sendPositionsTime>0)){
            long id = mDataDBHelper.insertDetection(detection, ts, siteId, userId); //save detection in DB
            if(debug)       Log.d(TAG,"inserting detection into DB. id: " + id);
        }
    }

    private void checkPositionsToSend(){
        // get positions from DB and send them to the server
        ArrayList<Position> positions = mDataDBHelper.getPositionsWithStatus(LocalDBHelper.STATUS_READY);
        int numPos = positions.size();
        final ArrayList<Integer> posIds = new ArrayList<Integer>();
        ArrayList<DbDetection> detections = new ArrayList<DbDetection>();
        if(sendDetections){
            detections = mDataDBHelper.getDetectionsWithStatus(LocalDBHelper.STATUS_READY);
        }
        int numDet = detections.size();
        final ArrayList<Integer> detIds = new ArrayList<Integer>();
        if(debug)       Log.d(TAG,"numPositions: " + numPos + " , numDetections: " + numDet);
        if(!positions.isEmpty() || !detections.isEmpty()){
            try{
                JSONArray positionsJSON = new JSONArray();
                for(Position p : positions){
                    JSONObject jsonObj = new JSONObject();
                    //if(debug)     Log.d(TAG,"id: " + p.getId());
                    jsonObj.put("lat", p.getLat());
                    jsonObj.put("lng", p.getLng());
                    jsonObj.put("alt", p.getAlt());
                    /*
                    if (p.getX() == -1 && p.getY() == -1 && p.getZ() == -1){
                        jsonObj.put("x", JSONObject.NULL);
                        jsonObj.put("y", JSONObject.NULL);
                        jsonObj.put("z", JSONObject.NULL);
                    }else{
                        jsonObj.put("x", p.getX());
                        jsonObj.put("y", p.getY());
                        jsonObj.put("z", p.getZ());
                    }
                    */
                    jsonObj.put("prec", p.getAccuracy());
                    jsonObj.put("vel", p.getVelocity());
                    jsonObj.put("date",p.getTime());
                    jsonObj.put("type", p.getTech());
                    jsonObj.put("site", p.getSiteId());
                    positionsJSON.put(jsonObj);
                    posIds.add(p.getId());
                }
                JSONArray detectionsJSON = new JSONArray();
                for(DbDetection d : detections){
                    JSONObject jsonObj = new JSONObject();
                    if(debug)       Log.d(TAG,"det_id: " + d.getId());
                    jsonObj.put("id", d.getMac());
                    jsonObj.put("lat", d.getPosGlobal().x);
                    jsonObj.put("lng", d.getPosGlobal().y);
                    jsonObj.put("alt", d.getPosGlobal().z);
                    /*
                    if (d.getPosLocal().x == -1 && d.getPosLocal().y == -1 && d.getPosLocal().z == -1){
                        jsonObj.put("x", JSONObject.NULL);
                        jsonObj.put("y", JSONObject.NULL);
                        jsonObj.put("z", JSONObject.NULL);
                    }else{
                        jsonObj.put("x", d.getPosLocal().x);
                        jsonObj.put("y", d.getPosLocal().y);
                        jsonObj.put("z", d.getPosLocal().z);
                    }
                    */
                    jsonObj.put("date", d.getTs());
                    jsonObj.put("site", d.getSiteId());
                    detectionsJSON.put(jsonObj);
                    detIds.add(d.getId());
                }

                Calendar c = Calendar.getInstance();
                int ts = (int) (c.getTimeInMillis()/1000);
                final JSONObject json = new JSONObject();
                json.put("device_id", deviceId);
                json.put("ts", ts);
                json.put("positions", positionsJSON);
                if(sendDetections){
                    json.put("detections", detectionsJSON);
                }
                //Log.d(TAG,"json: " + json);

                //Update positions status
                int numPositionsUpdated = mDataDBHelper.updatePositions(posIds, LocalDBHelper.STATUS_READY, LocalDBHelper.STATUS_PENDING);
                if(debug)   Log.d(TAG,"numPositionsUpdated: " + numPositionsUpdated);
                if(sendDetections){
                    int numDetectionsUpdated = mDataDBHelper.updateDetections(detIds, LocalDBHelper.STATUS_READY, LocalDBHelper.STATUS_PENDING);
                    if(debug)   Log.d(TAG,"numDetectionsUpdated: " + numDetectionsUpdated);
                }
                final long req_ts = System.currentTimeMillis();
                /** Code added to store data info into logs.txt **/
                if(storeLogs && storeLogSendPos && !positions.isEmpty()){
                    if(mLogWriter.startLogsNoText()>0){
                        mLogWriter.addTextLog(req_ts, "SEND_POS(REQ) | req_ts: " + req_ts + " , numPos: " + numPos + " , lastPos: " + positions.get(numPos-1).getPosGlobal());
                        mLogWriter.stopLogs();
                    }
                }

                Thread sendPosThread = new Thread(new Runnable() {
                    public void run() {
                        if(debug)       Log.d("SendPosThread","send positions to server");
                        sendPositions(urlToSendPositions, WebService.RequestMethod.POST, json, posIds, detIds, req_ts);
                    }
                });
                //Log.d(TAG,"sendPosThread: " + sendPosThread.toString());
                sendPosThread.start();
            }catch (Exception e){
                Log.e("WebService AT","Error!", e);
            }
        }
    }

    private void sendPositions(String url, WebService.RequestMethod method, JSONObject json, ArrayList<Integer> posIds, ArrayList<Integer> detIds, long req_ts){
        WebService ws = new WebService(url);
        boolean use_token = false;
        boolean cache = true;
        String token = "";

        try {
            if(use_token){
                ws.AddHeader("TOKEN", token);
            }
            if(!cache){
                ws.AddHeader("Cache-Control", "no-cache, no-store");
            }
            ws.AddHeader("Content-Type", "application/json");
            ws.AddJSON(json);
            ws.Execute(method);
        } catch (Exception e) {
            e.printStackTrace();
        }

        int statusCode = ws.getResponseCode();
        String response = ws.getResponse();
        /** Code added to store data info into logs.txt **/
        if(storeLogs && storeLogSendPos){
            if(mLogWriter.startLogsNoText()>0){
                long ts = System.currentTimeMillis();
                mLogWriter.addTextLog(ts, "SEND_POS(RESP) | req_ts: " + req_ts + ", status: " + statusCode + ", diffTs: " + (ts-req_ts));
                mLogWriter.stopLogs();
            }
        }
        processWsResponse(statusCode, response, posIds, detIds);
    }

    private void processWsResponse(int statusCode, String response, ArrayList<Integer> posIds, ArrayList<Integer> detIds){
        try{
            if(mDataDBHelper != null){
                boolean forceOpen = false;
                if(!mDataDBHelper.isOpened()){
                    mDataDBHelper.open();
                    forceOpen = true;
                }
                if(statusCode == 200){
                    //If the answer of the WS is correct, delete positions from the table. Else update status of positions to READY
                    JSONObject json = new JSONObject(response);
                    String status = json.getString("status");
                    if(status.equalsIgnoreCase("success")){
                        mDataDBHelper.deletePositions(posIds);
                        if(sendDetections){
                            mDataDBHelper.deleteDetections(detIds);
                        }
                    }
                    else{
                        mDataDBHelper.updatePositions(posIds, LocalDBHelper.STATUS_PENDING, LocalDBHelper.STATUS_READY);
                        if(sendDetections){
                            mDataDBHelper.updateDetections(detIds, LocalDBHelper.STATUS_PENDING, LocalDBHelper.STATUS_READY);
                        }
                    }
                }
                else{
                    //If the answer of the WS is not correct, update status of positions to READY
                    mDataDBHelper.updatePositions(posIds, LocalDBHelper.STATUS_PENDING, LocalDBHelper.STATUS_READY);
                    if(sendDetections){
                        mDataDBHelper.updateDetections(detIds, LocalDBHelper.STATUS_PENDING, LocalDBHelper.STATUS_READY);
                    }
                }
                if(forceOpen){
                    mDataDBHelper.close();
                }
            }
        } catch(Exception e){
            Log.e("WebService AT","Parsing JSON Error!", e);
        }
    }
}
