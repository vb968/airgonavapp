package com.ascamm.motion.database;

import com.ascamm.motion.utils.Point3DWithFloor;

import java.util.ArrayList;

public class PointWithCalibrationItems {
	private Point3DWithFloor mPoint;
	private ArrayList<CalibrationItem> mListOfCalibrationItems;
	
	public PointWithCalibrationItems(Point3DWithFloor point, ArrayList<CalibrationItem> listOfCalibs) {
		mPoint = point;
		mListOfCalibrationItems = listOfCalibs;
	}
		
	public Point3DWithFloor getPoint(){
		return mPoint;
	}
	
	public ArrayList<CalibrationItem> getCalibrationItems(){
		return mListOfCalibrationItems;
	}
	
	public void setCalibrationItems(ArrayList<CalibrationItem> cis){
		this.mListOfCalibrationItems = cis;
	}
	
}
