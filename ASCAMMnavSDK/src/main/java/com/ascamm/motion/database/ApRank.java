package com.ascamm.motion.database;

public class ApRank {
	private String mSSID;
	private String mBSSID;
	private int mRank;
	private int mCount;
	
	public ApRank(String bssid, int rank){
		mBSSID = bssid;
		mRank = rank;
	}

	public ApRank(String ssid, String bssid, int rank, int count){
		mSSID = ssid;
		mBSSID = bssid;
		mRank = rank;
		mCount = count;
	}
	
	public String getSSID() {
		return mSSID;
	}

	public String getBSSID(){
		return mBSSID;
	}
	
	public int getRank(){
		return mRank;
	}

	public int getCount(){
		return mCount;
	}
	
	public void setRank(int rank){
		mRank = rank;
	}
}
