package com.ascamm.motion.database;

import com.ascamm.utils.Point3D;

/**
 * Class used to store a BSSID-RSS pair for a position
 * @author Junzi, CTAE
 */
public class CalibrationPoint {
	private int id;
	private Point3D point;
	private String tech;
	
	public CalibrationPoint(int id, Point3D point, String tech) {
		this.id = id;
		this.point = point;
		this.tech = tech;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Point3D getPoint() {
		return point;
	}

	public void setPoint(Point3D point) {
		this.point = point;
	}

	public String getTech() {
		return tech;
	}

	public void setTech(String tech) {
		this.tech = tech;
	}
	
}
