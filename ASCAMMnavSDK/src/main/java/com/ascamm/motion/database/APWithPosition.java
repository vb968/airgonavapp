package com.ascamm.motion.database;

import com.ascamm.utils.Point3D;

public class APWithPosition {
	
	private String mSSID;
	private String mBSSID;
	private Point3D mPoint;
	
	public APWithPosition(String ssid, String bssid, Point3D point) {
		mSSID = ssid;
		mBSSID = bssid;
		mPoint = point;
	}
	
	public APWithPosition(String ssid, String bssid, double x, double y, double z) {
		mSSID = ssid;
		mBSSID = bssid;
		mPoint = new Point3D(x,y,z);
	}
	
	public String getSSID(){
		return mSSID;
	}
	
	public String getBSSID(){
		return mBSSID;
	}
	
	public Point3D getPoint(){
		return mPoint;
	}

}
