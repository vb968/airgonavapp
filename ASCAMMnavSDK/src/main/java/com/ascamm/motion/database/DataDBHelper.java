package com.ascamm.motion.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ascamm.motion.utils.Constants;
import com.ascamm.motion.LocationData;
import com.ascamm.motion.detections.DbDetection;
import com.ascamm.motion.detections.Detection;
import com.ascamm.utils.Point3D;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * Database helper class, defines the SQLite database, tables, columns, and all the methods provided publicly.
 * @author Dani Fernandez [CTAE]
 */
public class DataDBHelper extends SQLiteOpenHelper {

	private String TAG = getClass().getSimpleName();
	private boolean debug = Constants.DEBUGMODE;

	/* define Database, tables, and columns */
	public static final String 	DB_NAME 		=	"AscammDataDB";
	public static final int 	DB_VERSION		=	1;
	public static final String 	TB_POSITIONS 	= 	"positions";
	public static final String  TB_DETECTIONS   =   "detections";
	public static final String 	COL_ID 			= 	"id" ;
	public static final String 	COL_X	 		= 	"x" ;
	public static final String 	COL_Y 			= 	"y" ;
	public static final String 	COL_Z 			= 	"z" ;
	public static final String 	COL_LAT	 		= 	"lat" ;
	public static final String 	COL_LNG			= 	"lng" ;
	public static final String 	COL_ALT			= 	"alt" ;
	public static final String 	COL_TECH		= 	"tech" ;
	public static final String 	COL_ACCURACY	= 	"accuracy" ;
	public static final String 	COL_VELOCITY	= 	"velocity" ;
	public static final String 	COL_TIME		= 	"time" ;
	public static final String 	COL_SITE_ID		= 	"site_id" ;
	public static final String 	COL_USER_ID		= 	"user_id" ;
	public static final String 	COL_STATUS		= 	"status" ;
	public static final String  COL_DET_ID		=	"det_id" ;

	private static final String SQLITE_FILE_NAME = "ascamm_data.sqlite";

	public static final int STATUS_READY = 0;
	public static final int STATUS_PENDING = 1;

	private SQLiteDatabase db;

	public DataDBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	public DataDBHelper(Context context, boolean saveInSD) {
		super(context, Constants.APP_DATA_FOLDER + File.separator + DB_NAME + ".sqlite", null, DB_VERSION);
	}

	public DataDBHelper(Context context, boolean saveInSD, String userId) {
		super(context, Constants.APP_DATA_FOLDER + File.separator + DB_NAME + "_" + userId + ".sqlite", null, DB_VERSION);
	}

	/**
	 * Create one SQLite database named AscammLocationDB. This database is divided in multiple tables: 
	 *  <ul>
	 *  <li>TB_POSITIONS: Contains information related to Positions of the user</li>
	 *  </ul>
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			
			db.execSQL("CREATE TABLE " + TB_POSITIONS + " (" 
					+ COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
					+ COL_X + " FLOAT, " 
					+ COL_Y + " FLOAT, " 
					+ COL_Z + " FLOAT, " 
					+ COL_LAT + " FLOAT, " 
					+ COL_LNG + " FLOAT, " 
					+ COL_ALT + " FLOAT, " 
					+ COL_TECH + " TEXT, " 
					+ COL_ACCURACY + " FLOAT, " 
					+ COL_VELOCITY + " FLOAT, " 
					+ COL_TIME + " DOUBLE, " 
					+ COL_SITE_ID + " INTEGER, "
					+ COL_USER_ID + " TEXT, "
					+ COL_STATUS + " INTEGER "
					+ ");");

			db.execSQL("CREATE TABLE " + TB_DETECTIONS + " ("
					+ COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ COL_X + " FLOAT, "
					+ COL_Y + " FLOAT, "
					+ COL_Z + " FLOAT, "
					+ COL_LAT + " FLOAT, "
					+ COL_LNG + " FLOAT, "
					+ COL_ALT + " FLOAT, "
					+ COL_TECH + " TEXT, "
					+ COL_TIME + " DOUBLE, "
					+ COL_SITE_ID + " INTEGER, "
					+ COL_USER_ID + " TEXT, "
					+ COL_DET_ID + " TEXT, "
					+ COL_STATUS + " INTEGER "
					+ ");");

		} catch ( SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		/* 
		   Called when the database needs to be upgraded. The implementation 
		   should use this method to drop tables, add tables, or do anything 
		   else it needs to upgrade to the new schema version. 
		 */
	}
	
	public boolean isOpened(){
		boolean isOpened = false;
		if(db!=null && db.isOpen()){
			isOpened = true;
		}
		if(debug)	Log.d(TAG,"isOpened: " + isOpened);
		return isOpened;
	}
	
	public boolean open(){
		boolean success = false;
		try {
			db = getWritableDatabase();
			success = true;
			if(debug)	Log.d(TAG,"DB opened");
		} catch(SQLiteException e){
			Log.e(TAG,"Problems opening DB");
		}
		return success;
	}
	
	public void close(){
		if(debug)	Log.d(TAG,"closing DB");
		if(db!=null && db.isOpen()){
			db.close();
		}
	}
	
	/**
	 * Export the SQLite database to SD card as a ".sqlite" file
	 */
	public boolean exportDB(String targetDir){
		//SQLiteDatabase db = getReadableDatabase();
		String targetPath = targetDir + "/" + SQLITE_FILE_NAME;

		File targetFolder = new File(targetDir);
		if (!targetFolder.exists()) {
			targetFolder.mkdirs();
		}
		try{
			File sourceFile = new File(db.getPath());
			FileInputStream inStream = new FileInputStream(sourceFile);

			File targetFile = new File(targetPath);
			FileOutputStream outStream = new FileOutputStream(targetFile);

			int c;
			while( (c = inStream.read()) != -1 ){
				outStream.write(c);
			}
			inStream.close();
			outStream.close();

		} catch (Exception e) {
			Log.e("DBHelper", "Can not export database to file");
			return false;
		}

		//db.close();
		return true;
	}

	/**
	 * Import the ".sqlite" file to SQLite database
	 */
	public boolean importDB(String sourceDirPath){
		String sourceFilePath = sourceDirPath + "/" + SQLITE_FILE_NAME;
		//SQLiteDatabase db = getReadableDatabase();

		try{
			File sourceFile = new File(sourceFilePath);
			FileInputStream inStream = new FileInputStream(sourceFile);

			File targetFile = new File(db.getPath());
			FileOutputStream outStream = new FileOutputStream(targetFile);

			int c;
			while( (c = inStream.read()) != -1 ){
				outStream.write(c);
			}
			inStream.close();
			outStream.close();

		} catch (Exception e) {
			Log.e("DBHelper", "Can not import database file");
			return false;
		}

		//db.close();
		return true;
	}

	/**
	 *  Insert a new position to the database Positions table
	 * @param pos - LocationData
	 * @param userId - String
	 * @return newRecordID
	 */
	public long insertPosition(LocationData pos, String userId){
		long newRecordID = -1;
		//SQLiteDatabase db = getWritableDatabase();
		if(isOpened()){
			try{
				ContentValues cvs = new ContentValues();
				cvs.put(COL_X, pos.positionLocal[0]);
				cvs.put(COL_Y, pos.positionLocal[1]);
				cvs.put(COL_Z, pos.positionLocal[2]);
				cvs.put(COL_LAT, pos.positionGlobal[0]);
				cvs.put(COL_LNG, pos.positionGlobal[1]);
				cvs.put(COL_ALT, pos.positionGlobal[2]);
				cvs.put(COL_TECH, pos.sourceType);
				cvs.put(COL_ACCURACY, pos.accuracy);
				cvs.put(COL_VELOCITY, pos.velocity);
				cvs.put(COL_TIME, pos.utctime);
				cvs.put(COL_SITE_ID, pos.siteId);
				cvs.put(COL_USER_ID, userId);
				cvs.put(COL_STATUS, STATUS_READY);
				newRecordID = db.insert(TB_POSITIONS, null, cvs);
			}catch(Exception e){
				Log.e(TAG,"insertPosition exception!!!");
				e.printStackTrace();
			}
		}
		//db.close();
		return newRecordID;
	}
	
	/**
	 * Update a position from database Positions table
	 * @param db - A writable SQLiteDatabase object
	 * @param id - Position ID to update
	 * @param oldStatus - The old status
	 * @param newStatus - The new status
	 * @return updatedRow - number of rows updated
	 */
	private int updatePosition(SQLiteDatabase db, int id, int oldStatus, int newStatus){
		ContentValues cvs = new ContentValues();
		cvs.put(COL_STATUS, newStatus);
		int updatedRow = db.update(TB_POSITIONS, cvs, COL_ID+"="+id+" AND "+COL_STATUS+"="+oldStatus, null);
		return updatedRow;
	}
	
	/**
	 * Update positions from the list
	 * @param ids - list with positions' IDs
	 * @param oldStatus - The old status
	 * @param newStatus - The new status
	 * @return updatedRows - number of rows updated
	 */
	public int updatePositions(ArrayList<Integer> ids, int oldStatus, int newStatus){
		int rowsUpdated = 0;
		if(isOpened()){
			//SQLiteDatabase db = getWritableDatabase();
			//db.beginTransaction();	// Begin Transaction 
			for(int id : ids){
				int updated = updatePosition(db, id, oldStatus, newStatus);
				rowsUpdated += updated;
			}
			//db.setTransactionSuccessful();
			//db.endTransaction();	// End Transaction
			//db.close();
		}
		if(debug)	Log.d(TAG,"rowsUpdated: " + rowsUpdated);
		return rowsUpdated;
	}
	
	/**
	 * Update all positions from the table
	 * @param newStatus - The new status
	 * @return updatedRows - number of rows updated
	 */
	public int updateAllPositions(int newStatus){
		int updatedRows = 0;
		if(isOpened()){
			//SQLiteDatabase db = getWritableDatabase();
			//db.beginTransaction();	// Begin Transaction 
			
			ContentValues cvs = new ContentValues();
			cvs.put(COL_STATUS, newStatus);
			updatedRows = db.update(TB_POSITIONS, cvs, null, null);
			
			//db.setTransactionSuccessful();
			//db.endTransaction();	// End Transaction
			//db.close();
		}
		if(debug)	Log.d(TAG,"updatedRows: " + updatedRows);
		return updatedRows;
	}
	
	/**
	 * Delete a position from database Positions table
	 * @param db - A writable SQLiteDatabase object
	 * @param id - Position ID to delete
	 * @return deletedRow - number of rows deleted
	 */
	private int deletePosition(SQLiteDatabase db, int id){
		int deleteRow = db.delete(TB_POSITIONS, COL_ID+"="+id, null);
		return deleteRow;
	}

	/**
	 * Delete positions from the list
	 * @param ids - list with positions' IDs
	 * @return deletedRows - number of rows deleted
	 */
	public int deletePositions(ArrayList<Integer> ids){
		int rowsDeleted = 0;
		if(isOpened()){
			//SQLiteDatabase db = getWritableDatabase();
			//db.beginTransaction();	// Begin Transaction 
			for(int id : ids){
				int deleted = deletePosition(db,id);
				rowsDeleted += deleted;
			}
			//db.setTransactionSuccessful();
			//db.endTransaction();	// End Transaction
			//db.close();
		}
		if(debug)	Log.d(TAG,"rowsDeleted: " + rowsDeleted);
		return rowsDeleted;
	}
	
	/**
	 * Get positions with status from the table
	 * @param status - the status of the desired positions
	 * @return ArrayList<Position> - an array of position objects
	 */
	public ArrayList<Position> getPositionsWithStatus(int status){
		ArrayList<Position> positions = new ArrayList<Position>();
		if(isOpened()){
			//SQLiteDatabase db = getReadableDatabase();
	
			String sql = "SELECT * FROM " + TB_POSITIONS + " WHERE " + COL_STATUS + "=" + status + " LIMIT 0,5000";
			Cursor cur = db.rawQuery(sql, null);
			//Log.d(TAG,"sql: " + sql);
			
			while (cur.moveToNext()){
				Position position = new Position(
						cur.getInt(0),			// id
						cur.getDouble(1),		// x
						cur.getDouble(2),		// y
						cur.getDouble(3),		// z
						cur.getDouble(4),		// lat
						cur.getDouble(5),		// lng
						cur.getDouble(6),		// alt
						cur.getString(7),		// tech
						cur.getFloat(8),		// accuracy
						cur.getFloat(9),		// velocity
						cur.getDouble(10),		// time
						cur.getInt(11), 		// siteId
						cur.getString(12),		// userId
						cur.getInt(13)			// status
				);
				positions.add(position);
			}
	
			//db.close();
		}
		return positions;
	}


	/**
	 *  Insert a new detection to the database Detections table
	 * @param det - Detection
	 * @param userId - String
	 * @return newRecordID
	 */
	public long insertDetection(Detection det, long ts, int siteId, String userId){
		long newRecordID = -1;
		//SQLiteDatabase db = getWritableDatabase();
		if(isOpened()){
			try{
				ContentValues cvs = new ContentValues();
				cvs.put(COL_X, det.getPosLocal().x);
				cvs.put(COL_Y, det.getPosLocal().y);
				cvs.put(COL_Z, det.getPosLocal().z);
				cvs.put(COL_LAT, det.getPosGlobal().x);
				cvs.put(COL_LNG, det.getPosGlobal().y);
				cvs.put(COL_ALT, det.getPosGlobal().z);
				cvs.put(COL_TIME, ts);
				cvs.put(COL_SITE_ID, siteId);
				cvs.put(COL_USER_ID, userId);
				cvs.put(COL_DET_ID, det.getMac());
				cvs.put(COL_STATUS, STATUS_READY);
				newRecordID = db.insert(TB_DETECTIONS, null, cvs);
			}catch(Exception e){
				Log.e(TAG,"insertDetection exception!!!");
				e.printStackTrace();
			}
		}
		//db.close();
		return newRecordID;
	}

	/**
	 * Update a detection from database Detections table
	 * @param db - A writable SQLiteDatabase object
	 * @param id - Detection ID to update
	 * @param oldStatus - The old status
	 * @param newStatus - The new status
	 * @return updatedRow - number of rows updated
	 */
	private int updateDetection(SQLiteDatabase db, int id, int oldStatus, int newStatus){
		ContentValues cvs = new ContentValues();
		cvs.put(COL_STATUS, newStatus);
		int updatedRow = db.update(TB_DETECTIONS, cvs, COL_ID+"="+id+" AND "+COL_STATUS+"="+oldStatus, null);
		return updatedRow;
	}

	/**
	 * Update detections from the list
	 * @param ids - list with detections' IDs
	 * @param oldStatus - The old status
	 * @param newStatus - The new status
	 * @return updatedRows - number of rows updated
	 */
	public int updateDetections(ArrayList<Integer> ids, int oldStatus, int newStatus){
		int rowsUpdated = 0;
		if(isOpened()){
			//SQLiteDatabase db = getWritableDatabase();
			//db.beginTransaction();        // Begin Transaction
			for(int id : ids){
				int updated = updateDetection(db, id, oldStatus, newStatus);
				rowsUpdated += updated;
			}
			//db.setTransactionSuccessful();
			//db.endTransaction();  // End Transaction
			//db.close();
		}
		if(debug)       Log.d(TAG,"detectionRowsUpdated: " + rowsUpdated);
		return rowsUpdated;
	}

	/**
	 * Update all detections from the table
	 * @param newStatus - The new status
	 * @return updatedRows - number of rows updated
	 */
	public int updateAllDetections(int newStatus){
		int updatedRows = 0;
		if(isOpened()){
			//SQLiteDatabase db = getWritableDatabase();
			//db.beginTransaction();        // Begin Transaction

			ContentValues cvs = new ContentValues();
			cvs.put(COL_STATUS, newStatus);
			updatedRows = db.update(TB_DETECTIONS, cvs, null, null);

			//db.setTransactionSuccessful();
			//db.endTransaction();  // End Transaction
			//db.close();
		}
		if(debug)       Log.d(TAG,"detectionRowsUpdated: " + updatedRows);
		return updatedRows;
	}

	/**
	 * Delete a detection from database Detections table
	 * @param db - A writable SQLiteDatabase object
	 * @param id - Position ID to delete
	 * @return deletedRow - number of rows deleted
	 */
	private int deleteDetection(SQLiteDatabase db, int id){
		int deleteRow = db.delete(TB_DETECTIONS, COL_ID+"="+id, null);
		return deleteRow;
	}

	/**
	 * Delete detections from the list
	 * @param ids - list with detections' IDs
	 * @return deletedRows - number of rows deleted
	 */
	public int deleteDetections(ArrayList<Integer> ids){
		int rowsDeleted = 0;
		if(isOpened()){
			//SQLiteDatabase db = getWritableDatabase();
			//db.beginTransaction();        // Begin Transaction
			for(int id : ids){
				int deleted = deleteDetection(db,id);
				rowsDeleted += deleted;
			}
			//db.setTransactionSuccessful();
			//db.endTransaction();  // End Transaction
			//db.close();
		}
		if(debug)       Log.d(TAG,"detctionRowsDeleted: " + rowsDeleted);
		return rowsDeleted;
	}

	/**
	 * Get detections with status from the table
	 * @param status - the status of the desired detections
	 * @return ArrayList<DbDetection> - an array of DbDetection objects
	 */
	public ArrayList<DbDetection> getDetectionsWithStatus(int status){
		ArrayList<DbDetection> detections = new ArrayList<DbDetection>();
		if(isOpened()){
			//SQLiteDatabase db = getReadableDatabase();

			String sql = "SELECT * FROM " + TB_DETECTIONS + " WHERE " + COL_STATUS + "=" + status + " LIMIT 0,5000";
			Cursor cur = db.rawQuery(sql, null);
			//Log.d(TAG,"sql: " + sql);

			while (cur.moveToNext()){
				DbDetection det = new DbDetection(
						cur.getInt(0),                  // id
						cur.getString(11),              // MAC
						new Point3D(cur.getDouble(1), cur.getDouble(2), cur.getDouble(3)),              // posLocal
						new Point3D(cur.getDouble(4), cur.getDouble(5), cur.getDouble(6)),              // posGlobal
						(long) cur.getDouble(8),                // time
						cur.getInt(9),          // siteId
						cur.getString(10)               // userId
				);
				detections.add(det);
			}

			//db.close();
		}
		return detections;
	}


}
