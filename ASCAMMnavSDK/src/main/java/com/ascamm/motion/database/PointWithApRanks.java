package com.ascamm.motion.database;

import java.util.ArrayList;

import android.graphics.Point;

public class PointWithApRanks {
	private int mPosX;
	private int mPosY;
	private ArrayList<ApRank> mListOfApRanks;

	public PointWithApRanks(int pos_x, int pos_y, ArrayList<ApRank> listOfApRanks) {
		mPosX = pos_x;
		mPosY = pos_y;
		mListOfApRanks = listOfApRanks;
	}
	
	public int getPositionX(){
		return mPosX;
	}
	
	public int getPositionY(){
		return mPosY;
	}
	
	public Point getPoint(){
		return new Point(mPosX, mPosY);
	}
	
	public ArrayList<ApRank> getListOfApRanks(){
		return mListOfApRanks;
	}
}
