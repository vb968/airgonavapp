package com.ascamm.motion.database;

import java.util.ArrayList;

import com.ascamm.motion.utils.PointWithOnlineItems;

public class PointsAndEdges {

	private ArrayList<PointWithOnlineItems> pointsWithOnlineItems;
	private ArrayList<Integer> edgesId;
	
	public PointsAndEdges(ArrayList<PointWithOnlineItems> pointsWithOnlineItems, ArrayList<Integer> edgesId){
		this.pointsWithOnlineItems = pointsWithOnlineItems;
		this.edgesId = edgesId;
	}

	public ArrayList<PointWithOnlineItems> getPointsWithOnlineItems() {
		return pointsWithOnlineItems;
	}

	public void setPointsWithOnlineItems(
			ArrayList<PointWithOnlineItems> pointsWithOnlineItems) {
		this.pointsWithOnlineItems = pointsWithOnlineItems;
	}

	public ArrayList<Integer> getEdgesId() {
		return edgesId;
	}

	public void setEdgesId(ArrayList<Integer> edgesId) {
		this.edgesId = edgesId;
	}
	
	
}
