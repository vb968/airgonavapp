package com.ascamm.motion.database;

public class Position {

	private int id;
	private double x;
	private double y;
	private double z;
	private double lat;
	private double lng;
	private double alt;
	private String tech;
	private float accuracy;
	private float velocity;
	private double time;
	private int siteId;
	private String userId;
	private int status;
	
	public Position(int id, double x, double y, double z, double lat, double lng, double alt, String tech, float accuracy, float velocity, double time, int siteId, String userId, int status){
		this.id = id;
		this.x = x;
		this.y = y;
		this.z = z;
		this.lat = lat;
		this.lng = lng;
		this.alt = alt;
		this.tech = tech;
		this.accuracy = accuracy;
		this.velocity = velocity;
		this.time = time;
		this.siteId = siteId;
		this.userId = userId;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public double getAlt() {
		return alt;
	}

	public void setAlt(double alt) {
		this.alt = alt;
	}

	public String getTech() {
		return tech;
	}

	public void setTech(String tech) {
		this.tech = tech;
	}

	public float getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(float accuracy) {
		this.accuracy = accuracy;
	}

	public float getVelocity() {
		return velocity;
	}

	public void setVelocity(float velocity) {
		this.velocity = velocity;
	}

	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}

	public int getSiteId() {
		return siteId;
	}

	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getPosGlobal(){
		return "PosGlobal: ("+lat+","+lng+","+alt+")";
	}
}
