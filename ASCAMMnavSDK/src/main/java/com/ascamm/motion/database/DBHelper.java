package com.ascamm.motion.database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Point;
import android.util.Log;

import com.ascamm.motion.algorithm.APData;
import com.ascamm.motion.algorithm.CoordConversion;
import com.ascamm.motion.utils.Edge;

import com.ascamm.motion.utils.Constants;
import com.ascamm.motion.utils.FloorData;
import com.ascamm.motion.utils.Point3DWithFloor;
import com.ascamm.motion.utils.Util;
import com.ascamm.utils.Point3D;

/**
 * Database helper class, defines the SQLite database, tables, columns, and all the methods provided publicly.
 * @author Junzi Sun [CTAE]
 */
public class DBHelper extends SQLiteOpenHelper {
	
	private String TAG = getClass().getSimpleName();
	
	private boolean debug = Constants.DEBUGMODE;
	
	/* define Database, tables, and columns */
	public static final String 	DB_NAME 		=	"pos_calc_db";
	public static final int 	DB_VERSION			=	1;
	
	public static final String 	TB_APS		 	= 	"aps";
	public static final String 	TB_NODES	 	= 	"nodes";
	public static final String 	TB_EDGES	 	= 	"edges";
	public static final String 	TB_POINTS	 	= 	"points";
	public static final String 	TB_MEAS_POINTS	= 	"measpoints";
	public static final String 	TB_ZONES	 	= 	"zones";
	public static final String 	TB_MEAS_ZONES	= 	"measzones";
	public static final String 	TB_SITE_INFO	= 	"site_info";
	public static final String 	TB_CONV_MATRIX	= 	"conversion_matrix";
	
	public static final String 	COL_ID 			= 	"id" ; 
	public static final String 	COL_ID_START_NODE	= 	"start_node" ;
	public static final String 	COL_ID_END_NODE	= 	"end_node" ;
	public static final String 	COL_ID_POINT	= 	"point_id" ;
	public static final String 	COL_ID_ZONE		= 	"zone_id" ;
	public static final String 	COL_SSID 		= 	"ssid" ; 
	public static final String 	COL_BSSID 		= 	"mac" ; 
	public static final String 	COL_POSITIONX 	= 	"x_coor" ;
	public static final String 	COL_POSITIONY 	= 	"y_coor" ;
	public static final String 	COL_POSITIONZ 	= 	"z_coor" ;
	public static final String 	COL_RSS_AVG		= 	"rss_avg" ;				/* Received Signal Strength */
	public static final String 	COL_RSS_STD 	= 	"rss_std" ;
	public static final String 	COL_TECH 		= 	"tech" ;
	public static final String 	COL_COUNT 		= 	"count" ;
	public static final String 	COL_PERCENTAGE 	= 	"percentage" ;
	public static final String 	COL_ORDER		= 	"order_num" ;
	public static final String 	COL_LENGTH		= 	"length" ;
	public static final String 	COL_WEIGHT		= 	"weight" ;
	public static final String 	COL_SCALE		= 	"scale" ;
	public static final String 	COL_RESOLUTION	= 	"resolution" ;
	public static final String 	COL_M_11	= 	"m11" ;
	public static final String 	COL_M_12	= 	"m12" ;
	public static final String 	COL_M_13	= 	"m13" ;
	public static final String 	COL_M_21	= 	"m21" ;
	public static final String 	COL_M_22	= 	"m22" ;
	public static final String 	COL_M_23	= 	"m23" ;
	public static final String 	COL_M_31	= 	"m31" ;
	public static final String 	COL_M_32	= 	"m32" ;
	public static final String 	COL_M_33	= 	"m33" ;
	
	public SQLiteDatabase mDB;
	private static String DB_PATH = Constants.APP_DATA_FOLDER.toString();
	private static String DB_FILE = "default.db";
	
	private static final int DEFAULT_RANK = 9999;
	private static final int DEFAULT_RSS = -150;
	
	private float DEFAULT_RSS_STDEV = Constants.mDefaultRssStdev;
	private float MAX_RSS_STDEV = Constants.mMaxRssStdev;
    private int MIN_COUNT = Constants.mMinCount;
    private float MIN_COUNT_PERCENTAGE = Constants.mMinCountPercentage;
	private float N_STD = Constants.DB_N_STD;

	private Context mContext;
	private int idSite = 0;
	private boolean dbOpened = false;

	private String getDatabaseFilename(int idSite){
		String DB_FILE = "default.db";
		String SQLITE_FILE_NAME_SITE_1 = "pdb_site_1.db";
		//String SQLITE_FILE_NAME_SITE_1 = "pdb_site_1_Method_1_1.5_15.db";
		//String SQLITE_FILE_NAME = "pdb_site_1_Method_2_1.5_15.db";
		//String SQLITE_FILE_NAME = "pdb_site_1_Method_3_7.5_7.5.db";
		String SQLITE_FILE_NAME_SITE_2 = "pdb_site_2.db";
		//String SQLITE_FILE_NAME_SITE_3 = "pdb_site_3_res1_0dBm.db";
		//String SQLITE_FILE_NAME_SITE_3 = "pdb_site_3_res0$5_0dBm.db";
		String SQLITE_FILE_NAME_SITE_3 = "pdb_site_3_res0$5_0dBm_ibeacon.db";
		//String SQLITE_FILE_NAME_SITE_3 = "pdb_site_3_res1_minus8dBm.db";
		//String SQLITE_FILE_NAME_SITE_3 = "pdb_site_3_res0$5_minus8dBm.db";
		//String SQLITE_FILE_NAME_SITE_3 = "pdb_site_3_res1_minus16dBm.db";
		//String SQLITE_FILE_NAME_SITE_3 = "pdb_site_3_res0$5_minus16dBm.db";
		String SQLITE_FILE_NAME_SITE_5 = "pdb_site_5.db";
		String SQLITE_FILE_NAME_SITE_6 = "pdb_site_6.db";
		if(idSite==1){
			DB_FILE = SQLITE_FILE_NAME_SITE_1;
		}
		else if(idSite==2){
			DB_FILE = SQLITE_FILE_NAME_SITE_2;
		}
		else if(idSite==3){
			DB_FILE = SQLITE_FILE_NAME_SITE_3;
		}
		else DB_FILE = "pdb_site_" + String.valueOf(idSite) + ".db";
		return DB_FILE;
	}
	
	public DBHelper(Context context, int idSite, String dbFile, boolean fromAssets) {
		super(context, DB_NAME, null, DB_VERSION);
		this.mContext = context;
		this.idSite = idSite;
		if(dbFile == null){
			dbFile = getDatabaseFilename(idSite);
		}
		if(fromAssets){
			if(idSite==23){
				dbFile = "pdb_CB_v2.db";
			}
			boolean dbexist = checkDatabase(dbFile);
	        if (dbexist) {
	            //System.out.println("Database exists");
	        	dbOpened = openDatabase(dbFile);
	        } else {
	        	if(debug){
	            	String msgTxt = "Database doesn't exist. Copy and open it";
	            	Log.d(TAG,msgTxt);
	            	//Toast.makeText(context, msgTxt, Toast.LENGTH_SHORT).show();
	            }
	            if(copyDatabaseFromAssetsToSDcard(dbFile)){
            		dbOpened = openDatabase(dbFile);
            	}
	        }
		}
		else{
			dbFile = idSite + "/" + dbFile;
			boolean dbexist = checkDatabase(dbFile);
	        if (dbexist) {
	            //System.out.println("Database exists");
	        	dbOpened = openDatabase(dbFile);
	        } else {
	        	String msgTxt = "Database doesn't exist";
	            Log.e(TAG,msgTxt);
	            //Toast.makeText(context, msgTxt, Toast.LENGTH_SHORT).show();
	        }
		}
	}
	
	//Copy the database from assets
    private boolean copyDatabaseFromAssetsToSDcard(String dbFile) {
    	boolean success = false;
		boolean dbFolderOk = true;
    	try{
			File dbFolder = new File(DB_PATH);
			if(!dbFolder.exists()){
				dbFolderOk = dbFolder.mkdir();
			}
			if(dbFolderOk) {
				InputStream mInput = mContext.getAssets().open("databases/" + dbFile);
				String outFileName = DB_PATH + "/" + dbFile;
				OutputStream mOutput = new FileOutputStream(outFileName);
				byte[] mBuffer = new byte[1024];
				int mLength;
				while ((mLength = mInput.read(mBuffer)) > 0) {
					mOutput.write(mBuffer, 0, mLength);
				}
				mOutput.flush();
				mOutput.close();
				mInput.close();
				success = true;
			}
    	} catch (IOException e){
    		Log.e(TAG,"Error copying Database from Assets to SD card");
        	e.printStackTrace();
    	}
    	return success;
    }
	
	private boolean checkDatabase(String dbFile) {
        boolean checkdb = false;
        String myPath = DB_PATH + "/" + dbFile;
        File dbfile = new File(myPath);
        checkdb = dbfile.exists();
        return checkdb;
    }
	
	private boolean openDatabase(String dbFile) {
        boolean success = false;
		//Open the database
        String mypath = DB_PATH + "/" + dbFile;
        if(debug){
        	Log.d(TAG,"Opening DB");
        	if(mDB!=null){
            	Log.d(TAG,"It's open? "  + mDB.isOpen());
            }
        }
        try{
        	mDB = SQLiteDatabase.openDatabase(mypath, null, SQLiteDatabase.OPEN_READWRITE);
        	success = true;
        } catch(SQLiteException e) {
        	Log.e(TAG,"Error opening DB");
        }
        return success;
    }
	
	public void closeDatabase() {
		if(debug)	Log.d(TAG,"closing DB. dbOpened: "  + dbOpened);
		if(dbOpened){
			//Close the database
			mDB.close();
			dbOpened = false;
        }
    }
	
	public boolean isDatabaseOk(){
		return dbOpened;
	}
	
	private boolean tableExists(String tableName){
		boolean exists = false;
		String sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='"+tableName+"'";
		Cursor cur = mDB.rawQuery(sql, null);
		
		if (cur.moveToFirst()){
			exists = true;
		}
		return exists;
	}

	/**
	 * Create one SQLite database named AscammNavDB. This database is divided in multiple tables: 
	 *  <ul>
	 *  <li>TB_APS: Contains information related to APs of the site</li>
	 *  <li>TB_NODES: Contains information of the path to calibrate</li>
	 *  <li>TB_EDGES: Contains information of the path to calibrate</li>
	 *  <li>TB_CALIB_POINTS: Contains calibration information (points calibrated)</li>
	 *  <li>TB_CALIB_MEAS: Contains calibration information (RSS measurements)</li>
	 *  </ul>
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		if(debug)	Log.d(TAG,"inside onCreate");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		/* 
		   Called when the database needs to be upgraded. The implementation 
		   should use this method to drop tables, add tables, or do anything 
		   else it needs to upgrade to the new schema version. 
		 */
		if(debug)	Log.d(TAG,"inside onUpgrade");
	}

	/**
	 * Get an ArrayList of all points that have been calibrated 
	 * @return points - ArrayList of points 
	 */
	public ArrayList<Point> getCalibratedPositions(int floor){
		ArrayList<Point> points = new ArrayList<Point>();
		if(isDatabaseOk()){
			String sql = "SELECT "+COL_POSITIONX+", "+COL_POSITIONY+" FROM "+TB_POINTS+" WHERE "+COL_POSITIONZ+"="+floor;
			Cursor cur = mDB.rawQuery(sql, null);
	
			while (cur.moveToNext()){
				Point p = new Point(cur.getInt(0), cur.getInt(1));
				points.add(p);
			}
		}
		return points;
	}

	public ArrayList<APWithPosition> getAllAPs(){
		ArrayList<APWithPosition> aps = new ArrayList<APWithPosition>();
		if(isDatabaseOk()){
			String sql = "SELECT * FROM "+TB_APS;
			Cursor cur = mDB.rawQuery(sql, null);
	
			while (cur.moveToNext()){
				Point3D p = new Point3D(cur.getFloat(1), cur.getFloat(2), cur.getFloat(3));
				APWithPosition apWithPosition = new APWithPosition(
						cur.getString(4),		// ssid
						cur.getString(0).toLowerCase(),		// bssid
						p		// Point3D
				);
				aps.add(apWithPosition);
			}
			/*
			for(APWithPosition ap : aps){
				Log.d(TAG,"ap: (" + ap.getSSID() + " , " + ap.getBSSID() + ") , " + ap.getPoint().toString());
			}
			*/
		}
		return aps;
	}

	public ArrayList<String> getAllAPsMac(){
		ArrayList<String> aps = new ArrayList<String>();
		if(isDatabaseOk()){
			String sql = "SELECT "+COL_BSSID+" FROM "+TB_APS;
			Cursor cur = mDB.rawQuery(sql, null);
	
			while (cur.moveToNext()){
				aps.add(cur.getString(0).toLowerCase());		// mac address
			}
		}
		return aps;
	}
	
	public ArrayList<APWithPosition> getAPsOfFloor(int floor){
		ArrayList<APWithPosition> aps = new ArrayList<APWithPosition>();
		if(isDatabaseOk()){
			String sql = "SELECT * FROM "+TB_APS+" WHERE "+COL_POSITIONZ+"="+floor;
			Cursor cur = mDB.rawQuery(sql, null);
	
			while (cur.moveToNext()){
				Point3D p = new Point3D(cur.getFloat(1), cur.getFloat(2), cur.getFloat(3));
				APWithPosition apWithPosition = new APWithPosition(
						cur.getString(4),		// ssid
						cur.getString(0).toLowerCase(),		// bssid
						p		// Point3D
				);
				aps.add(apWithPosition);
			}
		}
		return aps;
	} 
	
	public ArrayList<Integer> getPointsToUseWithOAP(ArrayList<APData> aps, int floor){
		ArrayList<Integer> points = new ArrayList<>();
		String aps_list = "(";
		int N = aps.size();
		for(int i=0; i<N; i++){
			if(i==(N-1)){
				aps_list += '"' + aps.get(i).getBSSID() + '"';
			}
			else{
				aps_list += '"' + aps.get(i).getBSSID() + '"' + ",";
			}
		}
		aps_list += ")"; 
		if(isDatabaseOk()){
			String sql = "SELECT DISTINCT p."+COL_ID+" FROM "+TB_MEAS_POINTS+" as m INNER JOIN "+TB_POINTS+" as p ON p."+COL_ID+"=m."+COL_ID_POINT+" WHERE m."+COL_BSSID+" in "+aps_list;
			if(floor != -100){
				sql += getFloorCondition(floor);
			}
			
			Cursor cur = mDB.rawQuery(sql, null);
	
			while (cur.moveToNext()){
				points.add(cur.getInt(0));
			}
		}
		return points;
	}
	
	public ArrayList<Integer> getPointsToUseWithSAP(String apMaxRSS, int floor){
		ArrayList<Integer> points = new ArrayList<>();
		if(isDatabaseOk()){
			String sql = "SELECT DISTINCT p."+COL_ID+" FROM "+TB_MEAS_POINTS+" as m INNER JOIN "+TB_POINTS+" as p ON p."+COL_ID+"=m."+COL_ID_POINT+" WHERE m."+COL_BSSID+"="+'"'+apMaxRSS+'"';
			if(floor != -100){
				sql += getFloorCondition(floor);
			}
			
			Cursor cur = mDB.rawQuery(sql, null);
	
			while (cur.moveToNext()){
				points.add(cur.getInt(0));
			}
		}
		return points;
	}
	
	public ArrayList<Integer> getPointsToUseWithVSAP(String apMaxRSS, int floor){
		ArrayList<Integer> points = new ArrayList<Integer>();
		if(isDatabaseOk()){
			String sql = "SELECT DISTINCT p."+COL_ID+" FROM "+TB_MEAS_POINTS+" as m INNER JOIN "+TB_POINTS+" as p ON p."+COL_ID+"=m."+COL_ID_POINT+" WHERE m."+COL_BSSID+"="+'"'+apMaxRSS+'"'+" AND "+COL_ORDER+"=1";
			if(floor != -100){
				sql += getFloorCondition(floor);
			}
			//Log.d("DBHelper", "SQL: " + sql);
			
			Cursor cur = mDB.rawQuery(sql, null);
	
			while (cur.moveToNext()){
				points.add(cur.getInt(0));
			}
		}
		return points;
	}
	
	public ArrayList<Integer> getPointsToUseWithNVSAP(ArrayList<APData> aps, int num, int floor){
		ArrayList<Integer> points = new ArrayList<>();
		String aps_list = "(";
		int N = aps.size();
		if(num>N){
			num = N;
		}
		for(int i=0; i<num; i++){
			if(i==(num-1)){
				aps_list += '"' + aps.get(i).getBSSID() + '"';
			}
			else{
				aps_list += '"' + aps.get(i).getBSSID() + '"' + ",";
			}
		}
		aps_list += ")";
		if(isDatabaseOk()){
			String sql = "SELECT DISTINCT p."+COL_ID+" FROM "+TB_MEAS_POINTS+" as m INNER JOIN "+TB_POINTS+" as p ON p."+COL_ID+"=m."+COL_ID_POINT+" WHERE m."+COL_BSSID+" in "+aps_list+" AND "+COL_ORDER+"=1";
			
			if(floor != -100){
				sql += getFloorCondition(floor);
			}
			//Log.d("DBHelper", "SQL: " + sql);
			
			Cursor cur = mDB.rawQuery(sql, null);
	
			while (cur.moveToNext()){
				points.add(cur.getInt(0));
			}
		}
		return points;
	}
	
	private String getFloorCondition(int floor){
		//return " AND p."+COL_POSITIONZ+"="+floor;
		return " AND p."+COL_POSITIONZ+"<="+((float)floor+0.5)+" AND p."+COL_POSITIONZ+">"+((float)floor-0.5);
	}
	
	public ArrayList<Integer> getPointsZones(ArrayList<Integer> zonesId){
		ArrayList<Integer> points = new ArrayList<>();
		String zones_list = "(";
		int N = zonesId.size();
		for(int i=0; i<N; i++){
			if(i==(N-1)){
				zones_list += zonesId.get(i);
			}
			else{
				zones_list += zonesId.get(i) + ",";
			}
		}
		zones_list += ")";
		if(isDatabaseOk()){
			String sql = "SELECT "+COL_ID+" FROM "+TB_POINTS+" WHERE "+COL_ID_ZONE+" in "+zones_list;
			
			//Log.d("DBHelper", "SQL: " + sql);
			
			Cursor cur = mDB.rawQuery(sql, null);
	
			while (cur.moveToNext()){
				points.add(cur.getInt(0));
			}
		}
		return points;
	}

	public ArrayList<PointWithCalibrationItems> getPointsFloorDetermination(){
		ArrayList<PointWithCalibrationItems> calibs = new ArrayList<PointWithCalibrationItems>();
		ArrayList<CalibrationItem> tmpListofCalibrations = new ArrayList<CalibrationItem>();;
		int lastPosX, lastPosY, lastPosZ, currentPosX, currentPosY, currentPosZ;
		/*
		SQLiteDatabase db = getReadableDatabase();
		
		String sql = "select "+COL_POSITIONX+","+COL_POSITIONY+","+COL_POSITIONZ+","+COL_SSID+","+COL_BSSID+","+COL_RSS+","+COL_RSS_STD+","+COL_COUNT
		+" from "+TB_CALIBRATIONS +" where floor_det=1 order by "+COL_POSITIONX+" asc, "+COL_POSITIONY+" asc, "+COL_POSITIONZ+" asc";
		// Treure el order by per agilitzar la consulta
		
		Cursor cur = db.rawQuery(sql, null);

		if(cur.getCount()>0){
			// process with the first position
			cur.moveToFirst();
			
			// get result of the first record
			lastPosX = currentPosX	= cur.getInt(0);
			lastPosY = currentPosY	= cur.getInt(1);
			lastPosZ = currentPosZ  = cur.getInt(2);
			tmpListofCalibrations.add(new CalibrationItem(cur.getString(3),cur.getString(4),cur.getFloat(5),cur.getFloat(6),cur.getInt(7)));
			
			// next positions
			while (cur.moveToNext()){
				currentPosX = cur.getInt(0);
				currentPosY = cur.getInt(1);
				currentPosZ = cur.getInt(2);
				
				if(lastPosX==currentPosX && lastPosY==currentPosY && lastPosZ==currentPosZ){
					tmpListofCalibrations.add(new CalibrationItem(cur.getString(3),cur.getString(4),cur.getFloat(5),cur.getFloat(6),cur.getInt(7)));
				} else {
					//System.out.println("point: (" + lastPosX + " , " + lastPosY + " , " + lastPosZ + ") , calibs: " + tmpListofCalibrations.size());
					calibs.add(new PointWithCalibrationItems(lastPosX, lastPosY, lastPosZ, tmpListofCalibrations));
					tmpListofCalibrations = new ArrayList<CalibrationItem>();
					tmpListofCalibrations.add(new CalibrationItem(cur.getString(3),cur.getString(4),cur.getFloat(5),cur.getFloat(6),cur.getInt(7)));
					// update the value of the last position					
					lastPosX = currentPosX;
					lastPosY = currentPosY;
					lastPosZ = currentPosZ;
				}
			}

			// add the last position
			//System.out.println("point: (" + lastPosX + " , " + lastPosY + " , " + lastPosZ + ") , calibs: " + tmpListofCalibrations.size());
			calibs.add(new PointWithCalibrationItems(currentPosX, currentPosY, currentPosZ, tmpListofCalibrations));
		}
		db.close();
		*/
		return calibs;
	}
	
	public Hashtable<Integer,PointWithCalibrationItems> getPointsWithCalibrations(boolean useWifi, boolean useBle){
		Hashtable<Integer, PointWithCalibrationItems> calibs = new Hashtable<Integer, PointWithCalibrationItems>();
		ArrayList<CalibrationItem> tmpListofCalibrations = new ArrayList<CalibrationItem>();;
		int lastPosId, currentPosId;
		Point3DWithFloor lastPoint;

		if(isDatabaseOk()){

			HashMap<Integer,Double> floorsMap = getFloorsData();
			ArrayList<FloorData> floorsArray = getFloorsDataArrayComplete();
			String whereCondition = "";
			if(useWifi && useBle){
				whereCondition = " WHERE ("+COL_TECH+"='wifi' OR "+COL_TECH+"='ble')";
			}else if(useWifi){
				whereCondition = " WHERE "+COL_TECH+"='wifi'";
			}else if(useBle){
				whereCondition = " WHERE "+COL_TECH+"='ble'";
			}

			//whereCondition += " AND " + COL_PERCENTAGE + ">" + MIN_COUNT_PERCENTAGE;

			String sql = "SELECT p."+COL_ID+","+COL_POSITIONX+","+COL_POSITIONY+","+COL_POSITIONZ+","+COL_SSID+","+COL_BSSID+","+COL_RSS_AVG+","+COL_RSS_STD+","+COL_TECH+","+COL_COUNT+","+COL_PERCENTAGE+","+COL_ORDER
			+" FROM "+TB_POINTS +" AS p INNER JOIN "+TB_MEAS_POINTS+" AS m on m."+COL_ID_POINT+"=p."+COL_ID
			+ whereCondition
			+" ORDER BY p."+COL_ID+" asc";
			
			//Log.d(TAG,"sql: " + sql);
			
			Cursor cur = mDB.rawQuery(sql, null);
			if(cur.getCount()>0){
				// process with the first position
				cur.moveToFirst();
				
				// get result of the first record
				lastPosId = currentPosId = cur.getInt(0);
				double height = getHeightOfPoint(cur.getFloat(3), floorsMap, floorsArray);
				//Log.d(TAG,"point height: (" + cur.getFloat(3) + " , " + height + ")");
				lastPoint = new Point3DWithFloor(cur.getFloat(1), cur.getFloat(2), height, cur.getFloat(3));
				lastPoint.setId(lastPosId);
				double adjustedStdev = adjustRssStdev(cur.getFloat(7)*N_STD, cur.getInt(9), cur.getFloat(10));
				tmpListofCalibrations.add(new CalibrationItem(cur.getString(4),cur.getString(5),cur.getFloat(6),(float)adjustedStdev,cur.getString(8),cur.getInt(9),cur.getFloat(10),cur.getInt(11)));
				
				// next positions
				while (cur.moveToNext()){
					currentPosId = cur.getInt(0);
					
					if(lastPosId==currentPosId){
						double adjustedStdev2 = adjustRssStdev(cur.getFloat(7)*N_STD, cur.getInt(9), cur.getFloat(10));
                        tmpListofCalibrations.add(new CalibrationItem(cur.getString(4),cur.getString(5),cur.getFloat(6),(float)adjustedStdev2,cur.getString(8),cur.getInt(9),cur.getFloat(10),cur.getInt(11)));
					} else {
						//System.out.println("point_id: (" + lastPosId + ") , calibs: " + tmpListofCalibrations.size());
						PointWithCalibrationItems pwci = new PointWithCalibrationItems(lastPoint, tmpListofCalibrations);
						calibs.put(lastPosId, pwci);
						tmpListofCalibrations = new ArrayList<CalibrationItem>();
						double adjustedStdev2 = adjustRssStdev(cur.getFloat(7)*N_STD, cur.getInt(9), cur.getFloat(10));
                        tmpListofCalibrations.add(new CalibrationItem(cur.getString(4),cur.getString(5),cur.getFloat(6),(float)adjustedStdev2,cur.getString(8),cur.getInt(9),cur.getFloat(10),cur.getInt(11)));
						// update the value of the last position					
						lastPosId = currentPosId;
						height = getHeightOfPoint(cur.getFloat(3), floorsMap, floorsArray);
						//Log.d(TAG,"point height: (" + cur.getFloat(3) + " , " + height + ")");
						lastPoint = new Point3DWithFloor(cur.getFloat(1), cur.getFloat(2), height, cur.getFloat(3));
						lastPoint.setId(lastPosId);
					}
				}
	
				// add the last position
				//System.out.println("point_id: (" + lastPosId + ") , calibs: " + tmpListofCalibrations.size());
				PointWithCalibrationItems pwci = new PointWithCalibrationItems(lastPoint, tmpListofCalibrations);
				calibs.put(lastPosId, pwci);
			}
		}
		return calibs;
	}
	
	public Hashtable<Integer, PointWithCalibrationItems> getZonesWithCalibrations(){
		Hashtable<Integer, PointWithCalibrationItems> calibs = new Hashtable<Integer, PointWithCalibrationItems>();
		ArrayList<CalibrationItem> tmpListofCalibrations = new ArrayList<CalibrationItem>();;
		int lastPosId, currentPosId;
		Point3DWithFloor lastPoint;

		if(isDatabaseOk()){
			HashMap<Integer,Double> floorsMap = getFloorsData();
			ArrayList<FloorData> floorsArray = getFloorsDataArrayComplete();

			String sql = "SELECT z."+COL_ID+","+COL_POSITIONX+","+COL_POSITIONY+","+COL_POSITIONZ+","+COL_SSID+","+COL_BSSID+","+COL_RSS_AVG+","+COL_RSS_STD+","+COL_TECH+","+COL_COUNT+","+COL_PERCENTAGE+","+COL_ORDER
					+" FROM "+TB_ZONES +" AS z INNER JOIN "+TB_MEAS_ZONES+" AS m on m."+COL_ID_ZONE+"=z."+COL_ID
					+" ORDER BY z."+COL_ID+" asc";
					
			//Log.d(TAG,"sql: " + sql);
			
			Cursor cur = mDB.rawQuery(sql, null);
	
			if(cur.getCount()>0){
				// process with the first position
				cur.moveToFirst();
				
				// get result of the first record
				lastPosId = currentPosId = cur.getInt(0);
				double height = getHeightOfPoint(cur.getFloat(3), floorsMap, floorsArray);
				//Log.d(TAG,"point height: (" + cur.getFloat(3) + " , " + height + ")");
				lastPoint = new Point3DWithFloor(cur.getFloat(1), cur.getFloat(2), height, cur.getFloat(3));
				lastPoint.setId(lastPosId);
				tmpListofCalibrations.add(new CalibrationItem(cur.getString(4),cur.getString(5),cur.getFloat(6),cur.getFloat(7),cur.getString(8),cur.getInt(9),cur.getFloat(10),cur.getInt(11)));
				
				// next positions
				while (cur.moveToNext()){
					currentPosId = cur.getInt(0);
					
					if(lastPosId==currentPosId){
						tmpListofCalibrations.add(new CalibrationItem(cur.getString(4),cur.getString(5),cur.getFloat(6),cur.getFloat(7),cur.getString(8),cur.getInt(9),cur.getFloat(10),cur.getInt(11)));
					} else {
						//System.out.println("point: (" + lastPosX + " , " + lastPosY + " , " + lastPosZ + ") , calibs: " + tmpListofCalibrations.size());
						PointWithCalibrationItems pwci = new PointWithCalibrationItems(lastPoint, tmpListofCalibrations);
						calibs.put(lastPosId, pwci);
						tmpListofCalibrations = new ArrayList<CalibrationItem>();
						tmpListofCalibrations.add(new CalibrationItem(cur.getString(4),cur.getString(5),cur.getFloat(6),cur.getFloat(7),cur.getString(8),cur.getInt(9),cur.getFloat(10),cur.getInt(11)));
						// update the value of the last position					
						lastPosId = currentPosId;
						height = getHeightOfPoint(cur.getFloat(3), floorsMap, floorsArray);
						//Log.d(TAG,"point height: (" + cur.getFloat(3) + " , " + height + ")");
						lastPoint = new Point3DWithFloor(cur.getFloat(1), cur.getFloat(2), height, cur.getFloat(3));
						lastPoint.setId(lastPosId);
					}
				}
	
				// add the last position
				//System.out.println("point: (" + lastPosX + " , " + lastPosY + " , " + lastPosZ + ") , calibs: " + tmpListofCalibrations.size());
				PointWithCalibrationItems pwci = new PointWithCalibrationItems(lastPoint, tmpListofCalibrations);
				calibs.put(lastPosId, pwci);
			}
		}
		return calibs;
	}
	
	public Hashtable<Integer, PointWithCalibrationItems> getZonesWithCalibrationsOfRepresentativePoint(){
		//Hashtable<Point3D,ArrayList<CalibrationItem>> calibs = new Hashtable<Point3D,ArrayList<CalibrationItem>>();
		Hashtable<Integer, PointWithCalibrationItems> calibs = new Hashtable<Integer, PointWithCalibrationItems>();
		ArrayList<CalibrationItem> tmpListofCalibrations = new ArrayList<CalibrationItem>();;
		int lastPosId, currentPosId;
		Point3DWithFloor lastPoint;

		if(isDatabaseOk()){
			HashMap<Integer,Double> floorsMap = getFloorsData();
			ArrayList<FloorData> floorsArray = getFloorsDataArrayComplete();

			String sql = "SELECT z."+COL_ID+","+COL_POSITIONX+","+COL_POSITIONY+","+COL_POSITIONZ+","+COL_SSID+","+COL_BSSID+","+COL_RSS_AVG+","+COL_RSS_STD+","+COL_TECH+","+COL_COUNT+","+COL_PERCENTAGE+","+COL_ORDER
					+" FROM "+TB_ZONES +" AS z INNER JOIN "+TB_MEAS_POINTS+" AS m on m."+COL_ID_POINT+"=z."+COL_ID_POINT
					+" ORDER BY z."+COL_ID+" asc";
					
			//Log.d(TAG,"sql: " + sql);
			
			Cursor cur = mDB.rawQuery(sql, null);
	
			if(cur.getCount()>0){
				// process with the first position
				cur.moveToFirst();
				
				// get result of the first record
				lastPosId = currentPosId = cur.getInt(0);
				double height = getHeightOfPoint(cur.getFloat(3), floorsMap, floorsArray);
				//Log.d(TAG,"point height: (" + cur.getFloat(3) + " , " + height + ")");
				lastPoint = new Point3DWithFloor(cur.getFloat(1), cur.getFloat(2), height, cur.getFloat(3));
				lastPoint.setId(lastPosId);
				tmpListofCalibrations.add(new CalibrationItem(cur.getString(4),cur.getString(5),cur.getFloat(6),cur.getFloat(7),cur.getString(8),cur.getInt(9),cur.getFloat(10),cur.getInt(11)));
				
				// next positions
				while (cur.moveToNext()){
					currentPosId = cur.getInt(0);
					
					if(lastPosId==currentPosId){
						tmpListofCalibrations.add(new CalibrationItem(cur.getString(4),cur.getString(5),cur.getFloat(6),cur.getFloat(7),cur.getString(8),cur.getInt(9),cur.getFloat(10),cur.getInt(11)));
					} else {
						//System.out.println("point: (" + lastPosX + " , " + lastPosY + " , " + lastPosZ + ") , calibs: " + tmpListofCalibrations.size());
						PointWithCalibrationItems pwci = new PointWithCalibrationItems(lastPoint, tmpListofCalibrations);
						calibs.put(lastPosId, pwci);
						tmpListofCalibrations = new ArrayList<CalibrationItem>();
						tmpListofCalibrations.add(new CalibrationItem(cur.getString(4),cur.getString(5),cur.getFloat(6),cur.getFloat(7),cur.getString(8),cur.getInt(9),cur.getFloat(10),cur.getInt(11)));
						// update the value of the last position					
						lastPosId = currentPosId;
						height = getHeightOfPoint(cur.getFloat(3), floorsMap, floorsArray);
						//Log.d(TAG,"point height: (" + cur.getFloat(3) + " , " + height + ")");
						lastPoint = new Point3DWithFloor(cur.getFloat(1), cur.getFloat(2), height,cur.getFloat(3));
						lastPoint.setId(lastPosId);
					}
				}
	
				// add the last position
				//System.out.println("point: (" + lastPosX + " , " + lastPosY + " , " + lastPosZ + ") , calibs: " + tmpListofCalibrations.size());
				PointWithCalibrationItems pwci = new PointWithCalibrationItems(lastPoint, tmpListofCalibrations);
				calibs.put(lastPosId, pwci);
			}
		}
		return calibs;
	}

	public Hashtable<Integer,Point3D> getNodes(){
		Hashtable <Integer,Point3D> nodes = new Hashtable <Integer,Point3D>();
		if(isDatabaseOk()){
			String sql = "SELECT * FROM " + TB_NODES;
			Cursor cur = mDB.rawQuery(sql, null);
	
			while (cur.moveToNext()){
				Point3D node = new Point3D(
						cur.getFloat(1),		// x
						cur.getFloat(2),		// y
						cur.getFloat(3)		// z
						);
				nodes.put(cur.getInt(0), node);		// key -> id , value -> Point3D
			}
		}
		return nodes;
	}
	
	public ArrayList<Point3D> getNodesOfFloor(int floor){
		ArrayList<Point3D> nodes = new ArrayList<Point3D>();
		if(isDatabaseOk()){
			String sql = "SELECT * FROM " + TB_NODES + " WHERE "+COL_POSITIONZ+"=" + floor;
			Cursor cur = mDB.rawQuery(sql, null);
	
			while (cur.moveToNext()){
				Point3D node = new Point3D(
						cur.getFloat(1),		// x
						cur.getFloat(2),		// y
						cur.getFloat(3)		// z
						);
				node.setId(cur.getInt(0));	//id
				nodes.add(node);
			}
		}
		return nodes;
	}
	
	public ArrayList<Edge> getEdgesOfFloor(int floor){
		ArrayList<Edge> edges = new ArrayList<Edge>();
		if(isDatabaseOk()){
			String sql = "SELECT * FROM " + TB_EDGES + " as e " +
					"JOIN " + TB_NODES + " as n1 ON n1." + COL_ID + "=e." + COL_ID_START_NODE + 
					" JOIN " + TB_NODES + " as n2 ON n2." + COL_ID + "=e." + COL_ID_END_NODE +  
					" WHERE n1." + COL_POSITIONZ + "=" + floor + " AND n2." + COL_POSITIONZ+ "=" + floor;
			Cursor cur = mDB.rawQuery(sql, null);
	
			while (cur.moveToNext()){
				int edgeId = cur.getInt(0);
				int startNodeId = cur.getInt(1);
				int endNodeId = cur.getInt(2);
				float startNodeX = cur.getFloat(6);
				float startNodeY = cur.getFloat(7);
				float startNodeFloor = cur.getFloat(8);
				float endNodeX = cur.getFloat(10);
				float endNodeY = cur.getFloat(11);
				float endNodeFloor = cur.getFloat(12);
				float length = cur.getFloat(3);

				Edge edge = new Edge(
						edgeId,
						startNodeId,
						endNodeId,
						startNodeX,
						startNodeY,
						startNodeFloor,
						startNodeFloor,
						endNodeX,
						endNodeY,
						endNodeFloor,
						endNodeFloor,
						length
						);
				edges.add(edge);
			}
		}
		return edges;
	}

	/*
	public Hashtable<Integer,ArrayList<Edge>> getEdgesByFloor(){
		Hashtable <Integer,ArrayList<Edge>> edgesByFloor = new Hashtable <Integer,ArrayList<Edge>>();
		//SQLiteDatabase db = getReadableDatabase();
		if(isDatabaseOk()){
			//Obtain all the floors of the site
			String sqlFloors = "SELECT DISTINCT "+COL_POSITIONZ+" FROM " + TB_NODES;
			Cursor cur = mDB.rawQuery(sqlFloors, null);
			if(cur.getCount()>0){
				while (cur.moveToNext()){
					int floor = cur.getInt(0); // floor
					edgesByFloor.put(floor, new ArrayList<Edge>());
				}
				
				String sql = "SELECT * FROM " + TB_EDGES + " as e " +
						"JOIN " + TB_NODES + " as n1 ON n1." + COL_ID + "=e." + COL_ID_START_NODE + 
						" JOIN " + TB_NODES + " as n2 ON n2." + COL_ID + "=e." + COL_ID_END_NODE +  
						" order by n1."+ COL_POSITIONZ +" asc";

				Cursor cur2 = mDB.rawQuery(sql, null);
				
				while (cur2.moveToNext()){
					int startFloor = cur2.getInt(8); // startNodeZ
					int endFloor = cur2.getInt(12); // endNodeZ
					
					Edge edge = new Edge(
							cur2.getInt(0),		// edgeId
							cur2.getInt(1),		// startNodeId
							cur2.getInt(2),		// endNodeId
							cur2.getFloat(6),	// startNodeX
							cur2.getFloat(7),	// startNodeY
							cur2.getFloat(8),	// startNodeZ
							cur2.getFloat(10),	// endNodeX
							cur2.getFloat(11),	// endNodeY
							cur2.getFloat(12),	// endNodeZ
							cur2.getFloat(3)	// length
							);

					Point3D startNode = new Point3D(cur2.getFloat(6),cur2.getFloat(7),cur2.getFloat(8));
					Point3D endNode = new Point3D(cur2.getFloat(10),cur2.getFloat(11),cur2.getFloat(12));
					float angleLineDeg1 = angleLine(startNode, endNode);
					float angleLineDeg2 = angleLine(endNode, startNode);
					edge.setLineAngle1(angleLineDeg1);
					edge.setLineAngle2(angleLineDeg2);
					//Log.d(TAG,edge.toString());
					
					if(startFloor==endFloor){
						edgesByFloor.get(startFloor).add(edge);
					} else {
						edgesByFloor.get(startFloor).add(edge);
						edgesByFloor.get(endFloor).add(edge);
					}
				}
			}
		}
		//db.close();
		return edgesByFloor;
	}
	*/

	public Hashtable<Integer,ArrayList<Edge>> getEdgesByFloor(){
		Hashtable <Integer,ArrayList<Edge>> edgesByFloor = new Hashtable <Integer,ArrayList<Edge>>();
		if(isDatabaseOk()){
			//Obtain all the floors of the site
			ArrayList<FloorData> floorsData = getFloorsDataArray();
			int numFloors = floorsData.size();
			boolean isFirst = true;
			boolean isLast = false;
			for (int i=0; i<numFloors; i++){
				int currentFloor = floorsData.get(i).getNum();
				double currentFloorHeight = floorsData.get(i).getHeight();
				String whereCondition = "";
				boolean useIncrement = false;
				boolean useDecrement = false;
				double increment = 0;
				double decrement = 0;

				if(i==(numFloors-1)){
					isLast = true;
				}

				if(isFirst && isLast){
					// only one floor
					whereCondition = "";
				}
				else if(isFirst && !isLast){
					int nextFloor = floorsData.get(i+1).getNum();
					double maxFloor = (currentFloor + nextFloor)/2d;
					whereCondition = " WHERE n1." + COL_POSITIONZ + "=" + currentFloor + " OR n2." + COL_POSITIONZ + "=" + currentFloor;
					useIncrement = true;
					double nextFloorHeight = floorsData.get(i+1).getHeight();
					double diffNextFloors = nextFloor - currentFloor;
					double diffNextFloorsHeight = nextFloorHeight - currentFloorHeight;
					increment = diffNextFloorsHeight/diffNextFloors;
				}
				else if(!isFirst && !isLast){
					int previousFloor = floorsData.get(i-1).getNum();
					int nextFloor = floorsData.get(i+1).getNum();
					double minFloor = (currentFloor + previousFloor)/2d;
					double maxFloor = (currentFloor + nextFloor)/2d;
					whereCondition = " WHERE (n1." + COL_POSITIONZ + "=" + currentFloor + ") OR (n2." + COL_POSITIONZ + "=" + currentFloor + ")";
					useIncrement = true;
					double nextFloorHeight = floorsData.get(i+1).getHeight();
					double diffNextFloors = nextFloor - currentFloor;
					double diffNextFloorsHeight = nextFloorHeight - currentFloorHeight;
					increment = diffNextFloorsHeight/diffNextFloors;
					useDecrement = true;
					double previousFloorHeight = floorsData.get(i-1).getHeight();
					double diffPreviousFloors = previousFloor - currentFloor;
					double diffPreviousFloorsHeight = previousFloorHeight - currentFloorHeight;
					decrement = diffPreviousFloorsHeight/diffPreviousFloors;
				}
				else{
					// it is not first, but it is last
					int previousFloor = floorsData.get(i-1).getNum();
					double minFloor = (currentFloor + previousFloor)/2d;
					whereCondition = " WHERE n1." + COL_POSITIONZ + "=" + currentFloor + " OR n2." + COL_POSITIONZ + "=" + currentFloor;
					useDecrement = true;
					double previousFloorHeight = floorsData.get(i-1).getHeight();
					double diffPreviousFloors = previousFloor - currentFloor;
					double diffPreviousFloorsHeight = previousFloorHeight - currentFloorHeight;
					decrement = diffPreviousFloorsHeight/diffPreviousFloors;
				}
				//Log.d(TAG, "isFirst: " + isFirst + " , isLast: " + isLast + " , whereCondition: " + whereCondition);

				String sql = "SELECT * FROM " + TB_EDGES + " as e " +
						"JOIN " + TB_NODES + " as n1 ON n1." + COL_ID + "=e." + COL_ID_START_NODE +
						" JOIN " + TB_NODES + " as n2 ON n2." + COL_ID + "=e." + COL_ID_END_NODE +
						whereCondition +
						" order by n1."+ COL_POSITIONZ +" asc";

				Cursor cur2 = mDB.rawQuery(sql, null);

				ArrayList<Edge> edges = new ArrayList<>();
				while (cur2.moveToNext()){
					int edgeId = cur2.getInt(0);
					int startNodeId = cur2.getInt(1);
					int endNodeId = cur2.getInt(2);
					float startNodeX = cur2.getFloat(6);
					float startNodeY = cur2.getFloat(7);
					float startNodeFloor = cur2.getFloat(8);
					float endNodeX = cur2.getFloat(10);
					float endNodeY = cur2.getFloat(11);
					float endNodeFloor = cur2.getFloat(12);
					float length = cur2.getFloat(3);
					// calculate height of nodes
					float startNodeZ = (float) currentFloorHeight;
					if(startNodeFloor>currentFloor){
						if(useIncrement){
							startNodeZ = (float) (currentFloorHeight + (startNodeFloor - currentFloor)*increment);
						}
					}
					else{
						// startNodeFloor less than currentFloor
						if(useDecrement){
							startNodeZ = (float) (currentFloorHeight + (startNodeFloor - currentFloor)*decrement);
						}
					}
					float endNodeZ = (float) currentFloorHeight;
					if(endNodeFloor>currentFloor){
						if(useIncrement){
							endNodeZ = (float) (currentFloorHeight + (endNodeFloor - currentFloor)*increment);
						}
					}
					else{
						// endNodeFloor less than currentFloor
						if(useDecrement){
							endNodeZ = (float) (currentFloorHeight + (endNodeFloor - currentFloor)*decrement);
						}
					}

					Point3DWithFloor startNode = new Point3DWithFloor(startNodeX, startNodeY, startNodeZ, startNodeFloor);
					Point3DWithFloor endNode = new Point3DWithFloor(endNodeX, endNodeY, endNodeZ, endNodeFloor);

					if(startNodeFloor != endNodeFloor){
						// calculate length
						length = (float) Util.distanceBetweenPoints(startNode, endNode);
					}

					Edge edge = new Edge(edgeId, startNodeId, endNodeId, startNode, endNode, length);

					float angleLineDeg1 = angleLine(startNode, endNode);
					float angleLineDeg2 = angleLine(endNode, startNode);
					edge.setLineAngle1(angleLineDeg1);
					edge.setLineAngle2(angleLineDeg2);
					//Log.d(TAG,edge.toString());

					edges.add(edge);
				}
				edgesByFloor.put(currentFloor, edges);
				isFirst = false;
			}
		}
		return edgesByFloor;
	}
	
	public float getPxMeter(){
		float pxMeter = -1;
		if(idSite==1){
			//ASCAMM Lab
			pxMeter = 12.3f;
		}
		else if(idSite==3){
			//ASCAMM Supermarket
			pxMeter = 55.2f;
		}
		else if(idSite==5){
			//Hackney wear shop
			pxMeter = 49.0f;
		}
		else if(idSite==6){
			//Hackney shoes shop
			pxMeter = 56.0f;
		}
		else if(idSite==8){
			//BCN Languages S. Familia
			pxMeter = 79.0f;
		}
		else if(idSite==9){
			//ASCAMM Supermarket
			pxMeter = 55.2f;
		}
		else if(idSite==11){
			//BCN Languages Joanic
			pxMeter = 39.36f;
		}
		else if(idSite==12){
			//Mercat Rubí
			pxMeter = 11.86f;
		}
		else if(idSite==15){
			//Acuario Medellin
			pxMeter = 9.71f;
		}
		else{
			if(isDatabaseOk()){
				if(tableExists(TB_SITE_INFO)){
					String sql = "SELECT "+COL_SCALE+" FROM "+TB_SITE_INFO;
					Cursor cur = mDB.rawQuery(sql, null);
			
					if (cur.moveToFirst()){
						pxMeter = cur.getFloat(0);
					}
				}
			}
		}
		return pxMeter;
	}
	
	/** Returns the resolution of the DB in meters **/
	public float getResolution(){
		float res = -1;
		if(idSite==1){
			//ASCAMM Lab
			res = 0.5f;
		}
		else if(idSite==3){
			//ASCAMM Supermarket
			res = 0.5f;
		}
		else if(idSite==9){
			//ASCAMM Supermarket
			res = 0.5f;
		}
		else if(idSite==12){
			//Mercat Rubí
			res = 1;
		}
		else if(idSite==15){
			//Acuario Medellin
			res = 0.5f;
		}
		else{
			if(isDatabaseOk()){
				if(tableExists(TB_SITE_INFO)){
					String sql = "SELECT "+COL_RESOLUTION+" FROM "+TB_SITE_INFO;
					Cursor cur = mDB.rawQuery(sql, null);
			
					if (cur.moveToFirst()){
						res = cur.getFloat(0);
					}
				}
			}
		}
		return res;
	}

	/** Returns the map angle to north in degrees **/
	// LQ: this is bullshit
	public double getMapAngleToNorth(){
		double angle = 0;
		if(idSite==1){
			//ASCAMM lab
			angle = 18.7;
		}
		else if(idSite==23){
			//Casa Batllo
			angle = 360 - 316.3;
		}
		else if(idSite==26){
			//Renault Eurecat
			angle = 109.04;
		}
		else if(idSite==28){
			//Eurecat Proves (Raspberry)
			angle = 108.7;
		}
		else if(idSite==33){
			//Smart Retail
			angle = 18.7;
		}
		else if(idSite==35){
			//Illa Diagonal
			angle = 360 - 161;
		}
		else if(idSite==36) {
			//Eurecat Proves (Nou servidor)
			angle = 108.7;
		}
		/*
		else{
			if(isDatabaseOk()){
				if(tableExists(TB_SITE_INFO)){
					String sql = "SELECT "+COL_ANGLE_TO_NORTH+" FROM "+TB_SITE_INFO;
					Cursor cur = mDB.rawQuery(sql, null);

					if (cur.moveToFirst()){
						angle = cur.getFloat(0);
					}
				}
			}
		}
		*/
		return angle;
	}

	public HashMap<Integer,Double> getFloorsData(){
		// LQ: this is bullshit
		float pxMeter = getPxMeter();
		HashMap<Integer,Double> floors = new HashMap<Integer,Double>();
		if(idSite==28 || idSite == 36){
			floors.put(1, 0d*pxMeter);
			floors.put(2, 2.88d*pxMeter);
		}
		else if(idSite==35){
			floors.put(0, 0d*pxMeter);
			floors.put(1, 4d*pxMeter);
			floors.put(-1, -4d*pxMeter);
		}
		else{
			floors.put(0, 0d*pxMeter);
		}
		return floors;
	}

	public ArrayList<FloorData> getFloorsDataArray(){
		float pxMeter = getPxMeter();
		ArrayList<FloorData> floors = new ArrayList<>();
		if(idSite==28 || idSite == 36){
			floors.add(new FloorData(1, 0d*pxMeter));
			floors.add(new FloorData(2, 2.88d*pxMeter));
		}
		else if(idSite==35){
			floors.add(new FloorData(0, 0d*pxMeter));
			floors.add(new FloorData(1, 4d*pxMeter));
			floors.add(new FloorData(-1, -4d*pxMeter));
		}
		else{
			floors.add(new FloorData(0, 0d*pxMeter));
		}
		return floors;
	}

	/*
	public Matrix getConversionMatrix(){
		Matrix matrixLocalToGlobal = null;
		if(idSite==1){
			//ASCAMM Lab
			matrixLocalToGlobal = new Matrix(7.6235246259386e-7,2.5804207223574e-7,2.1253579179929,1.9299226745177e-7,-5.7017109294884e-7,41.489573593072,(double) 0,(double) 0,(double) 1);
		}
		else if(idSite==12){
			// Mercat Rubi
			matrixLocalToGlobal = new Matrix(1.0136901121309e-6,0,2.03434322,0,-7.7533195020349e-7,41.48972017,(double) 0,(double) 0,(double) 1);
		}
		/*
		else if(idSite==21){
			// CAR Sant Cugat
			matrixLocalToGlobal = new Matrix(-8.0710913606375e-7,-5.3608129668299e-8,2.075868909,-4.2375095185548e-8,6.3798768353696e-7,41.483306432,(double) 0,(double) 0,(double) 1);
		}
		*//*
		else{
			if(isDatabaseOk()){
				if(tableExists(TB_CONV_MATRIX)){
					String sql = "SELECT "+COL_M_11+","+COL_M_12+","+COL_M_13+","+COL_M_21+","+COL_M_22+","+COL_M_23+","+COL_M_31+","+COL_M_32+","+COL_M_33+" FROM "+TB_CONV_MATRIX;
					Cursor cur = mDB.rawQuery(sql, null);
					
					if (cur.moveToFirst()){
						matrixLocalToGlobal = new Matrix(cur.getDouble(0),cur.getDouble(1),cur.getDouble(2),cur.getDouble(3),cur.getDouble(4),cur.getDouble(5),cur.getDouble(6),cur.getDouble(7),cur.getDouble(8));
					}
				}
			}
		}
		return matrixLocalToGlobal;
	}
	*/

	public CoordConversion getConversionClass() {

		CoordConversion conv = null;

		if(idSite == 1) {
			conv = new CoordConversion(7.6235246259386e-7, 2.5804207223574e-7, 1.9299226745177e-7, -5.7017109294884e-7, 41.489573593072, 2.1253579179929);
		} else{
			if(isDatabaseOk()) {
				if(tableExists(TB_CONV_MATRIX)) {
					String sql = "SELECT "+COL_M_11+","+COL_M_12+","+COL_M_13+","+COL_M_21+","+COL_M_22+","+COL_M_23+","+COL_M_31+","+COL_M_32+","+COL_M_33+" FROM "+TB_CONV_MATRIX;
					Cursor cur = mDB.rawQuery(sql, null);
					if(cur.moveToFirst()) {
						conv = new CoordConversion(cur.getDouble(0), cur.getDouble(1), cur.getDouble(3), cur.getDouble(4), cur.getDouble(5), cur.getDouble(2));
					}
				}
			}
		}

		return conv;
	}

	public List<Integer> getFloorList() {

		List<Integer> result = new ArrayList<Integer>();

		if(isDatabaseOk()) {
			if(tableExists(TB_NODES)) {
				String sql = "SELECT " + COL_POSITIONZ + " FROM " + TB_NODES + " GROUP BY " +  COL_POSITIONZ;
				Cursor cur = mDB.rawQuery(sql, null);

				while (cur.moveToNext()) {
					result.add(cur.getInt(0));
				}
			}
		}

		return result;
	}
	
	private double adjustRssStdev(double rssStdev, int count, float countPercentage){
		if(countPercentage<MIN_COUNT_PERCENTAGE || count<MIN_COUNT){
			//double rssStdevOld = rssStdev;
			rssStdev = DEFAULT_RSS_STDEV;
			//Log.d(TAG,"rssStdev adjusted for count: (" + rssStdevOld + " , " + rssStdev + ") , (" + count + " , " + countPercentage + ")");
		}
		else if(rssStdev==0){
			//rssStdev = Math.pow(10,-16);
			rssStdev = 0.1;
			//Log.d(TAG,"rssStdev adjusted for being 0. new rssStdev: " + rssStdev);
		}
		else if(rssStdev > MAX_RSS_STDEV){
			rssStdev = MAX_RSS_STDEV;
			//Log.d(TAG,"rssStdev adjusted for exceeding maxValue. new rssStdev: " + rssStdev);
		}
		return rssStdev;
	}

	/** Calculate the angle of a line **/
	public float angleLine (Point3D start, Point3D end) {
		float angle = (float) Math.atan2( (end.y - start.y), (end.x - start.x) );
		float angleDeg = ((float)Math.toDegrees(angle)) % 360;
		angleDeg += 90; // to make 0 degrees point to relative north of the map
		if(angleDeg<0){
			angleDeg += 360;
		}
		return angleDeg;
	}

	public ArrayList<FloorData> getFloorsDataArrayComplete(){
		ArrayList<FloorData> floorsData = getFloorsDataArray();
		int numFloors = floorsData.size();
		boolean isFirst = true;
		boolean isLast = false;
		for (int i=0; i<numFloors; i++) {
			int currentFloor = floorsData.get(i).getNum();
			double currentFloorHeight = floorsData.get(i).getHeight();
			double minFloor = currentFloor;
			double maxFloor = currentFloor;
			boolean useIncrement = false;
			boolean useDecrement = false;
			double increment = 0;
			double decrement = 0;

			if (i == (numFloors - 1)) {
				isLast = true;
			}

			if (isFirst && isLast) {
				// only one floor
			} else if (isFirst && !isLast) {
				int nextFloor = floorsData.get(i + 1).getNum();
				maxFloor = (currentFloor + nextFloor) / 2d;
				useIncrement = true;
				double nextFloorHeight = floorsData.get(i + 1).getHeight();
				double diffNextFloors = nextFloor - currentFloor;
				double diffNextFloorsHeight = nextFloorHeight - currentFloorHeight;
				increment = diffNextFloorsHeight / diffNextFloors;
			} else if (!isFirst && !isLast) {
				int previousFloor = floorsData.get(i - 1).getNum();
				int nextFloor = floorsData.get(i + 1).getNum();
				minFloor = (currentFloor + previousFloor) / 2d;
				maxFloor = (currentFloor + nextFloor) / 2d;
				useIncrement = true;
				double nextFloorHeight = floorsData.get(i + 1).getHeight();
				double diffNextFloors = nextFloor - currentFloor;
				double diffNextFloorsHeight = nextFloorHeight - currentFloorHeight;
				increment = diffNextFloorsHeight / diffNextFloors;
				useDecrement = true;
				double previousFloorHeight = floorsData.get(i - 1).getHeight();
				double diffPreviousFloors = previousFloor - currentFloor;
				double diffPreviousFloorsHeight = previousFloorHeight - currentFloorHeight;
				decrement = diffPreviousFloorsHeight / diffPreviousFloors;
			} else {
				// it is not first, but it is last
				int previousFloor = floorsData.get(i - 1).getNum();
				minFloor = (currentFloor + previousFloor) / 2d;
				useDecrement = true;
				double previousFloorHeight = floorsData.get(i - 1).getHeight();
				double diffPreviousFloors = previousFloor - currentFloor;
				double diffPreviousFloorsHeight = previousFloorHeight - currentFloorHeight;
				decrement = diffPreviousFloorsHeight / diffPreviousFloors;
			}
			//Log.d(TAG, "isFirst: " + isFirst + " , isLast: " + isLast + " , useIncrement: " + useIncrement + " , useDecrement: " + useDecrement + " , min: " + minFloor + " , max: " + maxFloor + " , increment: " + increment + " , decrement: " + decrement);
			FloorData fd = floorsData.get(i);
			fd.setMin(minFloor);
			fd.setMax(maxFloor);
			fd.setUseIncrement(useIncrement);
			fd.setIncrement(increment);
			fd.setUseDecrement(useDecrement);
			fd.setDecrement(decrement);
			isFirst = false;
		}
		return floorsData;
	}

	public double getHeightOfPoint(double z, HashMap<Integer,Double> floorsMap, ArrayList<FloorData> floorsData){
		double height = -1000;
		// RP: TODO: fix in floorsMap
		if(floorsMap.containsKey((int) z)){
			height = floorsMap.get((int) z);
		}
		else{
			for(FloorData fd : floorsData){
				double minFloor = fd.getMin();
				double maxFloor = fd.getMax();
				if(z>=minFloor && z<=maxFloor){
					// z inside range
					int currentFloor= fd.getNum();
					double currentFloorHeight = fd.getHeight();
					height = currentFloorHeight;
					if(z>currentFloor){
						if(fd.isUseIncrement()){
							height = (float) (currentFloorHeight + (z - currentFloor)*fd.getIncrement());
						}
					}
					else if(z<currentFloor){
						if(fd.isUseDecrement()){
							height = (float) (currentFloorHeight + (z - currentFloor)*fd.getDecrement());
						}
					}
					break;
				}
			}
		}
		return height;
	}
}
