package com.ascamm.motion.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ascamm.motion.utils.Constants;

/**
 * Database helper class, defines the SQLite database, tables, columns, and all the methods provided publicly.
 * @author Dani Fernandez [CTAE]
 */
public class LocalDBHelper extends SQLiteOpenHelper {
	
	private String TAG = getClass().getSimpleName();
	private boolean debug = Constants.DEBUGMODE;
	
	/* define Database, tables, and columns */
	public static final String 	DB_NAME 		=	"AscammLocationDB";
	public static final int 	DB_VERSION		=	1;
	public static final String 	TB_PDB		 	= 	"pdb";
	public static final String 	COL_ID 			= 	"id" ; 
	public static final String 	COL_SITE_ID		= 	"site_id" ;
	public static final String 	COL_VERSION		= 	"version" ;
	public static final String 	COL_NAME		= 	"name" ;
	
	private static final String SQLITE_FILE_NAME = "ascamm_location.sqlite";
	
	public static final int STATUS_READY = 0;
	public static final int STATUS_PENDING = 1;
	
	private SQLiteDatabase db;
	
	public LocalDBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}
	
	public LocalDBHelper(Context context, boolean saveInSD) {
		super(context, Constants.APP_DATA_FOLDER + File.separator + DB_NAME + ".sqlite", null, DB_VERSION);
	}
	
	public LocalDBHelper(Context context, boolean saveInSD, String userId) {
		super(context, Constants.APP_DATA_FOLDER + File.separator + DB_NAME + "_" + userId + ".sqlite", null, DB_VERSION);
	}

	/**
	 * Create one SQLite database named AscammLocationDB. This database is divided in multiple tables: 
	 *  <ul>
	 *  <li>TB_POSITIONS: Contains information related to Positions of the user</li>
	 *  </ul>
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			
			db.execSQL("CREATE TABLE " + TB_PDB + " (" 
					+ COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
					+ COL_SITE_ID + " INTEGER, "
					+ COL_VERSION + " INTEGER, "
					+ COL_NAME + " TEXT "
					+ ");");

		} catch ( SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		/* 
		   Called when the database needs to be upgraded. The implementation 
		   should use this method to drop tables, add tables, or do anything 
		   else it needs to upgrade to the new schema version. 
		 */
	}
	
	public boolean isOpened(){
		boolean isOpened = false;
		if(db!=null && db.isOpen()){
			isOpened = true;
		}
		if(debug)	Log.d(TAG,"isOpened: " + isOpened);
		return isOpened;
	}
	
	public boolean open(){
		boolean success = false;
		try {
			db = getWritableDatabase();
			success = true;
			if(debug)	Log.d(TAG,"DB opened");
		} catch(SQLiteException e){
			Log.e(TAG,"Problems opening DB");
		}
		return success;
	}
	
	public void close(){
		if(debug)	Log.d(TAG,"closing DB");
		if(db!=null && db.isOpen()){
			db.close();
		}
	}
	
	/**
	 * Export the SQLite database to SD card as a ".sqlite" file
	 */
	public boolean exportDB(String targetDir){
		//SQLiteDatabase db = getReadableDatabase();
		String targetPath = targetDir + "/" + SQLITE_FILE_NAME;

		File targetFolder = new File(targetDir);
		if (!targetFolder.exists()) {
			targetFolder.mkdirs();
		}
		try{
			File sourceFile = new File(db.getPath());
			FileInputStream inStream = new FileInputStream(sourceFile);

			File targetFile = new File(targetPath);
			FileOutputStream outStream = new FileOutputStream(targetFile);

			int c;
			while( (c = inStream.read()) != -1 ){
				outStream.write(c);
			}
			inStream.close();
			outStream.close();

		} catch (Exception e) {
			Log.e("DBHelper", "Can not export database to file");
			return false;
		}

		//db.close();
		return true;
	}

	/**
	 * Import the ".sqlite" file to SQLite database
	 */
	public boolean importDB(String sourceDirPath){
		String sourceFilePath = sourceDirPath + "/" + SQLITE_FILE_NAME;
		//SQLiteDatabase db = getReadableDatabase();

		try{
			File sourceFile = new File(sourceFilePath);
			FileInputStream inStream = new FileInputStream(sourceFile);

			File targetFile = new File(db.getPath());
			FileOutputStream outStream = new FileOutputStream(targetFile);

			int c;
			while( (c = inStream.read()) != -1 ){
				outStream.write(c);
			}
			inStream.close();
			outStream.close();

		} catch (Exception e) {
			Log.e("DBHelper", "Can not import database file");
			return false;
		}

		//db.close();
		return true;
	}
	
	/**
	 *  Insert a new PDB to the database PDB table
	 * @param siteId - Integer
	 * @param version - Integer
	 * @param name - String
	 * @return newRecordID
	 */
	public long insertPDB(int siteId, int version, String name){
		long newRecordID = 0;
		if(isOpened()){
			//SQLiteDatabase db = getWritableDatabase();
			ContentValues cvs = new ContentValues();
			cvs.put(COL_SITE_ID, siteId);
			cvs.put(COL_VERSION, version);
			cvs.put(COL_NAME, name);
			newRecordID = db.insert(TB_PDB, null, cvs);
			//db.close();
		}
		return newRecordID;
	}
	
	/**
	 * Update a PDB from database PDB table
	 * @param siteId - Site ID to update
	 * @param newVersion - The new version
	 * @param newName - The new name
	 * @return updatedRow - number of rows updated
	 */
	public int updatePDB(int siteId, int newVersion, String newName){
		int updatedRow = 0;
		if(isOpened()){
			ContentValues cvs = new ContentValues();
			cvs.put(COL_VERSION, newVersion);
			cvs.put(COL_NAME, newName);
			updatedRow = db.update(TB_PDB, cvs, COL_SITE_ID+"="+siteId, null);
		}
		return updatedRow;
	}
	
	/**
	 * Get PDB version
	 * @param siteId - Site ID
	 * @return version - the PDB version of the site
	 */
	public int getVersion(int siteId){
		int version = -1;
		if(isOpened()){
			//SQLiteDatabase db = getReadableDatabase();
	
			String sql = "SELECT " + COL_VERSION + " FROM " + TB_PDB + " WHERE " + COL_SITE_ID + "=" + siteId;
			Cursor cur = db.rawQuery(sql, null);
			//Log.d(TAG,"sql: " + sql);
			
			if(cur.moveToFirst()){
				version = cur.getInt(0);
			}
			
			//db.close();
		}
		return version;
	}
	
	/**
	 * Get pdb_name from the table
	 * @param siteId - Site ID
	 * @return pdbName - the name of the PDB of the site
	 */
	public String getPdbName(int siteId){
		String pdbName = null;
		if(isOpened()){
			//SQLiteDatabase db = getReadableDatabase();
	
			String sql = "SELECT " + COL_NAME + " FROM " + TB_PDB + " WHERE " + COL_SITE_ID + "=" + siteId;
			Cursor cur = db.rawQuery(sql, null);
			//Log.d(TAG,"sql: " + sql);
			
			if(cur.moveToFirst()){
				pdbName = cur.getString(0);
			}
			
			//db.close();
		}
		return pdbName;
	}

}
