package com.ascamm.motion.database;

/**
 * Class used to store a scanning information for a position
 * @author Dani, ASCAMM
 */
public class CalibrationItem {
	private String mSSID;
	private String mBSSID;
	private float mRSS;
	private float mRSSstdev;
	private String mTech;
	private int mCount;
	private float mPercentage;
	private int mOrder;
	
	public CalibrationItem(String bssid, float rss) {
		mSSID = "";
		mBSSID = bssid;
		mRSS = rss;
		mCount = 0;
	}
	
	public CalibrationItem(String ssid, String bssid, float rss) {
		mSSID = ssid;
		mBSSID = bssid;
		mRSS = rss;
	}
	
	public CalibrationItem(String ssid, String bssid, float avgRss, int count){
		mSSID = ssid;
		mBSSID = bssid;
		mRSS = avgRss;
		mCount = count;
	}
	
	public CalibrationItem(String ssid, String bssid, float rss, float rssStdev, int count) {
		mSSID = ssid;
		mBSSID = bssid;
		mRSS = rss;
		mRSSstdev = rssStdev;
		mCount = count;
	}
	
	/*
	public CalibrationItem(String ssid, String bssid, float rss, float rssStdev, String tech, int count, float percentage) {
		mSSID = ssid;
		mBSSID = bssid;
		mRSS = rss;
		mRSSstdev = rssStdev;
		mTech = tech;
		mCount = count;
		mPercentage = percentage;
	}
	*/
	
	public CalibrationItem(String ssid, String bssid, float rss, float rssStdev, String tech, int count, float percentage, int order) {
		mSSID = ssid;
		mBSSID = bssid;
		mRSS = rss;
		mRSSstdev = rssStdev;
		mTech = tech;
		mCount = count;
		mPercentage = percentage;
		mOrder = order;
	}
	
	public String getSSID() {
		return mSSID;
	}
	public String getBSSID() {
		return mBSSID;
	}
	public float getRSS() {
		return mRSS;
	}
	public float getRSSstdev() {
		return mRSSstdev;
	}
	public String getTech(){
		return mTech;
	}
	public int getCount(){
		return mCount;
	}
	public float getPercentage(){
		return mPercentage;
	}
	public int getOrder(){
		return mOrder;
	}
	
	
	public void setSSID(String ssid){
		mSSID = ssid;
	}
	public void setBSSID(String bssid){
		mBSSID = bssid;
	}
	public void setRSS(float rss){
		mRSS = rss;
	}
	public void setRSSstdev(float rssStdev){
		mRSSstdev = rssStdev;
	}
	public void setTech(String tech){
		mTech = tech;
	}
	public void setCount(int count){
		mCount = count;
	}
	public void setPercentage(float percentage){
		mPercentage = percentage;
	}
	public void setOrder(int order){
		mOrder = order;
	}
}
