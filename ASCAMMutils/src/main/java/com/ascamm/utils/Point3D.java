package com.ascamm.utils;

import android.graphics.Point;

import java.io.Serializable;

/**
 * Class used to store a 3D position
 * @author Marc Ciurana, ASCAMM
 */
public class Point3D implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int id;
	public double x;
	public double y;
	public double z;	
	
	public Point3D(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Point3D(){
		this.x = this.y = this.z = 0.0;
	}
	
    public void set(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
	
	public Point getPoint2D(){
		return new Point((int)this.x, (int)this.y);
	}
	
	public String toString(){
		return "Point3D(" + this.x + ", " + this.y + ", " + this.z + ")";
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public double distanceTo(Point3D p){
		double dist = Math.sqrt(Math.pow(x-p.x,2) + Math.pow(y-p.y,2) + Math.pow(z-p.z,2));
		return dist;
	}

	// Next two methods added to use Point3D objects as Keys in Hashtable (Dani)
	public boolean equals(Object obj) {
	    if (!(obj instanceof Point3D)) {
	        return false;
	    } else {
	        Point3D that = (Point3D)obj;
	        return ((this.x==that.x) && (this.y==that.y) && (this.z==that.z));
	    }
	}

	public int hashCode() {
	    int hash = toString().hashCode();
	    return hash;
	}
}
