package com.ascamm.utils;

public class LatLng3D {

    public double lat;
    public double lng;
    public double z;

    public LatLng3D(double lat, double lng, double z) {
        this.lat = lat;
        this.lng = lng;
        this.z = z;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public double getZ() {
        return z;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public Point3D getPoint3DfromLatLng3D(){
        return new Point3D(lat, lng, z);
    }

}
