package com.ascamm.pois;

import android.content.Context;

import com.ascamm.pois.spatialite.GeoFence;
import com.ascamm.pois.spatialite.FenceInfoDB;
import com.ascamm.pois.spatialite.SpatialiteHelper;
import com.ascamm.pois.utils.FenceInfo;
import com.ascamm.utils.Point3D;

import java.util.ArrayList;
import java.util.Locale;

import android.util.Log;

/**
 * Created by dfernandez on 2/9/16.
 */
public class FenceCalculator {

    private float pxMeter = -1;
    private float angleDiff = -1;
    private ArrayList<GeoFence> geoFences;
    private SpatialiteHelper spHelper;

    private String decFor = "%.2f";

    private Context context;

    private int numPointsHystPos = 3;
    private int hystPosCounter = 0;
    private String lastPos = "A";
    private String currentPos = null;
    private boolean updatePos = false;

    public FenceCalculator(Context context){
        this.context = context;
    }

    public boolean initDBFences(String dbPath){
        if(dbPath==null) {
            spHelper = new SpatialiteHelper(context, null, true);
        }
        else{
            //System.out.println("dbPath: " + dbPath);
            spHelper = new SpatialiteHelper(context, dbPath, false);
        }
        boolean success = spHelper.isDbOk();
        return success;
    }

    public void setPxMeter(float pxMeter){
        this.pxMeter = pxMeter;
    }

    public void setAngleDiff(float angleDiff){
        this.angleDiff = angleDiff;
    }

    public void setCurrentPos(String pos){
        this.currentPos = pos;
    }

    public void getGeoFences(int siteId){
        geoFences = spHelper.getFences(siteId);
    }

    public ArrayList<GeoFence> copyGeoFences(){
        ArrayList<GeoFence> fencesArray = null;
        if(geoFences !=null){
            fencesArray = new ArrayList<GeoFence>();
            for(GeoFence fence : geoFences){
                fencesArray.add(fence);
            }
        }
        return fencesArray;
    }

    public boolean FenceExists(int siteId, int fenceId){
        return spHelper.fenceExists(siteId, fenceId);
    }

    public GeoFence getSelectedGeoFenceAndCentroid(int siteId, Point3D currentLocation){
        GeoFence fence = null;
        ArrayList<GeoFence> geoFences = spHelper.getFencesThatIntersectAndCentroid(siteId, currentLocation);
        if(geoFences.size()>0){
            fence = geoFences.get(0);
        }
        return fence;
    }

    public Point3D getCentroidOfSelectedFence(int fenceId, Point3D point){
        return spHelper.getCentroidOfSelectedFence(fenceId, point);
    }

    public GeoFence getReachedGeoFence(int siteId, Point3D currentLocation){
        GeoFence fence = null;
        ArrayList<GeoFence> geoFences = spHelper.getFencesThatIntersect(siteId, currentLocation);
        if(!geoFences.isEmpty()){
            fence = geoFences.get(0);
        }
        return fence;
    }

    public FenceInfo getFenceInfo(Point3D currentLocation, int selectedFenceId, int lastReachedFenceId){
        String distStr_px = "";
        String distStr_m = "";
        String angleStr_rel = "";
        String angleStr_abs = "";
        String pos = "A";
        String darknessStr = "";
        if(selectedFenceId == lastReachedFenceId){
            distStr_px = "0";
            distStr_m = "0";
            pos = spHelper.getPosAB(selectedFenceId, currentLocation);
            pos = applyPosHysteresis(pos);
            darknessStr = "0";
        }
        else{
            //calculate dist and angle
            FenceInfoDB infoDB = spHelper.getFenceInfoDB(selectedFenceId, currentLocation);
            if(infoDB == null){
                return null;
            }
            else{
                int floorRelative = infoDB.getFloorRelativeToUser();
                if(floorRelative==0){
                    double dist_px = infoDB.getDistanceToRepresentativePoint();
                    double dist_m = dist_px/pxMeter;
                    distStr_px = String.format(Locale.ENGLISH, decFor, dist_px);
                    distStr_m = String.format(Locale.ENGLISH, decFor, dist_m);
                    // calculate angle
                    // LQ: reprPoint is null
                    Point3D reprPoint = infoDB.getRepresentativePoint();
                    if (reprPoint == null) reprPoint = new Point3D();
                    float angle = angleToRelativeNorth(currentLocation, reprPoint);
                    float angleToAbsoluteNorth = (angle - angleDiff)%360;
                    angleToAbsoluteNorth *= -1; // Videoguide interprets the angles the other way around
                    if(angleToAbsoluteNorth<0){
                        angleToAbsoluteNorth += 360;
                    }
                    angleStr_rel = String.format(Locale.ENGLISH, decFor, angle);
                    angleStr_abs = String.format(Locale.ENGLISH, decFor, angleToAbsoluteNorth);
                    pos = spHelper.getPosAB(selectedFenceId, currentLocation);
                    pos = applyPosHysteresis(pos);
                    // calculate darkness
                    double distMin = infoDB.getDistMin();
                    double distMax = infoDB.getDistMax();
                    double distToGeom = infoDB.getDistanceToGeom();
                    double distToGeom_m = distToGeom/pxMeter;
                    if(distToGeom_m<=distMin){
                        darknessStr = "0";
                    }
                    else if(distToGeom_m>=distMax){
                        darknessStr = "1";
                    }
                    else{
                        double darkness = (distToGeom_m - distMin)/(distMax - distMin);
                        darknessStr = String.format(Locale.ENGLISH, decFor, darkness);
                    }
                }
                else if (floorRelative>0){
                    distStr_px = "UP";
                    distStr_m = "UP";
                    angleStr_rel = "UP";
                    angleStr_abs = "UP";
                    darknessStr = "1";
                }
                else{
                    distStr_px = "DOWN";
                    distStr_m = "DOWN";
                    angleStr_rel = "DOWN";
                    angleStr_abs = "DOWN";
                    darknessStr = "1";
                }
            }
        }
        return new FenceInfo(distStr_px, distStr_m, angleStr_rel, angleStr_abs, pos, darknessStr);
    }

    private String applyPosHysteresis(String pos){
        String resPos = currentPos;
        // Implementation of a hysteresis cycle when changing between A and B inside a Multiple Fence
        if(numPointsHystPos > 1){
            if(currentPos != null && !pos.equalsIgnoreCase(currentPos)){
                if(pos.equalsIgnoreCase(lastPos)){
                    hystPosCounter++;
                    if(hystPosCounter == (numPointsHystPos-1)){
                        hystPosCounter = 0;
                        updatePos = true;
                    }
                    else{
                        updatePos = false;
                    }
                }
                else{
                    lastPos = pos;
                    hystPosCounter = 0;
                    updatePos = false;
                }
            }
            else{
                lastPos = pos;
                hystPosCounter = 0;
                updatePos = true;
            }
        }
        else{
            updatePos = true;
        }

        if(updatePos){
            // update current pos
            currentPos = pos;
            resPos = pos;
        }
        //Log.d(TAG,"currentPos: " + currentPos + " , lastPos: " + lastPos + " , hystPosCounter: " + hystPosCounter + " , updatePos: " + updatePos);

        return resPos;
    }

    private float angleToRelativeNorth(Point3D start, Point3D end){
        Log.d("angleRelative", "start point " + start + " end point " + end);
        float angle = (float) Math.atan2( (end.x - start.x) , -(end.y - start.y) );
        //float angle = (float) Math.atan( (end.x - start.x) / -(end.y - start.y) );

        float angleDeg = ((float)Math.toDegrees(angle)) % 360;
        return angleDeg;
    }

}
