package com.ascamm.pois;


/**
 * This class contains FenceActions used tp communicate APP with SDK.
 */

public final class FenceActions {

	public static final String SERVICE_OK = "com.ascamm.pois.ServiceOk";
	public static final String SET_FENCE_DB = "com.ascamm.pois.SetFencesDB";
	public static final String SET_FENCE_DB_RESP = "com.ascamm.pois.SetFencesDBResp";
	public static final String SET_SITE_ID = "com.ascamm.pois.SetSiteId";
	public static final String SET_SITE_ID_RESP = "com.ascamm.pois.SetSiteIdResp";
	public static final String GET_FENCES = "com.ascamm.pois.GetFences";
	public static final String GET_FENCES_RESP = "com.ascamm.pois.GetFencesResp";
	public static final String CALC_CLICKED_FENCE = "com.ascamm.pois.CalcClickedFence";
	public static final String CALC_CLICKED_FENCE_RESP = "com.ascamm.pois.CalcClickedFenceResp";
	public static final String GET_CENTROID_OF_SELECTED_FENCE = "com.ascamm.pois.GetCentroidOfSelectedFence";
	public static final String GET_CENTROID_OF_SELECTED_FENCE_RESP = "com.ascamm.pois.GetCentroidOfSelectedFenceResp";
	public static final String FENCE_REACHED = "com.ascamm.pois.ReachedFence";
	public static final String SELECT_FENCE = "com.ascamm.pois.SelectFence";
	public static final String FENCE_INFO = "com.ascamm.pois.FenceInfo";
	public static final String DO_FENCE_CALCULATIONS = "com.ascamm.pois.DoFenceCalculations";
	public static final String SET_PX_METER = "com.ascamm.pois.SetPxMeter";
	public static final String SET_ANGLE_TO_NORTH = "com.ascamm.pois.SetAngleToNorth";
	public static final String ERROR = "com.ascamm.pois.Error";

}
