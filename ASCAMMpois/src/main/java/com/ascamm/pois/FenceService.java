package com.ascamm.pois;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.ascamm.pois.utils.Constants;
import com.ascamm.pois.utils.FenceInfo;
import com.ascamm.pois.utils.FenceInfoListener;
import com.ascamm.pois.utils.ReachedFenceListener;
import com.ascamm.utils.Point3D;

import java.util.ArrayList;

import com.ascamm.pois.spatialite.GeoFence;


public class FenceService extends Service {

	private String TAG = getClass().getSimpleName();
	
	private boolean debug = Constants.DEBUGMODE;
	
	private final IBinder mBinder = new ServiceBinder();
	
	private static boolean initialized;

	private Context UIcontext;

	/** Broadcast Receiver **/
	private FenceReceiver mFenceReceiver;

	private int lastReachedFenceId = -1;
	private int selectedFenceId = -1;
	private float pxMeter = -1;
	private float angleDiff = 360 - 316.3f;
	//private ArrayList<GeoFence> geoFences;

	private String decFor = "%.2f";

	private int siteId = 23;
	private FenceCalculator mFenceCalculator;

	private ArrayList<ReachedFenceListener> mReachedFenceListeners = new ArrayList<ReachedFenceListener>();
	private ArrayList<FenceInfoListener> mFenceInfoListeners = new ArrayList<FenceInfoListener>();

	@Override
	public void onCreate() {
		super.onCreate();
		//Toast.makeText(this, "FenceService onCreate", Toast.LENGTH_SHORT).show();

		IntentFilter filter = new IntentFilter();
		filter.addAction(FenceActions.SET_FENCE_DB);
		filter.addAction(FenceActions.SET_SITE_ID);
		filter.addAction(FenceActions.SET_PX_METER);
		filter.addAction(FenceActions.SET_ANGLE_TO_NORTH);
		filter.addAction(FenceActions.GET_FENCES);
		filter.addAction(FenceActions.CALC_CLICKED_FENCE);
		filter.addAction(FenceActions.GET_CENTROID_OF_SELECTED_FENCE);
		filter.addAction(FenceActions.SELECT_FENCE);
		filter.addAction(FenceActions.DO_FENCE_CALCULATIONS);
		mFenceReceiver = new FenceReceiver();
		registerReceiver(mFenceReceiver, filter);

		initialized = true;
						
		mFenceCalculator = new FenceCalculator(this);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startid){
		//Toast.makeText(this, "FenceService onStartCommand", Toast.LENGTH_SHORT).show();
		//return flags;
		Intent respIntent = new Intent();
		respIntent.setAction(FenceActions.SERVICE_OK);
		sendBroadcast(respIntent);
		return START_STICKY;
	}
	
	@Override
	public void onDestroy()
	{
		//Toast.makeText(this, "FenceService onDestroy", Toast.LENGTH_SHORT).show();
		unregisterReceiver(mFenceReceiver);
		super.onDestroy();
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		//Toast.makeText(this, "Location Service binded", Toast.LENGTH_SHORT).show();
		return mBinder;
	}
	
	@Override
	public boolean onUnbind(Intent intent) {
		initialized = false;
		//Toast.makeText(this, "Location Service unbinded", Toast.LENGTH_SHORT).show();
		return true;
	}

	public class ServiceBinder extends Binder {
		public FenceService getService() {
			return FenceService.this;
		}
	}
	
	/** Broadcast Receiver **/
	private class FenceReceiver extends BroadcastReceiver {
		private String TAG = getClass().getSimpleName();
		
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if(debug)	Log.d(TAG,"action: " + action);
			if(action.equalsIgnoreCase(FenceActions.SET_FENCE_DB)){
				String fencesDbPath = intent.getExtras().getString("fences_db_path");
				boolean success = setFencesDB(fencesDbPath);
				Intent respIntent = new Intent();
				respIntent.setAction(FenceActions.SET_FENCE_DB_RESP);
				respIntent.putExtra("success", success);
				sendBroadcast(respIntent);
			}
			else if(action.equalsIgnoreCase(FenceActions.SET_SITE_ID)){
				siteId = intent.getExtras().getInt("site_id");
				setSiteId(siteId);
				Intent respIntent = new Intent();
				respIntent.setAction(FenceActions.SET_SITE_ID_RESP);
				respIntent.putExtra("success", true);
				sendBroadcast(respIntent);
			}
			else if(action.equalsIgnoreCase(FenceActions.SET_PX_METER)) {
				float pxMeter = intent.getExtras().getFloat("px_meter");
				setPxMeter(pxMeter);
			}
			else if(action.equalsIgnoreCase(FenceActions.SET_ANGLE_TO_NORTH)) {
				float angle = intent.getExtras().getFloat("angle");
				setAngleToNorth(angle);
			}
			else if(action.equalsIgnoreCase(FenceActions.GET_FENCES)){
				ArrayList<GeoFence> geoFencesArray = getFences();
				Intent respIntent = new Intent();
				respIntent.setAction(FenceActions.GET_FENCES_RESP);
				respIntent.putExtra("fences", geoFencesArray);
				sendBroadcast(respIntent);
			}
			else if(action.equalsIgnoreCase(FenceActions.CALC_CLICKED_FENCE)){
				int siteId = intent.getExtras().getInt("site_id");
				Point3D point = (Point3D) intent.getSerializableExtra("point");
				GeoFence selectedFence = calcClickedFence(siteId, point);
				int selectedFenceId = -1;
				String selectedFenceName = "";
				double centroid_x = 0;
				double centroid_y = 0;
				double centroid_z = 0;
				if(selectedFence!=null){
					selectedFenceId = selectedFence.getId();
					selectedFenceName = selectedFence.getName();
					Point3D centroid = selectedFence.getCentroid();
					centroid_x = centroid.x;
					centroid_y = centroid.y;
					centroid_z = centroid.z;
				}
				Intent respIntent = new Intent();
				respIntent.setAction(FenceActions.CALC_CLICKED_FENCE_RESP);
				respIntent.putExtra("selected_fence_id", selectedFenceId);
				respIntent.putExtra("selected_fence_name", selectedFenceName);
				respIntent.putExtra("centroid_x", centroid_x);
				respIntent.putExtra("centroid_y", centroid_y);
				respIntent.putExtra("centroid_z", centroid_z);
				sendBroadcast(respIntent);
			}
			else if(action.equalsIgnoreCase(FenceActions.GET_CENTROID_OF_SELECTED_FENCE)){
				int fenceId = intent.getExtras().getInt("fence_id");
				String fenceName = intent.getExtras().getString("fence_name");
				Point3D point = (Point3D) intent.getSerializableExtra("point");
				Point3D centroid = getCentroidOfSelectedFence(fenceId, point);
				double centroid_x = 0;
				double centroid_y = 0;
				double centroid_z = 0;
				if(centroid!=null){
					centroid_x = centroid.x;
					centroid_y = centroid.y;
					centroid_z = centroid.z;
				}
				Intent respIntent = new Intent();
				respIntent.setAction(FenceActions.GET_CENTROID_OF_SELECTED_FENCE_RESP);
				respIntent.putExtra("fence_id", fenceId);
				respIntent.putExtra("fence_name", fenceName);
				respIntent.putExtra("centroid_x", centroid_x);
				respIntent.putExtra("centroid_y", centroid_y);
				respIntent.putExtra("centroid_z", centroid_z);
				sendBroadcast(respIntent);
			}
			else if(action.equalsIgnoreCase(FenceActions.SELECT_FENCE)) {
				int fenceId = intent.getExtras().getInt("fence");
				selectFence(fenceId);
			}
			else if(action.equalsIgnoreCase(FenceActions.DO_FENCE_CALCULATIONS)) {
				Point3D point = (Point3D) intent.getSerializableExtra("point");
				doFenceCalculations(point);
			}
		}
	};
	
	private ArrayList<GeoFence> copyGeoFences(){
		return mFenceCalculator.copyGeoFences();
	}

	private GeoFence getSelectedGeoFenceAndCentroid(int siteId, Point3D currentLocation){
		return mFenceCalculator.getSelectedGeoFenceAndCentroid(siteId, currentLocation);
	}

	private GeoFence getReachedGeoFence(int siteId, Point3D currentLocation){
		return mFenceCalculator.getReachedGeoFence(siteId, currentLocation);
	}

	private FenceInfo getFenceInfo(Point3D currentLocation, int selectedFenceId, int lastReachedFenceId){
		return mFenceCalculator.getFenceInfo(currentLocation, selectedFenceId, lastReachedFenceId);
	}

	private void sendErrorFenceNotFound(int fenceId){
		Intent intent = new Intent();
		intent.setAction(FenceActions.ERROR);
		intent.putExtra("type", "warning");
		intent.putExtra("code", "001");
		intent.putExtra("msg", "Fence " + fenceId + " not found");
		sendBroadcast(intent);
	}
	

	/**
	 * ------------------------------------------------------------- 
	 * Service API
	 * 
	 * ------------------------------------------------------------- 
	 */
	
	public void setUIcontext(Context context){
		UIcontext = context;
	}

	public boolean setFencesDB(String dbPath){
		return mFenceCalculator.initDBFences(dbPath);
	}

	public void setSiteId(int siteId){
		mFenceCalculator.getGeoFences(siteId);
	}

	public void doFenceCalculations(Point3D currentPosition) {

		//if (geoFences != null) {
			GeoFence reachedGeoFence = getReachedGeoFence(siteId, currentPosition);
			// LQ: DEBUG reached fence
			Log.d("LQ", "reached fence: "+reachedGeoFence);
			int reachedFenceId = -1;
			int reachedFenceLocalId = -1;
			if (reachedGeoFence != null) {
				reachedFenceId = reachedGeoFence.getId();
				reachedFenceLocalId = reachedGeoFence.getLocalId();
			}
			if (reachedFenceId != lastReachedFenceId) {
				// If APP has bound to service
				fireReachedFenceEvent(reachedFenceId, reachedFenceLocalId);

				// If APP has not bound to service
				Intent respIntentReachedFence = new Intent();
				respIntentReachedFence.setAction(FenceActions.FENCE_REACHED);
				respIntentReachedFence.putExtra("fence", reachedFenceId);
				respIntentReachedFence.putExtra("local_fence", reachedFenceLocalId);
				sendBroadcast(respIntentReachedFence);
				lastReachedFenceId = reachedFenceId;
				setCurrentPos(null);
			}
			if (selectedFenceId != -1) {
				FenceInfo fenceInfo = getFenceInfo(currentPosition, selectedFenceId, lastReachedFenceId);
				if(fenceInfo==null){
					sendErrorFenceNotFound(selectedFenceId);
				}
				else {
					// If APP has bound to service
					fireFenceInfoEvent(fenceInfo);

					// If APP has not bound to service
					Intent respIntentFenceInfo = new Intent();
					respIntentFenceInfo.setAction(FenceActions.FENCE_INFO);
					respIntentFenceInfo.putExtra("distance", fenceInfo.getDistance_px() + " (" + fenceInfo.getDistance_m() + "m)");
					respIntentFenceInfo.putExtra("angle", fenceInfo.getAngle_to_relative_north() + " (" + fenceInfo.getAngle_to_absolute_north() + ")");
					respIntentFenceInfo.putExtra("pos", fenceInfo.getPos());
					sendBroadcast(respIntentFenceInfo);
				}
			}
		//}
	}
	
	public void selectFence(int fenceId){
		selectedFenceId = fenceId;
	}
	
	public ArrayList<GeoFence> getFences(){
		ArrayList<GeoFence> geoFences = copyGeoFences();
		return geoFences;
	}

	public GeoFence calcClickedFence(int siteId, Point3D currentLocation){
		return getSelectedGeoFenceAndCentroid(siteId, currentLocation);
	}

	public Point3D getCentroidOfSelectedFence(int fenceId, Point3D point){
		return mFenceCalculator.getCentroidOfSelectedFence(fenceId, point);
	}

	public void setPxMeter(float pxMeter){
		mFenceCalculator.setPxMeter(pxMeter);
	}

	public void setAngleToNorth(float angle){
		mFenceCalculator.setAngleDiff(angle);
	}

	public void setCurrentPos(String pos){
		mFenceCalculator.setCurrentPos(pos);
	}

	/* event listeners corresponding to Fences*/
	public synchronized void addReachedFenceListener(ReachedFenceListener listener) {
		mReachedFenceListeners.add(listener);
	}

	public synchronized void removeReachedFenceListener(ReachedFenceListener listener) {
		mReachedFenceListeners.remove(listener);
	}

	private synchronized void fireReachedFenceEvent(int fenceId, int fenceLocalId){
		for(ReachedFenceListener listener : mReachedFenceListeners){
			listener.newReachedFence(fenceId, fenceLocalId);
		}
	}

	public synchronized void addFenceInfoListener(FenceInfoListener listener) {
		mFenceInfoListeners.add(listener);
	}

	public synchronized void removeFenceInfoListener(FenceInfoListener listener) {
		mFenceInfoListeners.remove(listener);
	}

	private synchronized void fireFenceInfoEvent(FenceInfo fenceInfo){
		for(FenceInfoListener listener : mFenceInfoListeners){
			listener.newFenceInfo(fenceInfo);
		}
	}

}
