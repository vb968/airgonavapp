package com.ascamm.pois.utils;

import android.os.Environment;

import java.io.File;

/**
 * This class contains constants used by the program.
 */

public final class Constants {
	
	public static final boolean DEBUGMODE = true;

	public static final String APP_DATA_FOLDER_NAME = "ASCAMM_Location";
	
	public static final File APP_DATA_FOLDER = new File (Environment.getExternalStorageDirectory() 
			+ "/" + APP_DATA_FOLDER_NAME);

}
