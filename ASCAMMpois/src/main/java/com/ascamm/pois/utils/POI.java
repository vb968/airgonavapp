package com.ascamm.pois.utils;

import java.io.Serializable;

import com.ascamm.utils.Point3D;

/**
 * Created by Dani Fernandez on 06/05/16.
 */

public class POI implements Serializable{

    private static final long serialVersionUID = 1L;

    private int id;
    private String name;
    private Point3D point;
    private float radius;
    private boolean isMultiple;
    private Point3D pointA;
    private Point3D pointB;
    private String type;
    private boolean mobile;
    private boolean found = false;
    private boolean available = false;

    public POI(int id, Point3D point, float radius){
        this.id = id;
        this.point = point;
        this.radius = radius;
        isMultiple = false;
        name = "POI " + id;
    }

    public POI(int id, Point3D point, float radius, boolean isMultiple, Point3D pointA, Point3D pointB){
        this.id = id;
        this.point = point;
        this.radius = radius;
        this.isMultiple = isMultiple;
        this.pointA = pointA;
        this.pointB = pointB;
        name = "POI " + id;
    }

    public POI(int id, Point3D point, String name, String type, boolean mobile) {
        this.id = id;
        this.point = point;
        this.name = name;
        this.type = type;
        this.radius = -1;
        this.isMultiple = false;
        this.mobile = mobile;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isAvailable() {
        return this.available;
    }

    public boolean isMobile() {
        return mobile;
    }

    public void setMobile(boolean mobile) {
        this.mobile = mobile;
    }

    public boolean getFound() {
        return found;
    }

    public void setFound(boolean found) {
        this.found = found;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Point3D getPoint() {
        return point;
    }

    public void setPoint(Point3D point) {
        this.point = point;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public boolean isMultiple() {
        return isMultiple;
    }

    public void setMultiple(boolean isMultiple) {
        this.isMultiple = isMultiple;
    }

    public Point3D getPointA() {
        return pointA;
    }

    public void setPointA(Point3D pointA) {
        this.pointA = pointA;
    }

    public Point3D getPointB() {
        return pointB;
    }

    public void setPointB(Point3D pointB) {
        this.pointB = pointB;
    }

}