package com.ascamm.pois.utils;

import com.ascamm.utils.Point3D;

import java.io.Serializable;

public class Fence implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private Point3D point;
	private float radius;
	private boolean isMultiple;
	private Point3D pointA;
	private Point3D pointB;
	
	public Fence(int id, Point3D point, float radius){
		this.id = id;
		this.point = point;
		this.radius = radius;
		isMultiple = false;
		name = "Fence " + id;
	}
	
	public Fence(int id, Point3D point, float radius, boolean isMultiple, Point3D pointA, Point3D pointB){
		this.id = id;
		this.point = point;
		this.radius = radius;
		this.isMultiple = isMultiple;
		this.pointA = pointA;
		this.pointB = pointB;
		name = "Fence " + id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Point3D getPoint() {
		return point;
	}

	public void setPoint(Point3D point) {
		this.point = point;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public boolean isMultiple() {
		return isMultiple;
	}

	public void setMultiple(boolean isMultiple) {
		this.isMultiple = isMultiple;
	}

	public Point3D getPointA() {
		return pointA;
	}

	public void setPointA(Point3D pointA) {
		this.pointA = pointA;
	}

	public Point3D getPointB() {
		return pointB;
	}

	public void setPointB(Point3D pointB) {
		this.pointB = pointB;
	}
	
}
