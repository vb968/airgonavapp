package com.ascamm.pois.utils;



public interface ReachedFenceListener {
	public void newReachedFence(int poiId, int poiLocalId);
}
