package com.ascamm.pois.utils;

public class FenceInfo {

	private String distance_px;
	private String distance_m;
	private String angle_to_relative_north;
	private String angle_to_absolute_north;
	private String pos;
	private String darkness;
	
	public FenceInfo(String distance_px, String distance_m, String angle_to_relative_north, String angle_to_absolute_north, String pos, String darkness){
		this.distance_px = distance_px;
		this.distance_m = distance_m;
		this.angle_to_relative_north = angle_to_relative_north;
		this.angle_to_absolute_north = angle_to_absolute_north;
		this.pos = pos;
		this.darkness = darkness;
	}

	public String getDistance_px() {
		return distance_px;
	}

	public void setDistance_px(String distance_px) {
		this.distance_px = distance_px;
	}

	public String getDistance_m() {
		return distance_m;
	}

	public void setDistance_m(String distance_m) {
		this.distance_m = distance_m;
	}

	public String getAngle_to_relative_north() {
		return angle_to_relative_north;
	}

	public void setAngle_to_relative_north(String angle_to_relative_north) {
		this.angle_to_relative_north = angle_to_relative_north;
	}

	public String getAngle_to_absolute_north() {
		return angle_to_absolute_north;
	}

	public void setAngle_to_absolute_north(String angle_to_absolute_north) {
		this.angle_to_absolute_north = angle_to_absolute_north;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getDarkness() {
		return darkness;
	}

	public void setDarkness(String darkness) {
		this.darkness = darkness;
	}
}
