package com.ascamm.pois.spatialite;

import com.ascamm.utils.Point3D;

public class FenceInfoDB {

	private double distanceToGeom;
	private Point3D closestPoint;
	private double distanceToRepresentativePoint;
	private Point3D representativePoint;
	private double distMin;
	private double distMax;
	private int floorRelativeToUser;

	public FenceInfoDB(double distanceToGeom, Point3D closestPoint, double distanceToRepresentativePoint, Point3D representativePoint, double distMin, double distMax, int floorRelativeToUser) {
		this.distanceToGeom = distanceToGeom;
		this.closestPoint = closestPoint;
		this.distanceToRepresentativePoint = distanceToRepresentativePoint;
		this.representativePoint = representativePoint;
		this.distMin = distMin;
		this.distMax = distMax;
		this.floorRelativeToUser = floorRelativeToUser;
	}

	public double getDistanceToGeom() {
		return distanceToGeom;
	}

	public void setDistanceToGeom(double distance) {
		this.distanceToGeom = distance;
	}

	public Point3D getClosestPoint() {
		return closestPoint;
	}

	public void setClosestPoint(Point3D closestPoint) {
		this.closestPoint = closestPoint;
	}

	public double getDistanceToRepresentativePoint() {
		return distanceToRepresentativePoint;
	}

	public void setDistanceToRepresentativePoint(double distance) {
		this.distanceToRepresentativePoint = distance;
	}

	public Point3D getRepresentativePoint() {
		return representativePoint;
	}

	public void setRepresentativePoint(Point3D representativePoint) {
		this.representativePoint = representativePoint;
	}

	public int getFloorRelativeToUser() {
		return floorRelativeToUser;
	}

	public void setFloorRelativeToUser(int floorRelativeToUser) {
		this.floorRelativeToUser = floorRelativeToUser;
	}

	public double getDistMin() {
		return distMin;
	}

	public void setDistMin(double distMin) {
		this.distMin = distMin;
	}

	public double getDistMax() {
		return distMax;
	}

	public void setDistMax(double distMax) {
		this.distMax = distMax;
	}

	public String toString(){
		return "FenceInfo: (" + distanceToGeom + "," + closestPoint + "," + distanceToRepresentativePoint + "," + representativePoint + "," + floorRelativeToUser + ")";
	}

}
