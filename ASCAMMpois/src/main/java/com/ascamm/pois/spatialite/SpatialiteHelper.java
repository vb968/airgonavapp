package com.ascamm.pois.spatialite;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.ascamm.utils.Point3D;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import jsqlite.Constants;
import jsqlite.Database;
import jsqlite.Stmt;

public class SpatialiteHelper {

	private String TAG = getClass().getSimpleName();

	private Context mContext;
	private Database db;
	private boolean dbOk = false;
	private String dbPath;

	private static boolean debug = false;
	private static int SRID = 23031;

	private static String DB_FOLDER_PATH = com.ascamm.pois.utils.Constants.APP_DATA_FOLDER.toString();
	private static String APP_DATA_FOLDER_NAME = "SpatialiteTest";

	public static final String TB_POIS					= "pois";
	public static final String TB_POIS_MULT    			= "pois_multiple";
	public static final String TB_POIS_GEOM    			= "pois_geom";
	public static final String COL_ID					= "id" ;
	public static final String COL_NAME					= "name" ;
	public static final String COL_IS_MULT				= "is_multiple" ;
	public static final String COL_SITE_ID				= "site_id" ;
	public static final String COL_LOCAL_ID				= "local_id" ;
	public static final String COL_REPRESENTATIVE_POINT	= "representative_point" ;
	public static final String COL_POI_ID				= "poi_id" ;
	public static final String COL_POINT_A				= "point_a" ;
	public static final String COL_POINT_B				= "point_b" ;
	public static final String COL_FLOOR				= "floor" ;
	public static final String COL_GEOM					= "geom" ;
	public static final String COL_DIST_MIN				= "dist_min" ;
	public static final String COL_DIST_MAX				= "dist_max" ;

	private void error( Exception e ) {
		Log.e(TAG, e.getLocalizedMessage());
	}

	/* if fromAssets is true, dbFile is the name of the file in assets folder.
	 * if fromAssets is false, dbFile is the path to the database that has be used
	 */
	public SpatialiteHelper(Context context, String dbFile, boolean fromAssets) {
		mContext = context;
		if(dbFile == null){
            // LQ: dbFile name differs from LocationIndoor
			dbFile = "pois_v2.sqlite";
		}
		if(fromAssets){
			dbPath = DB_FOLDER_PATH + File.separator + dbFile;
			boolean dbexist = checkDatabase(dbPath);
			if (dbexist) {
				//System.out.println("Database exists");
				try{
					openDatabase(dbPath);
					dbOk = true;
				}catch(Exception e){
					Log.e(TAG,"Error opening spDB");
					e.printStackTrace();
				}
			} else {
				String msgTxt = "Database doesn't exist. Copy and open it";
				Log.e(TAG,msgTxt);
				try{
					copyDatabaseFromAssetsToSDcard(dbFile);
					openDatabase(dbPath);
					dbOk = true;
				}catch(IOException e){
					Log.e(TAG,"Error copying Database from Assets to SD card");
					e.printStackTrace();
				}catch(Exception e){
					Log.e(TAG,"Error opening spDB");
					e.printStackTrace();
				}
				//Toast.makeText(context, msgTxt, Toast.LENGTH_SHORT).show();
			}
		}
		else{
			boolean dbexist = checkDatabase(dbFile);
			if (dbexist) {
				//System.out.println("Database exists");
				try{
					openDatabase(dbFile);
					dbOk = true;
				}catch(Exception e){
					Log.e(TAG,"Error opening spDB");
					e.printStackTrace();
				}
			} else {
				String msgTxt = "Database doesn't exist";
				Log.e(TAG,msgTxt);
				//Toast.makeText(context, msgTxt, Toast.LENGTH_SHORT).show();
			}
		}
	}

	//Copy the database from assets
	private void copyDatabaseFromAssetsToSDcard(String dbFile) throws IOException
	{
		InputStream mInput = mContext.getAssets().open("databases/" + dbFile);
		String outFileName = DB_FOLDER_PATH + "/" + dbFile;
		OutputStream mOutput = new FileOutputStream(outFileName);
		byte[] mBuffer = new byte[1024];
		int mLength;
		while ((mLength = mInput.read(mBuffer))>0)
		{
			mOutput.write(mBuffer, 0, mLength);
		}
		mOutput.flush();
		mOutput.close();
		mInput.close();
	}

	private boolean checkDatabase(String dbPath) {
		boolean checkdb = false;
		try {
			File dbfile = new File(dbPath);
			checkdb = dbfile.exists();
		} catch(SQLiteException e) {
			//Log.e(TAG,"Database doesn't exist");
		}
		return checkdb;
	}

	public void openDatabase(String dbPath) throws Exception {
		//Open the database
		try {
			File spatialDbFile = new File(dbPath);

			if (!spatialDbFile.getParentFile().exists()) {
				throw new RuntimeException();
			}
			db = new Database();
			db.open(spatialDbFile.getAbsolutePath(), Constants.SQLITE_OPEN_READWRITE | Constants.SQLITE_OPEN_CREATE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void closeDatabase() throws SQLException {
		if(dbOk){
			//Close the database
			Log.d(TAG,"closing spDB");
			try {
				db.close();
			} catch (Exception e) {
				Log.e(TAG,"Error closing spDB");
				e.printStackTrace();
			}
		}
	}

	public boolean isDbOk(){
		return dbOk;
	}

	public void queryVersions() throws Exception {

		Stmt stmt01 = db.prepare("SELECT spatialite_version();");
		if (stmt01.step()) {
			Log.i(TAG, "SPATIALITE_VERSION: " + stmt01.column_string(0));
		}

		stmt01 = db.prepare("SELECT proj4_version();");
		if (stmt01.step()) {
			Log.i(TAG, "PROJ4_VERSION: " + stmt01.column_string(0));
		}

		stmt01 = db.prepare("SELECT geos_version();");
		if (stmt01.step()) {
			Log.i(TAG, "GEOS_VERSION: " + stmt01.column_string(0));
		}
		stmt01.close();

	}

	public ArrayList<GeoFence> getFences(int siteId) {
		ArrayList<GeoFence> geoFences = new ArrayList<GeoFence>();
		ArrayList<Geometry> tmpGeometries = new ArrayList<Geometry>();;
		int lastFenceId = -1, currentFenceId;
		GeoFence lastFence = null;
		boolean isFirstRow = true;

		String query = "SELECT p."+COL_ID+",p."+COL_LOCAL_ID+",p."+COL_NAME+",p."+COL_IS_MULT+",pg."+COL_FLOOR+",AsGeoJSON(pg."+COL_GEOM+"),AsGeoJSON(pm."+COL_POINT_A+"),AsGeoJSON(pm."+COL_POINT_B+"),AsGeoJSON(p."+COL_REPRESENTATIVE_POINT+")"+
				" FROM "+TB_POIS_GEOM+" as pg" +
				" JOIN "+TB_POIS+" as p ON pg."+COL_POI_ID+"=p."+COL_ID+
				" LEFT JOIN "+TB_POIS_MULT+" as pm ON pm."+COL_POI_ID+"=p."+COL_ID+
				" WHERE p."+COL_SITE_ID+"="+siteId+";";

		if(debug)       Log.d(TAG, "query: " + query);
		if(dbOk) {
			try {
				Stmt stmt = db.prepare(query);
				while (stmt.step()) {
					int id = stmt.column_int(0);
					int localId = stmt.column_int(1);
					String name = stmt.column_string(2);
					int is_mult = stmt.column_int(3);
					int floor = stmt.column_int(4);
					String geom = stmt.column_string(5);
					String point_a = stmt.column_string(6);
					String point_b = stmt.column_string(7);
					String representativePointStr = stmt.column_string(8);
					Geometry fenceGeom = null;
					if (geom != null) {
						JSONObject jsonGeom;
						try {
							jsonGeom = new JSONObject(geom);
							String type = jsonGeom.getString("type");
							if (debug) Log.d(TAG, "type: " + type);
							JSONArray coordinates = jsonGeom.getJSONArray("coordinates");
							JSONArray polygon = coordinates.getJSONArray(0);
							JSONArray polygon_coordinates = polygon.getJSONArray(0);
							ArrayList<Point3D> points = new ArrayList<Point3D>();
							for (int i = 0; i < polygon_coordinates.length(); i++) {
								JSONArray p_coor = polygon_coordinates.getJSONArray(i);
								Object xObj = p_coor.get(0);
								double x = Double.parseDouble(xObj.toString());
								Object yObj = p_coor.get(1);
								double y = Double.parseDouble(yObj.toString());
								if (debug) Log.d(TAG, "(" + x + " , " + y + ")");
								Point3D point = new Point3D(x, y, floor);
								points.add(point);
							}
							fenceGeom = new Geometry(id, floor, points);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					Point3D pointA = null;
					Point3D pointB = null;
					boolean isMultiple = false;
					if (is_mult == 1) {
						isMultiple = true;
						if (point_a != null) {
							JSONObject jsonGeom;
							try {
								jsonGeom = new JSONObject(point_a);
								String type = jsonGeom.getString("type");
								if (debug) Log.d(TAG, "type: " + type);
								JSONArray coordinates = jsonGeom.getJSONArray("coordinates");
								Object xObj = coordinates.get(0);
								double x = Double.parseDouble(xObj.toString());
								Object yObj = coordinates.get(1);
								double y = Double.parseDouble(yObj.toString());
								Object zObj = coordinates.get(2);
								double z = Double.parseDouble(zObj.toString());
								if (debug) Log.d(TAG, "(" + x + " , " + y + " , " + z + ")");
								pointA = new Point3D(x, y, z);
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
						if (point_b != null) {
							JSONObject jsonGeom;
							try {
								jsonGeom = new JSONObject(point_b);
								String type = jsonGeom.getString("type");
								if (debug) Log.d(TAG, "type: " + type);
								JSONArray coordinates = jsonGeom.getJSONArray("coordinates");
								Object xObj = coordinates.get(0);
								double x = Double.parseDouble(xObj.toString());
								Object yObj = coordinates.get(1);
								double y = Double.parseDouble(yObj.toString());
								Object zObj = coordinates.get(2);
								double z = Double.parseDouble(zObj.toString());
								if (debug) Log.d(TAG, "(" + x + " , " + y + " , " + z + ")");
								pointB = new Point3D(x, y, z);
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
					Point3D representativePoint = null;
					if (representativePointStr != null) {
						JSONObject jsonRP;
						try {
							jsonRP = new JSONObject(representativePointStr);
							String type = jsonRP.getString("type");
							if (debug) Log.d(TAG, "type: " + type);
							JSONArray coordinates = jsonRP.getJSONArray("coordinates");
							Object xObj = coordinates.get(0);
							double x = Double.parseDouble(xObj.toString());
							Object yObj = coordinates.get(1);
							double y = Double.parseDouble(yObj.toString());
							Object zObj = coordinates.get(2);
							double z = Double.parseDouble(zObj.toString());
							if (debug) Log.d(TAG, "(" + x + " , " + y + " , " + z + ")");
							representativePoint = new Point3D(x, y, z);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					currentFenceId = id;
					if (isFirstRow) {
						lastFenceId = id;
						lastFence = new GeoFence(id, localId, name, null, isMultiple, pointA, pointB, representativePoint);
						tmpGeometries.add(fenceGeom);
						isFirstRow = false;
					} else {
						if (lastFenceId == currentFenceId) {
							tmpGeometries.add(fenceGeom);
						} else {
							//System.out.println("point: (" + lastPosX + " , " + lastPosY + " , " + lastPosZ + ") , calibs: " + tmpListofCalibrations.size());
							lastFence.setGeometries(tmpGeometries);
							geoFences.add(lastFence);
							tmpGeometries = new ArrayList<Geometry>();
							tmpGeometries.add(fenceGeom);// update the value of the current Fence
							lastFenceId = currentFenceId;
							lastFence = new GeoFence(id, localId, name, null, isMultiple, pointA, pointB, representativePoint);
						}
					}
				}
				if (!isFirstRow) {
					lastFence.setGeometries(tmpGeometries);
					geoFences.add(lastFence);
				}
				stmt.close();
			} catch (Exception e) {
				error(e);
			}
		}
		if(debug){
			for (GeoFence fence : geoFences){
				Log.d(TAG, fence.toString());
			}
		}

		return geoFences;
	}

	public boolean fenceExists(int siteId, int fenceId) {
		boolean exists = false;
		String query = "SELECT "+COL_ID+
				" FROM "+TB_POIS+
				" WHERE "+COL_SITE_ID+"="+siteId+" AND "+COL_ID+"="+fenceId;

		if(debug)       Log.d(TAG, "query: " + query);
		if(dbOk) {
			try {
				Stmt stmt = db.prepare(query);
				if (stmt.step()) {
					exists = true;
				}
				stmt.close();
			} catch (Exception e) {
				error(e);
			}
		}
		return exists;
	}

	public ArrayList<GeoFence> getFencesThatIntersect(int siteId, Point3D point){
		ArrayList<GeoFence> geoFences = new ArrayList<GeoFence>();

		String query = "SELECT p."+COL_ID+",p."+COL_LOCAL_ID+",p."+COL_NAME+
				" FROM "+TB_POIS_GEOM+" as pg" +
				" JOIN "+TB_POIS+" as p ON pg."+COL_POI_ID+"=p."+COL_ID+
				" WHERE p."+COL_SITE_ID+"="+siteId+" AND pg."+COL_FLOOR+"="+(int)point.z+" AND ST_Intersects("+COL_GEOM+",MakePoint("+point.x+","+point.y+","+SRID+"));";

		if(debug)       Log.d(TAG, "query: " + query);
		if(dbOk) {
			try {
				Stmt stmt = db.prepare(query);
				while (stmt.step()) {
					int id = stmt.column_int(0);
					int localId = stmt.column_int(1);
					String name = stmt.column_string(2);
					GeoFence fence = new GeoFence(id, localId, name);
					geoFences.add(fence);
				}
				stmt.close();
			} catch (Exception e) {
				error(e);
			}
		}
		if(debug){
			for (GeoFence fence : geoFences){
				Log.d(TAG, fence.toString());
			}
		}
		return geoFences;
	}

	public ArrayList<GeoFence> getFencesThatIntersectAndCentroid(int siteId, Point3D point){
		ArrayList<GeoFence> geoFences = new ArrayList<GeoFence>();

		String query = "SELECT p."+COL_ID+",p."+COL_LOCAL_ID+",p."+COL_NAME+",AsGeoJSON(Centroid(pg."+COL_GEOM+"))"+
				" FROM "+TB_POIS_GEOM+" as pg" +
				" JOIN "+TB_POIS+" as p ON pg."+COL_POI_ID+"=p."+COL_ID+
				" WHERE p."+COL_SITE_ID+"="+siteId+" AND pg."+COL_FLOOR+"="+(int)point.z+" AND ST_Intersects("+COL_GEOM+",MakePoint("+point.x+","+point.y+","+SRID+"));";

		if(debug)       Log.d(TAG, "query: " + query);
		if(dbOk) {
			try {
				Stmt stmt = db.prepare(query);
				while (stmt.step()) {
					int id = stmt.column_int(0);
					int localId = stmt.column_int(1);
					String name = stmt.column_string(2);
					String centroidStr = stmt.column_string(3);
					Log.d(TAG, "centroidStr: " + centroidStr);
					Point3D centroidPoint = null;
					if (centroidStr != null) {
						JSONObject jsonGeom;
						try {
							jsonGeom = new JSONObject(centroidStr);
							String type = jsonGeom.getString("type");
							if (debug) Log.d(TAG, "type: " + type);
							JSONArray coordinates = jsonGeom.getJSONArray("coordinates");
							Object xObj = coordinates.get(0);
							double x = Double.parseDouble(xObj.toString());
							Object yObj = coordinates.get(1);
							double y = Double.parseDouble(yObj.toString());
							if (debug) Log.d(TAG, "(" + x + " , " + y + ")");
							centroidPoint = new Point3D(x, y, point.z);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					GeoFence fence = new GeoFence(id, localId, name);
					fence.setCentroid(centroidPoint);
					geoFences.add(fence);
				}
				stmt.close();
			} catch (Exception e) {
				error(e);
			}
		}
		if(debug){
			for (GeoFence fence : geoFences){
				Log.d(TAG, fence.toString());
			}
		}
		return geoFences;
	}

	public FenceInfoDB getFenceInfoDB(int fenceId, Point3D point){
		FenceInfoDB fenceInfoDB = null;
		String query = "SELECT ST_Distance("+COL_GEOM+",MakePoint("+point.x+","+point.y+","+SRID+")), AsGeoJSON(ClosestPoint("+COL_GEOM+",MakePoint("+point.x+","+point.y+","+SRID+"))), ST_Distance("+COL_REPRESENTATIVE_POINT+",MakePoint("+point.x+","+point.y+","+SRID+")), AsGeoJSON("+COL_REPRESENTATIVE_POINT+"),"+COL_DIST_MIN+","+COL_DIST_MAX+
				" FROM "+TB_POIS_GEOM+","+TB_POIS+
				" WHERE "+TB_POIS+"."+COL_ID+"="+TB_POIS_GEOM+"."+COL_POI_ID+" AND "+COL_POI_ID+"="+fenceId+" AND "+COL_FLOOR+"="+(int)point.z+";";

		if(debug)       Log.d(TAG, "query: " + query);
		if(dbOk) {
			try {
				Stmt stmt = db.prepare(query);
				if (stmt.step()) {
					// Fence is in the same floor as the user
					double distToGeom = stmt.column_double(0);
					String closestPointStr = stmt.column_string(1);
					double distToRepresentativePoint = stmt.column_double(2);
					String representativePointStr = stmt.column_string(3);
					double distMin = stmt.column_double(4);
					double distMax = stmt.column_double(5);
					Log.d(TAG, "closestPointStr: " + closestPointStr + ", representativePointStr: " + representativePointStr);
					Point3D closestPoint = null;
					if (closestPointStr != null) {
						JSONObject jsonGeom;
						try {
							jsonGeom = new JSONObject(closestPointStr);
							String type = jsonGeom.getString("type");
							if (debug) Log.d(TAG, "type: " + type);
							JSONArray coordinates = jsonGeom.getJSONArray("coordinates");
							Object xObj = coordinates.get(0);
							double x = Double.parseDouble(xObj.toString());
							Object yObj = coordinates.get(1);
							double y = Double.parseDouble(yObj.toString());
							if (debug) Log.d(TAG, "(" + x + " , " + y + ")");
							closestPoint = new Point3D(x, y, point.z);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					Point3D representativePoint = null;
					if (representativePointStr != null) {
						JSONObject jsonRP;
						try {
							jsonRP = new JSONObject(representativePointStr);
							String type = jsonRP.getString("type");
							if (debug) Log.d(TAG, "type: " + type);
							JSONArray coordinates = jsonRP.getJSONArray("coordinates");
							Object xObj = coordinates.get(0);
							double x = Double.parseDouble(xObj.toString());
							Object yObj = coordinates.get(1);
							double y = Double.parseDouble(yObj.toString());
							Object zObj = coordinates.get(2);
							double z = Double.parseDouble(zObj.toString());
							if (debug) Log.d(TAG, "(" + x + " , " + y + " , " + z + ")");
							representativePoint = new Point3D(x, y, z);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					fenceInfoDB = new FenceInfoDB(distToGeom, closestPoint, distToRepresentativePoint, representativePoint, distMin, distMax, 0);
				} else {
					// get floor of the Fence
					int fenceFloor;
					query = "SELECT " + COL_FLOOR +
							" FROM " + TB_POIS_GEOM +
							" WHERE " + COL_POI_ID + "=" + fenceId + ";";
					if (debug) Log.d(TAG, "query: " + query);
					stmt = db.prepare(query);
					if (stmt.step()) {
						fenceFloor = stmt.column_int(0);
						int diffFloor = fenceFloor - (int) point.z;
						fenceInfoDB = new FenceInfoDB(-1, null, -1, null, -1, -1, diffFloor);
					}
				}
				stmt.close();
			} catch (Exception e) {
				error(e);
			}
		}
		if(debug){
			if(fenceInfoDB!=null){
				Log.d(TAG, fenceInfoDB.toString());
			}
		}
		return fenceInfoDB;
	}

	public String getPosAB(int fenceId, Point3D point){
		String pos = "A";
		String query = "SELECT ST_Distance("+COL_POINT_A+",MakePoint("+point.x+","+point.y+","+SRID+")),"+"ST_Distance("+COL_POINT_B+",MakePoint("+point.x+","+point.y+","+SRID+"))"+
				" FROM "+TB_POIS_MULT+
				" WHERE "+COL_POI_ID+"="+fenceId+";";

		if(debug)       Log.d(TAG, "query: " + query);
		if(dbOk) {
			try {
				Stmt stmt = db.prepare(query);
				if (stmt.step()) {
					double distA = stmt.column_double(0);
					double distB = stmt.column_double(1);
					if (debug) Log.d(TAG, "distA: " + distA + " , distB: " + distB);
					if (distB < distA) {
						pos = "B";
					}
				}
				stmt.close();
			} catch (Exception e) {
				error(e);
			}
		}
		if(debug){
			Log.d(TAG, "pos: " + pos);
		}
		return pos;
	}

	public Point3D getCentroidOfSelectedFence(int fenceId, Point3D point){
		Point3D centroidPoint = null;
		// Try first with geometries of the same floor as the current point
		String query = "SELECT AsGeoJson(Centroid("+COL_GEOM+")),Distance(Centroid("+COL_GEOM+"),MakePoint("+point.x+","+point.y+","+SRID+")) as dist"+
				" FROM "+TB_POIS_GEOM+
				" WHERE "+COL_POI_ID+"="+fenceId+" AND "+COL_FLOOR+"="+(int)point.z+
				" ORDER BY dist ASC;";

		if(debug)       Log.d(TAG, "query: " + query);
		if(dbOk) {
			try {
				Stmt stmt = db.prepare(query);
				if (stmt.step()) {
					String centroidStr = stmt.column_string(0);
					Log.d(TAG, "centroidStr: " + centroidStr);
					if (centroidStr != null) {
						JSONObject jsonGeom;
						try {
							jsonGeom = new JSONObject(centroidStr);
							String type = jsonGeom.getString("type");
							if (debug) Log.d(TAG, "type: " + type);
							JSONArray coordinates = jsonGeom.getJSONArray("coordinates");
							Object xObj = coordinates.get(0);
							double x = Double.parseDouble(xObj.toString());
							Object yObj = coordinates.get(1);
							double y = Double.parseDouble(yObj.toString());
							if (debug) Log.d(TAG, "(" + x + " , " + y + ")");
							centroidPoint = new Point3D(x, y, point.z);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				} else {
					// if there are no geometries in the same floor, get all the centroids of the geometries and select the one with min dist to point
					query = "SELECT AsGeoJson(Centroid(" + COL_GEOM + "))," + COL_FLOOR + " FROM " + TB_POIS_GEOM +
							" WHERE " + COL_POI_ID + "=" + fenceId;

					if (debug) Log.d(TAG, "query: " + query);
					double minDist = Math.pow(10, 9);
					stmt = db.prepare(query);
					while (stmt.step()) {
						String centroidStr = stmt.column_string(0);
						int floor = stmt.column_int(1);
						Log.d(TAG, "centroidStr: " + centroidStr + " , floor: " + floor);
						if (centroidStr != null) {
							JSONObject jsonGeom;
							try {
								jsonGeom = new JSONObject(centroidStr);
								String type = jsonGeom.getString("type");
								if (debug) Log.d(TAG, "type: " + type);
								JSONArray coordinates = jsonGeom.getJSONArray("coordinates");
								Object xObj = coordinates.get(0);
								double x = Double.parseDouble(xObj.toString());
								Object yObj = coordinates.get(1);
								double y = Double.parseDouble(yObj.toString());
								if (debug) Log.d(TAG, "(" + x + " , " + y + ")");
								Point3D centroidCandidate = new Point3D(x, y, floor);
								double dist = distanceBetweenPoints(point, centroidCandidate);
								if (dist < minDist) {
									minDist = dist;
									centroidPoint = new Point3D(centroidCandidate.x, centroidCandidate.y, centroidCandidate.z);
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
				}
				stmt.close();
			} catch (Exception e) {
				error(e);
			}
		}
		if(debug){
			Log.d(TAG, "centroidPoint: " + centroidPoint);
		}
		return centroidPoint;
	}

	/** calculate distance between two points **/
	public double distanceBetweenPoints(Point3D a, Point3D b){
		float x = (float) (a.x - b.x);
		float y = (float) (a.y - b.y);
		float z = (float) (a.z - b.z);
		return Math.sqrt(x*x + y*y + z*z);
	}

}
