package com.ascamm.pois.spatialite;

import com.ascamm.utils.Point3D;

import java.io.Serializable;
import java.util.ArrayList;

public class GeoFence implements Serializable{

	private static final long serialVersionUID = 1L;

	private int id;
	private int localId;
	private String name;
	private ArrayList<Geometry> geometries;
	private Point3D centroid;
	private boolean isMultiple;
	private Point3D pointA;
	private Point3D pointB;
	private Point3D representativePoint;

	public GeoFence(int id, int localId, String name){
		this.id = id;
		this.localId = localId;
		this.name = name;
		isMultiple = false;
	}

	public GeoFence(int id, int localId, String name, ArrayList<Geometry> geometries){
		this.id = id;
		this.localId = localId;
		this.name = name;
		this.geometries = geometries;
		isMultiple = false;
	}

	public GeoFence(int id, int localId, String name, ArrayList<Geometry> geometries, boolean isMultiple, Point3D pointA, Point3D pointB, Point3D representativePoint){
		this.id = id;
		this.localId = localId;
		this.name = name;
		this.geometries = geometries;
		this.isMultiple = isMultiple;
		this.pointA = pointA;
		this.pointB = pointB;
		this.representativePoint = representativePoint;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLocalId() {
		return localId;
	}

	public void setLocalId(int localId) {
		this.localId = localId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Geometry> getGeometries() {
		return geometries;
	}

	public void setGeometries(ArrayList<Geometry> geometries) {
		this.geometries = geometries;
	}

	public Point3D getCentroid() {
		return centroid;
	}

	public void setCentroid(Point3D centroid) {
		this.centroid = centroid;
	}

	public boolean isMultiple() {
		return isMultiple;
	}

	public void setMultiple(boolean isMultiple) {
		this.isMultiple = isMultiple;
	}

	public Point3D getPointA() {
		return pointA;
	}

	public void setPointA(Point3D pointA) {
		this.pointA = pointA;
	}

	public Point3D getPointB() {
		return pointB;
	}

	public void setPointB(Point3D pointB) {
		this.pointB = pointB;
	}

	public Point3D getRepresentativePoint() {
		return representativePoint;
	}

	public void setRepresentativePoint(Point3D representativePoint) {
		this.representativePoint = representativePoint;
	}

	public String toString(){
		String pointAstr = null;
		String pointBstr = null;
		String reprPointStr = null;
		if(pointA!=null){
			pointAstr = pointA.toString();
		}
		if(pointB!=null){
			pointBstr = pointB.toString();
		}
		if(representativePoint!=null){
			reprPointStr = representativePoint.toString();
		}
		String res = "Fence: (" + id + "," + localId + "," + name + "," + isMultiple + "," + pointAstr + "," + pointBstr + "," + reprPointStr + ")";
		if(geometries!=null){
			for(Geometry geom : geometries){
				res += "\n" + geom.toString();
			}
		}
		return res;
	}

}
