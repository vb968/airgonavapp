package com.ascamm.pois.spatialite;

import com.ascamm.utils.Point3D;

import java.io.Serializable;
import java.util.ArrayList;

public class Geometry implements Serializable{

	private static final long serialVersionUID = 1L;

	private int poiId;
	private int floor;
	private ArrayList<Point3D> points;
	private Point3D pointA;
	private Point3D pointB;
	private Point3D representativePoint;

	public Geometry(int poiId, int floor, ArrayList<Point3D> points){
		this.poiId = poiId;
		this.floor = floor;
		this.points = points;
	}

	public int getPoiId() {
		return poiId;
	}

	public void setPoiId(int poiId) {
		this.poiId = poiId;
	}

	public int getFloor() {
		return floor;
	}

	public void setFloor(int floor) {
		this.floor = floor;
	}

	public ArrayList<Point3D> getPoints() {
		return points;
	}

	public void setPoints(ArrayList<Point3D> points) {
		this.points = points;
	}

	public Point3D getPointA() {
		return pointA;
	}

	public void setPointA(Point3D pointA) {
		this.pointA = pointA;
	}

	public Point3D getPointB() {
		return pointB;
	}

	public void setPointB(Point3D pointB) {
		this.pointB = pointB;
	}

	public Point3D getRepresentativePoint() {
		return representativePoint;
	}

	public void setRepresentativePoint(Point3D representativePoint) {
		this.representativePoint = representativePoint;
	}

	public String toString(){
		String res = "GEOM: (" + poiId + "," + floor + ",";
		if(points!=null){
			for(Point3D p : points){
				res += "[" + p.x + "," + p.y + "]";
			}
		}
		res += ")";
		return res;
	}

}
